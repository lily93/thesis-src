#include "stdsimheader.h"

namespace maxcompilersim {

void CellCableDFEKernel::execute0() {
  { // Node ID: 744 (NodeConstantRawBits)
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  { // Node ID: 876 (NodeConstantRawBits)
  }
  { // Node ID: 766 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id766in_input = id876out_value;

    id766out_output[(getCycle()+1)%2] = id766in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 46 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id46in_enable = id41out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id46in_max = id766out_output[getCycle()%2];

    HWOffsetFix<33,0,UNSIGNED> id46x_1;
    HWOffsetFix<1,0,UNSIGNED> id46x_2;
    HWOffsetFix<1,0,UNSIGNED> id46x_3;
    HWOffsetFix<33,0,UNSIGNED> id46x_4t_1e_1;

    id46out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id46st_count)));
    (id46x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id46st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id46x_2) = (gte_fixed((id46x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id46in_max))));
    (id46x_3) = (and_fixed((id46x_2),id46in_enable));
    id46out_wrap = (id46x_3);
    if((id46in_enable.getValueAsBool())) {
      if(((id46x_3).getValueAsBool())) {
        (id46st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id46x_4t_1e_1) = (id46x_1);
        (id46st_count) = (id46x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 45 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id45in_enable = id46out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id45in_max = id38out_nx;

    HWOffsetFix<33,0,UNSIGNED> id45x_1;
    HWOffsetFix<1,0,UNSIGNED> id45x_2;
    HWOffsetFix<1,0,UNSIGNED> id45x_3;
    HWOffsetFix<33,0,UNSIGNED> id45x_4t_1e_1;

    id45out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id45st_count)));
    (id45x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id45st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id45x_2) = (gte_fixed((id45x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id45in_max))));
    (id45x_3) = (and_fixed((id45x_2),id45in_enable));
    id45out_wrap = (id45x_3);
    if((id45in_enable.getValueAsBool())) {
      if(((id45x_3).getValueAsBool())) {
        (id45st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id45x_4t_1e_1) = (id45x_1);
        (id45st_count) = (id45x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 778 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id778in_input = id45out_count;

    id778out_output[(getCycle()+1)%2] = id778in_input;
  }
  { // Node ID: 861 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id861in_input = id778out_output[getCycle()%2];

    id861out_output[(getCycle()+6)%7] = id861in_input;
  }
  { // Node ID: 862 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id862in_input = id861out_output[getCycle()%7];

    id862out_output[(getCycle()+36)%37] = id862in_input;
  }
  { // Node ID: 863 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id863in_input = id862out_output[getCycle()%37];

    id863out_output[(getCycle()+1)%2] = id863in_input;
  }
  { // Node ID: 864 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id864in_input = id863out_output[getCycle()%2];

    id864out_output[(getCycle()+2)%3] = id864in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 745 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id745in_output_control = id744out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id745in_data = id864out_output[getCycle()%3];

    bool id745x_1;

    (id745x_1) = ((id745in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id745x_1)) {
      writeOutput(m_internal_watch_cell_output, id745in_data);
    }
  }
  { // Node ID: 768 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id768in_input = id45out_wrap;

    id768out_output[(getCycle()+2)%3] = id768in_input;
  }
  { // Node ID: 39 (NodeInputMappedReg)
  }
  { // Node ID: 949 (NodeConstantRawBits)
  }
  { // Node ID: 43 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id43in_a = id39out_simTime;
    const HWOffsetFix<32,0,UNSIGNED> &id43in_b = id949out_value;

    id43out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id43in_a,id43in_b));
  }
  if ( (getFillLevel() >= (5l)))
  { // Node ID: 44 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id44in_enable = id768out_output[getCycle()%3];
    const HWOffsetFix<32,0,UNSIGNED> &id44in_max = id43out_result[getCycle()%2];

    HWOffsetFix<33,0,UNSIGNED> id44x_1;
    HWOffsetFix<1,0,UNSIGNED> id44x_2;
    HWOffsetFix<1,0,UNSIGNED> id44x_3;
    HWOffsetFix<33,0,UNSIGNED> id44x_4t_1e_1;

    id44out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id44st_count)));
    (id44x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id44st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id44x_2) = (gte_fixed((id44x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id44in_max))));
    (id44x_3) = (and_fixed((id44x_2),id44in_enable));
    id44out_wrap = (id44x_3);
    if((id44in_enable.getValueAsBool())) {
      if(((id44x_3).getValueAsBool())) {
        (id44st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id44x_4t_1e_1) = (id44x_1);
        (id44st_count) = (id44x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 769 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id769in_input = id44out_count;

    id769out_output[(getCycle()+44)%45] = id769in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 746 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id746in_output_control = id744out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id746in_data = id769out_output[getCycle()%45];

    bool id746x_1;

    (id746x_1) = ((id746in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id746x_1)) {
      writeOutput(m_internal_watch_step_output, id746in_data);
    }
  }
  { // Node ID: 948 (NodeConstantRawBits)
  }
  { // Node ID: 688 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id688in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id688in_b = id948out_value;

    id688out_result[(getCycle()+1)%2] = (eq_fixed(id688in_a,id688in_b));
  }
  { // Node ID: 775 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id775in_input = id688out_result[getCycle()%2];

    id775out_output[(getCycle()+6)%7] = id775in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id759out_output;

  { // Node ID: 759 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id759in_input = id864out_output[getCycle()%3];

    id759out_output = id759in_input;
  }
  HWOffsetFix<20,0,UNSIGNED> id614out_o;

  { // Node ID: 614 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id614in_i = id759out_output;

    id614out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id614in_i));
  }
  { // Node ID: 772 (NodeFIFO)
    const HWOffsetFix<20,0,UNSIGNED> &id772in_input = id614out_o;

    id772out_output[(getCycle()+6)%7] = id772in_input;
  }
  { // Node ID: 947 (NodeConstantRawBits)
  }
  { // Node ID: 616 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id616in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id616in_b = id947out_value;

    id616out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id616in_a,id616in_b));
  }
  { // Node ID: 689 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id689in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id689in_b = id616out_result[getCycle()%2];

    id689out_result[(getCycle()+1)%2] = (eq_fixed(id689in_a,id689in_b));
  }
  { // Node ID: 774 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id774in_input = id689out_result[getCycle()%2];

    id774out_output[(getCycle()+6)%7] = id774in_input;
  }
  HWOffsetFix<20,0,UNSIGNED> id47out_o;

  { // Node ID: 47 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id47in_i = id861out_output[getCycle()%7];

    id47out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id47in_i));
  }
  if ( (getFillLevel() >= (10l)))
  { // Node ID: 666 (NodeRAM)
    const bool id666_inputvalid = !(isFlushingActive() && getFlushLevel() >= (10l));
    const HWOffsetFix<20,0,UNSIGNED> &id666in_addrA = id772out_output[getCycle()%7];
    const HWFloat<11,53> &id666in_dina = id874out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id666in_wea = id774out_output[getCycle()%7];
    const HWOffsetFix<20,0,UNSIGNED> &id666in_addrB = id47out_o;

    long id666x_1;
    long id666x_2;
    HWFloat<11,53> id666x_3;

    (id666x_1) = (id666in_addrA.getValueAsLong());
    (id666x_2) = (id666in_addrB.getValueAsLong());
    switch(((long)((id666x_2)<(1000000l)))) {
      case 0l:
        id666x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id666x_3 = (id666sta_ram_store[(id666x_2)]);
        break;
      default:
        id666x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id666out_doutb[(getCycle()+2)%3] = (id666x_3);
    if(((id666in_wea.getValueAsBool())&id666_inputvalid)) {
      if(((id666x_1)<(1000000l))) {
        (id666sta_ram_store[(id666x_1)]) = id666in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_1("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 666) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 55 (NodeConstantRawBits)
  }
  { // Node ID: 56 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id56in_sel = id775out_output[getCycle()%7];
    const HWFloat<11,53> &id56in_option0 = id666out_doutb[getCycle()%3];
    const HWFloat<11,53> &id56in_option1 = id55out_value;

    HWFloat<11,53> id56x_1;

    switch((id56in_sel.getValueAsLong())) {
      case 0l:
        id56x_1 = id56in_option0;
        break;
      case 1l:
        id56x_1 = id56in_option1;
        break;
      default:
        id56x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id56out_result[(getCycle()+1)%2] = (id56x_1);
  }
  { // Node ID: 849 (NodeFIFO)
    const HWFloat<11,53> &id849in_input = id56out_result[getCycle()%2];

    id849out_output[(getCycle()+7)%8] = id849in_input;
  }
  { // Node ID: 865 (NodeFIFO)
    const HWFloat<11,53> &id865in_input = id849out_output[getCycle()%8];

    id865out_output[(getCycle()+1)%2] = id865in_input;
  }
  { // Node ID: 866 (NodeFIFO)
    const HWFloat<11,53> &id866in_input = id865out_output[getCycle()%2];

    id866out_output[(getCycle()+1)%2] = id866in_input;
  }
  { // Node ID: 867 (NodeFIFO)
    const HWFloat<11,53> &id867in_input = id866out_output[getCycle()%2];

    id867out_output[(getCycle()+1)%2] = id867in_input;
  }
  { // Node ID: 868 (NodeFIFO)
    const HWFloat<11,53> &id868in_input = id867out_output[getCycle()%2];

    id868out_output[(getCycle()+17)%18] = id868in_input;
  }
  { // Node ID: 869 (NodeFIFO)
    const HWFloat<11,53> &id869in_input = id868out_output[getCycle()%18];

    id869out_output[(getCycle()+9)%10] = id869in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id581out_result;

  { // Node ID: 581 (NodeGt)
    const HWFloat<11,53> &id581in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id581in_b = id15out_value;

    id581out_result = (gt_float(id581in_a,id581in_b));
  }
  { // Node ID: 34 (NodeConstantRawBits)
  }
  HWOffsetFix<20,0,UNSIGNED> id618out_o;

  { // Node ID: 618 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id618in_i = id759out_output;

    id618out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id618in_i));
  }
  HWFloat<11,53> id760out_output;

  { // Node ID: 760 (NodeStreamOffset)
    const HWFloat<11,53> &id760in_input = id777out_output[getCycle()%11];

    id760out_output = id760in_input;
  }
  { // Node ID: 946 (NodeConstantRawBits)
  }
  { // Node ID: 620 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id620in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id620in_b = id946out_value;

    id620out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id620in_a,id620in_b));
  }
  { // Node ID: 690 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id690in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id690in_b = id620out_result[getCycle()%2];

    id690out_result[(getCycle()+1)%2] = (eq_fixed(id690in_a,id690in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id48out_o;

  { // Node ID: 48 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id48in_i = id778out_output[getCycle()%2];

    id48out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id48in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 669 (NodeRAM)
    const bool id669_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id669in_addrA = id618out_o;
    const HWFloat<11,53> &id669in_dina = id760out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id669in_wea = id690out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id669in_addrB = id48out_o;

    long id669x_1;
    long id669x_2;
    HWFloat<11,53> id669x_3;

    (id669x_1) = (id669in_addrA.getValueAsLong());
    (id669x_2) = (id669in_addrB.getValueAsLong());
    switch(((long)((id669x_2)<(1000000l)))) {
      case 0l:
        id669x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id669x_3 = (id669sta_ram_store[(id669x_2)]);
        break;
      default:
        id669x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id669out_doutb[(getCycle()+2)%3] = (id669x_3);
    if(((id669in_wea.getValueAsBool())&id669_inputvalid)) {
      if(((id669x_1)<(1000000l))) {
        (id669sta_ram_store[(id669x_1)]) = id669in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_2("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 669) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 780 (NodeFIFO)
    const HWFloat<11,53> &id780in_input = id669out_doutb[getCycle()%3];

    id780out_output[(getCycle()+6)%7] = id780in_input;
  }
  { // Node ID: 57 (NodeConstantRawBits)
  }
  { // Node ID: 58 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id58in_sel = id775out_output[getCycle()%7];
    const HWFloat<11,53> &id58in_option0 = id780out_output[getCycle()%7];
    const HWFloat<11,53> &id58in_option1 = id57out_value;

    HWFloat<11,53> id58x_1;

    switch((id58in_sel.getValueAsLong())) {
      case 0l:
        id58x_1 = id58in_option0;
        break;
      case 1l:
        id58x_1 = id58in_option1;
        break;
      default:
        id58x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id58out_result[(getCycle()+1)%2] = (id58x_1);
  }
  { // Node ID: 781 (NodeFIFO)
    const HWFloat<11,53> &id781in_input = id58out_result[getCycle()%2];

    id781out_output[(getCycle()+12)%13] = id781in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id454out_result;

  { // Node ID: 454 (NodeGt)
    const HWFloat<11,53> &id454in_a = id56out_result[getCycle()%2];
    const HWFloat<11,53> &id454in_b = id15out_value;

    id454out_result = (gt_float(id454in_a,id454in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id445out_result;

  { // Node ID: 445 (NodeGt)
    const HWFloat<11,53> &id445in_a = id56out_result[getCycle()%2];
    const HWFloat<11,53> &id445in_b = id17out_value;

    id445out_result = (gt_float(id445in_a,id445in_b));
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  HWFloat<11,53> id446out_result;

  { // Node ID: 446 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id446in_sel = id445out_result;
    const HWFloat<11,53> &id446in_option0 = id35out_value;
    const HWFloat<11,53> &id446in_option1 = id34out_value;

    HWFloat<11,53> id446x_1;

    switch((id446in_sel.getValueAsLong())) {
      case 0l:
        id446x_1 = id446in_option0;
        break;
      case 1l:
        id446x_1 = id446in_option1;
        break;
      default:
        id446x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id446out_result = (id446x_1);
  }
  HWFloat<11,53> id457out_result;

  { // Node ID: 457 (NodeSub)
    const HWFloat<11,53> &id457in_a = id446out_result;
    const HWFloat<11,53> &id457in_b = id58out_result[getCycle()%2];

    id457out_result = (sub_float(id457in_a,id457in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id225out_result;

  { // Node ID: 225 (NodeGt)
    const HWFloat<11,53> &id225in_a = id56out_result[getCycle()%2];
    const HWFloat<11,53> &id225in_b = id16out_value;

    id225out_result = (gt_float(id225in_a,id225in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<11,53> id226out_result;

  { // Node ID: 226 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id226in_sel = id225out_result;
    const HWFloat<11,53> &id226in_option0 = id1out_value;
    const HWFloat<11,53> &id226in_option1 = id2out_value;

    HWFloat<11,53> id226x_1;

    switch((id226in_sel.getValueAsLong())) {
      case 0l:
        id226x_1 = id226in_option0;
        break;
      case 1l:
        id226x_1 = id226in_option1;
        break;
      default:
        id226x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id226out_result = (id226x_1);
  }
  HWFloat<11,53> id458out_result;

  { // Node ID: 458 (NodeDiv)
    const HWFloat<11,53> &id458in_a = id457out_result;
    const HWFloat<11,53> &id458in_b = id226out_result;

    id458out_result = (div_float(id458in_a,id458in_b));
  }
  HWFloat<11,53> id455out_result;

  { // Node ID: 455 (NodeNeg)
    const HWFloat<11,53> &id455in_a = id58out_result[getCycle()%2];

    id455out_result = (neg_float(id455in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  HWFloat<11,53> id456out_result;

  { // Node ID: 456 (NodeDiv)
    const HWFloat<11,53> &id456in_a = id455out_result;
    const HWFloat<11,53> &id456in_b = id0out_value;

    id456out_result = (div_float(id456in_a,id456in_b));
  }
  HWFloat<11,53> id459out_result;

  { // Node ID: 459 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id459in_sel = id454out_result;
    const HWFloat<11,53> &id459in_option0 = id458out_result;
    const HWFloat<11,53> &id459in_option1 = id456out_result;

    HWFloat<11,53> id459x_1;

    switch((id459in_sel.getValueAsLong())) {
      case 0l:
        id459x_1 = id459in_option0;
        break;
      case 1l:
        id459x_1 = id459in_option1;
        break;
      default:
        id459x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id459out_result = (id459x_1);
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 575 (NodeMul)
    const HWFloat<11,53> &id575in_a = id459out_result;
    const HWFloat<11,53> &id575in_b = id36out_dt;

    id575out_result[(getCycle()+12)%13] = (mul_float(id575in_a,id575in_b));
  }
  { // Node ID: 576 (NodeAdd)
    const HWFloat<11,53> &id576in_a = id781out_output[getCycle()%13];
    const HWFloat<11,53> &id576in_b = id575out_result[getCycle()%13];

    id576out_result[(getCycle()+14)%15] = (add_float(id576in_a,id576in_b));
  }
  { // Node ID: 777 (NodeFIFO)
    const HWFloat<11,53> &id777in_input = id576out_result[getCycle()%15];

    id777out_output[(getCycle()+10)%11] = id777in_input;
  }
  HWFloat<11,53> id582out_result;

  { // Node ID: 582 (NodeNeg)
    const HWFloat<11,53> &id582in_a = id777out_output[getCycle()%11];

    id582out_result = (neg_float(id582in_a));
  }
  HWFloat<11,53> id583out_result;

  { // Node ID: 583 (NodeSub)
    const HWFloat<11,53> &id583in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id583in_b = id15out_value;

    id583out_result = (sub_float(id583in_a,id583in_b));
  }
  HWFloat<11,53> id584out_result;

  { // Node ID: 584 (NodeMul)
    const HWFloat<11,53> &id584in_a = id582out_result;
    const HWFloat<11,53> &id584in_b = id583out_result;

    id584out_result = (mul_float(id584in_a,id584in_b));
  }
  { // Node ID: 28 (NodeConstantRawBits)
  }
  HWFloat<11,53> id585out_result;

  { // Node ID: 585 (NodeSub)
    const HWFloat<11,53> &id585in_a = id28out_value;
    const HWFloat<11,53> &id585in_b = id869out_output[getCycle()%10];

    id585out_result = (sub_float(id585in_a,id585in_b));
  }
  HWFloat<11,53> id586out_result;

  { // Node ID: 586 (NodeMul)
    const HWFloat<11,53> &id586in_a = id584out_result;
    const HWFloat<11,53> &id586in_b = id585out_result;

    id586out_result = (mul_float(id586in_a,id586in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  HWFloat<11,53> id587out_result;

  { // Node ID: 587 (NodeDiv)
    const HWFloat<11,53> &id587in_a = id586out_result;
    const HWFloat<11,53> &id587in_b = id8out_value;

    id587out_result = (div_float(id587in_a,id587in_b));
  }
  HWFloat<11,53> id588out_result;

  { // Node ID: 588 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id588in_sel = id581out_result;
    const HWFloat<11,53> &id588in_option0 = id34out_value;
    const HWFloat<11,53> &id588in_option1 = id587out_result;

    HWFloat<11,53> id588x_1;

    switch((id588in_sel.getValueAsLong())) {
      case 0l:
        id588x_1 = id588in_option0;
        break;
      case 1l:
        id588x_1 = id588in_option1;
        break;
      default:
        id588x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id588out_result = (id588x_1);
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id589out_result;

  { // Node ID: 589 (NodeGt)
    const HWFloat<11,53> &id589in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id589in_b = id19out_value;

    id589out_result = (gt_float(id589in_a,id589in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id443out_result;

  { // Node ID: 443 (NodeGt)
    const HWFloat<11,53> &id443in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id443in_b = id21out_value;

    id443out_result = (gt_float(id443in_a,id443in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  HWFloat<11,53> id444out_result;

  { // Node ID: 444 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id444in_sel = id443out_result;
    const HWFloat<11,53> &id444in_option0 = id9out_value;
    const HWFloat<11,53> &id444in_option1 = id10out_value;

    HWFloat<11,53> id444x_1;

    switch((id444in_sel.getValueAsLong())) {
      case 0l:
        id444x_1 = id444in_option0;
        break;
      case 1l:
        id444x_1 = id444in_option1;
        break;
      default:
        id444x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id444out_result = (id444x_1);
  }
  HWFloat<11,53> id593out_result;

  { // Node ID: 593 (NodeDiv)
    const HWFloat<11,53> &id593in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id593in_b = id444out_result;

    id593out_result = (div_float(id593in_a,id593in_b));
  }
  { // Node ID: 945 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 31 (NodeConstantRawBits)
  }
  { // Node ID: 944 (NodeConstantRawBits)
  }
  { // Node ID: 943 (NodeConstantRawBits)
  }
  { // Node ID: 942 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  HWFloat<11,53> id334out_result;

  { // Node ID: 334 (NodeSub)
    const HWFloat<11,53> &id334in_a = id868out_output[getCycle()%18];
    const HWFloat<11,53> &id334in_b = id29out_value;

    id334out_result = (sub_float(id334in_a,id334in_b));
  }
  HWFloat<11,53> id335out_result;

  { // Node ID: 335 (NodeMul)
    const HWFloat<11,53> &id335in_a = id24out_value;
    const HWFloat<11,53> &id335in_b = id334out_result;

    id335out_result = (mul_float(id335in_a,id335in_b));
  }
  { // Node ID: 735 (NodePO2FPMult)
    const HWFloat<11,53> &id735in_floatIn = id335out_result;

    id735out_floatOut[(getCycle()+1)%2] = (mul_float(id735in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id414out_result;

  { // Node ID: 414 (NodeSlice)
    const HWFloat<11,53> &id414in_a = id735out_floatOut[getCycle()%2];

    id414out_result = (slice<52,11>(id414in_a));
  }
  { // Node ID: 415 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id691out_result;

  { // Node ID: 691 (NodeEqInlined)
    const HWRawBits<11> &id691in_a = id414out_result;
    const HWRawBits<11> &id691in_b = id415out_value;

    id691out_result = (eq_bits(id691in_a,id691in_b));
  }
  HWRawBits<52> id413out_result;

  { // Node ID: 413 (NodeSlice)
    const HWFloat<11,53> &id413in_a = id735out_floatOut[getCycle()%2];

    id413out_result = (slice<0,52>(id413in_a));
  }
  { // Node ID: 941 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id692out_result;

  { // Node ID: 692 (NodeNeqInlined)
    const HWRawBits<52> &id692in_a = id413out_result;
    const HWRawBits<52> &id692in_b = id941out_value;

    id692out_result = (neq_bits(id692in_a,id692in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id419out_result;

  { // Node ID: 419 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id419in_a = id691out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id419in_b = id692out_result;

    HWOffsetFix<1,0,UNSIGNED> id419x_1;

    (id419x_1) = (and_fixed(id419in_a,id419in_b));
    id419out_result = (id419x_1);
  }
  { // Node ID: 798 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id798in_input = id419out_result;

    id798out_output[(getCycle()+8)%9] = id798in_input;
  }
  { // Node ID: 338 (NodeConstantRawBits)
  }
  HWFloat<11,53> id339out_output;
  HWOffsetFix<1,0,UNSIGNED> id339out_output_doubt;

  { // Node ID: 339 (NodeDoubtBitOp)
    const HWFloat<11,53> &id339in_input = id735out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id339in_doubt = id338out_value;

    id339out_output = id339in_input;
    id339out_output_doubt = id339in_doubt;
  }
  { // Node ID: 340 (NodeCast)
    const HWFloat<11,53> &id340in_i = id339out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id340in_i_doubt = id339out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id340x_1;

    id340out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id340in_i,(&(id340x_1))));
    id340out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id340x_1),(c_hw_fix_4_0_uns_bits))),id340in_i_doubt));
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id342out_result;
  HWOffsetFix<1,0,UNSIGNED> id342out_result_doubt;

  { // Node ID: 342 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id342in_a = id340out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id342in_a_doubt = id340out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id342in_b = id343out_value;

    HWOffsetFix<1,0,UNSIGNED> id342x_1;

    id342out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id342in_a,id342in_b,(&(id342x_1))));
    id342out_result_doubt = (or_fixed((neq_fixed((id342x_1),(c_hw_fix_1_0_uns_bits_1))),id342in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id344out_o;
  HWOffsetFix<1,0,UNSIGNED> id344out_o_doubt;

  { // Node ID: 344 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id344in_i = id342out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id344in_i_doubt = id342out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id344x_1;

    id344out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id344in_i,(&(id344x_1))));
    id344out_o_doubt = (or_fixed((neq_fixed((id344x_1),(c_hw_fix_1_0_uns_bits_1))),id344in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id353out_output;

  { // Node ID: 353 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id353in_input = id344out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id353in_input_doubt = id344out_o_doubt;

    id353out_output = id353in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id354out_o;

  { // Node ID: 354 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id354in_i = id353out_output;

    id354out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id354in_i));
  }
  { // Node ID: 789 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id789in_input = id354out_o;

    id789out_output[(getCycle()+2)%3] = id789in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id381out_o;

  { // Node ID: 381 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id381in_i = id789out_output[getCycle()%3];

    id381out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id381in_i));
  }
  { // Node ID: 384 (NodeConstantRawBits)
  }
  { // Node ID: 726 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id357out_o;

  { // Node ID: 357 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id357in_i = id353out_output;

    id357out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id357in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id429out_output;

  { // Node ID: 429 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id429in_input = id357out_o;

    id429out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id429in_input))));
  }
  { // Node ID: 430 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id430in_addr = id429out_output;

    HWOffsetFix<38,-53,UNSIGNED> id430x_1;

    switch(((long)((id430in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id430x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id430x_1 = (id430sta_rom_store[(id430in_addr.getValueAsLong())]);
        break;
      default:
        id430x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id430out_dout[(getCycle()+2)%3] = (id430x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id356out_o;

  { // Node ID: 356 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id356in_i = id353out_output;

    id356out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id356in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id426out_output;

  { // Node ID: 426 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id426in_input = id356out_o;

    id426out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id426in_input))));
  }
  { // Node ID: 427 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id427in_addr = id426out_output;

    HWOffsetFix<48,-53,UNSIGNED> id427x_1;

    switch(((long)((id427in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id427x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id427x_1 = (id427sta_rom_store[(id427in_addr.getValueAsLong())]);
        break;
      default:
        id427x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id427out_dout[(getCycle()+2)%3] = (id427x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id355out_o;

  { // Node ID: 355 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id355in_i = id353out_output;

    id355out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id355in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id423out_output;

  { // Node ID: 423 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id423in_input = id355out_o;

    id423out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id423in_input))));
  }
  { // Node ID: 424 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id424in_addr = id423out_output;

    HWOffsetFix<53,-53,UNSIGNED> id424x_1;

    switch(((long)((id424in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id424x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id424x_1 = (id424sta_rom_store[(id424in_addr.getValueAsLong())]);
        break;
      default:
        id424x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id424out_dout[(getCycle()+2)%3] = (id424x_1);
  }
  { // Node ID: 361 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id358out_o;

  { // Node ID: 358 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id358in_i = id353out_output;

    id358out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id358in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id360out_result;

  { // Node ID: 360 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id360in_a = id361out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id360in_b = id358out_o;

    id360out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id360in_a,id360in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id362out_o;

  { // Node ID: 362 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id362in_i = id360out_result;

    id362out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id362in_i));
  }
  { // Node ID: 790 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id790in_input = id362out_o;

    id790out_output[(getCycle()+2)%3] = id790in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id363out_result;

  { // Node ID: 363 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id363in_a = id424out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id363in_b = id790out_output[getCycle()%3];

    id363out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id363in_a,id363in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id364out_result;

  { // Node ID: 364 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id364in_a = id790out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id364in_b = id424out_dout[getCycle()%3];

    id364out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id364in_a,id364in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id365out_result;

  { // Node ID: 365 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id365in_a = id363out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id365in_b = id364out_result;

    id365out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id365in_a,id365in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id366out_o;

  { // Node ID: 366 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id366in_i = id365out_result;

    id366out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id366in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id367out_result;

  { // Node ID: 367 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id367in_a = id427out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id367in_b = id366out_o;

    id367out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id367in_a,id367in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id368out_result;

  { // Node ID: 368 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id368in_a = id366out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id368in_b = id427out_dout[getCycle()%3];

    id368out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id368in_a,id368in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id369out_result;

  { // Node ID: 369 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id369in_a = id367out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id369in_b = id368out_result;

    id369out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id369in_a,id369in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id370out_o;

  { // Node ID: 370 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id370in_i = id369out_result;

    id370out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id370in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id371out_result;

  { // Node ID: 371 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id371in_a = id430out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id371in_b = id370out_o;

    id371out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id371in_a,id371in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id372out_result;

  { // Node ID: 372 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id372in_a = id370out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id372in_b = id430out_dout[getCycle()%3];

    id372out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id372in_a,id372in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id373out_result;

  { // Node ID: 373 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id373in_a = id371out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id373in_b = id372out_result;

    id373out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id373in_a,id373in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id374out_o;

  { // Node ID: 374 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id374in_i = id373out_result;

    id374out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id374in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id375out_o;

  { // Node ID: 375 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id375in_i = id374out_o;

    id375out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id375in_i));
  }
  { // Node ID: 940 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id693out_result;

  { // Node ID: 693 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id693in_a = id375out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id693in_b = id940out_value;

    id693out_result = (gte_fixed(id693in_a,id693in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id734out_result;

  { // Node ID: 734 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id734in_a = id381out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id734in_b = id384out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id734in_c = id726out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_condb = id693out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id734x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id734x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id734x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id734x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id734x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id734x_1 = id734in_a;
        break;
      default:
        id734x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id734in_condb.getValueAsLong())) {
      case 0l:
        id734x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id734x_2 = id734in_b;
        break;
      default:
        id734x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id734x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id734x_3 = id734in_c;
        break;
      default:
        id734x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id734x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id734x_1),(id734x_2))),(id734x_3)));
    id734out_result = (id734x_4);
  }
  HWRawBits<1> id694out_result;

  { // Node ID: 694 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id694in_a = id734out_result;

    id694out_result = (slice<13,1>(id694in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id695out_output;

  { // Node ID: 695 (NodeReinterpret)
    const HWRawBits<1> &id695in_input = id694out_result;

    id695out_output = (cast_bits2fixed<1,0,UNSIGNED>(id695in_input));
  }
  { // Node ID: 939 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id346out_result;

  { // Node ID: 346 (NodeGt)
    const HWFloat<11,53> &id346in_a = id735out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id346in_b = id939out_value;

    id346out_result = (gt_float(id346in_a,id346in_b));
  }
  { // Node ID: 792 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id792in_input = id346out_result;

    id792out_output[(getCycle()+6)%7] = id792in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id347out_output;

  { // Node ID: 347 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id347in_input = id344out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id347in_input_doubt = id344out_o_doubt;

    id347out_output = id347in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id348out_result;

  { // Node ID: 348 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id348in_a = id792out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id348in_b = id347out_output;

    HWOffsetFix<1,0,UNSIGNED> id348x_1;

    (id348x_1) = (and_fixed(id348in_a,id348in_b));
    id348out_result = (id348x_1);
  }
  { // Node ID: 793 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id793in_input = id348out_result;

    id793out_output[(getCycle()+2)%3] = id793in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id390out_result;

  { // Node ID: 390 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id390in_a = id793out_output[getCycle()%3];

    id390out_result = (not_fixed(id390in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id391out_result;

  { // Node ID: 391 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id391in_a = id695out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id391in_b = id390out_result;

    HWOffsetFix<1,0,UNSIGNED> id391x_1;

    (id391x_1) = (and_fixed(id391in_a,id391in_b));
    id391out_result = (id391x_1);
  }
  { // Node ID: 938 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id350out_result;

  { // Node ID: 350 (NodeLt)
    const HWFloat<11,53> &id350in_a = id735out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id350in_b = id938out_value;

    id350out_result = (lt_float(id350in_a,id350in_b));
  }
  { // Node ID: 794 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id794in_input = id350out_result;

    id794out_output[(getCycle()+6)%7] = id794in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id351out_output;

  { // Node ID: 351 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id351in_input = id344out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id351in_input_doubt = id344out_o_doubt;

    id351out_output = id351in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id352out_result;

  { // Node ID: 352 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id352in_a = id794out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id352in_b = id351out_output;

    HWOffsetFix<1,0,UNSIGNED> id352x_1;

    (id352x_1) = (and_fixed(id352in_a,id352in_b));
    id352out_result = (id352x_1);
  }
  { // Node ID: 795 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id795in_input = id352out_result;

    id795out_output[(getCycle()+2)%3] = id795in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id392out_result;

  { // Node ID: 392 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id392in_a = id391out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id392in_b = id795out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id392x_1;

    (id392x_1) = (or_fixed(id392in_a,id392in_b));
    id392out_result = (id392x_1);
  }
  { // Node ID: 937 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id696out_result;

  { // Node ID: 696 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id696in_a = id734out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id696in_b = id937out_value;

    id696out_result = (gte_fixed(id696in_a,id696in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id399out_result;

  { // Node ID: 399 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_a = id795out_output[getCycle()%3];

    id399out_result = (not_fixed(id399in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id400out_result;

  { // Node ID: 400 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id696out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id400in_b = id399out_result;

    HWOffsetFix<1,0,UNSIGNED> id400x_1;

    (id400x_1) = (and_fixed(id400in_a,id400in_b));
    id400out_result = (id400x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_a = id400out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id401in_b = id793out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id401x_1;

    (id401x_1) = (or_fixed(id401in_a,id401in_b));
    id401out_result = (id401x_1);
  }
  HWRawBits<2> id402out_result;

  { // Node ID: 402 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_in0 = id392out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id402in_in1 = id401out_result;

    id402out_result = (cat(id402in_in0,id402in_in1));
  }
  { // Node ID: 394 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id393out_o;

  { // Node ID: 393 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id393in_i = id734out_result;

    id393out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id393in_i));
  }
  { // Node ID: 378 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id379out_result;

  { // Node ID: 379 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id379in_sel = id693out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id379in_option0 = id375out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id379in_option1 = id378out_value;

    HWOffsetFix<53,-52,UNSIGNED> id379x_1;

    switch((id379in_sel.getValueAsLong())) {
      case 0l:
        id379x_1 = id379in_option0;
        break;
      case 1l:
        id379x_1 = id379in_option1;
        break;
      default:
        id379x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id379out_result = (id379x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id380out_o;

  { // Node ID: 380 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id380in_i = id379out_result;

    id380out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id380in_i));
  }
  HWRawBits<64> id395out_result;

  { // Node ID: 395 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id395in_in0 = id394out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id395in_in1 = id393out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id395in_in2 = id380out_o;

    id395out_result = (cat((cat(id395in_in0,id395in_in1)),id395in_in2));
  }
  HWFloat<11,53> id396out_output;

  { // Node ID: 396 (NodeReinterpret)
    const HWRawBits<64> &id396in_input = id395out_result;

    id396out_output = (cast_bits2float<11,53>(id396in_input));
  }
  { // Node ID: 403 (NodeConstantRawBits)
  }
  { // Node ID: 404 (NodeConstantRawBits)
  }
  { // Node ID: 406 (NodeConstantRawBits)
  }
  HWRawBits<64> id697out_result;

  { // Node ID: 697 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id697in_in0 = id403out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id697in_in1 = id404out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id697in_in2 = id406out_value;

    id697out_result = (cat((cat(id697in_in0,id697in_in1)),id697in_in2));
  }
  HWFloat<11,53> id408out_output;

  { // Node ID: 408 (NodeReinterpret)
    const HWRawBits<64> &id408in_input = id697out_result;

    id408out_output = (cast_bits2float<11,53>(id408in_input));
  }
  { // Node ID: 685 (NodeConstantRawBits)
  }
  HWFloat<11,53> id411out_result;

  { // Node ID: 411 (NodeMux)
    const HWRawBits<2> &id411in_sel = id402out_result;
    const HWFloat<11,53> &id411in_option0 = id396out_output;
    const HWFloat<11,53> &id411in_option1 = id408out_output;
    const HWFloat<11,53> &id411in_option2 = id685out_value;
    const HWFloat<11,53> &id411in_option3 = id408out_output;

    HWFloat<11,53> id411x_1;

    switch((id411in_sel.getValueAsLong())) {
      case 0l:
        id411x_1 = id411in_option0;
        break;
      case 1l:
        id411x_1 = id411in_option1;
        break;
      case 2l:
        id411x_1 = id411in_option2;
        break;
      case 3l:
        id411x_1 = id411in_option3;
        break;
      default:
        id411x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id411out_result = (id411x_1);
  }
  { // Node ID: 936 (NodeConstantRawBits)
  }
  HWFloat<11,53> id421out_result;

  { // Node ID: 421 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id421in_sel = id798out_output[getCycle()%9];
    const HWFloat<11,53> &id421in_option0 = id411out_result;
    const HWFloat<11,53> &id421in_option1 = id936out_value;

    HWFloat<11,53> id421x_1;

    switch((id421in_sel.getValueAsLong())) {
      case 0l:
        id421x_1 = id421in_option0;
        break;
      case 1l:
        id421x_1 = id421in_option1;
        break;
      default:
        id421x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id421out_result = (id421x_1);
  }
  { // Node ID: 935 (NodeConstantRawBits)
  }
  HWFloat<11,53> id432out_result;

  { // Node ID: 432 (NodeAdd)
    const HWFloat<11,53> &id432in_a = id421out_result;
    const HWFloat<11,53> &id432in_b = id935out_value;

    id432out_result = (add_float(id432in_a,id432in_b));
  }
  HWFloat<11,53> id434out_result;

  { // Node ID: 434 (NodeDiv)
    const HWFloat<11,53> &id434in_a = id942out_value;
    const HWFloat<11,53> &id434in_b = id432out_result;

    id434out_result = (div_float(id434in_a,id434in_b));
  }
  HWFloat<11,53> id436out_result;

  { // Node ID: 436 (NodeSub)
    const HWFloat<11,53> &id436in_a = id943out_value;
    const HWFloat<11,53> &id436in_b = id434out_result;

    id436out_result = (sub_float(id436in_a,id436in_b));
  }
  HWFloat<11,53> id438out_result;

  { // Node ID: 438 (NodeAdd)
    const HWFloat<11,53> &id438in_a = id944out_value;
    const HWFloat<11,53> &id438in_b = id436out_result;

    id438out_result = (add_float(id438in_a,id438in_b));
  }
  HWFloat<11,53> id439out_result;

  { // Node ID: 439 (NodeMul)
    const HWFloat<11,53> &id439in_a = id31out_value;
    const HWFloat<11,53> &id439in_b = id438out_result;

    id439out_result = (mul_float(id439in_a,id439in_b));
  }
  HWFloat<11,53> id440out_result;

  { // Node ID: 440 (NodeAdd)
    const HWFloat<11,53> &id440in_a = id11out_value;
    const HWFloat<11,53> &id440in_b = id439out_result;

    id440out_result = (add_float(id440in_a,id440in_b));
  }
  HWFloat<11,53> id591out_result;

  { // Node ID: 591 (NodeDiv)
    const HWFloat<11,53> &id591in_a = id945out_value;
    const HWFloat<11,53> &id591in_b = id440out_result;

    id591out_result = (div_float(id591in_a,id591in_b));
  }
  HWFloat<11,53> id594out_result;

  { // Node ID: 594 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id594in_sel = id589out_result;
    const HWFloat<11,53> &id594in_option0 = id593out_result;
    const HWFloat<11,53> &id594in_option1 = id591out_result;

    HWFloat<11,53> id594x_1;

    switch((id594in_sel.getValueAsLong())) {
      case 0l:
        id594x_1 = id594in_option0;
        break;
      case 1l:
        id594x_1 = id594in_option1;
        break;
      default:
        id594x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id594out_result = (id594x_1);
  }
  HWFloat<11,53> id601out_result;

  { // Node ID: 601 (NodeAdd)
    const HWFloat<11,53> &id601in_a = id588out_result;
    const HWFloat<11,53> &id601in_b = id594out_result;

    id601out_result = (add_float(id601in_a,id601in_b));
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id595out_result;

  { // Node ID: 595 (NodeGt)
    const HWFloat<11,53> &id595in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id595in_b = id20out_value;

    id595out_result = (gt_float(id595in_a,id595in_b));
  }
  { // Node ID: 599 (NodeConstantRawBits)
  }
  HWOffsetFix<20,0,UNSIGNED> id622out_o;

  { // Node ID: 622 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id622in_i = id759out_output;

    id622out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id622in_i));
  }
  HWFloat<11,53> id761out_output;

  { // Node ID: 761 (NodeStreamOffset)
    const HWFloat<11,53> &id761in_input = id800out_output[getCycle()%2];

    id761out_output = id761in_input;
  }
  { // Node ID: 934 (NodeConstantRawBits)
  }
  { // Node ID: 624 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id624in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id624in_b = id934out_value;

    id624out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id624in_a,id624in_b));
  }
  { // Node ID: 698 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id698in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id698in_b = id624out_result[getCycle()%2];

    id698out_result[(getCycle()+1)%2] = (eq_fixed(id698in_a,id698in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id49out_o;

  { // Node ID: 49 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id49in_i = id778out_output[getCycle()%2];

    id49out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id49in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 668 (NodeRAM)
    const bool id668_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id668in_addrA = id622out_o;
    const HWFloat<11,53> &id668in_dina = id761out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id668in_wea = id698out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id668in_addrB = id49out_o;

    long id668x_1;
    long id668x_2;
    HWFloat<11,53> id668x_3;

    (id668x_1) = (id668in_addrA.getValueAsLong());
    (id668x_2) = (id668in_addrB.getValueAsLong());
    switch(((long)((id668x_2)<(1000000l)))) {
      case 0l:
        id668x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id668x_3 = (id668sta_ram_store[(id668x_2)]);
        break;
      default:
        id668x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id668out_doutb[(getCycle()+2)%3] = (id668x_3);
    if(((id668in_wea.getValueAsBool())&id668_inputvalid)) {
      if(((id668x_1)<(1000000l))) {
        (id668sta_ram_store[(id668x_1)]) = id668in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_3("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 668) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 803 (NodeFIFO)
    const HWFloat<11,53> &id803in_input = id668out_doutb[getCycle()%3];

    id803out_output[(getCycle()+6)%7] = id803in_input;
  }
  { // Node ID: 59 (NodeConstantRawBits)
  }
  { // Node ID: 60 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_sel = id775out_output[getCycle()%7];
    const HWFloat<11,53> &id60in_option0 = id803out_output[getCycle()%7];
    const HWFloat<11,53> &id60in_option1 = id59out_value;

    HWFloat<11,53> id60x_1;

    switch((id60in_sel.getValueAsLong())) {
      case 0l:
        id60x_1 = id60in_option0;
        break;
      case 1l:
        id60x_1 = id60in_option1;
        break;
      default:
        id60x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id60out_result[(getCycle()+1)%2] = (id60x_1);
  }
  { // Node ID: 807 (NodeFIFO)
    const HWFloat<11,53> &id807in_input = id60out_result[getCycle()%2];

    id807out_output[(getCycle()+9)%10] = id807in_input;
  }
  { // Node ID: 870 (NodeFIFO)
    const HWFloat<11,53> &id870in_input = id807out_output[getCycle()%10];

    id870out_output[(getCycle()+12)%13] = id870in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id460out_result;

  { // Node ID: 460 (NodeGt)
    const HWFloat<11,53> &id460in_a = id866out_output[getCycle()%2];
    const HWFloat<11,53> &id460in_b = id18out_value;

    id460out_result = (gt_float(id460in_a,id460in_b));
  }
  { // Node ID: 33 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id447out_result;

  { // Node ID: 447 (NodeGt)
    const HWFloat<11,53> &id447in_a = id866out_output[getCycle()%2];
    const HWFloat<11,53> &id447in_b = id33out_value;

    id447out_result = (gt_float(id447in_a,id447in_b));
  }
  { // Node ID: 933 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  HWFloat<11,53> id448out_result;

  { // Node ID: 448 (NodeDiv)
    const HWFloat<11,53> &id448in_a = id866out_output[getCycle()%2];
    const HWFloat<11,53> &id448in_b = id14out_value;

    id448out_result = (div_float(id448in_a,id448in_b));
  }
  HWFloat<11,53> id450out_result;

  { // Node ID: 450 (NodeSub)
    const HWFloat<11,53> &id450in_a = id933out_value;
    const HWFloat<11,53> &id450in_b = id448out_result;

    id450out_result = (sub_float(id450in_a,id450in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  HWFloat<11,53> id451out_result;

  { // Node ID: 451 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id451in_sel = id447out_result;
    const HWFloat<11,53> &id451in_option0 = id450out_result;
    const HWFloat<11,53> &id451in_option1 = id32out_value;

    HWFloat<11,53> id451x_1;

    switch((id451in_sel.getValueAsLong())) {
      case 0l:
        id451x_1 = id451in_option0;
        break;
      case 1l:
        id451x_1 = id451in_option1;
        break;
      default:
        id451x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id451out_result = (id451x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id452out_result;

  { // Node ID: 452 (NodeGt)
    const HWFloat<11,53> &id452in_a = id451out_result;
    const HWFloat<11,53> &id452in_b = id35out_value;

    id452out_result = (gt_float(id452in_a,id452in_b));
  }
  HWFloat<11,53> id453out_result;

  { // Node ID: 453 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id453in_sel = id452out_result;
    const HWFloat<11,53> &id453in_option0 = id451out_result;
    const HWFloat<11,53> &id453in_option1 = id35out_value;

    HWFloat<11,53> id453x_1;

    switch((id453in_sel.getValueAsLong())) {
      case 0l:
        id453x_1 = id453in_option0;
        break;
      case 1l:
        id453x_1 = id453in_option1;
        break;
      default:
        id453x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id453out_result = (id453x_1);
  }
  HWFloat<11,53> id463out_result;

  { // Node ID: 463 (NodeSub)
    const HWFloat<11,53> &id463in_a = id453out_result;
    const HWFloat<11,53> &id463in_b = id807out_output[getCycle()%10];

    id463out_result = (sub_float(id463in_a,id463in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  { // Node ID: 932 (NodeConstantRawBits)
  }
  { // Node ID: 931 (NodeConstantRawBits)
  }
  { // Node ID: 930 (NodeConstantRawBits)
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  HWFloat<11,53> id227out_result;

  { // Node ID: 227 (NodeSub)
    const HWFloat<11,53> &id227in_a = id56out_result[getCycle()%2];
    const HWFloat<11,53> &id227in_b = id25out_value;

    id227out_result = (sub_float(id227in_a,id227in_b));
  }
  HWFloat<11,53> id228out_result;

  { // Node ID: 228 (NodeMul)
    const HWFloat<11,53> &id228in_a = id22out_value;
    const HWFloat<11,53> &id228in_b = id227out_result;

    id228out_result = (mul_float(id228in_a,id228in_b));
  }
  { // Node ID: 736 (NodePO2FPMult)
    const HWFloat<11,53> &id736in_floatIn = id228out_result;

    id736out_floatOut[(getCycle()+1)%2] = (mul_float(id736in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id307out_result;

  { // Node ID: 307 (NodeSlice)
    const HWFloat<11,53> &id307in_a = id736out_floatOut[getCycle()%2];

    id307out_result = (slice<52,11>(id307in_a));
  }
  { // Node ID: 308 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id699out_result;

  { // Node ID: 699 (NodeEqInlined)
    const HWRawBits<11> &id699in_a = id307out_result;
    const HWRawBits<11> &id699in_b = id308out_value;

    id699out_result = (eq_bits(id699in_a,id699in_b));
  }
  HWRawBits<52> id306out_result;

  { // Node ID: 306 (NodeSlice)
    const HWFloat<11,53> &id306in_a = id736out_floatOut[getCycle()%2];

    id306out_result = (slice<0,52>(id306in_a));
  }
  { // Node ID: 929 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id700out_result;

  { // Node ID: 700 (NodeNeqInlined)
    const HWRawBits<52> &id700in_a = id306out_result;
    const HWRawBits<52> &id700in_b = id929out_value;

    id700out_result = (neq_bits(id700in_a,id700in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id312out_result;

  { // Node ID: 312 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id312in_a = id699out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id312in_b = id700out_result;

    HWOffsetFix<1,0,UNSIGNED> id312x_1;

    (id312x_1) = (and_fixed(id312in_a,id312in_b));
    id312out_result = (id312x_1);
  }
  { // Node ID: 817 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id817in_input = id312out_result;

    id817out_output[(getCycle()+8)%9] = id817in_input;
  }
  { // Node ID: 231 (NodeConstantRawBits)
  }
  HWFloat<11,53> id232out_output;
  HWOffsetFix<1,0,UNSIGNED> id232out_output_doubt;

  { // Node ID: 232 (NodeDoubtBitOp)
    const HWFloat<11,53> &id232in_input = id736out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id232in_doubt = id231out_value;

    id232out_output = id232in_input;
    id232out_output_doubt = id232in_doubt;
  }
  { // Node ID: 233 (NodeCast)
    const HWFloat<11,53> &id233in_i = id232out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_i_doubt = id232out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id233x_1;

    id233out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id233in_i,(&(id233x_1))));
    id233out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id233x_1),(c_hw_fix_4_0_uns_bits))),id233in_i_doubt));
  }
  { // Node ID: 236 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id235out_result;
  HWOffsetFix<1,0,UNSIGNED> id235out_result_doubt;

  { // Node ID: 235 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id235in_a = id233out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id235in_a_doubt = id233out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id235in_b = id236out_value;

    HWOffsetFix<1,0,UNSIGNED> id235x_1;

    id235out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id235in_a,id235in_b,(&(id235x_1))));
    id235out_result_doubt = (or_fixed((neq_fixed((id235x_1),(c_hw_fix_1_0_uns_bits_1))),id235in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id237out_o;
  HWOffsetFix<1,0,UNSIGNED> id237out_o_doubt;

  { // Node ID: 237 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id237in_i = id235out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id237in_i_doubt = id235out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id237x_1;

    id237out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id237in_i,(&(id237x_1))));
    id237out_o_doubt = (or_fixed((neq_fixed((id237x_1),(c_hw_fix_1_0_uns_bits_1))),id237in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id246out_output;

  { // Node ID: 246 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id246in_input = id237out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id246in_input_doubt = id237out_o_doubt;

    id246out_output = id246in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id247out_o;

  { // Node ID: 247 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id247in_i = id246out_output;

    id247out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id247in_i));
  }
  { // Node ID: 808 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id808in_input = id247out_o;

    id808out_output[(getCycle()+2)%3] = id808in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id274out_o;

  { // Node ID: 274 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id274in_i = id808out_output[getCycle()%3];

    id274out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id274in_i));
  }
  { // Node ID: 277 (NodeConstantRawBits)
  }
  { // Node ID: 728 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id250out_o;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id250in_i = id246out_output;

    id250out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id250in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id322out_output;

  { // Node ID: 322 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id322in_input = id250out_o;

    id322out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id322in_input))));
  }
  { // Node ID: 323 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id323in_addr = id322out_output;

    HWOffsetFix<38,-53,UNSIGNED> id323x_1;

    switch(((long)((id323in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id323x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id323x_1 = (id323sta_rom_store[(id323in_addr.getValueAsLong())]);
        break;
      default:
        id323x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id323out_dout[(getCycle()+2)%3] = (id323x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id249out_o;

  { // Node ID: 249 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id249in_i = id246out_output;

    id249out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id249in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id319out_output;

  { // Node ID: 319 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id319in_input = id249out_o;

    id319out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id319in_input))));
  }
  { // Node ID: 320 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id320in_addr = id319out_output;

    HWOffsetFix<48,-53,UNSIGNED> id320x_1;

    switch(((long)((id320in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id320x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id320x_1 = (id320sta_rom_store[(id320in_addr.getValueAsLong())]);
        break;
      default:
        id320x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id320out_dout[(getCycle()+2)%3] = (id320x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id248out_o;

  { // Node ID: 248 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id248in_i = id246out_output;

    id248out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id248in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id316out_output;

  { // Node ID: 316 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id316in_input = id248out_o;

    id316out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id316in_input))));
  }
  { // Node ID: 317 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id317in_addr = id316out_output;

    HWOffsetFix<53,-53,UNSIGNED> id317x_1;

    switch(((long)((id317in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id317x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id317x_1 = (id317sta_rom_store[(id317in_addr.getValueAsLong())]);
        break;
      default:
        id317x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id317out_dout[(getCycle()+2)%3] = (id317x_1);
  }
  { // Node ID: 254 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id251out_o;

  { // Node ID: 251 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id251in_i = id246out_output;

    id251out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id251in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id253out_result;

  { // Node ID: 253 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id253in_a = id254out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id253in_b = id251out_o;

    id253out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id253in_a,id253in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id255out_o;

  { // Node ID: 255 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id255in_i = id253out_result;

    id255out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id255in_i));
  }
  { // Node ID: 809 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id809in_input = id255out_o;

    id809out_output[(getCycle()+2)%3] = id809in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id256out_result;

  { // Node ID: 256 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id256in_a = id317out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id256in_b = id809out_output[getCycle()%3];

    id256out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id256in_a,id256in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id257out_result;

  { // Node ID: 257 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id257in_a = id809out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id257in_b = id317out_dout[getCycle()%3];

    id257out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id257in_a,id257in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id258out_result;

  { // Node ID: 258 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id258in_a = id256out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id258in_b = id257out_result;

    id258out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id258in_a,id258in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id259out_o;

  { // Node ID: 259 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id259in_i = id258out_result;

    id259out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id259in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id260out_result;

  { // Node ID: 260 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id260in_a = id320out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id260in_b = id259out_o;

    id260out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id260in_a,id260in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id261out_result;

  { // Node ID: 261 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id261in_a = id259out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id261in_b = id320out_dout[getCycle()%3];

    id261out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id261in_a,id261in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id262out_result;

  { // Node ID: 262 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id262in_a = id260out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id262in_b = id261out_result;

    id262out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id262in_a,id262in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id263out_o;

  { // Node ID: 263 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id263in_i = id262out_result;

    id263out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id263in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id264out_result;

  { // Node ID: 264 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id264in_a = id323out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id264in_b = id263out_o;

    id264out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id264in_a,id264in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id265out_result;

  { // Node ID: 265 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id265in_a = id263out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id265in_b = id323out_dout[getCycle()%3];

    id265out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id265in_a,id265in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id266out_result;

  { // Node ID: 266 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id266in_a = id264out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id266in_b = id265out_result;

    id266out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id266in_a,id266in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id267out_o;

  { // Node ID: 267 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id267in_i = id266out_result;

    id267out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id267in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id268out_o;

  { // Node ID: 268 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id268in_i = id267out_o;

    id268out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id268in_i));
  }
  { // Node ID: 928 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id701out_result;

  { // Node ID: 701 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id701in_a = id268out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id701in_b = id928out_value;

    id701out_result = (gte_fixed(id701in_a,id701in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id733out_result;

  { // Node ID: 733 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id733in_a = id274out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id733in_b = id277out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id733in_c = id728out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id733in_condb = id701out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id733x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id733x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id733x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id733x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id733x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id733x_1 = id733in_a;
        break;
      default:
        id733x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id733in_condb.getValueAsLong())) {
      case 0l:
        id733x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id733x_2 = id733in_b;
        break;
      default:
        id733x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id733x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id733x_3 = id733in_c;
        break;
      default:
        id733x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id733x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id733x_1),(id733x_2))),(id733x_3)));
    id733out_result = (id733x_4);
  }
  HWRawBits<1> id702out_result;

  { // Node ID: 702 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id702in_a = id733out_result;

    id702out_result = (slice<13,1>(id702in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id703out_output;

  { // Node ID: 703 (NodeReinterpret)
    const HWRawBits<1> &id703in_input = id702out_result;

    id703out_output = (cast_bits2fixed<1,0,UNSIGNED>(id703in_input));
  }
  { // Node ID: 927 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id239out_result;

  { // Node ID: 239 (NodeGt)
    const HWFloat<11,53> &id239in_a = id736out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id239in_b = id927out_value;

    id239out_result = (gt_float(id239in_a,id239in_b));
  }
  { // Node ID: 811 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id811in_input = id239out_result;

    id811out_output[(getCycle()+6)%7] = id811in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id240out_output;

  { // Node ID: 240 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id240in_input = id237out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id240in_input_doubt = id237out_o_doubt;

    id240out_output = id240in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id241out_result;

  { // Node ID: 241 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id241in_a = id811out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id241in_b = id240out_output;

    HWOffsetFix<1,0,UNSIGNED> id241x_1;

    (id241x_1) = (and_fixed(id241in_a,id241in_b));
    id241out_result = (id241x_1);
  }
  { // Node ID: 812 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id812in_input = id241out_result;

    id812out_output[(getCycle()+2)%3] = id812in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id283in_a = id812out_output[getCycle()%3];

    id283out_result = (not_fixed(id283in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id284out_result;

  { // Node ID: 284 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id284in_a = id703out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id284in_b = id283out_result;

    HWOffsetFix<1,0,UNSIGNED> id284x_1;

    (id284x_1) = (and_fixed(id284in_a,id284in_b));
    id284out_result = (id284x_1);
  }
  { // Node ID: 926 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id243out_result;

  { // Node ID: 243 (NodeLt)
    const HWFloat<11,53> &id243in_a = id736out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id243in_b = id926out_value;

    id243out_result = (lt_float(id243in_a,id243in_b));
  }
  { // Node ID: 813 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id813in_input = id243out_result;

    id813out_output[(getCycle()+6)%7] = id813in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id244out_output;

  { // Node ID: 244 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id244in_input = id237out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id244in_input_doubt = id237out_o_doubt;

    id244out_output = id244in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id245out_result;

  { // Node ID: 245 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id245in_a = id813out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id245in_b = id244out_output;

    HWOffsetFix<1,0,UNSIGNED> id245x_1;

    (id245x_1) = (and_fixed(id245in_a,id245in_b));
    id245out_result = (id245x_1);
  }
  { // Node ID: 814 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id814in_input = id245out_result;

    id814out_output[(getCycle()+2)%3] = id814in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id285in_a = id284out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id285in_b = id814out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id285x_1;

    (id285x_1) = (or_fixed(id285in_a,id285in_b));
    id285out_result = (id285x_1);
  }
  { // Node ID: 925 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id704out_result;

  { // Node ID: 704 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id704in_a = id733out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id704in_b = id925out_value;

    id704out_result = (gte_fixed(id704in_a,id704in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id292out_result;

  { // Node ID: 292 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id292in_a = id814out_output[getCycle()%3];

    id292out_result = (not_fixed(id292in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id293out_result;

  { // Node ID: 293 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id293in_a = id704out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id293in_b = id292out_result;

    HWOffsetFix<1,0,UNSIGNED> id293x_1;

    (id293x_1) = (and_fixed(id293in_a,id293in_b));
    id293out_result = (id293x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id294out_result;

  { // Node ID: 294 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id294in_a = id293out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id294in_b = id812out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id294x_1;

    (id294x_1) = (or_fixed(id294in_a,id294in_b));
    id294out_result = (id294x_1);
  }
  HWRawBits<2> id295out_result;

  { // Node ID: 295 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id295in_in0 = id285out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id295in_in1 = id294out_result;

    id295out_result = (cat(id295in_in0,id295in_in1));
  }
  { // Node ID: 287 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id286out_o;

  { // Node ID: 286 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id286in_i = id733out_result;

    id286out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id286in_i));
  }
  { // Node ID: 271 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id272out_result;

  { // Node ID: 272 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id272in_sel = id701out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id272in_option0 = id268out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id272in_option1 = id271out_value;

    HWOffsetFix<53,-52,UNSIGNED> id272x_1;

    switch((id272in_sel.getValueAsLong())) {
      case 0l:
        id272x_1 = id272in_option0;
        break;
      case 1l:
        id272x_1 = id272in_option1;
        break;
      default:
        id272x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id272out_result = (id272x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id273out_o;

  { // Node ID: 273 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id273in_i = id272out_result;

    id273out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id273in_i));
  }
  HWRawBits<64> id288out_result;

  { // Node ID: 288 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id288in_in0 = id287out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id288in_in1 = id286out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id288in_in2 = id273out_o;

    id288out_result = (cat((cat(id288in_in0,id288in_in1)),id288in_in2));
  }
  HWFloat<11,53> id289out_output;

  { // Node ID: 289 (NodeReinterpret)
    const HWRawBits<64> &id289in_input = id288out_result;

    id289out_output = (cast_bits2float<11,53>(id289in_input));
  }
  { // Node ID: 296 (NodeConstantRawBits)
  }
  { // Node ID: 297 (NodeConstantRawBits)
  }
  { // Node ID: 299 (NodeConstantRawBits)
  }
  HWRawBits<64> id705out_result;

  { // Node ID: 705 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id705in_in0 = id296out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id705in_in1 = id297out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id705in_in2 = id299out_value;

    id705out_result = (cat((cat(id705in_in0,id705in_in1)),id705in_in2));
  }
  HWFloat<11,53> id301out_output;

  { // Node ID: 301 (NodeReinterpret)
    const HWRawBits<64> &id301in_input = id705out_result;

    id301out_output = (cast_bits2float<11,53>(id301in_input));
  }
  { // Node ID: 686 (NodeConstantRawBits)
  }
  HWFloat<11,53> id304out_result;

  { // Node ID: 304 (NodeMux)
    const HWRawBits<2> &id304in_sel = id295out_result;
    const HWFloat<11,53> &id304in_option0 = id289out_output;
    const HWFloat<11,53> &id304in_option1 = id301out_output;
    const HWFloat<11,53> &id304in_option2 = id686out_value;
    const HWFloat<11,53> &id304in_option3 = id301out_output;

    HWFloat<11,53> id304x_1;

    switch((id304in_sel.getValueAsLong())) {
      case 0l:
        id304x_1 = id304in_option0;
        break;
      case 1l:
        id304x_1 = id304in_option1;
        break;
      case 2l:
        id304x_1 = id304in_option2;
        break;
      case 3l:
        id304x_1 = id304in_option3;
        break;
      default:
        id304x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id304out_result = (id304x_1);
  }
  { // Node ID: 924 (NodeConstantRawBits)
  }
  HWFloat<11,53> id314out_result;

  { // Node ID: 314 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id314in_sel = id817out_output[getCycle()%9];
    const HWFloat<11,53> &id314in_option0 = id304out_result;
    const HWFloat<11,53> &id314in_option1 = id924out_value;

    HWFloat<11,53> id314x_1;

    switch((id314in_sel.getValueAsLong())) {
      case 0l:
        id314x_1 = id314in_option0;
        break;
      case 1l:
        id314x_1 = id314in_option1;
        break;
      default:
        id314x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id314out_result = (id314x_1);
  }
  { // Node ID: 923 (NodeConstantRawBits)
  }
  HWFloat<11,53> id325out_result;

  { // Node ID: 325 (NodeAdd)
    const HWFloat<11,53> &id325in_a = id314out_result;
    const HWFloat<11,53> &id325in_b = id923out_value;

    id325out_result = (add_float(id325in_a,id325in_b));
  }
  HWFloat<11,53> id327out_result;

  { // Node ID: 327 (NodeDiv)
    const HWFloat<11,53> &id327in_a = id930out_value;
    const HWFloat<11,53> &id327in_b = id325out_result;

    id327out_result = (div_float(id327in_a,id327in_b));
  }
  HWFloat<11,53> id329out_result;

  { // Node ID: 329 (NodeSub)
    const HWFloat<11,53> &id329in_a = id931out_value;
    const HWFloat<11,53> &id329in_b = id327out_result;

    id329out_result = (sub_float(id329in_a,id329in_b));
  }
  HWFloat<11,53> id331out_result;

  { // Node ID: 331 (NodeAdd)
    const HWFloat<11,53> &id331in_a = id932out_value;
    const HWFloat<11,53> &id331in_b = id329out_result;

    id331out_result = (add_float(id331in_a,id331in_b));
  }
  HWFloat<11,53> id332out_result;

  { // Node ID: 332 (NodeMul)
    const HWFloat<11,53> &id332in_a = id30out_value;
    const HWFloat<11,53> &id332in_b = id331out_result;

    id332out_result = (mul_float(id332in_a,id332in_b));
  }
  HWFloat<11,53> id333out_result;

  { // Node ID: 333 (NodeAdd)
    const HWFloat<11,53> &id333in_a = id4out_value;
    const HWFloat<11,53> &id333in_b = id332out_result;

    id333out_result = (add_float(id333in_a,id333in_b));
  }
  HWFloat<11,53> id464out_result;

  { // Node ID: 464 (NodeDiv)
    const HWFloat<11,53> &id464in_a = id463out_result;
    const HWFloat<11,53> &id464in_b = id333out_result;

    id464out_result = (div_float(id464in_a,id464in_b));
  }
  HWFloat<11,53> id461out_result;

  { // Node ID: 461 (NodeNeg)
    const HWFloat<11,53> &id461in_a = id807out_output[getCycle()%10];

    id461out_result = (neg_float(id461in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<11,53> id462out_result;

  { // Node ID: 462 (NodeDiv)
    const HWFloat<11,53> &id462in_a = id461out_result;
    const HWFloat<11,53> &id462in_b = id3out_value;

    id462out_result = (div_float(id462in_a,id462in_b));
  }
  HWFloat<11,53> id465out_result;

  { // Node ID: 465 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id465in_sel = id460out_result;
    const HWFloat<11,53> &id465in_option0 = id464out_result;
    const HWFloat<11,53> &id465in_option1 = id462out_result;

    HWFloat<11,53> id465x_1;

    switch((id465in_sel.getValueAsLong())) {
      case 0l:
        id465x_1 = id465in_option0;
        break;
      case 1l:
        id465x_1 = id465in_option1;
        break;
      default:
        id465x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id465out_result = (id465x_1);
  }
  { // Node ID: 577 (NodeMul)
    const HWFloat<11,53> &id577in_a = id465out_result;
    const HWFloat<11,53> &id577in_b = id36out_dt;

    id577out_result[(getCycle()+12)%13] = (mul_float(id577in_a,id577in_b));
  }
  { // Node ID: 578 (NodeAdd)
    const HWFloat<11,53> &id578in_a = id870out_output[getCycle()%13];
    const HWFloat<11,53> &id578in_b = id577out_result[getCycle()%13];

    id578out_result[(getCycle()+14)%15] = (add_float(id578in_a,id578in_b));
  }
  { // Node ID: 800 (NodeFIFO)
    const HWFloat<11,53> &id800in_input = id578out_result[getCycle()%15];

    id800out_output[(getCycle()+1)%2] = id800in_input;
  }
  HWFloat<11,53> id596out_result;

  { // Node ID: 596 (NodeNeg)
    const HWFloat<11,53> &id596in_a = id800out_output[getCycle()%2];

    id596out_result = (neg_float(id596in_a));
  }
  HWOffsetFix<20,0,UNSIGNED> id626out_o;

  { // Node ID: 626 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id626in_i = id759out_output;

    id626out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id626in_i));
  }
  HWFloat<11,53> id762out_output;

  { // Node ID: 762 (NodeStreamOffset)
    const HWFloat<11,53> &id762in_input = id580out_result[getCycle()%15];

    id762out_output = id762in_input;
  }
  { // Node ID: 922 (NodeConstantRawBits)
  }
  { // Node ID: 628 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id628in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id628in_b = id922out_value;

    id628out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id628in_a,id628in_b));
  }
  { // Node ID: 706 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id706in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id706in_b = id628out_result[getCycle()%2];

    id706out_result[(getCycle()+1)%2] = (eq_fixed(id706in_a,id706in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id50out_o;

  { // Node ID: 50 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id50in_i = id778out_output[getCycle()%2];

    id50out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id50in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 670 (NodeRAM)
    const bool id670_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id670in_addrA = id626out_o;
    const HWFloat<11,53> &id670in_dina = id762out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id670in_wea = id706out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id670in_addrB = id50out_o;

    long id670x_1;
    long id670x_2;
    HWFloat<11,53> id670x_3;

    (id670x_1) = (id670in_addrA.getValueAsLong());
    (id670x_2) = (id670in_addrB.getValueAsLong());
    switch(((long)((id670x_2)<(1000000l)))) {
      case 0l:
        id670x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id670x_3 = (id670sta_ram_store[(id670x_2)]);
        break;
      default:
        id670x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id670out_doutb[(getCycle()+2)%3] = (id670x_3);
    if(((id670in_wea.getValueAsBool())&id670_inputvalid)) {
      if(((id670x_1)<(1000000l))) {
        (id670sta_ram_store[(id670x_1)]) = id670in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_4("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 670) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 61 (NodeConstantRawBits)
  }
  { // Node ID: 62 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id62in_sel = id688out_result[getCycle()%2];
    const HWFloat<11,53> &id62in_option0 = id670out_doutb[getCycle()%3];
    const HWFloat<11,53> &id62in_option1 = id61out_value;

    HWFloat<11,53> id62x_1;

    switch((id62in_sel.getValueAsLong())) {
      case 0l:
        id62x_1 = id62in_option0;
        break;
      case 1l:
        id62x_1 = id62in_option1;
        break;
      default:
        id62x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id62out_result[(getCycle()+1)%2] = (id62x_1);
  }
  { // Node ID: 832 (NodeFIFO)
    const HWFloat<11,53> &id832in_input = id62out_result[getCycle()%2];

    id832out_output[(getCycle()+16)%17] = id832in_input;
  }
  { // Node ID: 871 (NodeFIFO)
    const HWFloat<11,53> &id871in_input = id832out_output[getCycle()%17];

    id871out_output[(getCycle()+12)%13] = id871in_input;
  }
  { // Node ID: 921 (NodeConstantRawBits)
  }
  { // Node ID: 920 (NodeConstantRawBits)
  }
  { // Node ID: 919 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  HWFloat<11,53> id466out_result;

  { // Node ID: 466 (NodeSub)
    const HWFloat<11,53> &id466in_a = id56out_result[getCycle()%2];
    const HWFloat<11,53> &id466in_b = id26out_value;

    id466out_result = (sub_float(id466in_a,id466in_b));
  }
  HWFloat<11,53> id467out_result;

  { // Node ID: 467 (NodeMul)
    const HWFloat<11,53> &id467in_a = id23out_value;
    const HWFloat<11,53> &id467in_b = id466out_result;

    id467out_result = (mul_float(id467in_a,id467in_b));
  }
  { // Node ID: 737 (NodePO2FPMult)
    const HWFloat<11,53> &id737in_floatIn = id467out_result;

    id737out_floatOut[(getCycle()+1)%2] = (mul_float(id737in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id546out_result;

  { // Node ID: 546 (NodeSlice)
    const HWFloat<11,53> &id546in_a = id737out_floatOut[getCycle()%2];

    id546out_result = (slice<52,11>(id546in_a));
  }
  { // Node ID: 547 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id707out_result;

  { // Node ID: 707 (NodeEqInlined)
    const HWRawBits<11> &id707in_a = id546out_result;
    const HWRawBits<11> &id707in_b = id547out_value;

    id707out_result = (eq_bits(id707in_a,id707in_b));
  }
  HWRawBits<52> id545out_result;

  { // Node ID: 545 (NodeSlice)
    const HWFloat<11,53> &id545in_a = id737out_floatOut[getCycle()%2];

    id545out_result = (slice<0,52>(id545in_a));
  }
  { // Node ID: 918 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id708out_result;

  { // Node ID: 708 (NodeNeqInlined)
    const HWRawBits<52> &id708in_a = id545out_result;
    const HWRawBits<52> &id708in_b = id918out_value;

    id708out_result = (neq_bits(id708in_a,id708in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id551out_result;

  { // Node ID: 551 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id551in_a = id707out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id551in_b = id708out_result;

    HWOffsetFix<1,0,UNSIGNED> id551x_1;

    (id551x_1) = (and_fixed(id551in_a,id551in_b));
    id551out_result = (id551x_1);
  }
  { // Node ID: 831 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id831in_input = id551out_result;

    id831out_output[(getCycle()+8)%9] = id831in_input;
  }
  { // Node ID: 470 (NodeConstantRawBits)
  }
  HWFloat<11,53> id471out_output;
  HWOffsetFix<1,0,UNSIGNED> id471out_output_doubt;

  { // Node ID: 471 (NodeDoubtBitOp)
    const HWFloat<11,53> &id471in_input = id737out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id471in_doubt = id470out_value;

    id471out_output = id471in_input;
    id471out_output_doubt = id471in_doubt;
  }
  { // Node ID: 472 (NodeCast)
    const HWFloat<11,53> &id472in_i = id471out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id472in_i_doubt = id471out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id472x_1;

    id472out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id472in_i,(&(id472x_1))));
    id472out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id472x_1),(c_hw_fix_4_0_uns_bits))),id472in_i_doubt));
  }
  { // Node ID: 475 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id474out_result;
  HWOffsetFix<1,0,UNSIGNED> id474out_result_doubt;

  { // Node ID: 474 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id474in_a = id472out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id474in_a_doubt = id472out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id474in_b = id475out_value;

    HWOffsetFix<1,0,UNSIGNED> id474x_1;

    id474out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id474in_a,id474in_b,(&(id474x_1))));
    id474out_result_doubt = (or_fixed((neq_fixed((id474x_1),(c_hw_fix_1_0_uns_bits_1))),id474in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id476out_o;
  HWOffsetFix<1,0,UNSIGNED> id476out_o_doubt;

  { // Node ID: 476 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id476in_i = id474out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id476in_i_doubt = id474out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id476x_1;

    id476out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id476in_i,(&(id476x_1))));
    id476out_o_doubt = (or_fixed((neq_fixed((id476x_1),(c_hw_fix_1_0_uns_bits_1))),id476in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id485out_output;

  { // Node ID: 485 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id485in_input = id476out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id485in_input_doubt = id476out_o_doubt;

    id485out_output = id485in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id486out_o;

  { // Node ID: 486 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id486in_i = id485out_output;

    id486out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id486in_i));
  }
  { // Node ID: 822 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id822in_input = id486out_o;

    id822out_output[(getCycle()+2)%3] = id822in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id513out_o;

  { // Node ID: 513 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id513in_i = id822out_output[getCycle()%3];

    id513out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id513in_i));
  }
  { // Node ID: 516 (NodeConstantRawBits)
  }
  { // Node ID: 730 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id489out_o;

  { // Node ID: 489 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id489in_i = id485out_output;

    id489out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id489in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id561out_output;

  { // Node ID: 561 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id561in_input = id489out_o;

    id561out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id561in_input))));
  }
  { // Node ID: 562 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id562in_addr = id561out_output;

    HWOffsetFix<38,-53,UNSIGNED> id562x_1;

    switch(((long)((id562in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id562x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id562x_1 = (id562sta_rom_store[(id562in_addr.getValueAsLong())]);
        break;
      default:
        id562x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id562out_dout[(getCycle()+2)%3] = (id562x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id488out_o;

  { // Node ID: 488 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id488in_i = id485out_output;

    id488out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id488in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id558out_output;

  { // Node ID: 558 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id558in_input = id488out_o;

    id558out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id558in_input))));
  }
  { // Node ID: 559 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id559in_addr = id558out_output;

    HWOffsetFix<48,-53,UNSIGNED> id559x_1;

    switch(((long)((id559in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id559x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id559x_1 = (id559sta_rom_store[(id559in_addr.getValueAsLong())]);
        break;
      default:
        id559x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id559out_dout[(getCycle()+2)%3] = (id559x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id487out_o;

  { // Node ID: 487 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id487in_i = id485out_output;

    id487out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id487in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id555out_output;

  { // Node ID: 555 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id555in_input = id487out_o;

    id555out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id555in_input))));
  }
  { // Node ID: 556 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id556in_addr = id555out_output;

    HWOffsetFix<53,-53,UNSIGNED> id556x_1;

    switch(((long)((id556in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id556x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id556x_1 = (id556sta_rom_store[(id556in_addr.getValueAsLong())]);
        break;
      default:
        id556x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id556out_dout[(getCycle()+2)%3] = (id556x_1);
  }
  { // Node ID: 493 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id490out_o;

  { // Node ID: 490 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id490in_i = id485out_output;

    id490out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id490in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id492out_result;

  { // Node ID: 492 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id492in_a = id493out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id492in_b = id490out_o;

    id492out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id492in_a,id492in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id494out_o;

  { // Node ID: 494 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id494in_i = id492out_result;

    id494out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id494in_i));
  }
  { // Node ID: 823 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id823in_input = id494out_o;

    id823out_output[(getCycle()+2)%3] = id823in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id495out_result;

  { // Node ID: 495 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id495in_a = id556out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id495in_b = id823out_output[getCycle()%3];

    id495out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id495in_a,id495in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id496out_result;

  { // Node ID: 496 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id496in_a = id823out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id496in_b = id556out_dout[getCycle()%3];

    id496out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id496in_a,id496in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id497out_result;

  { // Node ID: 497 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id497in_a = id495out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id497in_b = id496out_result;

    id497out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id497in_a,id497in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id498out_o;

  { // Node ID: 498 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id498in_i = id497out_result;

    id498out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id498in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id499out_result;

  { // Node ID: 499 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id499in_a = id559out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id499in_b = id498out_o;

    id499out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id499in_a,id499in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id500out_result;

  { // Node ID: 500 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id500in_a = id498out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id500in_b = id559out_dout[getCycle()%3];

    id500out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id500in_a,id500in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id501out_result;

  { // Node ID: 501 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id501in_a = id499out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id501in_b = id500out_result;

    id501out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id501in_a,id501in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id502out_o;

  { // Node ID: 502 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id502in_i = id501out_result;

    id502out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id502in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id503out_result;

  { // Node ID: 503 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id503in_a = id562out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id503in_b = id502out_o;

    id503out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id503in_a,id503in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id504out_result;

  { // Node ID: 504 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id504in_a = id502out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id504in_b = id562out_dout[getCycle()%3];

    id504out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id504in_a,id504in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id505out_result;

  { // Node ID: 505 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id505in_a = id503out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id505in_b = id504out_result;

    id505out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id505in_a,id505in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id506out_o;

  { // Node ID: 506 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id506in_i = id505out_result;

    id506out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id506in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id507out_o;

  { // Node ID: 507 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id507in_i = id506out_o;

    id507out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id507in_i));
  }
  { // Node ID: 917 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id709out_result;

  { // Node ID: 709 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id709in_a = id507out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id709in_b = id917out_value;

    id709out_result = (gte_fixed(id709in_a,id709in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id732out_result;

  { // Node ID: 732 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id732in_a = id513out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id732in_b = id516out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id732in_c = id730out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id732in_condb = id709out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id732x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id732x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id732x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id732x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id732x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id732x_1 = id732in_a;
        break;
      default:
        id732x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id732in_condb.getValueAsLong())) {
      case 0l:
        id732x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id732x_2 = id732in_b;
        break;
      default:
        id732x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id732x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id732x_3 = id732in_c;
        break;
      default:
        id732x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id732x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id732x_1),(id732x_2))),(id732x_3)));
    id732out_result = (id732x_4);
  }
  HWRawBits<1> id710out_result;

  { // Node ID: 710 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id710in_a = id732out_result;

    id710out_result = (slice<13,1>(id710in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id711out_output;

  { // Node ID: 711 (NodeReinterpret)
    const HWRawBits<1> &id711in_input = id710out_result;

    id711out_output = (cast_bits2fixed<1,0,UNSIGNED>(id711in_input));
  }
  { // Node ID: 916 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id478out_result;

  { // Node ID: 478 (NodeGt)
    const HWFloat<11,53> &id478in_a = id737out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id478in_b = id916out_value;

    id478out_result = (gt_float(id478in_a,id478in_b));
  }
  { // Node ID: 825 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id825in_input = id478out_result;

    id825out_output[(getCycle()+6)%7] = id825in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id479out_output;

  { // Node ID: 479 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id479in_input = id476out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id479in_input_doubt = id476out_o_doubt;

    id479out_output = id479in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id480out_result;

  { // Node ID: 480 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id480in_a = id825out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id480in_b = id479out_output;

    HWOffsetFix<1,0,UNSIGNED> id480x_1;

    (id480x_1) = (and_fixed(id480in_a,id480in_b));
    id480out_result = (id480x_1);
  }
  { // Node ID: 826 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id826in_input = id480out_result;

    id826out_output[(getCycle()+2)%3] = id826in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id522out_result;

  { // Node ID: 522 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id522in_a = id826out_output[getCycle()%3];

    id522out_result = (not_fixed(id522in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id523out_result;

  { // Node ID: 523 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id523in_a = id711out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id523in_b = id522out_result;

    HWOffsetFix<1,0,UNSIGNED> id523x_1;

    (id523x_1) = (and_fixed(id523in_a,id523in_b));
    id523out_result = (id523x_1);
  }
  { // Node ID: 915 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id482out_result;

  { // Node ID: 482 (NodeLt)
    const HWFloat<11,53> &id482in_a = id737out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id482in_b = id915out_value;

    id482out_result = (lt_float(id482in_a,id482in_b));
  }
  { // Node ID: 827 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id827in_input = id482out_result;

    id827out_output[(getCycle()+6)%7] = id827in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id483out_output;

  { // Node ID: 483 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id483in_input = id476out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id483in_input_doubt = id476out_o_doubt;

    id483out_output = id483in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id484out_result;

  { // Node ID: 484 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id484in_a = id827out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id484in_b = id483out_output;

    HWOffsetFix<1,0,UNSIGNED> id484x_1;

    (id484x_1) = (and_fixed(id484in_a,id484in_b));
    id484out_result = (id484x_1);
  }
  { // Node ID: 828 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id828in_input = id484out_result;

    id828out_output[(getCycle()+2)%3] = id828in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id524out_result;

  { // Node ID: 524 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id524in_a = id523out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id524in_b = id828out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id524x_1;

    (id524x_1) = (or_fixed(id524in_a,id524in_b));
    id524out_result = (id524x_1);
  }
  { // Node ID: 914 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id712out_result;

  { // Node ID: 712 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id712in_a = id732out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id712in_b = id914out_value;

    id712out_result = (gte_fixed(id712in_a,id712in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id531out_result;

  { // Node ID: 531 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id531in_a = id828out_output[getCycle()%3];

    id531out_result = (not_fixed(id531in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id532out_result;

  { // Node ID: 532 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id532in_a = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id532in_b = id531out_result;

    HWOffsetFix<1,0,UNSIGNED> id532x_1;

    (id532x_1) = (and_fixed(id532in_a,id532in_b));
    id532out_result = (id532x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id533out_result;

  { // Node ID: 533 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id533in_a = id532out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id533in_b = id826out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id533x_1;

    (id533x_1) = (or_fixed(id533in_a,id533in_b));
    id533out_result = (id533x_1);
  }
  HWRawBits<2> id534out_result;

  { // Node ID: 534 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id534in_in0 = id524out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id534in_in1 = id533out_result;

    id534out_result = (cat(id534in_in0,id534in_in1));
  }
  { // Node ID: 526 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id525out_o;

  { // Node ID: 525 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id525in_i = id732out_result;

    id525out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id525in_i));
  }
  { // Node ID: 510 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id511out_result;

  { // Node ID: 511 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id511in_sel = id709out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id511in_option0 = id507out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id511in_option1 = id510out_value;

    HWOffsetFix<53,-52,UNSIGNED> id511x_1;

    switch((id511in_sel.getValueAsLong())) {
      case 0l:
        id511x_1 = id511in_option0;
        break;
      case 1l:
        id511x_1 = id511in_option1;
        break;
      default:
        id511x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id511out_result = (id511x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id512out_o;

  { // Node ID: 512 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id512in_i = id511out_result;

    id512out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id512in_i));
  }
  HWRawBits<64> id527out_result;

  { // Node ID: 527 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id527in_in0 = id526out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id527in_in1 = id525out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id527in_in2 = id512out_o;

    id527out_result = (cat((cat(id527in_in0,id527in_in1)),id527in_in2));
  }
  HWFloat<11,53> id528out_output;

  { // Node ID: 528 (NodeReinterpret)
    const HWRawBits<64> &id528in_input = id527out_result;

    id528out_output = (cast_bits2float<11,53>(id528in_input));
  }
  { // Node ID: 535 (NodeConstantRawBits)
  }
  { // Node ID: 536 (NodeConstantRawBits)
  }
  { // Node ID: 538 (NodeConstantRawBits)
  }
  HWRawBits<64> id713out_result;

  { // Node ID: 713 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id713in_in0 = id535out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id713in_in1 = id536out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id713in_in2 = id538out_value;

    id713out_result = (cat((cat(id713in_in0,id713in_in1)),id713in_in2));
  }
  HWFloat<11,53> id540out_output;

  { // Node ID: 540 (NodeReinterpret)
    const HWRawBits<64> &id540in_input = id713out_result;

    id540out_output = (cast_bits2float<11,53>(id540in_input));
  }
  { // Node ID: 687 (NodeConstantRawBits)
  }
  HWFloat<11,53> id543out_result;

  { // Node ID: 543 (NodeMux)
    const HWRawBits<2> &id543in_sel = id534out_result;
    const HWFloat<11,53> &id543in_option0 = id528out_output;
    const HWFloat<11,53> &id543in_option1 = id540out_output;
    const HWFloat<11,53> &id543in_option2 = id687out_value;
    const HWFloat<11,53> &id543in_option3 = id540out_output;

    HWFloat<11,53> id543x_1;

    switch((id543in_sel.getValueAsLong())) {
      case 0l:
        id543x_1 = id543in_option0;
        break;
      case 1l:
        id543x_1 = id543in_option1;
        break;
      case 2l:
        id543x_1 = id543in_option2;
        break;
      case 3l:
        id543x_1 = id543in_option3;
        break;
      default:
        id543x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id543out_result = (id543x_1);
  }
  { // Node ID: 913 (NodeConstantRawBits)
  }
  HWFloat<11,53> id553out_result;

  { // Node ID: 553 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id553in_sel = id831out_output[getCycle()%9];
    const HWFloat<11,53> &id553in_option0 = id543out_result;
    const HWFloat<11,53> &id553in_option1 = id913out_value;

    HWFloat<11,53> id553x_1;

    switch((id553in_sel.getValueAsLong())) {
      case 0l:
        id553x_1 = id553in_option0;
        break;
      case 1l:
        id553x_1 = id553in_option1;
        break;
      default:
        id553x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id553out_result = (id553x_1);
  }
  { // Node ID: 912 (NodeConstantRawBits)
  }
  HWFloat<11,53> id564out_result;

  { // Node ID: 564 (NodeAdd)
    const HWFloat<11,53> &id564in_a = id553out_result;
    const HWFloat<11,53> &id564in_b = id912out_value;

    id564out_result = (add_float(id564in_a,id564in_b));
  }
  HWFloat<11,53> id566out_result;

  { // Node ID: 566 (NodeDiv)
    const HWFloat<11,53> &id566in_a = id919out_value;
    const HWFloat<11,53> &id566in_b = id564out_result;

    id566out_result = (div_float(id566in_a,id566in_b));
  }
  HWFloat<11,53> id568out_result;

  { // Node ID: 568 (NodeSub)
    const HWFloat<11,53> &id568in_a = id920out_value;
    const HWFloat<11,53> &id568in_b = id566out_result;

    id568out_result = (sub_float(id568in_a,id568in_b));
  }
  HWFloat<11,53> id570out_result;

  { // Node ID: 570 (NodeAdd)
    const HWFloat<11,53> &id570in_a = id921out_value;
    const HWFloat<11,53> &id570in_b = id568out_result;

    id570out_result = (add_float(id570in_a,id570in_b));
  }
  { // Node ID: 738 (NodePO2FPMult)
    const HWFloat<11,53> &id738in_floatIn = id570out_result;

    id738out_floatOut[(getCycle()+1)%2] = (mul_float(id738in_floatIn,(c_hw_flt_11_53_0_5val)));
  }
  HWFloat<11,53> id573out_result;

  { // Node ID: 573 (NodeSub)
    const HWFloat<11,53> &id573in_a = id738out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id573in_b = id832out_output[getCycle()%17];

    id573out_result = (sub_float(id573in_a,id573in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id441out_result;

  { // Node ID: 441 (NodeGt)
    const HWFloat<11,53> &id441in_a = id867out_output[getCycle()%2];
    const HWFloat<11,53> &id441in_b = id18out_value;

    id441out_result = (gt_float(id441in_a,id441in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  HWFloat<11,53> id442out_result;

  { // Node ID: 442 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id442in_sel = id441out_result;
    const HWFloat<11,53> &id442in_option0 = id6out_value;
    const HWFloat<11,53> &id442in_option1 = id7out_value;

    HWFloat<11,53> id442x_1;

    switch((id442in_sel.getValueAsLong())) {
      case 0l:
        id442x_1 = id442in_option0;
        break;
      case 1l:
        id442x_1 = id442in_option1;
        break;
      default:
        id442x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id442out_result = (id442x_1);
  }
  HWFloat<11,53> id574out_result;

  { // Node ID: 574 (NodeDiv)
    const HWFloat<11,53> &id574in_a = id573out_result;
    const HWFloat<11,53> &id574in_b = id442out_result;

    id574out_result = (div_float(id574in_a,id574in_b));
  }
  { // Node ID: 579 (NodeMul)
    const HWFloat<11,53> &id579in_a = id574out_result;
    const HWFloat<11,53> &id579in_b = id36out_dt;

    id579out_result[(getCycle()+12)%13] = (mul_float(id579in_a,id579in_b));
  }
  { // Node ID: 580 (NodeAdd)
    const HWFloat<11,53> &id580in_a = id871out_output[getCycle()%13];
    const HWFloat<11,53> &id580in_b = id579out_result[getCycle()%13];

    id580out_result[(getCycle()+14)%15] = (add_float(id580in_a,id580in_b));
  }
  HWFloat<11,53> id597out_result;

  { // Node ID: 597 (NodeMul)
    const HWFloat<11,53> &id597in_a = id596out_result;
    const HWFloat<11,53> &id597in_b = id580out_result[getCycle()%15];

    id597out_result = (mul_float(id597in_a,id597in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  HWFloat<11,53> id598out_result;

  { // Node ID: 598 (NodeDiv)
    const HWFloat<11,53> &id598in_a = id597out_result;
    const HWFloat<11,53> &id598in_b = id13out_value;

    id598out_result = (div_float(id598in_a,id598in_b));
  }
  HWFloat<11,53> id600out_result;

  { // Node ID: 600 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id600in_sel = id595out_result;
    const HWFloat<11,53> &id600in_option0 = id599out_value;
    const HWFloat<11,53> &id600in_option1 = id598out_result;

    HWFloat<11,53> id600x_1;

    switch((id600in_sel.getValueAsLong())) {
      case 0l:
        id600x_1 = id600in_option0;
        break;
      case 1l:
        id600x_1 = id600in_option1;
        break;
      default:
        id600x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id600out_result = (id600x_1);
  }
  HWFloat<11,53> id602out_result;

  { // Node ID: 602 (NodeAdd)
    const HWFloat<11,53> &id602in_a = id601out_result;
    const HWFloat<11,53> &id602in_b = id600out_result;

    id602out_result = (add_float(id602in_a,id602in_b));
  }
  { // Node ID: 911 (NodeConstantRawBits)
  }
  { // Node ID: 714 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id714in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id714in_b = id911out_value;

    id714out_result[(getCycle()+1)%2] = (eq_fixed(id714in_a,id714in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id630out_o;

  { // Node ID: 630 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id630in_i = id759out_output;

    id630out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id630in_i));
  }
  HWFloat<11,53> id763out_output;

  { // Node ID: 763 (NodeStreamOffset)
    const HWFloat<11,53> &id763in_input = id71out_result[getCycle()%15];

    id763out_output = id763in_input;
  }
  { // Node ID: 836 (NodeFIFO)
    const HWFloat<11,53> &id836in_input = id763out_output;

    id836out_output[(getCycle()+4)%5] = id836in_input;
  }
  { // Node ID: 910 (NodeConstantRawBits)
  }
  { // Node ID: 632 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id632in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id632in_b = id910out_value;

    id632out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id632in_a,id632in_b));
  }
  { // Node ID: 715 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id715in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id715in_b = id632out_result[getCycle()%2];

    id715out_result[(getCycle()+1)%2] = (eq_fixed(id715in_a,id715in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id51out_o;

  { // Node ID: 51 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id51in_i = id778out_output[getCycle()%2];

    id51out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id51in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 671 (NodeRAM)
    const bool id671_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id671in_addrA = id630out_o;
    const HWFloat<11,53> &id671in_dina = id836out_output[getCycle()%5];
    const HWOffsetFix<1,0,UNSIGNED> &id671in_wea = id715out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id671in_addrB = id51out_o;

    long id671x_1;
    long id671x_2;
    HWFloat<11,53> id671x_3;

    (id671x_1) = (id671in_addrA.getValueAsLong());
    (id671x_2) = (id671in_addrB.getValueAsLong());
    switch(((long)((id671x_2)<(1000000l)))) {
      case 0l:
        id671x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id671x_3 = (id671sta_ram_store[(id671x_2)]);
        break;
      default:
        id671x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id671out_doutb[(getCycle()+2)%3] = (id671x_3);
    if(((id671in_wea.getValueAsBool())&id671_inputvalid)) {
      if(((id671x_1)<(1000000l))) {
        (id671sta_ram_store[(id671x_1)]) = id671in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_5("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 671) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 65 (NodeConstantRawBits)
  }
  { // Node ID: 66 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id66in_sel = id714out_result[getCycle()%2];
    const HWFloat<11,53> &id66in_option0 = id671out_doutb[getCycle()%3];
    const HWFloat<11,53> &id66in_option1 = id65out_value;

    HWFloat<11,53> id66x_1;

    switch((id66in_sel.getValueAsLong())) {
      case 0l:
        id66x_1 = id66in_option0;
        break;
      case 1l:
        id66x_1 = id66in_option1;
        break;
      default:
        id66x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id66out_result[(getCycle()+1)%2] = (id66x_1);
  }
  { // Node ID: 837 (NodeFIFO)
    const HWFloat<11,53> &id837in_input = id66out_result[getCycle()%2];

    id837out_output[(getCycle()+24)%25] = id837in_input;
  }
  { // Node ID: 71 (NodeAdd)
    const HWFloat<11,53> &id71in_a = id837out_output[getCycle()%25];
    const HWFloat<11,53> &id71in_b = id36out_dt;

    id71out_result[(getCycle()+14)%15] = (add_float(id71in_a,id71in_b));
  }
  { // Node ID: 909 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id81out_result;

  { // Node ID: 81 (NodeEq)
    const HWFloat<11,53> &id81in_a = id71out_result[getCycle()%15];
    const HWFloat<11,53> &id81in_b = id909out_value;

    id81out_result = (eq_float(id81in_a,id81in_b));
  }
  { // Node ID: 83 (NodeConstantRawBits)
  }
  { // Node ID: 82 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id84out_result;

  { // Node ID: 84 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id84in_sel = id81out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id84in_option0 = id83out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id84in_option1 = id82out_value;

    HWOffsetFix<1,0,UNSIGNED> id84x_1;

    switch((id84in_sel.getValueAsLong())) {
      case 0l:
        id84x_1 = id84in_option0;
        break;
      case 1l:
        id84x_1 = id84in_option1;
        break;
      default:
        id84x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id84out_result = (id84x_1);
  }
  { // Node ID: 908 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeLt)
    const HWFloat<11,53> &id76in_a = id71out_result[getCycle()%15];
    const HWFloat<11,53> &id76in_b = id908out_value;

    id76out_result = (lt_float(id76in_a,id76in_b));
  }
  { // Node ID: 78 (NodeConstantRawBits)
  }
  { // Node ID: 77 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id79out_result;

  { // Node ID: 79 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id79in_sel = id76out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_option0 = id78out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_option1 = id77out_value;

    HWOffsetFix<1,0,UNSIGNED> id79x_1;

    switch((id79in_sel.getValueAsLong())) {
      case 0l:
        id79x_1 = id79in_option0;
        break;
      case 1l:
        id79x_1 = id79in_option1;
        break;
      default:
        id79x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id79out_result = (id79x_1);
  }
  { // Node ID: 86 (NodeConstantRawBits)
  }
  { // Node ID: 85 (NodeConstantRawBits)
  }
  HWFloat<11,53> id87out_result;

  { // Node ID: 87 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id87in_sel = id79out_result;
    const HWFloat<11,53> &id87in_option0 = id86out_value;
    const HWFloat<11,53> &id87in_option1 = id85out_value;

    HWFloat<11,53> id87x_1;

    switch((id87in_sel.getValueAsLong())) {
      case 0l:
        id87x_1 = id87in_option0;
        break;
      case 1l:
        id87x_1 = id87in_option1;
        break;
      default:
        id87x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id87out_result = (id87x_1);
  }
  { // Node ID: 88 (NodeConstantRawBits)
  }
  HWFloat<11,53> id89out_result;

  { // Node ID: 89 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id89in_sel = id84out_result;
    const HWFloat<11,53> &id89in_option0 = id87out_result;
    const HWFloat<11,53> &id89in_option1 = id88out_value;

    HWFloat<11,53> id89x_1;

    switch((id89in_sel.getValueAsLong())) {
      case 0l:
        id89x_1 = id89in_option0;
        break;
      case 1l:
        id89x_1 = id89in_option1;
        break;
      default:
        id89x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id89out_result = (id89x_1);
  }
  { // Node ID: 907 (NodeConstantRawBits)
  }
  { // Node ID: 906 (NodeConstantRawBits)
  }
  { // Node ID: 716 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id716in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id716in_b = id906out_value;

    id716out_result[(getCycle()+1)%2] = (eq_fixed(id716in_a,id716in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id634out_o;

  { // Node ID: 634 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id634in_i = id759out_output;

    id634out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id634in_i));
  }
  HWFloat<11,53> id764out_output;

  { // Node ID: 764 (NodeStreamOffset)
    const HWFloat<11,53> &id764in_input = id838out_output[getCycle()%25];

    id764out_output = id764in_input;
  }
  { // Node ID: 840 (NodeFIFO)
    const HWFloat<11,53> &id840in_input = id764out_output;

    id840out_output[(getCycle()+4)%5] = id840in_input;
  }
  { // Node ID: 905 (NodeConstantRawBits)
  }
  { // Node ID: 636 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id636in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id636in_b = id905out_value;

    id636out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id636in_a,id636in_b));
  }
  { // Node ID: 717 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id717in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id717in_b = id636out_result[getCycle()%2];

    id717out_result[(getCycle()+1)%2] = (eq_fixed(id717in_a,id717in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id52out_o;

  { // Node ID: 52 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id52in_i = id778out_output[getCycle()%2];

    id52out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id52in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 672 (NodeRAM)
    const bool id672_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id672in_addrA = id634out_o;
    const HWFloat<11,53> &id672in_dina = id840out_output[getCycle()%5];
    const HWOffsetFix<1,0,UNSIGNED> &id672in_wea = id717out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id672in_addrB = id52out_o;

    long id672x_1;
    long id672x_2;
    HWFloat<11,53> id672x_3;

    (id672x_1) = (id672in_addrA.getValueAsLong());
    (id672x_2) = (id672in_addrB.getValueAsLong());
    switch(((long)((id672x_2)<(1000000l)))) {
      case 0l:
        id672x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id672x_3 = (id672sta_ram_store[(id672x_2)]);
        break;
      default:
        id672x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id672out_doutb[(getCycle()+2)%3] = (id672x_3);
    if(((id672in_wea.getValueAsBool())&id672_inputvalid)) {
      if(((id672x_1)<(1000000l))) {
        (id672sta_ram_store[(id672x_1)]) = id672in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_6("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 672) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 69 (NodeConstantRawBits)
  }
  { // Node ID: 70 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id70in_sel = id716out_result[getCycle()%2];
    const HWFloat<11,53> &id70in_option0 = id672out_doutb[getCycle()%3];
    const HWFloat<11,53> &id70in_option1 = id69out_value;

    HWFloat<11,53> id70x_1;

    switch((id70in_sel.getValueAsLong())) {
      case 0l:
        id70x_1 = id70in_option0;
        break;
      case 1l:
        id70x_1 = id70in_option1;
        break;
      default:
        id70x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id70out_result[(getCycle()+1)%2] = (id70x_1);
  }
  { // Node ID: 72 (NodeAdd)
    const HWFloat<11,53> &id72in_a = id70out_result[getCycle()%2];
    const HWFloat<11,53> &id72in_b = id36out_dt;

    id72out_result[(getCycle()+14)%15] = (add_float(id72in_a,id72in_b));
  }
  { // Node ID: 838 (NodeFIFO)
    const HWFloat<11,53> &id838in_input = id72out_result[getCycle()%15];

    id838out_output[(getCycle()+24)%25] = id838in_input;
  }
  { // Node ID: 904 (NodeConstantRawBits)
  }
  HWFloat<11,53> id91out_result;

  { // Node ID: 91 (NodeSub)
    const HWFloat<11,53> &id91in_a = id838out_output[getCycle()%25];
    const HWFloat<11,53> &id91in_b = id904out_value;

    id91out_result = (sub_float(id91in_a,id91in_b));
  }
  { // Node ID: 903 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id98out_result;

  { // Node ID: 98 (NodeEq)
    const HWFloat<11,53> &id98in_a = id91out_result;
    const HWFloat<11,53> &id98in_b = id903out_value;

    id98out_result = (eq_float(id98in_a,id98in_b));
  }
  { // Node ID: 100 (NodeConstantRawBits)
  }
  { // Node ID: 99 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id101out_result;

  { // Node ID: 101 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id101in_sel = id98out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id101in_option0 = id100out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id101in_option1 = id99out_value;

    HWOffsetFix<1,0,UNSIGNED> id101x_1;

    switch((id101in_sel.getValueAsLong())) {
      case 0l:
        id101x_1 = id101in_option0;
        break;
      case 1l:
        id101x_1 = id101in_option1;
        break;
      default:
        id101x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id101out_result = (id101x_1);
  }
  { // Node ID: 902 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id93out_result;

  { // Node ID: 93 (NodeLt)
    const HWFloat<11,53> &id93in_a = id91out_result;
    const HWFloat<11,53> &id93in_b = id902out_value;

    id93out_result = (lt_float(id93in_a,id93in_b));
  }
  { // Node ID: 95 (NodeConstantRawBits)
  }
  { // Node ID: 94 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id96out_result;

  { // Node ID: 96 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id96in_sel = id93out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id96in_option0 = id95out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id96in_option1 = id94out_value;

    HWOffsetFix<1,0,UNSIGNED> id96x_1;

    switch((id96in_sel.getValueAsLong())) {
      case 0l:
        id96x_1 = id96in_option0;
        break;
      case 1l:
        id96x_1 = id96in_option1;
        break;
      default:
        id96x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id96out_result = (id96x_1);
  }
  { // Node ID: 103 (NodeConstantRawBits)
  }
  { // Node ID: 102 (NodeConstantRawBits)
  }
  HWFloat<11,53> id104out_result;

  { // Node ID: 104 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id104in_sel = id96out_result;
    const HWFloat<11,53> &id104in_option0 = id103out_value;
    const HWFloat<11,53> &id104in_option1 = id102out_value;

    HWFloat<11,53> id104x_1;

    switch((id104in_sel.getValueAsLong())) {
      case 0l:
        id104x_1 = id104in_option0;
        break;
      case 1l:
        id104x_1 = id104in_option1;
        break;
      default:
        id104x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id104out_result = (id104x_1);
  }
  { // Node ID: 105 (NodeConstantRawBits)
  }
  HWFloat<11,53> id106out_result;

  { // Node ID: 106 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id106in_sel = id101out_result;
    const HWFloat<11,53> &id106in_option0 = id104out_result;
    const HWFloat<11,53> &id106in_option1 = id105out_value;

    HWFloat<11,53> id106x_1;

    switch((id106in_sel.getValueAsLong())) {
      case 0l:
        id106x_1 = id106in_option0;
        break;
      case 1l:
        id106x_1 = id106in_option1;
        break;
      default:
        id106x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id106out_result = (id106x_1);
  }
  HWFloat<11,53> id108out_result;

  { // Node ID: 108 (NodeSub)
    const HWFloat<11,53> &id108in_a = id907out_value;
    const HWFloat<11,53> &id108in_b = id106out_result;

    id108out_result = (sub_float(id108in_a,id108in_b));
  }
  HWFloat<11,53> id109out_result;

  { // Node ID: 109 (NodeMul)
    const HWFloat<11,53> &id109in_a = id89out_result;
    const HWFloat<11,53> &id109in_b = id108out_result;

    id109out_result = (mul_float(id109in_a,id109in_b));
  }
  { // Node ID: 901 (NodeConstantRawBits)
  }
  HWFloat<11,53> id111out_result;

  { // Node ID: 111 (NodeSub)
    const HWFloat<11,53> &id111in_a = id71out_result[getCycle()%15];
    const HWFloat<11,53> &id111in_b = id901out_value;

    id111out_result = (sub_float(id111in_a,id111in_b));
  }
  { // Node ID: 900 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id118out_result;

  { // Node ID: 118 (NodeEq)
    const HWFloat<11,53> &id118in_a = id111out_result;
    const HWFloat<11,53> &id118in_b = id900out_value;

    id118out_result = (eq_float(id118in_a,id118in_b));
  }
  { // Node ID: 120 (NodeConstantRawBits)
  }
  { // Node ID: 119 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id121out_result;

  { // Node ID: 121 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id121in_sel = id118out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id121in_option0 = id120out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id121in_option1 = id119out_value;

    HWOffsetFix<1,0,UNSIGNED> id121x_1;

    switch((id121in_sel.getValueAsLong())) {
      case 0l:
        id121x_1 = id121in_option0;
        break;
      case 1l:
        id121x_1 = id121in_option1;
        break;
      default:
        id121x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id121out_result = (id121x_1);
  }
  { // Node ID: 899 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id113out_result;

  { // Node ID: 113 (NodeLt)
    const HWFloat<11,53> &id113in_a = id111out_result;
    const HWFloat<11,53> &id113in_b = id899out_value;

    id113out_result = (lt_float(id113in_a,id113in_b));
  }
  { // Node ID: 115 (NodeConstantRawBits)
  }
  { // Node ID: 114 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id116out_result;

  { // Node ID: 116 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id116in_sel = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id116in_option0 = id115out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id116in_option1 = id114out_value;

    HWOffsetFix<1,0,UNSIGNED> id116x_1;

    switch((id116in_sel.getValueAsLong())) {
      case 0l:
        id116x_1 = id116in_option0;
        break;
      case 1l:
        id116x_1 = id116in_option1;
        break;
      default:
        id116x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id116out_result = (id116x_1);
  }
  { // Node ID: 123 (NodeConstantRawBits)
  }
  { // Node ID: 122 (NodeConstantRawBits)
  }
  HWFloat<11,53> id124out_result;

  { // Node ID: 124 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id124in_sel = id116out_result;
    const HWFloat<11,53> &id124in_option0 = id123out_value;
    const HWFloat<11,53> &id124in_option1 = id122out_value;

    HWFloat<11,53> id124x_1;

    switch((id124in_sel.getValueAsLong())) {
      case 0l:
        id124x_1 = id124in_option0;
        break;
      case 1l:
        id124x_1 = id124in_option1;
        break;
      default:
        id124x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id124out_result = (id124x_1);
  }
  { // Node ID: 125 (NodeConstantRawBits)
  }
  HWFloat<11,53> id126out_result;

  { // Node ID: 126 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id126in_sel = id121out_result;
    const HWFloat<11,53> &id126in_option0 = id124out_result;
    const HWFloat<11,53> &id126in_option1 = id125out_value;

    HWFloat<11,53> id126x_1;

    switch((id126in_sel.getValueAsLong())) {
      case 0l:
        id126x_1 = id126in_option0;
        break;
      case 1l:
        id126x_1 = id126in_option1;
        break;
      default:
        id126x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id126out_result = (id126x_1);
  }
  { // Node ID: 898 (NodeConstantRawBits)
  }
  { // Node ID: 897 (NodeConstantRawBits)
  }
  HWFloat<11,53> id128out_result;

  { // Node ID: 128 (NodeSub)
    const HWFloat<11,53> &id128in_a = id838out_output[getCycle()%25];
    const HWFloat<11,53> &id128in_b = id897out_value;

    id128out_result = (sub_float(id128in_a,id128in_b));
  }
  { // Node ID: 896 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id135out_result;

  { // Node ID: 135 (NodeEq)
    const HWFloat<11,53> &id135in_a = id128out_result;
    const HWFloat<11,53> &id135in_b = id896out_value;

    id135out_result = (eq_float(id135in_a,id135in_b));
  }
  { // Node ID: 137 (NodeConstantRawBits)
  }
  { // Node ID: 136 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id138out_result;

  { // Node ID: 138 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id138in_sel = id135out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id138in_option0 = id137out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id138in_option1 = id136out_value;

    HWOffsetFix<1,0,UNSIGNED> id138x_1;

    switch((id138in_sel.getValueAsLong())) {
      case 0l:
        id138x_1 = id138in_option0;
        break;
      case 1l:
        id138x_1 = id138in_option1;
        break;
      default:
        id138x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id138out_result = (id138x_1);
  }
  { // Node ID: 895 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id130out_result;

  { // Node ID: 130 (NodeLt)
    const HWFloat<11,53> &id130in_a = id128out_result;
    const HWFloat<11,53> &id130in_b = id895out_value;

    id130out_result = (lt_float(id130in_a,id130in_b));
  }
  { // Node ID: 132 (NodeConstantRawBits)
  }
  { // Node ID: 131 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id133out_result;

  { // Node ID: 133 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id133in_sel = id130out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id133in_option0 = id132out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id133in_option1 = id131out_value;

    HWOffsetFix<1,0,UNSIGNED> id133x_1;

    switch((id133in_sel.getValueAsLong())) {
      case 0l:
        id133x_1 = id133in_option0;
        break;
      case 1l:
        id133x_1 = id133in_option1;
        break;
      default:
        id133x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id133out_result = (id133x_1);
  }
  { // Node ID: 140 (NodeConstantRawBits)
  }
  { // Node ID: 139 (NodeConstantRawBits)
  }
  HWFloat<11,53> id141out_result;

  { // Node ID: 141 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id141in_sel = id133out_result;
    const HWFloat<11,53> &id141in_option0 = id140out_value;
    const HWFloat<11,53> &id141in_option1 = id139out_value;

    HWFloat<11,53> id141x_1;

    switch((id141in_sel.getValueAsLong())) {
      case 0l:
        id141x_1 = id141in_option0;
        break;
      case 1l:
        id141x_1 = id141in_option1;
        break;
      default:
        id141x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id141out_result = (id141x_1);
  }
  { // Node ID: 142 (NodeConstantRawBits)
  }
  HWFloat<11,53> id143out_result;

  { // Node ID: 143 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id143in_sel = id138out_result;
    const HWFloat<11,53> &id143in_option0 = id141out_result;
    const HWFloat<11,53> &id143in_option1 = id142out_value;

    HWFloat<11,53> id143x_1;

    switch((id143in_sel.getValueAsLong())) {
      case 0l:
        id143x_1 = id143in_option0;
        break;
      case 1l:
        id143x_1 = id143in_option1;
        break;
      default:
        id143x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id143out_result = (id143x_1);
  }
  HWFloat<11,53> id145out_result;

  { // Node ID: 145 (NodeSub)
    const HWFloat<11,53> &id145in_a = id898out_value;
    const HWFloat<11,53> &id145in_b = id143out_result;

    id145out_result = (sub_float(id145in_a,id145in_b));
  }
  HWFloat<11,53> id146out_result;

  { // Node ID: 146 (NodeMul)
    const HWFloat<11,53> &id146in_a = id126out_result;
    const HWFloat<11,53> &id146in_b = id145out_result;

    id146out_result = (mul_float(id146in_a,id146in_b));
  }
  HWFloat<11,53> id147out_result;

  { // Node ID: 147 (NodeAdd)
    const HWFloat<11,53> &id147in_a = id109out_result;
    const HWFloat<11,53> &id147in_b = id146out_result;

    id147out_result = (add_float(id147in_a,id147in_b));
  }
  { // Node ID: 894 (NodeConstantRawBits)
  }
  HWFloat<11,53> id149out_result;

  { // Node ID: 149 (NodeSub)
    const HWFloat<11,53> &id149in_a = id71out_result[getCycle()%15];
    const HWFloat<11,53> &id149in_b = id894out_value;

    id149out_result = (sub_float(id149in_a,id149in_b));
  }
  { // Node ID: 893 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id156out_result;

  { // Node ID: 156 (NodeEq)
    const HWFloat<11,53> &id156in_a = id149out_result;
    const HWFloat<11,53> &id156in_b = id893out_value;

    id156out_result = (eq_float(id156in_a,id156in_b));
  }
  { // Node ID: 158 (NodeConstantRawBits)
  }
  { // Node ID: 157 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id159out_result;

  { // Node ID: 159 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id159in_sel = id156out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id159in_option0 = id158out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id159in_option1 = id157out_value;

    HWOffsetFix<1,0,UNSIGNED> id159x_1;

    switch((id159in_sel.getValueAsLong())) {
      case 0l:
        id159x_1 = id159in_option0;
        break;
      case 1l:
        id159x_1 = id159in_option1;
        break;
      default:
        id159x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id159out_result = (id159x_1);
  }
  { // Node ID: 892 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id151out_result;

  { // Node ID: 151 (NodeLt)
    const HWFloat<11,53> &id151in_a = id149out_result;
    const HWFloat<11,53> &id151in_b = id892out_value;

    id151out_result = (lt_float(id151in_a,id151in_b));
  }
  { // Node ID: 153 (NodeConstantRawBits)
  }
  { // Node ID: 152 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id154out_result;

  { // Node ID: 154 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id154in_sel = id151out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id154in_option0 = id153out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id154in_option1 = id152out_value;

    HWOffsetFix<1,0,UNSIGNED> id154x_1;

    switch((id154in_sel.getValueAsLong())) {
      case 0l:
        id154x_1 = id154in_option0;
        break;
      case 1l:
        id154x_1 = id154in_option1;
        break;
      default:
        id154x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id154out_result = (id154x_1);
  }
  { // Node ID: 161 (NodeConstantRawBits)
  }
  { // Node ID: 160 (NodeConstantRawBits)
  }
  HWFloat<11,53> id162out_result;

  { // Node ID: 162 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id162in_sel = id154out_result;
    const HWFloat<11,53> &id162in_option0 = id161out_value;
    const HWFloat<11,53> &id162in_option1 = id160out_value;

    HWFloat<11,53> id162x_1;

    switch((id162in_sel.getValueAsLong())) {
      case 0l:
        id162x_1 = id162in_option0;
        break;
      case 1l:
        id162x_1 = id162in_option1;
        break;
      default:
        id162x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id162out_result = (id162x_1);
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  HWFloat<11,53> id164out_result;

  { // Node ID: 164 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id164in_sel = id159out_result;
    const HWFloat<11,53> &id164in_option0 = id162out_result;
    const HWFloat<11,53> &id164in_option1 = id163out_value;

    HWFloat<11,53> id164x_1;

    switch((id164in_sel.getValueAsLong())) {
      case 0l:
        id164x_1 = id164in_option0;
        break;
      case 1l:
        id164x_1 = id164in_option1;
        break;
      default:
        id164x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id164out_result = (id164x_1);
  }
  { // Node ID: 891 (NodeConstantRawBits)
  }
  { // Node ID: 890 (NodeConstantRawBits)
  }
  HWFloat<11,53> id166out_result;

  { // Node ID: 166 (NodeSub)
    const HWFloat<11,53> &id166in_a = id838out_output[getCycle()%25];
    const HWFloat<11,53> &id166in_b = id890out_value;

    id166out_result = (sub_float(id166in_a,id166in_b));
  }
  { // Node ID: 889 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id173out_result;

  { // Node ID: 173 (NodeEq)
    const HWFloat<11,53> &id173in_a = id166out_result;
    const HWFloat<11,53> &id173in_b = id889out_value;

    id173out_result = (eq_float(id173in_a,id173in_b));
  }
  { // Node ID: 175 (NodeConstantRawBits)
  }
  { // Node ID: 174 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id176out_result;

  { // Node ID: 176 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id176in_sel = id173out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id176in_option0 = id175out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id176in_option1 = id174out_value;

    HWOffsetFix<1,0,UNSIGNED> id176x_1;

    switch((id176in_sel.getValueAsLong())) {
      case 0l:
        id176x_1 = id176in_option0;
        break;
      case 1l:
        id176x_1 = id176in_option1;
        break;
      default:
        id176x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id176out_result = (id176x_1);
  }
  { // Node ID: 888 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id168out_result;

  { // Node ID: 168 (NodeLt)
    const HWFloat<11,53> &id168in_a = id166out_result;
    const HWFloat<11,53> &id168in_b = id888out_value;

    id168out_result = (lt_float(id168in_a,id168in_b));
  }
  { // Node ID: 170 (NodeConstantRawBits)
  }
  { // Node ID: 169 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id171out_result;

  { // Node ID: 171 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id171in_sel = id168out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id171in_option0 = id170out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id171in_option1 = id169out_value;

    HWOffsetFix<1,0,UNSIGNED> id171x_1;

    switch((id171in_sel.getValueAsLong())) {
      case 0l:
        id171x_1 = id171in_option0;
        break;
      case 1l:
        id171x_1 = id171in_option1;
        break;
      default:
        id171x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id171out_result = (id171x_1);
  }
  { // Node ID: 178 (NodeConstantRawBits)
  }
  { // Node ID: 177 (NodeConstantRawBits)
  }
  HWFloat<11,53> id179out_result;

  { // Node ID: 179 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id179in_sel = id171out_result;
    const HWFloat<11,53> &id179in_option0 = id178out_value;
    const HWFloat<11,53> &id179in_option1 = id177out_value;

    HWFloat<11,53> id179x_1;

    switch((id179in_sel.getValueAsLong())) {
      case 0l:
        id179x_1 = id179in_option0;
        break;
      case 1l:
        id179x_1 = id179in_option1;
        break;
      default:
        id179x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id179out_result = (id179x_1);
  }
  { // Node ID: 180 (NodeConstantRawBits)
  }
  HWFloat<11,53> id181out_result;

  { // Node ID: 181 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id181in_sel = id176out_result;
    const HWFloat<11,53> &id181in_option0 = id179out_result;
    const HWFloat<11,53> &id181in_option1 = id180out_value;

    HWFloat<11,53> id181x_1;

    switch((id181in_sel.getValueAsLong())) {
      case 0l:
        id181x_1 = id181in_option0;
        break;
      case 1l:
        id181x_1 = id181in_option1;
        break;
      default:
        id181x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id181out_result = (id181x_1);
  }
  HWFloat<11,53> id183out_result;

  { // Node ID: 183 (NodeSub)
    const HWFloat<11,53> &id183in_a = id891out_value;
    const HWFloat<11,53> &id183in_b = id181out_result;

    id183out_result = (sub_float(id183in_a,id183in_b));
  }
  HWFloat<11,53> id184out_result;

  { // Node ID: 184 (NodeMul)
    const HWFloat<11,53> &id184in_a = id164out_result;
    const HWFloat<11,53> &id184in_b = id183out_result;

    id184out_result = (mul_float(id184in_a,id184in_b));
  }
  HWFloat<11,53> id185out_result;

  { // Node ID: 185 (NodeAdd)
    const HWFloat<11,53> &id185in_a = id147out_result;
    const HWFloat<11,53> &id185in_b = id184out_result;

    id185out_result = (add_float(id185in_a,id185in_b));
  }
  { // Node ID: 887 (NodeConstantRawBits)
  }
  { // Node ID: 187 (NodeEq)
    const HWFloat<11,53> &id187in_a = id185out_result;
    const HWFloat<11,53> &id187in_b = id887out_value;

    id187out_result[(getCycle()+2)%3] = (eq_float(id187in_a,id187in_b));
  }
  { // Node ID: 886 (NodeConstantRawBits)
  }
  { // Node ID: 718 (NodeLtInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id718in_a = id862out_output[getCycle()%37];
    const HWOffsetFix<32,0,UNSIGNED> &id718in_b = id886out_value;

    id718out_result[(getCycle()+1)%2] = (lt_fixed(id718in_a,id718in_b));
  }
  { // Node ID: 190 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id190in_a = id187out_result[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id190in_b = id718out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id190x_1;

    (id190x_1) = (and_fixed(id190in_a,id190in_b));
    id190out_result[(getCycle()+1)%2] = (id190x_1);
  }
  { // Node ID: 192 (NodeConstantRawBits)
  }
  { // Node ID: 191 (NodeConstantRawBits)
  }
  { // Node ID: 193 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id193in_sel = id190out_result[getCycle()%2];
    const HWFloat<11,53> &id193in_option0 = id192out_value;
    const HWFloat<11,53> &id193in_option1 = id191out_value;

    HWFloat<11,53> id193x_1;

    switch((id193in_sel.getValueAsLong())) {
      case 0l:
        id193x_1 = id193in_option0;
        break;
      case 1l:
        id193x_1 = id193in_option1;
        break;
      default:
        id193x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id193out_result[(getCycle()+1)%2] = (id193x_1);
  }
  HWFloat<11,53> id603out_result;

  { // Node ID: 603 (NodeSub)
    const HWFloat<11,53> &id603in_a = id602out_result;
    const HWFloat<11,53> &id603in_b = id193out_result[getCycle()%2];

    id603out_result = (sub_float(id603in_a,id603in_b));
  }
  HWFloat<11,53> id604out_result;

  { // Node ID: 604 (NodeMul)
    const HWFloat<11,53> &id604in_a = id603out_result;
    const HWFloat<11,53> &id604in_b = id36out_dt;

    id604out_result = (mul_float(id604in_a,id604in_b));
  }
  HWFloat<11,53> id605out_result;

  { // Node ID: 605 (NodeSub)
    const HWFloat<11,53> &id605in_a = id869out_output[getCycle()%10];
    const HWFloat<11,53> &id605in_b = id604out_result;

    id605out_result = (sub_float(id605in_a,id605in_b));
  }
  { // Node ID: 885 (NodeConstantRawBits)
  }
  { // Node ID: 719 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id719in_a = id863out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id719in_b = id885out_value;

    id719out_result[(getCycle()+1)%2] = (eq_fixed(id719in_a,id719in_b));
  }
  { // Node ID: 884 (NodeConstantRawBits)
  }
  { // Node ID: 210 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id210in_a = id38out_nx;
    const HWOffsetFix<32,0,UNSIGNED> &id210in_b = id884out_value;

    id210out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id210in_a,id210in_b));
  }
  { // Node ID: 720 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id720in_a = id862out_output[getCycle()%37];
    const HWOffsetFix<32,0,UNSIGNED> &id720in_b = id210out_result[getCycle()%2];

    id720out_result[(getCycle()+1)%2] = (eq_fixed(id720in_a,id720in_b));
  }
  { // Node ID: 37 (NodeInputMappedReg)
  }
  HWFloat<11,53> id195out_output;

  { // Node ID: 195 (NodeStreamOffset)
    const HWFloat<11,53> &id195in_input = id848out_output[getCycle()%5];

    id195out_output = id195in_input;
  }
  { // Node ID: 883 (NodeConstantRawBits)
  }
  { // Node ID: 197 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id197in_a = id45out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id197in_b = id883out_value;

    id197out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id197in_a,id197in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id198out_o;

  { // Node ID: 198 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id198in_i = id197out_result[getCycle()%2];

    id198out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id198in_i));
  }
  HWFloat<11,53> id606out_result;

  { // Node ID: 606 (NodeAdd)
    const HWFloat<11,53> &id606in_a = id605out_result;
    const HWFloat<11,53> &id606in_b = id224out_result[getCycle()%2];

    id606out_result = (add_float(id606in_a,id606in_b));
  }
  HWFloat<11,53> id765out_output;

  { // Node ID: 765 (NodeStreamOffset)
    const HWFloat<11,53> &id765in_input = id606out_result;

    id765out_output = id765in_input;
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 667 (NodeRAM)
    const bool id667_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id667in_addrA = id614out_o;
    const HWFloat<11,53> &id667in_dina = id765out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_wea = id689out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id667in_addrB = id198out_o;

    long id667x_1;
    long id667x_2;
    HWFloat<11,53> id667x_3;

    (id667x_1) = (id667in_addrA.getValueAsLong());
    (id667x_2) = (id667in_addrB.getValueAsLong());
    switch(((long)((id667x_2)<(1000000l)))) {
      case 0l:
        id667x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id667x_3 = (id667sta_ram_store[(id667x_2)]);
        break;
      default:
        id667x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id667out_doutb[(getCycle()+2)%3] = (id667x_3);
    if(((id667in_wea.getValueAsBool())&id667_inputvalid)) {
      if(((id667x_1)<(1000000l))) {
        (id667sta_ram_store[(id667x_1)]) = id667in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_7("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 667) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 199 (NodeConstantRawBits)
  }
  { // Node ID: 200 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id200in_sel = id688out_result[getCycle()%2];
    const HWFloat<11,53> &id200in_option0 = id667out_doutb[getCycle()%3];
    const HWFloat<11,53> &id200in_option1 = id199out_value;

    HWFloat<11,53> id200x_1;

    switch((id200in_sel.getValueAsLong())) {
      case 0l:
        id200x_1 = id200in_option0;
        break;
      case 1l:
        id200x_1 = id200in_option1;
        break;
      default:
        id200x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id200out_result[(getCycle()+1)%2] = (id200x_1);
  }
  { // Node ID: 218 (NodeAdd)
    const HWFloat<11,53> &id218in_a = id195out_output;
    const HWFloat<11,53> &id218in_b = id200out_result[getCycle()%2];

    id218out_result[(getCycle()+14)%15] = (add_float(id218in_a,id218in_b));
  }
  { // Node ID: 739 (NodePO2FPMult)
    const HWFloat<11,53> &id739in_floatIn = id849out_output[getCycle()%8];

    id739out_floatOut[(getCycle()+1)%2] = (mul_float(id739in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 221 (NodeSub)
    const HWFloat<11,53> &id221in_a = id218out_result[getCycle()%15];
    const HWFloat<11,53> &id221in_b = id739out_floatOut[getCycle()%2];

    id221out_result[(getCycle()+14)%15] = (sub_float(id221in_a,id221in_b));
  }
  { // Node ID: 222 (NodeMul)
    const HWFloat<11,53> &id222in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id222in_b = id221out_result[getCycle()%15];

    id222out_result[(getCycle()+12)%13] = (mul_float(id222in_a,id222in_b));
  }
  { // Node ID: 740 (NodePO2FPMult)
    const HWFloat<11,53> &id740in_floatIn = id849out_output[getCycle()%8];

    id740out_floatOut[(getCycle()+1)%2] = (mul_float(id740in_floatIn,(c_hw_flt_11_53_n2_0val)));
  }
  { // Node ID: 851 (NodeFIFO)
    const HWFloat<11,53> &id851in_input = id195out_output;

    id851out_output[(getCycle()+13)%14] = id851in_input;
  }
  { // Node ID: 741 (NodePO2FPMult)
    const HWFloat<11,53> &id741in_floatIn = id851out_output[getCycle()%14];

    id741out_floatOut[(getCycle()+1)%2] = (mul_float(id741in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 216 (NodeAdd)
    const HWFloat<11,53> &id216in_a = id740out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id216in_b = id741out_floatOut[getCycle()%2];

    id216out_result[(getCycle()+14)%15] = (add_float(id216in_a,id216in_b));
  }
  { // Node ID: 217 (NodeMul)
    const HWFloat<11,53> &id217in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id217in_b = id216out_result[getCycle()%15];

    id217out_result[(getCycle()+12)%13] = (mul_float(id217in_a,id217in_b));
  }
  { // Node ID: 223 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id223in_sel = id720out_result[getCycle()%2];
    const HWFloat<11,53> &id223in_option0 = id222out_result[getCycle()%13];
    const HWFloat<11,53> &id223in_option1 = id217out_result[getCycle()%13];

    HWFloat<11,53> id223x_1;

    switch((id223in_sel.getValueAsLong())) {
      case 0l:
        id223x_1 = id223in_option0;
        break;
      case 1l:
        id223x_1 = id223in_option1;
        break;
      default:
        id223x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id223out_result[(getCycle()+1)%2] = (id223x_1);
  }
  { // Node ID: 742 (NodePO2FPMult)
    const HWFloat<11,53> &id742in_floatIn = id865out_output[getCycle()%2];

    id742out_floatOut[(getCycle()+1)%2] = (mul_float(id742in_floatIn,(c_hw_flt_11_53_n2_0val)));
  }
  { // Node ID: 853 (NodeFIFO)
    const HWFloat<11,53> &id853in_input = id200out_result[getCycle()%2];

    id853out_output[(getCycle()+14)%15] = id853in_input;
  }
  { // Node ID: 743 (NodePO2FPMult)
    const HWFloat<11,53> &id743in_floatIn = id853out_output[getCycle()%15];

    id743out_floatOut[(getCycle()+1)%2] = (mul_float(id743in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 207 (NodeAdd)
    const HWFloat<11,53> &id207in_a = id742out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id207in_b = id743out_floatOut[getCycle()%2];

    id207out_result[(getCycle()+14)%15] = (add_float(id207in_a,id207in_b));
  }
  { // Node ID: 208 (NodeMul)
    const HWFloat<11,53> &id208in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id208in_b = id207out_result[getCycle()%15];

    id208out_result[(getCycle()+12)%13] = (mul_float(id208in_a,id208in_b));
  }
  { // Node ID: 224 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id224in_sel = id719out_result[getCycle()%2];
    const HWFloat<11,53> &id224in_option0 = id223out_result[getCycle()%2];
    const HWFloat<11,53> &id224in_option1 = id208out_result[getCycle()%13];

    HWFloat<11,53> id224x_1;

    switch((id224in_sel.getValueAsLong())) {
      case 0l:
        id224x_1 = id224in_option0;
        break;
      case 1l:
        id224x_1 = id224in_option1;
        break;
      default:
        id224x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id224out_result[(getCycle()+1)%2] = (id224x_1);
  }
  { // Node ID: 848 (NodeFIFO)
    const HWFloat<11,53> &id848in_input = id765out_output;

    id848out_output[(getCycle()+4)%5] = id848in_input;
  }
  { // Node ID: 874 (NodeFIFO)
    const HWFloat<11,53> &id874in_input = id848out_output[getCycle()%5];

    id874out_output[(getCycle()+2)%3] = id874in_input;
  }
  { // Node ID: 875 (NodeFIFO)
    const HWFloat<11,53> &id875in_input = id874out_output[getCycle()%3];

    id875out_output[(getCycle()+39)%40] = id875in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 747 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id747in_output_control = id744out_value;
    const HWFloat<11,53> &id747in_data = id875out_output[getCycle()%40];

    bool id747x_1;

    (id747x_1) = ((id747in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id747x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id747in_data);
    }
  }
  { // Node ID: 872 (NodeFIFO)
    const HWFloat<11,53> &id872in_input = id851out_output[getCycle()%14];

    id872out_output[(getCycle()+29)%30] = id872in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 748 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id748in_output_control = id744out_value;
    const HWFloat<11,53> &id748in_data = id872out_output[getCycle()%30];

    bool id748x_1;

    (id748x_1) = ((id748in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id748x_1)) {
      writeOutput(m_internal_watch_previouscell_output, id748in_data);
    }
  }
  { // Node ID: 873 (NodeFIFO)
    const HWFloat<11,53> &id873in_input = id853out_output[getCycle()%15];

    id873out_output[(getCycle()+28)%29] = id873in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 749 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id749in_output_control = id744out_value;
    const HWFloat<11,53> &id749in_data = id873out_output[getCycle()%29];

    bool id749x_1;

    (id749x_1) = ((id749in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id749x_1)) {
      writeOutput(m_internal_watch_nextcell_output, id749in_data);
    }
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 750 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id750in_output_control = id744out_value;
    const HWFloat<11,53> &id750in_data = id224out_result[getCycle()%2];

    bool id750x_1;

    (id750x_1) = ((id750in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id750x_1)) {
      writeOutput(m_internal_watch_laplace_output, id750in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 194 (NodeWatch)
  }
  { // Node ID: 882 (NodeConstantRawBits)
  }
  { // Node ID: 639 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id639in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id639in_b = id882out_value;

    id639out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id639in_a,id639in_b));
  }
  { // Node ID: 721 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id721in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id721in_b = id639out_result[getCycle()%2];

    id721out_result[(getCycle()+1)%2] = (eq_fixed(id721in_a,id721in_b));
  }
  { // Node ID: 641 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id642out_result;

  { // Node ID: 642 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id642in_a = id641out_io_u_out_force_disabled;

    id642out_result = (not_fixed(id642in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id643out_result;

  { // Node ID: 643 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id643in_a = id721out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id643in_b = id642out_result;

    HWOffsetFix<1,0,UNSIGNED> id643x_1;

    (id643x_1) = (and_fixed(id643in_a,id643in_b));
    id643out_result = (id643x_1);
  }
  { // Node ID: 857 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id857in_input = id643out_result;

    id857out_output[(getCycle()+45)%46] = id857in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 644 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id644in_output_control = id857out_output[getCycle()%46];
    const HWFloat<11,53> &id644in_data = id606out_result;

    bool id644x_1;

    (id644x_1) = ((id644in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id644x_1)) {
      writeOutput(m_u_out, id644in_data);
    }
  }
  { // Node ID: 881 (NodeConstantRawBits)
  }
  { // Node ID: 646 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id646in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id646in_b = id881out_value;

    id646out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id646in_a,id646in_b));
  }
  { // Node ID: 722 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id722in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id722in_b = id646out_result[getCycle()%2];

    id722out_result[(getCycle()+1)%2] = (eq_fixed(id722in_a,id722in_b));
  }
  { // Node ID: 648 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id649out_result;

  { // Node ID: 649 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id649in_a = id648out_io_v_out_force_disabled;

    id649out_result = (not_fixed(id649in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id650out_result;

  { // Node ID: 650 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id650in_a = id722out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id650in_b = id649out_result;

    HWOffsetFix<1,0,UNSIGNED> id650x_1;

    (id650x_1) = (and_fixed(id650in_a,id650in_b));
    id650out_result = (id650x_1);
  }
  { // Node ID: 858 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_input = id650out_result;

    id858out_output[(getCycle()+35)%36] = id858in_input;
  }
  if ( (getFillLevel() >= (39l)) && (getFlushLevel() < (39l)|| !isFlushingActive() ))
  { // Node ID: 651 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id651in_output_control = id858out_output[getCycle()%36];
    const HWFloat<11,53> &id651in_data = id576out_result[getCycle()%15];

    bool id651x_1;

    (id651x_1) = ((id651in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(39l))&(isFlushingActive()))));
    if((id651x_1)) {
      writeOutput(m_v_out, id651in_data);
    }
  }
  { // Node ID: 880 (NodeConstantRawBits)
  }
  { // Node ID: 653 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id653in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id653in_b = id880out_value;

    id653out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id653in_a,id653in_b));
  }
  { // Node ID: 723 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id723in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id723in_b = id653out_result[getCycle()%2];

    id723out_result[(getCycle()+1)%2] = (eq_fixed(id723in_a,id723in_b));
  }
  { // Node ID: 655 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id656out_result;

  { // Node ID: 656 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id656in_a = id655out_io_w_out_force_disabled;

    id656out_result = (not_fixed(id656in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id657out_result;

  { // Node ID: 657 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id657in_a = id723out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id657in_b = id656out_result;

    HWOffsetFix<1,0,UNSIGNED> id657x_1;

    (id657x_1) = (and_fixed(id657in_a,id657in_b));
    id657out_result = (id657x_1);
  }
  { // Node ID: 859 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id859in_input = id657out_result;

    id859out_output[(getCycle()+44)%45] = id859in_input;
  }
  if ( (getFillLevel() >= (48l)) && (getFlushLevel() < (48l)|| !isFlushingActive() ))
  { // Node ID: 658 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_output_control = id859out_output[getCycle()%45];
    const HWFloat<11,53> &id658in_data = id578out_result[getCycle()%15];

    bool id658x_1;

    (id658x_1) = ((id658in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(48l))&(isFlushingActive()))));
    if((id658x_1)) {
      writeOutput(m_w_out, id658in_data);
    }
  }
  { // Node ID: 879 (NodeConstantRawBits)
  }
  { // Node ID: 660 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id660in_a = id876out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id660in_b = id879out_value;

    id660out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id660in_a,id660in_b));
  }
  { // Node ID: 724 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id724in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id724in_b = id660out_result[getCycle()%2];

    id724out_result[(getCycle()+1)%2] = (eq_fixed(id724in_a,id724in_b));
  }
  { // Node ID: 662 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id663out_result;

  { // Node ID: 663 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id663in_a = id662out_io_s_out_force_disabled;

    id663out_result = (not_fixed(id663in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id664out_result;

  { // Node ID: 664 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_a = id724out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id664in_b = id663out_result;

    HWOffsetFix<1,0,UNSIGNED> id664x_1;

    (id664x_1) = (and_fixed(id664in_a,id664in_b));
    id664out_result = (id664x_1);
  }
  { // Node ID: 860 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id860in_input = id664out_result;

    id860out_output[(getCycle()+45)%46] = id860in_input;
  }
  if ( (getFillLevel() >= (49l)) && (getFlushLevel() < (49l)|| !isFlushingActive() ))
  { // Node ID: 665 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id665in_output_control = id860out_output[getCycle()%46];
    const HWFloat<11,53> &id665in_data = id580out_result[getCycle()%15];

    bool id665x_1;

    (id665x_1) = ((id665in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(49l))&(isFlushingActive()))));
    if((id665x_1)) {
      writeOutput(m_s_out, id665in_data);
    }
  }
  { // Node ID: 677 (NodeConstantRawBits)
  }
  { // Node ID: 878 (NodeConstantRawBits)
  }
  { // Node ID: 674 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 675 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id675in_enable = id878out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id675in_max = id674out_value;

    HWOffsetFix<49,0,UNSIGNED> id675x_1;
    HWOffsetFix<1,0,UNSIGNED> id675x_2;
    HWOffsetFix<1,0,UNSIGNED> id675x_3;
    HWOffsetFix<49,0,UNSIGNED> id675x_4t_1e_1;

    id675out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id675st_count)));
    (id675x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id675st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id675x_2) = (gte_fixed((id675x_1),id675in_max));
    (id675x_3) = (and_fixed((id675x_2),id675in_enable));
    id675out_wrap = (id675x_3);
    if((id675in_enable.getValueAsBool())) {
      if(((id675x_3).getValueAsBool())) {
        (id675st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id675x_4t_1e_1) = (id675x_1);
        (id675st_count) = (id675x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id676out_output;

  { // Node ID: 676 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id676in_input = id675out_count;

    id676out_output = id676in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 678 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id678in_load = id677out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id678in_data = id676out_output;

    bool id678x_1;

    (id678x_1) = ((id678in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id678x_1)) {
      setMappedRegValue("current_run_cycle_count", id678in_data);
    }
  }
  { // Node ID: 877 (NodeConstantRawBits)
  }
  { // Node ID: 680 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 681 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id681in_enable = id877out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id681in_max = id680out_value;

    HWOffsetFix<49,0,UNSIGNED> id681x_1;
    HWOffsetFix<1,0,UNSIGNED> id681x_2;
    HWOffsetFix<1,0,UNSIGNED> id681x_3;
    HWOffsetFix<49,0,UNSIGNED> id681x_4t_1e_1;

    id681out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id681st_count)));
    (id681x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id681st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id681x_2) = (gte_fixed((id681x_1),id681in_max));
    (id681x_3) = (and_fixed((id681x_2),id681in_enable));
    id681out_wrap = (id681x_3);
    if((id681in_enable.getValueAsBool())) {
      if(((id681x_3).getValueAsBool())) {
        (id681st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id681x_4t_1e_1) = (id681x_1);
        (id681st_count) = (id681x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 683 (NodeInputMappedReg)
  }
  { // Node ID: 725 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id725in_a = id681out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id725in_b = id683out_run_cycle_count;

    id725out_result[(getCycle()+1)%2] = (eq_fixed(id725in_a,id725in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 682 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id682in_start = id725out_result[getCycle()%2];

    if((id682in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
