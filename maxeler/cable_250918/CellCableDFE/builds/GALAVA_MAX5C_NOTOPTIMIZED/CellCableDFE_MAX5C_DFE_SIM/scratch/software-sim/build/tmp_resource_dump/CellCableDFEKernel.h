#ifndef CELLCABLEDFEKERNEL_H_
#define CELLCABLEDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFEKernel : public KernelManagerBlockSync {
public:
  CellCableDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_cell_output;
  t_port_number m_internal_watch_step_output;
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_internal_watch_previouscell_output;
  t_port_number m_internal_watch_nextcell_output;
  t_port_number m_internal_watch_laplace_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id744out_value;

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<32,0,UNSIGNED> id876out_value;

  HWOffsetFix<32,0,UNSIGNED> id766out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id46out_count;
  HWOffsetFix<1,0,UNSIGNED> id46out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id46st_count;

  HWOffsetFix<32,0,UNSIGNED> id38out_nx;

  HWOffsetFix<32,0,UNSIGNED> id45out_count;
  HWOffsetFix<1,0,UNSIGNED> id45out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id45st_count;

  HWOffsetFix<32,0,UNSIGNED> id778out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id861out_output[7];

  HWOffsetFix<32,0,UNSIGNED> id862out_output[37];

  HWOffsetFix<32,0,UNSIGNED> id863out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id864out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id768out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id39out_simTime;

  HWOffsetFix<32,0,UNSIGNED> id949out_value;

  HWOffsetFix<32,0,UNSIGNED> id43out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id44out_count;
  HWOffsetFix<1,0,UNSIGNED> id44out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id44st_count;

  HWOffsetFix<32,0,UNSIGNED> id769out_output[45];

  HWOffsetFix<32,0,UNSIGNED> id948out_value;

  HWOffsetFix<1,0,UNSIGNED> id688out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id775out_output[7];

  HWOffsetFix<20,0,UNSIGNED> id772out_output[7];

  HWOffsetFix<32,0,UNSIGNED> id947out_value;

  HWOffsetFix<32,0,UNSIGNED> id616out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id689out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id774out_output[7];

  HWFloat<11,53> id666out_doutb[3];

  HWFloat<11,53> id666sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_1 (const char* _format_arg_format_string);
  HWFloat<11,53> id55out_value;

  HWFloat<11,53> id56out_result[2];

  HWFloat<11,53> id849out_output[8];

  HWFloat<11,53> id865out_output[2];

  HWFloat<11,53> id866out_output[2];

  HWFloat<11,53> id867out_output[2];

  HWFloat<11,53> id868out_output[18];

  HWFloat<11,53> id869out_output[10];

  HWFloat<11,53> id15out_value;

  HWFloat<11,53> id34out_value;

  HWOffsetFix<32,0,UNSIGNED> id946out_value;

  HWOffsetFix<32,0,UNSIGNED> id620out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id690out_result[2];

  HWFloat<11,53> id669out_doutb[3];

  HWFloat<11,53> id669sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_2 (const char* _format_arg_format_string);
  HWFloat<11,53> id780out_output[7];

  HWFloat<11,53> id57out_value;

  HWFloat<11,53> id58out_result[2];

  HWFloat<11,53> id781out_output[13];

  HWFloat<11,53> id17out_value;

  HWFloat<11,53> id35out_value;

  HWFloat<11,53> id16out_value;

  HWFloat<11,53> id1out_value;

  HWFloat<11,53> id2out_value;

  HWFloat<11,53> id0out_value;

  HWFloat<11,53> id36out_dt;

  HWFloat<11,53> id575out_result[13];

  HWFloat<11,53> id576out_result[15];

  HWFloat<11,53> id777out_output[11];

  HWFloat<11,53> id28out_value;

  HWFloat<11,53> id8out_value;

  HWFloat<11,53> id19out_value;

  HWFloat<11,53> id21out_value;

  HWFloat<11,53> id9out_value;

  HWFloat<11,53> id10out_value;

  HWFloat<11,53> id945out_value;

  HWFloat<11,53> id11out_value;

  HWFloat<11,53> id31out_value;

  HWFloat<11,53> id944out_value;

  HWFloat<11,53> id943out_value;

  HWFloat<11,53> id942out_value;

  HWFloat<11,53> id24out_value;

  HWFloat<11,53> id29out_value;

  HWFloat<11,53> id735out_floatOut[2];

  HWRawBits<11> id415out_value;

  HWRawBits<52> id941out_value;

  HWOffsetFix<1,0,UNSIGNED> id798out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id338out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id340out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id340out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id343out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id789out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id384out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id726out_value;

  HWOffsetFix<38,-53,UNSIGNED> id430out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id430sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id427out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id427sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id424out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id424sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id361out_value;

  HWOffsetFix<26,-51,UNSIGNED> id790out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id940out_value;

  HWFloat<11,53> id939out_value;

  HWOffsetFix<1,0,UNSIGNED> id792out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id793out_output[3];

  HWFloat<11,53> id938out_value;

  HWOffsetFix<1,0,UNSIGNED> id794out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id795out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id937out_value;

  HWOffsetFix<1,0,UNSIGNED> id394out_value;

  HWOffsetFix<53,-52,UNSIGNED> id378out_value;

  HWOffsetFix<1,0,UNSIGNED> id403out_value;

  HWOffsetFix<11,0,UNSIGNED> id404out_value;

  HWOffsetFix<52,0,UNSIGNED> id406out_value;

  HWFloat<11,53> id685out_value;

  HWFloat<11,53> id936out_value;

  HWFloat<11,53> id935out_value;

  HWFloat<11,53> id20out_value;

  HWFloat<11,53> id599out_value;

  HWOffsetFix<32,0,UNSIGNED> id934out_value;

  HWOffsetFix<32,0,UNSIGNED> id624out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id698out_result[2];

  HWFloat<11,53> id668out_doutb[3];

  HWFloat<11,53> id668sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_3 (const char* _format_arg_format_string);
  HWFloat<11,53> id803out_output[7];

  HWFloat<11,53> id59out_value;

  HWFloat<11,53> id60out_result[2];

  HWFloat<11,53> id807out_output[10];

  HWFloat<11,53> id870out_output[13];

  HWFloat<11,53> id18out_value;

  HWFloat<11,53> id33out_value;

  HWFloat<11,53> id933out_value;

  HWFloat<11,53> id14out_value;

  HWFloat<11,53> id32out_value;

  HWFloat<11,53> id4out_value;

  HWFloat<11,53> id30out_value;

  HWFloat<11,53> id932out_value;

  HWFloat<11,53> id931out_value;

  HWFloat<11,53> id930out_value;

  HWFloat<11,53> id22out_value;

  HWFloat<11,53> id25out_value;

  HWFloat<11,53> id736out_floatOut[2];

  HWRawBits<11> id308out_value;

  HWRawBits<52> id929out_value;

  HWOffsetFix<1,0,UNSIGNED> id817out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id231out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id233out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id233out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id236out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id808out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id277out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id728out_value;

  HWOffsetFix<38,-53,UNSIGNED> id323out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id323sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id320out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id320sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id317out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id317sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id254out_value;

  HWOffsetFix<26,-51,UNSIGNED> id809out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id928out_value;

  HWFloat<11,53> id927out_value;

  HWOffsetFix<1,0,UNSIGNED> id811out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id812out_output[3];

  HWFloat<11,53> id926out_value;

  HWOffsetFix<1,0,UNSIGNED> id813out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id814out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id925out_value;

  HWOffsetFix<1,0,UNSIGNED> id287out_value;

  HWOffsetFix<53,-52,UNSIGNED> id271out_value;

  HWOffsetFix<1,0,UNSIGNED> id296out_value;

  HWOffsetFix<11,0,UNSIGNED> id297out_value;

  HWOffsetFix<52,0,UNSIGNED> id299out_value;

  HWFloat<11,53> id686out_value;

  HWFloat<11,53> id924out_value;

  HWFloat<11,53> id923out_value;

  HWFloat<11,53> id3out_value;

  HWFloat<11,53> id577out_result[13];

  HWFloat<11,53> id578out_result[15];

  HWFloat<11,53> id800out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id922out_value;

  HWOffsetFix<32,0,UNSIGNED> id628out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id706out_result[2];

  HWFloat<11,53> id670out_doutb[3];

  HWFloat<11,53> id670sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_4 (const char* _format_arg_format_string);
  HWFloat<11,53> id61out_value;

  HWFloat<11,53> id62out_result[2];

  HWFloat<11,53> id832out_output[17];

  HWFloat<11,53> id871out_output[13];

  HWFloat<11,53> id921out_value;

  HWFloat<11,53> id920out_value;

  HWFloat<11,53> id919out_value;

  HWFloat<11,53> id23out_value;

  HWFloat<11,53> id26out_value;

  HWFloat<11,53> id737out_floatOut[2];

  HWRawBits<11> id547out_value;

  HWRawBits<52> id918out_value;

  HWOffsetFix<1,0,UNSIGNED> id831out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id470out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id472out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id472out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id475out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id822out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id516out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id730out_value;

  HWOffsetFix<38,-53,UNSIGNED> id562out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id562sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id559out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id559sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id556out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id556sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id493out_value;

  HWOffsetFix<26,-51,UNSIGNED> id823out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id917out_value;

  HWFloat<11,53> id916out_value;

  HWOffsetFix<1,0,UNSIGNED> id825out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id826out_output[3];

  HWFloat<11,53> id915out_value;

  HWOffsetFix<1,0,UNSIGNED> id827out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id828out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id914out_value;

  HWOffsetFix<1,0,UNSIGNED> id526out_value;

  HWOffsetFix<53,-52,UNSIGNED> id510out_value;

  HWOffsetFix<1,0,UNSIGNED> id535out_value;

  HWOffsetFix<11,0,UNSIGNED> id536out_value;

  HWOffsetFix<52,0,UNSIGNED> id538out_value;

  HWFloat<11,53> id687out_value;

  HWFloat<11,53> id913out_value;

  HWFloat<11,53> id912out_value;

  HWFloat<11,53> id738out_floatOut[2];

  HWFloat<11,53> id6out_value;

  HWFloat<11,53> id7out_value;

  HWFloat<11,53> id579out_result[13];

  HWFloat<11,53> id580out_result[15];

  HWFloat<11,53> id13out_value;

  HWOffsetFix<32,0,UNSIGNED> id911out_value;

  HWOffsetFix<1,0,UNSIGNED> id714out_result[2];

  HWFloat<11,53> id836out_output[5];

  HWOffsetFix<32,0,UNSIGNED> id910out_value;

  HWOffsetFix<32,0,UNSIGNED> id632out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id715out_result[2];

  HWFloat<11,53> id671out_doutb[3];

  HWFloat<11,53> id671sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_5 (const char* _format_arg_format_string);
  HWFloat<11,53> id65out_value;

  HWFloat<11,53> id66out_result[2];

  HWFloat<11,53> id837out_output[25];

  HWFloat<11,53> id71out_result[15];

  HWFloat<11,53> id909out_value;

  HWOffsetFix<1,0,UNSIGNED> id83out_value;

  HWOffsetFix<1,0,UNSIGNED> id82out_value;

  HWFloat<11,53> id908out_value;

  HWOffsetFix<1,0,UNSIGNED> id78out_value;

  HWOffsetFix<1,0,UNSIGNED> id77out_value;

  HWFloat<11,53> id86out_value;

  HWFloat<11,53> id85out_value;

  HWFloat<11,53> id88out_value;

  HWFloat<11,53> id907out_value;

  HWOffsetFix<32,0,UNSIGNED> id906out_value;

  HWOffsetFix<1,0,UNSIGNED> id716out_result[2];

  HWFloat<11,53> id840out_output[5];

  HWOffsetFix<32,0,UNSIGNED> id905out_value;

  HWOffsetFix<32,0,UNSIGNED> id636out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id717out_result[2];

  HWFloat<11,53> id672out_doutb[3];

  HWFloat<11,53> id672sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_6 (const char* _format_arg_format_string);
  HWFloat<11,53> id69out_value;

  HWFloat<11,53> id70out_result[2];

  HWFloat<11,53> id72out_result[15];

  HWFloat<11,53> id838out_output[25];

  HWFloat<11,53> id904out_value;

  HWFloat<11,53> id903out_value;

  HWOffsetFix<1,0,UNSIGNED> id100out_value;

  HWOffsetFix<1,0,UNSIGNED> id99out_value;

  HWFloat<11,53> id902out_value;

  HWOffsetFix<1,0,UNSIGNED> id95out_value;

  HWOffsetFix<1,0,UNSIGNED> id94out_value;

  HWFloat<11,53> id103out_value;

  HWFloat<11,53> id102out_value;

  HWFloat<11,53> id105out_value;

  HWFloat<11,53> id901out_value;

  HWFloat<11,53> id900out_value;

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<1,0,UNSIGNED> id119out_value;

  HWFloat<11,53> id899out_value;

  HWOffsetFix<1,0,UNSIGNED> id115out_value;

  HWOffsetFix<1,0,UNSIGNED> id114out_value;

  HWFloat<11,53> id123out_value;

  HWFloat<11,53> id122out_value;

  HWFloat<11,53> id125out_value;

  HWFloat<11,53> id898out_value;

  HWFloat<11,53> id897out_value;

  HWFloat<11,53> id896out_value;

  HWOffsetFix<1,0,UNSIGNED> id137out_value;

  HWOffsetFix<1,0,UNSIGNED> id136out_value;

  HWFloat<11,53> id895out_value;

  HWOffsetFix<1,0,UNSIGNED> id132out_value;

  HWOffsetFix<1,0,UNSIGNED> id131out_value;

  HWFloat<11,53> id140out_value;

  HWFloat<11,53> id139out_value;

  HWFloat<11,53> id142out_value;

  HWFloat<11,53> id894out_value;

  HWFloat<11,53> id893out_value;

  HWOffsetFix<1,0,UNSIGNED> id158out_value;

  HWOffsetFix<1,0,UNSIGNED> id157out_value;

  HWFloat<11,53> id892out_value;

  HWOffsetFix<1,0,UNSIGNED> id153out_value;

  HWOffsetFix<1,0,UNSIGNED> id152out_value;

  HWFloat<11,53> id161out_value;

  HWFloat<11,53> id160out_value;

  HWFloat<11,53> id163out_value;

  HWFloat<11,53> id891out_value;

  HWFloat<11,53> id890out_value;

  HWFloat<11,53> id889out_value;

  HWOffsetFix<1,0,UNSIGNED> id175out_value;

  HWOffsetFix<1,0,UNSIGNED> id174out_value;

  HWFloat<11,53> id888out_value;

  HWOffsetFix<1,0,UNSIGNED> id170out_value;

  HWOffsetFix<1,0,UNSIGNED> id169out_value;

  HWFloat<11,53> id178out_value;

  HWFloat<11,53> id177out_value;

  HWFloat<11,53> id180out_value;

  HWFloat<11,53> id887out_value;

  HWOffsetFix<1,0,UNSIGNED> id187out_result[3];

  HWOffsetFix<32,0,UNSIGNED> id886out_value;

  HWOffsetFix<1,0,UNSIGNED> id718out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id190out_result[2];

  HWFloat<11,53> id192out_value;

  HWFloat<11,53> id191out_value;

  HWFloat<11,53> id193out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id885out_value;

  HWOffsetFix<1,0,UNSIGNED> id719out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id884out_value;

  HWOffsetFix<32,0,UNSIGNED> id210out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id720out_result[2];

  HWFloat<11,53> id37out_ddt_o_dx2;

  HWOffsetFix<32,0,UNSIGNED> id883out_value;

  HWOffsetFix<32,0,UNSIGNED> id197out_result[2];

  HWFloat<11,53> id667out_doutb[3];

  HWFloat<11,53> id667sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_7 (const char* _format_arg_format_string);
  HWFloat<11,53> id199out_value;

  HWFloat<11,53> id200out_result[2];

  HWFloat<11,53> id218out_result[15];

  HWFloat<11,53> id739out_floatOut[2];

  HWFloat<11,53> id221out_result[15];

  HWFloat<11,53> id222out_result[13];

  HWFloat<11,53> id740out_floatOut[2];

  HWFloat<11,53> id851out_output[14];

  HWFloat<11,53> id741out_floatOut[2];

  HWFloat<11,53> id216out_result[15];

  HWFloat<11,53> id217out_result[13];

  HWFloat<11,53> id223out_result[2];

  HWFloat<11,53> id742out_floatOut[2];

  HWFloat<11,53> id853out_output[15];

  HWFloat<11,53> id743out_floatOut[2];

  HWFloat<11,53> id207out_result[15];

  HWFloat<11,53> id208out_result[13];

  HWFloat<11,53> id224out_result[2];

  HWFloat<11,53> id848out_output[5];

  HWFloat<11,53> id874out_output[3];

  HWFloat<11,53> id875out_output[40];

  HWFloat<11,53> id872out_output[30];

  HWFloat<11,53> id873out_output[29];

  HWOffsetFix<32,0,UNSIGNED> id882out_value;

  HWOffsetFix<32,0,UNSIGNED> id639out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id721out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id641out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id857out_output[46];

  HWOffsetFix<32,0,UNSIGNED> id881out_value;

  HWOffsetFix<32,0,UNSIGNED> id646out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id722out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id648out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id858out_output[36];

  HWOffsetFix<32,0,UNSIGNED> id880out_value;

  HWOffsetFix<32,0,UNSIGNED> id653out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id723out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id655out_io_w_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id859out_output[45];

  HWOffsetFix<32,0,UNSIGNED> id879out_value;

  HWOffsetFix<32,0,UNSIGNED> id660out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id724out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id662out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id860out_output[46];

  HWOffsetFix<1,0,UNSIGNED> id677out_value;

  HWOffsetFix<1,0,UNSIGNED> id878out_value;

  HWOffsetFix<49,0,UNSIGNED> id674out_value;

  HWOffsetFix<48,0,UNSIGNED> id675out_count;
  HWOffsetFix<1,0,UNSIGNED> id675out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id675st_count;

  HWOffsetFix<1,0,UNSIGNED> id877out_value;

  HWOffsetFix<49,0,UNSIGNED> id680out_value;

  HWOffsetFix<48,0,UNSIGNED> id681out_count;
  HWOffsetFix<1,0,UNSIGNED> id681out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id681st_count;

  HWOffsetFix<48,0,UNSIGNED> id683out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id725out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_2;
  const HWOffsetFix<20,0,UNSIGNED> c_hw_fix_20_0_uns_undef;
  const HWFloat<11,53> c_hw_flt_11_53_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_1;
  const HWFloat<11,53> c_hw_flt_11_53_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_4;
  const HWFloat<11,53> c_hw_flt_11_53_bits_5;
  const HWFloat<11,53> c_hw_flt_11_53_bits_6;
  const HWFloat<11,53> c_hw_flt_11_53_bits_7;
  const HWFloat<11,53> c_hw_flt_11_53_bits_8;
  const HWFloat<11,53> c_hw_flt_11_53_bits_9;
  const HWFloat<11,53> c_hw_flt_11_53_bits_10;
  const HWFloat<11,53> c_hw_flt_11_53_bits_11;
  const HWFloat<11,53> c_hw_flt_11_53_bits_12;
  const HWFloat<11,53> c_hw_flt_11_53_bits_13;
  const HWFloat<11,53> c_hw_flt_11_53_bits_14;
  const HWFloat<11,53> c_hw_flt_11_53_bits_15;
  const HWFloat<11,53> c_hw_flt_11_53_2_0val;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<52> c_hw_bit_52_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<38,-53,UNSIGNED> c_hw_fix_38_n53_uns_undef;
  const HWOffsetFix<48,-53,UNSIGNED> c_hw_fix_48_n53_uns_undef;
  const HWOffsetFix<53,-53,UNSIGNED> c_hw_fix_53_n53_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_1;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<52,0,UNSIGNED> c_hw_fix_52_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_16;
  const HWFloat<11,53> c_hw_flt_11_53_bits_17;
  const HWFloat<11,53> c_hw_flt_11_53_bits_18;
  const HWFloat<11,53> c_hw_flt_11_53_bits_19;
  const HWFloat<11,53> c_hw_flt_11_53_bits_20;
  const HWFloat<11,53> c_hw_flt_11_53_bits_21;
  const HWFloat<11,53> c_hw_flt_11_53_bits_22;
  const HWFloat<11,53> c_hw_flt_11_53_bits_23;
  const HWFloat<11,53> c_hw_flt_11_53_bits_24;
  const HWFloat<11,53> c_hw_flt_11_53_bits_25;
  const HWFloat<11,53> c_hw_flt_11_53_0_5val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_26;
  const HWFloat<11,53> c_hw_flt_11_53_bits_27;
  const HWFloat<11,53> c_hw_flt_11_53_bits_28;
  const HWFloat<11,53> c_hw_flt_11_53_bits_29;
  const HWFloat<11,53> c_hw_flt_11_53_bits_30;
  const HWFloat<11,53> c_hw_flt_11_53_bits_31;
  const HWFloat<11,53> c_hw_flt_11_53_bits_32;
  const HWFloat<11,53> c_hw_flt_11_53_bits_33;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_34;
  const HWFloat<11,53> c_hw_flt_11_53_n2_0val;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFEKERNEL_H_ */
