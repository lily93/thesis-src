#include "stdsimheader.h"

namespace maxcompilersim {

void CellCableDFEKernel::execute0() {
  { // Node ID: 41 (NodeConstantRawBits)
  }
  { // Node ID: 932 (NodeConstantRawBits)
  }
  { // Node ID: 768 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id768in_input = id932out_value;

    id768out_output[(getCycle()+1)%2] = id768in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 46 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id46in_enable = id41out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id46in_max = id768out_output[getCycle()%2];

    HWOffsetFix<9,0,UNSIGNED> id46x_1;
    HWOffsetFix<1,0,UNSIGNED> id46x_2;
    HWOffsetFix<1,0,UNSIGNED> id46x_3;
    HWOffsetFix<9,0,UNSIGNED> id46x_4t_1e_1;

    id46out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id46st_count)));
    (id46x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id46st_count),(c_hw_fix_9_0_uns_bits_1)));
    (id46x_2) = (gte_fixed((id46x_1),(cast_fixed2fixed<9,0,UNSIGNED,TRUNCATE>(id46in_max))));
    (id46x_3) = (and_fixed((id46x_2),id46in_enable));
    id46out_wrap = (id46x_3);
    if((id46in_enable.getValueAsBool())) {
      if(((id46x_3).getValueAsBool())) {
        (id46st_count) = (c_hw_fix_9_0_uns_bits);
      }
      else {
        (id46x_4t_1e_1) = (id46x_1);
        (id46st_count) = (id46x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 1005 (NodeConstantRawBits)
  }
  { // Node ID: 648 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id648in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id648in_b = id1005out_value;

    id648out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id648in_a,id648in_b));
  }
  { // Node ID: 697 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id697in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id697in_b = id648out_result[getCycle()%2];

    id697out_result[(getCycle()+1)%2] = (eq_fixed(id697in_a,id697in_b));
  }
  { // Node ID: 650 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id651out_result;

  { // Node ID: 651 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id651in_a = id650out_io_u_out_force_disabled;

    id651out_result = (not_fixed(id651in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id652out_result;

  { // Node ID: 652 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id652in_a = id697out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id652in_b = id651out_result;

    HWOffsetFix<1,0,UNSIGNED> id652x_1;

    (id652x_1) = (and_fixed(id652in_a,id652in_b));
    id652out_result = (id652x_1);
  }
  { // Node ID: 902 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id902in_input = id652out_result;

    id902out_output[(getCycle()+252)%253] = id902in_input;
  }
  { // Node ID: 769 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id769in_input = id46out_wrap;

    id769out_output[(getCycle()+1)%2] = id769in_input;
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 45 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id45in_enable = id769out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id45in_max = id38out_nx;

    HWOffsetFix<33,0,UNSIGNED> id45x_1;
    HWOffsetFix<1,0,UNSIGNED> id45x_2;
    HWOffsetFix<1,0,UNSIGNED> id45x_3;
    HWOffsetFix<33,0,UNSIGNED> id45x_4t_1e_1;

    id45out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id45st_count)));
    (id45x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id45st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id45x_2) = (gte_fixed((id45x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id45in_max))));
    (id45x_3) = (and_fixed((id45x_2),id45in_enable));
    id45out_wrap = (id45x_3);
    if((id45in_enable.getValueAsBool())) {
      if(((id45x_3).getValueAsBool())) {
        (id45st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id45x_4t_1e_1) = (id45x_1);
        (id45st_count) = (id45x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 770 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id770in_input = id45out_wrap;

    id770out_output[(getCycle()+1)%2] = id770in_input;
  }
  { // Node ID: 39 (NodeInputMappedReg)
  }
  { // Node ID: 1004 (NodeConstantRawBits)
  }
  { // Node ID: 43 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id43in_a = id39out_simTime;
    const HWOffsetFix<32,0,UNSIGNED> &id43in_b = id1004out_value;

    id43out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id43in_a,id43in_b));
  }
  if ( (getFillLevel() >= (5l)))
  { // Node ID: 44 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id44in_enable = id770out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id44in_max = id43out_result[getCycle()%2];

    HWOffsetFix<33,0,UNSIGNED> id44x_1;
    HWOffsetFix<1,0,UNSIGNED> id44x_2;
    HWOffsetFix<1,0,UNSIGNED> id44x_3;
    HWOffsetFix<33,0,UNSIGNED> id44x_4t_1e_1;

    id44out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id44st_count)));
    (id44x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id44st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id44x_2) = (gte_fixed((id44x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id44in_max))));
    (id44x_3) = (and_fixed((id44x_2),id44in_enable));
    id44out_wrap = (id44x_3);
    if((id44in_enable.getValueAsBool())) {
      if(((id44x_3).getValueAsBool())) {
        (id44st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id44x_4t_1e_1) = (id44x_1);
        (id44st_count) = (id44x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 1003 (NodeConstantRawBits)
  }
  { // Node ID: 698 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id698in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id698in_b = id1003out_value;

    id698out_result[(getCycle()+1)%2] = (eq_fixed(id698in_a,id698in_b));
  }
  { // Node ID: 888 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id888in_input = id45out_count;

    id888out_output[(getCycle()+23)%24] = id888in_input;
  }
  { // Node ID: 906 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id906in_input = id888out_output[getCycle()%24];

    id906out_output[(getCycle()+1)%2] = id906in_input;
  }
  { // Node ID: 907 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id907in_input = id906out_output[getCycle()%2];

    id907out_output[(getCycle()+198)%199] = id907in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id761out_output;

  { // Node ID: 761 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id761in_input = id907out_output[getCycle()%199];

    id761out_output = id761in_input;
  }
  { // Node ID: 772 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id772in_input = id761out_output;

    id772out_output[(getCycle()+30)%31] = id772in_input;
  }
  HWOffsetFix<20,0,UNSIGNED> id623out_o;

  { // Node ID: 623 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id623in_i = id772out_output[getCycle()%31];

    id623out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id623in_i));
  }
  HWFloat<11,53> id762out_output;

  { // Node ID: 762 (NodeStreamOffset)
    const HWFloat<11,53> &id762in_input = id615out_result[getCycle()%8];

    id762out_output = id762in_input;
  }
  { // Node ID: 1002 (NodeConstantRawBits)
  }
  { // Node ID: 625 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id625in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id625in_b = id1002out_value;

    id625out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id625in_a,id625in_b));
  }
  { // Node ID: 699 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id699in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id699in_b = id625out_result[getCycle()%2];

    id699out_result[(getCycle()+1)%2] = (eq_fixed(id699in_a,id699in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id47out_o;

  { // Node ID: 47 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id47in_i = id45out_count;

    id47out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id47in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 675 (NodeRAM)
    const bool id675_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id675in_addrA = id623out_o;
    const HWFloat<11,53> &id675in_dina = id762out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id675in_wea = id699out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id675in_addrB = id47out_o;

    long id675x_1;
    long id675x_2;
    HWFloat<11,53> id675x_3;

    (id675x_1) = (id675in_addrA.getValueAsLong());
    (id675x_2) = (id675in_addrB.getValueAsLong());
    switch(((long)((id675x_2)<(1000000l)))) {
      case 0l:
        id675x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id675x_3 = (id675sta_ram_store[(id675x_2)]);
        break;
      default:
        id675x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id675out_doutb[(getCycle()+2)%3] = (id675x_3);
    if(((id675in_wea.getValueAsBool())&id675_inputvalid)) {
      if(((id675x_1)<(1000000l))) {
        (id675sta_ram_store[(id675x_1)]) = id675in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_1("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 675) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 58 (NodeConstantRawBits)
  }
  { // Node ID: 59 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id59in_sel = id698out_result[getCycle()%2];
    const HWFloat<11,53> &id59in_option0 = id675out_doutb[getCycle()%3];
    const HWFloat<11,53> &id59in_option1 = id58out_value;

    HWFloat<11,53> id59x_1;

    switch((id59in_sel.getValueAsLong())) {
      case 0l:
        id59x_1 = id59in_option0;
        break;
      case 1l:
        id59x_1 = id59in_option1;
        break;
      default:
        id59x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id59out_result[(getCycle()+1)%2] = (id59x_1);
  }
  { // Node ID: 896 (NodeFIFO)
    const HWFloat<11,53> &id896in_input = id59out_result[getCycle()%2];

    id896out_output[(getCycle()+1)%2] = id896in_input;
  }
  { // Node ID: 908 (NodeFIFO)
    const HWFloat<11,53> &id908in_input = id896out_output[getCycle()%2];

    id908out_output[(getCycle()+6)%7] = id908in_input;
  }
  { // Node ID: 909 (NodeFIFO)
    const HWFloat<11,53> &id909in_input = id908out_output[getCycle()%7];

    id909out_output[(getCycle()+30)%31] = id909in_input;
  }
  { // Node ID: 910 (NodeFIFO)
    const HWFloat<11,53> &id910in_input = id909out_output[getCycle()%31];

    id910out_output[(getCycle()+18)%19] = id910in_input;
  }
  { // Node ID: 911 (NodeFIFO)
    const HWFloat<11,53> &id911in_input = id910out_output[getCycle()%19];

    id911out_output[(getCycle()+23)%24] = id911in_input;
  }
  { // Node ID: 912 (NodeFIFO)
    const HWFloat<11,53> &id912in_input = id911out_output[getCycle()%24];

    id912out_output[(getCycle()+34)%35] = id912in_input;
  }
  { // Node ID: 913 (NodeFIFO)
    const HWFloat<11,53> &id913in_input = id912out_output[getCycle()%35];

    id913out_output[(getCycle()+3)%4] = id913in_input;
  }
  { // Node ID: 914 (NodeFIFO)
    const HWFloat<11,53> &id914in_input = id913out_output[getCycle()%4];

    id914out_output[(getCycle()+36)%37] = id914in_input;
  }
  { // Node ID: 915 (NodeFIFO)
    const HWFloat<11,53> &id915in_input = id914out_output[getCycle()%37];

    id915out_output[(getCycle()+9)%10] = id915in_input;
  }
  { // Node ID: 916 (NodeFIFO)
    const HWFloat<11,53> &id916in_input = id915out_output[getCycle()%10];

    id916out_output[(getCycle()+6)%7] = id916in_input;
  }
  { // Node ID: 917 (NodeFIFO)
    const HWFloat<11,53> &id917in_input = id916out_output[getCycle()%7];

    id917out_output[(getCycle()+10)%11] = id917in_input;
  }
  { // Node ID: 918 (NodeFIFO)
    const HWFloat<11,53> &id918in_input = id917out_output[getCycle()%11];

    id918out_output[(getCycle()+3)%4] = id918in_input;
  }
  { // Node ID: 919 (NodeFIFO)
    const HWFloat<11,53> &id919in_input = id918out_output[getCycle()%4];

    id919out_output[(getCycle()+27)%28] = id919in_input;
  }
  { // Node ID: 920 (NodeFIFO)
    const HWFloat<11,53> &id920in_input = id919out_output[getCycle()%28];

    id920out_output[(getCycle()+7)%8] = id920in_input;
  }
  { // Node ID: 921 (NodeFIFO)
    const HWFloat<11,53> &id921in_input = id920out_output[getCycle()%8];

    id921out_output[(getCycle()+22)%23] = id921in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  { // Node ID: 590 (NodeGt)
    const HWFloat<11,53> &id590in_a = id919out_output[getCycle()%28];
    const HWFloat<11,53> &id590in_b = id15out_value;

    id590out_result[(getCycle()+1)%2] = (gt_float(id590in_a,id590in_b));
  }
  { // Node ID: 34 (NodeConstantRawBits)
  }
  { // Node ID: 777 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id777in_input = id698out_result[getCycle()%2];

    id777out_output[(getCycle()+1)%2] = id777in_input;
  }
  HWOffsetFix<20,0,UNSIGNED> id627out_o;

  { // Node ID: 627 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id627in_i = id772out_output[getCycle()%31];

    id627out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id627in_i));
  }
  HWFloat<11,53> id763out_output;

  { // Node ID: 763 (NodeStreamOffset)
    const HWFloat<11,53> &id763in_input = id775out_output[getCycle()%102];

    id763out_output = id763in_input;
  }
  { // Node ID: 776 (NodeFIFO)
    const HWFloat<11,53> &id776in_input = id763out_output;

    id776out_output[(getCycle()+82)%83] = id776in_input;
  }
  { // Node ID: 1001 (NodeConstantRawBits)
  }
  { // Node ID: 629 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id629in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id629in_b = id1001out_value;

    id629out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id629in_a,id629in_b));
  }
  { // Node ID: 700 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id700in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id700in_b = id629out_result[getCycle()%2];

    id700out_result[(getCycle()+1)%2] = (eq_fixed(id700in_a,id700in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id48out_o;

  { // Node ID: 48 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id48in_i = id45out_count;

    id48out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id48in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 678 (NodeRAM)
    const bool id678_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id678in_addrA = id627out_o;
    const HWFloat<11,53> &id678in_dina = id776out_output[getCycle()%83];
    const HWOffsetFix<1,0,UNSIGNED> &id678in_wea = id700out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id678in_addrB = id48out_o;

    long id678x_1;
    long id678x_2;
    HWFloat<11,53> id678x_3;

    (id678x_1) = (id678in_addrA.getValueAsLong());
    (id678x_2) = (id678in_addrB.getValueAsLong());
    switch(((long)((id678x_2)<(1000000l)))) {
      case 0l:
        id678x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id678x_3 = (id678sta_ram_store[(id678x_2)]);
        break;
      default:
        id678x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id678out_doutb[(getCycle()+2)%3] = (id678x_3);
    if(((id678in_wea.getValueAsBool())&id678_inputvalid)) {
      if(((id678x_1)<(1000000l))) {
        (id678sta_ram_store[(id678x_1)]) = id678in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_2("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 678) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 778 (NodeFIFO)
    const HWFloat<11,53> &id778in_input = id678out_doutb[getCycle()%3];

    id778out_output[(getCycle()+1)%2] = id778in_input;
  }
  { // Node ID: 60 (NodeConstantRawBits)
  }
  { // Node ID: 61 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id61in_sel = id777out_output[getCycle()%2];
    const HWFloat<11,53> &id61in_option0 = id778out_output[getCycle()%2];
    const HWFloat<11,53> &id61in_option1 = id60out_value;

    HWFloat<11,53> id61x_1;

    switch((id61in_sel.getValueAsLong())) {
      case 0l:
        id61x_1 = id61in_option0;
        break;
      case 1l:
        id61x_1 = id61in_option1;
        break;
      default:
        id61x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id61out_result[(getCycle()+1)%2] = (id61x_1);
  }
  { // Node ID: 780 (NodeFIFO)
    const HWFloat<11,53> &id780in_input = id61out_result[getCycle()%2];

    id780out_output[(getCycle()+2)%3] = id780in_input;
  }
  { // Node ID: 922 (NodeFIFO)
    const HWFloat<11,53> &id922in_input = id780out_output[getCycle()%3];

    id922out_output[(getCycle()+7)%8] = id922in_input;
  }
  { // Node ID: 923 (NodeFIFO)
    const HWFloat<11,53> &id923in_input = id922out_output[getCycle()%8];

    id923out_output[(getCycle()+42)%43] = id923in_input;
  }
  { // Node ID: 460 (NodeGt)
    const HWFloat<11,53> &id460in_a = id909out_output[getCycle()%31];
    const HWFloat<11,53> &id460in_b = id15out_value;

    id460out_result[(getCycle()+1)%2] = (gt_float(id460in_a,id460in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  { // Node ID: 449 (NodeGt)
    const HWFloat<11,53> &id449in_a = id59out_result[getCycle()%2];
    const HWFloat<11,53> &id449in_b = id17out_value;

    id449out_result[(getCycle()+1)%2] = (gt_float(id449in_a,id449in_b));
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  { // Node ID: 450 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id450in_sel = id449out_result[getCycle()%2];
    const HWFloat<11,53> &id450in_option0 = id35out_value;
    const HWFloat<11,53> &id450in_option1 = id34out_value;

    HWFloat<11,53> id450x_1;

    switch((id450in_sel.getValueAsLong())) {
      case 0l:
        id450x_1 = id450in_option0;
        break;
      case 1l:
        id450x_1 = id450in_option1;
        break;
      default:
        id450x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id450out_result[(getCycle()+1)%2] = (id450x_1);
  }
  { // Node ID: 458 (NodeRegister)
    const HWFloat<11,53> &id458in_input = id450out_result[getCycle()%2];

    id458out_output[(getCycle()+1)%2] = id458in_input;
  }
  { // Node ID: 463 (NodeSub)
    const HWFloat<11,53> &id463in_a = id458out_output[getCycle()%2];
    const HWFloat<11,53> &id463in_b = id780out_output[getCycle()%3];

    id463out_result[(getCycle()+7)%8] = (sub_float(id463in_a,id463in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  { // Node ID: 224 (NodeGt)
    const HWFloat<11,53> &id224in_a = id908out_output[getCycle()%7];
    const HWFloat<11,53> &id224in_b = id16out_value;

    id224out_result[(getCycle()+1)%2] = (gt_float(id224in_a,id224in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  { // Node ID: 225 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id225in_sel = id224out_result[getCycle()%2];
    const HWFloat<11,53> &id225in_option0 = id1out_value;
    const HWFloat<11,53> &id225in_option1 = id2out_value;

    HWFloat<11,53> id225x_1;

    switch((id225in_sel.getValueAsLong())) {
      case 0l:
        id225x_1 = id225in_option0;
        break;
      case 1l:
        id225x_1 = id225in_option1;
        break;
      default:
        id225x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id225out_result[(getCycle()+1)%2] = (id225x_1);
  }
  { // Node ID: 444 (NodeRegister)
    const HWFloat<11,53> &id444in_input = id225out_result[getCycle()%2];

    id444out_output[(getCycle()+1)%2] = id444in_input;
  }
  { // Node ID: 464 (NodeDiv)
    const HWFloat<11,53> &id464in_a = id463out_result[getCycle()%8];
    const HWFloat<11,53> &id464in_b = id444out_output[getCycle()%2];

    id464out_result[(getCycle()+28)%29] = (div_float(id464in_a,id464in_b));
  }
  HWFloat<11,53> id461out_result;

  { // Node ID: 461 (NodeNeg)
    const HWFloat<11,53> &id461in_a = id922out_output[getCycle()%8];

    id461out_result = (neg_float(id461in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  { // Node ID: 462 (NodeDiv)
    const HWFloat<11,53> &id462in_a = id461out_result;
    const HWFloat<11,53> &id462in_b = id0out_value;

    id462out_result[(getCycle()+28)%29] = (div_float(id462in_a,id462in_b));
  }
  { // Node ID: 465 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id465in_sel = id460out_result[getCycle()%2];
    const HWFloat<11,53> &id465in_option0 = id464out_result[getCycle()%29];
    const HWFloat<11,53> &id465in_option1 = id462out_result[getCycle()%29];

    HWFloat<11,53> id465x_1;

    switch((id465in_sel.getValueAsLong())) {
      case 0l:
        id465x_1 = id465in_option0;
        break;
      case 1l:
        id465x_1 = id465in_option1;
        break;
      default:
        id465x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id465out_result[(getCycle()+1)%2] = (id465x_1);
  }
  { // Node ID: 581 (NodeRegister)
    const HWFloat<11,53> &id581in_input = id465out_result[getCycle()%2];

    id581out_output[(getCycle()+1)%2] = id581in_input;
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 584 (NodeMul)
    const HWFloat<11,53> &id584in_a = id581out_output[getCycle()%2];
    const HWFloat<11,53> &id584in_b = id36out_dt;

    id584out_result[(getCycle()+12)%13] = (mul_float(id584in_a,id584in_b));
  }
  { // Node ID: 585 (NodeAdd)
    const HWFloat<11,53> &id585in_a = id923out_output[getCycle()%43];
    const HWFloat<11,53> &id585in_b = id584out_result[getCycle()%13];

    id585out_result[(getCycle()+14)%15] = (add_float(id585in_a,id585in_b));
  }
  { // Node ID: 775 (NodeFIFO)
    const HWFloat<11,53> &id775in_input = id585out_result[getCycle()%15];

    id775out_output[(getCycle()+101)%102] = id775in_input;
  }
  HWFloat<11,53> id591out_result;

  { // Node ID: 591 (NodeNeg)
    const HWFloat<11,53> &id591in_a = id775out_output[getCycle()%102];

    id591out_result = (neg_float(id591in_a));
  }
  { // Node ID: 592 (NodeSub)
    const HWFloat<11,53> &id592in_a = id915out_output[getCycle()%10];
    const HWFloat<11,53> &id592in_b = id15out_value;

    id592out_result[(getCycle()+7)%8] = (sub_float(id592in_a,id592in_b));
  }
  { // Node ID: 593 (NodeMul)
    const HWFloat<11,53> &id593in_a = id591out_result;
    const HWFloat<11,53> &id593in_b = id592out_result[getCycle()%8];

    id593out_result[(getCycle()+6)%7] = (mul_float(id593in_a,id593in_b));
  }
  { // Node ID: 28 (NodeConstantRawBits)
  }
  { // Node ID: 594 (NodeSub)
    const HWFloat<11,53> &id594in_a = id28out_value;
    const HWFloat<11,53> &id594in_b = id916out_output[getCycle()%7];

    id594out_result[(getCycle()+7)%8] = (sub_float(id594in_a,id594in_b));
  }
  { // Node ID: 595 (NodeMul)
    const HWFloat<11,53> &id595in_a = id593out_result[getCycle()%7];
    const HWFloat<11,53> &id595in_b = id594out_result[getCycle()%8];

    id595out_result[(getCycle()+6)%7] = (mul_float(id595in_a,id595in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 596 (NodeDiv)
    const HWFloat<11,53> &id596in_a = id595out_result[getCycle()%7];
    const HWFloat<11,53> &id596in_b = id8out_value;

    id596out_result[(getCycle()+28)%29] = (div_float(id596in_a,id596in_b));
  }
  { // Node ID: 597 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id597in_sel = id590out_result[getCycle()%2];
    const HWFloat<11,53> &id597in_option0 = id34out_value;
    const HWFloat<11,53> &id597in_option1 = id596out_result[getCycle()%29];

    HWFloat<11,53> id597x_1;

    switch((id597in_sel.getValueAsLong())) {
      case 0l:
        id597x_1 = id597in_option0;
        break;
      case 1l:
        id597x_1 = id597in_option1;
        break;
      default:
        id597x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id597out_result[(getCycle()+1)%2] = (id597x_1);
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  { // Node ID: 598 (NodeGt)
    const HWFloat<11,53> &id598in_a = id919out_output[getCycle()%28];
    const HWFloat<11,53> &id598in_b = id19out_value;

    id598out_result[(getCycle()+1)%2] = (gt_float(id598in_a,id598in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  { // Node ID: 442 (NodeGt)
    const HWFloat<11,53> &id442in_a = id917out_output[getCycle()%11];
    const HWFloat<11,53> &id442in_b = id21out_value;

    id442out_result[(getCycle()+1)%2] = (gt_float(id442in_a,id442in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  { // Node ID: 443 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_sel = id442out_result[getCycle()%2];
    const HWFloat<11,53> &id443in_option0 = id9out_value;
    const HWFloat<11,53> &id443in_option1 = id10out_value;

    HWFloat<11,53> id443x_1;

    switch((id443in_sel.getValueAsLong())) {
      case 0l:
        id443x_1 = id443in_option0;
        break;
      case 1l:
        id443x_1 = id443in_option1;
        break;
      default:
        id443x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id443out_result[(getCycle()+1)%2] = (id443x_1);
  }
  { // Node ID: 448 (NodeRegister)
    const HWFloat<11,53> &id448in_input = id443out_result[getCycle()%2];

    id448out_output[(getCycle()+1)%2] = id448in_input;
  }
  { // Node ID: 602 (NodeDiv)
    const HWFloat<11,53> &id602in_a = id918out_output[getCycle()%4];
    const HWFloat<11,53> &id602in_b = id448out_output[getCycle()%2];

    id602out_result[(getCycle()+28)%29] = (div_float(id602in_a,id602in_b));
  }
  { // Node ID: 1000 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 31 (NodeConstantRawBits)
  }
  { // Node ID: 999 (NodeConstantRawBits)
  }
  { // Node ID: 998 (NodeConstantRawBits)
  }
  { // Node ID: 997 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  { // Node ID: 333 (NodeSub)
    const HWFloat<11,53> &id333in_a = id910out_output[getCycle()%19];
    const HWFloat<11,53> &id333in_b = id29out_value;

    id333out_result[(getCycle()+7)%8] = (sub_float(id333in_a,id333in_b));
  }
  { // Node ID: 334 (NodeMul)
    const HWFloat<11,53> &id334in_a = id24out_value;
    const HWFloat<11,53> &id334in_b = id333out_result[getCycle()%8];

    id334out_result[(getCycle()+6)%7] = (mul_float(id334in_a,id334in_b));
  }
  { // Node ID: 744 (NodePO2FPMult)
    const HWFloat<11,53> &id744in_floatIn = id334out_result[getCycle()%7];

    id744out_floatOut[(getCycle()+1)%2] = (mul_float(id744in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id413out_result;

  { // Node ID: 413 (NodeSlice)
    const HWFloat<11,53> &id413in_a = id744out_floatOut[getCycle()%2];

    id413out_result = (slice<52,11>(id413in_a));
  }
  { // Node ID: 414 (NodeConstantRawBits)
  }
  { // Node ID: 701 (NodeEqInlined)
    const HWRawBits<11> &id701in_a = id413out_result;
    const HWRawBits<11> &id701in_b = id414out_value;

    id701out_result[(getCycle()+1)%2] = (eq_bits(id701in_a,id701in_b));
  }
  HWRawBits<52> id412out_result;

  { // Node ID: 412 (NodeSlice)
    const HWFloat<11,53> &id412in_a = id744out_floatOut[getCycle()%2];

    id412out_result = (slice<0,52>(id412in_a));
  }
  { // Node ID: 996 (NodeConstantRawBits)
  }
  { // Node ID: 702 (NodeNeqInlined)
    const HWRawBits<52> &id702in_a = id412out_result;
    const HWRawBits<52> &id702in_b = id996out_value;

    id702out_result[(getCycle()+1)%2] = (neq_bits(id702in_a,id702in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id418out_result;

  { // Node ID: 418 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id418in_a = id701out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id418in_b = id702out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id418x_1;

    (id418x_1) = (and_fixed(id418in_a,id418in_b));
    id418out_result = (id418x_1);
  }
  { // Node ID: 809 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id809in_input = id418out_result;

    id809out_output[(getCycle()+45)%46] = id809in_input;
  }
  { // Node ID: 337 (NodeConstantRawBits)
  }
  HWFloat<11,53> id338out_output;
  HWOffsetFix<1,0,UNSIGNED> id338out_output_doubt;

  { // Node ID: 338 (NodeDoubtBitOp)
    const HWFloat<11,53> &id338in_input = id744out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id338in_doubt = id337out_value;

    id338out_output = id338in_input;
    id338out_output_doubt = id338in_doubt;
  }
  { // Node ID: 339 (NodeCast)
    const HWFloat<11,53> &id339in_i = id338out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id339in_i_doubt = id338out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id339x_1;

    id339out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id339in_i,(&(id339x_1))));
    id339out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id339x_1),(c_hw_fix_4_0_uns_bits))),id339in_i_doubt));
  }
  { // Node ID: 342 (NodeConstantRawBits)
  }
  { // Node ID: 341 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id341in_a = id339out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id341in_a_doubt = id339out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id341in_b = id342out_value;

    HWOffsetFix<1,0,UNSIGNED> id341x_1;

    id341out_result[(getCycle()+7)%8] = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id341in_a,id341in_b,(&(id341x_1))));
    id341out_result_doubt[(getCycle()+7)%8] = (or_fixed((neq_fixed((id341x_1),(c_hw_fix_1_0_uns_bits_1))),id341in_a_doubt));
  }
  { // Node ID: 343 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id343in_i = id341out_result[getCycle()%8];
    const HWOffsetFix<1,0,UNSIGNED> &id343in_i_doubt = id341out_result_doubt[getCycle()%8];

    HWOffsetFix<1,0,UNSIGNED> id343x_1;

    id343out_o[(getCycle()+1)%2] = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id343in_i,(&(id343x_1))));
    id343out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id343x_1),(c_hw_fix_1_0_uns_bits_1))),id343in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id352out_output;

  { // Node ID: 352 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id352in_input = id343out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id352in_input_doubt = id343out_o_doubt[getCycle()%2];

    id352out_output = id352in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id353out_o;

  { // Node ID: 353 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id353in_i = id352out_output;

    id353out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id353in_i));
  }
  { // Node ID: 791 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id791in_input = id353out_o;

    id791out_output[(getCycle()+29)%30] = id791in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id380out_o;

  { // Node ID: 380 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id380in_i = id791out_output[getCycle()%30];

    id380out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id380in_i));
  }
  { // Node ID: 383 (NodeConstantRawBits)
  }
  { // Node ID: 735 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id356out_o;

  { // Node ID: 356 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id356in_i = id352out_output;

    id356out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id356in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id428out_output;

  { // Node ID: 428 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id428in_input = id356out_o;

    id428out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id428in_input))));
  }
  { // Node ID: 792 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id792in_input = id428out_output;

    id792out_output[(getCycle()+17)%18] = id792in_input;
  }
  { // Node ID: 429 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id429in_addr = id792out_output[getCycle()%18];

    HWOffsetFix<38,-53,UNSIGNED> id429x_1;

    switch(((long)((id429in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id429x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id429x_1 = (id429sta_rom_store[(id429in_addr.getValueAsLong())]);
        break;
      default:
        id429x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id429out_dout[(getCycle()+2)%3] = (id429x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id355out_o;

  { // Node ID: 355 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id355in_i = id352out_output;

    id355out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id355in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id425out_output;

  { // Node ID: 425 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id425in_input = id355out_o;

    id425out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id425in_input))));
  }
  { // Node ID: 793 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id793in_input = id425out_output;

    id793out_output[(getCycle()+8)%9] = id793in_input;
  }
  { // Node ID: 426 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id426in_addr = id793out_output[getCycle()%9];

    HWOffsetFix<48,-53,UNSIGNED> id426x_1;

    switch(((long)((id426in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id426x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id426x_1 = (id426sta_rom_store[(id426in_addr.getValueAsLong())]);
        break;
      default:
        id426x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id426out_dout[(getCycle()+2)%3] = (id426x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id354out_o;

  { // Node ID: 354 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id354in_i = id352out_output;

    id354out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id354in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id422out_output;

  { // Node ID: 422 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id422in_input = id354out_o;

    id422out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id422in_input))));
  }
  { // Node ID: 794 (NodeFIFO)
    const HWOffsetFix<5,0,UNSIGNED> &id794in_input = id422out_output;

    id794out_output[(getCycle()+1)%2] = id794in_input;
  }
  { // Node ID: 423 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id423in_addr = id794out_output[getCycle()%2];

    HWOffsetFix<53,-53,UNSIGNED> id423x_1;

    switch(((long)((id423in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id423x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id423x_1 = (id423sta_rom_store[(id423in_addr.getValueAsLong())]);
        break;
      default:
        id423x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id423out_dout[(getCycle()+2)%3] = (id423x_1);
  }
  { // Node ID: 360 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id357out_o;

  { // Node ID: 357 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id357in_i = id352out_output;

    id357out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id357in_i));
  }
  { // Node ID: 359 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id359in_a = id360out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id359in_b = id357out_o;

    id359out_result[(getCycle()+2)%3] = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id359in_a,id359in_b));
  }
  { // Node ID: 361 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id361in_i = id359out_result[getCycle()%3];

    id361out_o[(getCycle()+1)%2] = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id361in_i));
  }
  { // Node ID: 362 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id362in_a = id423out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id362in_b = id361out_o[getCycle()%2];

    id362out_result[(getCycle()+1)%2] = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id362in_a,id362in_b));
  }
  { // Node ID: 795 (NodeFIFO)
    const HWOffsetFix<54,-53,UNSIGNED> &id795in_input = id362out_result[getCycle()%2];

    id795out_output[(getCycle()+3)%4] = id795in_input;
  }
  { // Node ID: 363 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id363in_a = id361out_o[getCycle()%2];
    const HWOffsetFix<53,-53,UNSIGNED> &id363in_b = id423out_dout[getCycle()%3];

    id363out_result[(getCycle()+4)%5] = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id363in_a,id363in_b));
  }
  { // Node ID: 364 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id364in_a = id795out_output[getCycle()%4];
    const HWOffsetFix<64,-89,UNSIGNED> &id364in_b = id363out_result[getCycle()%5];

    id364out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id364in_a,id364in_b));
  }
  { // Node ID: 365 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id365in_i = id364out_result[getCycle()%3];

    id365out_o[(getCycle()+1)%2] = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id365in_i));
  }
  { // Node ID: 366 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id366in_a = id426out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id366in_b = id365out_o[getCycle()%2];

    id366out_result[(getCycle()+1)%2] = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id366in_a,id366in_b));
  }
  { // Node ID: 796 (NodeFIFO)
    const HWOffsetFix<59,-57,UNSIGNED> &id796in_input = id366out_result[getCycle()%2];

    id796out_output[(getCycle()+5)%6] = id796in_input;
  }
  { // Node ID: 367 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id367in_a = id365out_o[getCycle()%2];
    const HWOffsetFix<48,-53,UNSIGNED> &id367in_b = id426out_dout[getCycle()%3];

    id367out_result[(getCycle()+6)%7] = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id367in_a,id367in_b));
  }
  { // Node ID: 368 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id368in_a = id796out_output[getCycle()%6];
    const HWOffsetFix<64,-68,UNSIGNED> &id368in_b = id367out_result[getCycle()%7];

    id368out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id368in_a,id368in_b));
  }
  { // Node ID: 369 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id369in_i = id368out_result[getCycle()%3];

    id369out_o[(getCycle()+1)%2] = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id369in_i));
  }
  { // Node ID: 370 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id370in_a = id429out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id370in_b = id369out_o[getCycle()%2];

    id370out_result[(getCycle()+1)%2] = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id370in_a,id370in_b));
  }
  { // Node ID: 797 (NodeFIFO)
    const HWOffsetFix<60,-57,UNSIGNED> &id797in_input = id370out_result[getCycle()%2];

    id797out_output[(getCycle()+4)%5] = id797in_input;
  }
  { // Node ID: 371 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id371in_a = id369out_o[getCycle()%2];
    const HWOffsetFix<38,-53,UNSIGNED> &id371in_b = id429out_dout[getCycle()%3];

    id371out_result[(getCycle()+5)%6] = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id371in_a,id371in_b));
  }
  { // Node ID: 372 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id372in_a = id797out_output[getCycle()%5];
    const HWOffsetFix<64,-77,UNSIGNED> &id372in_b = id371out_result[getCycle()%6];

    id372out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id372in_a,id372in_b));
  }
  { // Node ID: 373 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id373in_i = id372out_result[getCycle()%3];

    id373out_o[(getCycle()+1)%2] = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id373in_i));
  }
  { // Node ID: 374 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id374in_i = id373out_o[getCycle()%2];

    id374out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id374in_i));
  }
  { // Node ID: 995 (NodeConstantRawBits)
  }
  { // Node ID: 703 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id703in_a = id374out_o[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id703in_b = id995out_value;

    id703out_result[(getCycle()+1)%2] = (gte_fixed(id703in_a,id703in_b));
  }
  { // Node ID: 743 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id743in_a = id380out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id743in_b = id383out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id743in_c = id735out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id743in_condb = id703out_result[getCycle()%2];

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id743x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id743x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id743x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id743x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id743x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id743x_1 = id743in_a;
        break;
      default:
        id743x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id743in_condb.getValueAsLong())) {
      case 0l:
        id743x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id743x_2 = id743in_b;
        break;
      default:
        id743x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id743x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id743x_3 = id743in_c;
        break;
      default:
        id743x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id743x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id743x_1),(id743x_2))),(id743x_3)));
    id743out_result[(getCycle()+1)%2] = (id743x_4);
  }
  HWRawBits<1> id704out_result;

  { // Node ID: 704 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id704in_a = id743out_result[getCycle()%2];

    id704out_result = (slice<13,1>(id704in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id705out_output;

  { // Node ID: 705 (NodeReinterpret)
    const HWRawBits<1> &id705in_input = id704out_result;

    id705out_output = (cast_bits2fixed<1,0,UNSIGNED>(id705in_input));
  }
  { // Node ID: 994 (NodeConstantRawBits)
  }
  { // Node ID: 345 (NodeGt)
    const HWFloat<11,53> &id345in_a = id744out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id345in_b = id994out_value;

    id345out_result[(getCycle()+1)%2] = (gt_float(id345in_a,id345in_b));
  }
  { // Node ID: 798 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id798in_input = id345out_result[getCycle()%2];

    id798out_output[(getCycle()+13)%14] = id798in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id346out_output;

  { // Node ID: 346 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id346in_input = id343out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id346in_input_doubt = id343out_o_doubt[getCycle()%2];

    id346out_output = id346in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id347out_result;

  { // Node ID: 347 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id347in_a = id798out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id347in_b = id346out_output;

    HWOffsetFix<1,0,UNSIGNED> id347x_1;

    (id347x_1) = (and_fixed(id347in_a,id347in_b));
    id347out_result = (id347x_1);
  }
  { // Node ID: 799 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id799in_input = id347out_result;

    id799out_output[(getCycle()+30)%31] = id799in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id389out_result;

  { // Node ID: 389 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id389in_a = id799out_output[getCycle()%31];

    id389out_result = (not_fixed(id389in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id390out_result;

  { // Node ID: 390 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id390in_a = id705out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id390in_b = id389out_result;

    HWOffsetFix<1,0,UNSIGNED> id390x_1;

    (id390x_1) = (and_fixed(id390in_a,id390in_b));
    id390out_result = (id390x_1);
  }
  { // Node ID: 993 (NodeConstantRawBits)
  }
  { // Node ID: 349 (NodeLt)
    const HWFloat<11,53> &id349in_a = id744out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id349in_b = id993out_value;

    id349out_result[(getCycle()+1)%2] = (lt_float(id349in_a,id349in_b));
  }
  { // Node ID: 800 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id800in_input = id349out_result[getCycle()%2];

    id800out_output[(getCycle()+13)%14] = id800in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id350out_output;

  { // Node ID: 350 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id350in_input = id343out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id350in_input_doubt = id343out_o_doubt[getCycle()%2];

    id350out_output = id350in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id351out_result;

  { // Node ID: 351 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id351in_a = id800out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id351in_b = id350out_output;

    HWOffsetFix<1,0,UNSIGNED> id351x_1;

    (id351x_1) = (and_fixed(id351in_a,id351in_b));
    id351out_result = (id351x_1);
  }
  { // Node ID: 801 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id801in_input = id351out_result;

    id801out_output[(getCycle()+30)%31] = id801in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id391out_result;

  { // Node ID: 391 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id391in_a = id390out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id391in_b = id801out_output[getCycle()%31];

    HWOffsetFix<1,0,UNSIGNED> id391x_1;

    (id391x_1) = (or_fixed(id391in_a,id391in_b));
    id391out_result = (id391x_1);
  }
  { // Node ID: 805 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id805in_input = id391out_result;

    id805out_output[(getCycle()+1)%2] = id805in_input;
  }
  { // Node ID: 992 (NodeConstantRawBits)
  }
  { // Node ID: 706 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id706in_a = id743out_result[getCycle()%2];
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id706in_b = id992out_value;

    id706out_result[(getCycle()+1)%2] = (gte_fixed(id706in_a,id706in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id398out_result;

  { // Node ID: 398 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id398in_a = id801out_output[getCycle()%31];

    id398out_result = (not_fixed(id398in_a));
  }
  { // Node ID: 803 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id803in_input = id398out_result;

    id803out_output[(getCycle()+1)%2] = id803in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id399out_result;

  { // Node ID: 399 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_a = id706out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id399in_b = id803out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id399x_1;

    (id399x_1) = (and_fixed(id399in_a,id399in_b));
    id399out_result = (id399x_1);
  }
  { // Node ID: 924 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id924in_input = id799out_output[getCycle()%31];

    id924out_output[(getCycle()+1)%2] = id924in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id400out_result;

  { // Node ID: 400 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id399out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id400in_b = id924out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id400x_1;

    (id400x_1) = (or_fixed(id400in_a,id400in_b));
    id400out_result = (id400x_1);
  }
  HWRawBits<2> id401out_result;

  { // Node ID: 401 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_in0 = id805out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id401in_in1 = id400out_result;

    id401out_result = (cat(id401in_in0,id401in_in1));
  }
  { // Node ID: 393 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id392out_o;

  { // Node ID: 392 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id392in_i = id743out_result[getCycle()%2];

    id392out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id392in_i));
  }
  { // Node ID: 807 (NodeFIFO)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id807in_input = id392out_o;

    id807out_output[(getCycle()+1)%2] = id807in_input;
  }
  { // Node ID: 806 (NodeFIFO)
    const HWOffsetFix<53,-52,UNSIGNED> &id806in_input = id374out_o[getCycle()%2];

    id806out_output[(getCycle()+1)%2] = id806in_input;
  }
  { // Node ID: 377 (NodeConstantRawBits)
  }
  { // Node ID: 378 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id378in_sel = id703out_result[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id378in_option0 = id806out_output[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id378in_option1 = id377out_value;

    HWOffsetFix<53,-52,UNSIGNED> id378x_1;

    switch((id378in_sel.getValueAsLong())) {
      case 0l:
        id378x_1 = id378in_option0;
        break;
      case 1l:
        id378x_1 = id378in_option1;
        break;
      default:
        id378x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id378out_result[(getCycle()+1)%2] = (id378x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id379out_o;

  { // Node ID: 379 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id379in_i = id378out_result[getCycle()%2];

    id379out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id379in_i));
  }
  { // Node ID: 808 (NodeFIFO)
    const HWOffsetFix<52,-52,UNSIGNED> &id808in_input = id379out_o;

    id808out_output[(getCycle()+1)%2] = id808in_input;
  }
  HWRawBits<64> id394out_result;

  { // Node ID: 394 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id394in_in0 = id393out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id394in_in1 = id807out_output[getCycle()%2];
    const HWOffsetFix<52,-52,UNSIGNED> &id394in_in2 = id808out_output[getCycle()%2];

    id394out_result = (cat((cat(id394in_in0,id394in_in1)),id394in_in2));
  }
  HWFloat<11,53> id395out_output;

  { // Node ID: 395 (NodeReinterpret)
    const HWRawBits<64> &id395in_input = id394out_result;

    id395out_output = (cast_bits2float<11,53>(id395in_input));
  }
  { // Node ID: 402 (NodeConstantRawBits)
  }
  { // Node ID: 403 (NodeConstantRawBits)
  }
  { // Node ID: 405 (NodeConstantRawBits)
  }
  HWRawBits<64> id707out_result;

  { // Node ID: 707 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id707in_in0 = id402out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id707in_in1 = id403out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id707in_in2 = id405out_value;

    id707out_result = (cat((cat(id707in_in0,id707in_in1)),id707in_in2));
  }
  HWFloat<11,53> id407out_output;

  { // Node ID: 407 (NodeReinterpret)
    const HWRawBits<64> &id407in_input = id707out_result;

    id407out_output = (cast_bits2float<11,53>(id407in_input));
  }
  { // Node ID: 694 (NodeConstantRawBits)
  }
  { // Node ID: 410 (NodeMux)
    const HWRawBits<2> &id410in_sel = id401out_result;
    const HWFloat<11,53> &id410in_option0 = id395out_output;
    const HWFloat<11,53> &id410in_option1 = id407out_output;
    const HWFloat<11,53> &id410in_option2 = id694out_value;
    const HWFloat<11,53> &id410in_option3 = id407out_output;

    HWFloat<11,53> id410x_1;

    switch((id410in_sel.getValueAsLong())) {
      case 0l:
        id410x_1 = id410in_option0;
        break;
      case 1l:
        id410x_1 = id410in_option1;
        break;
      case 2l:
        id410x_1 = id410in_option2;
        break;
      case 3l:
        id410x_1 = id410in_option3;
        break;
      default:
        id410x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id410out_result[(getCycle()+1)%2] = (id410x_1);
  }
  { // Node ID: 991 (NodeConstantRawBits)
  }
  { // Node ID: 420 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id420in_sel = id809out_output[getCycle()%46];
    const HWFloat<11,53> &id420in_option0 = id410out_result[getCycle()%2];
    const HWFloat<11,53> &id420in_option1 = id991out_value;

    HWFloat<11,53> id420x_1;

    switch((id420in_sel.getValueAsLong())) {
      case 0l:
        id420x_1 = id420in_option0;
        break;
      case 1l:
        id420x_1 = id420in_option1;
        break;
      default:
        id420x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id420out_result[(getCycle()+1)%2] = (id420x_1);
  }
  { // Node ID: 990 (NodeConstantRawBits)
  }
  { // Node ID: 431 (NodeAdd)
    const HWFloat<11,53> &id431in_a = id420out_result[getCycle()%2];
    const HWFloat<11,53> &id431in_b = id990out_value;

    id431out_result[(getCycle()+7)%8] = (add_float(id431in_a,id431in_b));
  }
  { // Node ID: 433 (NodeDiv)
    const HWFloat<11,53> &id433in_a = id997out_value;
    const HWFloat<11,53> &id433in_b = id431out_result[getCycle()%8];

    id433out_result[(getCycle()+28)%29] = (div_float(id433in_a,id433in_b));
  }
  { // Node ID: 435 (NodeSub)
    const HWFloat<11,53> &id435in_a = id998out_value;
    const HWFloat<11,53> &id435in_b = id433out_result[getCycle()%29];

    id435out_result[(getCycle()+7)%8] = (sub_float(id435in_a,id435in_b));
  }
  { // Node ID: 437 (NodeAdd)
    const HWFloat<11,53> &id437in_a = id999out_value;
    const HWFloat<11,53> &id437in_b = id435out_result[getCycle()%8];

    id437out_result[(getCycle()+7)%8] = (add_float(id437in_a,id437in_b));
  }
  { // Node ID: 438 (NodeMul)
    const HWFloat<11,53> &id438in_a = id31out_value;
    const HWFloat<11,53> &id438in_b = id437out_result[getCycle()%8];

    id438out_result[(getCycle()+6)%7] = (mul_float(id438in_a,id438in_b));
  }
  { // Node ID: 439 (NodeAdd)
    const HWFloat<11,53> &id439in_a = id11out_value;
    const HWFloat<11,53> &id439in_b = id438out_result[getCycle()%7];

    id439out_result[(getCycle()+7)%8] = (add_float(id439in_a,id439in_b));
  }
  { // Node ID: 446 (NodeRegister)
    const HWFloat<11,53> &id446in_input = id439out_result[getCycle()%8];

    id446out_output[(getCycle()+1)%2] = id446in_input;
  }
  { // Node ID: 600 (NodeDiv)
    const HWFloat<11,53> &id600in_a = id1000out_value;
    const HWFloat<11,53> &id600in_b = id446out_output[getCycle()%2];

    id600out_result[(getCycle()+28)%29] = (div_float(id600in_a,id600in_b));
  }
  { // Node ID: 603 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id603in_sel = id598out_result[getCycle()%2];
    const HWFloat<11,53> &id603in_option0 = id602out_result[getCycle()%29];
    const HWFloat<11,53> &id603in_option1 = id600out_result[getCycle()%29];

    HWFloat<11,53> id603x_1;

    switch((id603in_sel.getValueAsLong())) {
      case 0l:
        id603x_1 = id603in_option0;
        break;
      case 1l:
        id603x_1 = id603in_option1;
        break;
      default:
        id603x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id603out_result[(getCycle()+1)%2] = (id603x_1);
  }
  { // Node ID: 610 (NodeAdd)
    const HWFloat<11,53> &id610in_a = id597out_result[getCycle()%2];
    const HWFloat<11,53> &id610in_b = id603out_result[getCycle()%2];

    id610out_result[(getCycle()+7)%8] = (add_float(id610in_a,id610in_b));
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  { // Node ID: 604 (NodeGt)
    const HWFloat<11,53> &id604in_a = id920out_output[getCycle()%8];
    const HWFloat<11,53> &id604in_b = id20out_value;

    id604out_result[(getCycle()+1)%2] = (gt_float(id604in_a,id604in_b));
  }
  { // Node ID: 608 (NodeConstantRawBits)
  }
  HWOffsetFix<20,0,UNSIGNED> id631out_o;

  { // Node ID: 631 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id631in_i = id772out_output[getCycle()%31];

    id631out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id631in_i));
  }
  HWFloat<11,53> id764out_output;

  { // Node ID: 764 (NodeStreamOffset)
    const HWFloat<11,53> &id764in_input = id587out_result[getCycle()%15];

    id764out_output = id764in_input;
  }
  { // Node ID: 812 (NodeFIFO)
    const HWFloat<11,53> &id812in_input = id764out_output;

    id812out_output[(getCycle()+69)%70] = id812in_input;
  }
  { // Node ID: 989 (NodeConstantRawBits)
  }
  { // Node ID: 633 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id633in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id633in_b = id989out_value;

    id633out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id633in_a,id633in_b));
  }
  { // Node ID: 708 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id708in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id708in_b = id633out_result[getCycle()%2];

    id708out_result[(getCycle()+1)%2] = (eq_fixed(id708in_a,id708in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id49out_o;

  { // Node ID: 49 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id49in_i = id45out_count;

    id49out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id49in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 677 (NodeRAM)
    const bool id677_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id677in_addrA = id631out_o;
    const HWFloat<11,53> &id677in_dina = id812out_output[getCycle()%70];
    const HWOffsetFix<1,0,UNSIGNED> &id677in_wea = id708out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id677in_addrB = id49out_o;

    long id677x_1;
    long id677x_2;
    HWFloat<11,53> id677x_3;

    (id677x_1) = (id677in_addrA.getValueAsLong());
    (id677x_2) = (id677in_addrB.getValueAsLong());
    switch(((long)((id677x_2)<(1000000l)))) {
      case 0l:
        id677x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id677x_3 = (id677sta_ram_store[(id677x_2)]);
        break;
      default:
        id677x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id677out_doutb[(getCycle()+2)%3] = (id677x_3);
    if(((id677in_wea.getValueAsBool())&id677_inputvalid)) {
      if(((id677x_1)<(1000000l))) {
        (id677sta_ram_store[(id677x_1)]) = id677in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_3("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 677) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 814 (NodeFIFO)
    const HWFloat<11,53> &id814in_input = id677out_doutb[getCycle()%3];

    id814out_output[(getCycle()+1)%2] = id814in_input;
  }
  { // Node ID: 62 (NodeConstantRawBits)
  }
  { // Node ID: 63 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id63in_sel = id777out_output[getCycle()%2];
    const HWFloat<11,53> &id63in_option0 = id814out_output[getCycle()%2];
    const HWFloat<11,53> &id63in_option1 = id62out_value;

    HWFloat<11,53> id63x_1;

    switch((id63in_sel.getValueAsLong())) {
      case 0l:
        id63x_1 = id63in_option0;
        break;
      case 1l:
        id63x_1 = id63in_option1;
        break;
      default:
        id63x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id63out_result[(getCycle()+1)%2] = (id63x_1);
  }
  { // Node ID: 819 (NodeFIFO)
    const HWFloat<11,53> &id819in_input = id63out_result[getCycle()%2];

    id819out_output[(getCycle()+116)%117] = id819in_input;
  }
  { // Node ID: 925 (NodeFIFO)
    const HWFloat<11,53> &id925in_input = id819out_output[getCycle()%117];

    id925out_output[(getCycle()+7)%8] = id925in_input;
  }
  { // Node ID: 926 (NodeFIFO)
    const HWFloat<11,53> &id926in_input = id925out_output[getCycle()%8];

    id926out_output[(getCycle()+42)%43] = id926in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  { // Node ID: 466 (NodeGt)
    const HWFloat<11,53> &id466in_a = id914out_output[getCycle()%37];
    const HWFloat<11,53> &id466in_b = id18out_value;

    id466out_result[(getCycle()+1)%2] = (gt_float(id466in_a,id466in_b));
  }
  { // Node ID: 33 (NodeConstantRawBits)
  }
  { // Node ID: 451 (NodeGt)
    const HWFloat<11,53> &id451in_a = id912out_output[getCycle()%35];
    const HWFloat<11,53> &id451in_b = id33out_value;

    id451out_result[(getCycle()+1)%2] = (gt_float(id451in_a,id451in_b));
  }
  { // Node ID: 988 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 452 (NodeDiv)
    const HWFloat<11,53> &id452in_a = id911out_output[getCycle()%24];
    const HWFloat<11,53> &id452in_b = id14out_value;

    id452out_result[(getCycle()+28)%29] = (div_float(id452in_a,id452in_b));
  }
  { // Node ID: 454 (NodeSub)
    const HWFloat<11,53> &id454in_a = id988out_value;
    const HWFloat<11,53> &id454in_b = id452out_result[getCycle()%29];

    id454out_result[(getCycle()+7)%8] = (sub_float(id454in_a,id454in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  { // Node ID: 455 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id455in_sel = id451out_result[getCycle()%2];
    const HWFloat<11,53> &id455in_option0 = id454out_result[getCycle()%8];
    const HWFloat<11,53> &id455in_option1 = id32out_value;

    HWFloat<11,53> id455x_1;

    switch((id455in_sel.getValueAsLong())) {
      case 0l:
        id455x_1 = id455in_option0;
        break;
      case 1l:
        id455x_1 = id455in_option1;
        break;
      default:
        id455x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id455out_result[(getCycle()+1)%2] = (id455x_1);
  }
  { // Node ID: 456 (NodeGt)
    const HWFloat<11,53> &id456in_a = id455out_result[getCycle()%2];
    const HWFloat<11,53> &id456in_b = id35out_value;

    id456out_result[(getCycle()+1)%2] = (gt_float(id456in_a,id456in_b));
  }
  { // Node ID: 818 (NodeFIFO)
    const HWFloat<11,53> &id818in_input = id455out_result[getCycle()%2];

    id818out_output[(getCycle()+1)%2] = id818in_input;
  }
  { // Node ID: 457 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id457in_sel = id456out_result[getCycle()%2];
    const HWFloat<11,53> &id457in_option0 = id818out_output[getCycle()%2];
    const HWFloat<11,53> &id457in_option1 = id35out_value;

    HWFloat<11,53> id457x_1;

    switch((id457in_sel.getValueAsLong())) {
      case 0l:
        id457x_1 = id457in_option0;
        break;
      case 1l:
        id457x_1 = id457in_option1;
        break;
      default:
        id457x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id457out_result[(getCycle()+1)%2] = (id457x_1);
  }
  { // Node ID: 459 (NodeRegister)
    const HWFloat<11,53> &id459in_input = id457out_result[getCycle()%2];

    id459out_output[(getCycle()+1)%2] = id459in_input;
  }
  { // Node ID: 469 (NodeSub)
    const HWFloat<11,53> &id469in_a = id459out_output[getCycle()%2];
    const HWFloat<11,53> &id469in_b = id819out_output[getCycle()%117];

    id469out_result[(getCycle()+7)%8] = (sub_float(id469in_a,id469in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  { // Node ID: 987 (NodeConstantRawBits)
  }
  { // Node ID: 986 (NodeConstantRawBits)
  }
  { // Node ID: 985 (NodeConstantRawBits)
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  { // Node ID: 226 (NodeSub)
    const HWFloat<11,53> &id226in_a = id59out_result[getCycle()%2];
    const HWFloat<11,53> &id226in_b = id25out_value;

    id226out_result[(getCycle()+7)%8] = (sub_float(id226in_a,id226in_b));
  }
  { // Node ID: 227 (NodeMul)
    const HWFloat<11,53> &id227in_a = id22out_value;
    const HWFloat<11,53> &id227in_b = id226out_result[getCycle()%8];

    id227out_result[(getCycle()+6)%7] = (mul_float(id227in_a,id227in_b));
  }
  { // Node ID: 745 (NodePO2FPMult)
    const HWFloat<11,53> &id745in_floatIn = id227out_result[getCycle()%7];

    id745out_floatOut[(getCycle()+1)%2] = (mul_float(id745in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id306out_result;

  { // Node ID: 306 (NodeSlice)
    const HWFloat<11,53> &id306in_a = id745out_floatOut[getCycle()%2];

    id306out_result = (slice<52,11>(id306in_a));
  }
  { // Node ID: 307 (NodeConstantRawBits)
  }
  { // Node ID: 709 (NodeEqInlined)
    const HWRawBits<11> &id709in_a = id306out_result;
    const HWRawBits<11> &id709in_b = id307out_value;

    id709out_result[(getCycle()+1)%2] = (eq_bits(id709in_a,id709in_b));
  }
  HWRawBits<52> id305out_result;

  { // Node ID: 305 (NodeSlice)
    const HWFloat<11,53> &id305in_a = id745out_floatOut[getCycle()%2];

    id305out_result = (slice<0,52>(id305in_a));
  }
  { // Node ID: 984 (NodeConstantRawBits)
  }
  { // Node ID: 710 (NodeNeqInlined)
    const HWRawBits<52> &id710in_a = id305out_result;
    const HWRawBits<52> &id710in_b = id984out_value;

    id710out_result[(getCycle()+1)%2] = (neq_bits(id710in_a,id710in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id311out_result;

  { // Node ID: 311 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id311in_a = id709out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id311in_b = id710out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id311x_1;

    (id311x_1) = (and_fixed(id311in_a,id311in_b));
    id311out_result = (id311x_1);
  }
  { // Node ID: 838 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id838in_input = id311out_result;

    id838out_output[(getCycle()+45)%46] = id838in_input;
  }
  { // Node ID: 230 (NodeConstantRawBits)
  }
  HWFloat<11,53> id231out_output;
  HWOffsetFix<1,0,UNSIGNED> id231out_output_doubt;

  { // Node ID: 231 (NodeDoubtBitOp)
    const HWFloat<11,53> &id231in_input = id745out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id231in_doubt = id230out_value;

    id231out_output = id231in_input;
    id231out_output_doubt = id231in_doubt;
  }
  { // Node ID: 232 (NodeCast)
    const HWFloat<11,53> &id232in_i = id231out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id232in_i_doubt = id231out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id232x_1;

    id232out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id232in_i,(&(id232x_1))));
    id232out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id232x_1),(c_hw_fix_4_0_uns_bits))),id232in_i_doubt));
  }
  { // Node ID: 235 (NodeConstantRawBits)
  }
  { // Node ID: 234 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id234in_a = id232out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id234in_a_doubt = id232out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id234in_b = id235out_value;

    HWOffsetFix<1,0,UNSIGNED> id234x_1;

    id234out_result[(getCycle()+7)%8] = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id234in_a,id234in_b,(&(id234x_1))));
    id234out_result_doubt[(getCycle()+7)%8] = (or_fixed((neq_fixed((id234x_1),(c_hw_fix_1_0_uns_bits_1))),id234in_a_doubt));
  }
  { // Node ID: 236 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id236in_i = id234out_result[getCycle()%8];
    const HWOffsetFix<1,0,UNSIGNED> &id236in_i_doubt = id234out_result_doubt[getCycle()%8];

    HWOffsetFix<1,0,UNSIGNED> id236x_1;

    id236out_o[(getCycle()+1)%2] = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id236in_i,(&(id236x_1))));
    id236out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id236x_1),(c_hw_fix_1_0_uns_bits_1))),id236in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id245out_output;

  { // Node ID: 245 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id245in_input = id236out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id245in_input_doubt = id236out_o_doubt[getCycle()%2];

    id245out_output = id245in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id246out_o;

  { // Node ID: 246 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id246in_i = id245out_output;

    id246out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id246in_i));
  }
  { // Node ID: 820 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id820in_input = id246out_o;

    id820out_output[(getCycle()+29)%30] = id820in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id273out_o;

  { // Node ID: 273 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id273in_i = id820out_output[getCycle()%30];

    id273out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id273in_i));
  }
  { // Node ID: 276 (NodeConstantRawBits)
  }
  { // Node ID: 737 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id249out_o;

  { // Node ID: 249 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id249in_i = id245out_output;

    id249out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id249in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id321out_output;

  { // Node ID: 321 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id321in_input = id249out_o;

    id321out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id321in_input))));
  }
  { // Node ID: 821 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id821in_input = id321out_output;

    id821out_output[(getCycle()+17)%18] = id821in_input;
  }
  { // Node ID: 322 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id322in_addr = id821out_output[getCycle()%18];

    HWOffsetFix<38,-53,UNSIGNED> id322x_1;

    switch(((long)((id322in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id322x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id322x_1 = (id322sta_rom_store[(id322in_addr.getValueAsLong())]);
        break;
      default:
        id322x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id322out_dout[(getCycle()+2)%3] = (id322x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id248out_o;

  { // Node ID: 248 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id248in_i = id245out_output;

    id248out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id248in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id318out_output;

  { // Node ID: 318 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id318in_input = id248out_o;

    id318out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id318in_input))));
  }
  { // Node ID: 822 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id822in_input = id318out_output;

    id822out_output[(getCycle()+8)%9] = id822in_input;
  }
  { // Node ID: 319 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id319in_addr = id822out_output[getCycle()%9];

    HWOffsetFix<48,-53,UNSIGNED> id319x_1;

    switch(((long)((id319in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id319x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id319x_1 = (id319sta_rom_store[(id319in_addr.getValueAsLong())]);
        break;
      default:
        id319x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id319out_dout[(getCycle()+2)%3] = (id319x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id247out_o;

  { // Node ID: 247 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id247in_i = id245out_output;

    id247out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id247in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id315out_output;

  { // Node ID: 315 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id315in_input = id247out_o;

    id315out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id315in_input))));
  }
  { // Node ID: 823 (NodeFIFO)
    const HWOffsetFix<5,0,UNSIGNED> &id823in_input = id315out_output;

    id823out_output[(getCycle()+1)%2] = id823in_input;
  }
  { // Node ID: 316 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id316in_addr = id823out_output[getCycle()%2];

    HWOffsetFix<53,-53,UNSIGNED> id316x_1;

    switch(((long)((id316in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id316x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id316x_1 = (id316sta_rom_store[(id316in_addr.getValueAsLong())]);
        break;
      default:
        id316x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id316out_dout[(getCycle()+2)%3] = (id316x_1);
  }
  { // Node ID: 253 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id250out_o;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id250in_i = id245out_output;

    id250out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id250in_i));
  }
  { // Node ID: 252 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id252in_a = id253out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id252in_b = id250out_o;

    id252out_result[(getCycle()+2)%3] = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id252in_a,id252in_b));
  }
  { // Node ID: 254 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id254in_i = id252out_result[getCycle()%3];

    id254out_o[(getCycle()+1)%2] = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id254in_i));
  }
  { // Node ID: 255 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id255in_a = id316out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id255in_b = id254out_o[getCycle()%2];

    id255out_result[(getCycle()+1)%2] = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id255in_a,id255in_b));
  }
  { // Node ID: 824 (NodeFIFO)
    const HWOffsetFix<54,-53,UNSIGNED> &id824in_input = id255out_result[getCycle()%2];

    id824out_output[(getCycle()+3)%4] = id824in_input;
  }
  { // Node ID: 256 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id256in_a = id254out_o[getCycle()%2];
    const HWOffsetFix<53,-53,UNSIGNED> &id256in_b = id316out_dout[getCycle()%3];

    id256out_result[(getCycle()+4)%5] = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id256in_a,id256in_b));
  }
  { // Node ID: 257 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id257in_a = id824out_output[getCycle()%4];
    const HWOffsetFix<64,-89,UNSIGNED> &id257in_b = id256out_result[getCycle()%5];

    id257out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id257in_a,id257in_b));
  }
  { // Node ID: 258 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id258in_i = id257out_result[getCycle()%3];

    id258out_o[(getCycle()+1)%2] = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id258in_i));
  }
  { // Node ID: 259 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id259in_a = id319out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id259in_b = id258out_o[getCycle()%2];

    id259out_result[(getCycle()+1)%2] = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id259in_a,id259in_b));
  }
  { // Node ID: 825 (NodeFIFO)
    const HWOffsetFix<59,-57,UNSIGNED> &id825in_input = id259out_result[getCycle()%2];

    id825out_output[(getCycle()+5)%6] = id825in_input;
  }
  { // Node ID: 260 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id260in_a = id258out_o[getCycle()%2];
    const HWOffsetFix<48,-53,UNSIGNED> &id260in_b = id319out_dout[getCycle()%3];

    id260out_result[(getCycle()+6)%7] = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id260in_a,id260in_b));
  }
  { // Node ID: 261 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id261in_a = id825out_output[getCycle()%6];
    const HWOffsetFix<64,-68,UNSIGNED> &id261in_b = id260out_result[getCycle()%7];

    id261out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id261in_a,id261in_b));
  }
  { // Node ID: 262 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id262in_i = id261out_result[getCycle()%3];

    id262out_o[(getCycle()+1)%2] = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id262in_i));
  }
  { // Node ID: 263 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id263in_a = id322out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id263in_b = id262out_o[getCycle()%2];

    id263out_result[(getCycle()+1)%2] = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id263in_a,id263in_b));
  }
  { // Node ID: 826 (NodeFIFO)
    const HWOffsetFix<60,-57,UNSIGNED> &id826in_input = id263out_result[getCycle()%2];

    id826out_output[(getCycle()+4)%5] = id826in_input;
  }
  { // Node ID: 264 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id264in_a = id262out_o[getCycle()%2];
    const HWOffsetFix<38,-53,UNSIGNED> &id264in_b = id322out_dout[getCycle()%3];

    id264out_result[(getCycle()+5)%6] = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id264in_a,id264in_b));
  }
  { // Node ID: 265 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id265in_a = id826out_output[getCycle()%5];
    const HWOffsetFix<64,-77,UNSIGNED> &id265in_b = id264out_result[getCycle()%6];

    id265out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id265in_a,id265in_b));
  }
  { // Node ID: 266 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id266in_i = id265out_result[getCycle()%3];

    id266out_o[(getCycle()+1)%2] = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id266in_i));
  }
  { // Node ID: 267 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id267in_i = id266out_o[getCycle()%2];

    id267out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id267in_i));
  }
  { // Node ID: 983 (NodeConstantRawBits)
  }
  { // Node ID: 711 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id711in_a = id267out_o[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id711in_b = id983out_value;

    id711out_result[(getCycle()+1)%2] = (gte_fixed(id711in_a,id711in_b));
  }
  { // Node ID: 742 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id742in_a = id273out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id742in_b = id276out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id742in_c = id737out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id742in_condb = id711out_result[getCycle()%2];

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id742x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id742x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id742x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id742x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id742x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id742x_1 = id742in_a;
        break;
      default:
        id742x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id742in_condb.getValueAsLong())) {
      case 0l:
        id742x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id742x_2 = id742in_b;
        break;
      default:
        id742x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id742x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id742x_3 = id742in_c;
        break;
      default:
        id742x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id742x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id742x_1),(id742x_2))),(id742x_3)));
    id742out_result[(getCycle()+1)%2] = (id742x_4);
  }
  HWRawBits<1> id712out_result;

  { // Node ID: 712 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id712in_a = id742out_result[getCycle()%2];

    id712out_result = (slice<13,1>(id712in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id713out_output;

  { // Node ID: 713 (NodeReinterpret)
    const HWRawBits<1> &id713in_input = id712out_result;

    id713out_output = (cast_bits2fixed<1,0,UNSIGNED>(id713in_input));
  }
  { // Node ID: 982 (NodeConstantRawBits)
  }
  { // Node ID: 238 (NodeGt)
    const HWFloat<11,53> &id238in_a = id745out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id238in_b = id982out_value;

    id238out_result[(getCycle()+1)%2] = (gt_float(id238in_a,id238in_b));
  }
  { // Node ID: 827 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id827in_input = id238out_result[getCycle()%2];

    id827out_output[(getCycle()+13)%14] = id827in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id239out_output;

  { // Node ID: 239 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id239in_input = id236out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id239in_input_doubt = id236out_o_doubt[getCycle()%2];

    id239out_output = id239in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id240out_result;

  { // Node ID: 240 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id240in_a = id827out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id240in_b = id239out_output;

    HWOffsetFix<1,0,UNSIGNED> id240x_1;

    (id240x_1) = (and_fixed(id240in_a,id240in_b));
    id240out_result = (id240x_1);
  }
  { // Node ID: 828 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id828in_input = id240out_result;

    id828out_output[(getCycle()+30)%31] = id828in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id282out_result;

  { // Node ID: 282 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id282in_a = id828out_output[getCycle()%31];

    id282out_result = (not_fixed(id282in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id283in_a = id713out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id283in_b = id282out_result;

    HWOffsetFix<1,0,UNSIGNED> id283x_1;

    (id283x_1) = (and_fixed(id283in_a,id283in_b));
    id283out_result = (id283x_1);
  }
  { // Node ID: 981 (NodeConstantRawBits)
  }
  { // Node ID: 242 (NodeLt)
    const HWFloat<11,53> &id242in_a = id745out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id242in_b = id981out_value;

    id242out_result[(getCycle()+1)%2] = (lt_float(id242in_a,id242in_b));
  }
  { // Node ID: 829 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id829in_input = id242out_result[getCycle()%2];

    id829out_output[(getCycle()+13)%14] = id829in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id243out_output;

  { // Node ID: 243 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id243in_input = id236out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id243in_input_doubt = id236out_o_doubt[getCycle()%2];

    id243out_output = id243in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id244out_result;

  { // Node ID: 244 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id244in_a = id829out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id244in_b = id243out_output;

    HWOffsetFix<1,0,UNSIGNED> id244x_1;

    (id244x_1) = (and_fixed(id244in_a,id244in_b));
    id244out_result = (id244x_1);
  }
  { // Node ID: 830 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id830in_input = id244out_result;

    id830out_output[(getCycle()+30)%31] = id830in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id284out_result;

  { // Node ID: 284 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id284in_a = id283out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id284in_b = id830out_output[getCycle()%31];

    HWOffsetFix<1,0,UNSIGNED> id284x_1;

    (id284x_1) = (or_fixed(id284in_a,id284in_b));
    id284out_result = (id284x_1);
  }
  { // Node ID: 834 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id834in_input = id284out_result;

    id834out_output[(getCycle()+1)%2] = id834in_input;
  }
  { // Node ID: 980 (NodeConstantRawBits)
  }
  { // Node ID: 714 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id714in_a = id742out_result[getCycle()%2];
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id714in_b = id980out_value;

    id714out_result[(getCycle()+1)%2] = (gte_fixed(id714in_a,id714in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id291out_result;

  { // Node ID: 291 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id291in_a = id830out_output[getCycle()%31];

    id291out_result = (not_fixed(id291in_a));
  }
  { // Node ID: 832 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id832in_input = id291out_result;

    id832out_output[(getCycle()+1)%2] = id832in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id292out_result;

  { // Node ID: 292 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id292in_a = id714out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id292in_b = id832out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id292x_1;

    (id292x_1) = (and_fixed(id292in_a,id292in_b));
    id292out_result = (id292x_1);
  }
  { // Node ID: 927 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id927in_input = id828out_output[getCycle()%31];

    id927out_output[(getCycle()+1)%2] = id927in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id293out_result;

  { // Node ID: 293 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id293in_a = id292out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id293in_b = id927out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id293x_1;

    (id293x_1) = (or_fixed(id293in_a,id293in_b));
    id293out_result = (id293x_1);
  }
  HWRawBits<2> id294out_result;

  { // Node ID: 294 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id294in_in0 = id834out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id294in_in1 = id293out_result;

    id294out_result = (cat(id294in_in0,id294in_in1));
  }
  { // Node ID: 286 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id285out_o;

  { // Node ID: 285 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id285in_i = id742out_result[getCycle()%2];

    id285out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id285in_i));
  }
  { // Node ID: 836 (NodeFIFO)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id836in_input = id285out_o;

    id836out_output[(getCycle()+1)%2] = id836in_input;
  }
  { // Node ID: 835 (NodeFIFO)
    const HWOffsetFix<53,-52,UNSIGNED> &id835in_input = id267out_o[getCycle()%2];

    id835out_output[(getCycle()+1)%2] = id835in_input;
  }
  { // Node ID: 270 (NodeConstantRawBits)
  }
  { // Node ID: 271 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id271in_sel = id711out_result[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id271in_option0 = id835out_output[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id271in_option1 = id270out_value;

    HWOffsetFix<53,-52,UNSIGNED> id271x_1;

    switch((id271in_sel.getValueAsLong())) {
      case 0l:
        id271x_1 = id271in_option0;
        break;
      case 1l:
        id271x_1 = id271in_option1;
        break;
      default:
        id271x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id271out_result[(getCycle()+1)%2] = (id271x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id272out_o;

  { // Node ID: 272 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id272in_i = id271out_result[getCycle()%2];

    id272out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id272in_i));
  }
  { // Node ID: 837 (NodeFIFO)
    const HWOffsetFix<52,-52,UNSIGNED> &id837in_input = id272out_o;

    id837out_output[(getCycle()+1)%2] = id837in_input;
  }
  HWRawBits<64> id287out_result;

  { // Node ID: 287 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id287in_in0 = id286out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id287in_in1 = id836out_output[getCycle()%2];
    const HWOffsetFix<52,-52,UNSIGNED> &id287in_in2 = id837out_output[getCycle()%2];

    id287out_result = (cat((cat(id287in_in0,id287in_in1)),id287in_in2));
  }
  HWFloat<11,53> id288out_output;

  { // Node ID: 288 (NodeReinterpret)
    const HWRawBits<64> &id288in_input = id287out_result;

    id288out_output = (cast_bits2float<11,53>(id288in_input));
  }
  { // Node ID: 295 (NodeConstantRawBits)
  }
  { // Node ID: 296 (NodeConstantRawBits)
  }
  { // Node ID: 298 (NodeConstantRawBits)
  }
  HWRawBits<64> id715out_result;

  { // Node ID: 715 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id715in_in0 = id295out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id715in_in1 = id296out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id715in_in2 = id298out_value;

    id715out_result = (cat((cat(id715in_in0,id715in_in1)),id715in_in2));
  }
  HWFloat<11,53> id300out_output;

  { // Node ID: 300 (NodeReinterpret)
    const HWRawBits<64> &id300in_input = id715out_result;

    id300out_output = (cast_bits2float<11,53>(id300in_input));
  }
  { // Node ID: 695 (NodeConstantRawBits)
  }
  { // Node ID: 303 (NodeMux)
    const HWRawBits<2> &id303in_sel = id294out_result;
    const HWFloat<11,53> &id303in_option0 = id288out_output;
    const HWFloat<11,53> &id303in_option1 = id300out_output;
    const HWFloat<11,53> &id303in_option2 = id695out_value;
    const HWFloat<11,53> &id303in_option3 = id300out_output;

    HWFloat<11,53> id303x_1;

    switch((id303in_sel.getValueAsLong())) {
      case 0l:
        id303x_1 = id303in_option0;
        break;
      case 1l:
        id303x_1 = id303in_option1;
        break;
      case 2l:
        id303x_1 = id303in_option2;
        break;
      case 3l:
        id303x_1 = id303in_option3;
        break;
      default:
        id303x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id303out_result[(getCycle()+1)%2] = (id303x_1);
  }
  { // Node ID: 979 (NodeConstantRawBits)
  }
  { // Node ID: 313 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_sel = id838out_output[getCycle()%46];
    const HWFloat<11,53> &id313in_option0 = id303out_result[getCycle()%2];
    const HWFloat<11,53> &id313in_option1 = id979out_value;

    HWFloat<11,53> id313x_1;

    switch((id313in_sel.getValueAsLong())) {
      case 0l:
        id313x_1 = id313in_option0;
        break;
      case 1l:
        id313x_1 = id313in_option1;
        break;
      default:
        id313x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id313out_result[(getCycle()+1)%2] = (id313x_1);
  }
  { // Node ID: 978 (NodeConstantRawBits)
  }
  { // Node ID: 324 (NodeAdd)
    const HWFloat<11,53> &id324in_a = id313out_result[getCycle()%2];
    const HWFloat<11,53> &id324in_b = id978out_value;

    id324out_result[(getCycle()+7)%8] = (add_float(id324in_a,id324in_b));
  }
  { // Node ID: 326 (NodeDiv)
    const HWFloat<11,53> &id326in_a = id985out_value;
    const HWFloat<11,53> &id326in_b = id324out_result[getCycle()%8];

    id326out_result[(getCycle()+28)%29] = (div_float(id326in_a,id326in_b));
  }
  { // Node ID: 328 (NodeSub)
    const HWFloat<11,53> &id328in_a = id986out_value;
    const HWFloat<11,53> &id328in_b = id326out_result[getCycle()%29];

    id328out_result[(getCycle()+7)%8] = (sub_float(id328in_a,id328in_b));
  }
  { // Node ID: 330 (NodeAdd)
    const HWFloat<11,53> &id330in_a = id987out_value;
    const HWFloat<11,53> &id330in_b = id328out_result[getCycle()%8];

    id330out_result[(getCycle()+7)%8] = (add_float(id330in_a,id330in_b));
  }
  { // Node ID: 331 (NodeMul)
    const HWFloat<11,53> &id331in_a = id30out_value;
    const HWFloat<11,53> &id331in_b = id330out_result[getCycle()%8];

    id331out_result[(getCycle()+6)%7] = (mul_float(id331in_a,id331in_b));
  }
  { // Node ID: 332 (NodeAdd)
    const HWFloat<11,53> &id332in_a = id4out_value;
    const HWFloat<11,53> &id332in_b = id331out_result[getCycle()%7];

    id332out_result[(getCycle()+7)%8] = (add_float(id332in_a,id332in_b));
  }
  { // Node ID: 445 (NodeRegister)
    const HWFloat<11,53> &id445in_input = id332out_result[getCycle()%8];

    id445out_output[(getCycle()+1)%2] = id445in_input;
  }
  { // Node ID: 470 (NodeDiv)
    const HWFloat<11,53> &id470in_a = id469out_result[getCycle()%8];
    const HWFloat<11,53> &id470in_b = id445out_output[getCycle()%2];

    id470out_result[(getCycle()+28)%29] = (div_float(id470in_a,id470in_b));
  }
  HWFloat<11,53> id467out_result;

  { // Node ID: 467 (NodeNeg)
    const HWFloat<11,53> &id467in_a = id925out_output[getCycle()%8];

    id467out_result = (neg_float(id467in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  { // Node ID: 468 (NodeDiv)
    const HWFloat<11,53> &id468in_a = id467out_result;
    const HWFloat<11,53> &id468in_b = id3out_value;

    id468out_result[(getCycle()+28)%29] = (div_float(id468in_a,id468in_b));
  }
  { // Node ID: 471 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id471in_sel = id466out_result[getCycle()%2];
    const HWFloat<11,53> &id471in_option0 = id470out_result[getCycle()%29];
    const HWFloat<11,53> &id471in_option1 = id468out_result[getCycle()%29];

    HWFloat<11,53> id471x_1;

    switch((id471in_sel.getValueAsLong())) {
      case 0l:
        id471x_1 = id471in_option0;
        break;
      case 1l:
        id471x_1 = id471in_option1;
        break;
      default:
        id471x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id471out_result[(getCycle()+1)%2] = (id471x_1);
  }
  { // Node ID: 582 (NodeRegister)
    const HWFloat<11,53> &id582in_input = id471out_result[getCycle()%2];

    id582out_output[(getCycle()+1)%2] = id582in_input;
  }
  { // Node ID: 586 (NodeMul)
    const HWFloat<11,53> &id586in_a = id582out_output[getCycle()%2];
    const HWFloat<11,53> &id586in_b = id36out_dt;

    id586out_result[(getCycle()+12)%13] = (mul_float(id586in_a,id586in_b));
  }
  { // Node ID: 587 (NodeAdd)
    const HWFloat<11,53> &id587in_a = id926out_output[getCycle()%43];
    const HWFloat<11,53> &id587in_b = id586out_result[getCycle()%13];

    id587out_result[(getCycle()+14)%15] = (add_float(id587in_a,id587in_b));
  }
  HWFloat<11,53> id605out_result;

  { // Node ID: 605 (NodeNeg)
    const HWFloat<11,53> &id605in_a = id587out_result[getCycle()%15];

    id605out_result = (neg_float(id605in_a));
  }
  HWOffsetFix<20,0,UNSIGNED> id635out_o;

  { // Node ID: 635 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id635in_i = id772out_output[getCycle()%31];

    id635out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id635in_i));
  }
  HWFloat<11,53> id765out_output;

  { // Node ID: 765 (NodeStreamOffset)
    const HWFloat<11,53> &id765in_input = id842out_output[getCycle()%8];

    id765out_output = id765in_input;
  }
  { // Node ID: 843 (NodeFIFO)
    const HWFloat<11,53> &id843in_input = id765out_output;

    id843out_output[(getCycle()+69)%70] = id843in_input;
  }
  { // Node ID: 977 (NodeConstantRawBits)
  }
  { // Node ID: 637 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id637in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id637in_b = id977out_value;

    id637out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id637in_a,id637in_b));
  }
  { // Node ID: 716 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id716in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id716in_b = id637out_result[getCycle()%2];

    id716out_result[(getCycle()+1)%2] = (eq_fixed(id716in_a,id716in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id50out_o;

  { // Node ID: 50 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id50in_i = id45out_count;

    id50out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id50in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 679 (NodeRAM)
    const bool id679_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id679in_addrA = id635out_o;
    const HWFloat<11,53> &id679in_dina = id843out_output[getCycle()%70];
    const HWOffsetFix<1,0,UNSIGNED> &id679in_wea = id716out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id679in_addrB = id50out_o;

    long id679x_1;
    long id679x_2;
    HWFloat<11,53> id679x_3;

    (id679x_1) = (id679in_addrA.getValueAsLong());
    (id679x_2) = (id679in_addrB.getValueAsLong());
    switch(((long)((id679x_2)<(1000000l)))) {
      case 0l:
        id679x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id679x_3 = (id679sta_ram_store[(id679x_2)]);
        break;
      default:
        id679x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id679out_doutb[(getCycle()+2)%3] = (id679x_3);
    if(((id679in_wea.getValueAsBool())&id679_inputvalid)) {
      if(((id679x_1)<(1000000l))) {
        (id679sta_ram_store[(id679x_1)]) = id679in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_4("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 679) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 64 (NodeConstantRawBits)
  }
  { // Node ID: 65 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id65in_sel = id698out_result[getCycle()%2];
    const HWFloat<11,53> &id65in_option0 = id679out_doutb[getCycle()%3];
    const HWFloat<11,53> &id65in_option1 = id64out_value;

    HWFloat<11,53> id65x_1;

    switch((id65in_sel.getValueAsLong())) {
      case 0l:
        id65x_1 = id65in_option0;
        break;
      case 1l:
        id65x_1 = id65in_option1;
        break;
      default:
        id65x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id65out_result[(getCycle()+1)%2] = (id65x_1);
  }
  { // Node ID: 863 (NodeFIFO)
    const HWFloat<11,53> &id863in_input = id65out_result[getCycle()%2];

    id863out_output[(getCycle()+111)%112] = id863in_input;
  }
  { // Node ID: 928 (NodeFIFO)
    const HWFloat<11,53> &id928in_input = id863out_output[getCycle()%112];

    id928out_output[(getCycle()+48)%49] = id928in_input;
  }
  { // Node ID: 976 (NodeConstantRawBits)
  }
  { // Node ID: 975 (NodeConstantRawBits)
  }
  { // Node ID: 974 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 472 (NodeSub)
    const HWFloat<11,53> &id472in_a = id59out_result[getCycle()%2];
    const HWFloat<11,53> &id472in_b = id26out_value;

    id472out_result[(getCycle()+7)%8] = (sub_float(id472in_a,id472in_b));
  }
  { // Node ID: 473 (NodeMul)
    const HWFloat<11,53> &id473in_a = id23out_value;
    const HWFloat<11,53> &id473in_b = id472out_result[getCycle()%8];

    id473out_result[(getCycle()+6)%7] = (mul_float(id473in_a,id473in_b));
  }
  { // Node ID: 746 (NodePO2FPMult)
    const HWFloat<11,53> &id746in_floatIn = id473out_result[getCycle()%7];

    id746out_floatOut[(getCycle()+1)%2] = (mul_float(id746in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id552out_result;

  { // Node ID: 552 (NodeSlice)
    const HWFloat<11,53> &id552in_a = id746out_floatOut[getCycle()%2];

    id552out_result = (slice<52,11>(id552in_a));
  }
  { // Node ID: 553 (NodeConstantRawBits)
  }
  { // Node ID: 717 (NodeEqInlined)
    const HWRawBits<11> &id717in_a = id552out_result;
    const HWRawBits<11> &id717in_b = id553out_value;

    id717out_result[(getCycle()+1)%2] = (eq_bits(id717in_a,id717in_b));
  }
  HWRawBits<52> id551out_result;

  { // Node ID: 551 (NodeSlice)
    const HWFloat<11,53> &id551in_a = id746out_floatOut[getCycle()%2];

    id551out_result = (slice<0,52>(id551in_a));
  }
  { // Node ID: 973 (NodeConstantRawBits)
  }
  { // Node ID: 718 (NodeNeqInlined)
    const HWRawBits<52> &id718in_a = id551out_result;
    const HWRawBits<52> &id718in_b = id973out_value;

    id718out_result[(getCycle()+1)%2] = (neq_bits(id718in_a,id718in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id557out_result;

  { // Node ID: 557 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id557in_a = id717out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id557in_b = id718out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id557x_1;

    (id557x_1) = (and_fixed(id557in_a,id557in_b));
    id557out_result = (id557x_1);
  }
  { // Node ID: 862 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id862in_input = id557out_result;

    id862out_output[(getCycle()+45)%46] = id862in_input;
  }
  { // Node ID: 476 (NodeConstantRawBits)
  }
  HWFloat<11,53> id477out_output;
  HWOffsetFix<1,0,UNSIGNED> id477out_output_doubt;

  { // Node ID: 477 (NodeDoubtBitOp)
    const HWFloat<11,53> &id477in_input = id746out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id477in_doubt = id476out_value;

    id477out_output = id477in_input;
    id477out_output_doubt = id477in_doubt;
  }
  { // Node ID: 478 (NodeCast)
    const HWFloat<11,53> &id478in_i = id477out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id478in_i_doubt = id477out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id478x_1;

    id478out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id478in_i,(&(id478x_1))));
    id478out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id478x_1),(c_hw_fix_4_0_uns_bits))),id478in_i_doubt));
  }
  { // Node ID: 481 (NodeConstantRawBits)
  }
  { // Node ID: 480 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id480in_a = id478out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id480in_a_doubt = id478out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id480in_b = id481out_value;

    HWOffsetFix<1,0,UNSIGNED> id480x_1;

    id480out_result[(getCycle()+7)%8] = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id480in_a,id480in_b,(&(id480x_1))));
    id480out_result_doubt[(getCycle()+7)%8] = (or_fixed((neq_fixed((id480x_1),(c_hw_fix_1_0_uns_bits_1))),id480in_a_doubt));
  }
  { // Node ID: 482 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id482in_i = id480out_result[getCycle()%8];
    const HWOffsetFix<1,0,UNSIGNED> &id482in_i_doubt = id480out_result_doubt[getCycle()%8];

    HWOffsetFix<1,0,UNSIGNED> id482x_1;

    id482out_o[(getCycle()+1)%2] = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id482in_i,(&(id482x_1))));
    id482out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id482x_1),(c_hw_fix_1_0_uns_bits_1))),id482in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id491out_output;

  { // Node ID: 491 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id491in_input = id482out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id491in_input_doubt = id482out_o_doubt[getCycle()%2];

    id491out_output = id491in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id492out_o;

  { // Node ID: 492 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id492in_i = id491out_output;

    id492out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id492in_i));
  }
  { // Node ID: 844 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id844in_input = id492out_o;

    id844out_output[(getCycle()+29)%30] = id844in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id519out_o;

  { // Node ID: 519 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id519in_i = id844out_output[getCycle()%30];

    id519out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id519in_i));
  }
  { // Node ID: 522 (NodeConstantRawBits)
  }
  { // Node ID: 739 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id495out_o;

  { // Node ID: 495 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id495in_i = id491out_output;

    id495out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id495in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id567out_output;

  { // Node ID: 567 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id567in_input = id495out_o;

    id567out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id567in_input))));
  }
  { // Node ID: 845 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id845in_input = id567out_output;

    id845out_output[(getCycle()+17)%18] = id845in_input;
  }
  { // Node ID: 568 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id568in_addr = id845out_output[getCycle()%18];

    HWOffsetFix<38,-53,UNSIGNED> id568x_1;

    switch(((long)((id568in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id568x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id568x_1 = (id568sta_rom_store[(id568in_addr.getValueAsLong())]);
        break;
      default:
        id568x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id568out_dout[(getCycle()+2)%3] = (id568x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id494out_o;

  { // Node ID: 494 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id494in_i = id491out_output;

    id494out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id494in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id564out_output;

  { // Node ID: 564 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id564in_input = id494out_o;

    id564out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id564in_input))));
  }
  { // Node ID: 846 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id846in_input = id564out_output;

    id846out_output[(getCycle()+8)%9] = id846in_input;
  }
  { // Node ID: 565 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id565in_addr = id846out_output[getCycle()%9];

    HWOffsetFix<48,-53,UNSIGNED> id565x_1;

    switch(((long)((id565in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id565x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id565x_1 = (id565sta_rom_store[(id565in_addr.getValueAsLong())]);
        break;
      default:
        id565x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id565out_dout[(getCycle()+2)%3] = (id565x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id493out_o;

  { // Node ID: 493 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id493in_i = id491out_output;

    id493out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id493in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id561out_output;

  { // Node ID: 561 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id561in_input = id493out_o;

    id561out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id561in_input))));
  }
  { // Node ID: 847 (NodeFIFO)
    const HWOffsetFix<5,0,UNSIGNED> &id847in_input = id561out_output;

    id847out_output[(getCycle()+1)%2] = id847in_input;
  }
  { // Node ID: 562 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id562in_addr = id847out_output[getCycle()%2];

    HWOffsetFix<53,-53,UNSIGNED> id562x_1;

    switch(((long)((id562in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id562x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id562x_1 = (id562sta_rom_store[(id562in_addr.getValueAsLong())]);
        break;
      default:
        id562x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id562out_dout[(getCycle()+2)%3] = (id562x_1);
  }
  { // Node ID: 499 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id496out_o;

  { // Node ID: 496 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id496in_i = id491out_output;

    id496out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id496in_i));
  }
  { // Node ID: 498 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id498in_a = id499out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id498in_b = id496out_o;

    id498out_result[(getCycle()+2)%3] = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id498in_a,id498in_b));
  }
  { // Node ID: 500 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id500in_i = id498out_result[getCycle()%3];

    id500out_o[(getCycle()+1)%2] = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id500in_i));
  }
  { // Node ID: 501 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id501in_a = id562out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id501in_b = id500out_o[getCycle()%2];

    id501out_result[(getCycle()+1)%2] = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id501in_a,id501in_b));
  }
  { // Node ID: 848 (NodeFIFO)
    const HWOffsetFix<54,-53,UNSIGNED> &id848in_input = id501out_result[getCycle()%2];

    id848out_output[(getCycle()+3)%4] = id848in_input;
  }
  { // Node ID: 502 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id502in_a = id500out_o[getCycle()%2];
    const HWOffsetFix<53,-53,UNSIGNED> &id502in_b = id562out_dout[getCycle()%3];

    id502out_result[(getCycle()+4)%5] = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id502in_a,id502in_b));
  }
  { // Node ID: 503 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id503in_a = id848out_output[getCycle()%4];
    const HWOffsetFix<64,-89,UNSIGNED> &id503in_b = id502out_result[getCycle()%5];

    id503out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id503in_a,id503in_b));
  }
  { // Node ID: 504 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id504in_i = id503out_result[getCycle()%3];

    id504out_o[(getCycle()+1)%2] = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id504in_i));
  }
  { // Node ID: 505 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id505in_a = id565out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id505in_b = id504out_o[getCycle()%2];

    id505out_result[(getCycle()+1)%2] = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id505in_a,id505in_b));
  }
  { // Node ID: 849 (NodeFIFO)
    const HWOffsetFix<59,-57,UNSIGNED> &id849in_input = id505out_result[getCycle()%2];

    id849out_output[(getCycle()+5)%6] = id849in_input;
  }
  { // Node ID: 506 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id506in_a = id504out_o[getCycle()%2];
    const HWOffsetFix<48,-53,UNSIGNED> &id506in_b = id565out_dout[getCycle()%3];

    id506out_result[(getCycle()+6)%7] = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id506in_a,id506in_b));
  }
  { // Node ID: 507 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id507in_a = id849out_output[getCycle()%6];
    const HWOffsetFix<64,-68,UNSIGNED> &id507in_b = id506out_result[getCycle()%7];

    id507out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id507in_a,id507in_b));
  }
  { // Node ID: 508 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id508in_i = id507out_result[getCycle()%3];

    id508out_o[(getCycle()+1)%2] = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id508in_i));
  }
  { // Node ID: 509 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id509in_a = id568out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id509in_b = id508out_o[getCycle()%2];

    id509out_result[(getCycle()+1)%2] = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id509in_a,id509in_b));
  }
  { // Node ID: 850 (NodeFIFO)
    const HWOffsetFix<60,-57,UNSIGNED> &id850in_input = id509out_result[getCycle()%2];

    id850out_output[(getCycle()+4)%5] = id850in_input;
  }
  { // Node ID: 510 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id510in_a = id508out_o[getCycle()%2];
    const HWOffsetFix<38,-53,UNSIGNED> &id510in_b = id568out_dout[getCycle()%3];

    id510out_result[(getCycle()+5)%6] = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id510in_a,id510in_b));
  }
  { // Node ID: 511 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id511in_a = id850out_output[getCycle()%5];
    const HWOffsetFix<64,-77,UNSIGNED> &id511in_b = id510out_result[getCycle()%6];

    id511out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id511in_a,id511in_b));
  }
  { // Node ID: 512 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id512in_i = id511out_result[getCycle()%3];

    id512out_o[(getCycle()+1)%2] = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id512in_i));
  }
  { // Node ID: 513 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id513in_i = id512out_o[getCycle()%2];

    id513out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id513in_i));
  }
  { // Node ID: 972 (NodeConstantRawBits)
  }
  { // Node ID: 719 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id719in_a = id513out_o[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id719in_b = id972out_value;

    id719out_result[(getCycle()+1)%2] = (gte_fixed(id719in_a,id719in_b));
  }
  { // Node ID: 741 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id741in_a = id519out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id741in_b = id522out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id741in_c = id739out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id741in_condb = id719out_result[getCycle()%2];

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id741x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id741x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id741x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id741x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id741x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id741x_1 = id741in_a;
        break;
      default:
        id741x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id741in_condb.getValueAsLong())) {
      case 0l:
        id741x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id741x_2 = id741in_b;
        break;
      default:
        id741x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id741x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id741x_3 = id741in_c;
        break;
      default:
        id741x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id741x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id741x_1),(id741x_2))),(id741x_3)));
    id741out_result[(getCycle()+1)%2] = (id741x_4);
  }
  HWRawBits<1> id720out_result;

  { // Node ID: 720 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id720in_a = id741out_result[getCycle()%2];

    id720out_result = (slice<13,1>(id720in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id721out_output;

  { // Node ID: 721 (NodeReinterpret)
    const HWRawBits<1> &id721in_input = id720out_result;

    id721out_output = (cast_bits2fixed<1,0,UNSIGNED>(id721in_input));
  }
  { // Node ID: 971 (NodeConstantRawBits)
  }
  { // Node ID: 484 (NodeGt)
    const HWFloat<11,53> &id484in_a = id746out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id484in_b = id971out_value;

    id484out_result[(getCycle()+1)%2] = (gt_float(id484in_a,id484in_b));
  }
  { // Node ID: 851 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id851in_input = id484out_result[getCycle()%2];

    id851out_output[(getCycle()+13)%14] = id851in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id485out_output;

  { // Node ID: 485 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id485in_input = id482out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id485in_input_doubt = id482out_o_doubt[getCycle()%2];

    id485out_output = id485in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id486out_result;

  { // Node ID: 486 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id486in_a = id851out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id486in_b = id485out_output;

    HWOffsetFix<1,0,UNSIGNED> id486x_1;

    (id486x_1) = (and_fixed(id486in_a,id486in_b));
    id486out_result = (id486x_1);
  }
  { // Node ID: 852 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id852in_input = id486out_result;

    id852out_output[(getCycle()+30)%31] = id852in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id528out_result;

  { // Node ID: 528 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id528in_a = id852out_output[getCycle()%31];

    id528out_result = (not_fixed(id528in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id529out_result;

  { // Node ID: 529 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id529in_a = id721out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id529in_b = id528out_result;

    HWOffsetFix<1,0,UNSIGNED> id529x_1;

    (id529x_1) = (and_fixed(id529in_a,id529in_b));
    id529out_result = (id529x_1);
  }
  { // Node ID: 970 (NodeConstantRawBits)
  }
  { // Node ID: 488 (NodeLt)
    const HWFloat<11,53> &id488in_a = id746out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id488in_b = id970out_value;

    id488out_result[(getCycle()+1)%2] = (lt_float(id488in_a,id488in_b));
  }
  { // Node ID: 853 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id853in_input = id488out_result[getCycle()%2];

    id853out_output[(getCycle()+13)%14] = id853in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id489out_output;

  { // Node ID: 489 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id489in_input = id482out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id489in_input_doubt = id482out_o_doubt[getCycle()%2];

    id489out_output = id489in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id490out_result;

  { // Node ID: 490 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id490in_a = id853out_output[getCycle()%14];
    const HWOffsetFix<1,0,UNSIGNED> &id490in_b = id489out_output;

    HWOffsetFix<1,0,UNSIGNED> id490x_1;

    (id490x_1) = (and_fixed(id490in_a,id490in_b));
    id490out_result = (id490x_1);
  }
  { // Node ID: 854 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id854in_input = id490out_result;

    id854out_output[(getCycle()+30)%31] = id854in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id530out_result;

  { // Node ID: 530 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id530in_a = id529out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id530in_b = id854out_output[getCycle()%31];

    HWOffsetFix<1,0,UNSIGNED> id530x_1;

    (id530x_1) = (or_fixed(id530in_a,id530in_b));
    id530out_result = (id530x_1);
  }
  { // Node ID: 858 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_input = id530out_result;

    id858out_output[(getCycle()+1)%2] = id858in_input;
  }
  { // Node ID: 969 (NodeConstantRawBits)
  }
  { // Node ID: 722 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id722in_a = id741out_result[getCycle()%2];
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id722in_b = id969out_value;

    id722out_result[(getCycle()+1)%2] = (gte_fixed(id722in_a,id722in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id537out_result;

  { // Node ID: 537 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id537in_a = id854out_output[getCycle()%31];

    id537out_result = (not_fixed(id537in_a));
  }
  { // Node ID: 856 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id856in_input = id537out_result;

    id856out_output[(getCycle()+1)%2] = id856in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id538out_result;

  { // Node ID: 538 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id538in_a = id722out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id538in_b = id856out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id538x_1;

    (id538x_1) = (and_fixed(id538in_a,id538in_b));
    id538out_result = (id538x_1);
  }
  { // Node ID: 929 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id929in_input = id852out_output[getCycle()%31];

    id929out_output[(getCycle()+1)%2] = id929in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id539out_result;

  { // Node ID: 539 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id539in_a = id538out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id539in_b = id929out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id539x_1;

    (id539x_1) = (or_fixed(id539in_a,id539in_b));
    id539out_result = (id539x_1);
  }
  HWRawBits<2> id540out_result;

  { // Node ID: 540 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id540in_in0 = id858out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id540in_in1 = id539out_result;

    id540out_result = (cat(id540in_in0,id540in_in1));
  }
  { // Node ID: 532 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id531out_o;

  { // Node ID: 531 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id531in_i = id741out_result[getCycle()%2];

    id531out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id531in_i));
  }
  { // Node ID: 860 (NodeFIFO)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id860in_input = id531out_o;

    id860out_output[(getCycle()+1)%2] = id860in_input;
  }
  { // Node ID: 859 (NodeFIFO)
    const HWOffsetFix<53,-52,UNSIGNED> &id859in_input = id513out_o[getCycle()%2];

    id859out_output[(getCycle()+1)%2] = id859in_input;
  }
  { // Node ID: 516 (NodeConstantRawBits)
  }
  { // Node ID: 517 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id517in_sel = id719out_result[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id517in_option0 = id859out_output[getCycle()%2];
    const HWOffsetFix<53,-52,UNSIGNED> &id517in_option1 = id516out_value;

    HWOffsetFix<53,-52,UNSIGNED> id517x_1;

    switch((id517in_sel.getValueAsLong())) {
      case 0l:
        id517x_1 = id517in_option0;
        break;
      case 1l:
        id517x_1 = id517in_option1;
        break;
      default:
        id517x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id517out_result[(getCycle()+1)%2] = (id517x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id518out_o;

  { // Node ID: 518 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id518in_i = id517out_result[getCycle()%2];

    id518out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id518in_i));
  }
  { // Node ID: 861 (NodeFIFO)
    const HWOffsetFix<52,-52,UNSIGNED> &id861in_input = id518out_o;

    id861out_output[(getCycle()+1)%2] = id861in_input;
  }
  HWRawBits<64> id533out_result;

  { // Node ID: 533 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id533in_in0 = id532out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id533in_in1 = id860out_output[getCycle()%2];
    const HWOffsetFix<52,-52,UNSIGNED> &id533in_in2 = id861out_output[getCycle()%2];

    id533out_result = (cat((cat(id533in_in0,id533in_in1)),id533in_in2));
  }
  HWFloat<11,53> id534out_output;

  { // Node ID: 534 (NodeReinterpret)
    const HWRawBits<64> &id534in_input = id533out_result;

    id534out_output = (cast_bits2float<11,53>(id534in_input));
  }
  { // Node ID: 541 (NodeConstantRawBits)
  }
  { // Node ID: 542 (NodeConstantRawBits)
  }
  { // Node ID: 544 (NodeConstantRawBits)
  }
  HWRawBits<64> id723out_result;

  { // Node ID: 723 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id723in_in0 = id541out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id723in_in1 = id542out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id723in_in2 = id544out_value;

    id723out_result = (cat((cat(id723in_in0,id723in_in1)),id723in_in2));
  }
  HWFloat<11,53> id546out_output;

  { // Node ID: 546 (NodeReinterpret)
    const HWRawBits<64> &id546in_input = id723out_result;

    id546out_output = (cast_bits2float<11,53>(id546in_input));
  }
  { // Node ID: 696 (NodeConstantRawBits)
  }
  { // Node ID: 549 (NodeMux)
    const HWRawBits<2> &id549in_sel = id540out_result;
    const HWFloat<11,53> &id549in_option0 = id534out_output;
    const HWFloat<11,53> &id549in_option1 = id546out_output;
    const HWFloat<11,53> &id549in_option2 = id696out_value;
    const HWFloat<11,53> &id549in_option3 = id546out_output;

    HWFloat<11,53> id549x_1;

    switch((id549in_sel.getValueAsLong())) {
      case 0l:
        id549x_1 = id549in_option0;
        break;
      case 1l:
        id549x_1 = id549in_option1;
        break;
      case 2l:
        id549x_1 = id549in_option2;
        break;
      case 3l:
        id549x_1 = id549in_option3;
        break;
      default:
        id549x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id549out_result[(getCycle()+1)%2] = (id549x_1);
  }
  { // Node ID: 968 (NodeConstantRawBits)
  }
  { // Node ID: 559 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id559in_sel = id862out_output[getCycle()%46];
    const HWFloat<11,53> &id559in_option0 = id549out_result[getCycle()%2];
    const HWFloat<11,53> &id559in_option1 = id968out_value;

    HWFloat<11,53> id559x_1;

    switch((id559in_sel.getValueAsLong())) {
      case 0l:
        id559x_1 = id559in_option0;
        break;
      case 1l:
        id559x_1 = id559in_option1;
        break;
      default:
        id559x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id559out_result[(getCycle()+1)%2] = (id559x_1);
  }
  { // Node ID: 967 (NodeConstantRawBits)
  }
  { // Node ID: 570 (NodeAdd)
    const HWFloat<11,53> &id570in_a = id559out_result[getCycle()%2];
    const HWFloat<11,53> &id570in_b = id967out_value;

    id570out_result[(getCycle()+7)%8] = (add_float(id570in_a,id570in_b));
  }
  { // Node ID: 572 (NodeDiv)
    const HWFloat<11,53> &id572in_a = id974out_value;
    const HWFloat<11,53> &id572in_b = id570out_result[getCycle()%8];

    id572out_result[(getCycle()+28)%29] = (div_float(id572in_a,id572in_b));
  }
  { // Node ID: 574 (NodeSub)
    const HWFloat<11,53> &id574in_a = id975out_value;
    const HWFloat<11,53> &id574in_b = id572out_result[getCycle()%29];

    id574out_result[(getCycle()+7)%8] = (sub_float(id574in_a,id574in_b));
  }
  { // Node ID: 576 (NodeAdd)
    const HWFloat<11,53> &id576in_a = id976out_value;
    const HWFloat<11,53> &id576in_b = id574out_result[getCycle()%8];

    id576out_result[(getCycle()+7)%8] = (add_float(id576in_a,id576in_b));
  }
  { // Node ID: 747 (NodePO2FPMult)
    const HWFloat<11,53> &id747in_floatIn = id576out_result[getCycle()%8];

    id747out_floatOut[(getCycle()+1)%2] = (mul_float(id747in_floatIn,(c_hw_flt_11_53_0_5val)));
  }
  { // Node ID: 579 (NodeSub)
    const HWFloat<11,53> &id579in_a = id747out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id579in_b = id863out_output[getCycle()%112];

    id579out_result[(getCycle()+7)%8] = (sub_float(id579in_a,id579in_b));
  }
  { // Node ID: 440 (NodeGt)
    const HWFloat<11,53> &id440in_a = id913out_output[getCycle()%4];
    const HWFloat<11,53> &id440in_b = id18out_value;

    id440out_result[(getCycle()+1)%2] = (gt_float(id440in_a,id440in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  { // Node ID: 441 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id441in_sel = id440out_result[getCycle()%2];
    const HWFloat<11,53> &id441in_option0 = id6out_value;
    const HWFloat<11,53> &id441in_option1 = id7out_value;

    HWFloat<11,53> id441x_1;

    switch((id441in_sel.getValueAsLong())) {
      case 0l:
        id441x_1 = id441in_option0;
        break;
      case 1l:
        id441x_1 = id441in_option1;
        break;
      default:
        id441x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id441out_result[(getCycle()+1)%2] = (id441x_1);
  }
  { // Node ID: 447 (NodeRegister)
    const HWFloat<11,53> &id447in_input = id441out_result[getCycle()%2];

    id447out_output[(getCycle()+1)%2] = id447in_input;
  }
  { // Node ID: 580 (NodeDiv)
    const HWFloat<11,53> &id580in_a = id579out_result[getCycle()%8];
    const HWFloat<11,53> &id580in_b = id447out_output[getCycle()%2];

    id580out_result[(getCycle()+28)%29] = (div_float(id580in_a,id580in_b));
  }
  { // Node ID: 583 (NodeRegister)
    const HWFloat<11,53> &id583in_input = id580out_result[getCycle()%29];

    id583out_output[(getCycle()+1)%2] = id583in_input;
  }
  { // Node ID: 588 (NodeMul)
    const HWFloat<11,53> &id588in_a = id583out_output[getCycle()%2];
    const HWFloat<11,53> &id588in_b = id36out_dt;

    id588out_result[(getCycle()+12)%13] = (mul_float(id588in_a,id588in_b));
  }
  { // Node ID: 589 (NodeAdd)
    const HWFloat<11,53> &id589in_a = id928out_output[getCycle()%49];
    const HWFloat<11,53> &id589in_b = id588out_result[getCycle()%13];

    id589out_result[(getCycle()+14)%15] = (add_float(id589in_a,id589in_b));
  }
  { // Node ID: 842 (NodeFIFO)
    const HWFloat<11,53> &id842in_input = id589out_result[getCycle()%15];

    id842out_output[(getCycle()+7)%8] = id842in_input;
  }
  { // Node ID: 606 (NodeMul)
    const HWFloat<11,53> &id606in_a = id605out_result;
    const HWFloat<11,53> &id606in_b = id842out_output[getCycle()%8];

    id606out_result[(getCycle()+6)%7] = (mul_float(id606in_a,id606in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  { // Node ID: 607 (NodeDiv)
    const HWFloat<11,53> &id607in_a = id606out_result[getCycle()%7];
    const HWFloat<11,53> &id607in_b = id13out_value;

    id607out_result[(getCycle()+28)%29] = (div_float(id607in_a,id607in_b));
  }
  { // Node ID: 609 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id609in_sel = id604out_result[getCycle()%2];
    const HWFloat<11,53> &id609in_option0 = id608out_value;
    const HWFloat<11,53> &id609in_option1 = id607out_result[getCycle()%29];

    HWFloat<11,53> id609x_1;

    switch((id609in_sel.getValueAsLong())) {
      case 0l:
        id609x_1 = id609in_option0;
        break;
      case 1l:
        id609x_1 = id609in_option1;
        break;
      default:
        id609x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id609out_result[(getCycle()+1)%2] = (id609x_1);
  }
  { // Node ID: 611 (NodeAdd)
    const HWFloat<11,53> &id611in_a = id610out_result[getCycle()%8];
    const HWFloat<11,53> &id611in_b = id609out_result[getCycle()%2];

    id611out_result[(getCycle()+7)%8] = (add_float(id611in_a,id611in_b));
  }
  { // Node ID: 966 (NodeConstantRawBits)
  }
  { // Node ID: 724 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id724in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id724in_b = id966out_value;

    id724out_result[(getCycle()+1)%2] = (eq_fixed(id724in_a,id724in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id639out_o;

  { // Node ID: 639 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id639in_i = id772out_output[getCycle()%31];

    id639out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id639in_i));
  }
  HWFloat<11,53> id766out_output;

  { // Node ID: 766 (NodeStreamOffset)
    const HWFloat<11,53> &id766in_input = id930out_output[getCycle()%2];

    id766out_output = id766in_input;
  }
  { // Node ID: 869 (NodeFIFO)
    const HWFloat<11,53> &id869in_input = id766out_output;

    id869out_output[(getCycle()+54)%55] = id869in_input;
  }
  { // Node ID: 965 (NodeConstantRawBits)
  }
  { // Node ID: 641 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id641in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id641in_b = id965out_value;

    id641out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id641in_a,id641in_b));
  }
  { // Node ID: 725 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id725in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id725in_b = id641out_result[getCycle()%2];

    id725out_result[(getCycle()+1)%2] = (eq_fixed(id725in_a,id725in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id51out_o;

  { // Node ID: 51 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id51in_i = id45out_count;

    id51out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id51in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 680 (NodeRAM)
    const bool id680_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id680in_addrA = id639out_o;
    const HWFloat<11,53> &id680in_dina = id869out_output[getCycle()%55];
    const HWOffsetFix<1,0,UNSIGNED> &id680in_wea = id725out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id680in_addrB = id51out_o;

    long id680x_1;
    long id680x_2;
    HWFloat<11,53> id680x_3;

    (id680x_1) = (id680in_addrA.getValueAsLong());
    (id680x_2) = (id680in_addrB.getValueAsLong());
    switch(((long)((id680x_2)<(1000000l)))) {
      case 0l:
        id680x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id680x_3 = (id680sta_ram_store[(id680x_2)]);
        break;
      default:
        id680x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id680out_doutb[(getCycle()+2)%3] = (id680x_3);
    if(((id680in_wea.getValueAsBool())&id680_inputvalid)) {
      if(((id680x_1)<(1000000l))) {
        (id680sta_ram_store[(id680x_1)]) = id680in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_5("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 680) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 68 (NodeConstantRawBits)
  }
  { // Node ID: 69 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id69in_sel = id724out_result[getCycle()%2];
    const HWFloat<11,53> &id69in_option0 = id680out_doutb[getCycle()%3];
    const HWFloat<11,53> &id69in_option1 = id68out_value;

    HWFloat<11,53> id69x_1;

    switch((id69in_sel.getValueAsLong())) {
      case 0l:
        id69x_1 = id69in_option0;
        break;
      case 1l:
        id69x_1 = id69in_option1;
        break;
      default:
        id69x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id69out_result[(getCycle()+1)%2] = (id69x_1);
  }
  { // Node ID: 870 (NodeFIFO)
    const HWFloat<11,53> &id870in_input = id69out_result[getCycle()%2];

    id870out_output[(getCycle()+173)%174] = id870in_input;
  }
  { // Node ID: 74 (NodeAdd)
    const HWFloat<11,53> &id74in_a = id870out_output[getCycle()%174];
    const HWFloat<11,53> &id74in_b = id36out_dt;

    id74out_result[(getCycle()+14)%15] = (add_float(id74in_a,id74in_b));
  }
  { // Node ID: 872 (NodeFIFO)
    const HWFloat<11,53> &id872in_input = id74out_result[getCycle()%15];

    id872out_output[(getCycle()+7)%8] = id872in_input;
  }
  { // Node ID: 930 (NodeFIFO)
    const HWFloat<11,53> &id930in_input = id872out_output[getCycle()%8];

    id930out_output[(getCycle()+1)%2] = id930in_input;
  }
  { // Node ID: 964 (NodeConstantRawBits)
  }
  { // Node ID: 84 (NodeEq)
    const HWFloat<11,53> &id84in_a = id930out_output[getCycle()%2];
    const HWFloat<11,53> &id84in_b = id964out_value;

    id84out_result[(getCycle()+1)%2] = (eq_float(id84in_a,id84in_b));
  }
  { // Node ID: 86 (NodeConstantRawBits)
  }
  { // Node ID: 85 (NodeConstantRawBits)
  }
  { // Node ID: 87 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id87in_sel = id84out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id87in_option0 = id86out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id87in_option1 = id85out_value;

    HWOffsetFix<1,0,UNSIGNED> id87x_1;

    switch((id87in_sel.getValueAsLong())) {
      case 0l:
        id87x_1 = id87in_option0;
        break;
      case 1l:
        id87x_1 = id87in_option1;
        break;
      default:
        id87x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id87out_result[(getCycle()+1)%2] = (id87x_1);
  }
  { // Node ID: 963 (NodeConstantRawBits)
  }
  { // Node ID: 79 (NodeLt)
    const HWFloat<11,53> &id79in_a = id872out_output[getCycle()%8];
    const HWFloat<11,53> &id79in_b = id963out_value;

    id79out_result[(getCycle()+1)%2] = (lt_float(id79in_a,id79in_b));
  }
  { // Node ID: 81 (NodeConstantRawBits)
  }
  { // Node ID: 80 (NodeConstantRawBits)
  }
  { // Node ID: 82 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id82in_sel = id79out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id82in_option0 = id81out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id82in_option1 = id80out_value;

    HWOffsetFix<1,0,UNSIGNED> id82x_1;

    switch((id82in_sel.getValueAsLong())) {
      case 0l:
        id82x_1 = id82in_option0;
        break;
      case 1l:
        id82x_1 = id82in_option1;
        break;
      default:
        id82x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id82out_result[(getCycle()+1)%2] = (id82x_1);
  }
  { // Node ID: 89 (NodeConstantRawBits)
  }
  { // Node ID: 88 (NodeConstantRawBits)
  }
  { // Node ID: 90 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id90in_sel = id82out_result[getCycle()%2];
    const HWFloat<11,53> &id90in_option0 = id89out_value;
    const HWFloat<11,53> &id90in_option1 = id88out_value;

    HWFloat<11,53> id90x_1;

    switch((id90in_sel.getValueAsLong())) {
      case 0l:
        id90x_1 = id90in_option0;
        break;
      case 1l:
        id90x_1 = id90in_option1;
        break;
      default:
        id90x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id90out_result[(getCycle()+1)%2] = (id90x_1);
  }
  { // Node ID: 91 (NodeConstantRawBits)
  }
  { // Node ID: 92 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id92in_sel = id87out_result[getCycle()%2];
    const HWFloat<11,53> &id92in_option0 = id90out_result[getCycle()%2];
    const HWFloat<11,53> &id92in_option1 = id91out_value;

    HWFloat<11,53> id92x_1;

    switch((id92in_sel.getValueAsLong())) {
      case 0l:
        id92x_1 = id92in_option0;
        break;
      case 1l:
        id92x_1 = id92in_option1;
        break;
      default:
        id92x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id92out_result[(getCycle()+1)%2] = (id92x_1);
  }
  { // Node ID: 962 (NodeConstantRawBits)
  }
  { // Node ID: 961 (NodeConstantRawBits)
  }
  { // Node ID: 726 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id726in_a = id44out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id726in_b = id961out_value;

    id726out_result[(getCycle()+1)%2] = (eq_fixed(id726in_a,id726in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id643out_o;

  { // Node ID: 643 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id643in_i = id772out_output[getCycle()%31];

    id643out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id643in_i));
  }
  { // Node ID: 931 (NodeFIFO)
    const HWFloat<11,53> &id931in_input = id876out_output[getCycle()%167];

    id931out_output[(getCycle()+7)%8] = id931in_input;
  }
  HWFloat<11,53> id767out_output;

  { // Node ID: 767 (NodeStreamOffset)
    const HWFloat<11,53> &id767in_input = id931out_output[getCycle()%8];

    id767out_output = id767in_input;
  }
  { // Node ID: 875 (NodeFIFO)
    const HWFloat<11,53> &id875in_input = id767out_output;

    id875out_output[(getCycle()+62)%63] = id875in_input;
  }
  { // Node ID: 960 (NodeConstantRawBits)
  }
  { // Node ID: 645 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id645in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id645in_b = id960out_value;

    id645out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id645in_a,id645in_b));
  }
  { // Node ID: 727 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id727in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id727in_b = id645out_result[getCycle()%2];

    id727out_result[(getCycle()+1)%2] = (eq_fixed(id727in_a,id727in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id52out_o;

  { // Node ID: 52 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id52in_i = id45out_count;

    id52out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id52in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 681 (NodeRAM)
    const bool id681_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<20,0,UNSIGNED> &id681in_addrA = id643out_o;
    const HWFloat<11,53> &id681in_dina = id875out_output[getCycle()%63];
    const HWOffsetFix<1,0,UNSIGNED> &id681in_wea = id727out_result[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id681in_addrB = id52out_o;

    long id681x_1;
    long id681x_2;
    HWFloat<11,53> id681x_3;

    (id681x_1) = (id681in_addrA.getValueAsLong());
    (id681x_2) = (id681in_addrB.getValueAsLong());
    switch(((long)((id681x_2)<(1000000l)))) {
      case 0l:
        id681x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id681x_3 = (id681sta_ram_store[(id681x_2)]);
        break;
      default:
        id681x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id681out_doutb[(getCycle()+2)%3] = (id681x_3);
    if(((id681in_wea.getValueAsBool())&id681_inputvalid)) {
      if(((id681x_1)<(1000000l))) {
        (id681sta_ram_store[(id681x_1)]) = id681in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_6("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 681) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 72 (NodeConstantRawBits)
  }
  { // Node ID: 73 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id73in_sel = id726out_result[getCycle()%2];
    const HWFloat<11,53> &id73in_option0 = id681out_doutb[getCycle()%3];
    const HWFloat<11,53> &id73in_option1 = id72out_value;

    HWFloat<11,53> id73x_1;

    switch((id73in_sel.getValueAsLong())) {
      case 0l:
        id73x_1 = id73in_option0;
        break;
      case 1l:
        id73x_1 = id73in_option1;
        break;
      default:
        id73x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id73out_result[(getCycle()+1)%2] = (id73x_1);
  }
  { // Node ID: 75 (NodeAdd)
    const HWFloat<11,53> &id75in_a = id73out_result[getCycle()%2];
    const HWFloat<11,53> &id75in_b = id36out_dt;

    id75out_result[(getCycle()+14)%15] = (add_float(id75in_a,id75in_b));
  }
  { // Node ID: 876 (NodeFIFO)
    const HWFloat<11,53> &id876in_input = id75out_result[getCycle()%15];

    id876out_output[(getCycle()+166)%167] = id876in_input;
  }
  { // Node ID: 959 (NodeConstantRawBits)
  }
  { // Node ID: 94 (NodeSub)
    const HWFloat<11,53> &id94in_a = id876out_output[getCycle()%167];
    const HWFloat<11,53> &id94in_b = id959out_value;

    id94out_result[(getCycle()+7)%8] = (sub_float(id94in_a,id94in_b));
  }
  { // Node ID: 958 (NodeConstantRawBits)
  }
  { // Node ID: 101 (NodeEq)
    const HWFloat<11,53> &id101in_a = id94out_result[getCycle()%8];
    const HWFloat<11,53> &id101in_b = id958out_value;

    id101out_result[(getCycle()+1)%2] = (eq_float(id101in_a,id101in_b));
  }
  { // Node ID: 103 (NodeConstantRawBits)
  }
  { // Node ID: 102 (NodeConstantRawBits)
  }
  { // Node ID: 104 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id104in_sel = id101out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id104in_option0 = id103out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id104in_option1 = id102out_value;

    HWOffsetFix<1,0,UNSIGNED> id104x_1;

    switch((id104in_sel.getValueAsLong())) {
      case 0l:
        id104x_1 = id104in_option0;
        break;
      case 1l:
        id104x_1 = id104in_option1;
        break;
      default:
        id104x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id104out_result[(getCycle()+1)%2] = (id104x_1);
  }
  { // Node ID: 877 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id877in_input = id104out_result[getCycle()%2];

    id877out_output[(getCycle()+1)%2] = id877in_input;
  }
  { // Node ID: 957 (NodeConstantRawBits)
  }
  { // Node ID: 96 (NodeLt)
    const HWFloat<11,53> &id96in_a = id94out_result[getCycle()%8];
    const HWFloat<11,53> &id96in_b = id957out_value;

    id96out_result[(getCycle()+1)%2] = (lt_float(id96in_a,id96in_b));
  }
  { // Node ID: 98 (NodeConstantRawBits)
  }
  { // Node ID: 97 (NodeConstantRawBits)
  }
  { // Node ID: 99 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id99in_sel = id96out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id99in_option0 = id98out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id99in_option1 = id97out_value;

    HWOffsetFix<1,0,UNSIGNED> id99x_1;

    switch((id99in_sel.getValueAsLong())) {
      case 0l:
        id99x_1 = id99in_option0;
        break;
      case 1l:
        id99x_1 = id99in_option1;
        break;
      default:
        id99x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id99out_result[(getCycle()+1)%2] = (id99x_1);
  }
  { // Node ID: 106 (NodeConstantRawBits)
  }
  { // Node ID: 105 (NodeConstantRawBits)
  }
  { // Node ID: 107 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id107in_sel = id99out_result[getCycle()%2];
    const HWFloat<11,53> &id107in_option0 = id106out_value;
    const HWFloat<11,53> &id107in_option1 = id105out_value;

    HWFloat<11,53> id107x_1;

    switch((id107in_sel.getValueAsLong())) {
      case 0l:
        id107x_1 = id107in_option0;
        break;
      case 1l:
        id107x_1 = id107in_option1;
        break;
      default:
        id107x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id107out_result[(getCycle()+1)%2] = (id107x_1);
  }
  { // Node ID: 108 (NodeConstantRawBits)
  }
  { // Node ID: 109 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id109in_sel = id877out_output[getCycle()%2];
    const HWFloat<11,53> &id109in_option0 = id107out_result[getCycle()%2];
    const HWFloat<11,53> &id109in_option1 = id108out_value;

    HWFloat<11,53> id109x_1;

    switch((id109in_sel.getValueAsLong())) {
      case 0l:
        id109x_1 = id109in_option0;
        break;
      case 1l:
        id109x_1 = id109in_option1;
        break;
      default:
        id109x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id109out_result[(getCycle()+1)%2] = (id109x_1);
  }
  { // Node ID: 111 (NodeSub)
    const HWFloat<11,53> &id111in_a = id962out_value;
    const HWFloat<11,53> &id111in_b = id109out_result[getCycle()%2];

    id111out_result[(getCycle()+7)%8] = (sub_float(id111in_a,id111in_b));
  }
  { // Node ID: 112 (NodeMul)
    const HWFloat<11,53> &id112in_a = id92out_result[getCycle()%2];
    const HWFloat<11,53> &id112in_b = id111out_result[getCycle()%8];

    id112out_result[(getCycle()+6)%7] = (mul_float(id112in_a,id112in_b));
  }
  { // Node ID: 956 (NodeConstantRawBits)
  }
  { // Node ID: 114 (NodeSub)
    const HWFloat<11,53> &id114in_a = id74out_result[getCycle()%15];
    const HWFloat<11,53> &id114in_b = id956out_value;

    id114out_result[(getCycle()+7)%8] = (sub_float(id114in_a,id114in_b));
  }
  { // Node ID: 955 (NodeConstantRawBits)
  }
  { // Node ID: 121 (NodeEq)
    const HWFloat<11,53> &id121in_a = id114out_result[getCycle()%8];
    const HWFloat<11,53> &id121in_b = id955out_value;

    id121out_result[(getCycle()+1)%2] = (eq_float(id121in_a,id121in_b));
  }
  { // Node ID: 123 (NodeConstantRawBits)
  }
  { // Node ID: 122 (NodeConstantRawBits)
  }
  { // Node ID: 124 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id124in_sel = id121out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id124in_option0 = id123out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id124in_option1 = id122out_value;

    HWOffsetFix<1,0,UNSIGNED> id124x_1;

    switch((id124in_sel.getValueAsLong())) {
      case 0l:
        id124x_1 = id124in_option0;
        break;
      case 1l:
        id124x_1 = id124in_option1;
        break;
      default:
        id124x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id124out_result[(getCycle()+1)%2] = (id124x_1);
  }
  { // Node ID: 878 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id878in_input = id124out_result[getCycle()%2];

    id878out_output[(getCycle()+1)%2] = id878in_input;
  }
  { // Node ID: 954 (NodeConstantRawBits)
  }
  { // Node ID: 116 (NodeLt)
    const HWFloat<11,53> &id116in_a = id114out_result[getCycle()%8];
    const HWFloat<11,53> &id116in_b = id954out_value;

    id116out_result[(getCycle()+1)%2] = (lt_float(id116in_a,id116in_b));
  }
  { // Node ID: 118 (NodeConstantRawBits)
  }
  { // Node ID: 117 (NodeConstantRawBits)
  }
  { // Node ID: 119 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id119in_sel = id116out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id119in_option0 = id118out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id119in_option1 = id117out_value;

    HWOffsetFix<1,0,UNSIGNED> id119x_1;

    switch((id119in_sel.getValueAsLong())) {
      case 0l:
        id119x_1 = id119in_option0;
        break;
      case 1l:
        id119x_1 = id119in_option1;
        break;
      default:
        id119x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id119out_result[(getCycle()+1)%2] = (id119x_1);
  }
  { // Node ID: 126 (NodeConstantRawBits)
  }
  { // Node ID: 125 (NodeConstantRawBits)
  }
  { // Node ID: 127 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id127in_sel = id119out_result[getCycle()%2];
    const HWFloat<11,53> &id127in_option0 = id126out_value;
    const HWFloat<11,53> &id127in_option1 = id125out_value;

    HWFloat<11,53> id127x_1;

    switch((id127in_sel.getValueAsLong())) {
      case 0l:
        id127x_1 = id127in_option0;
        break;
      case 1l:
        id127x_1 = id127in_option1;
        break;
      default:
        id127x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id127out_result[(getCycle()+1)%2] = (id127x_1);
  }
  { // Node ID: 128 (NodeConstantRawBits)
  }
  { // Node ID: 129 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id129in_sel = id878out_output[getCycle()%2];
    const HWFloat<11,53> &id129in_option0 = id127out_result[getCycle()%2];
    const HWFloat<11,53> &id129in_option1 = id128out_value;

    HWFloat<11,53> id129x_1;

    switch((id129in_sel.getValueAsLong())) {
      case 0l:
        id129x_1 = id129in_option0;
        break;
      case 1l:
        id129x_1 = id129in_option1;
        break;
      default:
        id129x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id129out_result[(getCycle()+1)%2] = (id129x_1);
  }
  { // Node ID: 953 (NodeConstantRawBits)
  }
  { // Node ID: 952 (NodeConstantRawBits)
  }
  { // Node ID: 131 (NodeSub)
    const HWFloat<11,53> &id131in_a = id876out_output[getCycle()%167];
    const HWFloat<11,53> &id131in_b = id952out_value;

    id131out_result[(getCycle()+7)%8] = (sub_float(id131in_a,id131in_b));
  }
  { // Node ID: 951 (NodeConstantRawBits)
  }
  { // Node ID: 138 (NodeEq)
    const HWFloat<11,53> &id138in_a = id131out_result[getCycle()%8];
    const HWFloat<11,53> &id138in_b = id951out_value;

    id138out_result[(getCycle()+1)%2] = (eq_float(id138in_a,id138in_b));
  }
  { // Node ID: 140 (NodeConstantRawBits)
  }
  { // Node ID: 139 (NodeConstantRawBits)
  }
  { // Node ID: 141 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id141in_sel = id138out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id141in_option0 = id140out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id141in_option1 = id139out_value;

    HWOffsetFix<1,0,UNSIGNED> id141x_1;

    switch((id141in_sel.getValueAsLong())) {
      case 0l:
        id141x_1 = id141in_option0;
        break;
      case 1l:
        id141x_1 = id141in_option1;
        break;
      default:
        id141x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id141out_result[(getCycle()+1)%2] = (id141x_1);
  }
  { // Node ID: 880 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id880in_input = id141out_result[getCycle()%2];

    id880out_output[(getCycle()+1)%2] = id880in_input;
  }
  { // Node ID: 950 (NodeConstantRawBits)
  }
  { // Node ID: 133 (NodeLt)
    const HWFloat<11,53> &id133in_a = id131out_result[getCycle()%8];
    const HWFloat<11,53> &id133in_b = id950out_value;

    id133out_result[(getCycle()+1)%2] = (lt_float(id133in_a,id133in_b));
  }
  { // Node ID: 135 (NodeConstantRawBits)
  }
  { // Node ID: 134 (NodeConstantRawBits)
  }
  { // Node ID: 136 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id136in_sel = id133out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id136in_option0 = id135out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id136in_option1 = id134out_value;

    HWOffsetFix<1,0,UNSIGNED> id136x_1;

    switch((id136in_sel.getValueAsLong())) {
      case 0l:
        id136x_1 = id136in_option0;
        break;
      case 1l:
        id136x_1 = id136in_option1;
        break;
      default:
        id136x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id136out_result[(getCycle()+1)%2] = (id136x_1);
  }
  { // Node ID: 143 (NodeConstantRawBits)
  }
  { // Node ID: 142 (NodeConstantRawBits)
  }
  { // Node ID: 144 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id144in_sel = id136out_result[getCycle()%2];
    const HWFloat<11,53> &id144in_option0 = id143out_value;
    const HWFloat<11,53> &id144in_option1 = id142out_value;

    HWFloat<11,53> id144x_1;

    switch((id144in_sel.getValueAsLong())) {
      case 0l:
        id144x_1 = id144in_option0;
        break;
      case 1l:
        id144x_1 = id144in_option1;
        break;
      default:
        id144x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id144out_result[(getCycle()+1)%2] = (id144x_1);
  }
  { // Node ID: 145 (NodeConstantRawBits)
  }
  { // Node ID: 146 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id146in_sel = id880out_output[getCycle()%2];
    const HWFloat<11,53> &id146in_option0 = id144out_result[getCycle()%2];
    const HWFloat<11,53> &id146in_option1 = id145out_value;

    HWFloat<11,53> id146x_1;

    switch((id146in_sel.getValueAsLong())) {
      case 0l:
        id146x_1 = id146in_option0;
        break;
      case 1l:
        id146x_1 = id146in_option1;
        break;
      default:
        id146x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id146out_result[(getCycle()+1)%2] = (id146x_1);
  }
  { // Node ID: 148 (NodeSub)
    const HWFloat<11,53> &id148in_a = id953out_value;
    const HWFloat<11,53> &id148in_b = id146out_result[getCycle()%2];

    id148out_result[(getCycle()+7)%8] = (sub_float(id148in_a,id148in_b));
  }
  { // Node ID: 149 (NodeMul)
    const HWFloat<11,53> &id149in_a = id129out_result[getCycle()%2];
    const HWFloat<11,53> &id149in_b = id148out_result[getCycle()%8];

    id149out_result[(getCycle()+6)%7] = (mul_float(id149in_a,id149in_b));
  }
  { // Node ID: 150 (NodeAdd)
    const HWFloat<11,53> &id150in_a = id112out_result[getCycle()%7];
    const HWFloat<11,53> &id150in_b = id149out_result[getCycle()%7];

    id150out_result[(getCycle()+7)%8] = (add_float(id150in_a,id150in_b));
  }
  { // Node ID: 949 (NodeConstantRawBits)
  }
  { // Node ID: 152 (NodeSub)
    const HWFloat<11,53> &id152in_a = id872out_output[getCycle()%8];
    const HWFloat<11,53> &id152in_b = id949out_value;

    id152out_result[(getCycle()+7)%8] = (sub_float(id152in_a,id152in_b));
  }
  { // Node ID: 948 (NodeConstantRawBits)
  }
  { // Node ID: 159 (NodeEq)
    const HWFloat<11,53> &id159in_a = id152out_result[getCycle()%8];
    const HWFloat<11,53> &id159in_b = id948out_value;

    id159out_result[(getCycle()+1)%2] = (eq_float(id159in_a,id159in_b));
  }
  { // Node ID: 161 (NodeConstantRawBits)
  }
  { // Node ID: 160 (NodeConstantRawBits)
  }
  { // Node ID: 162 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id162in_sel = id159out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id162in_option0 = id161out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id162in_option1 = id160out_value;

    HWOffsetFix<1,0,UNSIGNED> id162x_1;

    switch((id162in_sel.getValueAsLong())) {
      case 0l:
        id162x_1 = id162in_option0;
        break;
      case 1l:
        id162x_1 = id162in_option1;
        break;
      default:
        id162x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id162out_result[(getCycle()+1)%2] = (id162x_1);
  }
  { // Node ID: 882 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id882in_input = id162out_result[getCycle()%2];

    id882out_output[(getCycle()+1)%2] = id882in_input;
  }
  { // Node ID: 947 (NodeConstantRawBits)
  }
  { // Node ID: 154 (NodeLt)
    const HWFloat<11,53> &id154in_a = id152out_result[getCycle()%8];
    const HWFloat<11,53> &id154in_b = id947out_value;

    id154out_result[(getCycle()+1)%2] = (lt_float(id154in_a,id154in_b));
  }
  { // Node ID: 156 (NodeConstantRawBits)
  }
  { // Node ID: 155 (NodeConstantRawBits)
  }
  { // Node ID: 157 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id157in_sel = id154out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id157in_option0 = id156out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id157in_option1 = id155out_value;

    HWOffsetFix<1,0,UNSIGNED> id157x_1;

    switch((id157in_sel.getValueAsLong())) {
      case 0l:
        id157x_1 = id157in_option0;
        break;
      case 1l:
        id157x_1 = id157in_option1;
        break;
      default:
        id157x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id157out_result[(getCycle()+1)%2] = (id157x_1);
  }
  { // Node ID: 164 (NodeConstantRawBits)
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  { // Node ID: 165 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id165in_sel = id157out_result[getCycle()%2];
    const HWFloat<11,53> &id165in_option0 = id164out_value;
    const HWFloat<11,53> &id165in_option1 = id163out_value;

    HWFloat<11,53> id165x_1;

    switch((id165in_sel.getValueAsLong())) {
      case 0l:
        id165x_1 = id165in_option0;
        break;
      case 1l:
        id165x_1 = id165in_option1;
        break;
      default:
        id165x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id165out_result[(getCycle()+1)%2] = (id165x_1);
  }
  { // Node ID: 166 (NodeConstantRawBits)
  }
  { // Node ID: 167 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id167in_sel = id882out_output[getCycle()%2];
    const HWFloat<11,53> &id167in_option0 = id165out_result[getCycle()%2];
    const HWFloat<11,53> &id167in_option1 = id166out_value;

    HWFloat<11,53> id167x_1;

    switch((id167in_sel.getValueAsLong())) {
      case 0l:
        id167x_1 = id167in_option0;
        break;
      case 1l:
        id167x_1 = id167in_option1;
        break;
      default:
        id167x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id167out_result[(getCycle()+1)%2] = (id167x_1);
  }
  { // Node ID: 946 (NodeConstantRawBits)
  }
  { // Node ID: 945 (NodeConstantRawBits)
  }
  { // Node ID: 169 (NodeSub)
    const HWFloat<11,53> &id169in_a = id931out_output[getCycle()%8];
    const HWFloat<11,53> &id169in_b = id945out_value;

    id169out_result[(getCycle()+7)%8] = (sub_float(id169in_a,id169in_b));
  }
  { // Node ID: 944 (NodeConstantRawBits)
  }
  { // Node ID: 176 (NodeEq)
    const HWFloat<11,53> &id176in_a = id169out_result[getCycle()%8];
    const HWFloat<11,53> &id176in_b = id944out_value;

    id176out_result[(getCycle()+1)%2] = (eq_float(id176in_a,id176in_b));
  }
  { // Node ID: 178 (NodeConstantRawBits)
  }
  { // Node ID: 177 (NodeConstantRawBits)
  }
  { // Node ID: 179 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id179in_sel = id176out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id179in_option0 = id178out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id179in_option1 = id177out_value;

    HWOffsetFix<1,0,UNSIGNED> id179x_1;

    switch((id179in_sel.getValueAsLong())) {
      case 0l:
        id179x_1 = id179in_option0;
        break;
      case 1l:
        id179x_1 = id179in_option1;
        break;
      default:
        id179x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id179out_result[(getCycle()+1)%2] = (id179x_1);
  }
  { // Node ID: 884 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id884in_input = id179out_result[getCycle()%2];

    id884out_output[(getCycle()+1)%2] = id884in_input;
  }
  { // Node ID: 943 (NodeConstantRawBits)
  }
  { // Node ID: 171 (NodeLt)
    const HWFloat<11,53> &id171in_a = id169out_result[getCycle()%8];
    const HWFloat<11,53> &id171in_b = id943out_value;

    id171out_result[(getCycle()+1)%2] = (lt_float(id171in_a,id171in_b));
  }
  { // Node ID: 173 (NodeConstantRawBits)
  }
  { // Node ID: 172 (NodeConstantRawBits)
  }
  { // Node ID: 174 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id174in_sel = id171out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id174in_option0 = id173out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id174in_option1 = id172out_value;

    HWOffsetFix<1,0,UNSIGNED> id174x_1;

    switch((id174in_sel.getValueAsLong())) {
      case 0l:
        id174x_1 = id174in_option0;
        break;
      case 1l:
        id174x_1 = id174in_option1;
        break;
      default:
        id174x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id174out_result[(getCycle()+1)%2] = (id174x_1);
  }
  { // Node ID: 181 (NodeConstantRawBits)
  }
  { // Node ID: 180 (NodeConstantRawBits)
  }
  { // Node ID: 182 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id182in_sel = id174out_result[getCycle()%2];
    const HWFloat<11,53> &id182in_option0 = id181out_value;
    const HWFloat<11,53> &id182in_option1 = id180out_value;

    HWFloat<11,53> id182x_1;

    switch((id182in_sel.getValueAsLong())) {
      case 0l:
        id182x_1 = id182in_option0;
        break;
      case 1l:
        id182x_1 = id182in_option1;
        break;
      default:
        id182x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id182out_result[(getCycle()+1)%2] = (id182x_1);
  }
  { // Node ID: 183 (NodeConstantRawBits)
  }
  { // Node ID: 184 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id184in_sel = id884out_output[getCycle()%2];
    const HWFloat<11,53> &id184in_option0 = id182out_result[getCycle()%2];
    const HWFloat<11,53> &id184in_option1 = id183out_value;

    HWFloat<11,53> id184x_1;

    switch((id184in_sel.getValueAsLong())) {
      case 0l:
        id184x_1 = id184in_option0;
        break;
      case 1l:
        id184x_1 = id184in_option1;
        break;
      default:
        id184x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id184out_result[(getCycle()+1)%2] = (id184x_1);
  }
  { // Node ID: 186 (NodeSub)
    const HWFloat<11,53> &id186in_a = id946out_value;
    const HWFloat<11,53> &id186in_b = id184out_result[getCycle()%2];

    id186out_result[(getCycle()+7)%8] = (sub_float(id186in_a,id186in_b));
  }
  { // Node ID: 187 (NodeMul)
    const HWFloat<11,53> &id187in_a = id167out_result[getCycle()%2];
    const HWFloat<11,53> &id187in_b = id186out_result[getCycle()%8];

    id187out_result[(getCycle()+6)%7] = (mul_float(id187in_a,id187in_b));
  }
  { // Node ID: 188 (NodeAdd)
    const HWFloat<11,53> &id188in_a = id150out_result[getCycle()%8];
    const HWFloat<11,53> &id188in_b = id187out_result[getCycle()%7];

    id188out_result[(getCycle()+7)%8] = (add_float(id188in_a,id188in_b));
  }
  { // Node ID: 942 (NodeConstantRawBits)
  }
  { // Node ID: 190 (NodeEq)
    const HWFloat<11,53> &id190in_a = id188out_result[getCycle()%8];
    const HWFloat<11,53> &id190in_b = id942out_value;

    id190out_result[(getCycle()+2)%3] = (eq_float(id190in_a,id190in_b));
  }
  { // Node ID: 941 (NodeConstantRawBits)
  }
  { // Node ID: 728 (NodeLtInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id728in_a = id907out_output[getCycle()%199];
    const HWOffsetFix<32,0,UNSIGNED> &id728in_b = id941out_value;

    id728out_result[(getCycle()+1)%2] = (lt_fixed(id728in_a,id728in_b));
  }
  { // Node ID: 193 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id193in_a = id190out_result[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id193in_b = id728out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id193x_1;

    (id193x_1) = (and_fixed(id193in_a,id193in_b));
    id193out_result[(getCycle()+1)%2] = (id193x_1);
  }
  { // Node ID: 195 (NodeConstantRawBits)
  }
  { // Node ID: 194 (NodeConstantRawBits)
  }
  { // Node ID: 196 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id196in_sel = id193out_result[getCycle()%2];
    const HWFloat<11,53> &id196in_option0 = id195out_value;
    const HWFloat<11,53> &id196in_option1 = id194out_value;

    HWFloat<11,53> id196x_1;

    switch((id196in_sel.getValueAsLong())) {
      case 0l:
        id196x_1 = id196in_option0;
        break;
      case 1l:
        id196x_1 = id196in_option1;
        break;
      default:
        id196x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id196out_result[(getCycle()+1)%2] = (id196x_1);
  }
  { // Node ID: 612 (NodeSub)
    const HWFloat<11,53> &id612in_a = id611out_result[getCycle()%8];
    const HWFloat<11,53> &id612in_b = id196out_result[getCycle()%2];

    id612out_result[(getCycle()+7)%8] = (sub_float(id612in_a,id612in_b));
  }
  { // Node ID: 613 (NodeMul)
    const HWFloat<11,53> &id613in_a = id612out_result[getCycle()%8];
    const HWFloat<11,53> &id613in_b = id36out_dt;

    id613out_result[(getCycle()+6)%7] = (mul_float(id613in_a,id613in_b));
  }
  { // Node ID: 614 (NodeSub)
    const HWFloat<11,53> &id614in_a = id921out_output[getCycle()%23];
    const HWFloat<11,53> &id614in_b = id613out_result[getCycle()%7];

    id614out_result[(getCycle()+7)%8] = (sub_float(id614in_a,id614in_b));
  }
  { // Node ID: 940 (NodeConstantRawBits)
  }
  { // Node ID: 729 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id729in_a = id906out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id729in_b = id940out_value;

    id729out_result[(getCycle()+1)%2] = (eq_fixed(id729in_a,id729in_b));
  }
  { // Node ID: 939 (NodeConstantRawBits)
  }
  { // Node ID: 209 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id209in_a = id38out_nx;
    const HWOffsetFix<32,0,UNSIGNED> &id209in_b = id939out_value;

    id209out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id209in_a,id209in_b));
  }
  { // Node ID: 730 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id730in_a = id888out_output[getCycle()%24];
    const HWOffsetFix<32,0,UNSIGNED> &id730in_b = id209out_result[getCycle()%2];

    id730out_result[(getCycle()+1)%2] = (eq_fixed(id730in_a,id730in_b));
  }
  { // Node ID: 37 (NodeInputMappedReg)
  }
  { // Node ID: 889 (NodeFIFO)
    const HWFloat<11,53> &id889in_input = id762out_output;

    id889out_output[(getCycle()+1)%2] = id889in_input;
  }
  HWFloat<11,53> id197out_output;

  { // Node ID: 197 (NodeStreamOffset)
    const HWFloat<11,53> &id197in_input = id889out_output[getCycle()%2];

    id197out_output = id197in_input;
  }
  { // Node ID: 894 (NodeFIFO)
    const HWFloat<11,53> &id894in_input = id197out_output;

    id894out_output[(getCycle()+4)%5] = id894in_input;
  }
  { // Node ID: 890 (NodeFIFO)
    const HWOffsetFix<20,0,UNSIGNED> &id890in_input = id623out_o;

    id890out_output[(getCycle()+1)%2] = id890in_input;
  }
  { // Node ID: 892 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id892in_input = id699out_result[getCycle()%2];

    id892out_output[(getCycle()+1)%2] = id892in_input;
  }
  { // Node ID: 938 (NodeConstantRawBits)
  }
  { // Node ID: 54 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id54in_a = id45out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id54in_b = id938out_value;

    id54out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id54in_a,id54in_b));
  }
  HWOffsetFix<20,0,UNSIGNED> id55out_o;

  { // Node ID: 55 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id55in_i = id54out_result[getCycle()%2];

    id55out_o = (cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(id55in_i));
  }
  if ( (getFillLevel() >= (5l)))
  { // Node ID: 676 (NodeRAM)
    const bool id676_inputvalid = !(isFlushingActive() && getFlushLevel() >= (5l));
    const HWOffsetFix<20,0,UNSIGNED> &id676in_addrA = id890out_output[getCycle()%2];
    const HWFloat<11,53> &id676in_dina = id889out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id676in_wea = id892out_output[getCycle()%2];
    const HWOffsetFix<20,0,UNSIGNED> &id676in_addrB = id55out_o;

    long id676x_1;
    long id676x_2;
    HWFloat<11,53> id676x_3;

    (id676x_1) = (id676in_addrA.getValueAsLong());
    (id676x_2) = (id676in_addrB.getValueAsLong());
    switch(((long)((id676x_2)<(1000000l)))) {
      case 0l:
        id676x_3 = (c_hw_flt_11_53_undef);
        break;
      case 1l:
        id676x_3 = (id676sta_ram_store[(id676x_2)]);
        break;
      default:
        id676x_3 = (c_hw_flt_11_53_undef);
        break;
    }
    id676out_doutb[(getCycle()+2)%3] = (id676x_3);
    if(((id676in_wea.getValueAsBool())&id676_inputvalid)) {
      if(((id676x_1)<(1000000l))) {
        (id676sta_ram_store[(id676x_1)]) = id676in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFEKernel_7("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 676) on port A, ram depth is 1000000.")));
      }
    }
  }
  { // Node ID: 198 (NodeConstantRawBits)
  }
  { // Node ID: 199 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id199in_sel = id777out_output[getCycle()%2];
    const HWFloat<11,53> &id199in_option0 = id676out_doutb[getCycle()%3];
    const HWFloat<11,53> &id199in_option1 = id198out_value;

    HWFloat<11,53> id199x_1;

    switch((id199in_sel.getValueAsLong())) {
      case 0l:
        id199x_1 = id199in_option0;
        break;
      case 1l:
        id199x_1 = id199in_option1;
        break;
      default:
        id199x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id199out_result[(getCycle()+1)%2] = (id199x_1);
  }
  { // Node ID: 217 (NodeAdd)
    const HWFloat<11,53> &id217in_a = id894out_output[getCycle()%5];
    const HWFloat<11,53> &id217in_b = id199out_result[getCycle()%2];

    id217out_result[(getCycle()+7)%8] = (add_float(id217in_a,id217in_b));
  }
  { // Node ID: 748 (NodePO2FPMult)
    const HWFloat<11,53> &id748in_floatIn = id908out_output[getCycle()%7];

    id748out_floatOut[(getCycle()+1)%2] = (mul_float(id748in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 220 (NodeSub)
    const HWFloat<11,53> &id220in_a = id217out_result[getCycle()%8];
    const HWFloat<11,53> &id220in_b = id748out_floatOut[getCycle()%2];

    id220out_result[(getCycle()+7)%8] = (sub_float(id220in_a,id220in_b));
  }
  { // Node ID: 221 (NodeMul)
    const HWFloat<11,53> &id221in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id221in_b = id220out_result[getCycle()%8];

    id221out_result[(getCycle()+6)%7] = (mul_float(id221in_a,id221in_b));
  }
  { // Node ID: 749 (NodePO2FPMult)
    const HWFloat<11,53> &id749in_floatIn = id896out_output[getCycle()%2];

    id749out_floatOut[(getCycle()+1)%2] = (mul_float(id749in_floatIn,(c_hw_flt_11_53_n2_0val)));
  }
  { // Node ID: 750 (NodePO2FPMult)
    const HWFloat<11,53> &id750in_floatIn = id894out_output[getCycle()%5];

    id750out_floatOut[(getCycle()+1)%2] = (mul_float(id750in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 215 (NodeAdd)
    const HWFloat<11,53> &id215in_a = id749out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id215in_b = id750out_floatOut[getCycle()%2];

    id215out_result[(getCycle()+7)%8] = (add_float(id215in_a,id215in_b));
  }
  { // Node ID: 216 (NodeMul)
    const HWFloat<11,53> &id216in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id216in_b = id215out_result[getCycle()%8];

    id216out_result[(getCycle()+6)%7] = (mul_float(id216in_a,id216in_b));
  }
  { // Node ID: 898 (NodeFIFO)
    const HWFloat<11,53> &id898in_input = id216out_result[getCycle()%7];

    id898out_output[(getCycle()+6)%7] = id898in_input;
  }
  { // Node ID: 222 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id222in_sel = id730out_result[getCycle()%2];
    const HWFloat<11,53> &id222in_option0 = id221out_result[getCycle()%7];
    const HWFloat<11,53> &id222in_option1 = id898out_output[getCycle()%7];

    HWFloat<11,53> id222x_1;

    switch((id222in_sel.getValueAsLong())) {
      case 0l:
        id222x_1 = id222in_option0;
        break;
      case 1l:
        id222x_1 = id222in_option1;
        break;
      default:
        id222x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id222out_result[(getCycle()+1)%2] = (id222x_1);
  }
  { // Node ID: 751 (NodePO2FPMult)
    const HWFloat<11,53> &id751in_floatIn = id896out_output[getCycle()%2];

    id751out_floatOut[(getCycle()+1)%2] = (mul_float(id751in_floatIn,(c_hw_flt_11_53_n2_0val)));
  }
  { // Node ID: 752 (NodePO2FPMult)
    const HWFloat<11,53> &id752in_floatIn = id199out_result[getCycle()%2];

    id752out_floatOut[(getCycle()+1)%2] = (mul_float(id752in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  { // Node ID: 206 (NodeAdd)
    const HWFloat<11,53> &id206in_a = id751out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id206in_b = id752out_floatOut[getCycle()%2];

    id206out_result[(getCycle()+7)%8] = (add_float(id206in_a,id206in_b));
  }
  { // Node ID: 207 (NodeMul)
    const HWFloat<11,53> &id207in_a = id37out_ddt_o_dx2;
    const HWFloat<11,53> &id207in_b = id206out_result[getCycle()%8];

    id207out_result[(getCycle()+6)%7] = (mul_float(id207in_a,id207in_b));
  }
  { // Node ID: 900 (NodeFIFO)
    const HWFloat<11,53> &id900in_input = id207out_result[getCycle()%7];

    id900out_output[(getCycle()+7)%8] = id900in_input;
  }
  { // Node ID: 223 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id223in_sel = id729out_result[getCycle()%2];
    const HWFloat<11,53> &id223in_option0 = id222out_result[getCycle()%2];
    const HWFloat<11,53> &id223in_option1 = id900out_output[getCycle()%8];

    HWFloat<11,53> id223x_1;

    switch((id223in_sel.getValueAsLong())) {
      case 0l:
        id223x_1 = id223in_option0;
        break;
      case 1l:
        id223x_1 = id223in_option1;
        break;
      default:
        id223x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id223out_result[(getCycle()+1)%2] = (id223x_1);
  }
  { // Node ID: 901 (NodeFIFO)
    const HWFloat<11,53> &id901in_input = id223out_result[getCycle()%2];

    id901out_output[(getCycle()+219)%220] = id901in_input;
  }
  { // Node ID: 615 (NodeAdd)
    const HWFloat<11,53> &id615in_a = id614out_result[getCycle()%8];
    const HWFloat<11,53> &id615in_b = id901out_output[getCycle()%220];

    id615out_result[(getCycle()+7)%8] = (add_float(id615in_a,id615in_b));
  }
  if ( (getFillLevel() >= (256l)) && (getFlushLevel() < (256l)|| !isFlushingActive() ))
  { // Node ID: 653 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id653in_output_control = id902out_output[getCycle()%253];
    const HWFloat<11,53> &id653in_data = id615out_result[getCycle()%8];

    bool id653x_1;

    (id653x_1) = ((id653in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(256l))&(isFlushingActive()))));
    if((id653x_1)) {
      writeOutput(m_u_out, id653in_data);
    }
  }
  { // Node ID: 937 (NodeConstantRawBits)
  }
  { // Node ID: 655 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id655in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id655in_b = id937out_value;

    id655out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id655in_a,id655in_b));
  }
  { // Node ID: 731 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id731in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id731in_b = id655out_result[getCycle()%2];

    id731out_result[(getCycle()+1)%2] = (eq_fixed(id731in_a,id731in_b));
  }
  { // Node ID: 657 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id658out_result;

  { // Node ID: 658 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_a = id657out_io_v_out_force_disabled;

    id658out_result = (not_fixed(id658in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id659out_result;

  { // Node ID: 659 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id659in_a = id731out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id659in_b = id658out_result;

    HWOffsetFix<1,0,UNSIGNED> id659x_1;

    (id659x_1) = (and_fixed(id659in_a,id659in_b));
    id659out_result = (id659x_1);
  }
  { // Node ID: 903 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id903in_input = id659out_result;

    id903out_output[(getCycle()+69)%70] = id903in_input;
  }
  if ( (getFillLevel() >= (73l)) && (getFlushLevel() < (73l)|| !isFlushingActive() ))
  { // Node ID: 660 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id660in_output_control = id903out_output[getCycle()%70];
    const HWFloat<11,53> &id660in_data = id585out_result[getCycle()%15];

    bool id660x_1;

    (id660x_1) = ((id660in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(73l))&(isFlushingActive()))));
    if((id660x_1)) {
      writeOutput(m_v_out, id660in_data);
    }
  }
  { // Node ID: 936 (NodeConstantRawBits)
  }
  { // Node ID: 662 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id662in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id662in_b = id936out_value;

    id662out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id662in_a,id662in_b));
  }
  { // Node ID: 732 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id732in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id732in_b = id662out_result[getCycle()%2];

    id732out_result[(getCycle()+1)%2] = (eq_fixed(id732in_a,id732in_b));
  }
  { // Node ID: 664 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id665out_result;

  { // Node ID: 665 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id665in_a = id664out_io_w_out_force_disabled;

    id665out_result = (not_fixed(id665in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id666out_result;

  { // Node ID: 666 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id666in_a = id732out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id666in_b = id665out_result;

    HWOffsetFix<1,0,UNSIGNED> id666x_1;

    (id666x_1) = (and_fixed(id666in_a,id666in_b));
    id666out_result = (id666x_1);
  }
  { // Node ID: 904 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id904in_input = id666out_result;

    id904out_output[(getCycle()+183)%184] = id904in_input;
  }
  if ( (getFillLevel() >= (187l)) && (getFlushLevel() < (187l)|| !isFlushingActive() ))
  { // Node ID: 667 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id667in_output_control = id904out_output[getCycle()%184];
    const HWFloat<11,53> &id667in_data = id587out_result[getCycle()%15];

    bool id667x_1;

    (id667x_1) = ((id667in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(187l))&(isFlushingActive()))));
    if((id667x_1)) {
      writeOutput(m_w_out, id667in_data);
    }
  }
  { // Node ID: 935 (NodeConstantRawBits)
  }
  { // Node ID: 669 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id669in_a = id932out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id669in_b = id935out_value;

    id669out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id669in_a,id669in_b));
  }
  { // Node ID: 733 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id733in_a = id46out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id733in_b = id669out_result[getCycle()%2];

    id733out_result[(getCycle()+1)%2] = (eq_fixed(id733in_a,id733in_b));
  }
  { // Node ID: 671 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id672out_result;

  { // Node ID: 672 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id672in_a = id671out_io_s_out_force_disabled;

    id672out_result = (not_fixed(id672in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id673out_result;

  { // Node ID: 673 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id673in_a = id733out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id673in_b = id672out_result;

    HWOffsetFix<1,0,UNSIGNED> id673x_1;

    (id673x_1) = (and_fixed(id673in_a,id673in_b));
    id673out_result = (id673x_1);
  }
  { // Node ID: 905 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id905in_input = id673out_result;

    id905out_output[(getCycle()+176)%177] = id905in_input;
  }
  if ( (getFillLevel() >= (180l)) && (getFlushLevel() < (180l)|| !isFlushingActive() ))
  { // Node ID: 674 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id674in_output_control = id905out_output[getCycle()%177];
    const HWFloat<11,53> &id674in_data = id589out_result[getCycle()%15];

    bool id674x_1;

    (id674x_1) = ((id674in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(180l))&(isFlushingActive()))));
    if((id674x_1)) {
      writeOutput(m_s_out, id674in_data);
    }
  }
  { // Node ID: 686 (NodeConstantRawBits)
  }
  { // Node ID: 934 (NodeConstantRawBits)
  }
  { // Node ID: 683 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 684 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id684in_enable = id934out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id684in_max = id683out_value;

    HWOffsetFix<49,0,UNSIGNED> id684x_1;
    HWOffsetFix<1,0,UNSIGNED> id684x_2;
    HWOffsetFix<1,0,UNSIGNED> id684x_3;
    HWOffsetFix<49,0,UNSIGNED> id684x_4t_1e_1;

    id684out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id684st_count)));
    (id684x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id684st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id684x_2) = (gte_fixed((id684x_1),id684in_max));
    (id684x_3) = (and_fixed((id684x_2),id684in_enable));
    id684out_wrap = (id684x_3);
    if((id684in_enable.getValueAsBool())) {
      if(((id684x_3).getValueAsBool())) {
        (id684st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id684x_4t_1e_1) = (id684x_1);
        (id684st_count) = (id684x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id685out_output;

  { // Node ID: 685 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id685in_input = id684out_count;

    id685out_output = id685in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 687 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id687in_load = id686out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id687in_data = id685out_output;

    bool id687x_1;

    (id687x_1) = ((id687in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id687x_1)) {
      setMappedRegValue("current_run_cycle_count", id687in_data);
    }
  }
  { // Node ID: 933 (NodeConstantRawBits)
  }
  { // Node ID: 689 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 690 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id690in_enable = id933out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id690in_max = id689out_value;

    HWOffsetFix<49,0,UNSIGNED> id690x_1;
    HWOffsetFix<1,0,UNSIGNED> id690x_2;
    HWOffsetFix<1,0,UNSIGNED> id690x_3;
    HWOffsetFix<49,0,UNSIGNED> id690x_4t_1e_1;

    id690out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id690st_count)));
    (id690x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id690st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id690x_2) = (gte_fixed((id690x_1),id690in_max));
    (id690x_3) = (and_fixed((id690x_2),id690in_enable));
    id690out_wrap = (id690x_3);
    if((id690in_enable.getValueAsBool())) {
      if(((id690x_3).getValueAsBool())) {
        (id690st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id690x_4t_1e_1) = (id690x_1);
        (id690st_count) = (id690x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 692 (NodeInputMappedReg)
  }
  { // Node ID: 734 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id734in_a = id690out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id734in_b = id692out_run_cycle_count;

    id734out_result[(getCycle()+1)%2] = (eq_fixed(id734in_a,id734in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 691 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id691in_start = id734out_result[getCycle()%2];

    if((id691in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
