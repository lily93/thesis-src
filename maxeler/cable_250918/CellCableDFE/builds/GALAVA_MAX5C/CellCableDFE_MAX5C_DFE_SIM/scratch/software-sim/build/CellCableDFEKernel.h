#ifndef CELLCABLEDFEKERNEL_H_
#define CELLCABLEDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFEKernel : public KernelManagerBlockSync {
public:
  CellCableDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<8,0,UNSIGNED> id932out_value;

  HWOffsetFix<8,0,UNSIGNED> id768out_output[2];

  HWOffsetFix<8,0,UNSIGNED> id46out_count;
  HWOffsetFix<1,0,UNSIGNED> id46out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id46st_count;

  HWOffsetFix<8,0,UNSIGNED> id1005out_value;

  HWOffsetFix<8,0,UNSIGNED> id648out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id697out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id650out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id902out_output[253];

  HWOffsetFix<1,0,UNSIGNED> id769out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id38out_nx;

  HWOffsetFix<32,0,UNSIGNED> id45out_count;
  HWOffsetFix<1,0,UNSIGNED> id45out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id45st_count;

  HWOffsetFix<1,0,UNSIGNED> id770out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id39out_simTime;

  HWOffsetFix<32,0,UNSIGNED> id1004out_value;

  HWOffsetFix<32,0,UNSIGNED> id43out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id44out_count;
  HWOffsetFix<1,0,UNSIGNED> id44out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id44st_count;

  HWOffsetFix<32,0,UNSIGNED> id1003out_value;

  HWOffsetFix<1,0,UNSIGNED> id698out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id888out_output[24];

  HWOffsetFix<32,0,UNSIGNED> id906out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id907out_output[199];

  HWOffsetFix<32,0,UNSIGNED> id772out_output[31];

  HWOffsetFix<8,0,UNSIGNED> id1002out_value;

  HWOffsetFix<8,0,UNSIGNED> id625out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id699out_result[2];

  HWFloat<11,53> id675out_doutb[3];

  HWFloat<11,53> id675sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_1 (const char* _format_arg_format_string);
  HWFloat<11,53> id58out_value;

  HWFloat<11,53> id59out_result[2];

  HWFloat<11,53> id896out_output[2];

  HWFloat<11,53> id908out_output[7];

  HWFloat<11,53> id909out_output[31];

  HWFloat<11,53> id910out_output[19];

  HWFloat<11,53> id911out_output[24];

  HWFloat<11,53> id912out_output[35];

  HWFloat<11,53> id913out_output[4];

  HWFloat<11,53> id914out_output[37];

  HWFloat<11,53> id915out_output[10];

  HWFloat<11,53> id916out_output[7];

  HWFloat<11,53> id917out_output[11];

  HWFloat<11,53> id918out_output[4];

  HWFloat<11,53> id919out_output[28];

  HWFloat<11,53> id920out_output[8];

  HWFloat<11,53> id921out_output[23];

  HWFloat<11,53> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id590out_result[2];

  HWFloat<11,53> id34out_value;

  HWOffsetFix<1,0,UNSIGNED> id777out_output[2];

  HWFloat<11,53> id776out_output[83];

  HWOffsetFix<8,0,UNSIGNED> id1001out_value;

  HWOffsetFix<8,0,UNSIGNED> id629out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id700out_result[2];

  HWFloat<11,53> id678out_doutb[3];

  HWFloat<11,53> id678sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_2 (const char* _format_arg_format_string);
  HWFloat<11,53> id778out_output[2];

  HWFloat<11,53> id60out_value;

  HWFloat<11,53> id61out_result[2];

  HWFloat<11,53> id780out_output[3];

  HWFloat<11,53> id922out_output[8];

  HWFloat<11,53> id923out_output[43];

  HWOffsetFix<1,0,UNSIGNED> id460out_result[2];

  HWFloat<11,53> id17out_value;

  HWOffsetFix<1,0,UNSIGNED> id449out_result[2];

  HWFloat<11,53> id35out_value;

  HWFloat<11,53> id450out_result[2];

  HWFloat<11,53> id458out_output[2];

  HWFloat<11,53> id463out_result[8];

  HWFloat<11,53> id16out_value;

  HWOffsetFix<1,0,UNSIGNED> id224out_result[2];

  HWFloat<11,53> id1out_value;

  HWFloat<11,53> id2out_value;

  HWFloat<11,53> id225out_result[2];

  HWFloat<11,53> id444out_output[2];

  HWFloat<11,53> id464out_result[29];

  HWFloat<11,53> id0out_value;

  HWFloat<11,53> id462out_result[29];

  HWFloat<11,53> id465out_result[2];

  HWFloat<11,53> id581out_output[2];

  HWFloat<11,53> id36out_dt;

  HWFloat<11,53> id584out_result[13];

  HWFloat<11,53> id585out_result[15];

  HWFloat<11,53> id775out_output[102];

  HWFloat<11,53> id592out_result[8];

  HWFloat<11,53> id593out_result[7];

  HWFloat<11,53> id28out_value;

  HWFloat<11,53> id594out_result[8];

  HWFloat<11,53> id595out_result[7];

  HWFloat<11,53> id8out_value;

  HWFloat<11,53> id596out_result[29];

  HWFloat<11,53> id597out_result[2];

  HWFloat<11,53> id19out_value;

  HWOffsetFix<1,0,UNSIGNED> id598out_result[2];

  HWFloat<11,53> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id442out_result[2];

  HWFloat<11,53> id9out_value;

  HWFloat<11,53> id10out_value;

  HWFloat<11,53> id443out_result[2];

  HWFloat<11,53> id448out_output[2];

  HWFloat<11,53> id602out_result[29];

  HWFloat<11,53> id1000out_value;

  HWFloat<11,53> id11out_value;

  HWFloat<11,53> id31out_value;

  HWFloat<11,53> id999out_value;

  HWFloat<11,53> id998out_value;

  HWFloat<11,53> id997out_value;

  HWFloat<11,53> id24out_value;

  HWFloat<11,53> id29out_value;

  HWFloat<11,53> id333out_result[8];

  HWFloat<11,53> id334out_result[7];

  HWFloat<11,53> id744out_floatOut[2];

  HWRawBits<11> id414out_value;

  HWOffsetFix<1,0,UNSIGNED> id701out_result[2];

  HWRawBits<52> id996out_value;

  HWOffsetFix<1,0,UNSIGNED> id702out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id809out_output[46];

  HWOffsetFix<1,0,UNSIGNED> id337out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id339out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id339out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id342out_value;

  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id341out_result[8];
  HWOffsetFix<1,0,UNSIGNED> id341out_result_doubt[8];

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id343out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id343out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id791out_output[30];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id383out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id735out_value;

  HWOffsetFix<10,0,UNSIGNED> id792out_output[18];

  HWOffsetFix<38,-53,UNSIGNED> id429out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id429sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id793out_output[9];

  HWOffsetFix<48,-53,UNSIGNED> id426out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id426sta_rom_store[1024];

  HWOffsetFix<5,0,UNSIGNED> id794out_output[2];

  HWOffsetFix<53,-53,UNSIGNED> id423out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id423sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id360out_value;

  HWOffsetFix<47,-72,UNSIGNED> id359out_result[3];

  HWOffsetFix<26,-51,UNSIGNED> id361out_o[2];

  HWOffsetFix<54,-53,UNSIGNED> id362out_result[2];

  HWOffsetFix<54,-53,UNSIGNED> id795out_output[4];

  HWOffsetFix<64,-89,UNSIGNED> id363out_result[5];

  HWOffsetFix<64,-63,UNSIGNED> id364out_result[3];

  HWOffsetFix<58,-57,UNSIGNED> id365out_o[2];

  HWOffsetFix<59,-57,UNSIGNED> id366out_result[2];

  HWOffsetFix<59,-57,UNSIGNED> id796out_output[6];

  HWOffsetFix<64,-68,UNSIGNED> id367out_result[7];

  HWOffsetFix<64,-62,UNSIGNED> id368out_result[3];

  HWOffsetFix<59,-57,UNSIGNED> id369out_o[2];

  HWOffsetFix<60,-57,UNSIGNED> id370out_result[2];

  HWOffsetFix<60,-57,UNSIGNED> id797out_output[5];

  HWOffsetFix<64,-77,UNSIGNED> id371out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id372out_result[3];

  HWOffsetFix<60,-57,UNSIGNED> id373out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id374out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id995out_value;

  HWOffsetFix<1,0,UNSIGNED> id703out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id743out_result[2];

  HWFloat<11,53> id994out_value;

  HWOffsetFix<1,0,UNSIGNED> id345out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id798out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id799out_output[31];

  HWFloat<11,53> id993out_value;

  HWOffsetFix<1,0,UNSIGNED> id349out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id800out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id801out_output[31];

  HWOffsetFix<1,0,UNSIGNED> id805out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id992out_value;

  HWOffsetFix<1,0,UNSIGNED> id706out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id803out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id924out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id393out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id807out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id806out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id377out_value;

  HWOffsetFix<53,-52,UNSIGNED> id378out_result[2];

  HWOffsetFix<52,-52,UNSIGNED> id808out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id402out_value;

  HWOffsetFix<11,0,UNSIGNED> id403out_value;

  HWOffsetFix<52,0,UNSIGNED> id405out_value;

  HWFloat<11,53> id694out_value;

  HWFloat<11,53> id410out_result[2];

  HWFloat<11,53> id991out_value;

  HWFloat<11,53> id420out_result[2];

  HWFloat<11,53> id990out_value;

  HWFloat<11,53> id431out_result[8];

  HWFloat<11,53> id433out_result[29];

  HWFloat<11,53> id435out_result[8];

  HWFloat<11,53> id437out_result[8];

  HWFloat<11,53> id438out_result[7];

  HWFloat<11,53> id439out_result[8];

  HWFloat<11,53> id446out_output[2];

  HWFloat<11,53> id600out_result[29];

  HWFloat<11,53> id603out_result[2];

  HWFloat<11,53> id610out_result[8];

  HWFloat<11,53> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id604out_result[2];

  HWFloat<11,53> id608out_value;

  HWFloat<11,53> id812out_output[70];

  HWOffsetFix<8,0,UNSIGNED> id989out_value;

  HWOffsetFix<8,0,UNSIGNED> id633out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id708out_result[2];

  HWFloat<11,53> id677out_doutb[3];

  HWFloat<11,53> id677sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_3 (const char* _format_arg_format_string);
  HWFloat<11,53> id814out_output[2];

  HWFloat<11,53> id62out_value;

  HWFloat<11,53> id63out_result[2];

  HWFloat<11,53> id819out_output[117];

  HWFloat<11,53> id925out_output[8];

  HWFloat<11,53> id926out_output[43];

  HWFloat<11,53> id18out_value;

  HWOffsetFix<1,0,UNSIGNED> id466out_result[2];

  HWFloat<11,53> id33out_value;

  HWOffsetFix<1,0,UNSIGNED> id451out_result[2];

  HWFloat<11,53> id988out_value;

  HWFloat<11,53> id14out_value;

  HWFloat<11,53> id452out_result[29];

  HWFloat<11,53> id454out_result[8];

  HWFloat<11,53> id32out_value;

  HWFloat<11,53> id455out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id456out_result[2];

  HWFloat<11,53> id818out_output[2];

  HWFloat<11,53> id457out_result[2];

  HWFloat<11,53> id459out_output[2];

  HWFloat<11,53> id469out_result[8];

  HWFloat<11,53> id4out_value;

  HWFloat<11,53> id30out_value;

  HWFloat<11,53> id987out_value;

  HWFloat<11,53> id986out_value;

  HWFloat<11,53> id985out_value;

  HWFloat<11,53> id22out_value;

  HWFloat<11,53> id25out_value;

  HWFloat<11,53> id226out_result[8];

  HWFloat<11,53> id227out_result[7];

  HWFloat<11,53> id745out_floatOut[2];

  HWRawBits<11> id307out_value;

  HWOffsetFix<1,0,UNSIGNED> id709out_result[2];

  HWRawBits<52> id984out_value;

  HWOffsetFix<1,0,UNSIGNED> id710out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id838out_output[46];

  HWOffsetFix<1,0,UNSIGNED> id230out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id232out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id232out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id235out_value;

  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id234out_result[8];
  HWOffsetFix<1,0,UNSIGNED> id234out_result_doubt[8];

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id236out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id236out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id820out_output[30];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id276out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id737out_value;

  HWOffsetFix<10,0,UNSIGNED> id821out_output[18];

  HWOffsetFix<38,-53,UNSIGNED> id322out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id322sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id822out_output[9];

  HWOffsetFix<48,-53,UNSIGNED> id319out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id319sta_rom_store[1024];

  HWOffsetFix<5,0,UNSIGNED> id823out_output[2];

  HWOffsetFix<53,-53,UNSIGNED> id316out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id316sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id253out_value;

  HWOffsetFix<47,-72,UNSIGNED> id252out_result[3];

  HWOffsetFix<26,-51,UNSIGNED> id254out_o[2];

  HWOffsetFix<54,-53,UNSIGNED> id255out_result[2];

  HWOffsetFix<54,-53,UNSIGNED> id824out_output[4];

  HWOffsetFix<64,-89,UNSIGNED> id256out_result[5];

  HWOffsetFix<64,-63,UNSIGNED> id257out_result[3];

  HWOffsetFix<58,-57,UNSIGNED> id258out_o[2];

  HWOffsetFix<59,-57,UNSIGNED> id259out_result[2];

  HWOffsetFix<59,-57,UNSIGNED> id825out_output[6];

  HWOffsetFix<64,-68,UNSIGNED> id260out_result[7];

  HWOffsetFix<64,-62,UNSIGNED> id261out_result[3];

  HWOffsetFix<59,-57,UNSIGNED> id262out_o[2];

  HWOffsetFix<60,-57,UNSIGNED> id263out_result[2];

  HWOffsetFix<60,-57,UNSIGNED> id826out_output[5];

  HWOffsetFix<64,-77,UNSIGNED> id264out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id265out_result[3];

  HWOffsetFix<60,-57,UNSIGNED> id266out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id267out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id983out_value;

  HWOffsetFix<1,0,UNSIGNED> id711out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id742out_result[2];

  HWFloat<11,53> id982out_value;

  HWOffsetFix<1,0,UNSIGNED> id238out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id827out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id828out_output[31];

  HWFloat<11,53> id981out_value;

  HWOffsetFix<1,0,UNSIGNED> id242out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id829out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id830out_output[31];

  HWOffsetFix<1,0,UNSIGNED> id834out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id980out_value;

  HWOffsetFix<1,0,UNSIGNED> id714out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id832out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id927out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id286out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id836out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id835out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id270out_value;

  HWOffsetFix<53,-52,UNSIGNED> id271out_result[2];

  HWOffsetFix<52,-52,UNSIGNED> id837out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id295out_value;

  HWOffsetFix<11,0,UNSIGNED> id296out_value;

  HWOffsetFix<52,0,UNSIGNED> id298out_value;

  HWFloat<11,53> id695out_value;

  HWFloat<11,53> id303out_result[2];

  HWFloat<11,53> id979out_value;

  HWFloat<11,53> id313out_result[2];

  HWFloat<11,53> id978out_value;

  HWFloat<11,53> id324out_result[8];

  HWFloat<11,53> id326out_result[29];

  HWFloat<11,53> id328out_result[8];

  HWFloat<11,53> id330out_result[8];

  HWFloat<11,53> id331out_result[7];

  HWFloat<11,53> id332out_result[8];

  HWFloat<11,53> id445out_output[2];

  HWFloat<11,53> id470out_result[29];

  HWFloat<11,53> id3out_value;

  HWFloat<11,53> id468out_result[29];

  HWFloat<11,53> id471out_result[2];

  HWFloat<11,53> id582out_output[2];

  HWFloat<11,53> id586out_result[13];

  HWFloat<11,53> id587out_result[15];

  HWFloat<11,53> id843out_output[70];

  HWOffsetFix<8,0,UNSIGNED> id977out_value;

  HWOffsetFix<8,0,UNSIGNED> id637out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id716out_result[2];

  HWFloat<11,53> id679out_doutb[3];

  HWFloat<11,53> id679sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_4 (const char* _format_arg_format_string);
  HWFloat<11,53> id64out_value;

  HWFloat<11,53> id65out_result[2];

  HWFloat<11,53> id863out_output[112];

  HWFloat<11,53> id928out_output[49];

  HWFloat<11,53> id976out_value;

  HWFloat<11,53> id975out_value;

  HWFloat<11,53> id974out_value;

  HWFloat<11,53> id23out_value;

  HWFloat<11,53> id26out_value;

  HWFloat<11,53> id472out_result[8];

  HWFloat<11,53> id473out_result[7];

  HWFloat<11,53> id746out_floatOut[2];

  HWRawBits<11> id553out_value;

  HWOffsetFix<1,0,UNSIGNED> id717out_result[2];

  HWRawBits<52> id973out_value;

  HWOffsetFix<1,0,UNSIGNED> id718out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id862out_output[46];

  HWOffsetFix<1,0,UNSIGNED> id476out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id478out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id478out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id481out_value;

  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id480out_result[8];
  HWOffsetFix<1,0,UNSIGNED> id480out_result_doubt[8];

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id482out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id482out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id844out_output[30];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id522out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id739out_value;

  HWOffsetFix<10,0,UNSIGNED> id845out_output[18];

  HWOffsetFix<38,-53,UNSIGNED> id568out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id568sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id846out_output[9];

  HWOffsetFix<48,-53,UNSIGNED> id565out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id565sta_rom_store[1024];

  HWOffsetFix<5,0,UNSIGNED> id847out_output[2];

  HWOffsetFix<53,-53,UNSIGNED> id562out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id562sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id499out_value;

  HWOffsetFix<47,-72,UNSIGNED> id498out_result[3];

  HWOffsetFix<26,-51,UNSIGNED> id500out_o[2];

  HWOffsetFix<54,-53,UNSIGNED> id501out_result[2];

  HWOffsetFix<54,-53,UNSIGNED> id848out_output[4];

  HWOffsetFix<64,-89,UNSIGNED> id502out_result[5];

  HWOffsetFix<64,-63,UNSIGNED> id503out_result[3];

  HWOffsetFix<58,-57,UNSIGNED> id504out_o[2];

  HWOffsetFix<59,-57,UNSIGNED> id505out_result[2];

  HWOffsetFix<59,-57,UNSIGNED> id849out_output[6];

  HWOffsetFix<64,-68,UNSIGNED> id506out_result[7];

  HWOffsetFix<64,-62,UNSIGNED> id507out_result[3];

  HWOffsetFix<59,-57,UNSIGNED> id508out_o[2];

  HWOffsetFix<60,-57,UNSIGNED> id509out_result[2];

  HWOffsetFix<60,-57,UNSIGNED> id850out_output[5];

  HWOffsetFix<64,-77,UNSIGNED> id510out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id511out_result[3];

  HWOffsetFix<60,-57,UNSIGNED> id512out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id513out_o[2];

  HWOffsetFix<53,-52,UNSIGNED> id972out_value;

  HWOffsetFix<1,0,UNSIGNED> id719out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id741out_result[2];

  HWFloat<11,53> id971out_value;

  HWOffsetFix<1,0,UNSIGNED> id484out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id851out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id852out_output[31];

  HWFloat<11,53> id970out_value;

  HWOffsetFix<1,0,UNSIGNED> id488out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id853out_output[14];

  HWOffsetFix<1,0,UNSIGNED> id854out_output[31];

  HWOffsetFix<1,0,UNSIGNED> id858out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id969out_value;

  HWOffsetFix<1,0,UNSIGNED> id722out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id856out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id929out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id532out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id860out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id859out_output[2];

  HWOffsetFix<53,-52,UNSIGNED> id516out_value;

  HWOffsetFix<53,-52,UNSIGNED> id517out_result[2];

  HWOffsetFix<52,-52,UNSIGNED> id861out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id541out_value;

  HWOffsetFix<11,0,UNSIGNED> id542out_value;

  HWOffsetFix<52,0,UNSIGNED> id544out_value;

  HWFloat<11,53> id696out_value;

  HWFloat<11,53> id549out_result[2];

  HWFloat<11,53> id968out_value;

  HWFloat<11,53> id559out_result[2];

  HWFloat<11,53> id967out_value;

  HWFloat<11,53> id570out_result[8];

  HWFloat<11,53> id572out_result[29];

  HWFloat<11,53> id574out_result[8];

  HWFloat<11,53> id576out_result[8];

  HWFloat<11,53> id747out_floatOut[2];

  HWFloat<11,53> id579out_result[8];

  HWOffsetFix<1,0,UNSIGNED> id440out_result[2];

  HWFloat<11,53> id6out_value;

  HWFloat<11,53> id7out_value;

  HWFloat<11,53> id441out_result[2];

  HWFloat<11,53> id447out_output[2];

  HWFloat<11,53> id580out_result[29];

  HWFloat<11,53> id583out_output[2];

  HWFloat<11,53> id588out_result[13];

  HWFloat<11,53> id589out_result[15];

  HWFloat<11,53> id842out_output[8];

  HWFloat<11,53> id606out_result[7];

  HWFloat<11,53> id13out_value;

  HWFloat<11,53> id607out_result[29];

  HWFloat<11,53> id609out_result[2];

  HWFloat<11,53> id611out_result[8];

  HWOffsetFix<32,0,UNSIGNED> id966out_value;

  HWOffsetFix<1,0,UNSIGNED> id724out_result[2];

  HWFloat<11,53> id869out_output[55];

  HWOffsetFix<8,0,UNSIGNED> id965out_value;

  HWOffsetFix<8,0,UNSIGNED> id641out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id725out_result[2];

  HWFloat<11,53> id680out_doutb[3];

  HWFloat<11,53> id680sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_5 (const char* _format_arg_format_string);
  HWFloat<11,53> id68out_value;

  HWFloat<11,53> id69out_result[2];

  HWFloat<11,53> id870out_output[174];

  HWFloat<11,53> id74out_result[15];

  HWFloat<11,53> id872out_output[8];

  HWFloat<11,53> id930out_output[2];

  HWFloat<11,53> id964out_value;

  HWOffsetFix<1,0,UNSIGNED> id84out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id86out_value;

  HWOffsetFix<1,0,UNSIGNED> id85out_value;

  HWOffsetFix<1,0,UNSIGNED> id87out_result[2];

  HWFloat<11,53> id963out_value;

  HWOffsetFix<1,0,UNSIGNED> id79out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id81out_value;

  HWOffsetFix<1,0,UNSIGNED> id80out_value;

  HWOffsetFix<1,0,UNSIGNED> id82out_result[2];

  HWFloat<11,53> id89out_value;

  HWFloat<11,53> id88out_value;

  HWFloat<11,53> id90out_result[2];

  HWFloat<11,53> id91out_value;

  HWFloat<11,53> id92out_result[2];

  HWFloat<11,53> id962out_value;

  HWOffsetFix<32,0,UNSIGNED> id961out_value;

  HWOffsetFix<1,0,UNSIGNED> id726out_result[2];

  HWFloat<11,53> id931out_output[8];

  HWFloat<11,53> id875out_output[63];

  HWOffsetFix<8,0,UNSIGNED> id960out_value;

  HWOffsetFix<8,0,UNSIGNED> id645out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id727out_result[2];

  HWFloat<11,53> id681out_doutb[3];

  HWFloat<11,53> id681sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_6 (const char* _format_arg_format_string);
  HWFloat<11,53> id72out_value;

  HWFloat<11,53> id73out_result[2];

  HWFloat<11,53> id75out_result[15];

  HWFloat<11,53> id876out_output[167];

  HWFloat<11,53> id959out_value;

  HWFloat<11,53> id94out_result[8];

  HWFloat<11,53> id958out_value;

  HWOffsetFix<1,0,UNSIGNED> id101out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id103out_value;

  HWOffsetFix<1,0,UNSIGNED> id102out_value;

  HWOffsetFix<1,0,UNSIGNED> id104out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id877out_output[2];

  HWFloat<11,53> id957out_value;

  HWOffsetFix<1,0,UNSIGNED> id96out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id98out_value;

  HWOffsetFix<1,0,UNSIGNED> id97out_value;

  HWOffsetFix<1,0,UNSIGNED> id99out_result[2];

  HWFloat<11,53> id106out_value;

  HWFloat<11,53> id105out_value;

  HWFloat<11,53> id107out_result[2];

  HWFloat<11,53> id108out_value;

  HWFloat<11,53> id109out_result[2];

  HWFloat<11,53> id111out_result[8];

  HWFloat<11,53> id112out_result[7];

  HWFloat<11,53> id956out_value;

  HWFloat<11,53> id114out_result[8];

  HWFloat<11,53> id955out_value;

  HWOffsetFix<1,0,UNSIGNED> id121out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id123out_value;

  HWOffsetFix<1,0,UNSIGNED> id122out_value;

  HWOffsetFix<1,0,UNSIGNED> id124out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id878out_output[2];

  HWFloat<11,53> id954out_value;

  HWOffsetFix<1,0,UNSIGNED> id116out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id118out_value;

  HWOffsetFix<1,0,UNSIGNED> id117out_value;

  HWOffsetFix<1,0,UNSIGNED> id119out_result[2];

  HWFloat<11,53> id126out_value;

  HWFloat<11,53> id125out_value;

  HWFloat<11,53> id127out_result[2];

  HWFloat<11,53> id128out_value;

  HWFloat<11,53> id129out_result[2];

  HWFloat<11,53> id953out_value;

  HWFloat<11,53> id952out_value;

  HWFloat<11,53> id131out_result[8];

  HWFloat<11,53> id951out_value;

  HWOffsetFix<1,0,UNSIGNED> id138out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id140out_value;

  HWOffsetFix<1,0,UNSIGNED> id139out_value;

  HWOffsetFix<1,0,UNSIGNED> id141out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id880out_output[2];

  HWFloat<11,53> id950out_value;

  HWOffsetFix<1,0,UNSIGNED> id133out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id135out_value;

  HWOffsetFix<1,0,UNSIGNED> id134out_value;

  HWOffsetFix<1,0,UNSIGNED> id136out_result[2];

  HWFloat<11,53> id143out_value;

  HWFloat<11,53> id142out_value;

  HWFloat<11,53> id144out_result[2];

  HWFloat<11,53> id145out_value;

  HWFloat<11,53> id146out_result[2];

  HWFloat<11,53> id148out_result[8];

  HWFloat<11,53> id149out_result[7];

  HWFloat<11,53> id150out_result[8];

  HWFloat<11,53> id949out_value;

  HWFloat<11,53> id152out_result[8];

  HWFloat<11,53> id948out_value;

  HWOffsetFix<1,0,UNSIGNED> id159out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id161out_value;

  HWOffsetFix<1,0,UNSIGNED> id160out_value;

  HWOffsetFix<1,0,UNSIGNED> id162out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id882out_output[2];

  HWFloat<11,53> id947out_value;

  HWOffsetFix<1,0,UNSIGNED> id154out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id156out_value;

  HWOffsetFix<1,0,UNSIGNED> id155out_value;

  HWOffsetFix<1,0,UNSIGNED> id157out_result[2];

  HWFloat<11,53> id164out_value;

  HWFloat<11,53> id163out_value;

  HWFloat<11,53> id165out_result[2];

  HWFloat<11,53> id166out_value;

  HWFloat<11,53> id167out_result[2];

  HWFloat<11,53> id946out_value;

  HWFloat<11,53> id945out_value;

  HWFloat<11,53> id169out_result[8];

  HWFloat<11,53> id944out_value;

  HWOffsetFix<1,0,UNSIGNED> id176out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id178out_value;

  HWOffsetFix<1,0,UNSIGNED> id177out_value;

  HWOffsetFix<1,0,UNSIGNED> id179out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id884out_output[2];

  HWFloat<11,53> id943out_value;

  HWOffsetFix<1,0,UNSIGNED> id171out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id173out_value;

  HWOffsetFix<1,0,UNSIGNED> id172out_value;

  HWOffsetFix<1,0,UNSIGNED> id174out_result[2];

  HWFloat<11,53> id181out_value;

  HWFloat<11,53> id180out_value;

  HWFloat<11,53> id182out_result[2];

  HWFloat<11,53> id183out_value;

  HWFloat<11,53> id184out_result[2];

  HWFloat<11,53> id186out_result[8];

  HWFloat<11,53> id187out_result[7];

  HWFloat<11,53> id188out_result[8];

  HWFloat<11,53> id942out_value;

  HWOffsetFix<1,0,UNSIGNED> id190out_result[3];

  HWOffsetFix<32,0,UNSIGNED> id941out_value;

  HWOffsetFix<1,0,UNSIGNED> id728out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id193out_result[2];

  HWFloat<11,53> id195out_value;

  HWFloat<11,53> id194out_value;

  HWFloat<11,53> id196out_result[2];

  HWFloat<11,53> id612out_result[8];

  HWFloat<11,53> id613out_result[7];

  HWFloat<11,53> id614out_result[8];

  HWOffsetFix<32,0,UNSIGNED> id940out_value;

  HWOffsetFix<1,0,UNSIGNED> id729out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id939out_value;

  HWOffsetFix<32,0,UNSIGNED> id209out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id730out_result[2];

  HWFloat<11,53> id37out_ddt_o_dx2;

  HWFloat<11,53> id889out_output[2];

  HWFloat<11,53> id894out_output[5];

  HWOffsetFix<20,0,UNSIGNED> id890out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id892out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id938out_value;

  HWOffsetFix<32,0,UNSIGNED> id54out_result[2];

  HWFloat<11,53> id676out_doutb[3];

  HWFloat<11,53> id676sta_ram_store[1000000];

  std::string format_string_CellCableDFEKernel_7 (const char* _format_arg_format_string);
  HWFloat<11,53> id198out_value;

  HWFloat<11,53> id199out_result[2];

  HWFloat<11,53> id217out_result[8];

  HWFloat<11,53> id748out_floatOut[2];

  HWFloat<11,53> id220out_result[8];

  HWFloat<11,53> id221out_result[7];

  HWFloat<11,53> id749out_floatOut[2];

  HWFloat<11,53> id750out_floatOut[2];

  HWFloat<11,53> id215out_result[8];

  HWFloat<11,53> id216out_result[7];

  HWFloat<11,53> id898out_output[7];

  HWFloat<11,53> id222out_result[2];

  HWFloat<11,53> id751out_floatOut[2];

  HWFloat<11,53> id752out_floatOut[2];

  HWFloat<11,53> id206out_result[8];

  HWFloat<11,53> id207out_result[7];

  HWFloat<11,53> id900out_output[8];

  HWFloat<11,53> id223out_result[2];

  HWFloat<11,53> id901out_output[220];

  HWFloat<11,53> id615out_result[8];

  HWOffsetFix<8,0,UNSIGNED> id937out_value;

  HWOffsetFix<8,0,UNSIGNED> id655out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id731out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id657out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id903out_output[70];

  HWOffsetFix<8,0,UNSIGNED> id936out_value;

  HWOffsetFix<8,0,UNSIGNED> id662out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id732out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id664out_io_w_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id904out_output[184];

  HWOffsetFix<8,0,UNSIGNED> id935out_value;

  HWOffsetFix<8,0,UNSIGNED> id669out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id733out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id671out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id905out_output[177];

  HWOffsetFix<1,0,UNSIGNED> id686out_value;

  HWOffsetFix<1,0,UNSIGNED> id934out_value;

  HWOffsetFix<49,0,UNSIGNED> id683out_value;

  HWOffsetFix<48,0,UNSIGNED> id684out_count;
  HWOffsetFix<1,0,UNSIGNED> id684out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id684st_count;

  HWOffsetFix<1,0,UNSIGNED> id933out_value;

  HWOffsetFix<49,0,UNSIGNED> id689out_value;

  HWOffsetFix<48,0,UNSIGNED> id690out_count;
  HWOffsetFix<1,0,UNSIGNED> id690out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id690st_count;

  HWOffsetFix<48,0,UNSIGNED> id692out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id734out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_undef;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits_1;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_undef;
  const HWFloat<11,53> c_hw_flt_11_53_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_1;
  const HWFloat<11,53> c_hw_flt_11_53_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_4;
  const HWFloat<11,53> c_hw_flt_11_53_bits_5;
  const HWFloat<11,53> c_hw_flt_11_53_bits_6;
  const HWFloat<11,53> c_hw_flt_11_53_bits_7;
  const HWFloat<11,53> c_hw_flt_11_53_bits_8;
  const HWFloat<11,53> c_hw_flt_11_53_bits_9;
  const HWFloat<11,53> c_hw_flt_11_53_bits_10;
  const HWFloat<11,53> c_hw_flt_11_53_bits_11;
  const HWFloat<11,53> c_hw_flt_11_53_bits_12;
  const HWFloat<11,53> c_hw_flt_11_53_bits_13;
  const HWFloat<11,53> c_hw_flt_11_53_bits_14;
  const HWFloat<11,53> c_hw_flt_11_53_bits_15;
  const HWFloat<11,53> c_hw_flt_11_53_2_0val;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<52> c_hw_bit_52_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<10,0,UNSIGNED> c_hw_fix_10_0_uns_undef;
  const HWOffsetFix<38,-53,UNSIGNED> c_hw_fix_38_n53_uns_undef;
  const HWOffsetFix<48,-53,UNSIGNED> c_hw_fix_48_n53_uns_undef;
  const HWOffsetFix<5,0,UNSIGNED> c_hw_fix_5_0_uns_undef;
  const HWOffsetFix<53,-53,UNSIGNED> c_hw_fix_53_n53_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<54,-53,UNSIGNED> c_hw_fix_54_n53_uns_undef;
  const HWOffsetFix<59,-57,UNSIGNED> c_hw_fix_59_n57_uns_undef;
  const HWOffsetFix<60,-57,UNSIGNED> c_hw_fix_60_n57_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_1;
  const HWOffsetFix<52,-52,UNSIGNED> c_hw_fix_52_n52_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<52,0,UNSIGNED> c_hw_fix_52_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_16;
  const HWFloat<11,53> c_hw_flt_11_53_bits_17;
  const HWFloat<11,53> c_hw_flt_11_53_bits_18;
  const HWFloat<11,53> c_hw_flt_11_53_bits_19;
  const HWFloat<11,53> c_hw_flt_11_53_bits_20;
  const HWFloat<11,53> c_hw_flt_11_53_bits_21;
  const HWFloat<11,53> c_hw_flt_11_53_bits_22;
  const HWFloat<11,53> c_hw_flt_11_53_bits_23;
  const HWFloat<11,53> c_hw_flt_11_53_bits_24;
  const HWFloat<11,53> c_hw_flt_11_53_bits_25;
  const HWFloat<11,53> c_hw_flt_11_53_0_5val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_26;
  const HWFloat<11,53> c_hw_flt_11_53_bits_27;
  const HWFloat<11,53> c_hw_flt_11_53_bits_28;
  const HWFloat<11,53> c_hw_flt_11_53_bits_29;
  const HWFloat<11,53> c_hw_flt_11_53_bits_30;
  const HWFloat<11,53> c_hw_flt_11_53_bits_31;
  const HWFloat<11,53> c_hw_flt_11_53_bits_32;
  const HWFloat<11,53> c_hw_flt_11_53_bits_33;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_34;
  const HWOffsetFix<20,0,UNSIGNED> c_hw_fix_20_0_uns_undef;
  const HWFloat<11,53> c_hw_flt_11_53_n2_0val;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFEKERNEL_H_ */
