#include "stdsimheader.h"

using namespace maxcompilersim;

namespace maxcompilersim {
// Templated Types used in the kernel
template class HWOffsetFix<58,-57,UNSIGNED>;
template class HWOffsetFix<49,0,UNSIGNED>;
template class HWOffsetFix<5,-5,UNSIGNED>;
template class HWOffsetFix<14,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<64,-61,UNSIGNED>;
template class HWOffsetFix<47,-72,UNSIGNED>;
template class HWOffsetFix<48,-53,UNSIGNED>;
template class HWOffsetFix<64,-89,UNSIGNED>;
template class HWOffsetFix<20,0,UNSIGNED>;
template class HWOffsetFix<1,0,UNSIGNED>;
template class HWRawBits<52>;
template class HWRawBits<10>;
template class HWOffsetFix<10,-15,UNSIGNED>;
template class HWOffsetFix<10,-25,UNSIGNED>;
template class HWOffsetFix<60,-57,UNSIGNED>;
template class HWOffsetFix<32,0,UNSIGNED>;
template class HWRawBits<12>;
template class HWOffsetFix<33,0,UNSIGNED>;
template class HWOffsetFix<64,-63,UNSIGNED>;
template class HWRawBits<2>;
template class HWOffsetFix<26,-51,UNSIGNED>;
template class HWOffsetFix<48,0,UNSIGNED>;
template class HWOffsetFix<13,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<53,-53,UNSIGNED>;
template class HWOffsetFix<52,-52,UNSIGNED>;
template class HWRawBits<64>;
template class HWOffsetFix<64,-51,TWOSCOMPLEMENT>;
template class HWOffsetFix<11,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<64,-62,UNSIGNED>;
template class HWOffsetFix<52,0,UNSIGNED>;
template class HWOffsetFix<4,0,UNSIGNED>;
template class HWOffsetFix<64,-77,UNSIGNED>;
template class HWOffsetFix<38,-53,UNSIGNED>;
template class HWOffsetFix<8,0,UNSIGNED>;
template class HWOffsetFix<5,0,UNSIGNED>;
template class HWRawBits<11>;
template class HWOffsetFix<21,-21,UNSIGNED>;
template class HWFloat<11,53>;
template class HWRawBits<5>;
template class HWOffsetFix<9,0,UNSIGNED>;
template class HWRawBits<1>;
template class HWOffsetFix<59,-57,UNSIGNED>;
template class HWOffsetFix<11,0,UNSIGNED>;
template class HWOffsetFix<10,0,UNSIGNED>;
template class HWOffsetFix<64,-68,UNSIGNED>;
template class HWOffsetFix<52,-51,UNSIGNED>;
template class HWOffsetFix<116,-102,TWOSCOMPLEMENT>;
template class HWOffsetFix<54,-53,UNSIGNED>;
template class HWOffsetFix<53,-52,UNSIGNED>;
// add. templates from the default formatter 


// Templated Methods/Functions
template HWRawBits<12> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<11,0,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<4,0,UNSIGNED> &a, const HWOffsetFix<4,0,UNSIGNED> &b );
template HWRawBits<5> cast_fixed2bits<>(const HWOffsetFix<5,-5,UNSIGNED> &a);
template HWRawBits<64> cat<>(const HWRawBits<12> &a, const HWOffsetFix<52,-52,UNSIGNED> &b);
template HWOffsetFix<20,0,UNSIGNED> cast_fixed2fixed<20,0,UNSIGNED,TONEAREVEN>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWRawBits<2> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<53,-52,UNSIGNED> &a, const HWOffsetFix<53,-52,UNSIGNED> &b );
template HWOffsetFix<8,0,UNSIGNED> cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<9,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>or_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>not_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a );
template HWOffsetFix<10,-25,UNSIGNED> cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<10,0,UNSIGNED> cast_bits2fixed<10,0,UNSIGNED>(const HWRawBits<10> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template void KernelManagerBlockSync::writeOutput< HWFloat<11,53> >(const t_port_number port_number, const HWFloat<11,53> &value);
template HWRawBits<12> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b);
template HWOffsetFix<9,0,UNSIGNED>add_fixed <9,0,UNSIGNED,TRUNCATE,9,0,UNSIGNED,9,0,UNSIGNED, false>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<1,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<48,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name);
template HWFloat<11,53>add_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template HWOffsetFix<116,-102,TWOSCOMPLEMENT>mul_fixed <116,-102,TWOSCOMPLEMENT,TONEAREVEN,64,-51,TWOSCOMPLEMENT,52,-51,UNSIGNED, true>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a, const HWOffsetFix<52,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>neq_bits<>(const HWRawBits<52> &a,  const HWRawBits<52> &b );
template HWOffsetFix<10,-15,UNSIGNED> cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a);
template HWRawBits<1> slice<13,1>(const HWOffsetFix<14,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b );
template HWFloat<11,53> KernelManagerBlockSync::getMappedRegValue< HWFloat<11,53> >(const std::string &name);
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<33,0,UNSIGNED> &a);
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-15,UNSIGNED> &a);
template HWOffsetFix<64,-51,TWOSCOMPLEMENT> cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(const HWFloat<11,53> &a, HWOffsetFix<4,0,UNSIGNED>* exception );
template HWRawBits<52> slice<0,52>(const HWFloat<11,53> &a);
template HWOffsetFix<26,-51,UNSIGNED> cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(const HWOffsetFix<47,-72,UNSIGNED> &a);
template HWOffsetFix<58,-57,UNSIGNED> cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-63,UNSIGNED> &a);
template HWOffsetFix<59,-57,UNSIGNED>add_fixed <59,-57,UNSIGNED,TONEAREVEN,48,-53,UNSIGNED,58,-57,UNSIGNED, false>(const HWOffsetFix<48,-53,UNSIGNED> &a, const HWOffsetFix<58,-57,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<11,53>sub_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b );
template HWRawBits<64> cat<>(const HWRawBits<12> &a, const HWOffsetFix<52,0,UNSIGNED> &b);
template HWOffsetFix<64,-61,UNSIGNED>add_fixed <64,-61,UNSIGNED,TONEAREVEN,60,-57,UNSIGNED,64,-77,UNSIGNED, false>(const HWOffsetFix<60,-57,UNSIGNED> &a, const HWOffsetFix<64,-77,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-63,UNSIGNED>add_fixed <64,-63,UNSIGNED,TONEAREVEN,54,-53,UNSIGNED,64,-89,UNSIGNED, false>(const HWOffsetFix<54,-53,UNSIGNED> &a, const HWOffsetFix<64,-89,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<33,0,UNSIGNED>add_fixed <33,0,UNSIGNED,TRUNCATE,33,0,UNSIGNED,33,0,UNSIGNED, false>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<60,-57,UNSIGNED> cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-61,UNSIGNED> &a);
template HWOffsetFix<26,-51,UNSIGNED> cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<13,0,TWOSCOMPLEMENT> cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED> cast_bits2fixed<1,0,UNSIGNED>(const HWRawBits<1> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT> cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<14,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<54,-53,UNSIGNED>add_fixed <54,-53,UNSIGNED,TONEAREVEN,53,-53,UNSIGNED,26,-51,UNSIGNED, false>(const HWOffsetFix<53,-53,UNSIGNED> &a, const HWOffsetFix<26,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWOffsetFix<64,-68,UNSIGNED>mul_fixed <64,-68,UNSIGNED,TONEAREVEN,58,-57,UNSIGNED,48,-53,UNSIGNED, false>(const HWOffsetFix<58,-57,UNSIGNED> &a, const HWOffsetFix<48,-53,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-62,UNSIGNED>add_fixed <64,-62,UNSIGNED,TONEAREVEN,59,-57,UNSIGNED,64,-68,UNSIGNED, false>(const HWOffsetFix<59,-57,UNSIGNED> &a, const HWOffsetFix<64,-68,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<32,0,UNSIGNED>add_fixed <32,0,UNSIGNED,TONEAREVEN,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<11,53>neg_float<>(const HWFloat<11,53> &a );
template HWOffsetFix<1,0,UNSIGNED>lt_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template HWFloat<11,53> cast_bits2float<11,53>(const HWRawBits<64> &a);
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-25,UNSIGNED> &a);
template HWOffsetFix<8,0,UNSIGNED>sub_fixed <8,0,UNSIGNED,TONEAREVEN,8,0,UNSIGNED,8,0,UNSIGNED, false>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<48,0,UNSIGNED> &a, const HWOffsetFix<48,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<14,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<14,0,TWOSCOMPLEMENT> &b );
template HWRawBits<11> slice<52,11>(const HWFloat<11,53> &a);
template HWOffsetFix<60,-57,UNSIGNED>add_fixed <60,-57,UNSIGNED,TONEAREVEN,38,-53,UNSIGNED,59,-57,UNSIGNED, false>(const HWOffsetFix<38,-53,UNSIGNED> &a, const HWOffsetFix<59,-57,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-77,UNSIGNED>mul_fixed <64,-77,UNSIGNED,TONEAREVEN,59,-57,UNSIGNED,38,-53,UNSIGNED, false>(const HWOffsetFix<59,-57,UNSIGNED> &a, const HWOffsetFix<38,-53,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-51,TWOSCOMPLEMENT> cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &a, HWOffsetFix<1,0,UNSIGNED>* exception );
template HWOffsetFix<9,0,UNSIGNED> cast_fixed2fixed<9,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<8,0,UNSIGNED> &a);
template HWOffsetFix<33,0,UNSIGNED> cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWFloat<11,53>div_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template void KernelManagerBlockSync::setMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name, const HWOffsetFix<48,0,UNSIGNED> & value);
template HWOffsetFix<53,-52,UNSIGNED> cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(const HWOffsetFix<60,-57,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b );
template HWOffsetFix<32,0,UNSIGNED>sub_fixed <32,0,UNSIGNED,TONEAREVEN,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<48,0,UNSIGNED> cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<49,0,UNSIGNED> &a);
template HWOffsetFix<59,-57,UNSIGNED> cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-62,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_bits<>(const HWRawBits<11> &a,  const HWRawBits<11> &b );
template HWOffsetFix<14,0,TWOSCOMPLEMENT> cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<13,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<64,-89,UNSIGNED>mul_fixed <64,-89,UNSIGNED,TONEAREVEN,26,-51,UNSIGNED,53,-53,UNSIGNED, false>(const HWOffsetFix<26,-51,UNSIGNED> &a, const HWOffsetFix<53,-53,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<5,-5,UNSIGNED> cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<49,0,UNSIGNED>add_fixed <49,0,UNSIGNED,TRUNCATE,49,0,UNSIGNED,49,0,UNSIGNED, false>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>lt_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>gt_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template HWFloat<11,53>mul_float<>(const HWFloat<11,53> &a, const HWFloat<11,53> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWOffsetFix<47,-72,UNSIGNED>mul_fixed <47,-72,UNSIGNED,TONEAREVEN,21,-21,UNSIGNED,26,-51,UNSIGNED, false>(const HWOffsetFix<21,-21,UNSIGNED> &a, const HWOffsetFix<26,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<32,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<32,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<5,0,UNSIGNED> cast_bits2fixed<5,0,UNSIGNED>(const HWRawBits<5> &a);
template HWOffsetFix<14,0,TWOSCOMPLEMENT>add_fixed <14,0,TWOSCOMPLEMENT,TRUNCATE,14,0,TWOSCOMPLEMENT,14,0,TWOSCOMPLEMENT, false>(const HWOffsetFix<14,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<14,0,TWOSCOMPLEMENT> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>and_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<52,-52,UNSIGNED> cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(const HWOffsetFix<53,-52,UNSIGNED> &a);


// Additional Code
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_1 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_2 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_3 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_4 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_5 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_6 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFEKernel::format_string_CellCableDFEKernel_7 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}

} // End of maxcompilersim namespace
