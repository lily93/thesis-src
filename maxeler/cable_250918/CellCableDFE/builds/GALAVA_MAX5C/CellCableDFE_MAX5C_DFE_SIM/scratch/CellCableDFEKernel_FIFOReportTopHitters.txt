Top 10 largest FIFOs
     Costs                                              From                                                To
        29                       CellCableDFEKernel.maxj:504                       CellCableDFEKernel.maxj:504
         8                       CellCableDFEKernel.maxj:138                       CellCableDFEKernel.maxj:229
         8                       CellCableDFEKernel.maxj:145                       CellCableDFEKernel.maxj:152
         8                       CellCableDFEKernel.maxj:138                       CellCableDFEKernel.maxj:185
         6                       CellCableDFEKernel.maxj:140                       CellCableDFEKernel.maxj:193
         4                       CellCableDFEKernel.maxj:139                       CellCableDFEKernel.maxj:223
         4                       CellCableDFEKernel.maxj:138                       CellCableDFEKernel.maxj:193
         4                       CellCableDFEKernel.maxj:138                       CellCableDFEKernel.maxj:192
         4                       CellCableDFEManager.maxj:20                       CellCableDFEKernel.maxj:229
         4                       CellCableDFEManager.maxj:20                       CellCableDFEKernel.maxj:236

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
        29                       CellCableDFEKernel.maxj:504         9
        14                       CellCableDFEKernel.maxj:229         6
        10                       CellCableDFEKernel.maxj:193         4
        10                       CellCableDFEKernel.maxj:152         3
         8                       CellCableDFEKernel.maxj:185         3
         6                       CellCableDFEKernel.maxj:238         3
         6                       CellCableDFEKernel.maxj:192         3
         6                       CellCableDFEKernel.maxj:235         3
         6                       CellCableDFEKernel.maxj:236         3
         5                       CellCableDFEKernel.maxj:161         3

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
        42                       CellCableDFEKernel.maxj:138        19
        29                       CellCableDFEKernel.maxj:504         9
        16                       CellCableDFEManager.maxj:20         8
        10                       CellCableDFEKernel.maxj:140         4
        10                       CellCableDFEKernel.maxj:145         3
         6                       CellCableDFEKernel.maxj:139         3
         6                       CellCableDFEKernel.maxj:141         3
         6                       CellCableDFEKernel.maxj:262         6
         6                       CellCableDFEKernel.maxj:144         3
         4                        CellCableDFEKernel.maxj:96         4
