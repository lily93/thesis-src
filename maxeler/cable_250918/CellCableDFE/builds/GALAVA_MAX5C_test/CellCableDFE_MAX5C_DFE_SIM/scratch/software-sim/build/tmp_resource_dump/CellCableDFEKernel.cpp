#include "stdsimheader.h"
#include "CellCableDFEKernel.h"

namespace maxcompilersim {

CellCableDFEKernel::CellCableDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 5, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 658 (NodeConstantRawBits)
    id658out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 668 (NodeConstantRawBits)
    id668out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 655 (NodeConstantRawBits)
    id655out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 659 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 667 (NodeConstantRawBits)
    id667out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 661 (NodeConstantRawBits)
    id661out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 664 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
  { // Node ID: 36 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 37 (NodeInputMappedReg)
    registerMappedRegister("ddt_o_dx2", Data(64));
  }
  { // Node ID: 38 (NodeInputMappedReg)
    registerMappedRegister("nx", Data(32));
  }
  { // Node ID: 39 (NodeInputMappedReg)
    registerMappedRegister("simTime", Data(32));
  }
}

void CellCableDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void CellCableDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 656 (NodeCounter)

    (id656st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 662 (NodeCounter)

    (id662st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 664 (NodeInputMappedReg)
    id664out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 37 (NodeInputMappedReg)
    id37out_ddt_o_dx2 = getMappedRegValue<HWFloat<11,53> >("ddt_o_dx2");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_nx = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("nx");
  }
  { // Node ID: 39 (NodeInputMappedReg)
    id39out_simTime = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("simTime");
  }
}

void CellCableDFEKernel::updateState() {
  { // Node ID: 664 (NodeInputMappedReg)
    id664out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 37 (NodeInputMappedReg)
    id37out_ddt_o_dx2 = getMappedRegValue<HWFloat<11,53> >("ddt_o_dx2");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_nx = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("nx");
  }
  { // Node ID: 39 (NodeInputMappedReg)
    id39out_simTime = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("simTime");
  }
}

void CellCableDFEKernel::preExecute() {
}

void CellCableDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "CellCableDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int CellCableDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
