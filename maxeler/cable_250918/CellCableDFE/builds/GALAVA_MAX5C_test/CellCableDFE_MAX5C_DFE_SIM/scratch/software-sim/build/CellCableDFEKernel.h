#ifndef CELLCABLEDFEKERNEL_H_
#define CELLCABLEDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFEKernel : public KernelManagerBlockSync {
public:
  CellCableDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  HWOffsetFix<1,0,UNSIGNED> id658out_value;

  HWOffsetFix<1,0,UNSIGNED> id668out_value;

  HWOffsetFix<49,0,UNSIGNED> id655out_value;

  HWOffsetFix<48,0,UNSIGNED> id656out_count;
  HWOffsetFix<1,0,UNSIGNED> id656out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id656st_count;

  HWOffsetFix<1,0,UNSIGNED> id667out_value;

  HWOffsetFix<49,0,UNSIGNED> id661out_value;

  HWOffsetFix<48,0,UNSIGNED> id662out_count;
  HWOffsetFix<1,0,UNSIGNED> id662out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id662st_count;

  HWOffsetFix<48,0,UNSIGNED> id664out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id666out_result[2];

  HWFloat<11,53> id36out_dt;

  HWFloat<11,53> id37out_ddt_o_dx2;

  HWOffsetFix<32,0,UNSIGNED> id38out_nx;

  HWOffsetFix<32,0,UNSIGNED> id39out_simTime;

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFEKERNEL_H_ */
