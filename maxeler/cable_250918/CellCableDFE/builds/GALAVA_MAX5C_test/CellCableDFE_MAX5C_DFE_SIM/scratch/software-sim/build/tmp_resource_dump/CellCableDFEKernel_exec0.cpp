#include "stdsimheader.h"

namespace maxcompilersim {

void CellCableDFEKernel::execute0() {
  { // Node ID: 658 (NodeConstantRawBits)
  }
  { // Node ID: 668 (NodeConstantRawBits)
  }
  { // Node ID: 655 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 656 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id656in_enable = id668out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id656in_max = id655out_value;

    HWOffsetFix<49,0,UNSIGNED> id656x_1;
    HWOffsetFix<1,0,UNSIGNED> id656x_2;
    HWOffsetFix<1,0,UNSIGNED> id656x_3;
    HWOffsetFix<49,0,UNSIGNED> id656x_4t_1e_1;

    id656out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id656st_count)));
    (id656x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id656st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id656x_2) = (gte_fixed((id656x_1),id656in_max));
    (id656x_3) = (and_fixed((id656x_2),id656in_enable));
    id656out_wrap = (id656x_3);
    if((id656in_enable.getValueAsBool())) {
      if(((id656x_3).getValueAsBool())) {
        (id656st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id656x_4t_1e_1) = (id656x_1);
        (id656st_count) = (id656x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id657out_output;

  { // Node ID: 657 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id657in_input = id656out_count;

    id657out_output = id657in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 659 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id659in_load = id658out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id659in_data = id657out_output;

    bool id659x_1;

    (id659x_1) = ((id659in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id659x_1)) {
      setMappedRegValue("current_run_cycle_count", id659in_data);
    }
  }
  { // Node ID: 667 (NodeConstantRawBits)
  }
  { // Node ID: 661 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 662 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id662in_enable = id667out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id662in_max = id661out_value;

    HWOffsetFix<49,0,UNSIGNED> id662x_1;
    HWOffsetFix<1,0,UNSIGNED> id662x_2;
    HWOffsetFix<1,0,UNSIGNED> id662x_3;
    HWOffsetFix<49,0,UNSIGNED> id662x_4t_1e_1;

    id662out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id662st_count)));
    (id662x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id662st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id662x_2) = (gte_fixed((id662x_1),id662in_max));
    (id662x_3) = (and_fixed((id662x_2),id662in_enable));
    id662out_wrap = (id662x_3);
    if((id662in_enable.getValueAsBool())) {
      if(((id662x_3).getValueAsBool())) {
        (id662st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id662x_4t_1e_1) = (id662x_1);
        (id662st_count) = (id662x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 664 (NodeInputMappedReg)
  }
  { // Node ID: 666 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id666in_a = id662out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id666in_b = id664out_run_cycle_count;

    id666out_result[(getCycle()+1)%2] = (eq_fixed(id666in_a,id666in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 663 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id663in_start = id666out_result[getCycle()%2];

    if((id663in_start.getValueAsBool())) {
      startFlushing();
    }
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeInputMappedReg)
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  { // Node ID: 39 (NodeInputMappedReg)
  }
};

};
