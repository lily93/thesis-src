package minimal.fourtyeight.cable;

import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Optimization.PipelinedOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;
import com.maxeler.maxcompiler.v2.utils.MathUtils;


public class CellCableDFE48Kernel extends Kernel {


	 static final DFEType scalarType = dfeFloat(8, 24);
	 static final DFEType calculationType = dfeFloat(8, 40);
	 static final DFEVectorType<DFEVar> vectorType =
			  new DFEVectorType<DFEVar>(calculationType, 4);
	//Constants
	final DFEVar EPI_TVP = constant.var(calculationType, 1.4506);
	final DFEVar EPI_TV1M = constant.var(calculationType, 60);
	final DFEVar EPI_TV2M = constant.var(calculationType,1150);
	final DFEVar EPI_TWP = constant.var(calculationType,200);
	final DFEVar EPI_TW1M = constant.var(calculationType,60);
	final DFEVar EPI_TW2M = constant.var(calculationType,15);
	final DFEVar EPI_TS1 = constant.var(calculationType,2.7342);
	final DFEVar EPI_TS2 = constant.var(calculationType,16);
	final DFEVar EPI_TFI = constant.var(calculationType,0.11);
	final DFEVar EPI_TO1 = constant.var(calculationType,400);
	final DFEVar EPI_TO2 = constant.var(calculationType,6);
	final DFEVar EPI_TSO1 = constant.var(calculationType,30.0181);
	final DFEVar EPI_TSO2 = constant.var(calculationType,0.9957);
	final DFEVar EPI_TSI = constant.var(calculationType,1.8875);
	final DFEVar EPI_TWINF = constant.var(calculationType,0.07);
	final DFEVar EPI_THV = constant.var(calculationType,0.3);
	final DFEVar EPI_THVM = constant.var(calculationType,0.006);
	final DFEVar EPI_THVINF = constant.var(calculationType,0.006);
	final DFEVar EPI_THW = constant.var(calculationType,0.13);
	final DFEVar EPI_THSO = constant.var(calculationType,0.006);
	final DFEVar EPI_THSI = constant.var(calculationType,0.13);
	final DFEVar EPI_THO = constant.var(calculationType,0.006);
	final DFEVar EPI_KWM = constant.var(calculationType,65);
	final DFEVar EPI_KS = constant.var(calculationType,2.0994);
	final DFEVar EPI_KSO = constant.var(calculationType,2.0458);
	final DFEVar EPI_UWM = constant.var(calculationType,0.03);
	final DFEVar EPI_US = constant.var(calculationType,0.9087);
	final DFEVar EPI_U0 = constant.var(calculationType,0);
	final DFEVar EPI_UU = constant.var(calculationType,1.55);
	final DFEVar EPI_USO = constant.var(calculationType,0.65);
	final DFEVar TW2M_TW1M_DIVBY2 = constant.var(calculationType,-22.5);
	final DFEVar TSO2_TSO1_DIVBY2 = constant.var(calculationType,-14.5112);
	final DFEVar  WINFSTAR = constant.var( calculationType,0.94); 
	final DFEVar  EPI_THWINF = constant.var(  calculationType,0.006);
	
	final DFEVar Zero = constant.var(calculationType, 0.0);
	final DFEVar One = constant.var(calculationType, 1.0);


	 

	CellCableDFE48Kernel(KernelParameters parameters, int X, int duration) {
		super(parameters);

		//time given by cpu
		DFEVar dt = io.scalarInput("dt", scalarType);
		dt = dt.cast(calculationType);
		DFEVar ddt_o_dx2 = io.scalarInput("ddt_o_dx2", scalarType);
		ddt_o_dx2 = ddt_o_dx2.cast(calculationType);
		final DFEVar nx = io.scalarInput("nx", dfeUInt(32));
		final DFEVar simTime = io.scalarInput("simTime", dfeUInt(32));





		//create autoloop offset to create backwards edge for calculation
		OffsetExpr loopLength = stream.makeOffsetAutoLoop("loopLength"); //
		DFEVar loopLengthVal = loopLength.getDFEVar(this, dfeUInt(32));





		CounterChain chain = control.count.makeCounterChain();
		DFEVar step = chain.addCounter(simTime+1, 1); //counter for simulation time..
		//DFEVar carriedT1 = scalarType.newInstance(this);
		//DFEVar carriedT2 = scalarType.newInstance(this);






		DFEVar cellNumAddress = chain.addCounter(nx, 1);//counter for number of cells ; resets automatically when reaching X; -> inner loop



		DFEVar loopCounter = chain.addCounter(loopLengthVal, 1); //counter for validation of values;


		Memory<DFEVar> uMem = mem.alloc(calculationType, X);
		Memory<DFEVar> wMem = mem.alloc(calculationType, X);
		Memory<DFEVar> vMem = mem.alloc(scalarType, X);
		Memory<DFEVar> sMem = mem.alloc(scalarType, X);
		Memory<DFEVar> t1Mem = mem.alloc(calculationType, X);
		Memory<DFEVar> t2Mem = mem.alloc(calculationType, X);
		DFEVar uFromMem = uMem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
		DFEVar vFromMem = vMem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
		DFEVar wFromMem = wMem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
		DFEVar sFromMem = sMem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
		DFEVar t1FromMem = t1Mem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
		DFEVar t2FromMem = t2Mem.read(cellNumAddress.cast(dfeUInt(MathUtils.bitsToAddress(X))));


		//create sourcless streams for variables

		DFEVar carriedU = calculationType.newInstance(this);
//		DFEVar carriedV = scalarType.newInstance(this);
//		DFEVar carriedW = scalarType.newInstance(this);
//		DFEVar carriedS = scalarType.newInstance(this);

		//allocate Memory for BRAM
			//define initCondition - easier to read;
		DFEVar initCondition = (step===0);




		//init variables needed;

		 DFEVar u = initCondition? 0.0 : uFromMem; //initCondition? 0.0 : carriedU;
	     DFEVar    v = initCondition? 1.0 :  vFromMem;
	     DFEVar   w = initCondition ? 1.0 :  wFromMem;
	     DFEVar     s = initCondition? 0.0 :sFromMem;
		    DFEVar t1 = step===0? 0.0 : t1FromMem;
		     DFEVar t2 = step===0? 0.0 : t2FromMem;
				DFEVar t1Curr = t1+dt;
				DFEVar t2Curr = t2+dt;

		 	DFEVar active = dfeBool().newInstance(this);
		 	//debug.simPrintf("step: %d\t uFromMem: %f\n",step, uFromMem);


		optimization.pushPipeliningFactor(0.5,PipelinedOps.ALL);
	 	DFEVar stimFlag = heavisidefun(t1Curr - 0)*(1 - heavisidefun(t2Curr - 1)) + heavisidefun(t1Curr - 300)*(1 - heavisidefun(t2Curr - 301)) + heavisidefun(t1Curr - 700)*(1 - heavisidefun(t2Curr - 701));
		optimization.popPipeliningFactor(PipelinedOps.ALL);

	 	active = (stimFlag===1)&(cellNumAddress<duration);//#
	      DFEVar stim = active? constant.var(calculationType,0.66) : constant.var(calculationType, 0.0);
	      cellNumAddress.simWatch("cell");
	      step.simWatch("step");
		   DFEVar prevNeigh = stream.offset(carriedU, -1);
		   carriedU.simWatch("carriedU");
		   prevNeigh.simWatch("previousCell");
		  DFEVar neighbourAdress = cellNumAddress+1;
	  	   DFEVar nextNeigh = initCondition? 0.0: uMem.read(neighbourAdress.cast(dfeUInt(MathUtils.bitsToAddress(X))));
	  	   nextNeigh.simWatch("nextCell");  	   
	  	   DFEVar lap = cellNumAddress===0? 	  ddt_o_dx2 * (-2.0*u + 2.0*nextNeigh) :
	  		   					cellNumAddress===(nx-1)?  ddt_o_dx2 * (-2.0*u + 2.0*prevNeigh) :
													 		  ddt_o_dx2 * (prevNeigh + nextNeigh - 2.0*u);
	      lap.simWatch("laplace");
	      
	      DFEVar tvm, vinf, winf;
		  DFEVar jfi, jso, jsi;
		  DFEVar twm, tso, ts, to, ds, dw, dv;

		  optimization.pushPipeliningFactor(0.5, PipelinedOps.ALL);
		  tvm =  (u >   EPI_THVM) ?   EPI_TV2M :   EPI_TV1M;
	      twm =    EPI_TW1M + (  TW2M_TW1M_DIVBY2)*(1+tanh(  EPI_KWM*(u-  EPI_UWM)));
	      tso =    EPI_TSO1 + (  TSO2_TSO1_DIVBY2)*(1+tanh(  EPI_KSO*(u-  EPI_USO)));
	      ts  = (u >   EPI_THW)  ?   EPI_TS2  :   EPI_TS1;
	      to  = (u >   EPI_THO)  ?   EPI_TO2  :   EPI_TO1;
	      optimization.popPipeliningFactor(PipelinedOps.ALL);
	      tvm = optimization.pipeline(tvm);
	      twm = optimization.pipeline(twm);
	      tso = optimization.pipeline(tso);
	      ts = optimization.pipeline(ts);
	      to = optimization.pipeline(to);
	      
		  optimization.pushPipeliningFactor(0.5, PipelinedOps.ALL);
	      vinf = (u >    EPI_THVINF) ? Zero : One;
		  winf = (u >    EPI_THWINF) ?   WINFSTAR: (1.0-u/  EPI_TWINF);
	      winf =  (winf > One) ? One : winf;
	      optimization.popPipeliningFactor(PipelinedOps.ALL);
	      vinf = optimization.pipeline(vinf);
	      winf = optimization.pipeline(winf);
		  optimization.pushPipeliningFactor(0.5, PipelinedOps.ALL);

	      dv = (u >   EPI_THV) ? -v/  EPI_TVP : (vinf-v)/tvm;
	      dw = (u >   EPI_THW) ? -w/  EPI_TWP : (winf-w)/twm;
	      ds = (((1.+tanh(  EPI_KS*(u-  EPI_US)))/2.) - s)/ts;
	      optimization.popPipeliningFactor(PipelinedOps.ALL);
	      dv = optimization.pipeline(dv);
	      dw = optimization.pipeline(dw);
	      ds = optimization.pipeline(ds);
	    

	      //winf = (*u > 0.06) ? 0.94: 1.0-*u/0.07;
	      //twm  =  60 + (-22.5)*(1.+tanh(65*(*u-0.03)));
	      //dw   = (*u > 0.13) ? -*w/200 : (winf-*w)/twm;




		  //tvm =  (*u >   THVM) ?   TV2M :   TV1M;
		  //one_o_twm = segm_table[0][th] * (*u) + segm_table[1][th];
		  //vinf = (*u >    THVINF) ? 0.0: 1.0;
		  //winf = (*u >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*u) + segm2_table[1][th2]);
		  //if (winf >one_o_twm) winf = one_o_twm;
		  //dv = (*u >   THV) ? -*v/  TVP : (vinf-*v)/tvm;
		  //dw = (*u >   THW) ? -*w/  TWP : winf - *w * one_o_twm;
		  //ds = (((1.+tanh(  KS*(*u-  US)))/2.) - *s)/ts;





		  //Update gates

	       v += dv*dt;
	       w += dw*dt;
	       s += ds*dt;

		  //Compute currents
	       optimization.pushPipeliningFactor(0.5, PipelinedOps.ALL);
	      jfi = (u >   EPI_THV)  ? -v * (u -   EPI_THV) * (  EPI_UU - u)/  EPI_TFI : Zero;
	      /*if (*u >   THV){
	         	  if (((*v - dv*dt) > 0.0) && *v < 0.0001){
	         		  printf("v=%4.20f, u=%f, jfi=%f\n", *v, *u, jfi);
	         	  }
	           }*/
	      jso = (u >   EPI_THSO) ? 1/tso : (u-  EPI_U0)/to;
	      jsi = (u >   EPI_THSI) ? -w * s/  EPI_TSI : 0.0;

	      u = u  - (jfi+jso+jsi-stim)*dt + lap;
	      optimization.popPipeliningFactor(PipelinedOps.ALL);
	  	DFEVar uOffset = stream.offset(u, -loopLength);
		DFEVar vOffset = stream.offset(v, -loopLength);
		DFEVar wOffset = stream.offset(w, -loopLength);
		DFEVar sOffset = stream.offset(s, -loopLength);
	
	//	DFEVar stimOffset = stream.offset(stim, -loopLength);

          


		//generate offset for backward edge

		DFEVar t1Offset = stream.offset(t1Curr, -loopLength);
		DFEVar t2Offset = stream.offset(t2Curr, -loopLength);





		// At the foot of the loop, we add the backward edge

		carriedU <== uOffset;


		//write to outputstreams;

		DFEVar memOffset = stream.offset(cellNumAddress, -loopLength);

		 uMem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), uOffset, (loopCounter === (loopLengthVal-1)));
		 vMem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), vOffset, (loopCounter === (loopLengthVal-1)));
		 wMem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), wOffset, (loopCounter === (loopLengthVal-1)));
		 sMem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), sOffset, (loopCounter === (loopLengthVal-1)));
		t1Mem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), t1Offset, (loopCounter === (loopLengthVal-1)));
		 t2Mem.write(memOffset.cast(dfeUInt(MathUtils.bitsToAddress(X))), t2Offset, (loopCounter === (loopLengthVal-1)));
		 	
		  u =u.cast(scalarType);
			v = v.cast(scalarType); 
			w = w.cast(scalarType);
			s =  s.cast(scalarType);

		io.output("u_out", u, scalarType,loopCounter === (loopLengthVal-1));
		io.output("v_out", v, scalarType,loopCounter === (loopLengthVal-1));
		io.output("w_out", w, scalarType,loopCounter === (loopLengthVal-1)) ;
		io.output("s_out", s, scalarType,loopCounter === (loopLengthVal-1));
		//io.output("stim_out", stim, scalarType,loopCounter === (loopLengthVal-1));

	}
	
	DFEVector<DFEVar> cstep_epi(OffsetExpr loopLength, DFEVector<DFEVar> statevars, DFEVar ui, DFEVar vi, DFEVar wi, DFEVar si, DFEVar stim, DFEVar dt, DFEVar lap) {
		 
		
		  DFEVar tvm, vinf, winf;
		  DFEVar jfi, jso, jsi;
		  DFEVar twm, tso, ts, to, ds, dw, dv;

		  optimization.pushPipeliningFactor(0.5, PipelinedOps.ALL);
		  tvm =  (ui >   EPI_THVM) ?   EPI_TV2M :   EPI_TV1M;
	      twm =    EPI_TW1M + (  TW2M_TW1M_DIVBY2)*(1+tanh(  EPI_KWM*(ui-  EPI_UWM)));
	      tso =    EPI_TSO1 + (  TSO2_TSO1_DIVBY2)*(1+tanh(  EPI_KSO*(ui-  EPI_USO)));
	      ts  = (ui >   EPI_THW)  ?   EPI_TS2  :   EPI_TS1;
	      to  = (ui >   EPI_THO)  ?   EPI_TO2  :   EPI_TO1;
	      
	      tvm = optimization.pipeline(tvm);
	      twm = optimization.pipeline(twm);
	      tso = optimization.pipeline(tso);
	      ts = optimization.pipeline(ts);
	      to = optimization.pipeline(to);
	      

	      vinf = (ui >    EPI_THVINF) ? Zero : One;
		  winf = (ui >    EPI_THWINF) ?   WINFSTAR: (1.0-ui/  EPI_TWINF);
	      winf =  (winf > One) ? One : winf;
	      
	      vinf = optimization.pipeline(vinf);
	      winf = optimization.pipeline(winf);
	      

	      dv = (ui >   EPI_THV) ? -vi/  EPI_TVP : (vinf-vi)/tvm;
	      dw = (ui >   EPI_THW) ? -wi/  EPI_TWP : (winf-wi)/twm;
	      ds = (((1.+tanh(  EPI_KS*(ui-  EPI_US)))/2.) - si)/ts;
	      
	      dv = optimization.pipeline(dv);
	      dw = optimization.pipeline(dw);
	      ds = optimization.pipeline(ds);
	      optimization.popPipeliningFactor(PipelinedOps.ALL);

	      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
	      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
	      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




		  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
		  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
		  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
		  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
		  //if (winf >one_o_twm) winf = one_o_twm;
		  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
		  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
		  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;





		  //Update gates

	       vi += dv*dt;
	       wi += dw*dt;
	       si += ds*dt;

		  //Compute currents
	       optimization.pushPipeliningFactor(1.0, PipelinedOps.ALL);
	      jfi = (ui >   EPI_THV)  ? -vi * (ui -   EPI_THV) * (  EPI_UU - ui)/  EPI_TFI : Zero;
	      /*if (*ui >   THV){
	         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
	         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
	         	  }
	           }*/
	      jso = (ui >   EPI_THSO) ? 1/tso : (ui-  EPI_U0)/to;
	      jsi = (ui >   EPI_THSI) ? -wi * si/  EPI_TSI : 0.0;

	      ui = ui  - (jfi+jso+jsi-stim)*dt + lap;
	      optimization.popPipeliningFactor(PipelinedOps.ALL);
	  	DFEVar uOffset = stream.offset(ui, -loopLength);
		DFEVar vOffset = stream.offset(vi, -loopLength);
		DFEVar wOffset = stream.offset(wi, -loopLength);
		DFEVar sOffset = stream.offset(si, -loopLength);
	
	//	DFEVar stimOffset = stream.offset(stim, -loopLength);

        statevars[0] <== uOffset;
        statevars[1] <==vOffset;
        statevars[2] <== wOffset;
        statevars[3] <== sOffset;
        
        return statevars;
	}

//DFEVector<DFEVar> cstep_old(OffsetExpr loopLength, DFEVector<DFEVar> stateVars, DFEVar u, DFEVar v, DFEVar w, DFEVar s, DFEVar stim, DFEVar dt, DFEVar lap) {
//
//    //define Conditions for calculation;
//		DFEVar calcCondition1 = u<0.006;
//		DFEVar calcCondition2 = u<0.13;
//		DFEVar calcCondition3 = u<0.3;
//
//		//Define current values;
//		DFEVar wCurr;
//		DFEVar uCurr;
//		DFEVar sCurr;
//		DFEVar vCurr;
//
//
//
//		//jfi; jso; jsi
//
//		DFEVar jfi;
//       DFEVar jso;
//       DFEVar jsi;
//       // stim
//
//
//
//
//	//calculation stimulus
//
//	//calculation
//       optimization.pushPipeliningFactor(0.0,PipelinedOps.ALL);
//
//	 wCurr = calcCondition1 ? (w + ((1.0 -(u/EPI_TWINF) - w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1+tanh(EPI_KWM*(u-EPI_UWM)))))*dt):
//		 	 calcCondition2 ? w + ((0.94-w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1+tanh(EPI_KWM*(u-EPI_UWM)))))*dt :
//		 	 calcCondition3 ? w + (-w/EPI_TWP)*dt:
//		 		 			  w + (-w/EPI_TWP)*dt;
//
//	  vCurr = calcCondition1 ? (v + ((1.0-v)/EPI_TV1M)*dt): //u<0.06
//		 	 calcCondition2 ? (v + (-v/EPI_TV2M)*dt) : //elseif u<0.13
//			 calcCondition3 ?( v + (-v/EPI_TV2M)*dt): //elseif u<0.3
//				 			   v + (-v/EPI_TVP)*dt; //else
//
//
//
//	 sCurr = calcCondition1 ? (s + ((((1+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS1)*dt):
//		 	 calcCondition2 ? (s +((((1+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS1)*dt):
//		 	 calcCondition3 ? (s + ((((1.+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS2)*dt):
//		 		 			   s +((((1.+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS2)*dt;
//
//
//	 jfi = calcCondition1 ? Zero :
//   	   calcCondition2 ? Zero :
//   	   calcCondition3 ? Zero :
//   		  				(-v * (u - EPI_THV) * (EPI_UU - u)/EPI_TFI);
//
//    jso = calcCondition1 ? u/EPI_TO1 :
//   	   calcCondition2 ? u/EPI_TO2 :
//   	   calcCondition3 ? (1/(EPI_TSO1+((EPI_TSO2-EPI_TSO1)*(1.+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5)) :
//   		   				(1/(EPI_TSO1+((EPI_TSO2 - EPI_TSO1)*(1+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5));
//
//    jsi = calcCondition1 ? Zero :
//   	   calcCondition2 ? Zero :
//   	   calcCondition3 ? (-w * s/EPI_TSI) :
//   		   				(-w * s/EPI_TSI);
//
//
//   	 
//   		   				   uCurr = u  - (jfi+jso+jsi-stim)*dt+lap;
//   		   			   	optimization.popPipeliningFactor(PipelinedOps.ALL);
//   		   			   	
//   		   				DFEVar uOffset = stream.offset(uCurr, -loopLength);
//   		   				DFEVar vOffset = stream.offset(vCurr, -loopLength);
//   		   				DFEVar wOffset = stream.offset(wCurr, -loopLength);
//   		   				DFEVar sOffset = stream.offset(sCurr, -loopLength);
//   		   				
//   		   				stateVars[0] <== uOffset;
//   		   				stateVars[1] <== vOffset;
//   		   				stateVars[2] <== wOffset;
//   		   				stateVars[3] <== sOffset;
//
//   	return stateVars;
//}

DFEVar laplace(DFEVar initCondition, DFEVar carriedU, DFEVar uFromMem, DFEVar cellNumAddress, DFEVar ddt_o_dx2, DFEVar u, DFEVar nx) {
  	   DFEVar prevNeigh = stream.offset(carriedU, -1);

  	   DFEVar nextNeigh = initCondition? 0.0: stream.offset(uFromMem, 1);
  	   optimization.pushPipeliningFactor(0.5,PipelinedOps.ALL);

  	     	   DFEVar lap = cellNumAddress===0? 	  ddt_o_dx2 * (-2.0*u + 2.0*nextNeigh) :
  		   					cellNumAddress===(nx-1)?  ddt_o_dx2 * (-2.0*u + 2.0*prevNeigh) :
												 		  ddt_o_dx2 * (prevNeigh + nextNeigh - 2.0*u);
  		   					optimization.popPipeliningFactor(PipelinedOps.ALL);
  		   		return lap;
}

	/***
	 *
	 * @param x value to check
	 * @return heaviside(x) returns the value 0 for x < 0, 1 for x > 0, and 1/2 for x = 0.
	 */

	protected DFEVar heavisidefun(DFEVar x) {


//		//check if the value is below or above zero
		DFEVar c = x<0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
//
//		//check if the value is zero
		DFEVar z = x===0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
//
//		//assign values regarding to checks - first if value is below or above zero
		DFEVar ret = c?constant.var(scalarType, 0.0) : constant.var(scalarType, 1.0);
//
//		//if x is zero, z is true, as c would not consider this case, we can disregard the value of c if z is true
		ret = z? constant.var(scalarType, 0.5) : ret;


		return ret;




	}


	protected DFEVar tanh(DFEVar x) {
		//tangens hyperbolicus: 1-(2/(e^(2*x)+1))

			DFEVar v = KernelMath.exp(2*x);
			DFEVar approximation = 1- (2/(v+1));
			return approximation;

	}





}