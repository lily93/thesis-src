/**
 * Document: MaxCompiler Tutorial (maxcompiler-tutorial)
 * Chapter: 3      Example: 1      Name: Moving Average Simple
 * MaxFile name: MovingAverageSimple
 * Summary:
 *   CPU code for the three point moving average design.
 */
#include "Maxfiles.h" 			// Includes .max files
#include <MaxSLiCInterface.h>	// Simple Live CPU interface
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
//#include "cpu.h"


#define TVP      1.4506 //The same with Flavio's paper
#define TV1M     60.    //The same with Flavio's paper
#define TV2M     1150.  //The same with Flavio's paper

#define TWP      200.0   // We have TWP1 and TWP2 = TWP in Flavio's paper

//#define TW1P     200.0
//#define TW2P     200.0

#define TW1M     60.0    //The same with Flavio's paper
#define TW2M     15.     //The same with Flavio's paper
#define TS1      2.7342  //The same with Flavio's paper
#define TS2      16.     //The same with Flavio's paper
#define TFI      0.11    //The same with Flavio's paper
#define TO1      400.    //The same with Flavio's paper
#define TO2      6.      //The same with Flavio's paper
#define TSO1     30.0181 //The same with Flavio's paper
#define TSO2     0.9957  //The same with Flavio's paper

#define TSI      1.8875  // We have TSI1 and TSI2 = TSI in Flavio's paper
//#define TSI1     1.8875
//#define TSI2     1.8875


#define TWINF    0.07    //The same with Flavio's paper
#define THV      0.3     //EPUM // The same of Flavio's paper
#define THVM     0.006   //EPUQ // The same of Flavio's paper
#define THVINF   0.006   //EPUQ // The same of Flavio's paper
#define THW      0.13    //EPUP // The same of Flavio's paper
#define THWINF   0.006    //EPURR // In Flavio's paper 0.13
#define THSO     0.13    //EPUP // The same of Flavio's paper
#define THSI     0.13    //EPUP // The same of Flavio's paper
#define THO      0.006    //EPURR // The same of Flavio's paper
//#define KWP      5.7
#define KWM      65.     //The same of Flavio's paper
#define KS       2.0994  //The same of Flavio's paper
#define KSO      2.0458  //The same of Flavio's paper
//#define KSI      97.8
#define UWM      0.03    //The same of Flavio's paper
#define US       0.9087  //The same of Flavio's paper
#define UO       0.     // The same of Flavio's paper
#define UU       1.55   // The same of Flavio's paper
#define USO      0.65   // The same of Flavio's paper
#define SC       0.007
//#define WCP      0.15

#define WINFSTAR 0.94   // The same of Flavio's paper

#define TW2M_TW1M -45.0
#define TSO2_TSO1 -29.0224

#define TW2M_TW1M_DIVBY2 -22.5
#define TSO2_TSO1_DIVBY2 -14.5112

#define NEXT_LINE(fp) while(getc(fp)!='\n');
double dt = 0;
double ddt_o_dx2 = 0;
double dx = 0;
double diff = 0;
int number_of_cells = 0;
int num_iteration = 0;
const int stimDur = 5;
int simTime = 0;
const int preciscion = 1;
int spiral = 0;
int bits = 0;

void print64( double * u_out, double * v_out, double * s_out, double * w_out, double * stim_out) {
	char filename[1024];
		sprintf(filename, "cellcable_out_maxeler_%f_%d_%d_%d.csv", dt, number_of_cells ,simTime, bits);
		if( access(filename, F_OK) != -1) {
				remove(filename);
			}
		FILE *fp = fopen(filename, "w");

	int ticks = number_of_cells * num_iteration;

	if(fp==NULL) {
		printf("no file found");
	}
	else {
		//printf("u;v;w;s;stim\n");

		fprintf(fp,"u;v;w;s;stim\n");

for (int i = 0;i<ticks;i++) {
if(i%number_of_cells==0) {
//	printf("\n");


}
//printf("%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
fprintf(fp,"%g;%g;%g;%g;%g\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);



}
fclose(fp);
fp = NULL;
	}
}
void print32( float * u_out, float * v_out, float * s_out, float * w_out, float * stim_out) {
	char filename[1024];
		sprintf(filename, "cellcable_out_maxeler_%f_%d_%d_%d.csv", dt, number_of_cells ,simTime, bits);
		if( access(filename, F_OK) != -1) {
				remove(filename);
			}
		FILE *fp = fopen(filename, "w");

	int ticks = number_of_cells * num_iteration;

	if(fp==NULL) {
		printf("no file found");
	}
	else {
		//printf("u;v;w;s;stim\n");

		fprintf(fp,"u;v;w;s;stim\n");

for (int i = 0;i<ticks;i++) {
if(i%number_of_cells==0) {
//	printf("\n");


}
//printf("%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
fprintf(fp,"%g;%g;%g;%g;%g\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);



}
fclose(fp);
fp = NULL;
	}
}

double heavisidefun(double x) {
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}


void  cstep_epi ( double *ui, double *vi, double *wi, double *si, double lap, double stim){

	  double tvm, vinf, winf;
	  double jfi, jso, jsi;
	  double twm, tso, ts, to, ds, dw, dv;

	  tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
      twm =    TW1M + (  TW2M_TW1M_DIVBY2)*(1.+tanh(  KWM*(*ui-  UWM)));
      tso =    TSO1 + (  TSO2_TSO1_DIVBY2)*(1.+tanh(  KSO*(*ui-  USO)));
      ts  = (*ui >   THW)  ?   TS2  :   TS1;
      to  = (*ui >   THO)  ?   TO2  :   TO1;

      vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  winf = (*ui >    THWINF) ?   WINFSTAR: 1.0-*ui/  TWINF;
      if (winf > 1) winf = 1.0;

      dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
      dw = (*ui >   THW) ? -*wi/  TWP : (winf-*wi)/twm;
      ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;

      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




	  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
	  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
	  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
	  //if (winf >one_o_twm) winf = one_o_twm;
	  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
	  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
	  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;





	  //Update gates

       *vi += dv*dt;
       *wi += dw*dt;
       *si += ds*dt;

	  //Compute currents
      jfi = (*ui >   THV)  ? -*vi * (*ui -   THV) * (  UU - *ui)/  TFI : 0.0;
      /*if (*ui >   THV){
         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
         	  }
           }*/
      jso = (*ui >   THSO) ? 1./tso : (*ui-  UO)/to;
      jsi = (*ui >   THSI) ? -*wi * *si/  TSI : 0.0;

      *ui = *ui  - (jfi+jso+jsi-stim)*dt + lap;
}

double diffusionTerm(double curr, double prev, double next, int index) {
	double lap;
	if(index==0) {
            lap = ddt_o_dx2 * (-2*curr + 2*next);
        } else if(index==(number_of_cells-1)) {
            lap = ddt_o_dx2 * (-2*curr +2*prev);
        }else {
          lap = ddt_o_dx2 * (next + prev - 2.0 *curr);
        }

	return lap;

}

void initValues(double * u, double * v, double * w, double * s) {
	for(int i = 0; i<number_of_cells; i++) {
		u[i] = 0.0;
		v[i] = 1.0;
		w[i] = 1.0;
		s[i] = 0.0;

	}
}

//
//double * runCPU() {
//	int sizeBytesCells = number_of_cells * sizeof(double);
//	double * us = calloc((num_iteration*number_of_cells), sizeof(double));
//	double * uCable = malloc(sizeBytesCells);
//	double * vCable = malloc(sizeBytesCells);
//	double * wCable = malloc(sizeBytesCells);
//	double * sCable = malloc(sizeBytesCells);
//
//
//
//	struct timeval start, end;
//		long mtime, seconds, useconds;
//
//	initValues(uCable, vCable, wCable, sCable);
//
//	double jfi;
//	double jso;
//	double jsi;
//
//	double lap = 0;
//
//	double stim = 0;
//	double t1 = 0;
//	double t2 = 0;
//	double prev = 0;
//	double next = 0;
//	double err = 0;
//	int c = 0;
//
//	printf("Running CPU\n");
//			//gettimeofday(&start, NULL);
//	  for (int i=0; i<num_iteration; i++) {
//		  	t1 += dt;
//			t2 += dt;
//
//
//		 	stim= ((heavisidefun(t1 - 0)*(1 - heavisidefun(t2 - 1)) + heavisidefun(t1 - 300)*(1 - heavisidefun(t2 - 301)) + heavisidefun(t1 - 700)*(1 - heavisidefun(t2 - 701)))==1)? 0.66 : 0;
//
//
//
//	            for (int j=0; j<number_of_cells; j++) {
//	            	 double  prev = j==0? 0 : uCable[j-1];
//	            	 double  next = j==number_of_cells? 0 : uCable[j+1];
//			stim = j<stimDur ? stim : 0;
//
//			lap = diffusionTerm(uCable[j], prev, next,j);
//	                         	cstep_epi(&uCable[j], &vCable[j], &wCable[j], &sCable[j], lap, stim);
////			writeToFile(uCable[j], vCable[j], wCable[j], sCable[j], stim);
//
//	                   us[c] = uCable[j];
//	                     c++;
//		//printf("u: %f\n v: %f\n, w: %f\n, s: %f\n, stim: %f\n", uCable[j], vCable[j], wCable[j], sCable[j], stim);
//
//
//	            }
//
//
//
//	            }
//
////
////		gettimeofday(&end, NULL);
////	  seconds = end.tv_sec - start.tv_sec;
////	  		useconds = end.tv_usec - start.tv_usec;
////
////	  		mtime = ((seconds) * 1000 + useconds / 1000.0);
////
////	  		printf("CPU elapsed time: %ld milliseconds \n", mtime);
//	  printf("ERROR: %g\n", err);
//
//		 free(uCable);
//	  	uCable = NULL;
//	  	free(vCable);
//	  	vCable = NULL;
//	  	free(wCable);
//	  	wCable = NULL;
//
//
//}
//
//void runDFE64() {
//
//	int sizeBytes = (number_of_cells*num_iteration) * sizeof(double);
//
//
//	double * u_out = malloc(sizeBytes);
//	double * v_out = malloc(sizeBytes);
//	double * w_out = malloc(sizeBytes);
//	double * s_out =  malloc(sizeBytes);
//	double * stim_out= malloc(sizeBytes);
//
//
//
//
//	struct timeval start, end;
//		long mtime, seconds, useconds;
//
//
//
//
//		max_file_t *myMaxFile = CellCableDFE_init();
//		max_engine_t *myDFE = max_load(myMaxFile, "local:0");
//
//
//
//		const int ticks = num_iteration*number_of_cells;
//
//
//		CellCableDFE_actions_t actions;
//
//		actions.param_length = ticks;
//		actions.param_dt = dt;
//		actions.param_ddt_o_dx2 = ddt_o_dx2;
//		actions.param_simTime = num_iteration;
//		actions.param_nx = number_of_cells;
//		actions.outstream_u_out = u_out;
//		actions.outstream_v_out = v_out;
//		actions.outstream_w_out = w_out;
//		actions.outstream_s_out = s_out;
//	//	actions.outstream_stim_out = stim_out;
//
//
//
//
//
//
//			printf("Running DFE 64\n");
//			gettimeofday(&start, NULL);
//
//			CellCableDFE_run(myDFE, &actions);
//			printf("Printing Data from DFE\n");
//			print64(u_out, v_out, s_out, w_out, stim_out);
//
//			gettimeofday(&end, NULL);
//			seconds = end.tv_sec - start.tv_sec;
//			useconds = end.tv_usec - start.tv_usec;
//
//			mtime = ((seconds) * 1000 + useconds / 1000.0);
//
//			printf("DFE elapsed time: %ld milliseconds \n", mtime);
//			printTimes(mtime);
//			//runCPU(u_out);
//
//			free(u_out);
//			u_out = NULL;
//			free(v_out);
//			v_out = NULL;
//			free(w_out);
//			w_out = NULL;
//			free(stim_out);
//			stim_out = NULL;
//			max_unload(myDFE);
//
//
//
//}
//
//void runDFE56() {
//
//	int sizeBytes = (number_of_cells*num_iteration) * sizeof(double);
//
//
//	double * u_out = malloc(sizeBytes);
//	double * v_out = malloc(sizeBytes);
//	double * w_out = malloc(sizeBytes);
//	double * s_out =  malloc(sizeBytes);
//	double * stim_out= malloc(sizeBytes);
//
//
//
//
//	struct timeval start, end;
//		long mtime, seconds, useconds;
//
//
//
//
//		max_file_t *myMaxFile = CellCableDFE56_init();
//		max_engine_t *myDFE = max_load(myMaxFile, "local:0");
//
//
//
//		const int ticks = num_iteration*number_of_cells;
//
//
//		CellCableDFE56_actions_t actions;
//
//		actions.param_length = ticks;
//		actions.param_dt = dt;
//		actions.param_ddt_o_dx2 = ddt_o_dx2;
//		actions.param_simTime = num_iteration;
//		actions.param_nx = number_of_cells;
//		actions.outstream_u_out = u_out;
//		actions.outstream_v_out = v_out;
//		actions.outstream_w_out = w_out;
//		actions.outstream_s_out = s_out;
//	//	actions.outstream_stim_out = stim_out;
//
//
//
//
//
//
//			printf("Running DFE 56\n");
//			gettimeofday(&start, NULL);
//
//			CellCableDFE56_run(myDFE, &actions);
//			printf("Printing Data from DFE 56\n");
//			print64(u_out, v_out, s_out, w_out, stim_out);
//
//			gettimeofday(&end, NULL);
//			seconds = end.tv_sec - start.tv_sec;
//			useconds = end.tv_usec - start.tv_usec;
//
//			mtime = ((seconds) * 1000 + useconds / 1000.0);
//
//			printf("DFE elapsed time: %ld milliseconds \n", mtime);
//			printTimes(mtime,56);
////			runCPU(u_out);
//
//			free(u_out);
//			u_out = NULL;
//			free(v_out);
//			v_out = NULL;
//			free(w_out);
//			w_out = NULL;
//			free(stim_out);
//			stim_out = NULL;
//			max_unload(myDFE);
//
//
//
//}
//
//void runDFE48() {
//
//	int sizeBytes = (number_of_cells*num_iteration) * sizeof(float);
//
//
//	float * u_out = malloc(sizeBytes);
//	float * v_out = malloc(sizeBytes);
//	float * w_out = malloc(sizeBytes);
//	float * s_out =  malloc(sizeBytes);
//	float * stim_out= malloc(sizeBytes);
//
//
//
//
//	struct timeval start, end;
//		long mtime, seconds, useconds;
//
//
//
//
//		max_file_t *myMaxFile = CellCableDFE48_init();
//		max_engine_t *myDFE = max_load(myMaxFile, "local:0");
//
//
//
//		const int ticks = num_iteration*number_of_cells;
//
//
//		CellCableDFE48_actions_t actions;
//
//		actions.param_length = ticks;
//		actions.param_dt = dt;
//		actions.param_ddt_o_dx2 = ddt_o_dx2;
//		actions.param_simTime = num_iteration;
//		actions.param_nx = number_of_cells;
//		actions.outstream_u_out = u_out;
//		actions.outstream_v_out = v_out;
//		actions.outstream_w_out = w_out;
//		actions.outstream_s_out = s_out;
//	//	actions.outstream_stim_out = stim_out;
//
//
//
//
//
//
//			printf("Running DFE 48\n");
//			gettimeofday(&start, NULL);
//
//			CellCableDFE48_run(myDFE, &actions);
//			printf("Printing Data from DFE 48\n");
//			print32(u_out, v_out, s_out, w_out, stim_out);
//
//			gettimeofday(&end, NULL);
//			seconds = end.tv_sec - start.tv_sec;
//			useconds = end.tv_usec - start.tv_usec;
//
//			mtime = ((seconds) * 1000 + useconds / 1000.0);
//
//			printf("DFE elapsed time: %ld milliseconds \n", mtime);
//			printTimes(mtime,48);
////			runCPU(u_out);
//
//			free(u_out);
//			u_out = NULL;
//			free(v_out);
//			v_out = NULL;
//			free(w_out);
//			w_out = NULL;
//			free(stim_out);
//			stim_out = NULL;
//			max_unload(myDFE);
//
//
//
//}

void runDFE32() {

	int sizeBytes = (number_of_cells*num_iteration) * sizeof(float);


	float * u_out = malloc(sizeBytes);
	float * v_out = malloc(sizeBytes);
	float * w_out = malloc(sizeBytes);
	float * s_out =  malloc(sizeBytes);
	float * stim_out= malloc(sizeBytes);




	struct timeval start, end;
		long mtime, seconds, useconds;




		max_file_t *myMaxFile = CellCableDFE32_init();
		max_engine_t *myDFE = max_load(myMaxFile, "local:0");



		const int ticks = num_iteration*number_of_cells;


		CellCableDFE32_actions_t actions;

		actions.param_length = ticks;
		actions.param_dt = dt;
		actions.param_ddt_o_dx2 = ddt_o_dx2;
		actions.param_simTime = num_iteration;
		actions.param_nx = number_of_cells;
		actions.outstream_u_out = u_out;
		actions.outstream_v_out = v_out;
		actions.outstream_w_out = w_out;
		actions.outstream_s_out = s_out;
	//	actions.outstream_stim_out = stim_out;






			printf("Running DFE 32\n");
			gettimeofday(&start, NULL);

			CellCableDFE32_run(myDFE, &actions);
			printf("Printing Data from DFE 32\n");
			print32(u_out, v_out, s_out, w_out, stim_out);

			gettimeofday(&end, NULL);
			seconds = end.tv_sec - start.tv_sec;
			useconds = end.tv_usec - start.tv_usec;

			mtime = ((seconds) * 1000 + useconds / 1000.0);

			printf("DFE 32 elapsed time: %ld milliseconds \n", mtime);
			printTimes(mtime,32);
//			runCPU(u_out);

			free(u_out);
			u_out = NULL;
			free(v_out);
			v_out = NULL;
			free(w_out);
			w_out = NULL;
			free(stim_out);
			stim_out = NULL;
			max_unload(myDFE);



}

void writeToFile(char * filename, double u, double v, double s, double  w, double stim) {
FILE *fp = fopen(filename, "a");
fprintf(fp, "%g\t%f\t%f\t%f\t%f\n", u, v,  s, w,stim);
fclose(fp);
}
void initParam(char * params) {

	  FILE *fp;
	  fp=fopen(params,"r");
	  if(fp==NULL)
	  {
		  printf("NO FILE FOUND: USING DEFAULT");
		  dt                      =   0.05;//Original is 0.1
		  dx                      =   0.02;//Original is 0.25
		  diff                    =   0.00116;
		  number_of_cells = 50;
		  simTime = 2;
		  spiral = 0;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%f",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %f\n", dt);
		fscanf(fp,"%f",&dx);     NEXT_LINE(fp);
		printf("READ DX (CM): %f\n", dx);
		fscanf(fp,"%f",&diff);     NEXT_LINE(fp);
		printf("READ DIFF: %f\n", diff);
		fscanf(fp,"%i",&number_of_cells);     NEXT_LINE(fp);
		printf("READ NUMBER OF CELLS: %i\n", number_of_cells);
		fscanf(fp, "%i", &spiral);
		printf("READ SPIRAL: %i\n", spiral);

	   fclose(fp);

	  }
	  ddt_o_dx2      = dt*diff/(dx * dx);
	  num_iteration = (int)(simTime/dt);


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("number of cells     = %d\n", number_of_cells);
	  printf("dt     = %f\n", dt);
	  printf("dx    = %f\n", dx);
	  printf("diff   = %f\n", diff);
	  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);



}

void printTimes(long time, int bits) {
	FILE *fp = fopen("DFE_elapsedtime.dat", "a");
	fprintf(fp, "Bits: %d\nSimulation Time (ms) %d\nNumber of Cells: %d\nStep Size: %f\nElapsed Time: %ld\n-----------------------------------\n", bits, simTime, number_of_cells, dt, time );
	fclose(fp);

}

//void testAll() {
//	dt = 0.0125;
//	 dx                      =   0.02;//Original is 0.25
//			  diff                    =   0.00116;
//			  number_of_cells = 256;
//			  simTime = 2;
//			  ddt_o_dx2      = dt*diff/(dx * dx);
//				  num_iteration = (int)(simTime/dt);
//
//
//
//while(number_of_cells<10000000) {
//	  printf("\n\n Parameters: \n");
//	  printf("timesteps    = %d\n", num_iteration);
//	  printf("number of cells     = %d\n", number_of_cells);
//	  printf("dt     = %f\n", dt);
//	  printf("dx    = %f\n", dx);
//	  printf("diff   = %f\n", diff);
//	  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);
//	  bits = 64;
//	runDFE64();
//	bits = 32;
//	runDFE32();
//	bits = 48;
//	runDFE48();
//	bits = 56;
//	runDFE56();
//	number_of_cells = number_of_cells*2;
//}
//
//}
void test_DFE32(int time) {
	dt = 0.0125;
	 dx                      =   0.02;//Original is 0.25
			  diff                    =   0.00116;
			  number_of_cells = 256;
			  simTime = time;
			  ddt_o_dx2      = dt*diff/(dx * dx);
				  num_iteration = (int)(simTime/dt);
				  bits = 32;

while(number_of_cells<1000000) {

				  printf("\n\n Parameters: \n");
				  printf("timesteps    = %d\n", num_iteration);
				  printf("number of cells     = %d\n", number_of_cells);
				  printf("dt     = %f\n", dt);
				  printf("dx    = %f\n", dx);
				  printf("diff   = %f\n", diff);
				  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);
				  printf("precision   = %d\n", bits);

	runDFE32();
	number_of_cells = number_of_cells*2;
}
}

//void test_DFE64(int time) {
//	dt = 0.0125;
//	 dx                      =   0.02;//Original is 0.25
//			  diff                    =   0.00116;
//			  number_of_cells = 256;
//			  simTime = time;
//			  ddt_o_dx2      = dt*diff/(dx * dx);
//				  num_iteration = (int)(simTime/dt);
//				  bits = 64;
//
//while(number_of_cells<500000) {
//
//				  printf("\n\n Parameters: \n");
//				  printf("timesteps    = %d\n", num_iteration);
//				  printf("number of cells     = %d\n", number_of_cells);
//				  printf("dt     = %f\n", dt);
//				  printf("dx    = %f\n", dx);
//				  printf("diff   = %f\n", diff);
//				  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);
//				  printf("precision   = %d\n", bits);
//
//	runDFE64();
//	number_of_cells = number_of_cells*2;
//}
//}
//void error_per_cell(int time, int bit) {
//	dt = 0.0125;
//		 dx                      =   0.02;//Original is 0.25
//				  diff                    =   0.00116;
//				  number_of_cells = 256;
//				  simTime = time;
//				  ddt_o_dx2      = dt*diff/(dx * dx);
//					  num_iteration = (int)(simTime/dt);
//					  bits = bit;
//
//
//					  printf("\n\n Parameters: \n");
//					  printf("timesteps    = %d\n", num_iteration);
//					  printf("number of cells     = %d\n", number_of_cells);
//					  printf("dt     = %f\n", dt);
//					  printf("dx    = %f\n", dx);
//					  printf("diff   = %f\n", diff);
//					  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);
//					  printf("precision   = %d\n", bits);
//
//	switch(bit) {
//	case 32:
//		runDFE32();
//		break;
//	case 64:
//		runDFE64();
//		break;
//	case 48:
//		runDFE48();
//		break;
//	case 56:
//		runDFE56();
//		break;
//	default:
//		runDFE32();
//
//
//	}
//	double * ref = runCPU();
//}
int main()
{
	test_DFE32(2);
	test_DFE32(1000);
	//test_DFE64(2);
	//test_DFE64(1000);




	return 0;
}
