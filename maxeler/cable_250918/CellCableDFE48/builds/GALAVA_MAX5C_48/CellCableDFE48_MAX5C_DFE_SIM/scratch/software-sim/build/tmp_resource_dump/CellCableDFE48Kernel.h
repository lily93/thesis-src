#ifndef CELLCABLEDFE48KERNEL_H_
#define CELLCABLEDFE48KERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFE48Kernel : public KernelManagerBlockSync {
public:
  CellCableDFE48Kernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id43out_value;

  HWOffsetFix<8,0,UNSIGNED> id936out_value;

  HWOffsetFix<8,0,UNSIGNED> id774out_output[2];

  HWOffsetFix<8,0,UNSIGNED> id48out_count;
  HWOffsetFix<1,0,UNSIGNED> id48out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id48st_count;

  HWOffsetFix<8,0,UNSIGNED> id1009out_value;

  HWOffsetFix<8,0,UNSIGNED> id654out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id703out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id656out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id907out_output[223];

  HWOffsetFix<1,0,UNSIGNED> id775out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id40out_nx;

  HWOffsetFix<32,0,UNSIGNED> id47out_count;
  HWOffsetFix<1,0,UNSIGNED> id47out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id47st_count;

  HWOffsetFix<1,0,UNSIGNED> id776out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id41out_simTime;

  HWOffsetFix<32,0,UNSIGNED> id1008out_value;

  HWOffsetFix<32,0,UNSIGNED> id45out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id46out_count;
  HWOffsetFix<1,0,UNSIGNED> id46out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id46st_count;

  HWOffsetFix<32,0,UNSIGNED> id1007out_value;

  HWOffsetFix<1,0,UNSIGNED> id704out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id893out_output[22];

  HWOffsetFix<32,0,UNSIGNED> id911out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id912out_output[171];

  HWOffsetFix<32,0,UNSIGNED> id778out_output[28];

  HWOffsetFix<8,0,UNSIGNED> id1006out_value;

  HWOffsetFix<8,0,UNSIGNED> id627out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id705out_result[2];

  HWFloat<8,40> id681out_doutb[3];

  HWFloat<8,40> id681sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_1 (const char* _format_arg_format_string);
  HWFloat<8,40> id60out_value;

  HWFloat<8,40> id61out_result[2];

  HWFloat<8,40> id901out_output[2];

  HWFloat<8,40> id913out_output[6];

  HWFloat<8,40> id914out_output[25];

  HWFloat<8,40> id915out_output[19];

  HWFloat<8,40> id916out_output[24];

  HWFloat<8,40> id917out_output[28];

  HWFloat<8,40> id918out_output[3];

  HWFloat<8,40> id919out_output[31];

  HWFloat<8,40> id920out_output[10];

  HWFloat<8,40> id921out_output[7];

  HWFloat<8,40> id922out_output[10];

  HWFloat<8,40> id923out_output[4];

  HWFloat<8,40> id924out_output[22];

  HWFloat<8,40> id925out_output[7];

  HWFloat<8,40> id926out_output[21];

  HWFloat<8,40> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id592out_result[2];

  HWFloat<8,40> id34out_value;

  HWOffsetFix<1,0,UNSIGNED> id783out_output[2];

  HWFloat<8,40> id782out_output[72];

  HWOffsetFix<8,0,UNSIGNED> id1005out_value;

  HWOffsetFix<8,0,UNSIGNED> id631out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id706out_result[2];

  HWFloat<8,40> id684out_doutb[3];

  HWFloat<8,40> id684sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_2 (const char* _format_arg_format_string);
  HWFloat<8,40> id784out_output[2];

  HWFloat<8,40> id62out_value;

  HWFloat<8,40> id63out_result[2];

  HWFloat<8,40> id786out_output[3];

  HWFloat<8,40> id927out_output[7];

  HWFloat<8,40> id928out_output[37];

  HWOffsetFix<1,0,UNSIGNED> id462out_result[2];

  HWFloat<8,40> id17out_value;

  HWOffsetFix<1,0,UNSIGNED> id451out_result[2];

  HWFloat<8,40> id35out_value;

  HWFloat<8,40> id452out_result[2];

  HWFloat<8,40> id460out_output[2];

  HWFloat<8,40> id465out_result[7];

  HWFloat<8,40> id16out_value;

  HWOffsetFix<1,0,UNSIGNED> id226out_result[2];

  HWFloat<8,40> id1out_value;

  HWFloat<8,40> id2out_value;

  HWFloat<8,40> id227out_result[2];

  HWFloat<8,40> id446out_output[2];

  HWFloat<8,40> id466out_result[23];

  HWFloat<8,40> id0out_value;

  HWFloat<8,40> id464out_result[23];

  HWFloat<8,40> id467out_result[2];

  HWFloat<8,40> id583out_output[2];

  HWFloat<11,53> id36out_dt;

  HWFloat<8,40> id37out_o[4];

  HWFloat<8,40> id586out_result[13];

  HWFloat<8,40> id587out_result[13];

  HWFloat<8,40> id781out_output[89];

  HWFloat<8,40> id594out_result[7];

  HWFloat<8,40> id595out_result[7];

  HWFloat<8,40> id28out_value;

  HWFloat<8,40> id596out_result[7];

  HWFloat<8,40> id597out_result[7];

  HWFloat<8,40> id8out_value;

  HWFloat<8,40> id598out_result[23];

  HWFloat<8,40> id599out_result[2];

  HWFloat<8,40> id19out_value;

  HWOffsetFix<1,0,UNSIGNED> id600out_result[2];

  HWFloat<8,40> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id444out_result[2];

  HWFloat<8,40> id9out_value;

  HWFloat<8,40> id10out_value;

  HWFloat<8,40> id445out_result[2];

  HWFloat<8,40> id450out_output[2];

  HWFloat<8,40> id604out_result[23];

  HWFloat<8,40> id1004out_value;

  HWFloat<8,40> id11out_value;

  HWFloat<8,40> id31out_value;

  HWFloat<8,40> id1003out_value;

  HWFloat<8,40> id1002out_value;

  HWFloat<8,40> id1001out_value;

  HWFloat<8,40> id24out_value;

  HWFloat<8,40> id29out_value;

  HWFloat<8,40> id335out_result[7];

  HWFloat<8,40> id336out_result[7];

  HWFloat<8,40> id750out_floatOut[2];

  HWRawBits<8> id416out_value;

  HWOffsetFix<1,0,UNSIGNED> id707out_result[2];

  HWRawBits<39> id1000out_value;

  HWOffsetFix<1,0,UNSIGNED> id708out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id815out_output[42];

  HWOffsetFix<1,0,UNSIGNED> id339out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id341out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id341out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id344out_value;

  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id343out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id343out_result_doubt[7];

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id345out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id345out_o_doubt[2];

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id797out_output[27];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id385out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id741out_value;

  HWOffsetFix<10,0,UNSIGNED> id798out_output[15];

  HWOffsetFix<29,-40,UNSIGNED> id431out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id431sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id799out_output[7];

  HWOffsetFix<39,-40,UNSIGNED> id428out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id428sta_rom_store[1024];

  HWOffsetFix<1,0,UNSIGNED> id800out_output[2];

  HWOffsetFix<40,-40,UNSIGNED> id425out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id425sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id362out_value;

  HWOffsetFix<43,-64,UNSIGNED> id361out_result[3];

  HWOffsetFix<22,-43,UNSIGNED> id363out_o[2];

  HWOffsetFix<44,-43,UNSIGNED> id364out_result[2];

  HWOffsetFix<44,-43,UNSIGNED> id801out_output[2];

  HWOffsetFix<62,-83,UNSIGNED> id365out_result[3];

  HWOffsetFix<64,-63,UNSIGNED> id366out_result[3];

  HWOffsetFix<44,-43,UNSIGNED> id367out_o[2];

  HWOffsetFix<45,-43,UNSIGNED> id368out_result[2];

  HWOffsetFix<45,-43,UNSIGNED> id802out_output[5];

  HWOffsetFix<64,-64,UNSIGNED> id369out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id370out_result[3];

  HWOffsetFix<45,-43,UNSIGNED> id371out_o[2];

  HWOffsetFix<46,-43,UNSIGNED> id372out_result[2];

  HWOffsetFix<46,-43,UNSIGNED> id803out_output[5];

  HWOffsetFix<64,-73,UNSIGNED> id373out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id374out_result[3];

  HWOffsetFix<46,-43,UNSIGNED> id375out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id376out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id999out_value;

  HWOffsetFix<1,0,UNSIGNED> id709out_result[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id749out_result[2];

  HWFloat<8,40> id998out_value;

  HWOffsetFix<1,0,UNSIGNED> id347out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id804out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id805out_output[28];

  HWFloat<8,40> id997out_value;

  HWOffsetFix<1,0,UNSIGNED> id351out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id806out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id807out_output[28];

  HWOffsetFix<1,0,UNSIGNED> id811out_output[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id996out_value;

  HWOffsetFix<1,0,UNSIGNED> id712out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id809out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id929out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id395out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id813out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id812out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id379out_value;

  HWOffsetFix<40,-39,UNSIGNED> id380out_result[2];

  HWOffsetFix<39,-39,UNSIGNED> id814out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id404out_value;

  HWOffsetFix<8,0,UNSIGNED> id405out_value;

  HWOffsetFix<39,0,UNSIGNED> id407out_value;

  HWFloat<8,40> id700out_value;

  HWFloat<8,40> id412out_result[2];

  HWFloat<8,40> id995out_value;

  HWFloat<8,40> id422out_result[2];

  HWFloat<8,40> id994out_value;

  HWFloat<8,40> id433out_result[7];

  HWFloat<8,40> id435out_result[23];

  HWFloat<8,40> id437out_result[7];

  HWFloat<8,40> id439out_result[7];

  HWFloat<8,40> id440out_result[7];

  HWFloat<8,40> id441out_result[7];

  HWFloat<8,40> id448out_output[2];

  HWFloat<8,40> id602out_result[23];

  HWFloat<8,40> id605out_result[2];

  HWFloat<8,40> id612out_result[7];

  HWFloat<8,40> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id606out_result[2];

  HWFloat<8,40> id610out_value;

  HWFloat<8,40> id818out_output[60];

  HWOffsetFix<8,0,UNSIGNED> id993out_value;

  HWOffsetFix<8,0,UNSIGNED> id635out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id714out_result[2];

  HWFloat<8,40> id683out_doutb[3];

  HWFloat<8,40> id683sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_3 (const char* _format_arg_format_string);
  HWFloat<8,40> id820out_output[2];

  HWFloat<8,40> id64out_value;

  HWFloat<8,40> id65out_result[2];

  HWFloat<8,40> id825out_output[103];

  HWFloat<8,40> id930out_output[7];

  HWFloat<8,40> id931out_output[37];

  HWFloat<8,40> id18out_value;

  HWOffsetFix<1,0,UNSIGNED> id468out_result[2];

  HWFloat<8,40> id33out_value;

  HWOffsetFix<1,0,UNSIGNED> id453out_result[2];

  HWFloat<8,40> id992out_value;

  HWFloat<8,40> id14out_value;

  HWFloat<8,40> id454out_result[23];

  HWFloat<8,40> id456out_result[7];

  HWFloat<8,40> id32out_value;

  HWFloat<8,40> id457out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id458out_result[2];

  HWFloat<8,40> id824out_output[2];

  HWFloat<8,40> id459out_result[2];

  HWFloat<8,40> id461out_output[2];

  HWFloat<8,40> id471out_result[7];

  HWFloat<8,40> id4out_value;

  HWFloat<8,40> id30out_value;

  HWFloat<8,40> id991out_value;

  HWFloat<8,40> id990out_value;

  HWFloat<8,40> id989out_value;

  HWFloat<8,40> id22out_value;

  HWFloat<8,40> id25out_value;

  HWFloat<8,40> id228out_result[7];

  HWFloat<8,40> id229out_result[7];

  HWFloat<8,40> id751out_floatOut[2];

  HWRawBits<8> id309out_value;

  HWOffsetFix<1,0,UNSIGNED> id715out_result[2];

  HWRawBits<39> id988out_value;

  HWOffsetFix<1,0,UNSIGNED> id716out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id844out_output[42];

  HWOffsetFix<1,0,UNSIGNED> id232out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id234out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id234out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id237out_value;

  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id236out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id236out_result_doubt[7];

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id238out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id238out_o_doubt[2];

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id826out_output[27];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id278out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id743out_value;

  HWOffsetFix<10,0,UNSIGNED> id827out_output[15];

  HWOffsetFix<29,-40,UNSIGNED> id324out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id324sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id828out_output[7];

  HWOffsetFix<39,-40,UNSIGNED> id321out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id321sta_rom_store[1024];

  HWOffsetFix<1,0,UNSIGNED> id829out_output[2];

  HWOffsetFix<40,-40,UNSIGNED> id318out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id318sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id255out_value;

  HWOffsetFix<43,-64,UNSIGNED> id254out_result[3];

  HWOffsetFix<22,-43,UNSIGNED> id256out_o[2];

  HWOffsetFix<44,-43,UNSIGNED> id257out_result[2];

  HWOffsetFix<44,-43,UNSIGNED> id830out_output[2];

  HWOffsetFix<62,-83,UNSIGNED> id258out_result[3];

  HWOffsetFix<64,-63,UNSIGNED> id259out_result[3];

  HWOffsetFix<44,-43,UNSIGNED> id260out_o[2];

  HWOffsetFix<45,-43,UNSIGNED> id261out_result[2];

  HWOffsetFix<45,-43,UNSIGNED> id831out_output[5];

  HWOffsetFix<64,-64,UNSIGNED> id262out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id263out_result[3];

  HWOffsetFix<45,-43,UNSIGNED> id264out_o[2];

  HWOffsetFix<46,-43,UNSIGNED> id265out_result[2];

  HWOffsetFix<46,-43,UNSIGNED> id832out_output[5];

  HWOffsetFix<64,-73,UNSIGNED> id266out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id267out_result[3];

  HWOffsetFix<46,-43,UNSIGNED> id268out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id269out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id987out_value;

  HWOffsetFix<1,0,UNSIGNED> id717out_result[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id748out_result[2];

  HWFloat<8,40> id986out_value;

  HWOffsetFix<1,0,UNSIGNED> id240out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id833out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id834out_output[28];

  HWFloat<8,40> id985out_value;

  HWOffsetFix<1,0,UNSIGNED> id244out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id835out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id836out_output[28];

  HWOffsetFix<1,0,UNSIGNED> id840out_output[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id984out_value;

  HWOffsetFix<1,0,UNSIGNED> id720out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id838out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id932out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id288out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id842out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id841out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id272out_value;

  HWOffsetFix<40,-39,UNSIGNED> id273out_result[2];

  HWOffsetFix<39,-39,UNSIGNED> id843out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id297out_value;

  HWOffsetFix<8,0,UNSIGNED> id298out_value;

  HWOffsetFix<39,0,UNSIGNED> id300out_value;

  HWFloat<8,40> id701out_value;

  HWFloat<8,40> id305out_result[2];

  HWFloat<8,40> id983out_value;

  HWFloat<8,40> id315out_result[2];

  HWFloat<8,40> id982out_value;

  HWFloat<8,40> id326out_result[7];

  HWFloat<8,40> id328out_result[23];

  HWFloat<8,40> id330out_result[7];

  HWFloat<8,40> id332out_result[7];

  HWFloat<8,40> id333out_result[7];

  HWFloat<8,40> id334out_result[7];

  HWFloat<8,40> id447out_output[2];

  HWFloat<8,40> id472out_result[23];

  HWFloat<8,40> id3out_value;

  HWFloat<8,40> id470out_result[23];

  HWFloat<8,40> id473out_result[2];

  HWFloat<8,40> id584out_output[2];

  HWFloat<8,40> id588out_result[13];

  HWFloat<8,40> id589out_result[13];

  HWFloat<8,40> id849out_output[60];

  HWOffsetFix<8,0,UNSIGNED> id981out_value;

  HWOffsetFix<8,0,UNSIGNED> id639out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id722out_result[2];

  HWFloat<8,40> id685out_doutb[3];

  HWFloat<8,40> id685sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_4 (const char* _format_arg_format_string);
  HWFloat<8,40> id66out_value;

  HWFloat<8,40> id67out_result[2];

  HWFloat<8,40> id869out_output[98];

  HWFloat<8,40> id933out_output[42];

  HWFloat<8,40> id980out_value;

  HWFloat<8,40> id979out_value;

  HWFloat<8,40> id978out_value;

  HWFloat<8,40> id23out_value;

  HWFloat<8,40> id26out_value;

  HWFloat<8,40> id474out_result[7];

  HWFloat<8,40> id475out_result[7];

  HWFloat<8,40> id752out_floatOut[2];

  HWRawBits<8> id555out_value;

  HWOffsetFix<1,0,UNSIGNED> id723out_result[2];

  HWRawBits<39> id977out_value;

  HWOffsetFix<1,0,UNSIGNED> id724out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id868out_output[42];

  HWOffsetFix<1,0,UNSIGNED> id478out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id480out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id480out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id483out_value;

  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id482out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id482out_result_doubt[7];

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id484out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id484out_o_doubt[2];

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id850out_output[27];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id524out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id745out_value;

  HWOffsetFix<10,0,UNSIGNED> id851out_output[15];

  HWOffsetFix<29,-40,UNSIGNED> id570out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id570sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id852out_output[7];

  HWOffsetFix<39,-40,UNSIGNED> id567out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id567sta_rom_store[1024];

  HWOffsetFix<1,0,UNSIGNED> id853out_output[2];

  HWOffsetFix<40,-40,UNSIGNED> id564out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id564sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id501out_value;

  HWOffsetFix<43,-64,UNSIGNED> id500out_result[3];

  HWOffsetFix<22,-43,UNSIGNED> id502out_o[2];

  HWOffsetFix<44,-43,UNSIGNED> id503out_result[2];

  HWOffsetFix<44,-43,UNSIGNED> id854out_output[2];

  HWOffsetFix<62,-83,UNSIGNED> id504out_result[3];

  HWOffsetFix<64,-63,UNSIGNED> id505out_result[3];

  HWOffsetFix<44,-43,UNSIGNED> id506out_o[2];

  HWOffsetFix<45,-43,UNSIGNED> id507out_result[2];

  HWOffsetFix<45,-43,UNSIGNED> id855out_output[5];

  HWOffsetFix<64,-64,UNSIGNED> id508out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id509out_result[3];

  HWOffsetFix<45,-43,UNSIGNED> id510out_o[2];

  HWOffsetFix<46,-43,UNSIGNED> id511out_result[2];

  HWOffsetFix<46,-43,UNSIGNED> id856out_output[5];

  HWOffsetFix<64,-73,UNSIGNED> id512out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id513out_result[3];

  HWOffsetFix<46,-43,UNSIGNED> id514out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id515out_o[2];

  HWOffsetFix<40,-39,UNSIGNED> id976out_value;

  HWOffsetFix<1,0,UNSIGNED> id725out_result[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id747out_result[2];

  HWFloat<8,40> id975out_value;

  HWOffsetFix<1,0,UNSIGNED> id486out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id857out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id858out_output[28];

  HWFloat<8,40> id974out_value;

  HWOffsetFix<1,0,UNSIGNED> id490out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id859out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id860out_output[28];

  HWOffsetFix<1,0,UNSIGNED> id864out_output[2];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id973out_value;

  HWOffsetFix<1,0,UNSIGNED> id728out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id862out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id934out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id534out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id866out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id865out_output[2];

  HWOffsetFix<40,-39,UNSIGNED> id518out_value;

  HWOffsetFix<40,-39,UNSIGNED> id519out_result[2];

  HWOffsetFix<39,-39,UNSIGNED> id867out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id543out_value;

  HWOffsetFix<8,0,UNSIGNED> id544out_value;

  HWOffsetFix<39,0,UNSIGNED> id546out_value;

  HWFloat<8,40> id702out_value;

  HWFloat<8,40> id551out_result[2];

  HWFloat<8,40> id972out_value;

  HWFloat<8,40> id561out_result[2];

  HWFloat<8,40> id971out_value;

  HWFloat<8,40> id572out_result[7];

  HWFloat<8,40> id574out_result[23];

  HWFloat<8,40> id576out_result[7];

  HWFloat<8,40> id578out_result[7];

  HWFloat<8,40> id753out_floatOut[2];

  HWFloat<8,40> id581out_result[7];

  HWOffsetFix<1,0,UNSIGNED> id442out_result[2];

  HWFloat<8,40> id6out_value;

  HWFloat<8,40> id7out_value;

  HWFloat<8,40> id443out_result[2];

  HWFloat<8,40> id449out_output[2];

  HWFloat<8,40> id582out_result[23];

  HWFloat<8,40> id585out_output[2];

  HWFloat<8,40> id590out_result[13];

  HWFloat<8,40> id591out_result[13];

  HWFloat<8,40> id848out_output[8];

  HWFloat<8,40> id608out_result[7];

  HWFloat<8,40> id13out_value;

  HWFloat<8,40> id609out_result[23];

  HWFloat<8,40> id611out_result[2];

  HWFloat<8,40> id613out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id970out_value;

  HWOffsetFix<1,0,UNSIGNED> id730out_result[2];

  HWFloat<8,40> id875out_output[50];

  HWOffsetFix<8,0,UNSIGNED> id969out_value;

  HWOffsetFix<8,0,UNSIGNED> id643out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id731out_result[2];

  HWFloat<8,40> id686out_doutb[3];

  HWFloat<8,40> id686sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_5 (const char* _format_arg_format_string);
  HWFloat<8,40> id70out_value;

  HWFloat<8,40> id71out_result[2];

  HWFloat<8,40> id876out_output[149];

  HWFloat<8,40> id76out_result[13];

  HWFloat<8,40> id878out_output[7];

  HWFloat<8,40> id935out_output[2];

  HWFloat<8,40> id968out_value;

  HWOffsetFix<1,0,UNSIGNED> id86out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id88out_value;

  HWOffsetFix<1,0,UNSIGNED> id87out_value;

  HWOffsetFix<1,0,UNSIGNED> id89out_result[2];

  HWFloat<8,40> id967out_value;

  HWOffsetFix<1,0,UNSIGNED> id81out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id83out_value;

  HWOffsetFix<1,0,UNSIGNED> id82out_value;

  HWOffsetFix<1,0,UNSIGNED> id84out_result[2];

  HWFloat<8,40> id91out_value;

  HWFloat<8,40> id90out_value;

  HWFloat<8,40> id92out_result[2];

  HWFloat<8,40> id93out_value;

  HWFloat<8,40> id94out_result[2];

  HWFloat<8,40> id966out_value;

  HWOffsetFix<32,0,UNSIGNED> id965out_value;

  HWOffsetFix<1,0,UNSIGNED> id732out_result[2];

  HWFloat<8,40> id880out_output[7];

  HWFloat<8,40> id881out_output[57];

  HWOffsetFix<8,0,UNSIGNED> id964out_value;

  HWOffsetFix<8,0,UNSIGNED> id647out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id733out_result[2];

  HWFloat<8,40> id687out_doutb[3];

  HWFloat<8,40> id687sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_6 (const char* _format_arg_format_string);
  HWFloat<8,40> id74out_value;

  HWFloat<8,40> id75out_result[2];

  HWFloat<8,40> id882out_output[143];

  HWFloat<8,40> id77out_result[13];

  HWFloat<8,40> id963out_value;

  HWFloat<8,40> id96out_result[7];

  HWFloat<8,40> id962out_value;

  HWOffsetFix<1,0,UNSIGNED> id103out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id105out_value;

  HWOffsetFix<1,0,UNSIGNED> id104out_value;

  HWOffsetFix<1,0,UNSIGNED> id106out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id883out_output[2];

  HWFloat<8,40> id961out_value;

  HWOffsetFix<1,0,UNSIGNED> id98out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id100out_value;

  HWOffsetFix<1,0,UNSIGNED> id99out_value;

  HWOffsetFix<1,0,UNSIGNED> id101out_result[2];

  HWFloat<8,40> id108out_value;

  HWFloat<8,40> id107out_value;

  HWFloat<8,40> id109out_result[2];

  HWFloat<8,40> id110out_value;

  HWFloat<8,40> id111out_result[2];

  HWFloat<8,40> id113out_result[7];

  HWFloat<8,40> id114out_result[7];

  HWFloat<8,40> id960out_value;

  HWFloat<8,40> id116out_result[7];

  HWFloat<8,40> id959out_value;

  HWOffsetFix<1,0,UNSIGNED> id123out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id125out_value;

  HWOffsetFix<1,0,UNSIGNED> id124out_value;

  HWOffsetFix<1,0,UNSIGNED> id126out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id884out_output[2];

  HWFloat<8,40> id958out_value;

  HWOffsetFix<1,0,UNSIGNED> id118out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<1,0,UNSIGNED> id119out_value;

  HWOffsetFix<1,0,UNSIGNED> id121out_result[2];

  HWFloat<8,40> id128out_value;

  HWFloat<8,40> id127out_value;

  HWFloat<8,40> id129out_result[2];

  HWFloat<8,40> id130out_value;

  HWFloat<8,40> id131out_result[2];

  HWFloat<8,40> id957out_value;

  HWFloat<8,40> id956out_value;

  HWFloat<8,40> id133out_result[7];

  HWFloat<8,40> id955out_value;

  HWOffsetFix<1,0,UNSIGNED> id140out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id142out_value;

  HWOffsetFix<1,0,UNSIGNED> id141out_value;

  HWOffsetFix<1,0,UNSIGNED> id143out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id885out_output[2];

  HWFloat<8,40> id954out_value;

  HWOffsetFix<1,0,UNSIGNED> id135out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id137out_value;

  HWOffsetFix<1,0,UNSIGNED> id136out_value;

  HWOffsetFix<1,0,UNSIGNED> id138out_result[2];

  HWFloat<8,40> id145out_value;

  HWFloat<8,40> id144out_value;

  HWFloat<8,40> id146out_result[2];

  HWFloat<8,40> id147out_value;

  HWFloat<8,40> id148out_result[2];

  HWFloat<8,40> id150out_result[7];

  HWFloat<8,40> id151out_result[7];

  HWFloat<8,40> id152out_result[7];

  HWFloat<8,40> id953out_value;

  HWFloat<8,40> id154out_result[7];

  HWFloat<8,40> id952out_value;

  HWOffsetFix<1,0,UNSIGNED> id161out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id163out_value;

  HWOffsetFix<1,0,UNSIGNED> id162out_value;

  HWOffsetFix<1,0,UNSIGNED> id164out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id887out_output[2];

  HWFloat<8,40> id951out_value;

  HWOffsetFix<1,0,UNSIGNED> id156out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id158out_value;

  HWOffsetFix<1,0,UNSIGNED> id157out_value;

  HWOffsetFix<1,0,UNSIGNED> id159out_result[2];

  HWFloat<8,40> id166out_value;

  HWFloat<8,40> id165out_value;

  HWFloat<8,40> id167out_result[2];

  HWFloat<8,40> id168out_value;

  HWFloat<8,40> id169out_result[2];

  HWFloat<8,40> id950out_value;

  HWFloat<8,40> id949out_value;

  HWFloat<8,40> id171out_result[7];

  HWFloat<8,40> id948out_value;

  HWOffsetFix<1,0,UNSIGNED> id178out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id180out_value;

  HWOffsetFix<1,0,UNSIGNED> id179out_value;

  HWOffsetFix<1,0,UNSIGNED> id181out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id889out_output[2];

  HWFloat<8,40> id947out_value;

  HWOffsetFix<1,0,UNSIGNED> id173out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id175out_value;

  HWOffsetFix<1,0,UNSIGNED> id174out_value;

  HWOffsetFix<1,0,UNSIGNED> id176out_result[2];

  HWFloat<8,40> id183out_value;

  HWFloat<8,40> id182out_value;

  HWFloat<8,40> id184out_result[2];

  HWFloat<8,40> id185out_value;

  HWFloat<8,40> id186out_result[2];

  HWFloat<8,40> id188out_result[7];

  HWFloat<8,40> id189out_result[7];

  HWFloat<8,40> id190out_result[7];

  HWFloat<8,40> id946out_value;

  HWOffsetFix<1,0,UNSIGNED> id192out_result[3];

  HWOffsetFix<32,0,UNSIGNED> id945out_value;

  HWOffsetFix<1,0,UNSIGNED> id734out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id195out_result[2];

  HWFloat<8,40> id197out_value;

  HWFloat<8,40> id196out_value;

  HWFloat<8,40> id198out_result[2];

  HWFloat<8,40> id614out_result[7];

  HWFloat<8,40> id615out_result[7];

  HWFloat<8,40> id616out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id944out_value;

  HWOffsetFix<1,0,UNSIGNED> id735out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id943out_value;

  HWOffsetFix<32,0,UNSIGNED> id211out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id736out_result[2];

  HWFloat<11,53> id38out_ddt_o_dx2;

  HWFloat<8,40> id39out_o[4];

  HWFloat<8,40> id894out_output[2];

  HWFloat<8,40> id899out_output[5];

  HWOffsetFix<24,0,UNSIGNED> id895out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id897out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id942out_value;

  HWOffsetFix<32,0,UNSIGNED> id56out_result[2];

  HWFloat<8,40> id682out_doutb[3];

  HWFloat<8,40> id682sta_ram_store[10000000];

  std::string format_string_CellCableDFE48Kernel_7 (const char* _format_arg_format_string);
  HWFloat<8,40> id200out_value;

  HWFloat<8,40> id201out_result[2];

  HWFloat<8,40> id219out_result[7];

  HWFloat<8,40> id754out_floatOut[2];

  HWFloat<8,40> id222out_result[7];

  HWFloat<8,40> id223out_result[7];

  HWFloat<8,40> id755out_floatOut[2];

  HWFloat<8,40> id756out_floatOut[2];

  HWFloat<8,40> id217out_result[7];

  HWFloat<8,40> id218out_result[7];

  HWFloat<8,40> id903out_output[6];

  HWFloat<8,40> id224out_result[2];

  HWFloat<8,40> id757out_floatOut[2];

  HWFloat<8,40> id758out_floatOut[2];

  HWFloat<8,40> id208out_result[7];

  HWFloat<8,40> id209out_result[7];

  HWFloat<8,40> id905out_output[7];

  HWFloat<8,40> id225out_result[2];

  HWFloat<8,40> id906out_output[190];

  HWFloat<8,40> id617out_result[7];

  HWFloat<8,24> id649out_o[4];

  HWOffsetFix<8,0,UNSIGNED> id941out_value;

  HWOffsetFix<8,0,UNSIGNED> id661out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id737out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id663out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id908out_output[64];

  HWFloat<8,24> id650out_o[4];

  HWOffsetFix<8,0,UNSIGNED> id940out_value;

  HWOffsetFix<8,0,UNSIGNED> id668out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id738out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id670out_io_w_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id909out_output[164];

  HWFloat<8,24> id651out_o[4];

  HWOffsetFix<8,0,UNSIGNED> id939out_value;

  HWOffsetFix<8,0,UNSIGNED> id675out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id739out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id677out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id910out_output[157];

  HWFloat<8,24> id652out_o[4];

  HWOffsetFix<1,0,UNSIGNED> id692out_value;

  HWOffsetFix<1,0,UNSIGNED> id938out_value;

  HWOffsetFix<49,0,UNSIGNED> id689out_value;

  HWOffsetFix<48,0,UNSIGNED> id690out_count;
  HWOffsetFix<1,0,UNSIGNED> id690out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id690st_count;

  HWOffsetFix<1,0,UNSIGNED> id937out_value;

  HWOffsetFix<49,0,UNSIGNED> id695out_value;

  HWOffsetFix<48,0,UNSIGNED> id696out_count;
  HWOffsetFix<1,0,UNSIGNED> id696out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id696st_count;

  HWOffsetFix<48,0,UNSIGNED> id698out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id740out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_undef;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits_1;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_undef;
  const HWFloat<8,40> c_hw_flt_8_40_undef;
  const HWFloat<8,40> c_hw_flt_8_40_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits_1;
  const HWFloat<8,40> c_hw_flt_8_40_bits_2;
  const HWFloat<8,40> c_hw_flt_8_40_bits_3;
  const HWFloat<8,40> c_hw_flt_8_40_bits_4;
  const HWFloat<8,40> c_hw_flt_8_40_bits_5;
  const HWFloat<8,40> c_hw_flt_8_40_bits_6;
  const HWFloat<8,40> c_hw_flt_8_40_bits_7;
  const HWFloat<8,40> c_hw_flt_8_40_bits_8;
  const HWFloat<8,40> c_hw_flt_8_40_bits_9;
  const HWFloat<8,40> c_hw_flt_8_40_bits_10;
  const HWFloat<8,40> c_hw_flt_8_40_bits_11;
  const HWFloat<8,40> c_hw_flt_8_40_bits_12;
  const HWFloat<8,40> c_hw_flt_8_40_bits_13;
  const HWFloat<8,40> c_hw_flt_8_40_bits_14;
  const HWFloat<8,40> c_hw_flt_8_40_bits_15;
  const HWFloat<8,40> c_hw_flt_8_40_2_0val;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<39> c_hw_bit_39_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<10,0,UNSIGNED> c_hw_fix_10_0_uns_undef;
  const HWOffsetFix<29,-40,UNSIGNED> c_hw_fix_29_n40_uns_undef;
  const HWOffsetFix<39,-40,UNSIGNED> c_hw_fix_39_n40_uns_undef;
  const HWOffsetFix<40,-40,UNSIGNED> c_hw_fix_40_n40_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<44,-43,UNSIGNED> c_hw_fix_44_n43_uns_undef;
  const HWOffsetFix<45,-43,UNSIGNED> c_hw_fix_45_n43_uns_undef;
  const HWOffsetFix<46,-43,UNSIGNED> c_hw_fix_46_n43_uns_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits_1;
  const HWOffsetFix<39,-39,UNSIGNED> c_hw_fix_39_n39_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_2;
  const HWOffsetFix<39,0,UNSIGNED> c_hw_fix_39_0_uns_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits_16;
  const HWFloat<8,40> c_hw_flt_8_40_bits_17;
  const HWFloat<8,40> c_hw_flt_8_40_bits_18;
  const HWFloat<8,40> c_hw_flt_8_40_bits_19;
  const HWFloat<8,40> c_hw_flt_8_40_bits_20;
  const HWFloat<8,40> c_hw_flt_8_40_bits_21;
  const HWFloat<8,40> c_hw_flt_8_40_bits_22;
  const HWFloat<8,40> c_hw_flt_8_40_bits_23;
  const HWFloat<8,40> c_hw_flt_8_40_bits_24;
  const HWFloat<8,40> c_hw_flt_8_40_bits_25;
  const HWFloat<8,40> c_hw_flt_8_40_0_5val;
  const HWFloat<8,40> c_hw_flt_8_40_bits_26;
  const HWFloat<8,40> c_hw_flt_8_40_bits_27;
  const HWFloat<8,40> c_hw_flt_8_40_bits_28;
  const HWFloat<8,40> c_hw_flt_8_40_bits_29;
  const HWFloat<8,40> c_hw_flt_8_40_bits_30;
  const HWFloat<8,40> c_hw_flt_8_40_bits_31;
  const HWFloat<8,40> c_hw_flt_8_40_bits_32;
  const HWFloat<8,40> c_hw_flt_8_40_bits_33;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_2;
  const HWFloat<8,40> c_hw_flt_8_40_bits_34;
  const HWOffsetFix<24,0,UNSIGNED> c_hw_fix_24_0_uns_undef;
  const HWFloat<8,40> c_hw_flt_8_40_n2_0val;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFE48KERNEL_H_ */
