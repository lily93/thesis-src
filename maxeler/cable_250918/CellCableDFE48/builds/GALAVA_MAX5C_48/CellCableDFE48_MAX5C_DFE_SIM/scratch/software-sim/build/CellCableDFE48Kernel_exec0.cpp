#include "stdsimheader.h"

namespace maxcompilersim {

void CellCableDFE48Kernel::execute0() {
  { // Node ID: 43 (NodeConstantRawBits)
  }
  { // Node ID: 936 (NodeConstantRawBits)
  }
  { // Node ID: 774 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id774in_input = id936out_value;

    id774out_output[(getCycle()+1)%2] = id774in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 48 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id48in_enable = id43out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id48in_max = id774out_output[getCycle()%2];

    HWOffsetFix<9,0,UNSIGNED> id48x_1;
    HWOffsetFix<1,0,UNSIGNED> id48x_2;
    HWOffsetFix<1,0,UNSIGNED> id48x_3;
    HWOffsetFix<9,0,UNSIGNED> id48x_4t_1e_1;

    id48out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id48st_count)));
    (id48x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id48st_count),(c_hw_fix_9_0_uns_bits_1)));
    (id48x_2) = (gte_fixed((id48x_1),(cast_fixed2fixed<9,0,UNSIGNED,TRUNCATE>(id48in_max))));
    (id48x_3) = (and_fixed((id48x_2),id48in_enable));
    id48out_wrap = (id48x_3);
    if((id48in_enable.getValueAsBool())) {
      if(((id48x_3).getValueAsBool())) {
        (id48st_count) = (c_hw_fix_9_0_uns_bits);
      }
      else {
        (id48x_4t_1e_1) = (id48x_1);
        (id48st_count) = (id48x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 1009 (NodeConstantRawBits)
  }
  { // Node ID: 654 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id654in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id654in_b = id1009out_value;

    id654out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id654in_a,id654in_b));
  }
  { // Node ID: 703 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id703in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id703in_b = id654out_result[getCycle()%2];

    id703out_result[(getCycle()+1)%2] = (eq_fixed(id703in_a,id703in_b));
  }
  { // Node ID: 656 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id657out_result;

  { // Node ID: 657 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id657in_a = id656out_io_u_out_force_disabled;

    id657out_result = (not_fixed(id657in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id658out_result;

  { // Node ID: 658 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_a = id703out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id658in_b = id657out_result;

    HWOffsetFix<1,0,UNSIGNED> id658x_1;

    (id658x_1) = (and_fixed(id658in_a,id658in_b));
    id658out_result = (id658x_1);
  }
  { // Node ID: 907 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id907in_input = id658out_result;

    id907out_output[(getCycle()+222)%223] = id907in_input;
  }
  { // Node ID: 775 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id775in_input = id48out_wrap;

    id775out_output[(getCycle()+1)%2] = id775in_input;
  }
  { // Node ID: 40 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 47 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id47in_enable = id775out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id47in_max = id40out_nx;

    HWOffsetFix<33,0,UNSIGNED> id47x_1;
    HWOffsetFix<1,0,UNSIGNED> id47x_2;
    HWOffsetFix<1,0,UNSIGNED> id47x_3;
    HWOffsetFix<33,0,UNSIGNED> id47x_4t_1e_1;

    id47out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id47st_count)));
    (id47x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id47st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id47x_2) = (gte_fixed((id47x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id47in_max))));
    (id47x_3) = (and_fixed((id47x_2),id47in_enable));
    id47out_wrap = (id47x_3);
    if((id47in_enable.getValueAsBool())) {
      if(((id47x_3).getValueAsBool())) {
        (id47st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id47x_4t_1e_1) = (id47x_1);
        (id47st_count) = (id47x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 776 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id776in_input = id47out_wrap;

    id776out_output[(getCycle()+1)%2] = id776in_input;
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  { // Node ID: 1008 (NodeConstantRawBits)
  }
  { // Node ID: 45 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id45in_a = id41out_simTime;
    const HWOffsetFix<32,0,UNSIGNED> &id45in_b = id1008out_value;

    id45out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id45in_a,id45in_b));
  }
  if ( (getFillLevel() >= (5l)))
  { // Node ID: 46 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id46in_enable = id776out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id46in_max = id45out_result[getCycle()%2];

    HWOffsetFix<33,0,UNSIGNED> id46x_1;
    HWOffsetFix<1,0,UNSIGNED> id46x_2;
    HWOffsetFix<1,0,UNSIGNED> id46x_3;
    HWOffsetFix<33,0,UNSIGNED> id46x_4t_1e_1;

    id46out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id46st_count)));
    (id46x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id46st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id46x_2) = (gte_fixed((id46x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id46in_max))));
    (id46x_3) = (and_fixed((id46x_2),id46in_enable));
    id46out_wrap = (id46x_3);
    if((id46in_enable.getValueAsBool())) {
      if(((id46x_3).getValueAsBool())) {
        (id46st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id46x_4t_1e_1) = (id46x_1);
        (id46st_count) = (id46x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 1007 (NodeConstantRawBits)
  }
  { // Node ID: 704 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id704in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id704in_b = id1007out_value;

    id704out_result[(getCycle()+1)%2] = (eq_fixed(id704in_a,id704in_b));
  }
  { // Node ID: 893 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id893in_input = id47out_count;

    id893out_output[(getCycle()+21)%22] = id893in_input;
  }
  { // Node ID: 911 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id911in_input = id893out_output[getCycle()%22];

    id911out_output[(getCycle()+1)%2] = id911in_input;
  }
  { // Node ID: 912 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id912in_input = id911out_output[getCycle()%2];

    id912out_output[(getCycle()+170)%171] = id912in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id767out_output;

  { // Node ID: 767 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id767in_input = id912out_output[getCycle()%171];

    id767out_output = id767in_input;
  }
  { // Node ID: 778 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id778in_input = id767out_output;

    id778out_output[(getCycle()+27)%28] = id778in_input;
  }
  HWOffsetFix<24,0,UNSIGNED> id625out_o;

  { // Node ID: 625 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id625in_i = id778out_output[getCycle()%28];

    id625out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id625in_i));
  }
  HWFloat<8,40> id768out_output;

  { // Node ID: 768 (NodeStreamOffset)
    const HWFloat<8,40> &id768in_input = id617out_result[getCycle()%7];

    id768out_output = id768in_input;
  }
  { // Node ID: 1006 (NodeConstantRawBits)
  }
  { // Node ID: 627 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id627in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id627in_b = id1006out_value;

    id627out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id627in_a,id627in_b));
  }
  { // Node ID: 705 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id705in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id705in_b = id627out_result[getCycle()%2];

    id705out_result[(getCycle()+1)%2] = (eq_fixed(id705in_a,id705in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id49out_o;

  { // Node ID: 49 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id49in_i = id47out_count;

    id49out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id49in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 681 (NodeRAM)
    const bool id681_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id681in_addrA = id625out_o;
    const HWFloat<8,40> &id681in_dina = id768out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id681in_wea = id705out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id681in_addrB = id49out_o;

    long id681x_1;
    long id681x_2;
    HWFloat<8,40> id681x_3;

    (id681x_1) = (id681in_addrA.getValueAsLong());
    (id681x_2) = (id681in_addrB.getValueAsLong());
    switch(((long)((id681x_2)<(10000000l)))) {
      case 0l:
        id681x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id681x_3 = (id681sta_ram_store[(id681x_2)]);
        break;
      default:
        id681x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id681out_doutb[(getCycle()+2)%3] = (id681x_3);
    if(((id681in_wea.getValueAsBool())&id681_inputvalid)) {
      if(((id681x_1)<(10000000l))) {
        (id681sta_ram_store[(id681x_1)]) = id681in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_1("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 681) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 60 (NodeConstantRawBits)
  }
  { // Node ID: 61 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id61in_sel = id704out_result[getCycle()%2];
    const HWFloat<8,40> &id61in_option0 = id681out_doutb[getCycle()%3];
    const HWFloat<8,40> &id61in_option1 = id60out_value;

    HWFloat<8,40> id61x_1;

    switch((id61in_sel.getValueAsLong())) {
      case 0l:
        id61x_1 = id61in_option0;
        break;
      case 1l:
        id61x_1 = id61in_option1;
        break;
      default:
        id61x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id61out_result[(getCycle()+1)%2] = (id61x_1);
  }
  { // Node ID: 901 (NodeFIFO)
    const HWFloat<8,40> &id901in_input = id61out_result[getCycle()%2];

    id901out_output[(getCycle()+1)%2] = id901in_input;
  }
  { // Node ID: 913 (NodeFIFO)
    const HWFloat<8,40> &id913in_input = id901out_output[getCycle()%2];

    id913out_output[(getCycle()+5)%6] = id913in_input;
  }
  { // Node ID: 914 (NodeFIFO)
    const HWFloat<8,40> &id914in_input = id913out_output[getCycle()%6];

    id914out_output[(getCycle()+24)%25] = id914in_input;
  }
  { // Node ID: 915 (NodeFIFO)
    const HWFloat<8,40> &id915in_input = id914out_output[getCycle()%25];

    id915out_output[(getCycle()+18)%19] = id915in_input;
  }
  { // Node ID: 916 (NodeFIFO)
    const HWFloat<8,40> &id916in_input = id915out_output[getCycle()%19];

    id916out_output[(getCycle()+23)%24] = id916in_input;
  }
  { // Node ID: 917 (NodeFIFO)
    const HWFloat<8,40> &id917in_input = id916out_output[getCycle()%24];

    id917out_output[(getCycle()+27)%28] = id917in_input;
  }
  { // Node ID: 918 (NodeFIFO)
    const HWFloat<8,40> &id918in_input = id917out_output[getCycle()%28];

    id918out_output[(getCycle()+2)%3] = id918in_input;
  }
  { // Node ID: 919 (NodeFIFO)
    const HWFloat<8,40> &id919in_input = id918out_output[getCycle()%3];

    id919out_output[(getCycle()+30)%31] = id919in_input;
  }
  { // Node ID: 920 (NodeFIFO)
    const HWFloat<8,40> &id920in_input = id919out_output[getCycle()%31];

    id920out_output[(getCycle()+9)%10] = id920in_input;
  }
  { // Node ID: 921 (NodeFIFO)
    const HWFloat<8,40> &id921in_input = id920out_output[getCycle()%10];

    id921out_output[(getCycle()+6)%7] = id921in_input;
  }
  { // Node ID: 922 (NodeFIFO)
    const HWFloat<8,40> &id922in_input = id921out_output[getCycle()%7];

    id922out_output[(getCycle()+9)%10] = id922in_input;
  }
  { // Node ID: 923 (NodeFIFO)
    const HWFloat<8,40> &id923in_input = id922out_output[getCycle()%10];

    id923out_output[(getCycle()+3)%4] = id923in_input;
  }
  { // Node ID: 924 (NodeFIFO)
    const HWFloat<8,40> &id924in_input = id923out_output[getCycle()%4];

    id924out_output[(getCycle()+21)%22] = id924in_input;
  }
  { // Node ID: 925 (NodeFIFO)
    const HWFloat<8,40> &id925in_input = id924out_output[getCycle()%22];

    id925out_output[(getCycle()+6)%7] = id925in_input;
  }
  { // Node ID: 926 (NodeFIFO)
    const HWFloat<8,40> &id926in_input = id925out_output[getCycle()%7];

    id926out_output[(getCycle()+20)%21] = id926in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  { // Node ID: 592 (NodeGt)
    const HWFloat<8,40> &id592in_a = id924out_output[getCycle()%22];
    const HWFloat<8,40> &id592in_b = id15out_value;

    id592out_result[(getCycle()+1)%2] = (gt_float(id592in_a,id592in_b));
  }
  { // Node ID: 34 (NodeConstantRawBits)
  }
  { // Node ID: 783 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id783in_input = id704out_result[getCycle()%2];

    id783out_output[(getCycle()+1)%2] = id783in_input;
  }
  HWOffsetFix<24,0,UNSIGNED> id629out_o;

  { // Node ID: 629 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id629in_i = id778out_output[getCycle()%28];

    id629out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id629in_i));
  }
  HWFloat<8,40> id769out_output;

  { // Node ID: 769 (NodeStreamOffset)
    const HWFloat<8,40> &id769in_input = id781out_output[getCycle()%89];

    id769out_output = id769in_input;
  }
  { // Node ID: 782 (NodeFIFO)
    const HWFloat<8,40> &id782in_input = id769out_output;

    id782out_output[(getCycle()+71)%72] = id782in_input;
  }
  { // Node ID: 1005 (NodeConstantRawBits)
  }
  { // Node ID: 631 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id631in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id631in_b = id1005out_value;

    id631out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id631in_a,id631in_b));
  }
  { // Node ID: 706 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id706in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id706in_b = id631out_result[getCycle()%2];

    id706out_result[(getCycle()+1)%2] = (eq_fixed(id706in_a,id706in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id50out_o;

  { // Node ID: 50 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id50in_i = id47out_count;

    id50out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id50in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 684 (NodeRAM)
    const bool id684_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id684in_addrA = id629out_o;
    const HWFloat<8,40> &id684in_dina = id782out_output[getCycle()%72];
    const HWOffsetFix<1,0,UNSIGNED> &id684in_wea = id706out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id684in_addrB = id50out_o;

    long id684x_1;
    long id684x_2;
    HWFloat<8,40> id684x_3;

    (id684x_1) = (id684in_addrA.getValueAsLong());
    (id684x_2) = (id684in_addrB.getValueAsLong());
    switch(((long)((id684x_2)<(10000000l)))) {
      case 0l:
        id684x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id684x_3 = (id684sta_ram_store[(id684x_2)]);
        break;
      default:
        id684x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id684out_doutb[(getCycle()+2)%3] = (id684x_3);
    if(((id684in_wea.getValueAsBool())&id684_inputvalid)) {
      if(((id684x_1)<(10000000l))) {
        (id684sta_ram_store[(id684x_1)]) = id684in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_2("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 684) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 784 (NodeFIFO)
    const HWFloat<8,40> &id784in_input = id684out_doutb[getCycle()%3];

    id784out_output[(getCycle()+1)%2] = id784in_input;
  }
  { // Node ID: 62 (NodeConstantRawBits)
  }
  { // Node ID: 63 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id63in_sel = id783out_output[getCycle()%2];
    const HWFloat<8,40> &id63in_option0 = id784out_output[getCycle()%2];
    const HWFloat<8,40> &id63in_option1 = id62out_value;

    HWFloat<8,40> id63x_1;

    switch((id63in_sel.getValueAsLong())) {
      case 0l:
        id63x_1 = id63in_option0;
        break;
      case 1l:
        id63x_1 = id63in_option1;
        break;
      default:
        id63x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id63out_result[(getCycle()+1)%2] = (id63x_1);
  }
  { // Node ID: 786 (NodeFIFO)
    const HWFloat<8,40> &id786in_input = id63out_result[getCycle()%2];

    id786out_output[(getCycle()+2)%3] = id786in_input;
  }
  { // Node ID: 927 (NodeFIFO)
    const HWFloat<8,40> &id927in_input = id786out_output[getCycle()%3];

    id927out_output[(getCycle()+6)%7] = id927in_input;
  }
  { // Node ID: 928 (NodeFIFO)
    const HWFloat<8,40> &id928in_input = id927out_output[getCycle()%7];

    id928out_output[(getCycle()+36)%37] = id928in_input;
  }
  { // Node ID: 462 (NodeGt)
    const HWFloat<8,40> &id462in_a = id914out_output[getCycle()%25];
    const HWFloat<8,40> &id462in_b = id15out_value;

    id462out_result[(getCycle()+1)%2] = (gt_float(id462in_a,id462in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  { // Node ID: 451 (NodeGt)
    const HWFloat<8,40> &id451in_a = id61out_result[getCycle()%2];
    const HWFloat<8,40> &id451in_b = id17out_value;

    id451out_result[(getCycle()+1)%2] = (gt_float(id451in_a,id451in_b));
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  { // Node ID: 452 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id452in_sel = id451out_result[getCycle()%2];
    const HWFloat<8,40> &id452in_option0 = id35out_value;
    const HWFloat<8,40> &id452in_option1 = id34out_value;

    HWFloat<8,40> id452x_1;

    switch((id452in_sel.getValueAsLong())) {
      case 0l:
        id452x_1 = id452in_option0;
        break;
      case 1l:
        id452x_1 = id452in_option1;
        break;
      default:
        id452x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id452out_result[(getCycle()+1)%2] = (id452x_1);
  }
  { // Node ID: 460 (NodeRegister)
    const HWFloat<8,40> &id460in_input = id452out_result[getCycle()%2];

    id460out_output[(getCycle()+1)%2] = id460in_input;
  }
  { // Node ID: 465 (NodeSub)
    const HWFloat<8,40> &id465in_a = id460out_output[getCycle()%2];
    const HWFloat<8,40> &id465in_b = id786out_output[getCycle()%3];

    id465out_result[(getCycle()+6)%7] = (sub_float(id465in_a,id465in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  { // Node ID: 226 (NodeGt)
    const HWFloat<8,40> &id226in_a = id913out_output[getCycle()%6];
    const HWFloat<8,40> &id226in_b = id16out_value;

    id226out_result[(getCycle()+1)%2] = (gt_float(id226in_a,id226in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  { // Node ID: 227 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id227in_sel = id226out_result[getCycle()%2];
    const HWFloat<8,40> &id227in_option0 = id1out_value;
    const HWFloat<8,40> &id227in_option1 = id2out_value;

    HWFloat<8,40> id227x_1;

    switch((id227in_sel.getValueAsLong())) {
      case 0l:
        id227x_1 = id227in_option0;
        break;
      case 1l:
        id227x_1 = id227in_option1;
        break;
      default:
        id227x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id227out_result[(getCycle()+1)%2] = (id227x_1);
  }
  { // Node ID: 446 (NodeRegister)
    const HWFloat<8,40> &id446in_input = id227out_result[getCycle()%2];

    id446out_output[(getCycle()+1)%2] = id446in_input;
  }
  { // Node ID: 466 (NodeDiv)
    const HWFloat<8,40> &id466in_a = id465out_result[getCycle()%7];
    const HWFloat<8,40> &id466in_b = id446out_output[getCycle()%2];

    id466out_result[(getCycle()+22)%23] = (div_float(id466in_a,id466in_b));
  }
  HWFloat<8,40> id463out_result;

  { // Node ID: 463 (NodeNeg)
    const HWFloat<8,40> &id463in_a = id927out_output[getCycle()%7];

    id463out_result = (neg_float(id463in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  { // Node ID: 464 (NodeDiv)
    const HWFloat<8,40> &id464in_a = id463out_result;
    const HWFloat<8,40> &id464in_b = id0out_value;

    id464out_result[(getCycle()+22)%23] = (div_float(id464in_a,id464in_b));
  }
  { // Node ID: 467 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id467in_sel = id462out_result[getCycle()%2];
    const HWFloat<8,40> &id467in_option0 = id466out_result[getCycle()%23];
    const HWFloat<8,40> &id467in_option1 = id464out_result[getCycle()%23];

    HWFloat<8,40> id467x_1;

    switch((id467in_sel.getValueAsLong())) {
      case 0l:
        id467x_1 = id467in_option0;
        break;
      case 1l:
        id467x_1 = id467in_option1;
        break;
      default:
        id467x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id467out_result[(getCycle()+1)%2] = (id467x_1);
  }
  { // Node ID: 583 (NodeRegister)
    const HWFloat<8,40> &id583in_input = id467out_result[getCycle()%2];

    id583out_output[(getCycle()+1)%2] = id583in_input;
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeCast)
    const HWFloat<11,53> &id37in_i = id36out_dt;

    id37out_o[(getCycle()+3)%4] = (cast_float2float<8,40>(id37in_i));
  }
  { // Node ID: 586 (NodeMul)
    const HWFloat<8,40> &id586in_a = id583out_output[getCycle()%2];
    const HWFloat<8,40> &id586in_b = id37out_o[getCycle()%4];

    id586out_result[(getCycle()+12)%13] = (mul_float(id586in_a,id586in_b));
  }
  { // Node ID: 587 (NodeAdd)
    const HWFloat<8,40> &id587in_a = id928out_output[getCycle()%37];
    const HWFloat<8,40> &id587in_b = id586out_result[getCycle()%13];

    id587out_result[(getCycle()+12)%13] = (add_float(id587in_a,id587in_b));
  }
  { // Node ID: 781 (NodeFIFO)
    const HWFloat<8,40> &id781in_input = id587out_result[getCycle()%13];

    id781out_output[(getCycle()+88)%89] = id781in_input;
  }
  HWFloat<8,40> id593out_result;

  { // Node ID: 593 (NodeNeg)
    const HWFloat<8,40> &id593in_a = id781out_output[getCycle()%89];

    id593out_result = (neg_float(id593in_a));
  }
  { // Node ID: 594 (NodeSub)
    const HWFloat<8,40> &id594in_a = id920out_output[getCycle()%10];
    const HWFloat<8,40> &id594in_b = id15out_value;

    id594out_result[(getCycle()+6)%7] = (sub_float(id594in_a,id594in_b));
  }
  { // Node ID: 595 (NodeMul)
    const HWFloat<8,40> &id595in_a = id593out_result;
    const HWFloat<8,40> &id595in_b = id594out_result[getCycle()%7];

    id595out_result[(getCycle()+6)%7] = (mul_float(id595in_a,id595in_b));
  }
  { // Node ID: 28 (NodeConstantRawBits)
  }
  { // Node ID: 596 (NodeSub)
    const HWFloat<8,40> &id596in_a = id28out_value;
    const HWFloat<8,40> &id596in_b = id921out_output[getCycle()%7];

    id596out_result[(getCycle()+6)%7] = (sub_float(id596in_a,id596in_b));
  }
  { // Node ID: 597 (NodeMul)
    const HWFloat<8,40> &id597in_a = id595out_result[getCycle()%7];
    const HWFloat<8,40> &id597in_b = id596out_result[getCycle()%7];

    id597out_result[(getCycle()+6)%7] = (mul_float(id597in_a,id597in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 598 (NodeDiv)
    const HWFloat<8,40> &id598in_a = id597out_result[getCycle()%7];
    const HWFloat<8,40> &id598in_b = id8out_value;

    id598out_result[(getCycle()+22)%23] = (div_float(id598in_a,id598in_b));
  }
  { // Node ID: 599 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id599in_sel = id592out_result[getCycle()%2];
    const HWFloat<8,40> &id599in_option0 = id34out_value;
    const HWFloat<8,40> &id599in_option1 = id598out_result[getCycle()%23];

    HWFloat<8,40> id599x_1;

    switch((id599in_sel.getValueAsLong())) {
      case 0l:
        id599x_1 = id599in_option0;
        break;
      case 1l:
        id599x_1 = id599in_option1;
        break;
      default:
        id599x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id599out_result[(getCycle()+1)%2] = (id599x_1);
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  { // Node ID: 600 (NodeGt)
    const HWFloat<8,40> &id600in_a = id924out_output[getCycle()%22];
    const HWFloat<8,40> &id600in_b = id19out_value;

    id600out_result[(getCycle()+1)%2] = (gt_float(id600in_a,id600in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  { // Node ID: 444 (NodeGt)
    const HWFloat<8,40> &id444in_a = id922out_output[getCycle()%10];
    const HWFloat<8,40> &id444in_b = id21out_value;

    id444out_result[(getCycle()+1)%2] = (gt_float(id444in_a,id444in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  { // Node ID: 445 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id445in_sel = id444out_result[getCycle()%2];
    const HWFloat<8,40> &id445in_option0 = id9out_value;
    const HWFloat<8,40> &id445in_option1 = id10out_value;

    HWFloat<8,40> id445x_1;

    switch((id445in_sel.getValueAsLong())) {
      case 0l:
        id445x_1 = id445in_option0;
        break;
      case 1l:
        id445x_1 = id445in_option1;
        break;
      default:
        id445x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id445out_result[(getCycle()+1)%2] = (id445x_1);
  }
  { // Node ID: 450 (NodeRegister)
    const HWFloat<8,40> &id450in_input = id445out_result[getCycle()%2];

    id450out_output[(getCycle()+1)%2] = id450in_input;
  }
  { // Node ID: 604 (NodeDiv)
    const HWFloat<8,40> &id604in_a = id923out_output[getCycle()%4];
    const HWFloat<8,40> &id604in_b = id450out_output[getCycle()%2];

    id604out_result[(getCycle()+22)%23] = (div_float(id604in_a,id604in_b));
  }
  { // Node ID: 1004 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 31 (NodeConstantRawBits)
  }
  { // Node ID: 1003 (NodeConstantRawBits)
  }
  { // Node ID: 1002 (NodeConstantRawBits)
  }
  { // Node ID: 1001 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  { // Node ID: 335 (NodeSub)
    const HWFloat<8,40> &id335in_a = id915out_output[getCycle()%19];
    const HWFloat<8,40> &id335in_b = id29out_value;

    id335out_result[(getCycle()+6)%7] = (sub_float(id335in_a,id335in_b));
  }
  { // Node ID: 336 (NodeMul)
    const HWFloat<8,40> &id336in_a = id24out_value;
    const HWFloat<8,40> &id336in_b = id335out_result[getCycle()%7];

    id336out_result[(getCycle()+6)%7] = (mul_float(id336in_a,id336in_b));
  }
  { // Node ID: 750 (NodePO2FPMult)
    const HWFloat<8,40> &id750in_floatIn = id336out_result[getCycle()%7];

    id750out_floatOut[(getCycle()+1)%2] = (mul_float(id750in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  HWRawBits<8> id415out_result;

  { // Node ID: 415 (NodeSlice)
    const HWFloat<8,40> &id415in_a = id750out_floatOut[getCycle()%2];

    id415out_result = (slice<39,8>(id415in_a));
  }
  { // Node ID: 416 (NodeConstantRawBits)
  }
  { // Node ID: 707 (NodeEqInlined)
    const HWRawBits<8> &id707in_a = id415out_result;
    const HWRawBits<8> &id707in_b = id416out_value;

    id707out_result[(getCycle()+1)%2] = (eq_bits(id707in_a,id707in_b));
  }
  HWRawBits<39> id414out_result;

  { // Node ID: 414 (NodeSlice)
    const HWFloat<8,40> &id414in_a = id750out_floatOut[getCycle()%2];

    id414out_result = (slice<0,39>(id414in_a));
  }
  { // Node ID: 1000 (NodeConstantRawBits)
  }
  { // Node ID: 708 (NodeNeqInlined)
    const HWRawBits<39> &id708in_a = id414out_result;
    const HWRawBits<39> &id708in_b = id1000out_value;

    id708out_result[(getCycle()+1)%2] = (neq_bits(id708in_a,id708in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id420out_result;

  { // Node ID: 420 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id420in_a = id707out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id420in_b = id708out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id420x_1;

    (id420x_1) = (and_fixed(id420in_a,id420in_b));
    id420out_result = (id420x_1);
  }
  { // Node ID: 815 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id815in_input = id420out_result;

    id815out_output[(getCycle()+41)%42] = id815in_input;
  }
  { // Node ID: 339 (NodeConstantRawBits)
  }
  HWFloat<8,40> id340out_output;
  HWOffsetFix<1,0,UNSIGNED> id340out_output_doubt;

  { // Node ID: 340 (NodeDoubtBitOp)
    const HWFloat<8,40> &id340in_input = id750out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id340in_doubt = id339out_value;

    id340out_output = id340in_input;
    id340out_output_doubt = id340in_doubt;
  }
  { // Node ID: 341 (NodeCast)
    const HWFloat<8,40> &id341in_i = id340out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id341in_i_doubt = id340out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id341x_1;

    id341out_o[(getCycle()+6)%7] = (cast_float2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id341in_i,(&(id341x_1))));
    id341out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id341x_1),(c_hw_fix_4_0_uns_bits))),id341in_i_doubt));
  }
  { // Node ID: 344 (NodeConstantRawBits)
  }
  { // Node ID: 343 (NodeMul)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id343in_a = id341out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id343in_a_doubt = id341out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id343in_b = id344out_value;

    HWOffsetFix<1,0,UNSIGNED> id343x_1;

    id343out_result[(getCycle()+6)%7] = (mul_fixed<105,-94,TWOSCOMPLEMENT,TONEAREVEN>(id343in_a,id343in_b,(&(id343x_1))));
    id343out_result_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id343x_1),(c_hw_fix_1_0_uns_bits_1))),id343in_a_doubt));
  }
  { // Node ID: 345 (NodeCast)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id345in_i = id343out_result[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id345in_i_doubt = id343out_result_doubt[getCycle()%7];

    HWOffsetFix<1,0,UNSIGNED> id345x_1;

    id345out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id345in_i,(&(id345x_1))));
    id345out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id345x_1),(c_hw_fix_1_0_uns_bits_1))),id345in_i_doubt));
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id354out_output;

  { // Node ID: 354 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id354in_input = id345out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id354in_input_doubt = id345out_o_doubt[getCycle()%2];

    id354out_output = id354in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id355out_o;

  { // Node ID: 355 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id355in_i = id354out_output;

    id355out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id355in_i));
  }
  { // Node ID: 797 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id797in_input = id355out_o;

    id797out_output[(getCycle()+26)%27] = id797in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id382out_o;

  { // Node ID: 382 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id382in_i = id797out_output[getCycle()%27];

    id382out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id382in_i));
  }
  { // Node ID: 385 (NodeConstantRawBits)
  }
  { // Node ID: 741 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-21,UNSIGNED> id358out_o;

  { // Node ID: 358 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id358in_i = id354out_output;

    id358out_o = (cast_fixed2fixed<10,-21,UNSIGNED,TRUNCATE>(id358in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id430out_output;

  { // Node ID: 430 (NodeReinterpret)
    const HWOffsetFix<10,-21,UNSIGNED> &id430in_input = id358out_o;

    id430out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id430in_input))));
  }
  { // Node ID: 798 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id798in_input = id430out_output;

    id798out_output[(getCycle()+14)%15] = id798in_input;
  }
  { // Node ID: 431 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id431in_addr = id798out_output[getCycle()%15];

    HWOffsetFix<29,-40,UNSIGNED> id431x_1;

    switch(((long)((id431in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id431x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
      case 1l:
        id431x_1 = (id431sta_rom_store[(id431in_addr.getValueAsLong())]);
        break;
      default:
        id431x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
    }
    id431out_dout[(getCycle()+2)%3] = (id431x_1);
  }
  HWOffsetFix<10,-11,UNSIGNED> id357out_o;

  { // Node ID: 357 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id357in_i = id354out_output;

    id357out_o = (cast_fixed2fixed<10,-11,UNSIGNED,TRUNCATE>(id357in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id427out_output;

  { // Node ID: 427 (NodeReinterpret)
    const HWOffsetFix<10,-11,UNSIGNED> &id427in_input = id357out_o;

    id427out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id427in_input))));
  }
  { // Node ID: 799 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id799in_input = id427out_output;

    id799out_output[(getCycle()+6)%7] = id799in_input;
  }
  { // Node ID: 428 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id428in_addr = id799out_output[getCycle()%7];

    HWOffsetFix<39,-40,UNSIGNED> id428x_1;

    switch(((long)((id428in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id428x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
      case 1l:
        id428x_1 = (id428sta_rom_store[(id428in_addr.getValueAsLong())]);
        break;
      default:
        id428x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
    }
    id428out_dout[(getCycle()+2)%3] = (id428x_1);
  }
  HWOffsetFix<1,-1,UNSIGNED> id356out_o;

  { // Node ID: 356 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id356in_i = id354out_output;

    id356out_o = (cast_fixed2fixed<1,-1,UNSIGNED,TRUNCATE>(id356in_i));
  }
  HWOffsetFix<1,0,UNSIGNED> id424out_output;

  { // Node ID: 424 (NodeReinterpret)
    const HWOffsetFix<1,-1,UNSIGNED> &id424in_input = id356out_o;

    id424out_output = (cast_bits2fixed<1,0,UNSIGNED>((cast_fixed2bits(id424in_input))));
  }
  { // Node ID: 800 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id800in_input = id424out_output;

    id800out_output[(getCycle()+1)%2] = id800in_input;
  }
  { // Node ID: 425 (NodeROM)
    const HWOffsetFix<1,0,UNSIGNED> &id425in_addr = id800out_output[getCycle()%2];

    HWOffsetFix<40,-40,UNSIGNED> id425x_1;

    switch(((long)((id425in_addr.getValueAsLong())<(2l)))) {
      case 0l:
        id425x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
      case 1l:
        id425x_1 = (id425sta_rom_store[(id425in_addr.getValueAsLong())]);
        break;
      default:
        id425x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
    }
    id425out_dout[(getCycle()+2)%3] = (id425x_1);
  }
  { // Node ID: 362 (NodeConstantRawBits)
  }
  HWOffsetFix<22,-43,UNSIGNED> id359out_o;

  { // Node ID: 359 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id359in_i = id354out_output;

    id359out_o = (cast_fixed2fixed<22,-43,UNSIGNED,TRUNCATE>(id359in_i));
  }
  { // Node ID: 361 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id361in_a = id362out_value;
    const HWOffsetFix<22,-43,UNSIGNED> &id361in_b = id359out_o;

    id361out_result[(getCycle()+2)%3] = (mul_fixed<43,-64,UNSIGNED,TONEAREVEN>(id361in_a,id361in_b));
  }
  { // Node ID: 363 (NodeCast)
    const HWOffsetFix<43,-64,UNSIGNED> &id363in_i = id361out_result[getCycle()%3];

    id363out_o[(getCycle()+1)%2] = (cast_fixed2fixed<22,-43,UNSIGNED,TONEAREVEN>(id363in_i));
  }
  { // Node ID: 364 (NodeAdd)
    const HWOffsetFix<40,-40,UNSIGNED> &id364in_a = id425out_dout[getCycle()%3];
    const HWOffsetFix<22,-43,UNSIGNED> &id364in_b = id363out_o[getCycle()%2];

    id364out_result[(getCycle()+1)%2] = (add_fixed<44,-43,UNSIGNED,TONEAREVEN>(id364in_a,id364in_b));
  }
  { // Node ID: 801 (NodeFIFO)
    const HWOffsetFix<44,-43,UNSIGNED> &id801in_input = id364out_result[getCycle()%2];

    id801out_output[(getCycle()+1)%2] = id801in_input;
  }
  { // Node ID: 365 (NodeMul)
    const HWOffsetFix<22,-43,UNSIGNED> &id365in_a = id363out_o[getCycle()%2];
    const HWOffsetFix<40,-40,UNSIGNED> &id365in_b = id425out_dout[getCycle()%3];

    id365out_result[(getCycle()+2)%3] = (mul_fixed<62,-83,UNSIGNED,TONEAREVEN>(id365in_a,id365in_b));
  }
  { // Node ID: 366 (NodeAdd)
    const HWOffsetFix<44,-43,UNSIGNED> &id366in_a = id801out_output[getCycle()%2];
    const HWOffsetFix<62,-83,UNSIGNED> &id366in_b = id365out_result[getCycle()%3];

    id366out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id366in_a,id366in_b));
  }
  { // Node ID: 367 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id367in_i = id366out_result[getCycle()%3];

    id367out_o[(getCycle()+1)%2] = (cast_fixed2fixed<44,-43,UNSIGNED,TONEAREVEN>(id367in_i));
  }
  { // Node ID: 368 (NodeAdd)
    const HWOffsetFix<39,-40,UNSIGNED> &id368in_a = id428out_dout[getCycle()%3];
    const HWOffsetFix<44,-43,UNSIGNED> &id368in_b = id367out_o[getCycle()%2];

    id368out_result[(getCycle()+1)%2] = (add_fixed<45,-43,UNSIGNED,TONEAREVEN>(id368in_a,id368in_b));
  }
  { // Node ID: 802 (NodeFIFO)
    const HWOffsetFix<45,-43,UNSIGNED> &id802in_input = id368out_result[getCycle()%2];

    id802out_output[(getCycle()+4)%5] = id802in_input;
  }
  { // Node ID: 369 (NodeMul)
    const HWOffsetFix<44,-43,UNSIGNED> &id369in_a = id367out_o[getCycle()%2];
    const HWOffsetFix<39,-40,UNSIGNED> &id369in_b = id428out_dout[getCycle()%3];

    id369out_result[(getCycle()+5)%6] = (mul_fixed<64,-64,UNSIGNED,TONEAREVEN>(id369in_a,id369in_b));
  }
  { // Node ID: 370 (NodeAdd)
    const HWOffsetFix<45,-43,UNSIGNED> &id370in_a = id802out_output[getCycle()%5];
    const HWOffsetFix<64,-64,UNSIGNED> &id370in_b = id369out_result[getCycle()%6];

    id370out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id370in_a,id370in_b));
  }
  { // Node ID: 371 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id371in_i = id370out_result[getCycle()%3];

    id371out_o[(getCycle()+1)%2] = (cast_fixed2fixed<45,-43,UNSIGNED,TONEAREVEN>(id371in_i));
  }
  { // Node ID: 372 (NodeAdd)
    const HWOffsetFix<29,-40,UNSIGNED> &id372in_a = id431out_dout[getCycle()%3];
    const HWOffsetFix<45,-43,UNSIGNED> &id372in_b = id371out_o[getCycle()%2];

    id372out_result[(getCycle()+1)%2] = (add_fixed<46,-43,UNSIGNED,TONEAREVEN>(id372in_a,id372in_b));
  }
  { // Node ID: 803 (NodeFIFO)
    const HWOffsetFix<46,-43,UNSIGNED> &id803in_input = id372out_result[getCycle()%2];

    id803out_output[(getCycle()+4)%5] = id803in_input;
  }
  { // Node ID: 373 (NodeMul)
    const HWOffsetFix<45,-43,UNSIGNED> &id373in_a = id371out_o[getCycle()%2];
    const HWOffsetFix<29,-40,UNSIGNED> &id373in_b = id431out_dout[getCycle()%3];

    id373out_result[(getCycle()+5)%6] = (mul_fixed<64,-73,UNSIGNED,TONEAREVEN>(id373in_a,id373in_b));
  }
  { // Node ID: 374 (NodeAdd)
    const HWOffsetFix<46,-43,UNSIGNED> &id374in_a = id803out_output[getCycle()%5];
    const HWOffsetFix<64,-73,UNSIGNED> &id374in_b = id373out_result[getCycle()%6];

    id374out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id374in_a,id374in_b));
  }
  { // Node ID: 375 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id375in_i = id374out_result[getCycle()%3];

    id375out_o[(getCycle()+1)%2] = (cast_fixed2fixed<46,-43,UNSIGNED,TONEAREVEN>(id375in_i));
  }
  { // Node ID: 376 (NodeCast)
    const HWOffsetFix<46,-43,UNSIGNED> &id376in_i = id375out_o[getCycle()%2];

    id376out_o[(getCycle()+1)%2] = (cast_fixed2fixed<40,-39,UNSIGNED,TONEAREVEN>(id376in_i));
  }
  { // Node ID: 999 (NodeConstantRawBits)
  }
  { // Node ID: 709 (NodeGteInlined)
    const HWOffsetFix<40,-39,UNSIGNED> &id709in_a = id376out_o[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id709in_b = id999out_value;

    id709out_result[(getCycle()+1)%2] = (gte_fixed(id709in_a,id709in_b));
  }
  { // Node ID: 749 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id749in_a = id382out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id749in_b = id385out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id749in_c = id741out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id749in_condb = id709out_result[getCycle()%2];

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id749x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id749x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id749x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id749x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id749x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id749x_1 = id749in_a;
        break;
      default:
        id749x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id749in_condb.getValueAsLong())) {
      case 0l:
        id749x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id749x_2 = id749in_b;
        break;
      default:
        id749x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id749x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id749x_3 = id749in_c;
        break;
      default:
        id749x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id749x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id749x_1),(id749x_2))),(id749x_3)));
    id749out_result[(getCycle()+1)%2] = (id749x_4);
  }
  HWRawBits<1> id710out_result;

  { // Node ID: 710 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id710in_a = id749out_result[getCycle()%2];

    id710out_result = (slice<10,1>(id710in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id711out_output;

  { // Node ID: 711 (NodeReinterpret)
    const HWRawBits<1> &id711in_input = id710out_result;

    id711out_output = (cast_bits2fixed<1,0,UNSIGNED>(id711in_input));
  }
  { // Node ID: 998 (NodeConstantRawBits)
  }
  { // Node ID: 347 (NodeGt)
    const HWFloat<8,40> &id347in_a = id750out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id347in_b = id998out_value;

    id347out_result[(getCycle()+1)%2] = (gt_float(id347in_a,id347in_b));
  }
  { // Node ID: 804 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id804in_input = id347out_result[getCycle()%2];

    id804out_output[(getCycle()+12)%13] = id804in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id348out_output;

  { // Node ID: 348 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id348in_input = id345out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id348in_input_doubt = id345out_o_doubt[getCycle()%2];

    id348out_output = id348in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id349out_result;

  { // Node ID: 349 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id349in_a = id804out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id349in_b = id348out_output;

    HWOffsetFix<1,0,UNSIGNED> id349x_1;

    (id349x_1) = (and_fixed(id349in_a,id349in_b));
    id349out_result = (id349x_1);
  }
  { // Node ID: 805 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id805in_input = id349out_result;

    id805out_output[(getCycle()+27)%28] = id805in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id391out_result;

  { // Node ID: 391 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id391in_a = id805out_output[getCycle()%28];

    id391out_result = (not_fixed(id391in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id392out_result;

  { // Node ID: 392 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id392in_a = id711out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id392in_b = id391out_result;

    HWOffsetFix<1,0,UNSIGNED> id392x_1;

    (id392x_1) = (and_fixed(id392in_a,id392in_b));
    id392out_result = (id392x_1);
  }
  { // Node ID: 997 (NodeConstantRawBits)
  }
  { // Node ID: 351 (NodeLt)
    const HWFloat<8,40> &id351in_a = id750out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id351in_b = id997out_value;

    id351out_result[(getCycle()+1)%2] = (lt_float(id351in_a,id351in_b));
  }
  { // Node ID: 806 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id806in_input = id351out_result[getCycle()%2];

    id806out_output[(getCycle()+12)%13] = id806in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id352out_output;

  { // Node ID: 352 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id352in_input = id345out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id352in_input_doubt = id345out_o_doubt[getCycle()%2];

    id352out_output = id352in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id353in_a = id806out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id353in_b = id352out_output;

    HWOffsetFix<1,0,UNSIGNED> id353x_1;

    (id353x_1) = (and_fixed(id353in_a,id353in_b));
    id353out_result = (id353x_1);
  }
  { // Node ID: 807 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id807in_input = id353out_result;

    id807out_output[(getCycle()+27)%28] = id807in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id393out_result;

  { // Node ID: 393 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id393in_a = id392out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id393in_b = id807out_output[getCycle()%28];

    HWOffsetFix<1,0,UNSIGNED> id393x_1;

    (id393x_1) = (or_fixed(id393in_a,id393in_b));
    id393out_result = (id393x_1);
  }
  { // Node ID: 811 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id811in_input = id393out_result;

    id811out_output[(getCycle()+1)%2] = id811in_input;
  }
  { // Node ID: 996 (NodeConstantRawBits)
  }
  { // Node ID: 712 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id712in_a = id749out_result[getCycle()%2];
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id712in_b = id996out_value;

    id712out_result[(getCycle()+1)%2] = (gte_fixed(id712in_a,id712in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id400out_result;

  { // Node ID: 400 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id807out_output[getCycle()%28];

    id400out_result = (not_fixed(id400in_a));
  }
  { // Node ID: 809 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id809in_input = id400out_result;

    id809out_output[(getCycle()+1)%2] = id809in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_a = id712out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id401in_b = id809out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id401x_1;

    (id401x_1) = (and_fixed(id401in_a,id401in_b));
    id401out_result = (id401x_1);
  }
  { // Node ID: 929 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id929in_input = id805out_output[getCycle()%28];

    id929out_output[(getCycle()+1)%2] = id929in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id402out_result;

  { // Node ID: 402 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_a = id401out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id402in_b = id929out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id402x_1;

    (id402x_1) = (or_fixed(id402in_a,id402in_b));
    id402out_result = (id402x_1);
  }
  HWRawBits<2> id403out_result;

  { // Node ID: 403 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_in0 = id811out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id403in_in1 = id402out_result;

    id403out_result = (cat(id403in_in0,id403in_in1));
  }
  { // Node ID: 395 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id394out_o;

  { // Node ID: 394 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id394in_i = id749out_result[getCycle()%2];

    id394out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id394in_i));
  }
  { // Node ID: 813 (NodeFIFO)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id813in_input = id394out_o;

    id813out_output[(getCycle()+1)%2] = id813in_input;
  }
  { // Node ID: 812 (NodeFIFO)
    const HWOffsetFix<40,-39,UNSIGNED> &id812in_input = id376out_o[getCycle()%2];

    id812out_output[(getCycle()+1)%2] = id812in_input;
  }
  { // Node ID: 379 (NodeConstantRawBits)
  }
  { // Node ID: 380 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id380in_sel = id709out_result[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id380in_option0 = id812out_output[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id380in_option1 = id379out_value;

    HWOffsetFix<40,-39,UNSIGNED> id380x_1;

    switch((id380in_sel.getValueAsLong())) {
      case 0l:
        id380x_1 = id380in_option0;
        break;
      case 1l:
        id380x_1 = id380in_option1;
        break;
      default:
        id380x_1 = (c_hw_fix_40_n39_uns_undef);
        break;
    }
    id380out_result[(getCycle()+1)%2] = (id380x_1);
  }
  HWOffsetFix<39,-39,UNSIGNED> id381out_o;

  { // Node ID: 381 (NodeCast)
    const HWOffsetFix<40,-39,UNSIGNED> &id381in_i = id380out_result[getCycle()%2];

    id381out_o = (cast_fixed2fixed<39,-39,UNSIGNED,TONEAREVEN>(id381in_i));
  }
  { // Node ID: 814 (NodeFIFO)
    const HWOffsetFix<39,-39,UNSIGNED> &id814in_input = id381out_o;

    id814out_output[(getCycle()+1)%2] = id814in_input;
  }
  HWRawBits<48> id396out_result;

  { // Node ID: 396 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id396in_in0 = id395out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id396in_in1 = id813out_output[getCycle()%2];
    const HWOffsetFix<39,-39,UNSIGNED> &id396in_in2 = id814out_output[getCycle()%2];

    id396out_result = (cat((cat(id396in_in0,id396in_in1)),id396in_in2));
  }
  HWFloat<8,40> id397out_output;

  { // Node ID: 397 (NodeReinterpret)
    const HWRawBits<48> &id397in_input = id396out_result;

    id397out_output = (cast_bits2float<8,40>(id397in_input));
  }
  { // Node ID: 404 (NodeConstantRawBits)
  }
  { // Node ID: 405 (NodeConstantRawBits)
  }
  { // Node ID: 407 (NodeConstantRawBits)
  }
  HWRawBits<48> id713out_result;

  { // Node ID: 713 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id713in_in0 = id404out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id713in_in1 = id405out_value;
    const HWOffsetFix<39,0,UNSIGNED> &id713in_in2 = id407out_value;

    id713out_result = (cat((cat(id713in_in0,id713in_in1)),id713in_in2));
  }
  HWFloat<8,40> id409out_output;

  { // Node ID: 409 (NodeReinterpret)
    const HWRawBits<48> &id409in_input = id713out_result;

    id409out_output = (cast_bits2float<8,40>(id409in_input));
  }
  { // Node ID: 700 (NodeConstantRawBits)
  }
  { // Node ID: 412 (NodeMux)
    const HWRawBits<2> &id412in_sel = id403out_result;
    const HWFloat<8,40> &id412in_option0 = id397out_output;
    const HWFloat<8,40> &id412in_option1 = id409out_output;
    const HWFloat<8,40> &id412in_option2 = id700out_value;
    const HWFloat<8,40> &id412in_option3 = id409out_output;

    HWFloat<8,40> id412x_1;

    switch((id412in_sel.getValueAsLong())) {
      case 0l:
        id412x_1 = id412in_option0;
        break;
      case 1l:
        id412x_1 = id412in_option1;
        break;
      case 2l:
        id412x_1 = id412in_option2;
        break;
      case 3l:
        id412x_1 = id412in_option3;
        break;
      default:
        id412x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id412out_result[(getCycle()+1)%2] = (id412x_1);
  }
  { // Node ID: 995 (NodeConstantRawBits)
  }
  { // Node ID: 422 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id422in_sel = id815out_output[getCycle()%42];
    const HWFloat<8,40> &id422in_option0 = id412out_result[getCycle()%2];
    const HWFloat<8,40> &id422in_option1 = id995out_value;

    HWFloat<8,40> id422x_1;

    switch((id422in_sel.getValueAsLong())) {
      case 0l:
        id422x_1 = id422in_option0;
        break;
      case 1l:
        id422x_1 = id422in_option1;
        break;
      default:
        id422x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id422out_result[(getCycle()+1)%2] = (id422x_1);
  }
  { // Node ID: 994 (NodeConstantRawBits)
  }
  { // Node ID: 433 (NodeAdd)
    const HWFloat<8,40> &id433in_a = id422out_result[getCycle()%2];
    const HWFloat<8,40> &id433in_b = id994out_value;

    id433out_result[(getCycle()+6)%7] = (add_float(id433in_a,id433in_b));
  }
  { // Node ID: 435 (NodeDiv)
    const HWFloat<8,40> &id435in_a = id1001out_value;
    const HWFloat<8,40> &id435in_b = id433out_result[getCycle()%7];

    id435out_result[(getCycle()+22)%23] = (div_float(id435in_a,id435in_b));
  }
  { // Node ID: 437 (NodeSub)
    const HWFloat<8,40> &id437in_a = id1002out_value;
    const HWFloat<8,40> &id437in_b = id435out_result[getCycle()%23];

    id437out_result[(getCycle()+6)%7] = (sub_float(id437in_a,id437in_b));
  }
  { // Node ID: 439 (NodeAdd)
    const HWFloat<8,40> &id439in_a = id1003out_value;
    const HWFloat<8,40> &id439in_b = id437out_result[getCycle()%7];

    id439out_result[(getCycle()+6)%7] = (add_float(id439in_a,id439in_b));
  }
  { // Node ID: 440 (NodeMul)
    const HWFloat<8,40> &id440in_a = id31out_value;
    const HWFloat<8,40> &id440in_b = id439out_result[getCycle()%7];

    id440out_result[(getCycle()+6)%7] = (mul_float(id440in_a,id440in_b));
  }
  { // Node ID: 441 (NodeAdd)
    const HWFloat<8,40> &id441in_a = id11out_value;
    const HWFloat<8,40> &id441in_b = id440out_result[getCycle()%7];

    id441out_result[(getCycle()+6)%7] = (add_float(id441in_a,id441in_b));
  }
  { // Node ID: 448 (NodeRegister)
    const HWFloat<8,40> &id448in_input = id441out_result[getCycle()%7];

    id448out_output[(getCycle()+1)%2] = id448in_input;
  }
  { // Node ID: 602 (NodeDiv)
    const HWFloat<8,40> &id602in_a = id1004out_value;
    const HWFloat<8,40> &id602in_b = id448out_output[getCycle()%2];

    id602out_result[(getCycle()+22)%23] = (div_float(id602in_a,id602in_b));
  }
  { // Node ID: 605 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id605in_sel = id600out_result[getCycle()%2];
    const HWFloat<8,40> &id605in_option0 = id604out_result[getCycle()%23];
    const HWFloat<8,40> &id605in_option1 = id602out_result[getCycle()%23];

    HWFloat<8,40> id605x_1;

    switch((id605in_sel.getValueAsLong())) {
      case 0l:
        id605x_1 = id605in_option0;
        break;
      case 1l:
        id605x_1 = id605in_option1;
        break;
      default:
        id605x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id605out_result[(getCycle()+1)%2] = (id605x_1);
  }
  { // Node ID: 612 (NodeAdd)
    const HWFloat<8,40> &id612in_a = id599out_result[getCycle()%2];
    const HWFloat<8,40> &id612in_b = id605out_result[getCycle()%2];

    id612out_result[(getCycle()+6)%7] = (add_float(id612in_a,id612in_b));
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  { // Node ID: 606 (NodeGt)
    const HWFloat<8,40> &id606in_a = id925out_output[getCycle()%7];
    const HWFloat<8,40> &id606in_b = id20out_value;

    id606out_result[(getCycle()+1)%2] = (gt_float(id606in_a,id606in_b));
  }
  { // Node ID: 610 (NodeConstantRawBits)
  }
  HWOffsetFix<24,0,UNSIGNED> id633out_o;

  { // Node ID: 633 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id633in_i = id778out_output[getCycle()%28];

    id633out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id633in_i));
  }
  HWFloat<8,40> id770out_output;

  { // Node ID: 770 (NodeStreamOffset)
    const HWFloat<8,40> &id770in_input = id589out_result[getCycle()%13];

    id770out_output = id770in_input;
  }
  { // Node ID: 818 (NodeFIFO)
    const HWFloat<8,40> &id818in_input = id770out_output;

    id818out_output[(getCycle()+59)%60] = id818in_input;
  }
  { // Node ID: 993 (NodeConstantRawBits)
  }
  { // Node ID: 635 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id635in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id635in_b = id993out_value;

    id635out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id635in_a,id635in_b));
  }
  { // Node ID: 714 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id714in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id714in_b = id635out_result[getCycle()%2];

    id714out_result[(getCycle()+1)%2] = (eq_fixed(id714in_a,id714in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id51out_o;

  { // Node ID: 51 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id51in_i = id47out_count;

    id51out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id51in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 683 (NodeRAM)
    const bool id683_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id683in_addrA = id633out_o;
    const HWFloat<8,40> &id683in_dina = id818out_output[getCycle()%60];
    const HWOffsetFix<1,0,UNSIGNED> &id683in_wea = id714out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id683in_addrB = id51out_o;

    long id683x_1;
    long id683x_2;
    HWFloat<8,40> id683x_3;

    (id683x_1) = (id683in_addrA.getValueAsLong());
    (id683x_2) = (id683in_addrB.getValueAsLong());
    switch(((long)((id683x_2)<(10000000l)))) {
      case 0l:
        id683x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id683x_3 = (id683sta_ram_store[(id683x_2)]);
        break;
      default:
        id683x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id683out_doutb[(getCycle()+2)%3] = (id683x_3);
    if(((id683in_wea.getValueAsBool())&id683_inputvalid)) {
      if(((id683x_1)<(10000000l))) {
        (id683sta_ram_store[(id683x_1)]) = id683in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_3("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 683) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 820 (NodeFIFO)
    const HWFloat<8,40> &id820in_input = id683out_doutb[getCycle()%3];

    id820out_output[(getCycle()+1)%2] = id820in_input;
  }
  { // Node ID: 64 (NodeConstantRawBits)
  }
  { // Node ID: 65 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id65in_sel = id783out_output[getCycle()%2];
    const HWFloat<8,40> &id65in_option0 = id820out_output[getCycle()%2];
    const HWFloat<8,40> &id65in_option1 = id64out_value;

    HWFloat<8,40> id65x_1;

    switch((id65in_sel.getValueAsLong())) {
      case 0l:
        id65x_1 = id65in_option0;
        break;
      case 1l:
        id65x_1 = id65in_option1;
        break;
      default:
        id65x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id65out_result[(getCycle()+1)%2] = (id65x_1);
  }
  { // Node ID: 825 (NodeFIFO)
    const HWFloat<8,40> &id825in_input = id65out_result[getCycle()%2];

    id825out_output[(getCycle()+102)%103] = id825in_input;
  }
  { // Node ID: 930 (NodeFIFO)
    const HWFloat<8,40> &id930in_input = id825out_output[getCycle()%103];

    id930out_output[(getCycle()+6)%7] = id930in_input;
  }
  { // Node ID: 931 (NodeFIFO)
    const HWFloat<8,40> &id931in_input = id930out_output[getCycle()%7];

    id931out_output[(getCycle()+36)%37] = id931in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  { // Node ID: 468 (NodeGt)
    const HWFloat<8,40> &id468in_a = id919out_output[getCycle()%31];
    const HWFloat<8,40> &id468in_b = id18out_value;

    id468out_result[(getCycle()+1)%2] = (gt_float(id468in_a,id468in_b));
  }
  { // Node ID: 33 (NodeConstantRawBits)
  }
  { // Node ID: 453 (NodeGt)
    const HWFloat<8,40> &id453in_a = id917out_output[getCycle()%28];
    const HWFloat<8,40> &id453in_b = id33out_value;

    id453out_result[(getCycle()+1)%2] = (gt_float(id453in_a,id453in_b));
  }
  { // Node ID: 992 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 454 (NodeDiv)
    const HWFloat<8,40> &id454in_a = id916out_output[getCycle()%24];
    const HWFloat<8,40> &id454in_b = id14out_value;

    id454out_result[(getCycle()+22)%23] = (div_float(id454in_a,id454in_b));
  }
  { // Node ID: 456 (NodeSub)
    const HWFloat<8,40> &id456in_a = id992out_value;
    const HWFloat<8,40> &id456in_b = id454out_result[getCycle()%23];

    id456out_result[(getCycle()+6)%7] = (sub_float(id456in_a,id456in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  { // Node ID: 457 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id457in_sel = id453out_result[getCycle()%2];
    const HWFloat<8,40> &id457in_option0 = id456out_result[getCycle()%7];
    const HWFloat<8,40> &id457in_option1 = id32out_value;

    HWFloat<8,40> id457x_1;

    switch((id457in_sel.getValueAsLong())) {
      case 0l:
        id457x_1 = id457in_option0;
        break;
      case 1l:
        id457x_1 = id457in_option1;
        break;
      default:
        id457x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id457out_result[(getCycle()+1)%2] = (id457x_1);
  }
  { // Node ID: 458 (NodeGt)
    const HWFloat<8,40> &id458in_a = id457out_result[getCycle()%2];
    const HWFloat<8,40> &id458in_b = id35out_value;

    id458out_result[(getCycle()+1)%2] = (gt_float(id458in_a,id458in_b));
  }
  { // Node ID: 824 (NodeFIFO)
    const HWFloat<8,40> &id824in_input = id457out_result[getCycle()%2];

    id824out_output[(getCycle()+1)%2] = id824in_input;
  }
  { // Node ID: 459 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id459in_sel = id458out_result[getCycle()%2];
    const HWFloat<8,40> &id459in_option0 = id824out_output[getCycle()%2];
    const HWFloat<8,40> &id459in_option1 = id35out_value;

    HWFloat<8,40> id459x_1;

    switch((id459in_sel.getValueAsLong())) {
      case 0l:
        id459x_1 = id459in_option0;
        break;
      case 1l:
        id459x_1 = id459in_option1;
        break;
      default:
        id459x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id459out_result[(getCycle()+1)%2] = (id459x_1);
  }
  { // Node ID: 461 (NodeRegister)
    const HWFloat<8,40> &id461in_input = id459out_result[getCycle()%2];

    id461out_output[(getCycle()+1)%2] = id461in_input;
  }
  { // Node ID: 471 (NodeSub)
    const HWFloat<8,40> &id471in_a = id461out_output[getCycle()%2];
    const HWFloat<8,40> &id471in_b = id825out_output[getCycle()%103];

    id471out_result[(getCycle()+6)%7] = (sub_float(id471in_a,id471in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  { // Node ID: 991 (NodeConstantRawBits)
  }
  { // Node ID: 990 (NodeConstantRawBits)
  }
  { // Node ID: 989 (NodeConstantRawBits)
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  { // Node ID: 228 (NodeSub)
    const HWFloat<8,40> &id228in_a = id61out_result[getCycle()%2];
    const HWFloat<8,40> &id228in_b = id25out_value;

    id228out_result[(getCycle()+6)%7] = (sub_float(id228in_a,id228in_b));
  }
  { // Node ID: 229 (NodeMul)
    const HWFloat<8,40> &id229in_a = id22out_value;
    const HWFloat<8,40> &id229in_b = id228out_result[getCycle()%7];

    id229out_result[(getCycle()+6)%7] = (mul_float(id229in_a,id229in_b));
  }
  { // Node ID: 751 (NodePO2FPMult)
    const HWFloat<8,40> &id751in_floatIn = id229out_result[getCycle()%7];

    id751out_floatOut[(getCycle()+1)%2] = (mul_float(id751in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  HWRawBits<8> id308out_result;

  { // Node ID: 308 (NodeSlice)
    const HWFloat<8,40> &id308in_a = id751out_floatOut[getCycle()%2];

    id308out_result = (slice<39,8>(id308in_a));
  }
  { // Node ID: 309 (NodeConstantRawBits)
  }
  { // Node ID: 715 (NodeEqInlined)
    const HWRawBits<8> &id715in_a = id308out_result;
    const HWRawBits<8> &id715in_b = id309out_value;

    id715out_result[(getCycle()+1)%2] = (eq_bits(id715in_a,id715in_b));
  }
  HWRawBits<39> id307out_result;

  { // Node ID: 307 (NodeSlice)
    const HWFloat<8,40> &id307in_a = id751out_floatOut[getCycle()%2];

    id307out_result = (slice<0,39>(id307in_a));
  }
  { // Node ID: 988 (NodeConstantRawBits)
  }
  { // Node ID: 716 (NodeNeqInlined)
    const HWRawBits<39> &id716in_a = id307out_result;
    const HWRawBits<39> &id716in_b = id988out_value;

    id716out_result[(getCycle()+1)%2] = (neq_bits(id716in_a,id716in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_a = id715out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id313in_b = id716out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id313x_1;

    (id313x_1) = (and_fixed(id313in_a,id313in_b));
    id313out_result = (id313x_1);
  }
  { // Node ID: 844 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id844in_input = id313out_result;

    id844out_output[(getCycle()+41)%42] = id844in_input;
  }
  { // Node ID: 232 (NodeConstantRawBits)
  }
  HWFloat<8,40> id233out_output;
  HWOffsetFix<1,0,UNSIGNED> id233out_output_doubt;

  { // Node ID: 233 (NodeDoubtBitOp)
    const HWFloat<8,40> &id233in_input = id751out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id233in_doubt = id232out_value;

    id233out_output = id233in_input;
    id233out_output_doubt = id233in_doubt;
  }
  { // Node ID: 234 (NodeCast)
    const HWFloat<8,40> &id234in_i = id233out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id234in_i_doubt = id233out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id234x_1;

    id234out_o[(getCycle()+6)%7] = (cast_float2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id234in_i,(&(id234x_1))));
    id234out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id234x_1),(c_hw_fix_4_0_uns_bits))),id234in_i_doubt));
  }
  { // Node ID: 237 (NodeConstantRawBits)
  }
  { // Node ID: 236 (NodeMul)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id236in_a = id234out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id236in_a_doubt = id234out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id236in_b = id237out_value;

    HWOffsetFix<1,0,UNSIGNED> id236x_1;

    id236out_result[(getCycle()+6)%7] = (mul_fixed<105,-94,TWOSCOMPLEMENT,TONEAREVEN>(id236in_a,id236in_b,(&(id236x_1))));
    id236out_result_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id236x_1),(c_hw_fix_1_0_uns_bits_1))),id236in_a_doubt));
  }
  { // Node ID: 238 (NodeCast)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id238in_i = id236out_result[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id238in_i_doubt = id236out_result_doubt[getCycle()%7];

    HWOffsetFix<1,0,UNSIGNED> id238x_1;

    id238out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id238in_i,(&(id238x_1))));
    id238out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id238x_1),(c_hw_fix_1_0_uns_bits_1))),id238in_i_doubt));
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id247out_output;

  { // Node ID: 247 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id247in_input = id238out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id247in_input_doubt = id238out_o_doubt[getCycle()%2];

    id247out_output = id247in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id248out_o;

  { // Node ID: 248 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id248in_i = id247out_output;

    id248out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id248in_i));
  }
  { // Node ID: 826 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id826in_input = id248out_o;

    id826out_output[(getCycle()+26)%27] = id826in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id275out_o;

  { // Node ID: 275 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id275in_i = id826out_output[getCycle()%27];

    id275out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id275in_i));
  }
  { // Node ID: 278 (NodeConstantRawBits)
  }
  { // Node ID: 743 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-21,UNSIGNED> id251out_o;

  { // Node ID: 251 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id251in_i = id247out_output;

    id251out_o = (cast_fixed2fixed<10,-21,UNSIGNED,TRUNCATE>(id251in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id323out_output;

  { // Node ID: 323 (NodeReinterpret)
    const HWOffsetFix<10,-21,UNSIGNED> &id323in_input = id251out_o;

    id323out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id323in_input))));
  }
  { // Node ID: 827 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id827in_input = id323out_output;

    id827out_output[(getCycle()+14)%15] = id827in_input;
  }
  { // Node ID: 324 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id324in_addr = id827out_output[getCycle()%15];

    HWOffsetFix<29,-40,UNSIGNED> id324x_1;

    switch(((long)((id324in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id324x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
      case 1l:
        id324x_1 = (id324sta_rom_store[(id324in_addr.getValueAsLong())]);
        break;
      default:
        id324x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
    }
    id324out_dout[(getCycle()+2)%3] = (id324x_1);
  }
  HWOffsetFix<10,-11,UNSIGNED> id250out_o;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id250in_i = id247out_output;

    id250out_o = (cast_fixed2fixed<10,-11,UNSIGNED,TRUNCATE>(id250in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id320out_output;

  { // Node ID: 320 (NodeReinterpret)
    const HWOffsetFix<10,-11,UNSIGNED> &id320in_input = id250out_o;

    id320out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id320in_input))));
  }
  { // Node ID: 828 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id828in_input = id320out_output;

    id828out_output[(getCycle()+6)%7] = id828in_input;
  }
  { // Node ID: 321 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id321in_addr = id828out_output[getCycle()%7];

    HWOffsetFix<39,-40,UNSIGNED> id321x_1;

    switch(((long)((id321in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id321x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
      case 1l:
        id321x_1 = (id321sta_rom_store[(id321in_addr.getValueAsLong())]);
        break;
      default:
        id321x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
    }
    id321out_dout[(getCycle()+2)%3] = (id321x_1);
  }
  HWOffsetFix<1,-1,UNSIGNED> id249out_o;

  { // Node ID: 249 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id249in_i = id247out_output;

    id249out_o = (cast_fixed2fixed<1,-1,UNSIGNED,TRUNCATE>(id249in_i));
  }
  HWOffsetFix<1,0,UNSIGNED> id317out_output;

  { // Node ID: 317 (NodeReinterpret)
    const HWOffsetFix<1,-1,UNSIGNED> &id317in_input = id249out_o;

    id317out_output = (cast_bits2fixed<1,0,UNSIGNED>((cast_fixed2bits(id317in_input))));
  }
  { // Node ID: 829 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id829in_input = id317out_output;

    id829out_output[(getCycle()+1)%2] = id829in_input;
  }
  { // Node ID: 318 (NodeROM)
    const HWOffsetFix<1,0,UNSIGNED> &id318in_addr = id829out_output[getCycle()%2];

    HWOffsetFix<40,-40,UNSIGNED> id318x_1;

    switch(((long)((id318in_addr.getValueAsLong())<(2l)))) {
      case 0l:
        id318x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
      case 1l:
        id318x_1 = (id318sta_rom_store[(id318in_addr.getValueAsLong())]);
        break;
      default:
        id318x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
    }
    id318out_dout[(getCycle()+2)%3] = (id318x_1);
  }
  { // Node ID: 255 (NodeConstantRawBits)
  }
  HWOffsetFix<22,-43,UNSIGNED> id252out_o;

  { // Node ID: 252 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id252in_i = id247out_output;

    id252out_o = (cast_fixed2fixed<22,-43,UNSIGNED,TRUNCATE>(id252in_i));
  }
  { // Node ID: 254 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id254in_a = id255out_value;
    const HWOffsetFix<22,-43,UNSIGNED> &id254in_b = id252out_o;

    id254out_result[(getCycle()+2)%3] = (mul_fixed<43,-64,UNSIGNED,TONEAREVEN>(id254in_a,id254in_b));
  }
  { // Node ID: 256 (NodeCast)
    const HWOffsetFix<43,-64,UNSIGNED> &id256in_i = id254out_result[getCycle()%3];

    id256out_o[(getCycle()+1)%2] = (cast_fixed2fixed<22,-43,UNSIGNED,TONEAREVEN>(id256in_i));
  }
  { // Node ID: 257 (NodeAdd)
    const HWOffsetFix<40,-40,UNSIGNED> &id257in_a = id318out_dout[getCycle()%3];
    const HWOffsetFix<22,-43,UNSIGNED> &id257in_b = id256out_o[getCycle()%2];

    id257out_result[(getCycle()+1)%2] = (add_fixed<44,-43,UNSIGNED,TONEAREVEN>(id257in_a,id257in_b));
  }
  { // Node ID: 830 (NodeFIFO)
    const HWOffsetFix<44,-43,UNSIGNED> &id830in_input = id257out_result[getCycle()%2];

    id830out_output[(getCycle()+1)%2] = id830in_input;
  }
  { // Node ID: 258 (NodeMul)
    const HWOffsetFix<22,-43,UNSIGNED> &id258in_a = id256out_o[getCycle()%2];
    const HWOffsetFix<40,-40,UNSIGNED> &id258in_b = id318out_dout[getCycle()%3];

    id258out_result[(getCycle()+2)%3] = (mul_fixed<62,-83,UNSIGNED,TONEAREVEN>(id258in_a,id258in_b));
  }
  { // Node ID: 259 (NodeAdd)
    const HWOffsetFix<44,-43,UNSIGNED> &id259in_a = id830out_output[getCycle()%2];
    const HWOffsetFix<62,-83,UNSIGNED> &id259in_b = id258out_result[getCycle()%3];

    id259out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id259in_a,id259in_b));
  }
  { // Node ID: 260 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id260in_i = id259out_result[getCycle()%3];

    id260out_o[(getCycle()+1)%2] = (cast_fixed2fixed<44,-43,UNSIGNED,TONEAREVEN>(id260in_i));
  }
  { // Node ID: 261 (NodeAdd)
    const HWOffsetFix<39,-40,UNSIGNED> &id261in_a = id321out_dout[getCycle()%3];
    const HWOffsetFix<44,-43,UNSIGNED> &id261in_b = id260out_o[getCycle()%2];

    id261out_result[(getCycle()+1)%2] = (add_fixed<45,-43,UNSIGNED,TONEAREVEN>(id261in_a,id261in_b));
  }
  { // Node ID: 831 (NodeFIFO)
    const HWOffsetFix<45,-43,UNSIGNED> &id831in_input = id261out_result[getCycle()%2];

    id831out_output[(getCycle()+4)%5] = id831in_input;
  }
  { // Node ID: 262 (NodeMul)
    const HWOffsetFix<44,-43,UNSIGNED> &id262in_a = id260out_o[getCycle()%2];
    const HWOffsetFix<39,-40,UNSIGNED> &id262in_b = id321out_dout[getCycle()%3];

    id262out_result[(getCycle()+5)%6] = (mul_fixed<64,-64,UNSIGNED,TONEAREVEN>(id262in_a,id262in_b));
  }
  { // Node ID: 263 (NodeAdd)
    const HWOffsetFix<45,-43,UNSIGNED> &id263in_a = id831out_output[getCycle()%5];
    const HWOffsetFix<64,-64,UNSIGNED> &id263in_b = id262out_result[getCycle()%6];

    id263out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id263in_a,id263in_b));
  }
  { // Node ID: 264 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id264in_i = id263out_result[getCycle()%3];

    id264out_o[(getCycle()+1)%2] = (cast_fixed2fixed<45,-43,UNSIGNED,TONEAREVEN>(id264in_i));
  }
  { // Node ID: 265 (NodeAdd)
    const HWOffsetFix<29,-40,UNSIGNED> &id265in_a = id324out_dout[getCycle()%3];
    const HWOffsetFix<45,-43,UNSIGNED> &id265in_b = id264out_o[getCycle()%2];

    id265out_result[(getCycle()+1)%2] = (add_fixed<46,-43,UNSIGNED,TONEAREVEN>(id265in_a,id265in_b));
  }
  { // Node ID: 832 (NodeFIFO)
    const HWOffsetFix<46,-43,UNSIGNED> &id832in_input = id265out_result[getCycle()%2];

    id832out_output[(getCycle()+4)%5] = id832in_input;
  }
  { // Node ID: 266 (NodeMul)
    const HWOffsetFix<45,-43,UNSIGNED> &id266in_a = id264out_o[getCycle()%2];
    const HWOffsetFix<29,-40,UNSIGNED> &id266in_b = id324out_dout[getCycle()%3];

    id266out_result[(getCycle()+5)%6] = (mul_fixed<64,-73,UNSIGNED,TONEAREVEN>(id266in_a,id266in_b));
  }
  { // Node ID: 267 (NodeAdd)
    const HWOffsetFix<46,-43,UNSIGNED> &id267in_a = id832out_output[getCycle()%5];
    const HWOffsetFix<64,-73,UNSIGNED> &id267in_b = id266out_result[getCycle()%6];

    id267out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id267in_a,id267in_b));
  }
  { // Node ID: 268 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id268in_i = id267out_result[getCycle()%3];

    id268out_o[(getCycle()+1)%2] = (cast_fixed2fixed<46,-43,UNSIGNED,TONEAREVEN>(id268in_i));
  }
  { // Node ID: 269 (NodeCast)
    const HWOffsetFix<46,-43,UNSIGNED> &id269in_i = id268out_o[getCycle()%2];

    id269out_o[(getCycle()+1)%2] = (cast_fixed2fixed<40,-39,UNSIGNED,TONEAREVEN>(id269in_i));
  }
  { // Node ID: 987 (NodeConstantRawBits)
  }
  { // Node ID: 717 (NodeGteInlined)
    const HWOffsetFix<40,-39,UNSIGNED> &id717in_a = id269out_o[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id717in_b = id987out_value;

    id717out_result[(getCycle()+1)%2] = (gte_fixed(id717in_a,id717in_b));
  }
  { // Node ID: 748 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id748in_a = id275out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id748in_b = id278out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id748in_c = id743out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id748in_condb = id717out_result[getCycle()%2];

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id748x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id748x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id748x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id748x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id748x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id748x_1 = id748in_a;
        break;
      default:
        id748x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id748in_condb.getValueAsLong())) {
      case 0l:
        id748x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id748x_2 = id748in_b;
        break;
      default:
        id748x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id748x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id748x_3 = id748in_c;
        break;
      default:
        id748x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id748x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id748x_1),(id748x_2))),(id748x_3)));
    id748out_result[(getCycle()+1)%2] = (id748x_4);
  }
  HWRawBits<1> id718out_result;

  { // Node ID: 718 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id718in_a = id748out_result[getCycle()%2];

    id718out_result = (slice<10,1>(id718in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id719out_output;

  { // Node ID: 719 (NodeReinterpret)
    const HWRawBits<1> &id719in_input = id718out_result;

    id719out_output = (cast_bits2fixed<1,0,UNSIGNED>(id719in_input));
  }
  { // Node ID: 986 (NodeConstantRawBits)
  }
  { // Node ID: 240 (NodeGt)
    const HWFloat<8,40> &id240in_a = id751out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id240in_b = id986out_value;

    id240out_result[(getCycle()+1)%2] = (gt_float(id240in_a,id240in_b));
  }
  { // Node ID: 833 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id833in_input = id240out_result[getCycle()%2];

    id833out_output[(getCycle()+12)%13] = id833in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id241out_output;

  { // Node ID: 241 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id241in_input = id238out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id241in_input_doubt = id238out_o_doubt[getCycle()%2];

    id241out_output = id241in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id242out_result;

  { // Node ID: 242 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id242in_a = id833out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id242in_b = id241out_output;

    HWOffsetFix<1,0,UNSIGNED> id242x_1;

    (id242x_1) = (and_fixed(id242in_a,id242in_b));
    id242out_result = (id242x_1);
  }
  { // Node ID: 834 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id834in_input = id242out_result;

    id834out_output[(getCycle()+27)%28] = id834in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id284out_result;

  { // Node ID: 284 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id284in_a = id834out_output[getCycle()%28];

    id284out_result = (not_fixed(id284in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id285in_a = id719out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id285in_b = id284out_result;

    HWOffsetFix<1,0,UNSIGNED> id285x_1;

    (id285x_1) = (and_fixed(id285in_a,id285in_b));
    id285out_result = (id285x_1);
  }
  { // Node ID: 985 (NodeConstantRawBits)
  }
  { // Node ID: 244 (NodeLt)
    const HWFloat<8,40> &id244in_a = id751out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id244in_b = id985out_value;

    id244out_result[(getCycle()+1)%2] = (lt_float(id244in_a,id244in_b));
  }
  { // Node ID: 835 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id835in_input = id244out_result[getCycle()%2];

    id835out_output[(getCycle()+12)%13] = id835in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id245out_output;

  { // Node ID: 245 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id245in_input = id238out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id245in_input_doubt = id238out_o_doubt[getCycle()%2];

    id245out_output = id245in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id246out_result;

  { // Node ID: 246 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id246in_a = id835out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id246in_b = id245out_output;

    HWOffsetFix<1,0,UNSIGNED> id246x_1;

    (id246x_1) = (and_fixed(id246in_a,id246in_b));
    id246out_result = (id246x_1);
  }
  { // Node ID: 836 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id836in_input = id246out_result;

    id836out_output[(getCycle()+27)%28] = id836in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id286out_result;

  { // Node ID: 286 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id286in_a = id285out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id286in_b = id836out_output[getCycle()%28];

    HWOffsetFix<1,0,UNSIGNED> id286x_1;

    (id286x_1) = (or_fixed(id286in_a,id286in_b));
    id286out_result = (id286x_1);
  }
  { // Node ID: 840 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id840in_input = id286out_result;

    id840out_output[(getCycle()+1)%2] = id840in_input;
  }
  { // Node ID: 984 (NodeConstantRawBits)
  }
  { // Node ID: 720 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id720in_a = id748out_result[getCycle()%2];
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id720in_b = id984out_value;

    id720out_result[(getCycle()+1)%2] = (gte_fixed(id720in_a,id720in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id293out_result;

  { // Node ID: 293 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id293in_a = id836out_output[getCycle()%28];

    id293out_result = (not_fixed(id293in_a));
  }
  { // Node ID: 838 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id838in_input = id293out_result;

    id838out_output[(getCycle()+1)%2] = id838in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id294out_result;

  { // Node ID: 294 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id294in_a = id720out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id294in_b = id838out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id294x_1;

    (id294x_1) = (and_fixed(id294in_a,id294in_b));
    id294out_result = (id294x_1);
  }
  { // Node ID: 932 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id932in_input = id834out_output[getCycle()%28];

    id932out_output[(getCycle()+1)%2] = id932in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id295out_result;

  { // Node ID: 295 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id295in_a = id294out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id295in_b = id932out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id295x_1;

    (id295x_1) = (or_fixed(id295in_a,id295in_b));
    id295out_result = (id295x_1);
  }
  HWRawBits<2> id296out_result;

  { // Node ID: 296 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id296in_in0 = id840out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id296in_in1 = id295out_result;

    id296out_result = (cat(id296in_in0,id296in_in1));
  }
  { // Node ID: 288 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id287out_o;

  { // Node ID: 287 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id287in_i = id748out_result[getCycle()%2];

    id287out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id287in_i));
  }
  { // Node ID: 842 (NodeFIFO)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id842in_input = id287out_o;

    id842out_output[(getCycle()+1)%2] = id842in_input;
  }
  { // Node ID: 841 (NodeFIFO)
    const HWOffsetFix<40,-39,UNSIGNED> &id841in_input = id269out_o[getCycle()%2];

    id841out_output[(getCycle()+1)%2] = id841in_input;
  }
  { // Node ID: 272 (NodeConstantRawBits)
  }
  { // Node ID: 273 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id273in_sel = id717out_result[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id273in_option0 = id841out_output[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id273in_option1 = id272out_value;

    HWOffsetFix<40,-39,UNSIGNED> id273x_1;

    switch((id273in_sel.getValueAsLong())) {
      case 0l:
        id273x_1 = id273in_option0;
        break;
      case 1l:
        id273x_1 = id273in_option1;
        break;
      default:
        id273x_1 = (c_hw_fix_40_n39_uns_undef);
        break;
    }
    id273out_result[(getCycle()+1)%2] = (id273x_1);
  }
  HWOffsetFix<39,-39,UNSIGNED> id274out_o;

  { // Node ID: 274 (NodeCast)
    const HWOffsetFix<40,-39,UNSIGNED> &id274in_i = id273out_result[getCycle()%2];

    id274out_o = (cast_fixed2fixed<39,-39,UNSIGNED,TONEAREVEN>(id274in_i));
  }
  { // Node ID: 843 (NodeFIFO)
    const HWOffsetFix<39,-39,UNSIGNED> &id843in_input = id274out_o;

    id843out_output[(getCycle()+1)%2] = id843in_input;
  }
  HWRawBits<48> id289out_result;

  { // Node ID: 289 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id289in_in0 = id288out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id289in_in1 = id842out_output[getCycle()%2];
    const HWOffsetFix<39,-39,UNSIGNED> &id289in_in2 = id843out_output[getCycle()%2];

    id289out_result = (cat((cat(id289in_in0,id289in_in1)),id289in_in2));
  }
  HWFloat<8,40> id290out_output;

  { // Node ID: 290 (NodeReinterpret)
    const HWRawBits<48> &id290in_input = id289out_result;

    id290out_output = (cast_bits2float<8,40>(id290in_input));
  }
  { // Node ID: 297 (NodeConstantRawBits)
  }
  { // Node ID: 298 (NodeConstantRawBits)
  }
  { // Node ID: 300 (NodeConstantRawBits)
  }
  HWRawBits<48> id721out_result;

  { // Node ID: 721 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in0 = id297out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id721in_in1 = id298out_value;
    const HWOffsetFix<39,0,UNSIGNED> &id721in_in2 = id300out_value;

    id721out_result = (cat((cat(id721in_in0,id721in_in1)),id721in_in2));
  }
  HWFloat<8,40> id302out_output;

  { // Node ID: 302 (NodeReinterpret)
    const HWRawBits<48> &id302in_input = id721out_result;

    id302out_output = (cast_bits2float<8,40>(id302in_input));
  }
  { // Node ID: 701 (NodeConstantRawBits)
  }
  { // Node ID: 305 (NodeMux)
    const HWRawBits<2> &id305in_sel = id296out_result;
    const HWFloat<8,40> &id305in_option0 = id290out_output;
    const HWFloat<8,40> &id305in_option1 = id302out_output;
    const HWFloat<8,40> &id305in_option2 = id701out_value;
    const HWFloat<8,40> &id305in_option3 = id302out_output;

    HWFloat<8,40> id305x_1;

    switch((id305in_sel.getValueAsLong())) {
      case 0l:
        id305x_1 = id305in_option0;
        break;
      case 1l:
        id305x_1 = id305in_option1;
        break;
      case 2l:
        id305x_1 = id305in_option2;
        break;
      case 3l:
        id305x_1 = id305in_option3;
        break;
      default:
        id305x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id305out_result[(getCycle()+1)%2] = (id305x_1);
  }
  { // Node ID: 983 (NodeConstantRawBits)
  }
  { // Node ID: 315 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id315in_sel = id844out_output[getCycle()%42];
    const HWFloat<8,40> &id315in_option0 = id305out_result[getCycle()%2];
    const HWFloat<8,40> &id315in_option1 = id983out_value;

    HWFloat<8,40> id315x_1;

    switch((id315in_sel.getValueAsLong())) {
      case 0l:
        id315x_1 = id315in_option0;
        break;
      case 1l:
        id315x_1 = id315in_option1;
        break;
      default:
        id315x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id315out_result[(getCycle()+1)%2] = (id315x_1);
  }
  { // Node ID: 982 (NodeConstantRawBits)
  }
  { // Node ID: 326 (NodeAdd)
    const HWFloat<8,40> &id326in_a = id315out_result[getCycle()%2];
    const HWFloat<8,40> &id326in_b = id982out_value;

    id326out_result[(getCycle()+6)%7] = (add_float(id326in_a,id326in_b));
  }
  { // Node ID: 328 (NodeDiv)
    const HWFloat<8,40> &id328in_a = id989out_value;
    const HWFloat<8,40> &id328in_b = id326out_result[getCycle()%7];

    id328out_result[(getCycle()+22)%23] = (div_float(id328in_a,id328in_b));
  }
  { // Node ID: 330 (NodeSub)
    const HWFloat<8,40> &id330in_a = id990out_value;
    const HWFloat<8,40> &id330in_b = id328out_result[getCycle()%23];

    id330out_result[(getCycle()+6)%7] = (sub_float(id330in_a,id330in_b));
  }
  { // Node ID: 332 (NodeAdd)
    const HWFloat<8,40> &id332in_a = id991out_value;
    const HWFloat<8,40> &id332in_b = id330out_result[getCycle()%7];

    id332out_result[(getCycle()+6)%7] = (add_float(id332in_a,id332in_b));
  }
  { // Node ID: 333 (NodeMul)
    const HWFloat<8,40> &id333in_a = id30out_value;
    const HWFloat<8,40> &id333in_b = id332out_result[getCycle()%7];

    id333out_result[(getCycle()+6)%7] = (mul_float(id333in_a,id333in_b));
  }
  { // Node ID: 334 (NodeAdd)
    const HWFloat<8,40> &id334in_a = id4out_value;
    const HWFloat<8,40> &id334in_b = id333out_result[getCycle()%7];

    id334out_result[(getCycle()+6)%7] = (add_float(id334in_a,id334in_b));
  }
  { // Node ID: 447 (NodeRegister)
    const HWFloat<8,40> &id447in_input = id334out_result[getCycle()%7];

    id447out_output[(getCycle()+1)%2] = id447in_input;
  }
  { // Node ID: 472 (NodeDiv)
    const HWFloat<8,40> &id472in_a = id471out_result[getCycle()%7];
    const HWFloat<8,40> &id472in_b = id447out_output[getCycle()%2];

    id472out_result[(getCycle()+22)%23] = (div_float(id472in_a,id472in_b));
  }
  HWFloat<8,40> id469out_result;

  { // Node ID: 469 (NodeNeg)
    const HWFloat<8,40> &id469in_a = id930out_output[getCycle()%7];

    id469out_result = (neg_float(id469in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  { // Node ID: 470 (NodeDiv)
    const HWFloat<8,40> &id470in_a = id469out_result;
    const HWFloat<8,40> &id470in_b = id3out_value;

    id470out_result[(getCycle()+22)%23] = (div_float(id470in_a,id470in_b));
  }
  { // Node ID: 473 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id473in_sel = id468out_result[getCycle()%2];
    const HWFloat<8,40> &id473in_option0 = id472out_result[getCycle()%23];
    const HWFloat<8,40> &id473in_option1 = id470out_result[getCycle()%23];

    HWFloat<8,40> id473x_1;

    switch((id473in_sel.getValueAsLong())) {
      case 0l:
        id473x_1 = id473in_option0;
        break;
      case 1l:
        id473x_1 = id473in_option1;
        break;
      default:
        id473x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id473out_result[(getCycle()+1)%2] = (id473x_1);
  }
  { // Node ID: 584 (NodeRegister)
    const HWFloat<8,40> &id584in_input = id473out_result[getCycle()%2];

    id584out_output[(getCycle()+1)%2] = id584in_input;
  }
  { // Node ID: 588 (NodeMul)
    const HWFloat<8,40> &id588in_a = id584out_output[getCycle()%2];
    const HWFloat<8,40> &id588in_b = id37out_o[getCycle()%4];

    id588out_result[(getCycle()+12)%13] = (mul_float(id588in_a,id588in_b));
  }
  { // Node ID: 589 (NodeAdd)
    const HWFloat<8,40> &id589in_a = id931out_output[getCycle()%37];
    const HWFloat<8,40> &id589in_b = id588out_result[getCycle()%13];

    id589out_result[(getCycle()+12)%13] = (add_float(id589in_a,id589in_b));
  }
  HWFloat<8,40> id607out_result;

  { // Node ID: 607 (NodeNeg)
    const HWFloat<8,40> &id607in_a = id589out_result[getCycle()%13];

    id607out_result = (neg_float(id607in_a));
  }
  HWOffsetFix<24,0,UNSIGNED> id637out_o;

  { // Node ID: 637 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id637in_i = id778out_output[getCycle()%28];

    id637out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id637in_i));
  }
  HWFloat<8,40> id771out_output;

  { // Node ID: 771 (NodeStreamOffset)
    const HWFloat<8,40> &id771in_input = id848out_output[getCycle()%8];

    id771out_output = id771in_input;
  }
  { // Node ID: 849 (NodeFIFO)
    const HWFloat<8,40> &id849in_input = id771out_output;

    id849out_output[(getCycle()+59)%60] = id849in_input;
  }
  { // Node ID: 981 (NodeConstantRawBits)
  }
  { // Node ID: 639 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id639in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id639in_b = id981out_value;

    id639out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id639in_a,id639in_b));
  }
  { // Node ID: 722 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id722in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id722in_b = id639out_result[getCycle()%2];

    id722out_result[(getCycle()+1)%2] = (eq_fixed(id722in_a,id722in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id52out_o;

  { // Node ID: 52 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id52in_i = id47out_count;

    id52out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id52in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 685 (NodeRAM)
    const bool id685_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id685in_addrA = id637out_o;
    const HWFloat<8,40> &id685in_dina = id849out_output[getCycle()%60];
    const HWOffsetFix<1,0,UNSIGNED> &id685in_wea = id722out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id685in_addrB = id52out_o;

    long id685x_1;
    long id685x_2;
    HWFloat<8,40> id685x_3;

    (id685x_1) = (id685in_addrA.getValueAsLong());
    (id685x_2) = (id685in_addrB.getValueAsLong());
    switch(((long)((id685x_2)<(10000000l)))) {
      case 0l:
        id685x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id685x_3 = (id685sta_ram_store[(id685x_2)]);
        break;
      default:
        id685x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id685out_doutb[(getCycle()+2)%3] = (id685x_3);
    if(((id685in_wea.getValueAsBool())&id685_inputvalid)) {
      if(((id685x_1)<(10000000l))) {
        (id685sta_ram_store[(id685x_1)]) = id685in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_4("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 685) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 66 (NodeConstantRawBits)
  }
  { // Node ID: 67 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id67in_sel = id704out_result[getCycle()%2];
    const HWFloat<8,40> &id67in_option0 = id685out_doutb[getCycle()%3];
    const HWFloat<8,40> &id67in_option1 = id66out_value;

    HWFloat<8,40> id67x_1;

    switch((id67in_sel.getValueAsLong())) {
      case 0l:
        id67x_1 = id67in_option0;
        break;
      case 1l:
        id67x_1 = id67in_option1;
        break;
      default:
        id67x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id67out_result[(getCycle()+1)%2] = (id67x_1);
  }
  { // Node ID: 869 (NodeFIFO)
    const HWFloat<8,40> &id869in_input = id67out_result[getCycle()%2];

    id869out_output[(getCycle()+97)%98] = id869in_input;
  }
  { // Node ID: 933 (NodeFIFO)
    const HWFloat<8,40> &id933in_input = id869out_output[getCycle()%98];

    id933out_output[(getCycle()+41)%42] = id933in_input;
  }
  { // Node ID: 980 (NodeConstantRawBits)
  }
  { // Node ID: 979 (NodeConstantRawBits)
  }
  { // Node ID: 978 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 474 (NodeSub)
    const HWFloat<8,40> &id474in_a = id61out_result[getCycle()%2];
    const HWFloat<8,40> &id474in_b = id26out_value;

    id474out_result[(getCycle()+6)%7] = (sub_float(id474in_a,id474in_b));
  }
  { // Node ID: 475 (NodeMul)
    const HWFloat<8,40> &id475in_a = id23out_value;
    const HWFloat<8,40> &id475in_b = id474out_result[getCycle()%7];

    id475out_result[(getCycle()+6)%7] = (mul_float(id475in_a,id475in_b));
  }
  { // Node ID: 752 (NodePO2FPMult)
    const HWFloat<8,40> &id752in_floatIn = id475out_result[getCycle()%7];

    id752out_floatOut[(getCycle()+1)%2] = (mul_float(id752in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  HWRawBits<8> id554out_result;

  { // Node ID: 554 (NodeSlice)
    const HWFloat<8,40> &id554in_a = id752out_floatOut[getCycle()%2];

    id554out_result = (slice<39,8>(id554in_a));
  }
  { // Node ID: 555 (NodeConstantRawBits)
  }
  { // Node ID: 723 (NodeEqInlined)
    const HWRawBits<8> &id723in_a = id554out_result;
    const HWRawBits<8> &id723in_b = id555out_value;

    id723out_result[(getCycle()+1)%2] = (eq_bits(id723in_a,id723in_b));
  }
  HWRawBits<39> id553out_result;

  { // Node ID: 553 (NodeSlice)
    const HWFloat<8,40> &id553in_a = id752out_floatOut[getCycle()%2];

    id553out_result = (slice<0,39>(id553in_a));
  }
  { // Node ID: 977 (NodeConstantRawBits)
  }
  { // Node ID: 724 (NodeNeqInlined)
    const HWRawBits<39> &id724in_a = id553out_result;
    const HWRawBits<39> &id724in_b = id977out_value;

    id724out_result[(getCycle()+1)%2] = (neq_bits(id724in_a,id724in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id559out_result;

  { // Node ID: 559 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id559in_a = id723out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id559in_b = id724out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id559x_1;

    (id559x_1) = (and_fixed(id559in_a,id559in_b));
    id559out_result = (id559x_1);
  }
  { // Node ID: 868 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id868in_input = id559out_result;

    id868out_output[(getCycle()+41)%42] = id868in_input;
  }
  { // Node ID: 478 (NodeConstantRawBits)
  }
  HWFloat<8,40> id479out_output;
  HWOffsetFix<1,0,UNSIGNED> id479out_output_doubt;

  { // Node ID: 479 (NodeDoubtBitOp)
    const HWFloat<8,40> &id479in_input = id752out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id479in_doubt = id478out_value;

    id479out_output = id479in_input;
    id479out_output_doubt = id479in_doubt;
  }
  { // Node ID: 480 (NodeCast)
    const HWFloat<8,40> &id480in_i = id479out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id480in_i_doubt = id479out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id480x_1;

    id480out_o[(getCycle()+6)%7] = (cast_float2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id480in_i,(&(id480x_1))));
    id480out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id480x_1),(c_hw_fix_4_0_uns_bits))),id480in_i_doubt));
  }
  { // Node ID: 483 (NodeConstantRawBits)
  }
  { // Node ID: 482 (NodeMul)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id482in_a = id480out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id482in_a_doubt = id480out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id482in_b = id483out_value;

    HWOffsetFix<1,0,UNSIGNED> id482x_1;

    id482out_result[(getCycle()+6)%7] = (mul_fixed<105,-94,TWOSCOMPLEMENT,TONEAREVEN>(id482in_a,id482in_b,(&(id482x_1))));
    id482out_result_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id482x_1),(c_hw_fix_1_0_uns_bits_1))),id482in_a_doubt));
  }
  { // Node ID: 484 (NodeCast)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id484in_i = id482out_result[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id484in_i_doubt = id482out_result_doubt[getCycle()%7];

    HWOffsetFix<1,0,UNSIGNED> id484x_1;

    id484out_o[(getCycle()+1)%2] = (cast_fixed2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id484in_i,(&(id484x_1))));
    id484out_o_doubt[(getCycle()+1)%2] = (or_fixed((neq_fixed((id484x_1),(c_hw_fix_1_0_uns_bits_1))),id484in_i_doubt));
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id493out_output;

  { // Node ID: 493 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id493in_input = id484out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id493in_input_doubt = id484out_o_doubt[getCycle()%2];

    id493out_output = id493in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id494out_o;

  { // Node ID: 494 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id494in_i = id493out_output;

    id494out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id494in_i));
  }
  { // Node ID: 850 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id850in_input = id494out_o;

    id850out_output[(getCycle()+26)%27] = id850in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id521out_o;

  { // Node ID: 521 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id521in_i = id850out_output[getCycle()%27];

    id521out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id521in_i));
  }
  { // Node ID: 524 (NodeConstantRawBits)
  }
  { // Node ID: 745 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-21,UNSIGNED> id497out_o;

  { // Node ID: 497 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id497in_i = id493out_output;

    id497out_o = (cast_fixed2fixed<10,-21,UNSIGNED,TRUNCATE>(id497in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id569out_output;

  { // Node ID: 569 (NodeReinterpret)
    const HWOffsetFix<10,-21,UNSIGNED> &id569in_input = id497out_o;

    id569out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id569in_input))));
  }
  { // Node ID: 851 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id851in_input = id569out_output;

    id851out_output[(getCycle()+14)%15] = id851in_input;
  }
  { // Node ID: 570 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id570in_addr = id851out_output[getCycle()%15];

    HWOffsetFix<29,-40,UNSIGNED> id570x_1;

    switch(((long)((id570in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id570x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
      case 1l:
        id570x_1 = (id570sta_rom_store[(id570in_addr.getValueAsLong())]);
        break;
      default:
        id570x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
    }
    id570out_dout[(getCycle()+2)%3] = (id570x_1);
  }
  HWOffsetFix<10,-11,UNSIGNED> id496out_o;

  { // Node ID: 496 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id496in_i = id493out_output;

    id496out_o = (cast_fixed2fixed<10,-11,UNSIGNED,TRUNCATE>(id496in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id566out_output;

  { // Node ID: 566 (NodeReinterpret)
    const HWOffsetFix<10,-11,UNSIGNED> &id566in_input = id496out_o;

    id566out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id566in_input))));
  }
  { // Node ID: 852 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id852in_input = id566out_output;

    id852out_output[(getCycle()+6)%7] = id852in_input;
  }
  { // Node ID: 567 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id567in_addr = id852out_output[getCycle()%7];

    HWOffsetFix<39,-40,UNSIGNED> id567x_1;

    switch(((long)((id567in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id567x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
      case 1l:
        id567x_1 = (id567sta_rom_store[(id567in_addr.getValueAsLong())]);
        break;
      default:
        id567x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
    }
    id567out_dout[(getCycle()+2)%3] = (id567x_1);
  }
  HWOffsetFix<1,-1,UNSIGNED> id495out_o;

  { // Node ID: 495 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id495in_i = id493out_output;

    id495out_o = (cast_fixed2fixed<1,-1,UNSIGNED,TRUNCATE>(id495in_i));
  }
  HWOffsetFix<1,0,UNSIGNED> id563out_output;

  { // Node ID: 563 (NodeReinterpret)
    const HWOffsetFix<1,-1,UNSIGNED> &id563in_input = id495out_o;

    id563out_output = (cast_bits2fixed<1,0,UNSIGNED>((cast_fixed2bits(id563in_input))));
  }
  { // Node ID: 853 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id853in_input = id563out_output;

    id853out_output[(getCycle()+1)%2] = id853in_input;
  }
  { // Node ID: 564 (NodeROM)
    const HWOffsetFix<1,0,UNSIGNED> &id564in_addr = id853out_output[getCycle()%2];

    HWOffsetFix<40,-40,UNSIGNED> id564x_1;

    switch(((long)((id564in_addr.getValueAsLong())<(2l)))) {
      case 0l:
        id564x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
      case 1l:
        id564x_1 = (id564sta_rom_store[(id564in_addr.getValueAsLong())]);
        break;
      default:
        id564x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
    }
    id564out_dout[(getCycle()+2)%3] = (id564x_1);
  }
  { // Node ID: 501 (NodeConstantRawBits)
  }
  HWOffsetFix<22,-43,UNSIGNED> id498out_o;

  { // Node ID: 498 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id498in_i = id493out_output;

    id498out_o = (cast_fixed2fixed<22,-43,UNSIGNED,TRUNCATE>(id498in_i));
  }
  { // Node ID: 500 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id500in_a = id501out_value;
    const HWOffsetFix<22,-43,UNSIGNED> &id500in_b = id498out_o;

    id500out_result[(getCycle()+2)%3] = (mul_fixed<43,-64,UNSIGNED,TONEAREVEN>(id500in_a,id500in_b));
  }
  { // Node ID: 502 (NodeCast)
    const HWOffsetFix<43,-64,UNSIGNED> &id502in_i = id500out_result[getCycle()%3];

    id502out_o[(getCycle()+1)%2] = (cast_fixed2fixed<22,-43,UNSIGNED,TONEAREVEN>(id502in_i));
  }
  { // Node ID: 503 (NodeAdd)
    const HWOffsetFix<40,-40,UNSIGNED> &id503in_a = id564out_dout[getCycle()%3];
    const HWOffsetFix<22,-43,UNSIGNED> &id503in_b = id502out_o[getCycle()%2];

    id503out_result[(getCycle()+1)%2] = (add_fixed<44,-43,UNSIGNED,TONEAREVEN>(id503in_a,id503in_b));
  }
  { // Node ID: 854 (NodeFIFO)
    const HWOffsetFix<44,-43,UNSIGNED> &id854in_input = id503out_result[getCycle()%2];

    id854out_output[(getCycle()+1)%2] = id854in_input;
  }
  { // Node ID: 504 (NodeMul)
    const HWOffsetFix<22,-43,UNSIGNED> &id504in_a = id502out_o[getCycle()%2];
    const HWOffsetFix<40,-40,UNSIGNED> &id504in_b = id564out_dout[getCycle()%3];

    id504out_result[(getCycle()+2)%3] = (mul_fixed<62,-83,UNSIGNED,TONEAREVEN>(id504in_a,id504in_b));
  }
  { // Node ID: 505 (NodeAdd)
    const HWOffsetFix<44,-43,UNSIGNED> &id505in_a = id854out_output[getCycle()%2];
    const HWOffsetFix<62,-83,UNSIGNED> &id505in_b = id504out_result[getCycle()%3];

    id505out_result[(getCycle()+2)%3] = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id505in_a,id505in_b));
  }
  { // Node ID: 506 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id506in_i = id505out_result[getCycle()%3];

    id506out_o[(getCycle()+1)%2] = (cast_fixed2fixed<44,-43,UNSIGNED,TONEAREVEN>(id506in_i));
  }
  { // Node ID: 507 (NodeAdd)
    const HWOffsetFix<39,-40,UNSIGNED> &id507in_a = id567out_dout[getCycle()%3];
    const HWOffsetFix<44,-43,UNSIGNED> &id507in_b = id506out_o[getCycle()%2];

    id507out_result[(getCycle()+1)%2] = (add_fixed<45,-43,UNSIGNED,TONEAREVEN>(id507in_a,id507in_b));
  }
  { // Node ID: 855 (NodeFIFO)
    const HWOffsetFix<45,-43,UNSIGNED> &id855in_input = id507out_result[getCycle()%2];

    id855out_output[(getCycle()+4)%5] = id855in_input;
  }
  { // Node ID: 508 (NodeMul)
    const HWOffsetFix<44,-43,UNSIGNED> &id508in_a = id506out_o[getCycle()%2];
    const HWOffsetFix<39,-40,UNSIGNED> &id508in_b = id567out_dout[getCycle()%3];

    id508out_result[(getCycle()+5)%6] = (mul_fixed<64,-64,UNSIGNED,TONEAREVEN>(id508in_a,id508in_b));
  }
  { // Node ID: 509 (NodeAdd)
    const HWOffsetFix<45,-43,UNSIGNED> &id509in_a = id855out_output[getCycle()%5];
    const HWOffsetFix<64,-64,UNSIGNED> &id509in_b = id508out_result[getCycle()%6];

    id509out_result[(getCycle()+2)%3] = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id509in_a,id509in_b));
  }
  { // Node ID: 510 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id510in_i = id509out_result[getCycle()%3];

    id510out_o[(getCycle()+1)%2] = (cast_fixed2fixed<45,-43,UNSIGNED,TONEAREVEN>(id510in_i));
  }
  { // Node ID: 511 (NodeAdd)
    const HWOffsetFix<29,-40,UNSIGNED> &id511in_a = id570out_dout[getCycle()%3];
    const HWOffsetFix<45,-43,UNSIGNED> &id511in_b = id510out_o[getCycle()%2];

    id511out_result[(getCycle()+1)%2] = (add_fixed<46,-43,UNSIGNED,TONEAREVEN>(id511in_a,id511in_b));
  }
  { // Node ID: 856 (NodeFIFO)
    const HWOffsetFix<46,-43,UNSIGNED> &id856in_input = id511out_result[getCycle()%2];

    id856out_output[(getCycle()+4)%5] = id856in_input;
  }
  { // Node ID: 512 (NodeMul)
    const HWOffsetFix<45,-43,UNSIGNED> &id512in_a = id510out_o[getCycle()%2];
    const HWOffsetFix<29,-40,UNSIGNED> &id512in_b = id570out_dout[getCycle()%3];

    id512out_result[(getCycle()+5)%6] = (mul_fixed<64,-73,UNSIGNED,TONEAREVEN>(id512in_a,id512in_b));
  }
  { // Node ID: 513 (NodeAdd)
    const HWOffsetFix<46,-43,UNSIGNED> &id513in_a = id856out_output[getCycle()%5];
    const HWOffsetFix<64,-73,UNSIGNED> &id513in_b = id512out_result[getCycle()%6];

    id513out_result[(getCycle()+2)%3] = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id513in_a,id513in_b));
  }
  { // Node ID: 514 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id514in_i = id513out_result[getCycle()%3];

    id514out_o[(getCycle()+1)%2] = (cast_fixed2fixed<46,-43,UNSIGNED,TONEAREVEN>(id514in_i));
  }
  { // Node ID: 515 (NodeCast)
    const HWOffsetFix<46,-43,UNSIGNED> &id515in_i = id514out_o[getCycle()%2];

    id515out_o[(getCycle()+1)%2] = (cast_fixed2fixed<40,-39,UNSIGNED,TONEAREVEN>(id515in_i));
  }
  { // Node ID: 976 (NodeConstantRawBits)
  }
  { // Node ID: 725 (NodeGteInlined)
    const HWOffsetFix<40,-39,UNSIGNED> &id725in_a = id515out_o[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id725in_b = id976out_value;

    id725out_result[(getCycle()+1)%2] = (gte_fixed(id725in_a,id725in_b));
  }
  { // Node ID: 747 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id747in_a = id521out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id747in_b = id524out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id747in_c = id745out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id747in_condb = id725out_result[getCycle()%2];

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id747x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id747x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id747x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id747x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id747x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id747x_1 = id747in_a;
        break;
      default:
        id747x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id747in_condb.getValueAsLong())) {
      case 0l:
        id747x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id747x_2 = id747in_b;
        break;
      default:
        id747x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id747x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id747x_3 = id747in_c;
        break;
      default:
        id747x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id747x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id747x_1),(id747x_2))),(id747x_3)));
    id747out_result[(getCycle()+1)%2] = (id747x_4);
  }
  HWRawBits<1> id726out_result;

  { // Node ID: 726 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id726in_a = id747out_result[getCycle()%2];

    id726out_result = (slice<10,1>(id726in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id727out_output;

  { // Node ID: 727 (NodeReinterpret)
    const HWRawBits<1> &id727in_input = id726out_result;

    id727out_output = (cast_bits2fixed<1,0,UNSIGNED>(id727in_input));
  }
  { // Node ID: 975 (NodeConstantRawBits)
  }
  { // Node ID: 486 (NodeGt)
    const HWFloat<8,40> &id486in_a = id752out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id486in_b = id975out_value;

    id486out_result[(getCycle()+1)%2] = (gt_float(id486in_a,id486in_b));
  }
  { // Node ID: 857 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id857in_input = id486out_result[getCycle()%2];

    id857out_output[(getCycle()+12)%13] = id857in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id487out_output;

  { // Node ID: 487 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id487in_input = id484out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id487in_input_doubt = id484out_o_doubt[getCycle()%2];

    id487out_output = id487in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id488out_result;

  { // Node ID: 488 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id488in_a = id857out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id488in_b = id487out_output;

    HWOffsetFix<1,0,UNSIGNED> id488x_1;

    (id488x_1) = (and_fixed(id488in_a,id488in_b));
    id488out_result = (id488x_1);
  }
  { // Node ID: 858 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_input = id488out_result;

    id858out_output[(getCycle()+27)%28] = id858in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id530out_result;

  { // Node ID: 530 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id530in_a = id858out_output[getCycle()%28];

    id530out_result = (not_fixed(id530in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id531out_result;

  { // Node ID: 531 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id531in_a = id727out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id531in_b = id530out_result;

    HWOffsetFix<1,0,UNSIGNED> id531x_1;

    (id531x_1) = (and_fixed(id531in_a,id531in_b));
    id531out_result = (id531x_1);
  }
  { // Node ID: 974 (NodeConstantRawBits)
  }
  { // Node ID: 490 (NodeLt)
    const HWFloat<8,40> &id490in_a = id752out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id490in_b = id974out_value;

    id490out_result[(getCycle()+1)%2] = (lt_float(id490in_a,id490in_b));
  }
  { // Node ID: 859 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id859in_input = id490out_result[getCycle()%2];

    id859out_output[(getCycle()+12)%13] = id859in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id491out_output;

  { // Node ID: 491 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id491in_input = id484out_o[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id491in_input_doubt = id484out_o_doubt[getCycle()%2];

    id491out_output = id491in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id492out_result;

  { // Node ID: 492 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id492in_a = id859out_output[getCycle()%13];
    const HWOffsetFix<1,0,UNSIGNED> &id492in_b = id491out_output;

    HWOffsetFix<1,0,UNSIGNED> id492x_1;

    (id492x_1) = (and_fixed(id492in_a,id492in_b));
    id492out_result = (id492x_1);
  }
  { // Node ID: 860 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id860in_input = id492out_result;

    id860out_output[(getCycle()+27)%28] = id860in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id532out_result;

  { // Node ID: 532 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id532in_a = id531out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id532in_b = id860out_output[getCycle()%28];

    HWOffsetFix<1,0,UNSIGNED> id532x_1;

    (id532x_1) = (or_fixed(id532in_a,id532in_b));
    id532out_result = (id532x_1);
  }
  { // Node ID: 864 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id864in_input = id532out_result;

    id864out_output[(getCycle()+1)%2] = id864in_input;
  }
  { // Node ID: 973 (NodeConstantRawBits)
  }
  { // Node ID: 728 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id728in_a = id747out_result[getCycle()%2];
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id728in_b = id973out_value;

    id728out_result[(getCycle()+1)%2] = (gte_fixed(id728in_a,id728in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id539out_result;

  { // Node ID: 539 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id539in_a = id860out_output[getCycle()%28];

    id539out_result = (not_fixed(id539in_a));
  }
  { // Node ID: 862 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id862in_input = id539out_result;

    id862out_output[(getCycle()+1)%2] = id862in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id540out_result;

  { // Node ID: 540 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id540in_a = id728out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id540in_b = id862out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id540x_1;

    (id540x_1) = (and_fixed(id540in_a,id540in_b));
    id540out_result = (id540x_1);
  }
  { // Node ID: 934 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id934in_input = id858out_output[getCycle()%28];

    id934out_output[(getCycle()+1)%2] = id934in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id541out_result;

  { // Node ID: 541 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id541in_a = id540out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id541in_b = id934out_output[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id541x_1;

    (id541x_1) = (or_fixed(id541in_a,id541in_b));
    id541out_result = (id541x_1);
  }
  HWRawBits<2> id542out_result;

  { // Node ID: 542 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id542in_in0 = id864out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id542in_in1 = id541out_result;

    id542out_result = (cat(id542in_in0,id542in_in1));
  }
  { // Node ID: 534 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id533out_o;

  { // Node ID: 533 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id533in_i = id747out_result[getCycle()%2];

    id533out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id533in_i));
  }
  { // Node ID: 866 (NodeFIFO)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id866in_input = id533out_o;

    id866out_output[(getCycle()+1)%2] = id866in_input;
  }
  { // Node ID: 865 (NodeFIFO)
    const HWOffsetFix<40,-39,UNSIGNED> &id865in_input = id515out_o[getCycle()%2];

    id865out_output[(getCycle()+1)%2] = id865in_input;
  }
  { // Node ID: 518 (NodeConstantRawBits)
  }
  { // Node ID: 519 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id519in_sel = id725out_result[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id519in_option0 = id865out_output[getCycle()%2];
    const HWOffsetFix<40,-39,UNSIGNED> &id519in_option1 = id518out_value;

    HWOffsetFix<40,-39,UNSIGNED> id519x_1;

    switch((id519in_sel.getValueAsLong())) {
      case 0l:
        id519x_1 = id519in_option0;
        break;
      case 1l:
        id519x_1 = id519in_option1;
        break;
      default:
        id519x_1 = (c_hw_fix_40_n39_uns_undef);
        break;
    }
    id519out_result[(getCycle()+1)%2] = (id519x_1);
  }
  HWOffsetFix<39,-39,UNSIGNED> id520out_o;

  { // Node ID: 520 (NodeCast)
    const HWOffsetFix<40,-39,UNSIGNED> &id520in_i = id519out_result[getCycle()%2];

    id520out_o = (cast_fixed2fixed<39,-39,UNSIGNED,TONEAREVEN>(id520in_i));
  }
  { // Node ID: 867 (NodeFIFO)
    const HWOffsetFix<39,-39,UNSIGNED> &id867in_input = id520out_o;

    id867out_output[(getCycle()+1)%2] = id867in_input;
  }
  HWRawBits<48> id535out_result;

  { // Node ID: 535 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id535in_in0 = id534out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id535in_in1 = id866out_output[getCycle()%2];
    const HWOffsetFix<39,-39,UNSIGNED> &id535in_in2 = id867out_output[getCycle()%2];

    id535out_result = (cat((cat(id535in_in0,id535in_in1)),id535in_in2));
  }
  HWFloat<8,40> id536out_output;

  { // Node ID: 536 (NodeReinterpret)
    const HWRawBits<48> &id536in_input = id535out_result;

    id536out_output = (cast_bits2float<8,40>(id536in_input));
  }
  { // Node ID: 543 (NodeConstantRawBits)
  }
  { // Node ID: 544 (NodeConstantRawBits)
  }
  { // Node ID: 546 (NodeConstantRawBits)
  }
  HWRawBits<48> id729out_result;

  { // Node ID: 729 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id729in_in0 = id543out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id729in_in1 = id544out_value;
    const HWOffsetFix<39,0,UNSIGNED> &id729in_in2 = id546out_value;

    id729out_result = (cat((cat(id729in_in0,id729in_in1)),id729in_in2));
  }
  HWFloat<8,40> id548out_output;

  { // Node ID: 548 (NodeReinterpret)
    const HWRawBits<48> &id548in_input = id729out_result;

    id548out_output = (cast_bits2float<8,40>(id548in_input));
  }
  { // Node ID: 702 (NodeConstantRawBits)
  }
  { // Node ID: 551 (NodeMux)
    const HWRawBits<2> &id551in_sel = id542out_result;
    const HWFloat<8,40> &id551in_option0 = id536out_output;
    const HWFloat<8,40> &id551in_option1 = id548out_output;
    const HWFloat<8,40> &id551in_option2 = id702out_value;
    const HWFloat<8,40> &id551in_option3 = id548out_output;

    HWFloat<8,40> id551x_1;

    switch((id551in_sel.getValueAsLong())) {
      case 0l:
        id551x_1 = id551in_option0;
        break;
      case 1l:
        id551x_1 = id551in_option1;
        break;
      case 2l:
        id551x_1 = id551in_option2;
        break;
      case 3l:
        id551x_1 = id551in_option3;
        break;
      default:
        id551x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id551out_result[(getCycle()+1)%2] = (id551x_1);
  }
  { // Node ID: 972 (NodeConstantRawBits)
  }
  { // Node ID: 561 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id561in_sel = id868out_output[getCycle()%42];
    const HWFloat<8,40> &id561in_option0 = id551out_result[getCycle()%2];
    const HWFloat<8,40> &id561in_option1 = id972out_value;

    HWFloat<8,40> id561x_1;

    switch((id561in_sel.getValueAsLong())) {
      case 0l:
        id561x_1 = id561in_option0;
        break;
      case 1l:
        id561x_1 = id561in_option1;
        break;
      default:
        id561x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id561out_result[(getCycle()+1)%2] = (id561x_1);
  }
  { // Node ID: 971 (NodeConstantRawBits)
  }
  { // Node ID: 572 (NodeAdd)
    const HWFloat<8,40> &id572in_a = id561out_result[getCycle()%2];
    const HWFloat<8,40> &id572in_b = id971out_value;

    id572out_result[(getCycle()+6)%7] = (add_float(id572in_a,id572in_b));
  }
  { // Node ID: 574 (NodeDiv)
    const HWFloat<8,40> &id574in_a = id978out_value;
    const HWFloat<8,40> &id574in_b = id572out_result[getCycle()%7];

    id574out_result[(getCycle()+22)%23] = (div_float(id574in_a,id574in_b));
  }
  { // Node ID: 576 (NodeSub)
    const HWFloat<8,40> &id576in_a = id979out_value;
    const HWFloat<8,40> &id576in_b = id574out_result[getCycle()%23];

    id576out_result[(getCycle()+6)%7] = (sub_float(id576in_a,id576in_b));
  }
  { // Node ID: 578 (NodeAdd)
    const HWFloat<8,40> &id578in_a = id980out_value;
    const HWFloat<8,40> &id578in_b = id576out_result[getCycle()%7];

    id578out_result[(getCycle()+6)%7] = (add_float(id578in_a,id578in_b));
  }
  { // Node ID: 753 (NodePO2FPMult)
    const HWFloat<8,40> &id753in_floatIn = id578out_result[getCycle()%7];

    id753out_floatOut[(getCycle()+1)%2] = (mul_float(id753in_floatIn,(c_hw_flt_8_40_0_5val)));
  }
  { // Node ID: 581 (NodeSub)
    const HWFloat<8,40> &id581in_a = id753out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id581in_b = id869out_output[getCycle()%98];

    id581out_result[(getCycle()+6)%7] = (sub_float(id581in_a,id581in_b));
  }
  { // Node ID: 442 (NodeGt)
    const HWFloat<8,40> &id442in_a = id918out_output[getCycle()%3];
    const HWFloat<8,40> &id442in_b = id18out_value;

    id442out_result[(getCycle()+1)%2] = (gt_float(id442in_a,id442in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  { // Node ID: 443 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_sel = id442out_result[getCycle()%2];
    const HWFloat<8,40> &id443in_option0 = id6out_value;
    const HWFloat<8,40> &id443in_option1 = id7out_value;

    HWFloat<8,40> id443x_1;

    switch((id443in_sel.getValueAsLong())) {
      case 0l:
        id443x_1 = id443in_option0;
        break;
      case 1l:
        id443x_1 = id443in_option1;
        break;
      default:
        id443x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id443out_result[(getCycle()+1)%2] = (id443x_1);
  }
  { // Node ID: 449 (NodeRegister)
    const HWFloat<8,40> &id449in_input = id443out_result[getCycle()%2];

    id449out_output[(getCycle()+1)%2] = id449in_input;
  }
  { // Node ID: 582 (NodeDiv)
    const HWFloat<8,40> &id582in_a = id581out_result[getCycle()%7];
    const HWFloat<8,40> &id582in_b = id449out_output[getCycle()%2];

    id582out_result[(getCycle()+22)%23] = (div_float(id582in_a,id582in_b));
  }
  { // Node ID: 585 (NodeRegister)
    const HWFloat<8,40> &id585in_input = id582out_result[getCycle()%23];

    id585out_output[(getCycle()+1)%2] = id585in_input;
  }
  { // Node ID: 590 (NodeMul)
    const HWFloat<8,40> &id590in_a = id585out_output[getCycle()%2];
    const HWFloat<8,40> &id590in_b = id37out_o[getCycle()%4];

    id590out_result[(getCycle()+12)%13] = (mul_float(id590in_a,id590in_b));
  }
  { // Node ID: 591 (NodeAdd)
    const HWFloat<8,40> &id591in_a = id933out_output[getCycle()%42];
    const HWFloat<8,40> &id591in_b = id590out_result[getCycle()%13];

    id591out_result[(getCycle()+12)%13] = (add_float(id591in_a,id591in_b));
  }
  { // Node ID: 848 (NodeFIFO)
    const HWFloat<8,40> &id848in_input = id591out_result[getCycle()%13];

    id848out_output[(getCycle()+7)%8] = id848in_input;
  }
  { // Node ID: 608 (NodeMul)
    const HWFloat<8,40> &id608in_a = id607out_result;
    const HWFloat<8,40> &id608in_b = id848out_output[getCycle()%8];

    id608out_result[(getCycle()+6)%7] = (mul_float(id608in_a,id608in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  { // Node ID: 609 (NodeDiv)
    const HWFloat<8,40> &id609in_a = id608out_result[getCycle()%7];
    const HWFloat<8,40> &id609in_b = id13out_value;

    id609out_result[(getCycle()+22)%23] = (div_float(id609in_a,id609in_b));
  }
  { // Node ID: 611 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id611in_sel = id606out_result[getCycle()%2];
    const HWFloat<8,40> &id611in_option0 = id610out_value;
    const HWFloat<8,40> &id611in_option1 = id609out_result[getCycle()%23];

    HWFloat<8,40> id611x_1;

    switch((id611in_sel.getValueAsLong())) {
      case 0l:
        id611x_1 = id611in_option0;
        break;
      case 1l:
        id611x_1 = id611in_option1;
        break;
      default:
        id611x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id611out_result[(getCycle()+1)%2] = (id611x_1);
  }
  { // Node ID: 613 (NodeAdd)
    const HWFloat<8,40> &id613in_a = id612out_result[getCycle()%7];
    const HWFloat<8,40> &id613in_b = id611out_result[getCycle()%2];

    id613out_result[(getCycle()+6)%7] = (add_float(id613in_a,id613in_b));
  }
  { // Node ID: 970 (NodeConstantRawBits)
  }
  { // Node ID: 730 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id730in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id730in_b = id970out_value;

    id730out_result[(getCycle()+1)%2] = (eq_fixed(id730in_a,id730in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id641out_o;

  { // Node ID: 641 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id641in_i = id778out_output[getCycle()%28];

    id641out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id641in_i));
  }
  HWFloat<8,40> id772out_output;

  { // Node ID: 772 (NodeStreamOffset)
    const HWFloat<8,40> &id772in_input = id935out_output[getCycle()%2];

    id772out_output = id772in_input;
  }
  { // Node ID: 875 (NodeFIFO)
    const HWFloat<8,40> &id875in_input = id772out_output;

    id875out_output[(getCycle()+49)%50] = id875in_input;
  }
  { // Node ID: 969 (NodeConstantRawBits)
  }
  { // Node ID: 643 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id643in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id643in_b = id969out_value;

    id643out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id643in_a,id643in_b));
  }
  { // Node ID: 731 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id731in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id731in_b = id643out_result[getCycle()%2];

    id731out_result[(getCycle()+1)%2] = (eq_fixed(id731in_a,id731in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id53out_o;

  { // Node ID: 53 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id53in_i = id47out_count;

    id53out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id53in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 686 (NodeRAM)
    const bool id686_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id686in_addrA = id641out_o;
    const HWFloat<8,40> &id686in_dina = id875out_output[getCycle()%50];
    const HWOffsetFix<1,0,UNSIGNED> &id686in_wea = id731out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id686in_addrB = id53out_o;

    long id686x_1;
    long id686x_2;
    HWFloat<8,40> id686x_3;

    (id686x_1) = (id686in_addrA.getValueAsLong());
    (id686x_2) = (id686in_addrB.getValueAsLong());
    switch(((long)((id686x_2)<(10000000l)))) {
      case 0l:
        id686x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id686x_3 = (id686sta_ram_store[(id686x_2)]);
        break;
      default:
        id686x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id686out_doutb[(getCycle()+2)%3] = (id686x_3);
    if(((id686in_wea.getValueAsBool())&id686_inputvalid)) {
      if(((id686x_1)<(10000000l))) {
        (id686sta_ram_store[(id686x_1)]) = id686in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_5("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 686) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 70 (NodeConstantRawBits)
  }
  { // Node ID: 71 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id71in_sel = id730out_result[getCycle()%2];
    const HWFloat<8,40> &id71in_option0 = id686out_doutb[getCycle()%3];
    const HWFloat<8,40> &id71in_option1 = id70out_value;

    HWFloat<8,40> id71x_1;

    switch((id71in_sel.getValueAsLong())) {
      case 0l:
        id71x_1 = id71in_option0;
        break;
      case 1l:
        id71x_1 = id71in_option1;
        break;
      default:
        id71x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id71out_result[(getCycle()+1)%2] = (id71x_1);
  }
  { // Node ID: 876 (NodeFIFO)
    const HWFloat<8,40> &id876in_input = id71out_result[getCycle()%2];

    id876out_output[(getCycle()+148)%149] = id876in_input;
  }
  { // Node ID: 76 (NodeAdd)
    const HWFloat<8,40> &id76in_a = id876out_output[getCycle()%149];
    const HWFloat<8,40> &id76in_b = id37out_o[getCycle()%4];

    id76out_result[(getCycle()+12)%13] = (add_float(id76in_a,id76in_b));
  }
  { // Node ID: 878 (NodeFIFO)
    const HWFloat<8,40> &id878in_input = id76out_result[getCycle()%13];

    id878out_output[(getCycle()+6)%7] = id878in_input;
  }
  { // Node ID: 935 (NodeFIFO)
    const HWFloat<8,40> &id935in_input = id878out_output[getCycle()%7];

    id935out_output[(getCycle()+1)%2] = id935in_input;
  }
  { // Node ID: 968 (NodeConstantRawBits)
  }
  { // Node ID: 86 (NodeEq)
    const HWFloat<8,40> &id86in_a = id935out_output[getCycle()%2];
    const HWFloat<8,40> &id86in_b = id968out_value;

    id86out_result[(getCycle()+1)%2] = (eq_float(id86in_a,id86in_b));
  }
  { // Node ID: 88 (NodeConstantRawBits)
  }
  { // Node ID: 87 (NodeConstantRawBits)
  }
  { // Node ID: 89 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id89in_sel = id86out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id89in_option0 = id88out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id89in_option1 = id87out_value;

    HWOffsetFix<1,0,UNSIGNED> id89x_1;

    switch((id89in_sel.getValueAsLong())) {
      case 0l:
        id89x_1 = id89in_option0;
        break;
      case 1l:
        id89x_1 = id89in_option1;
        break;
      default:
        id89x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id89out_result[(getCycle()+1)%2] = (id89x_1);
  }
  { // Node ID: 967 (NodeConstantRawBits)
  }
  { // Node ID: 81 (NodeLt)
    const HWFloat<8,40> &id81in_a = id878out_output[getCycle()%7];
    const HWFloat<8,40> &id81in_b = id967out_value;

    id81out_result[(getCycle()+1)%2] = (lt_float(id81in_a,id81in_b));
  }
  { // Node ID: 83 (NodeConstantRawBits)
  }
  { // Node ID: 82 (NodeConstantRawBits)
  }
  { // Node ID: 84 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id84in_sel = id81out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id84in_option0 = id83out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id84in_option1 = id82out_value;

    HWOffsetFix<1,0,UNSIGNED> id84x_1;

    switch((id84in_sel.getValueAsLong())) {
      case 0l:
        id84x_1 = id84in_option0;
        break;
      case 1l:
        id84x_1 = id84in_option1;
        break;
      default:
        id84x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id84out_result[(getCycle()+1)%2] = (id84x_1);
  }
  { // Node ID: 91 (NodeConstantRawBits)
  }
  { // Node ID: 90 (NodeConstantRawBits)
  }
  { // Node ID: 92 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id92in_sel = id84out_result[getCycle()%2];
    const HWFloat<8,40> &id92in_option0 = id91out_value;
    const HWFloat<8,40> &id92in_option1 = id90out_value;

    HWFloat<8,40> id92x_1;

    switch((id92in_sel.getValueAsLong())) {
      case 0l:
        id92x_1 = id92in_option0;
        break;
      case 1l:
        id92x_1 = id92in_option1;
        break;
      default:
        id92x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id92out_result[(getCycle()+1)%2] = (id92x_1);
  }
  { // Node ID: 93 (NodeConstantRawBits)
  }
  { // Node ID: 94 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id94in_sel = id89out_result[getCycle()%2];
    const HWFloat<8,40> &id94in_option0 = id92out_result[getCycle()%2];
    const HWFloat<8,40> &id94in_option1 = id93out_value;

    HWFloat<8,40> id94x_1;

    switch((id94in_sel.getValueAsLong())) {
      case 0l:
        id94x_1 = id94in_option0;
        break;
      case 1l:
        id94x_1 = id94in_option1;
        break;
      default:
        id94x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id94out_result[(getCycle()+1)%2] = (id94x_1);
  }
  { // Node ID: 966 (NodeConstantRawBits)
  }
  { // Node ID: 965 (NodeConstantRawBits)
  }
  { // Node ID: 732 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id732in_a = id46out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id732in_b = id965out_value;

    id732out_result[(getCycle()+1)%2] = (eq_fixed(id732in_a,id732in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id645out_o;

  { // Node ID: 645 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id645in_i = id778out_output[getCycle()%28];

    id645out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id645in_i));
  }
  { // Node ID: 880 (NodeFIFO)
    const HWFloat<8,40> &id880in_input = id77out_result[getCycle()%13];

    id880out_output[(getCycle()+6)%7] = id880in_input;
  }
  HWFloat<8,40> id773out_output;

  { // Node ID: 773 (NodeStreamOffset)
    const HWFloat<8,40> &id773in_input = id880out_output[getCycle()%7];

    id773out_output = id773in_input;
  }
  { // Node ID: 881 (NodeFIFO)
    const HWFloat<8,40> &id881in_input = id773out_output;

    id881out_output[(getCycle()+56)%57] = id881in_input;
  }
  { // Node ID: 964 (NodeConstantRawBits)
  }
  { // Node ID: 647 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id647in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id647in_b = id964out_value;

    id647out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id647in_a,id647in_b));
  }
  { // Node ID: 733 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id733in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id733in_b = id647out_result[getCycle()%2];

    id733out_result[(getCycle()+1)%2] = (eq_fixed(id733in_a,id733in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id54out_o;

  { // Node ID: 54 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id54in_i = id47out_count;

    id54out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id54in_i));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 687 (NodeRAM)
    const bool id687_inputvalid = !(isFlushingActive() && getFlushLevel() >= (4l));
    const HWOffsetFix<24,0,UNSIGNED> &id687in_addrA = id645out_o;
    const HWFloat<8,40> &id687in_dina = id881out_output[getCycle()%57];
    const HWOffsetFix<1,0,UNSIGNED> &id687in_wea = id733out_result[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id687in_addrB = id54out_o;

    long id687x_1;
    long id687x_2;
    HWFloat<8,40> id687x_3;

    (id687x_1) = (id687in_addrA.getValueAsLong());
    (id687x_2) = (id687in_addrB.getValueAsLong());
    switch(((long)((id687x_2)<(10000000l)))) {
      case 0l:
        id687x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id687x_3 = (id687sta_ram_store[(id687x_2)]);
        break;
      default:
        id687x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id687out_doutb[(getCycle()+2)%3] = (id687x_3);
    if(((id687in_wea.getValueAsBool())&id687_inputvalid)) {
      if(((id687x_1)<(10000000l))) {
        (id687sta_ram_store[(id687x_1)]) = id687in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_6("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 687) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 74 (NodeConstantRawBits)
  }
  { // Node ID: 75 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id75in_sel = id732out_result[getCycle()%2];
    const HWFloat<8,40> &id75in_option0 = id687out_doutb[getCycle()%3];
    const HWFloat<8,40> &id75in_option1 = id74out_value;

    HWFloat<8,40> id75x_1;

    switch((id75in_sel.getValueAsLong())) {
      case 0l:
        id75x_1 = id75in_option0;
        break;
      case 1l:
        id75x_1 = id75in_option1;
        break;
      default:
        id75x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id75out_result[(getCycle()+1)%2] = (id75x_1);
  }
  { // Node ID: 882 (NodeFIFO)
    const HWFloat<8,40> &id882in_input = id75out_result[getCycle()%2];

    id882out_output[(getCycle()+142)%143] = id882in_input;
  }
  { // Node ID: 77 (NodeAdd)
    const HWFloat<8,40> &id77in_a = id882out_output[getCycle()%143];
    const HWFloat<8,40> &id77in_b = id37out_o[getCycle()%4];

    id77out_result[(getCycle()+12)%13] = (add_float(id77in_a,id77in_b));
  }
  { // Node ID: 963 (NodeConstantRawBits)
  }
  { // Node ID: 96 (NodeSub)
    const HWFloat<8,40> &id96in_a = id77out_result[getCycle()%13];
    const HWFloat<8,40> &id96in_b = id963out_value;

    id96out_result[(getCycle()+6)%7] = (sub_float(id96in_a,id96in_b));
  }
  { // Node ID: 962 (NodeConstantRawBits)
  }
  { // Node ID: 103 (NodeEq)
    const HWFloat<8,40> &id103in_a = id96out_result[getCycle()%7];
    const HWFloat<8,40> &id103in_b = id962out_value;

    id103out_result[(getCycle()+1)%2] = (eq_float(id103in_a,id103in_b));
  }
  { // Node ID: 105 (NodeConstantRawBits)
  }
  { // Node ID: 104 (NodeConstantRawBits)
  }
  { // Node ID: 106 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id106in_sel = id103out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id106in_option0 = id105out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id106in_option1 = id104out_value;

    HWOffsetFix<1,0,UNSIGNED> id106x_1;

    switch((id106in_sel.getValueAsLong())) {
      case 0l:
        id106x_1 = id106in_option0;
        break;
      case 1l:
        id106x_1 = id106in_option1;
        break;
      default:
        id106x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id106out_result[(getCycle()+1)%2] = (id106x_1);
  }
  { // Node ID: 883 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id883in_input = id106out_result[getCycle()%2];

    id883out_output[(getCycle()+1)%2] = id883in_input;
  }
  { // Node ID: 961 (NodeConstantRawBits)
  }
  { // Node ID: 98 (NodeLt)
    const HWFloat<8,40> &id98in_a = id96out_result[getCycle()%7];
    const HWFloat<8,40> &id98in_b = id961out_value;

    id98out_result[(getCycle()+1)%2] = (lt_float(id98in_a,id98in_b));
  }
  { // Node ID: 100 (NodeConstantRawBits)
  }
  { // Node ID: 99 (NodeConstantRawBits)
  }
  { // Node ID: 101 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id101in_sel = id98out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id101in_option0 = id100out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id101in_option1 = id99out_value;

    HWOffsetFix<1,0,UNSIGNED> id101x_1;

    switch((id101in_sel.getValueAsLong())) {
      case 0l:
        id101x_1 = id101in_option0;
        break;
      case 1l:
        id101x_1 = id101in_option1;
        break;
      default:
        id101x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id101out_result[(getCycle()+1)%2] = (id101x_1);
  }
  { // Node ID: 108 (NodeConstantRawBits)
  }
  { // Node ID: 107 (NodeConstantRawBits)
  }
  { // Node ID: 109 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id109in_sel = id101out_result[getCycle()%2];
    const HWFloat<8,40> &id109in_option0 = id108out_value;
    const HWFloat<8,40> &id109in_option1 = id107out_value;

    HWFloat<8,40> id109x_1;

    switch((id109in_sel.getValueAsLong())) {
      case 0l:
        id109x_1 = id109in_option0;
        break;
      case 1l:
        id109x_1 = id109in_option1;
        break;
      default:
        id109x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id109out_result[(getCycle()+1)%2] = (id109x_1);
  }
  { // Node ID: 110 (NodeConstantRawBits)
  }
  { // Node ID: 111 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_sel = id883out_output[getCycle()%2];
    const HWFloat<8,40> &id111in_option0 = id109out_result[getCycle()%2];
    const HWFloat<8,40> &id111in_option1 = id110out_value;

    HWFloat<8,40> id111x_1;

    switch((id111in_sel.getValueAsLong())) {
      case 0l:
        id111x_1 = id111in_option0;
        break;
      case 1l:
        id111x_1 = id111in_option1;
        break;
      default:
        id111x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id111out_result[(getCycle()+1)%2] = (id111x_1);
  }
  { // Node ID: 113 (NodeSub)
    const HWFloat<8,40> &id113in_a = id966out_value;
    const HWFloat<8,40> &id113in_b = id111out_result[getCycle()%2];

    id113out_result[(getCycle()+6)%7] = (sub_float(id113in_a,id113in_b));
  }
  { // Node ID: 114 (NodeMul)
    const HWFloat<8,40> &id114in_a = id94out_result[getCycle()%2];
    const HWFloat<8,40> &id114in_b = id113out_result[getCycle()%7];

    id114out_result[(getCycle()+6)%7] = (mul_float(id114in_a,id114in_b));
  }
  { // Node ID: 960 (NodeConstantRawBits)
  }
  { // Node ID: 116 (NodeSub)
    const HWFloat<8,40> &id116in_a = id76out_result[getCycle()%13];
    const HWFloat<8,40> &id116in_b = id960out_value;

    id116out_result[(getCycle()+6)%7] = (sub_float(id116in_a,id116in_b));
  }
  { // Node ID: 959 (NodeConstantRawBits)
  }
  { // Node ID: 123 (NodeEq)
    const HWFloat<8,40> &id123in_a = id116out_result[getCycle()%7];
    const HWFloat<8,40> &id123in_b = id959out_value;

    id123out_result[(getCycle()+1)%2] = (eq_float(id123in_a,id123in_b));
  }
  { // Node ID: 125 (NodeConstantRawBits)
  }
  { // Node ID: 124 (NodeConstantRawBits)
  }
  { // Node ID: 126 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id126in_sel = id123out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id126in_option0 = id125out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id126in_option1 = id124out_value;

    HWOffsetFix<1,0,UNSIGNED> id126x_1;

    switch((id126in_sel.getValueAsLong())) {
      case 0l:
        id126x_1 = id126in_option0;
        break;
      case 1l:
        id126x_1 = id126in_option1;
        break;
      default:
        id126x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id126out_result[(getCycle()+1)%2] = (id126x_1);
  }
  { // Node ID: 884 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id884in_input = id126out_result[getCycle()%2];

    id884out_output[(getCycle()+1)%2] = id884in_input;
  }
  { // Node ID: 958 (NodeConstantRawBits)
  }
  { // Node ID: 118 (NodeLt)
    const HWFloat<8,40> &id118in_a = id116out_result[getCycle()%7];
    const HWFloat<8,40> &id118in_b = id958out_value;

    id118out_result[(getCycle()+1)%2] = (lt_float(id118in_a,id118in_b));
  }
  { // Node ID: 120 (NodeConstantRawBits)
  }
  { // Node ID: 119 (NodeConstantRawBits)
  }
  { // Node ID: 121 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id121in_sel = id118out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id121in_option0 = id120out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id121in_option1 = id119out_value;

    HWOffsetFix<1,0,UNSIGNED> id121x_1;

    switch((id121in_sel.getValueAsLong())) {
      case 0l:
        id121x_1 = id121in_option0;
        break;
      case 1l:
        id121x_1 = id121in_option1;
        break;
      default:
        id121x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id121out_result[(getCycle()+1)%2] = (id121x_1);
  }
  { // Node ID: 128 (NodeConstantRawBits)
  }
  { // Node ID: 127 (NodeConstantRawBits)
  }
  { // Node ID: 129 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id129in_sel = id121out_result[getCycle()%2];
    const HWFloat<8,40> &id129in_option0 = id128out_value;
    const HWFloat<8,40> &id129in_option1 = id127out_value;

    HWFloat<8,40> id129x_1;

    switch((id129in_sel.getValueAsLong())) {
      case 0l:
        id129x_1 = id129in_option0;
        break;
      case 1l:
        id129x_1 = id129in_option1;
        break;
      default:
        id129x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id129out_result[(getCycle()+1)%2] = (id129x_1);
  }
  { // Node ID: 130 (NodeConstantRawBits)
  }
  { // Node ID: 131 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id131in_sel = id884out_output[getCycle()%2];
    const HWFloat<8,40> &id131in_option0 = id129out_result[getCycle()%2];
    const HWFloat<8,40> &id131in_option1 = id130out_value;

    HWFloat<8,40> id131x_1;

    switch((id131in_sel.getValueAsLong())) {
      case 0l:
        id131x_1 = id131in_option0;
        break;
      case 1l:
        id131x_1 = id131in_option1;
        break;
      default:
        id131x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id131out_result[(getCycle()+1)%2] = (id131x_1);
  }
  { // Node ID: 957 (NodeConstantRawBits)
  }
  { // Node ID: 956 (NodeConstantRawBits)
  }
  { // Node ID: 133 (NodeSub)
    const HWFloat<8,40> &id133in_a = id77out_result[getCycle()%13];
    const HWFloat<8,40> &id133in_b = id956out_value;

    id133out_result[(getCycle()+6)%7] = (sub_float(id133in_a,id133in_b));
  }
  { // Node ID: 955 (NodeConstantRawBits)
  }
  { // Node ID: 140 (NodeEq)
    const HWFloat<8,40> &id140in_a = id133out_result[getCycle()%7];
    const HWFloat<8,40> &id140in_b = id955out_value;

    id140out_result[(getCycle()+1)%2] = (eq_float(id140in_a,id140in_b));
  }
  { // Node ID: 142 (NodeConstantRawBits)
  }
  { // Node ID: 141 (NodeConstantRawBits)
  }
  { // Node ID: 143 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id143in_sel = id140out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id143in_option0 = id142out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id143in_option1 = id141out_value;

    HWOffsetFix<1,0,UNSIGNED> id143x_1;

    switch((id143in_sel.getValueAsLong())) {
      case 0l:
        id143x_1 = id143in_option0;
        break;
      case 1l:
        id143x_1 = id143in_option1;
        break;
      default:
        id143x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id143out_result[(getCycle()+1)%2] = (id143x_1);
  }
  { // Node ID: 885 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id885in_input = id143out_result[getCycle()%2];

    id885out_output[(getCycle()+1)%2] = id885in_input;
  }
  { // Node ID: 954 (NodeConstantRawBits)
  }
  { // Node ID: 135 (NodeLt)
    const HWFloat<8,40> &id135in_a = id133out_result[getCycle()%7];
    const HWFloat<8,40> &id135in_b = id954out_value;

    id135out_result[(getCycle()+1)%2] = (lt_float(id135in_a,id135in_b));
  }
  { // Node ID: 137 (NodeConstantRawBits)
  }
  { // Node ID: 136 (NodeConstantRawBits)
  }
  { // Node ID: 138 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id138in_sel = id135out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id138in_option0 = id137out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id138in_option1 = id136out_value;

    HWOffsetFix<1,0,UNSIGNED> id138x_1;

    switch((id138in_sel.getValueAsLong())) {
      case 0l:
        id138x_1 = id138in_option0;
        break;
      case 1l:
        id138x_1 = id138in_option1;
        break;
      default:
        id138x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id138out_result[(getCycle()+1)%2] = (id138x_1);
  }
  { // Node ID: 145 (NodeConstantRawBits)
  }
  { // Node ID: 144 (NodeConstantRawBits)
  }
  { // Node ID: 146 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id146in_sel = id138out_result[getCycle()%2];
    const HWFloat<8,40> &id146in_option0 = id145out_value;
    const HWFloat<8,40> &id146in_option1 = id144out_value;

    HWFloat<8,40> id146x_1;

    switch((id146in_sel.getValueAsLong())) {
      case 0l:
        id146x_1 = id146in_option0;
        break;
      case 1l:
        id146x_1 = id146in_option1;
        break;
      default:
        id146x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id146out_result[(getCycle()+1)%2] = (id146x_1);
  }
  { // Node ID: 147 (NodeConstantRawBits)
  }
  { // Node ID: 148 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id148in_sel = id885out_output[getCycle()%2];
    const HWFloat<8,40> &id148in_option0 = id146out_result[getCycle()%2];
    const HWFloat<8,40> &id148in_option1 = id147out_value;

    HWFloat<8,40> id148x_1;

    switch((id148in_sel.getValueAsLong())) {
      case 0l:
        id148x_1 = id148in_option0;
        break;
      case 1l:
        id148x_1 = id148in_option1;
        break;
      default:
        id148x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id148out_result[(getCycle()+1)%2] = (id148x_1);
  }
  { // Node ID: 150 (NodeSub)
    const HWFloat<8,40> &id150in_a = id957out_value;
    const HWFloat<8,40> &id150in_b = id148out_result[getCycle()%2];

    id150out_result[(getCycle()+6)%7] = (sub_float(id150in_a,id150in_b));
  }
  { // Node ID: 151 (NodeMul)
    const HWFloat<8,40> &id151in_a = id131out_result[getCycle()%2];
    const HWFloat<8,40> &id151in_b = id150out_result[getCycle()%7];

    id151out_result[(getCycle()+6)%7] = (mul_float(id151in_a,id151in_b));
  }
  { // Node ID: 152 (NodeAdd)
    const HWFloat<8,40> &id152in_a = id114out_result[getCycle()%7];
    const HWFloat<8,40> &id152in_b = id151out_result[getCycle()%7];

    id152out_result[(getCycle()+6)%7] = (add_float(id152in_a,id152in_b));
  }
  { // Node ID: 953 (NodeConstantRawBits)
  }
  { // Node ID: 154 (NodeSub)
    const HWFloat<8,40> &id154in_a = id878out_output[getCycle()%7];
    const HWFloat<8,40> &id154in_b = id953out_value;

    id154out_result[(getCycle()+6)%7] = (sub_float(id154in_a,id154in_b));
  }
  { // Node ID: 952 (NodeConstantRawBits)
  }
  { // Node ID: 161 (NodeEq)
    const HWFloat<8,40> &id161in_a = id154out_result[getCycle()%7];
    const HWFloat<8,40> &id161in_b = id952out_value;

    id161out_result[(getCycle()+1)%2] = (eq_float(id161in_a,id161in_b));
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  { // Node ID: 162 (NodeConstantRawBits)
  }
  { // Node ID: 164 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id164in_sel = id161out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id164in_option0 = id163out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id164in_option1 = id162out_value;

    HWOffsetFix<1,0,UNSIGNED> id164x_1;

    switch((id164in_sel.getValueAsLong())) {
      case 0l:
        id164x_1 = id164in_option0;
        break;
      case 1l:
        id164x_1 = id164in_option1;
        break;
      default:
        id164x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id164out_result[(getCycle()+1)%2] = (id164x_1);
  }
  { // Node ID: 887 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id887in_input = id164out_result[getCycle()%2];

    id887out_output[(getCycle()+1)%2] = id887in_input;
  }
  { // Node ID: 951 (NodeConstantRawBits)
  }
  { // Node ID: 156 (NodeLt)
    const HWFloat<8,40> &id156in_a = id154out_result[getCycle()%7];
    const HWFloat<8,40> &id156in_b = id951out_value;

    id156out_result[(getCycle()+1)%2] = (lt_float(id156in_a,id156in_b));
  }
  { // Node ID: 158 (NodeConstantRawBits)
  }
  { // Node ID: 157 (NodeConstantRawBits)
  }
  { // Node ID: 159 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id159in_sel = id156out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id159in_option0 = id158out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id159in_option1 = id157out_value;

    HWOffsetFix<1,0,UNSIGNED> id159x_1;

    switch((id159in_sel.getValueAsLong())) {
      case 0l:
        id159x_1 = id159in_option0;
        break;
      case 1l:
        id159x_1 = id159in_option1;
        break;
      default:
        id159x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id159out_result[(getCycle()+1)%2] = (id159x_1);
  }
  { // Node ID: 166 (NodeConstantRawBits)
  }
  { // Node ID: 165 (NodeConstantRawBits)
  }
  { // Node ID: 167 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id167in_sel = id159out_result[getCycle()%2];
    const HWFloat<8,40> &id167in_option0 = id166out_value;
    const HWFloat<8,40> &id167in_option1 = id165out_value;

    HWFloat<8,40> id167x_1;

    switch((id167in_sel.getValueAsLong())) {
      case 0l:
        id167x_1 = id167in_option0;
        break;
      case 1l:
        id167x_1 = id167in_option1;
        break;
      default:
        id167x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id167out_result[(getCycle()+1)%2] = (id167x_1);
  }
  { // Node ID: 168 (NodeConstantRawBits)
  }
  { // Node ID: 169 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id169in_sel = id887out_output[getCycle()%2];
    const HWFloat<8,40> &id169in_option0 = id167out_result[getCycle()%2];
    const HWFloat<8,40> &id169in_option1 = id168out_value;

    HWFloat<8,40> id169x_1;

    switch((id169in_sel.getValueAsLong())) {
      case 0l:
        id169x_1 = id169in_option0;
        break;
      case 1l:
        id169x_1 = id169in_option1;
        break;
      default:
        id169x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id169out_result[(getCycle()+1)%2] = (id169x_1);
  }
  { // Node ID: 950 (NodeConstantRawBits)
  }
  { // Node ID: 949 (NodeConstantRawBits)
  }
  { // Node ID: 171 (NodeSub)
    const HWFloat<8,40> &id171in_a = id880out_output[getCycle()%7];
    const HWFloat<8,40> &id171in_b = id949out_value;

    id171out_result[(getCycle()+6)%7] = (sub_float(id171in_a,id171in_b));
  }
  { // Node ID: 948 (NodeConstantRawBits)
  }
  { // Node ID: 178 (NodeEq)
    const HWFloat<8,40> &id178in_a = id171out_result[getCycle()%7];
    const HWFloat<8,40> &id178in_b = id948out_value;

    id178out_result[(getCycle()+1)%2] = (eq_float(id178in_a,id178in_b));
  }
  { // Node ID: 180 (NodeConstantRawBits)
  }
  { // Node ID: 179 (NodeConstantRawBits)
  }
  { // Node ID: 181 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id181in_sel = id178out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id181in_option0 = id180out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id181in_option1 = id179out_value;

    HWOffsetFix<1,0,UNSIGNED> id181x_1;

    switch((id181in_sel.getValueAsLong())) {
      case 0l:
        id181x_1 = id181in_option0;
        break;
      case 1l:
        id181x_1 = id181in_option1;
        break;
      default:
        id181x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id181out_result[(getCycle()+1)%2] = (id181x_1);
  }
  { // Node ID: 889 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id889in_input = id181out_result[getCycle()%2];

    id889out_output[(getCycle()+1)%2] = id889in_input;
  }
  { // Node ID: 947 (NodeConstantRawBits)
  }
  { // Node ID: 173 (NodeLt)
    const HWFloat<8,40> &id173in_a = id171out_result[getCycle()%7];
    const HWFloat<8,40> &id173in_b = id947out_value;

    id173out_result[(getCycle()+1)%2] = (lt_float(id173in_a,id173in_b));
  }
  { // Node ID: 175 (NodeConstantRawBits)
  }
  { // Node ID: 174 (NodeConstantRawBits)
  }
  { // Node ID: 176 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id176in_sel = id173out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id176in_option0 = id175out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id176in_option1 = id174out_value;

    HWOffsetFix<1,0,UNSIGNED> id176x_1;

    switch((id176in_sel.getValueAsLong())) {
      case 0l:
        id176x_1 = id176in_option0;
        break;
      case 1l:
        id176x_1 = id176in_option1;
        break;
      default:
        id176x_1 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id176out_result[(getCycle()+1)%2] = (id176x_1);
  }
  { // Node ID: 183 (NodeConstantRawBits)
  }
  { // Node ID: 182 (NodeConstantRawBits)
  }
  { // Node ID: 184 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id184in_sel = id176out_result[getCycle()%2];
    const HWFloat<8,40> &id184in_option0 = id183out_value;
    const HWFloat<8,40> &id184in_option1 = id182out_value;

    HWFloat<8,40> id184x_1;

    switch((id184in_sel.getValueAsLong())) {
      case 0l:
        id184x_1 = id184in_option0;
        break;
      case 1l:
        id184x_1 = id184in_option1;
        break;
      default:
        id184x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id184out_result[(getCycle()+1)%2] = (id184x_1);
  }
  { // Node ID: 185 (NodeConstantRawBits)
  }
  { // Node ID: 186 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id186in_sel = id889out_output[getCycle()%2];
    const HWFloat<8,40> &id186in_option0 = id184out_result[getCycle()%2];
    const HWFloat<8,40> &id186in_option1 = id185out_value;

    HWFloat<8,40> id186x_1;

    switch((id186in_sel.getValueAsLong())) {
      case 0l:
        id186x_1 = id186in_option0;
        break;
      case 1l:
        id186x_1 = id186in_option1;
        break;
      default:
        id186x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id186out_result[(getCycle()+1)%2] = (id186x_1);
  }
  { // Node ID: 188 (NodeSub)
    const HWFloat<8,40> &id188in_a = id950out_value;
    const HWFloat<8,40> &id188in_b = id186out_result[getCycle()%2];

    id188out_result[(getCycle()+6)%7] = (sub_float(id188in_a,id188in_b));
  }
  { // Node ID: 189 (NodeMul)
    const HWFloat<8,40> &id189in_a = id169out_result[getCycle()%2];
    const HWFloat<8,40> &id189in_b = id188out_result[getCycle()%7];

    id189out_result[(getCycle()+6)%7] = (mul_float(id189in_a,id189in_b));
  }
  { // Node ID: 190 (NodeAdd)
    const HWFloat<8,40> &id190in_a = id152out_result[getCycle()%7];
    const HWFloat<8,40> &id190in_b = id189out_result[getCycle()%7];

    id190out_result[(getCycle()+6)%7] = (add_float(id190in_a,id190in_b));
  }
  { // Node ID: 946 (NodeConstantRawBits)
  }
  { // Node ID: 192 (NodeEq)
    const HWFloat<8,40> &id192in_a = id190out_result[getCycle()%7];
    const HWFloat<8,40> &id192in_b = id946out_value;

    id192out_result[(getCycle()+2)%3] = (eq_float(id192in_a,id192in_b));
  }
  { // Node ID: 945 (NodeConstantRawBits)
  }
  { // Node ID: 734 (NodeLtInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id734in_a = id912out_output[getCycle()%171];
    const HWOffsetFix<32,0,UNSIGNED> &id734in_b = id945out_value;

    id734out_result[(getCycle()+1)%2] = (lt_fixed(id734in_a,id734in_b));
  }
  { // Node ID: 195 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id195in_a = id192out_result[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id195in_b = id734out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id195x_1;

    (id195x_1) = (and_fixed(id195in_a,id195in_b));
    id195out_result[(getCycle()+1)%2] = (id195x_1);
  }
  { // Node ID: 197 (NodeConstantRawBits)
  }
  { // Node ID: 196 (NodeConstantRawBits)
  }
  { // Node ID: 198 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id198in_sel = id195out_result[getCycle()%2];
    const HWFloat<8,40> &id198in_option0 = id197out_value;
    const HWFloat<8,40> &id198in_option1 = id196out_value;

    HWFloat<8,40> id198x_1;

    switch((id198in_sel.getValueAsLong())) {
      case 0l:
        id198x_1 = id198in_option0;
        break;
      case 1l:
        id198x_1 = id198in_option1;
        break;
      default:
        id198x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id198out_result[(getCycle()+1)%2] = (id198x_1);
  }
  { // Node ID: 614 (NodeSub)
    const HWFloat<8,40> &id614in_a = id613out_result[getCycle()%7];
    const HWFloat<8,40> &id614in_b = id198out_result[getCycle()%2];

    id614out_result[(getCycle()+6)%7] = (sub_float(id614in_a,id614in_b));
  }
  { // Node ID: 615 (NodeMul)
    const HWFloat<8,40> &id615in_a = id614out_result[getCycle()%7];
    const HWFloat<8,40> &id615in_b = id37out_o[getCycle()%4];

    id615out_result[(getCycle()+6)%7] = (mul_float(id615in_a,id615in_b));
  }
  { // Node ID: 616 (NodeSub)
    const HWFloat<8,40> &id616in_a = id926out_output[getCycle()%21];
    const HWFloat<8,40> &id616in_b = id615out_result[getCycle()%7];

    id616out_result[(getCycle()+6)%7] = (sub_float(id616in_a,id616in_b));
  }
  { // Node ID: 944 (NodeConstantRawBits)
  }
  { // Node ID: 735 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id735in_a = id911out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id735in_b = id944out_value;

    id735out_result[(getCycle()+1)%2] = (eq_fixed(id735in_a,id735in_b));
  }
  { // Node ID: 943 (NodeConstantRawBits)
  }
  { // Node ID: 211 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id211in_a = id40out_nx;
    const HWOffsetFix<32,0,UNSIGNED> &id211in_b = id943out_value;

    id211out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAREVEN>(id211in_a,id211in_b));
  }
  { // Node ID: 736 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id736in_a = id893out_output[getCycle()%22];
    const HWOffsetFix<32,0,UNSIGNED> &id736in_b = id211out_result[getCycle()%2];

    id736out_result[(getCycle()+1)%2] = (eq_fixed(id736in_a,id736in_b));
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  { // Node ID: 39 (NodeCast)
    const HWFloat<11,53> &id39in_i = id38out_ddt_o_dx2;

    id39out_o[(getCycle()+3)%4] = (cast_float2float<8,40>(id39in_i));
  }
  { // Node ID: 894 (NodeFIFO)
    const HWFloat<8,40> &id894in_input = id768out_output;

    id894out_output[(getCycle()+1)%2] = id894in_input;
  }
  HWFloat<8,40> id199out_output;

  { // Node ID: 199 (NodeStreamOffset)
    const HWFloat<8,40> &id199in_input = id894out_output[getCycle()%2];

    id199out_output = id199in_input;
  }
  { // Node ID: 899 (NodeFIFO)
    const HWFloat<8,40> &id899in_input = id199out_output;

    id899out_output[(getCycle()+4)%5] = id899in_input;
  }
  { // Node ID: 895 (NodeFIFO)
    const HWOffsetFix<24,0,UNSIGNED> &id895in_input = id625out_o;

    id895out_output[(getCycle()+1)%2] = id895in_input;
  }
  { // Node ID: 897 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id897in_input = id705out_result[getCycle()%2];

    id897out_output[(getCycle()+1)%2] = id897in_input;
  }
  { // Node ID: 942 (NodeConstantRawBits)
  }
  { // Node ID: 56 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id56in_a = id47out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id56in_b = id942out_value;

    id56out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAREVEN>(id56in_a,id56in_b));
  }
  HWOffsetFix<24,0,UNSIGNED> id57out_o;

  { // Node ID: 57 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id57in_i = id56out_result[getCycle()%2];

    id57out_o = (cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(id57in_i));
  }
  if ( (getFillLevel() >= (5l)))
  { // Node ID: 682 (NodeRAM)
    const bool id682_inputvalid = !(isFlushingActive() && getFlushLevel() >= (5l));
    const HWOffsetFix<24,0,UNSIGNED> &id682in_addrA = id895out_output[getCycle()%2];
    const HWFloat<8,40> &id682in_dina = id894out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id682in_wea = id897out_output[getCycle()%2];
    const HWOffsetFix<24,0,UNSIGNED> &id682in_addrB = id57out_o;

    long id682x_1;
    long id682x_2;
    HWFloat<8,40> id682x_3;

    (id682x_1) = (id682in_addrA.getValueAsLong());
    (id682x_2) = (id682in_addrB.getValueAsLong());
    switch(((long)((id682x_2)<(10000000l)))) {
      case 0l:
        id682x_3 = (c_hw_flt_8_40_undef);
        break;
      case 1l:
        id682x_3 = (id682sta_ram_store[(id682x_2)]);
        break;
      default:
        id682x_3 = (c_hw_flt_8_40_undef);
        break;
    }
    id682out_doutb[(getCycle()+2)%3] = (id682x_3);
    if(((id682in_wea.getValueAsBool())&id682_inputvalid)) {
      if(((id682x_1)<(10000000l))) {
        (id682sta_ram_store[(id682x_1)]) = id682in_dina;
      }
      else {
        throw std::runtime_error((format_string_CellCableDFE48Kernel_7("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 682) on port A, ram depth is 10000000.")));
      }
    }
  }
  { // Node ID: 200 (NodeConstantRawBits)
  }
  { // Node ID: 201 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id201in_sel = id783out_output[getCycle()%2];
    const HWFloat<8,40> &id201in_option0 = id682out_doutb[getCycle()%3];
    const HWFloat<8,40> &id201in_option1 = id200out_value;

    HWFloat<8,40> id201x_1;

    switch((id201in_sel.getValueAsLong())) {
      case 0l:
        id201x_1 = id201in_option0;
        break;
      case 1l:
        id201x_1 = id201in_option1;
        break;
      default:
        id201x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id201out_result[(getCycle()+1)%2] = (id201x_1);
  }
  { // Node ID: 219 (NodeAdd)
    const HWFloat<8,40> &id219in_a = id899out_output[getCycle()%5];
    const HWFloat<8,40> &id219in_b = id201out_result[getCycle()%2];

    id219out_result[(getCycle()+6)%7] = (add_float(id219in_a,id219in_b));
  }
  { // Node ID: 754 (NodePO2FPMult)
    const HWFloat<8,40> &id754in_floatIn = id913out_output[getCycle()%6];

    id754out_floatOut[(getCycle()+1)%2] = (mul_float(id754in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  { // Node ID: 222 (NodeSub)
    const HWFloat<8,40> &id222in_a = id219out_result[getCycle()%7];
    const HWFloat<8,40> &id222in_b = id754out_floatOut[getCycle()%2];

    id222out_result[(getCycle()+6)%7] = (sub_float(id222in_a,id222in_b));
  }
  { // Node ID: 223 (NodeMul)
    const HWFloat<8,40> &id223in_a = id39out_o[getCycle()%4];
    const HWFloat<8,40> &id223in_b = id222out_result[getCycle()%7];

    id223out_result[(getCycle()+6)%7] = (mul_float(id223in_a,id223in_b));
  }
  { // Node ID: 755 (NodePO2FPMult)
    const HWFloat<8,40> &id755in_floatIn = id901out_output[getCycle()%2];

    id755out_floatOut[(getCycle()+1)%2] = (mul_float(id755in_floatIn,(c_hw_flt_8_40_n2_0val)));
  }
  { // Node ID: 756 (NodePO2FPMult)
    const HWFloat<8,40> &id756in_floatIn = id899out_output[getCycle()%5];

    id756out_floatOut[(getCycle()+1)%2] = (mul_float(id756in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  { // Node ID: 217 (NodeAdd)
    const HWFloat<8,40> &id217in_a = id755out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id217in_b = id756out_floatOut[getCycle()%2];

    id217out_result[(getCycle()+6)%7] = (add_float(id217in_a,id217in_b));
  }
  { // Node ID: 218 (NodeMul)
    const HWFloat<8,40> &id218in_a = id39out_o[getCycle()%4];
    const HWFloat<8,40> &id218in_b = id217out_result[getCycle()%7];

    id218out_result[(getCycle()+6)%7] = (mul_float(id218in_a,id218in_b));
  }
  { // Node ID: 903 (NodeFIFO)
    const HWFloat<8,40> &id903in_input = id218out_result[getCycle()%7];

    id903out_output[(getCycle()+5)%6] = id903in_input;
  }
  { // Node ID: 224 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id224in_sel = id736out_result[getCycle()%2];
    const HWFloat<8,40> &id224in_option0 = id223out_result[getCycle()%7];
    const HWFloat<8,40> &id224in_option1 = id903out_output[getCycle()%6];

    HWFloat<8,40> id224x_1;

    switch((id224in_sel.getValueAsLong())) {
      case 0l:
        id224x_1 = id224in_option0;
        break;
      case 1l:
        id224x_1 = id224in_option1;
        break;
      default:
        id224x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id224out_result[(getCycle()+1)%2] = (id224x_1);
  }
  { // Node ID: 757 (NodePO2FPMult)
    const HWFloat<8,40> &id757in_floatIn = id901out_output[getCycle()%2];

    id757out_floatOut[(getCycle()+1)%2] = (mul_float(id757in_floatIn,(c_hw_flt_8_40_n2_0val)));
  }
  { // Node ID: 758 (NodePO2FPMult)
    const HWFloat<8,40> &id758in_floatIn = id201out_result[getCycle()%2];

    id758out_floatOut[(getCycle()+1)%2] = (mul_float(id758in_floatIn,(c_hw_flt_8_40_2_0val)));
  }
  { // Node ID: 208 (NodeAdd)
    const HWFloat<8,40> &id208in_a = id757out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id208in_b = id758out_floatOut[getCycle()%2];

    id208out_result[(getCycle()+6)%7] = (add_float(id208in_a,id208in_b));
  }
  { // Node ID: 209 (NodeMul)
    const HWFloat<8,40> &id209in_a = id39out_o[getCycle()%4];
    const HWFloat<8,40> &id209in_b = id208out_result[getCycle()%7];

    id209out_result[(getCycle()+6)%7] = (mul_float(id209in_a,id209in_b));
  }
  { // Node ID: 905 (NodeFIFO)
    const HWFloat<8,40> &id905in_input = id209out_result[getCycle()%7];

    id905out_output[(getCycle()+6)%7] = id905in_input;
  }
  { // Node ID: 225 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id225in_sel = id735out_result[getCycle()%2];
    const HWFloat<8,40> &id225in_option0 = id224out_result[getCycle()%2];
    const HWFloat<8,40> &id225in_option1 = id905out_output[getCycle()%7];

    HWFloat<8,40> id225x_1;

    switch((id225in_sel.getValueAsLong())) {
      case 0l:
        id225x_1 = id225in_option0;
        break;
      case 1l:
        id225x_1 = id225in_option1;
        break;
      default:
        id225x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id225out_result[(getCycle()+1)%2] = (id225x_1);
  }
  { // Node ID: 906 (NodeFIFO)
    const HWFloat<8,40> &id906in_input = id225out_result[getCycle()%2];

    id906out_output[(getCycle()+189)%190] = id906in_input;
  }
  { // Node ID: 617 (NodeAdd)
    const HWFloat<8,40> &id617in_a = id616out_result[getCycle()%7];
    const HWFloat<8,40> &id617in_b = id906out_output[getCycle()%190];

    id617out_result[(getCycle()+6)%7] = (add_float(id617in_a,id617in_b));
  }
  { // Node ID: 649 (NodeCast)
    const HWFloat<8,40> &id649in_i = id617out_result[getCycle()%7];

    id649out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id649in_i));
  }
  if ( (getFillLevel() >= (226l)) && (getFlushLevel() < (226l)|| !isFlushingActive() ))
  { // Node ID: 659 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id659in_output_control = id907out_output[getCycle()%223];
    const HWFloat<8,24> &id659in_data = id649out_o[getCycle()%4];

    bool id659x_1;

    (id659x_1) = ((id659in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(226l))&(isFlushingActive()))));
    if((id659x_1)) {
      writeOutput(m_u_out, id659in_data);
    }
  }
  { // Node ID: 941 (NodeConstantRawBits)
  }
  { // Node ID: 661 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id661in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id661in_b = id941out_value;

    id661out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id661in_a,id661in_b));
  }
  { // Node ID: 737 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id737in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id737in_b = id661out_result[getCycle()%2];

    id737out_result[(getCycle()+1)%2] = (eq_fixed(id737in_a,id737in_b));
  }
  { // Node ID: 663 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id664out_result;

  { // Node ID: 664 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_a = id663out_io_v_out_force_disabled;

    id664out_result = (not_fixed(id664in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id665out_result;

  { // Node ID: 665 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id665in_a = id737out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id665in_b = id664out_result;

    HWOffsetFix<1,0,UNSIGNED> id665x_1;

    (id665x_1) = (and_fixed(id665in_a,id665in_b));
    id665out_result = (id665x_1);
  }
  { // Node ID: 908 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id908in_input = id665out_result;

    id908out_output[(getCycle()+63)%64] = id908in_input;
  }
  { // Node ID: 650 (NodeCast)
    const HWFloat<8,40> &id650in_i = id587out_result[getCycle()%13];

    id650out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id650in_i));
  }
  if ( (getFillLevel() >= (67l)) && (getFlushLevel() < (67l)|| !isFlushingActive() ))
  { // Node ID: 666 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id666in_output_control = id908out_output[getCycle()%64];
    const HWFloat<8,24> &id666in_data = id650out_o[getCycle()%4];

    bool id666x_1;

    (id666x_1) = ((id666in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(67l))&(isFlushingActive()))));
    if((id666x_1)) {
      writeOutput(m_v_out, id666in_data);
    }
  }
  { // Node ID: 940 (NodeConstantRawBits)
  }
  { // Node ID: 668 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id668in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id668in_b = id940out_value;

    id668out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id668in_a,id668in_b));
  }
  { // Node ID: 738 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id738in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id738in_b = id668out_result[getCycle()%2];

    id738out_result[(getCycle()+1)%2] = (eq_fixed(id738in_a,id738in_b));
  }
  { // Node ID: 670 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id671out_result;

  { // Node ID: 671 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id671in_a = id670out_io_w_out_force_disabled;

    id671out_result = (not_fixed(id671in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id672out_result;

  { // Node ID: 672 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id672in_a = id738out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id672in_b = id671out_result;

    HWOffsetFix<1,0,UNSIGNED> id672x_1;

    (id672x_1) = (and_fixed(id672in_a,id672in_b));
    id672out_result = (id672x_1);
  }
  { // Node ID: 909 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id909in_input = id672out_result;

    id909out_output[(getCycle()+163)%164] = id909in_input;
  }
  { // Node ID: 651 (NodeCast)
    const HWFloat<8,40> &id651in_i = id589out_result[getCycle()%13];

    id651out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id651in_i));
  }
  if ( (getFillLevel() >= (167l)) && (getFlushLevel() < (167l)|| !isFlushingActive() ))
  { // Node ID: 673 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id673in_output_control = id909out_output[getCycle()%164];
    const HWFloat<8,24> &id673in_data = id651out_o[getCycle()%4];

    bool id673x_1;

    (id673x_1) = ((id673in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(167l))&(isFlushingActive()))));
    if((id673x_1)) {
      writeOutput(m_w_out, id673in_data);
    }
  }
  { // Node ID: 939 (NodeConstantRawBits)
  }
  { // Node ID: 675 (NodeSub)
    const HWOffsetFix<8,0,UNSIGNED> &id675in_a = id936out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id675in_b = id939out_value;

    id675out_result[(getCycle()+1)%2] = (sub_fixed<8,0,UNSIGNED,TONEAREVEN>(id675in_a,id675in_b));
  }
  { // Node ID: 739 (NodeEqInlined)
    const HWOffsetFix<8,0,UNSIGNED> &id739in_a = id48out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id739in_b = id675out_result[getCycle()%2];

    id739out_result[(getCycle()+1)%2] = (eq_fixed(id739in_a,id739in_b));
  }
  { // Node ID: 677 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id678out_result;

  { // Node ID: 678 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id678in_a = id677out_io_s_out_force_disabled;

    id678out_result = (not_fixed(id678in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id679out_result;

  { // Node ID: 679 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id679in_a = id739out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id679in_b = id678out_result;

    HWOffsetFix<1,0,UNSIGNED> id679x_1;

    (id679x_1) = (and_fixed(id679in_a,id679in_b));
    id679out_result = (id679x_1);
  }
  { // Node ID: 910 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id910in_input = id679out_result;

    id910out_output[(getCycle()+156)%157] = id910in_input;
  }
  { // Node ID: 652 (NodeCast)
    const HWFloat<8,40> &id652in_i = id591out_result[getCycle()%13];

    id652out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id652in_i));
  }
  if ( (getFillLevel() >= (160l)) && (getFlushLevel() < (160l)|| !isFlushingActive() ))
  { // Node ID: 680 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id680in_output_control = id910out_output[getCycle()%157];
    const HWFloat<8,24> &id680in_data = id652out_o[getCycle()%4];

    bool id680x_1;

    (id680x_1) = ((id680in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(160l))&(isFlushingActive()))));
    if((id680x_1)) {
      writeOutput(m_s_out, id680in_data);
    }
  }
  { // Node ID: 692 (NodeConstantRawBits)
  }
  { // Node ID: 938 (NodeConstantRawBits)
  }
  { // Node ID: 689 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 690 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id690in_enable = id938out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id690in_max = id689out_value;

    HWOffsetFix<49,0,UNSIGNED> id690x_1;
    HWOffsetFix<1,0,UNSIGNED> id690x_2;
    HWOffsetFix<1,0,UNSIGNED> id690x_3;
    HWOffsetFix<49,0,UNSIGNED> id690x_4t_1e_1;

    id690out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id690st_count)));
    (id690x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id690st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id690x_2) = (gte_fixed((id690x_1),id690in_max));
    (id690x_3) = (and_fixed((id690x_2),id690in_enable));
    id690out_wrap = (id690x_3);
    if((id690in_enable.getValueAsBool())) {
      if(((id690x_3).getValueAsBool())) {
        (id690st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id690x_4t_1e_1) = (id690x_1);
        (id690st_count) = (id690x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id691out_output;

  { // Node ID: 691 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id691in_input = id690out_count;

    id691out_output = id691in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 693 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id693in_load = id692out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id693in_data = id691out_output;

    bool id693x_1;

    (id693x_1) = ((id693in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id693x_1)) {
      setMappedRegValue("current_run_cycle_count", id693in_data);
    }
  }
  { // Node ID: 937 (NodeConstantRawBits)
  }
  { // Node ID: 695 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 696 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id696in_enable = id937out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id696in_max = id695out_value;

    HWOffsetFix<49,0,UNSIGNED> id696x_1;
    HWOffsetFix<1,0,UNSIGNED> id696x_2;
    HWOffsetFix<1,0,UNSIGNED> id696x_3;
    HWOffsetFix<49,0,UNSIGNED> id696x_4t_1e_1;

    id696out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id696st_count)));
    (id696x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id696st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id696x_2) = (gte_fixed((id696x_1),id696in_max));
    (id696x_3) = (and_fixed((id696x_2),id696in_enable));
    id696out_wrap = (id696x_3);
    if((id696in_enable.getValueAsBool())) {
      if(((id696x_3).getValueAsBool())) {
        (id696st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id696x_4t_1e_1) = (id696x_1);
        (id696st_count) = (id696x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 698 (NodeInputMappedReg)
  }
  { // Node ID: 740 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id740in_a = id696out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id740in_b = id698out_run_cycle_count;

    id740out_result[(getCycle()+1)%2] = (eq_fixed(id740in_a,id740in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 697 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id697in_start = id740out_result[getCycle()%2];

    if((id697in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
