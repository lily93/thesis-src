#include "stdsimheader.h"

using namespace maxcompilersim;

namespace maxcompilersim {
// Templated Types used in the kernel
template class HWOffsetFix<40,-40,UNSIGNED>;
template class HWOffsetFix<48,0,UNSIGNED>;
template class HWOffsetFix<49,0,UNSIGNED>;
template class HWOffsetFix<8,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<62,-83,UNSIGNED>;
template class HWRawBits<48>;
template class HWOffsetFix<44,-43,UNSIGNED>;
template class HWOffsetFix<64,-61,UNSIGNED>;
template class HWOffsetFix<24,0,UNSIGNED>;
template class HWOffsetFix<10,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<1,-1,UNSIGNED>;
template class HWOffsetFix<10,-21,UNSIGNED>;
template class HWOffsetFix<22,-43,UNSIGNED>;
template class HWOffsetFix<11,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<105,-94,TWOSCOMPLEMENT>;
template class HWOffsetFix<39,0,UNSIGNED>;
template class HWOffsetFix<10,-11,UNSIGNED>;
template class HWOffsetFix<1,0,UNSIGNED>;
template class HWOffsetFix<64,-62,UNSIGNED>;
template class HWOffsetFix<39,-40,UNSIGNED>;
template class HWRawBits<10>;
template class HWOffsetFix<4,0,UNSIGNED>;
template class HWRawBits<39>;
template class HWOffsetFix<8,0,UNSIGNED>;
template class HWOffsetFix<32,0,UNSIGNED>;
template class HWOffsetFix<46,-43,UNSIGNED>;
template class HWOffsetFix<64,-73,UNSIGNED>;
template class HWOffsetFix<33,0,UNSIGNED>;
template class HWOffsetFix<40,-39,UNSIGNED>;
template class HWFloat<8,40>;
template class HWOffsetFix<64,-63,UNSIGNED>;
template class HWOffsetFix<21,-21,UNSIGNED>;
template class HWRawBits<2>;
template class HWFloat<11,53>;
template class HWFloat<8,24>;
template class HWOffsetFix<9,0,UNSIGNED>;
template class HWRawBits<1>;
template class HWOffsetFix<53,-43,TWOSCOMPLEMENT>;
template class HWOffsetFix<29,-40,UNSIGNED>;
template class HWOffsetFix<10,0,UNSIGNED>;
template class HWOffsetFix<43,-64,UNSIGNED>;
template class HWOffsetFix<45,-43,UNSIGNED>;
template class HWOffsetFix<39,-39,UNSIGNED>;
template class HWOffsetFix<64,-64,UNSIGNED>;
template class HWOffsetFix<52,-51,UNSIGNED>;
template class HWRawBits<9>;
template class HWRawBits<8>;
// add. templates from the default formatter 


// Templated Methods/Functions
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<4,0,UNSIGNED> &a, const HWOffsetFix<4,0,UNSIGNED> &b );
template HWOffsetFix<24,0,UNSIGNED> cast_fixed2fixed<24,0,UNSIGNED,TONEAREVEN>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWRawBits<2> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b);
template HWOffsetFix<64,-73,UNSIGNED>mul_fixed <64,-73,UNSIGNED,TONEAREVEN,45,-43,UNSIGNED,29,-40,UNSIGNED, false>(const HWOffsetFix<45,-43,UNSIGNED> &a, const HWOffsetFix<29,-40,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,40>sub_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-21,UNSIGNED> &a);
template HWOffsetFix<8,0,UNSIGNED> cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<9,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<64,-63,UNSIGNED>add_fixed <64,-63,UNSIGNED,TONEAREVEN,44,-43,UNSIGNED,62,-83,UNSIGNED, false>(const HWOffsetFix<44,-43,UNSIGNED> &a, const HWOffsetFix<62,-83,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<44,-43,UNSIGNED> cast_fixed2fixed<44,-43,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-63,UNSIGNED> &a);
template HWRawBits<48> cat<>(const HWRawBits<9> &a, const HWOffsetFix<39,0,UNSIGNED> &b);
template HWOffsetFix<53,-43,TWOSCOMPLEMENT> cast_fixed2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &a, HWOffsetFix<1,0,UNSIGNED>* exception );
template HWOffsetFix<1,0,UNSIGNED>or_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<10,-11,UNSIGNED> cast_fixed2fixed<10,-11,UNSIGNED,TRUNCATE>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>not_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a );
template HWOffsetFix<46,-43,UNSIGNED>add_fixed <46,-43,UNSIGNED,TONEAREVEN,29,-40,UNSIGNED,45,-43,UNSIGNED, false>(const HWOffsetFix<29,-40,UNSIGNED> &a, const HWOffsetFix<45,-43,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-64,UNSIGNED>mul_fixed <64,-64,UNSIGNED,TONEAREVEN,44,-43,UNSIGNED,39,-40,UNSIGNED, false>(const HWOffsetFix<44,-43,UNSIGNED> &a, const HWOffsetFix<39,-40,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<46,-43,UNSIGNED> cast_fixed2fixed<46,-43,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-61,UNSIGNED> &a);
template HWOffsetFix<10,0,UNSIGNED> cast_bits2fixed<10,0,UNSIGNED>(const HWRawBits<10> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_bits<>(const HWRawBits<8> &a,  const HWRawBits<8> &b );
template HWOffsetFix<45,-43,UNSIGNED> cast_fixed2fixed<45,-43,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-62,UNSIGNED> &a);
template HWOffsetFix<9,0,UNSIGNED>add_fixed <9,0,UNSIGNED,TRUNCATE,9,0,UNSIGNED,9,0,UNSIGNED, false>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b );
template HWOffsetFix<1,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<1,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<48,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<43,-64,UNSIGNED>mul_fixed <43,-64,UNSIGNED,TONEAREVEN,21,-21,UNSIGNED,22,-43,UNSIGNED, false>(const HWOffsetFix<21,-21,UNSIGNED> &a, const HWOffsetFix<22,-43,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<8,0,TWOSCOMPLEMENT> cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b );
template HWFloat<11,53> KernelManagerBlockSync::getMappedRegValue< HWFloat<11,53> >(const std::string &name);
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<33,0,UNSIGNED> &a);
template HWOffsetFix<105,-94,TWOSCOMPLEMENT>mul_fixed <105,-94,TWOSCOMPLEMENT,TONEAREVEN,53,-43,TWOSCOMPLEMENT,52,-51,UNSIGNED, true>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a, const HWOffsetFix<52,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<22,-43,UNSIGNED> cast_fixed2fixed<22,-43,UNSIGNED,TRUNCATE>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gt_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWOffsetFix<64,-61,UNSIGNED>add_fixed <64,-61,UNSIGNED,TONEAREVEN,46,-43,UNSIGNED,64,-73,UNSIGNED, false>(const HWOffsetFix<46,-43,UNSIGNED> &a, const HWOffsetFix<64,-73,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<40,-39,UNSIGNED> cast_fixed2fixed<40,-39,UNSIGNED,TONEAREVEN>(const HWOffsetFix<46,-43,UNSIGNED> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT>add_fixed <11,0,TWOSCOMPLEMENT,TRUNCATE,11,0,TWOSCOMPLEMENT,11,0,TWOSCOMPLEMENT, false>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b , EXCEPTOVERFLOW);
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b );
template HWRawBits<1> cast_fixed2bits<>(const HWOffsetFix<1,-1,UNSIGNED> &a);
template HWFloat<8,40> cast_float2float<8,40>(const HWFloat<11,53> &a);
template HWOffsetFix<10,-21,UNSIGNED> cast_fixed2fixed<10,-21,UNSIGNED,TRUNCATE>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>lt_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWOffsetFix<39,-39,UNSIGNED> cast_fixed2fixed<39,-39,UNSIGNED,TONEAREVEN>(const HWOffsetFix<40,-39,UNSIGNED> &a);
template HWOffsetFix<64,-62,UNSIGNED>add_fixed <64,-62,UNSIGNED,TONEAREVEN,45,-43,UNSIGNED,64,-64,UNSIGNED, false>(const HWOffsetFix<45,-43,UNSIGNED> &a, const HWOffsetFix<64,-64,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<33,0,UNSIGNED>add_fixed <33,0,UNSIGNED,TRUNCATE,33,0,UNSIGNED,33,0,UNSIGNED, false>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,40> cast_bits2float<8,40>(const HWRawBits<48> &a);
template HWRawBits<1> slice<10,1>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<44,-43,UNSIGNED>add_fixed <44,-43,UNSIGNED,TONEAREVEN,40,-40,UNSIGNED,22,-43,UNSIGNED, false>(const HWOffsetFix<40,-40,UNSIGNED> &a, const HWOffsetFix<22,-43,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>eq_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWRawBits<39> slice<0,39>(const HWFloat<8,40> &a);
template HWOffsetFix<1,0,UNSIGNED> cast_bits2fixed<1,0,UNSIGNED>(const HWRawBits<1> &a);
template HWOffsetFix<10,0,TWOSCOMPLEMENT> cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWFloat<8,40>add_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWRawBits<48> cat<>(const HWRawBits<9> &a, const HWOffsetFix<39,-39,UNSIGNED> &b);
template HWOffsetFix<32,0,UNSIGNED>add_fixed <32,0,UNSIGNED,TONEAREVEN,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,TWOSCOMPLEMENT> &b);
template HWFloat<8,40>mul_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWOffsetFix<8,0,UNSIGNED>sub_fixed <8,0,UNSIGNED,TONEAREVEN,8,0,UNSIGNED,8,0,UNSIGNED, false>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,24> cast_float2float<8,24>(const HWFloat<8,40> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<48,0,UNSIGNED> &a, const HWOffsetFix<48,0,UNSIGNED> &b );
template HWOffsetFix<1,-1,UNSIGNED> cast_fixed2fixed<1,-1,UNSIGNED,TRUNCATE>(const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &a);
template void KernelManagerBlockSync::writeOutput< HWFloat<8,24> >(const t_port_number port_number, const HWFloat<8,24> &value);
template HWOffsetFix<53,-43,TWOSCOMPLEMENT> cast_float2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(const HWFloat<8,40> &a, HWOffsetFix<4,0,UNSIGNED>* exception );
template HWOffsetFix<9,0,UNSIGNED> cast_fixed2fixed<9,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<8,0,UNSIGNED> &a);
template HWOffsetFix<33,0,UNSIGNED> cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWRawBits<8> slice<39,8>(const HWFloat<8,40> &a);
template void KernelManagerBlockSync::setMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name, const HWOffsetFix<48,0,UNSIGNED> & value);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b );
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-11,UNSIGNED> &a);
template HWOffsetFix<32,0,UNSIGNED>sub_fixed <32,0,UNSIGNED,TONEAREVEN,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<48,0,UNSIGNED> cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<49,0,UNSIGNED> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT> cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<10,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<62,-83,UNSIGNED>mul_fixed <62,-83,UNSIGNED,TONEAREVEN,22,-43,UNSIGNED,40,-40,UNSIGNED, false>(const HWOffsetFix<22,-43,UNSIGNED> &a, const HWOffsetFix<40,-40,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<45,-43,UNSIGNED>add_fixed <45,-43,UNSIGNED,TONEAREVEN,39,-40,UNSIGNED,44,-43,UNSIGNED, false>(const HWOffsetFix<39,-40,UNSIGNED> &a, const HWOffsetFix<44,-43,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<49,0,UNSIGNED>add_fixed <49,0,UNSIGNED,TRUNCATE,49,0,UNSIGNED,49,0,UNSIGNED, false>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>lt_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWFloat<8,40>neg_float<>(const HWFloat<8,40> &a );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWFloat<8,40>div_float<>(const HWFloat<8,40> &a, const HWFloat<8,40> &b );
template HWOffsetFix<32,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<32,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<22,-43,UNSIGNED> cast_fixed2fixed<22,-43,UNSIGNED,TONEAREVEN>(const HWOffsetFix<43,-64,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<40,-39,UNSIGNED> &a, const HWOffsetFix<40,-39,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>neq_bits<>(const HWRawBits<39> &a,  const HWRawBits<39> &b );
template HWOffsetFix<1,0,UNSIGNED>and_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );


// Additional Code
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_1 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_2 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_3 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_4 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_5 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_6 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}
std::string CellCableDFE48Kernel::format_string_CellCableDFE48Kernel_7 (const char* _format_arg_format_string)
{return ( bfmt(_format_arg_format_string)).str();}

} // End of maxcompilersim namespace
