#include "stdsimheader.h"
#include "CellCableDFE32Kernel.h"

namespace maxcompilersim {

CellCableDFE32Kernel::CellCableDFE32Kernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 5, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 634 (NodeConstantRawBits)
    id634out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 644 (NodeConstantRawBits)
    id644out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 631 (NodeConstantRawBits)
    id631out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 635 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 643 (NodeConstantRawBits)
    id643out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 637 (NodeConstantRawBits)
    id637out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 640 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
  { // Node ID: 36 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(32));
  }
  { // Node ID: 37 (NodeInputMappedReg)
    registerMappedRegister("ddt_o_dx2", Data(32));
  }
  { // Node ID: 38 (NodeInputMappedReg)
    registerMappedRegister("nx", Data(32));
  }
  { // Node ID: 39 (NodeInputMappedReg)
    registerMappedRegister("simTime", Data(32));
  }
}

void CellCableDFE32Kernel::resetComputation() {
  resetComputationAfterFlush();
}

void CellCableDFE32Kernel::resetComputationAfterFlush() {
  { // Node ID: 632 (NodeCounter)

    (id632st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 638 (NodeCounter)

    (id638st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 640 (NodeInputMappedReg)
    id640out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_dt = getMappedRegValue<HWFloat<8,24> >("dt");
  }
  { // Node ID: 37 (NodeInputMappedReg)
    id37out_ddt_o_dx2 = getMappedRegValue<HWFloat<8,24> >("ddt_o_dx2");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_nx = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("nx");
  }
  { // Node ID: 39 (NodeInputMappedReg)
    id39out_simTime = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("simTime");
  }
}

void CellCableDFE32Kernel::updateState() {
  { // Node ID: 640 (NodeInputMappedReg)
    id640out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_dt = getMappedRegValue<HWFloat<8,24> >("dt");
  }
  { // Node ID: 37 (NodeInputMappedReg)
    id37out_ddt_o_dx2 = getMappedRegValue<HWFloat<8,24> >("ddt_o_dx2");
  }
  { // Node ID: 38 (NodeInputMappedReg)
    id38out_nx = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("nx");
  }
  { // Node ID: 39 (NodeInputMappedReg)
    id39out_simTime = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("simTime");
  }
}

void CellCableDFE32Kernel::preExecute() {
}

void CellCableDFE32Kernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "CellCableDFE32Kernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int CellCableDFE32Kernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
