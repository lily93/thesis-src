#include "stdsimheader.h"

namespace maxcompilersim {

void CellCableDFE32Kernel::execute0() {
  { // Node ID: 634 (NodeConstantRawBits)
  }
  { // Node ID: 644 (NodeConstantRawBits)
  }
  { // Node ID: 631 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 632 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id632in_enable = id644out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id632in_max = id631out_value;

    HWOffsetFix<49,0,UNSIGNED> id632x_1;
    HWOffsetFix<1,0,UNSIGNED> id632x_2;
    HWOffsetFix<1,0,UNSIGNED> id632x_3;
    HWOffsetFix<49,0,UNSIGNED> id632x_4t_1e_1;

    id632out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id632st_count)));
    (id632x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id632st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id632x_2) = (gte_fixed((id632x_1),id632in_max));
    (id632x_3) = (and_fixed((id632x_2),id632in_enable));
    id632out_wrap = (id632x_3);
    if((id632in_enable.getValueAsBool())) {
      if(((id632x_3).getValueAsBool())) {
        (id632st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id632x_4t_1e_1) = (id632x_1);
        (id632st_count) = (id632x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id633out_output;

  { // Node ID: 633 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id633in_input = id632out_count;

    id633out_output = id633in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 635 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id635in_load = id634out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id635in_data = id633out_output;

    bool id635x_1;

    (id635x_1) = ((id635in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id635x_1)) {
      setMappedRegValue("current_run_cycle_count", id635in_data);
    }
  }
  { // Node ID: 643 (NodeConstantRawBits)
  }
  { // Node ID: 637 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 638 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id638in_enable = id643out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id638in_max = id637out_value;

    HWOffsetFix<49,0,UNSIGNED> id638x_1;
    HWOffsetFix<1,0,UNSIGNED> id638x_2;
    HWOffsetFix<1,0,UNSIGNED> id638x_3;
    HWOffsetFix<49,0,UNSIGNED> id638x_4t_1e_1;

    id638out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id638st_count)));
    (id638x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id638st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id638x_2) = (gte_fixed((id638x_1),id638in_max));
    (id638x_3) = (and_fixed((id638x_2),id638in_enable));
    id638out_wrap = (id638x_3);
    if((id638in_enable.getValueAsBool())) {
      if(((id638x_3).getValueAsBool())) {
        (id638st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id638x_4t_1e_1) = (id638x_1);
        (id638st_count) = (id638x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 640 (NodeInputMappedReg)
  }
  { // Node ID: 642 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id642in_a = id638out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id642in_b = id640out_run_cycle_count;

    id642out_result[(getCycle()+1)%2] = (eq_fixed(id642in_a,id642in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 639 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id639in_start = id642out_result[getCycle()%2];

    if((id639in_start.getValueAsBool())) {
      startFlushing();
    }
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeInputMappedReg)
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  { // Node ID: 39 (NodeInputMappedReg)
  }
};

};
