#ifndef CELLCABLEDFE32KERNEL_H_
#define CELLCABLEDFE32KERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFE32Kernel : public KernelManagerBlockSync {
public:
  CellCableDFE32Kernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  HWOffsetFix<1,0,UNSIGNED> id634out_value;

  HWOffsetFix<1,0,UNSIGNED> id644out_value;

  HWOffsetFix<49,0,UNSIGNED> id631out_value;

  HWOffsetFix<48,0,UNSIGNED> id632out_count;
  HWOffsetFix<1,0,UNSIGNED> id632out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id632st_count;

  HWOffsetFix<1,0,UNSIGNED> id643out_value;

  HWOffsetFix<49,0,UNSIGNED> id637out_value;

  HWOffsetFix<48,0,UNSIGNED> id638out_count;
  HWOffsetFix<1,0,UNSIGNED> id638out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id638st_count;

  HWOffsetFix<48,0,UNSIGNED> id640out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id642out_result[2];

  HWFloat<8,24> id36out_dt;

  HWFloat<8,24> id37out_ddt_o_dx2;

  HWOffsetFix<32,0,UNSIGNED> id38out_nx;

  HWOffsetFix<32,0,UNSIGNED> id39out_simTime;

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFE32KERNEL_H_ */
