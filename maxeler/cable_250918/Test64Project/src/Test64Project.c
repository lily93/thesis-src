#include "Maxfiles.h" 			// Includes .max files
#include <MaxSLiCInterface.h>	// Simple Live CPU interface
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#define NEXT_LINE(fp) while(getc(fp)!='\n');
double dt = 0;
double ddt_o_dx2 = 0;
double dx = 0;
double diff = 0;
int number_of_cells = 0;
int num_iteration = 0;
const int stimDur = 5;
int simTime = 0;
const int preciscion = 1;
int spiral = 0;
int bits = 0;


void print64( double * u_out, double * v_out, double * s_out, double * w_out, double * stim_out) {
	char filename[1024];
		sprintf(filename, "cellcable_out_maxeler_%f_%d_%d_%d.csv", dt, number_of_cells ,simTime, bits);
		if( access(filename, F_OK) != -1) {
				remove(filename);
			}
		FILE *fp = fopen(filename, "w");

	int ticks = number_of_cells * num_iteration;

	if(fp==NULL) {
		printf("no file found");
	}
	else {
		//printf("u;v;w;s;stim\n");

		fprintf(fp,"u;v;w;s;stim\n");

for (int i = 0;i<ticks;i++) {
if(i%number_of_cells==0) {
//	printf("\n");


}
//printf("%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
fprintf(fp,"%g;%g;%g;%g;%g\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);



}
fclose(fp);
fp = NULL;
	}
}


void printTimes(long time) {
	FILE *fp = fopen("DFE_elapsedtime.dat", "a");
	fprintf(fp, "Bits: %d\nSimulation Time (ms) %d\nNumber of Cells: %d\nStep Size: %f\nElapsed Time: %ld\n-----------------------------------\n", bits, simTime, number_of_cells, dt, time );
	fclose(fp);

}
void test_DFE64(int time) {
	dt = 0.0125;
	 dx                      =   0.02;//Original is 0.25
			  diff                    =   0.00116;
			  number_of_cells = 256;
			  simTime = time;
			  ddt_o_dx2      = dt*diff/(dx * dx);
				  num_iteration = (int)(simTime/dt);
				  bits = 64;

while(number_of_cells<10000) {

				  printf("\n\n Parameters: \n");
				  printf("timesteps    = %d\n", num_iteration);
				  printf("number of cells     = %d\n", number_of_cells);
				  printf("dt     = %f\n", dt);
				  printf("dx    = %f\n", dx);
				  printf("diff   = %f\n", diff);
				  printf("ddt_o_dx2   = %f\n", ddt_o_dx2);
				  printf("precision   = %d\n", bits);

	runDFE64();
	number_of_cells = number_of_cells*2;
}
}


void runDFE64() {

	int sizeBytes = (number_of_cells*num_iteration) * sizeof(double);


//	double * u_out = malloc(sizeBytes);
//	double * v_out = malloc(sizeBytes);
//	double * w_out = malloc(sizeBytes);
//	double * s_out =  malloc(sizeBytes);
//	double * stim_out= malloc(sizeBytes);




	struct timeval start, end;
		long mtime, seconds, useconds;




		max_file_t *myMaxFile = CellCableDFE_init();
		max_engine_t *myDFE = max_load(myMaxFile, "local:0");



		const int ticks = num_iteration*number_of_cells;


		CellCableDFE_actions_t actions;

		actions.param_length = ticks;
		actions.param_dt = dt;
		actions.param_ddt_o_dx2 = ddt_o_dx2;
		actions.param_simTime = num_iteration;
		actions.param_nx = number_of_cells;
//		actions.outstream_u_out = u_out;
//		actions.outstream_v_out = v_out;
//		actions.outstream_w_out = w_out;
//		actions.outstream_s_out = s_out;
	//	actions.outstream_stim_out = stim_out;






			printf("Running DFE 64\n");
			gettimeofday(&start, NULL);

			CellCableDFE_run(myDFE, &actions);


			gettimeofday(&end, NULL);
			seconds = end.tv_sec - start.tv_sec;
			useconds = end.tv_usec - start.tv_usec;

			mtime = ((seconds) * 1000 + useconds / 1000.0);

			printf("DFE elapsed time: %ld milliseconds \n", mtime);
			printTimes(mtime);
			//printf("Printing Data from DFE\n");
			//print64(u_out, v_out, s_out, w_out, stim_out);

//			free(u_out);
//			u_out = NULL;
//			free(v_out);
//			v_out = NULL;
//			free(w_out);
//			w_out = NULL;
//			free(stim_out);
//			stim_out = NULL;
			max_unload(myDFE);



}

int main(void) {
	test_DFE64(2);
	test_DFE64(1000);
	return 0;
}
