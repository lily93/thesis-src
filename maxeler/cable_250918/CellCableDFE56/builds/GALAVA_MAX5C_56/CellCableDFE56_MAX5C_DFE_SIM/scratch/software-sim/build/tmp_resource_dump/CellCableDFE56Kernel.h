#ifndef CELLCABLEDFE56KERNEL_H_
#define CELLCABLEDFE56KERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class CellCableDFE56Kernel : public KernelManagerBlockSync {
public:
  CellCableDFE56Kernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id43out_value;

  HWOffsetFix<8,0,UNSIGNED> id936out_value;

  HWOffsetFix<8,0,UNSIGNED> id774out_output[2];

  HWOffsetFix<8,0,UNSIGNED> id48out_count;
  HWOffsetFix<1,0,UNSIGNED> id48out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id48st_count;

  HWOffsetFix<8,0,UNSIGNED> id1009out_value;

  HWOffsetFix<8,0,UNSIGNED> id654out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id703out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id656out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id907out_output[234];

  HWOffsetFix<1,0,UNSIGNED> id775out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id40out_nx;

  HWOffsetFix<32,0,UNSIGNED> id47out_count;
  HWOffsetFix<1,0,UNSIGNED> id47out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id47st_count;

  HWOffsetFix<1,0,UNSIGNED> id776out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id41out_simTime;

  HWOffsetFix<32,0,UNSIGNED> id1008out_value;

  HWOffsetFix<32,0,UNSIGNED> id45out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id46out_count;
  HWOffsetFix<1,0,UNSIGNED> id46out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id46st_count;

  HWOffsetFix<32,0,UNSIGNED> id1007out_value;

  HWOffsetFix<1,0,UNSIGNED> id704out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id893out_output[23];

  HWOffsetFix<32,0,UNSIGNED> id911out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id912out_output[183];

  HWOffsetFix<32,0,UNSIGNED> id778out_output[29];

  HWOffsetFix<8,0,UNSIGNED> id1006out_value;

  HWOffsetFix<8,0,UNSIGNED> id627out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id705out_result[2];

  HWFloat<11,45> id681out_doutb[3];

  HWFloat<11,45> id681sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_1 (const char* _format_arg_format_string);
  HWFloat<11,45> id60out_value;

  HWFloat<11,45> id61out_result[2];

  HWFloat<11,45> id901out_output[2];

  HWFloat<11,45> id913out_output[6];

  HWFloat<11,45> id914out_output[27];

  HWFloat<11,45> id915out_output[23];

  HWFloat<11,45> id916out_output[21];

  HWFloat<11,45> id917out_output[30];

  HWFloat<11,45> id918out_output[2];

  HWFloat<11,45> id919out_output[34];

  HWFloat<11,45> id920out_output[12];

  HWFloat<11,45> id921out_output[8];

  HWFloat<11,45> id922out_output[11];

  HWFloat<11,45> id923out_output[4];

  HWFloat<11,45> id924out_output[24];

  HWFloat<11,45> id925out_output[7];

  HWFloat<11,45> id926out_output[22];

  HWFloat<11,45> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id592out_result[2];

  HWFloat<11,45> id34out_value;

  HWOffsetFix<1,0,UNSIGNED> id783out_output[2];

  HWFloat<11,45> id782out_output[77];

  HWOffsetFix<8,0,UNSIGNED> id1005out_value;

  HWOffsetFix<8,0,UNSIGNED> id631out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id706out_result[2];

  HWFloat<11,45> id684out_doutb[3];

  HWFloat<11,45> id684sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_2 (const char* _format_arg_format_string);
  HWFloat<11,45> id784out_output[2];

  HWFloat<11,45> id62out_value;

  HWFloat<11,45> id63out_result[2];

  HWFloat<11,45> id786out_output[3];

  HWFloat<11,45> id927out_output[7];

  HWFloat<11,45> id928out_output[42];

  HWOffsetFix<1,0,UNSIGNED> id462out_result[2];

  HWFloat<11,45> id17out_value;

  HWOffsetFix<1,0,UNSIGNED> id451out_result[2];

  HWFloat<11,45> id35out_value;

  HWFloat<11,45> id452out_result[2];

  HWFloat<11,45> id460out_output[2];

  HWFloat<11,45> id465out_result[7];

  HWFloat<11,45> id16out_value;

  HWOffsetFix<1,0,UNSIGNED> id226out_result[2];

  HWFloat<11,45> id1out_value;

  HWFloat<11,45> id2out_value;

  HWFloat<11,45> id227out_result[2];

  HWFloat<11,45> id446out_output[2];

  HWFloat<11,45> id466out_result[25];

  HWFloat<11,45> id0out_value;

  HWFloat<11,45> id464out_result[25];

  HWFloat<11,45> id467out_result[2];

  HWFloat<11,45> id583out_output[2];

  HWFloat<11,53> id36out_dt;

  HWFloat<11,45> id37out_o[4];

  HWFloat<11,45> id586out_result[16];

  HWFloat<11,45> id587out_result[13];

  HWFloat<11,45> id781out_output[93];

  HWFloat<11,45> id594out_result[7];

  HWFloat<11,45> id595out_result[8];

  HWFloat<11,45> id28out_value;

  HWFloat<11,45> id596out_result[7];

  HWFloat<11,45> id597out_result[8];

  HWFloat<11,45> id8out_value;

  HWFloat<11,45> id598out_result[25];

  HWFloat<11,45> id599out_result[2];

  HWFloat<11,45> id19out_value;

  HWOffsetFix<1,0,UNSIGNED> id600out_result[2];

  HWFloat<11,45> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id444out_result[2];

  HWFloat<11,45> id9out_value;

  HWFloat<11,45> id10out_value;

  HWFloat<11,45> id445out_result[2];

  HWFloat<11,45> id450out_output[2];

  HWFloat<11,45> id604out_result[25];

  HWFloat<11,45> id1004out_value;

  HWFloat<11,45> id11out_value;

  HWFloat<11,45> id31out_value;

  HWFloat<11,45> id1003out_value;

  HWFloat<11,45> id1002out_value;

  HWFloat<11,45> id1001out_value;

  HWFloat<11,45> id24out_value;

  HWFloat<11,45> id29out_value;

  HWFloat<11,45> id335out_result[7];

  HWFloat<11,45> id336out_result[8];

  HWFloat<11,45> id750out_floatOut[2];

  HWRawBits<11> id416out_value;

  HWOffsetFix<1,0,UNSIGNED> id707out_result[2];

  HWRawBits<44> id1000out_value;

  HWOffsetFix<1,0,UNSIGNED> id708out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id815out_output[43];

  HWOffsetFix<1,0,UNSIGNED> id339out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id341out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id341out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id344out_value;

  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id343out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id343out_result_doubt[7];

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id345out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id345out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id797out_output[28];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id385out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id741out_value;

  HWOffsetFix<10,0,UNSIGNED> id798out_output[16];

  HWOffsetFix<32,-45,UNSIGNED> id431out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id431sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id799out_output[8];

  HWOffsetFix<42,-45,UNSIGNED> id428out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id428sta_rom_store[1024];

  HWOffsetFix<3,0,UNSIGNED> id800out_output[2];

  HWOffsetFix<45,-45,UNSIGNED> id425out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id425sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id362out_value;

  HWOffsetFix<46,-69,UNSIGNED> id361out_result[3];

  HWOffsetFix<25,-48,UNSIGNED> id363out_o[2];

  HWOffsetFix<49,-48,UNSIGNED> id364out_result[2];

  HWOffsetFix<49,-48,UNSIGNED> id801out_output[3];

  HWOffsetFix<64,-87,UNSIGNED> id365out_result[4];

  HWOffsetFix<64,-63,UNSIGNED> id366out_result[3];

  HWOffsetFix<49,-48,UNSIGNED> id367out_o[2];

  HWOffsetFix<50,-48,UNSIGNED> id368out_result[2];

  HWOffsetFix<50,-48,UNSIGNED> id802out_output[5];

  HWOffsetFix<64,-66,UNSIGNED> id369out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id370out_result[3];

  HWOffsetFix<50,-48,UNSIGNED> id371out_o[2];

  HWOffsetFix<51,-48,UNSIGNED> id372out_result[2];

  HWOffsetFix<51,-48,UNSIGNED> id803out_output[5];

  HWOffsetFix<64,-75,UNSIGNED> id373out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id374out_result[3];

  HWOffsetFix<51,-48,UNSIGNED> id375out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id376out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id999out_value;

  HWOffsetFix<1,0,UNSIGNED> id709out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id749out_result[2];

  HWFloat<11,45> id998out_value;

  HWOffsetFix<1,0,UNSIGNED> id347out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id804out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id805out_output[29];

  HWFloat<11,45> id997out_value;

  HWOffsetFix<1,0,UNSIGNED> id351out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id806out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id807out_output[29];

  HWOffsetFix<1,0,UNSIGNED> id811out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id996out_value;

  HWOffsetFix<1,0,UNSIGNED> id712out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id809out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id929out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id395out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id813out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id812out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id379out_value;

  HWOffsetFix<45,-44,UNSIGNED> id380out_result[2];

  HWOffsetFix<44,-44,UNSIGNED> id814out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id404out_value;

  HWOffsetFix<11,0,UNSIGNED> id405out_value;

  HWOffsetFix<44,0,UNSIGNED> id407out_value;

  HWFloat<11,45> id700out_value;

  HWFloat<11,45> id412out_result[2];

  HWFloat<11,45> id995out_value;

  HWFloat<11,45> id422out_result[2];

  HWFloat<11,45> id994out_value;

  HWFloat<11,45> id433out_result[7];

  HWFloat<11,45> id435out_result[25];

  HWFloat<11,45> id437out_result[7];

  HWFloat<11,45> id439out_result[7];

  HWFloat<11,45> id440out_result[8];

  HWFloat<11,45> id441out_result[7];

  HWFloat<11,45> id448out_output[2];

  HWFloat<11,45> id602out_result[25];

  HWFloat<11,45> id605out_result[2];

  HWFloat<11,45> id612out_result[7];

  HWFloat<11,45> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id606out_result[2];

  HWFloat<11,45> id610out_value;

  HWFloat<11,45> id818out_output[64];

  HWOffsetFix<8,0,UNSIGNED> id993out_value;

  HWOffsetFix<8,0,UNSIGNED> id635out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id714out_result[2];

  HWFloat<11,45> id683out_doutb[3];

  HWFloat<11,45> id683sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_3 (const char* _format_arg_format_string);
  HWFloat<11,45> id820out_output[2];

  HWFloat<11,45> id64out_value;

  HWFloat<11,45> id65out_result[2];

  HWFloat<11,45> id825out_output[108];

  HWFloat<11,45> id930out_output[7];

  HWFloat<11,45> id931out_output[42];

  HWFloat<11,45> id18out_value;

  HWOffsetFix<1,0,UNSIGNED> id468out_result[2];

  HWFloat<11,45> id33out_value;

  HWOffsetFix<1,0,UNSIGNED> id453out_result[2];

  HWFloat<11,45> id992out_value;

  HWFloat<11,45> id14out_value;

  HWFloat<11,45> id454out_result[25];

  HWFloat<11,45> id456out_result[7];

  HWFloat<11,45> id32out_value;

  HWFloat<11,45> id457out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id458out_result[2];

  HWFloat<11,45> id824out_output[2];

  HWFloat<11,45> id459out_result[2];

  HWFloat<11,45> id461out_output[2];

  HWFloat<11,45> id471out_result[7];

  HWFloat<11,45> id4out_value;

  HWFloat<11,45> id30out_value;

  HWFloat<11,45> id991out_value;

  HWFloat<11,45> id990out_value;

  HWFloat<11,45> id989out_value;

  HWFloat<11,45> id22out_value;

  HWFloat<11,45> id25out_value;

  HWFloat<11,45> id228out_result[7];

  HWFloat<11,45> id229out_result[8];

  HWFloat<11,45> id751out_floatOut[2];

  HWRawBits<11> id309out_value;

  HWOffsetFix<1,0,UNSIGNED> id715out_result[2];

  HWRawBits<44> id988out_value;

  HWOffsetFix<1,0,UNSIGNED> id716out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id844out_output[43];

  HWOffsetFix<1,0,UNSIGNED> id232out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id234out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id234out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id237out_value;

  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id236out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id236out_result_doubt[7];

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id238out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id238out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id826out_output[28];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id278out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id743out_value;

  HWOffsetFix<10,0,UNSIGNED> id827out_output[16];

  HWOffsetFix<32,-45,UNSIGNED> id324out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id324sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id828out_output[8];

  HWOffsetFix<42,-45,UNSIGNED> id321out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id321sta_rom_store[1024];

  HWOffsetFix<3,0,UNSIGNED> id829out_output[2];

  HWOffsetFix<45,-45,UNSIGNED> id318out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id318sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id255out_value;

  HWOffsetFix<46,-69,UNSIGNED> id254out_result[3];

  HWOffsetFix<25,-48,UNSIGNED> id256out_o[2];

  HWOffsetFix<49,-48,UNSIGNED> id257out_result[2];

  HWOffsetFix<49,-48,UNSIGNED> id830out_output[3];

  HWOffsetFix<64,-87,UNSIGNED> id258out_result[4];

  HWOffsetFix<64,-63,UNSIGNED> id259out_result[3];

  HWOffsetFix<49,-48,UNSIGNED> id260out_o[2];

  HWOffsetFix<50,-48,UNSIGNED> id261out_result[2];

  HWOffsetFix<50,-48,UNSIGNED> id831out_output[5];

  HWOffsetFix<64,-66,UNSIGNED> id262out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id263out_result[3];

  HWOffsetFix<50,-48,UNSIGNED> id264out_o[2];

  HWOffsetFix<51,-48,UNSIGNED> id265out_result[2];

  HWOffsetFix<51,-48,UNSIGNED> id832out_output[5];

  HWOffsetFix<64,-75,UNSIGNED> id266out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id267out_result[3];

  HWOffsetFix<51,-48,UNSIGNED> id268out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id269out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id987out_value;

  HWOffsetFix<1,0,UNSIGNED> id717out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id748out_result[2];

  HWFloat<11,45> id986out_value;

  HWOffsetFix<1,0,UNSIGNED> id240out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id833out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id834out_output[29];

  HWFloat<11,45> id985out_value;

  HWOffsetFix<1,0,UNSIGNED> id244out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id835out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id836out_output[29];

  HWOffsetFix<1,0,UNSIGNED> id840out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id984out_value;

  HWOffsetFix<1,0,UNSIGNED> id720out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id838out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id932out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id288out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id842out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id841out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id272out_value;

  HWOffsetFix<45,-44,UNSIGNED> id273out_result[2];

  HWOffsetFix<44,-44,UNSIGNED> id843out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id297out_value;

  HWOffsetFix<11,0,UNSIGNED> id298out_value;

  HWOffsetFix<44,0,UNSIGNED> id300out_value;

  HWFloat<11,45> id701out_value;

  HWFloat<11,45> id305out_result[2];

  HWFloat<11,45> id983out_value;

  HWFloat<11,45> id315out_result[2];

  HWFloat<11,45> id982out_value;

  HWFloat<11,45> id326out_result[7];

  HWFloat<11,45> id328out_result[25];

  HWFloat<11,45> id330out_result[7];

  HWFloat<11,45> id332out_result[7];

  HWFloat<11,45> id333out_result[8];

  HWFloat<11,45> id334out_result[7];

  HWFloat<11,45> id447out_output[2];

  HWFloat<11,45> id472out_result[25];

  HWFloat<11,45> id3out_value;

  HWFloat<11,45> id470out_result[25];

  HWFloat<11,45> id473out_result[2];

  HWFloat<11,45> id584out_output[2];

  HWFloat<11,45> id588out_result[16];

  HWFloat<11,45> id589out_result[13];

  HWFloat<11,45> id849out_output[64];

  HWOffsetFix<8,0,UNSIGNED> id981out_value;

  HWOffsetFix<8,0,UNSIGNED> id639out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id722out_result[2];

  HWFloat<11,45> id685out_doutb[3];

  HWFloat<11,45> id685sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_4 (const char* _format_arg_format_string);
  HWFloat<11,45> id66out_value;

  HWFloat<11,45> id67out_result[2];

  HWFloat<11,45> id869out_output[102];

  HWFloat<11,45> id933out_output[47];

  HWFloat<11,45> id980out_value;

  HWFloat<11,45> id979out_value;

  HWFloat<11,45> id978out_value;

  HWFloat<11,45> id23out_value;

  HWFloat<11,45> id26out_value;

  HWFloat<11,45> id474out_result[7];

  HWFloat<11,45> id475out_result[8];

  HWFloat<11,45> id752out_floatOut[2];

  HWRawBits<11> id555out_value;

  HWOffsetFix<1,0,UNSIGNED> id723out_result[2];

  HWRawBits<44> id977out_value;

  HWOffsetFix<1,0,UNSIGNED> id724out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id868out_output[43];

  HWOffsetFix<1,0,UNSIGNED> id478out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id480out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id480out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id483out_value;

  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id482out_result[7];
  HWOffsetFix<1,0,UNSIGNED> id482out_result_doubt[7];

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id484out_o[2];
  HWOffsetFix<1,0,UNSIGNED> id484out_o_doubt[2];

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id850out_output[28];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id524out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id745out_value;

  HWOffsetFix<10,0,UNSIGNED> id851out_output[16];

  HWOffsetFix<32,-45,UNSIGNED> id570out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id570sta_rom_store[1024];

  HWOffsetFix<10,0,UNSIGNED> id852out_output[8];

  HWOffsetFix<42,-45,UNSIGNED> id567out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id567sta_rom_store[1024];

  HWOffsetFix<3,0,UNSIGNED> id853out_output[2];

  HWOffsetFix<45,-45,UNSIGNED> id564out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id564sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id501out_value;

  HWOffsetFix<46,-69,UNSIGNED> id500out_result[3];

  HWOffsetFix<25,-48,UNSIGNED> id502out_o[2];

  HWOffsetFix<49,-48,UNSIGNED> id503out_result[2];

  HWOffsetFix<49,-48,UNSIGNED> id854out_output[3];

  HWOffsetFix<64,-87,UNSIGNED> id504out_result[4];

  HWOffsetFix<64,-63,UNSIGNED> id505out_result[3];

  HWOffsetFix<49,-48,UNSIGNED> id506out_o[2];

  HWOffsetFix<50,-48,UNSIGNED> id507out_result[2];

  HWOffsetFix<50,-48,UNSIGNED> id855out_output[5];

  HWOffsetFix<64,-66,UNSIGNED> id508out_result[6];

  HWOffsetFix<64,-62,UNSIGNED> id509out_result[3];

  HWOffsetFix<50,-48,UNSIGNED> id510out_o[2];

  HWOffsetFix<51,-48,UNSIGNED> id511out_result[2];

  HWOffsetFix<51,-48,UNSIGNED> id856out_output[5];

  HWOffsetFix<64,-75,UNSIGNED> id512out_result[6];

  HWOffsetFix<64,-61,UNSIGNED> id513out_result[3];

  HWOffsetFix<51,-48,UNSIGNED> id514out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id515out_o[2];

  HWOffsetFix<45,-44,UNSIGNED> id976out_value;

  HWOffsetFix<1,0,UNSIGNED> id725out_result[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id747out_result[2];

  HWFloat<11,45> id975out_value;

  HWOffsetFix<1,0,UNSIGNED> id486out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id857out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id858out_output[29];

  HWFloat<11,45> id974out_value;

  HWOffsetFix<1,0,UNSIGNED> id490out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id859out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id860out_output[29];

  HWOffsetFix<1,0,UNSIGNED> id864out_output[2];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id973out_value;

  HWOffsetFix<1,0,UNSIGNED> id728out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id862out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id934out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id534out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id866out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id865out_output[2];

  HWOffsetFix<45,-44,UNSIGNED> id518out_value;

  HWOffsetFix<45,-44,UNSIGNED> id519out_result[2];

  HWOffsetFix<44,-44,UNSIGNED> id867out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id543out_value;

  HWOffsetFix<11,0,UNSIGNED> id544out_value;

  HWOffsetFix<44,0,UNSIGNED> id546out_value;

  HWFloat<11,45> id702out_value;

  HWFloat<11,45> id551out_result[2];

  HWFloat<11,45> id972out_value;

  HWFloat<11,45> id561out_result[2];

  HWFloat<11,45> id971out_value;

  HWFloat<11,45> id572out_result[7];

  HWFloat<11,45> id574out_result[25];

  HWFloat<11,45> id576out_result[7];

  HWFloat<11,45> id578out_result[7];

  HWFloat<11,45> id753out_floatOut[2];

  HWFloat<11,45> id581out_result[7];

  HWOffsetFix<1,0,UNSIGNED> id442out_result[2];

  HWFloat<11,45> id6out_value;

  HWFloat<11,45> id7out_value;

  HWFloat<11,45> id443out_result[2];

  HWFloat<11,45> id449out_output[2];

  HWFloat<11,45> id582out_result[25];

  HWFloat<11,45> id585out_output[2];

  HWFloat<11,45> id590out_result[16];

  HWFloat<11,45> id591out_result[13];

  HWFloat<11,45> id848out_output[9];

  HWFloat<11,45> id608out_result[8];

  HWFloat<11,45> id13out_value;

  HWFloat<11,45> id609out_result[25];

  HWFloat<11,45> id611out_result[2];

  HWFloat<11,45> id613out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id970out_value;

  HWOffsetFix<1,0,UNSIGNED> id730out_result[2];

  HWFloat<11,45> id875out_output[52];

  HWOffsetFix<8,0,UNSIGNED> id969out_value;

  HWOffsetFix<8,0,UNSIGNED> id643out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id731out_result[2];

  HWFloat<11,45> id686out_doutb[3];

  HWFloat<11,45> id686sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_5 (const char* _format_arg_format_string);
  HWFloat<11,45> id70out_value;

  HWFloat<11,45> id71out_result[2];

  HWFloat<11,45> id876out_output[161];

  HWFloat<11,45> id76out_result[13];

  HWFloat<11,45> id878out_output[7];

  HWFloat<11,45> id935out_output[2];

  HWFloat<11,45> id968out_value;

  HWOffsetFix<1,0,UNSIGNED> id86out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id88out_value;

  HWOffsetFix<1,0,UNSIGNED> id87out_value;

  HWOffsetFix<1,0,UNSIGNED> id89out_result[2];

  HWFloat<11,45> id967out_value;

  HWOffsetFix<1,0,UNSIGNED> id81out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id83out_value;

  HWOffsetFix<1,0,UNSIGNED> id82out_value;

  HWOffsetFix<1,0,UNSIGNED> id84out_result[2];

  HWFloat<11,45> id91out_value;

  HWFloat<11,45> id90out_value;

  HWFloat<11,45> id92out_result[2];

  HWFloat<11,45> id93out_value;

  HWFloat<11,45> id94out_result[2];

  HWFloat<11,45> id966out_value;

  HWOffsetFix<32,0,UNSIGNED> id965out_value;

  HWOffsetFix<1,0,UNSIGNED> id732out_result[2];

  HWFloat<11,45> id880out_output[7];

  HWFloat<11,45> id881out_output[59];

  HWOffsetFix<8,0,UNSIGNED> id964out_value;

  HWOffsetFix<8,0,UNSIGNED> id647out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id733out_result[2];

  HWFloat<11,45> id687out_doutb[3];

  HWFloat<11,45> id687sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_6 (const char* _format_arg_format_string);
  HWFloat<11,45> id74out_value;

  HWFloat<11,45> id75out_result[2];

  HWFloat<11,45> id882out_output[155];

  HWFloat<11,45> id77out_result[13];

  HWFloat<11,45> id963out_value;

  HWFloat<11,45> id96out_result[7];

  HWFloat<11,45> id962out_value;

  HWOffsetFix<1,0,UNSIGNED> id103out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id105out_value;

  HWOffsetFix<1,0,UNSIGNED> id104out_value;

  HWOffsetFix<1,0,UNSIGNED> id106out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id883out_output[2];

  HWFloat<11,45> id961out_value;

  HWOffsetFix<1,0,UNSIGNED> id98out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id100out_value;

  HWOffsetFix<1,0,UNSIGNED> id99out_value;

  HWOffsetFix<1,0,UNSIGNED> id101out_result[2];

  HWFloat<11,45> id108out_value;

  HWFloat<11,45> id107out_value;

  HWFloat<11,45> id109out_result[2];

  HWFloat<11,45> id110out_value;

  HWFloat<11,45> id111out_result[2];

  HWFloat<11,45> id113out_result[7];

  HWFloat<11,45> id114out_result[8];

  HWFloat<11,45> id960out_value;

  HWFloat<11,45> id116out_result[7];

  HWFloat<11,45> id959out_value;

  HWOffsetFix<1,0,UNSIGNED> id123out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id125out_value;

  HWOffsetFix<1,0,UNSIGNED> id124out_value;

  HWOffsetFix<1,0,UNSIGNED> id126out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id884out_output[2];

  HWFloat<11,45> id958out_value;

  HWOffsetFix<1,0,UNSIGNED> id118out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<1,0,UNSIGNED> id119out_value;

  HWOffsetFix<1,0,UNSIGNED> id121out_result[2];

  HWFloat<11,45> id128out_value;

  HWFloat<11,45> id127out_value;

  HWFloat<11,45> id129out_result[2];

  HWFloat<11,45> id130out_value;

  HWFloat<11,45> id131out_result[2];

  HWFloat<11,45> id957out_value;

  HWFloat<11,45> id956out_value;

  HWFloat<11,45> id133out_result[7];

  HWFloat<11,45> id955out_value;

  HWOffsetFix<1,0,UNSIGNED> id140out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id142out_value;

  HWOffsetFix<1,0,UNSIGNED> id141out_value;

  HWOffsetFix<1,0,UNSIGNED> id143out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id885out_output[2];

  HWFloat<11,45> id954out_value;

  HWOffsetFix<1,0,UNSIGNED> id135out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id137out_value;

  HWOffsetFix<1,0,UNSIGNED> id136out_value;

  HWOffsetFix<1,0,UNSIGNED> id138out_result[2];

  HWFloat<11,45> id145out_value;

  HWFloat<11,45> id144out_value;

  HWFloat<11,45> id146out_result[2];

  HWFloat<11,45> id147out_value;

  HWFloat<11,45> id148out_result[2];

  HWFloat<11,45> id150out_result[7];

  HWFloat<11,45> id151out_result[8];

  HWFloat<11,45> id152out_result[7];

  HWFloat<11,45> id953out_value;

  HWFloat<11,45> id154out_result[7];

  HWFloat<11,45> id952out_value;

  HWOffsetFix<1,0,UNSIGNED> id161out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id163out_value;

  HWOffsetFix<1,0,UNSIGNED> id162out_value;

  HWOffsetFix<1,0,UNSIGNED> id164out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id887out_output[2];

  HWFloat<11,45> id951out_value;

  HWOffsetFix<1,0,UNSIGNED> id156out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id158out_value;

  HWOffsetFix<1,0,UNSIGNED> id157out_value;

  HWOffsetFix<1,0,UNSIGNED> id159out_result[2];

  HWFloat<11,45> id166out_value;

  HWFloat<11,45> id165out_value;

  HWFloat<11,45> id167out_result[2];

  HWFloat<11,45> id168out_value;

  HWFloat<11,45> id169out_result[2];

  HWFloat<11,45> id950out_value;

  HWFloat<11,45> id949out_value;

  HWFloat<11,45> id171out_result[7];

  HWFloat<11,45> id948out_value;

  HWOffsetFix<1,0,UNSIGNED> id178out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id180out_value;

  HWOffsetFix<1,0,UNSIGNED> id179out_value;

  HWOffsetFix<1,0,UNSIGNED> id181out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id889out_output[2];

  HWFloat<11,45> id947out_value;

  HWOffsetFix<1,0,UNSIGNED> id173out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id175out_value;

  HWOffsetFix<1,0,UNSIGNED> id174out_value;

  HWOffsetFix<1,0,UNSIGNED> id176out_result[2];

  HWFloat<11,45> id183out_value;

  HWFloat<11,45> id182out_value;

  HWFloat<11,45> id184out_result[2];

  HWFloat<11,45> id185out_value;

  HWFloat<11,45> id186out_result[2];

  HWFloat<11,45> id188out_result[7];

  HWFloat<11,45> id189out_result[8];

  HWFloat<11,45> id190out_result[7];

  HWFloat<11,45> id946out_value;

  HWOffsetFix<1,0,UNSIGNED> id192out_result[3];

  HWOffsetFix<32,0,UNSIGNED> id945out_value;

  HWOffsetFix<1,0,UNSIGNED> id734out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id195out_result[2];

  HWFloat<11,45> id197out_value;

  HWFloat<11,45> id196out_value;

  HWFloat<11,45> id198out_result[2];

  HWFloat<11,45> id614out_result[7];

  HWFloat<11,45> id615out_result[8];

  HWFloat<11,45> id616out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id944out_value;

  HWOffsetFix<1,0,UNSIGNED> id735out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id943out_value;

  HWOffsetFix<32,0,UNSIGNED> id211out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id736out_result[2];

  HWFloat<11,53> id38out_ddt_o_dx2;

  HWFloat<11,45> id39out_o[4];

  HWFloat<11,45> id894out_output[2];

  HWFloat<11,45> id899out_output[5];

  HWOffsetFix<24,0,UNSIGNED> id895out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id897out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id942out_value;

  HWOffsetFix<32,0,UNSIGNED> id56out_result[2];

  HWFloat<11,45> id682out_doutb[3];

  HWFloat<11,45> id682sta_ram_store[10000000];

  std::string format_string_CellCableDFE56Kernel_7 (const char* _format_arg_format_string);
  HWFloat<11,45> id200out_value;

  HWFloat<11,45> id201out_result[2];

  HWFloat<11,45> id219out_result[7];

  HWFloat<11,45> id754out_floatOut[2];

  HWFloat<11,45> id222out_result[7];

  HWFloat<11,45> id223out_result[8];

  HWFloat<11,45> id755out_floatOut[2];

  HWFloat<11,45> id756out_floatOut[2];

  HWFloat<11,45> id217out_result[7];

  HWFloat<11,45> id218out_result[8];

  HWFloat<11,45> id903out_output[6];

  HWFloat<11,45> id224out_result[2];

  HWFloat<11,45> id757out_floatOut[2];

  HWFloat<11,45> id758out_floatOut[2];

  HWFloat<11,45> id208out_result[7];

  HWFloat<11,45> id209out_result[8];

  HWFloat<11,45> id905out_output[7];

  HWFloat<11,45> id225out_result[2];

  HWFloat<11,45> id906out_output[203];

  HWFloat<11,45> id617out_result[7];

  HWOffsetFix<8,0,UNSIGNED> id941out_value;

  HWOffsetFix<8,0,UNSIGNED> id661out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id737out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id663out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id908out_output[66];

  HWOffsetFix<8,0,UNSIGNED> id940out_value;

  HWOffsetFix<8,0,UNSIGNED> id668out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id738out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id670out_io_w_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id909out_output[171];

  HWOffsetFix<8,0,UNSIGNED> id939out_value;

  HWOffsetFix<8,0,UNSIGNED> id675out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id739out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id677out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id910out_output[163];

  HWOffsetFix<1,0,UNSIGNED> id692out_value;

  HWOffsetFix<1,0,UNSIGNED> id938out_value;

  HWOffsetFix<49,0,UNSIGNED> id689out_value;

  HWOffsetFix<48,0,UNSIGNED> id690out_count;
  HWOffsetFix<1,0,UNSIGNED> id690out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id690st_count;

  HWOffsetFix<1,0,UNSIGNED> id937out_value;

  HWOffsetFix<49,0,UNSIGNED> id695out_value;

  HWOffsetFix<48,0,UNSIGNED> id696out_count;
  HWOffsetFix<1,0,UNSIGNED> id696out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id696st_count;

  HWOffsetFix<48,0,UNSIGNED> id698out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id740out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_undef;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits_1;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_undef;
  const HWFloat<11,45> c_hw_flt_11_45_undef;
  const HWFloat<11,45> c_hw_flt_11_45_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits_1;
  const HWFloat<11,45> c_hw_flt_11_45_bits_2;
  const HWFloat<11,45> c_hw_flt_11_45_bits_3;
  const HWFloat<11,45> c_hw_flt_11_45_bits_4;
  const HWFloat<11,45> c_hw_flt_11_45_bits_5;
  const HWFloat<11,45> c_hw_flt_11_45_bits_6;
  const HWFloat<11,45> c_hw_flt_11_45_bits_7;
  const HWFloat<11,45> c_hw_flt_11_45_bits_8;
  const HWFloat<11,45> c_hw_flt_11_45_bits_9;
  const HWFloat<11,45> c_hw_flt_11_45_bits_10;
  const HWFloat<11,45> c_hw_flt_11_45_bits_11;
  const HWFloat<11,45> c_hw_flt_11_45_bits_12;
  const HWFloat<11,45> c_hw_flt_11_45_bits_13;
  const HWFloat<11,45> c_hw_flt_11_45_bits_14;
  const HWFloat<11,45> c_hw_flt_11_45_bits_15;
  const HWFloat<11,45> c_hw_flt_11_45_2_0val;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<44> c_hw_bit_44_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<10,0,UNSIGNED> c_hw_fix_10_0_uns_undef;
  const HWOffsetFix<32,-45,UNSIGNED> c_hw_fix_32_n45_uns_undef;
  const HWOffsetFix<42,-45,UNSIGNED> c_hw_fix_42_n45_uns_undef;
  const HWOffsetFix<3,0,UNSIGNED> c_hw_fix_3_0_uns_undef;
  const HWOffsetFix<45,-45,UNSIGNED> c_hw_fix_45_n45_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<49,-48,UNSIGNED> c_hw_fix_49_n48_uns_undef;
  const HWOffsetFix<50,-48,UNSIGNED> c_hw_fix_50_n48_uns_undef;
  const HWOffsetFix<51,-48,UNSIGNED> c_hw_fix_51_n48_uns_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits_1;
  const HWOffsetFix<44,-44,UNSIGNED> c_hw_fix_44_n44_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<44,0,UNSIGNED> c_hw_fix_44_0_uns_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits_16;
  const HWFloat<11,45> c_hw_flt_11_45_bits_17;
  const HWFloat<11,45> c_hw_flt_11_45_bits_18;
  const HWFloat<11,45> c_hw_flt_11_45_bits_19;
  const HWFloat<11,45> c_hw_flt_11_45_bits_20;
  const HWFloat<11,45> c_hw_flt_11_45_bits_21;
  const HWFloat<11,45> c_hw_flt_11_45_bits_22;
  const HWFloat<11,45> c_hw_flt_11_45_bits_23;
  const HWFloat<11,45> c_hw_flt_11_45_bits_24;
  const HWFloat<11,45> c_hw_flt_11_45_bits_25;
  const HWFloat<11,45> c_hw_flt_11_45_0_5val;
  const HWFloat<11,45> c_hw_flt_11_45_bits_26;
  const HWFloat<11,45> c_hw_flt_11_45_bits_27;
  const HWFloat<11,45> c_hw_flt_11_45_bits_28;
  const HWFloat<11,45> c_hw_flt_11_45_bits_29;
  const HWFloat<11,45> c_hw_flt_11_45_bits_30;
  const HWFloat<11,45> c_hw_flt_11_45_bits_31;
  const HWFloat<11,45> c_hw_flt_11_45_bits_32;
  const HWFloat<11,45> c_hw_flt_11_45_bits_33;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_2;
  const HWFloat<11,45> c_hw_flt_11_45_bits_34;
  const HWOffsetFix<24,0,UNSIGNED> c_hw_fix_24_0_uns_undef;
  const HWFloat<11,45> c_hw_flt_11_45_n2_0val;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* CELLCABLEDFE56KERNEL_H_ */
