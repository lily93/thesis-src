#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaDFEKernel::execute0() {
  { // Node ID: 880 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 914 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id914out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 932 (NodeConstantRawBits)
  }
  { // Node ID: 549 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id549in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id549in_b = id932out_value;

    id549out_result[(getCycle()+1)%2] = (eq_fixed(id549in_a,id549in_b));
  }
  { // Node ID: 901 (NodeFIFO)
    const HWFloat<11,53> &id901in_input = id23out_result[getCycle()%2];

    id901out_output[(getCycle()+9)%10] = id901in_input;
  }
  { // Node ID: 913 (NodeFIFO)
    const HWFloat<11,53> &id913in_input = id901out_output[getCycle()%10];

    id913out_output[(getCycle()+1)%2] = id913in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 931 (NodeConstantRawBits)
  }
  { // Node ID: 550 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id550in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id550in_b = id931out_value;

    id550out_result[(getCycle()+1)%2] = (eq_fixed(id550in_a,id550in_b));
  }
  { // Node ID: 930 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id507out_result;

  { // Node ID: 507 (NodeGt)
    const HWFloat<11,53> &id507in_a = id913out_output[getCycle()%2];
    const HWFloat<11,53> &id507in_b = id5out_value;

    id507out_result = (gt_float(id507in_a,id507in_b));
  }
  HWFloat<11,53> id508out_o;

  { // Node ID: 508 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id508in_i = id507out_result;

    id508out_o = (cast_fixed2float<11,53>(id508in_i));
  }
  HWFloat<11,53> id510out_result;

  { // Node ID: 510 (NodeMul)
    const HWFloat<11,53> &id510in_a = id930out_value;
    const HWFloat<11,53> &id510in_b = id508out_o;

    id510out_result = (mul_float(id510in_a,id510in_b));
  }
  HWFloat<11,53> id511out_result;

  { // Node ID: 511 (NodeSub)
    const HWFloat<11,53> &id511in_a = id510out_result;
    const HWFloat<11,53> &id511in_b = id887out_output[getCycle()%11];

    id511out_result = (sub_float(id511in_a,id511in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<11,53> id513out_result;

  { // Node ID: 513 (NodeMul)
    const HWFloat<11,53> &id513in_a = id511out_result;
    const HWFloat<11,53> &id513in_b = id3out_value;

    id513out_result = (mul_float(id513in_a,id513in_b));
  }
  HWFloat<11,53> id516out_result;

  { // Node ID: 516 (NodeMul)
    const HWFloat<11,53> &id516in_a = id18out_dt;
    const HWFloat<11,53> &id516in_b = id513out_result;

    id516out_result = (mul_float(id516in_a,id516in_b));
  }
  HWFloat<11,53> id517out_result;

  { // Node ID: 517 (NodeAdd)
    const HWFloat<11,53> &id517in_a = id887out_output[getCycle()%11];
    const HWFloat<11,53> &id517in_b = id516out_result;

    id517out_result = (add_float(id517in_a,id517in_b));
  }
  HWFloat<11,53> id884out_output;

  { // Node ID: 884 (NodeStreamOffset)
    const HWFloat<11,53> &id884in_input = id517out_result;

    id884out_output = id884in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id550out_result[getCycle()%2];
    const HWFloat<11,53> &id27in_option0 = id884out_output;
    const HWFloat<11,53> &id27in_option1 = id26out_value;

    HWFloat<11,53> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 887 (NodeFIFO)
    const HWFloat<11,53> &id887in_input = id27out_result[getCycle()%2];

    id887out_output[(getCycle()+10)%11] = id887in_input;
  }
  HWFloat<11,53> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<11,53> &id32in_a = id887out_output[getCycle()%11];
    const HWFloat<11,53> &id32in_b = id887out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<11,53> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<11,53> &id33in_a = id887out_output[getCycle()%11];
    const HWFloat<11,53> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<11,53> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<11,53> &id34in_a = id33out_result;
    const HWFloat<11,53> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<11,53> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<11,53> &id35in_a = id34out_result;
    const HWFloat<11,53> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<11,53> id504out_result;

  { // Node ID: 504 (NodeSub)
    const HWFloat<11,53> &id504in_a = id6out_value;
    const HWFloat<11,53> &id504in_b = id35out_result;

    id504out_result = (sub_float(id504in_a,id504in_b));
  }
  { // Node ID: 929 (NodeConstantRawBits)
  }
  { // Node ID: 928 (NodeConstantRawBits)
  }
  { // Node ID: 927 (NodeConstantRawBits)
  }
  HWRawBits<11> id473out_result;

  { // Node ID: 473 (NodeSlice)
    const HWFloat<11,53> &id473in_a = id852out_floatOut[getCycle()%2];

    id473out_result = (slice<52,11>(id473in_a));
  }
  { // Node ID: 474 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id551out_result;

  { // Node ID: 551 (NodeEqInlined)
    const HWRawBits<11> &id551in_a = id473out_result;
    const HWRawBits<11> &id551in_b = id474out_value;

    id551out_result = (eq_bits(id551in_a,id551in_b));
  }
  HWRawBits<52> id472out_result;

  { // Node ID: 472 (NodeSlice)
    const HWFloat<11,53> &id472in_a = id852out_floatOut[getCycle()%2];

    id472out_result = (slice<0,52>(id472in_a));
  }
  { // Node ID: 926 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id552out_result;

  { // Node ID: 552 (NodeNeqInlined)
    const HWRawBits<52> &id552in_a = id472out_result;
    const HWRawBits<52> &id552in_b = id926out_value;

    id552out_result = (neq_bits(id552in_a,id552in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id478out_result;

  { // Node ID: 478 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id478in_a = id551out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id478in_b = id552out_result;

    HWOffsetFix<1,0,UNSIGNED> id478x_1;

    (id478x_1) = (and_fixed(id478in_a,id478in_b));
    id478out_result = (id478x_1);
  }
  { // Node ID: 900 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id900in_input = id478out_result;

    id900out_output[(getCycle()+8)%9] = id900in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id553out_result;
  HWOffsetFix<1,0,UNSIGNED> id553out_result_doubt;

  { // Node ID: 553 (NodeSlice)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id553in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id553in_a_doubt = id43out_o_doubt[getCycle()%7];

    id553out_result = (slice<63,1>(id553in_a));
    id553out_result_doubt = id553in_a_doubt;
  }
  HWRawBits<1> id554out_result;
  HWOffsetFix<1,0,UNSIGNED> id554out_result_doubt;

  { // Node ID: 554 (NodeNot)
    const HWRawBits<1> &id554in_a = id553out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id554in_a_doubt = id553out_result_doubt;

    id554out_result = (not_bits(id554in_a));
    id554out_result_doubt = id554in_a_doubt;
  }
  HWRawBits<64> id617out_result;
  HWOffsetFix<1,0,UNSIGNED> id617out_result_doubt;

  { // Node ID: 617 (NodeCat)
    const HWRawBits<1> &id617in_in0 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in0_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in1 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in1_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in2 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in2_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in3 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in3_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in4 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in4_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in5 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in5_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in6 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in6_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in7 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in7_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in8 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in8_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in9 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in9_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in10 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in10_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in11 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in11_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in12 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in12_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in13 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in13_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in14 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in14_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in15 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in15_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in16 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in16_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in17 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in17_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in18 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in18_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in19 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in19_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in20 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in20_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in21 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in21_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in22 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in22_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in23 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in23_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in24 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in24_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in25 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in25_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in26 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in26_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in27 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in27_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in28 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in28_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in29 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in29_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in30 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in30_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in31 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in31_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in32 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in32_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in33 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in33_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in34 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in34_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in35 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in35_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in36 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in36_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in37 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in37_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in38 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in38_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in39 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in39_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in40 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in40_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in41 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in41_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in42 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in42_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in43 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in43_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in44 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in44_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in45 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in45_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in46 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in46_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in47 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in47_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in48 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in48_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in49 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in49_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in50 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in50_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in51 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in51_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in52 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in52_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in53 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in53_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in54 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in54_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in55 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in55_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in56 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in56_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in57 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in57_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in58 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in58_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in59 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in59_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in60 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in60_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in61 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in61_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in62 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in62_doubt = id554out_result_doubt;
    const HWRawBits<1> &id617in_in63 = id554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_in63_doubt = id554out_result_doubt;

    id617out_result = (cat((cat((cat((cat((cat((cat(id617in_in0,id617in_in1)),(cat(id617in_in2,id617in_in3)))),(cat((cat(id617in_in4,id617in_in5)),(cat(id617in_in6,id617in_in7)))))),(cat((cat((cat(id617in_in8,id617in_in9)),(cat(id617in_in10,id617in_in11)))),(cat((cat(id617in_in12,id617in_in13)),(cat(id617in_in14,id617in_in15)))))))),(cat((cat((cat((cat(id617in_in16,id617in_in17)),(cat(id617in_in18,id617in_in19)))),(cat((cat(id617in_in20,id617in_in21)),(cat(id617in_in22,id617in_in23)))))),(cat((cat((cat(id617in_in24,id617in_in25)),(cat(id617in_in26,id617in_in27)))),(cat((cat(id617in_in28,id617in_in29)),(cat(id617in_in30,id617in_in31)))))))))),(cat((cat((cat((cat((cat(id617in_in32,id617in_in33)),(cat(id617in_in34,id617in_in35)))),(cat((cat(id617in_in36,id617in_in37)),(cat(id617in_in38,id617in_in39)))))),(cat((cat((cat(id617in_in40,id617in_in41)),(cat(id617in_in42,id617in_in43)))),(cat((cat(id617in_in44,id617in_in45)),(cat(id617in_in46,id617in_in47)))))))),(cat((cat((cat((cat(id617in_in48,id617in_in49)),(cat(id617in_in50,id617in_in51)))),(cat((cat(id617in_in52,id617in_in53)),(cat(id617in_in54,id617in_in55)))))),(cat((cat((cat(id617in_in56,id617in_in57)),(cat(id617in_in58,id617in_in59)))),(cat((cat(id617in_in60,id617in_in61)),(cat(id617in_in62,id617in_in63))))))))))));
    id617out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id617in_in0_doubt,id617in_in1_doubt)),id617in_in2_doubt)),id617in_in3_doubt)),id617in_in4_doubt)),id617in_in5_doubt)),id617in_in6_doubt)),id617in_in7_doubt)),id617in_in8_doubt)),id617in_in9_doubt)),id617in_in10_doubt)),id617in_in11_doubt)),id617in_in12_doubt)),id617in_in13_doubt)),id617in_in14_doubt)),id617in_in15_doubt)),id617in_in16_doubt)),id617in_in17_doubt)),id617in_in18_doubt)),id617in_in19_doubt)),id617in_in20_doubt)),id617in_in21_doubt)),id617in_in22_doubt)),id617in_in23_doubt)),id617in_in24_doubt)),id617in_in25_doubt)),id617in_in26_doubt)),id617in_in27_doubt)),id617in_in28_doubt)),id617in_in29_doubt)),id617in_in30_doubt)),id617in_in31_doubt)),id617in_in32_doubt)),id617in_in33_doubt)),id617in_in34_doubt)),id617in_in35_doubt)),id617in_in36_doubt)),id617in_in37_doubt)),id617in_in38_doubt)),id617in_in39_doubt)),id617in_in40_doubt)),id617in_in41_doubt)),id617in_in42_doubt)),id617in_in43_doubt)),id617in_in44_doubt)),id617in_in45_doubt)),id617in_in46_doubt)),id617in_in47_doubt)),id617in_in48_doubt)),id617in_in49_doubt)),id617in_in50_doubt)),id617in_in51_doubt)),id617in_in52_doubt)),id617in_in53_doubt)),id617in_in54_doubt)),id617in_in55_doubt)),id617in_in56_doubt)),id617in_in57_doubt)),id617in_in58_doubt)),id617in_in59_doubt)),id617in_in60_doubt)),id617in_in61_doubt)),id617in_in62_doubt)),id617in_in63_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id112out_output;
  HWOffsetFix<1,0,UNSIGNED> id112out_output_doubt;

  { // Node ID: 112 (NodeReinterpret)
    const HWRawBits<64> &id112in_input = id617out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id112in_input_doubt = id617out_result_doubt;

    id112out_output = (cast_bits2fixed<64,-51,TWOSCOMPLEMENT>(id112in_input));
    id112out_output_doubt = id112in_input_doubt;
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id113out_result;
  HWOffsetFix<1,0,UNSIGNED> id113out_result_doubt;

  { // Node ID: 113 (NodeXor)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id113in_a = id46out_value;
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id113in_b = id112out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id113in_b_doubt = id112out_output_doubt;

    HWOffsetFix<64,-51,TWOSCOMPLEMENT> id113x_1;

    (id113x_1) = (xor_fixed(id113in_a,id113in_b));
    id113out_result = (id113x_1);
    id113out_result_doubt = id113in_b_doubt;
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id114out_result;
  HWOffsetFix<1,0,UNSIGNED> id114out_result_doubt;

  { // Node ID: 114 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id114in_sel = id45out_output;
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id114in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id114in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id114in_option1 = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id114in_option1_doubt = id113out_result_doubt;

    HWOffsetFix<64,-51,TWOSCOMPLEMENT> id114x_1;
    HWOffsetFix<1,0,UNSIGNED> id114x_2;

    switch((id114in_sel.getValueAsLong())) {
      case 0l:
        id114x_1 = id114in_option0;
        break;
      case 1l:
        id114x_1 = id114in_option1;
        break;
      default:
        id114x_1 = (c_hw_fix_64_n51_sgn_undef);
        break;
    }
    switch((id114in_sel.getValueAsLong())) {
      case 0l:
        id114x_2 = id114in_option0_doubt;
        break;
      case 1l:
        id114x_2 = id114in_option1_doubt;
        break;
      default:
        id114x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id114out_result = (id114x_1);
    id114out_result_doubt = (id114x_2);
  }
  { // Node ID: 117 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id116out_result;
  HWOffsetFix<1,0,UNSIGNED> id116out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id116out_exception;

  { // Node ID: 116 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id116in_a = id114out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id116in_a_doubt = id114out_result_doubt;
    const HWOffsetFix<52,-51,UNSIGNED> &id116in_b = id117out_value;

    HWOffsetFix<1,0,UNSIGNED> id116x_1;

    id116out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id116in_a,id116in_b,(&(id116x_1))));
    id116out_result_doubt = (or_fixed((neq_fixed((id116x_1),(c_hw_fix_1_0_uns_bits_1))),id116in_a_doubt));
    id116out_exception = (id116x_1);
  }
  { // Node ID: 119 (NodeConstantRawBits)
  }
  HWRawBits<1> id618out_result;
  HWOffsetFix<1,0,UNSIGNED> id618out_result_doubt;

  { // Node ID: 618 (NodeSlice)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id618in_a = id116out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id618in_a_doubt = id116out_result_doubt;

    id618out_result = (slice<115,1>(id618in_a));
    id618out_result_doubt = id618in_a_doubt;
  }
  HWRawBits<1> id619out_result;
  HWOffsetFix<1,0,UNSIGNED> id619out_result_doubt;

  { // Node ID: 619 (NodeNot)
    const HWRawBits<1> &id619in_a = id618out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id619in_a_doubt = id618out_result_doubt;

    id619out_result = (not_bits(id619in_a));
    id619out_result_doubt = id619in_a_doubt;
  }
  HWRawBits<116> id734out_result;
  HWOffsetFix<1,0,UNSIGNED> id734out_result_doubt;

  { // Node ID: 734 (NodeCat)
    const HWRawBits<1> &id734in_in0 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in0_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in1 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in1_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in2 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in2_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in3 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in3_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in4 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in4_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in5 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in5_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in6 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in6_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in7 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in7_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in8 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in8_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in9 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in9_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in10 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in10_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in11 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in11_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in12 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in12_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in13 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in13_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in14 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in14_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in15 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in15_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in16 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in16_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in17 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in17_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in18 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in18_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in19 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in19_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in20 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in20_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in21 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in21_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in22 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in22_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in23 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in23_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in24 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in24_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in25 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in25_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in26 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in26_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in27 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in27_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in28 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in28_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in29 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in29_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in30 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in30_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in31 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in31_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in32 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in32_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in33 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in33_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in34 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in34_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in35 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in35_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in36 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in36_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in37 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in37_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in38 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in38_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in39 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in39_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in40 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in40_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in41 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in41_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in42 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in42_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in43 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in43_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in44 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in44_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in45 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in45_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in46 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in46_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in47 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in47_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in48 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in48_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in49 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in49_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in50 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in50_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in51 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in51_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in52 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in52_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in53 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in53_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in54 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in54_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in55 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in55_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in56 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in56_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in57 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in57_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in58 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in58_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in59 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in59_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in60 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in60_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in61 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in61_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in62 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in62_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in63 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in63_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in64 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in64_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in65 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in65_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in66 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in66_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in67 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in67_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in68 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in68_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in69 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in69_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in70 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in70_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in71 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in71_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in72 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in72_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in73 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in73_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in74 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in74_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in75 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in75_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in76 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in76_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in77 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in77_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in78 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in78_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in79 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in79_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in80 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in80_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in81 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in81_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in82 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in82_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in83 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in83_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in84 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in84_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in85 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in85_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in86 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in86_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in87 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in87_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in88 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in88_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in89 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in89_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in90 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in90_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in91 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in91_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in92 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in92_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in93 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in93_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in94 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in94_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in95 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in95_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in96 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in96_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in97 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in97_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in98 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in98_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in99 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in99_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in100 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in100_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in101 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in101_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in102 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in102_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in103 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in103_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in104 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in104_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in105 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in105_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in106 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in106_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in107 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in107_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in108 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in108_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in109 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in109_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in110 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in110_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in111 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in111_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in112 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in112_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in113 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in113_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in114 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in114_doubt = id619out_result_doubt;
    const HWRawBits<1> &id734in_in115 = id619out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_in115_doubt = id619out_result_doubt;

    id734out_result = (cat((cat((cat((cat((cat((cat((cat(id734in_in0,id734in_in1)),(cat(id734in_in2,id734in_in3)))),(cat((cat(id734in_in4,id734in_in5)),(cat(id734in_in6,id734in_in7)))))),(cat((cat((cat(id734in_in8,id734in_in9)),(cat(id734in_in10,id734in_in11)))),(cat((cat(id734in_in12,id734in_in13)),id734in_in14)))))),(cat((cat((cat((cat(id734in_in15,id734in_in16)),(cat(id734in_in17,id734in_in18)))),(cat((cat(id734in_in19,id734in_in20)),id734in_in21)))),(cat((cat((cat(id734in_in22,id734in_in23)),(cat(id734in_in24,id734in_in25)))),(cat((cat(id734in_in26,id734in_in27)),id734in_in28)))))))),(cat((cat((cat((cat((cat(id734in_in29,id734in_in30)),(cat(id734in_in31,id734in_in32)))),(cat((cat(id734in_in33,id734in_in34)),(cat(id734in_in35,id734in_in36)))))),(cat((cat((cat(id734in_in37,id734in_in38)),(cat(id734in_in39,id734in_in40)))),(cat((cat(id734in_in41,id734in_in42)),id734in_in43)))))),(cat((cat((cat((cat(id734in_in44,id734in_in45)),(cat(id734in_in46,id734in_in47)))),(cat((cat(id734in_in48,id734in_in49)),id734in_in50)))),(cat((cat((cat(id734in_in51,id734in_in52)),(cat(id734in_in53,id734in_in54)))),(cat((cat(id734in_in55,id734in_in56)),id734in_in57)))))))))),(cat((cat((cat((cat((cat((cat(id734in_in58,id734in_in59)),(cat(id734in_in60,id734in_in61)))),(cat((cat(id734in_in62,id734in_in63)),(cat(id734in_in64,id734in_in65)))))),(cat((cat((cat(id734in_in66,id734in_in67)),(cat(id734in_in68,id734in_in69)))),(cat((cat(id734in_in70,id734in_in71)),id734in_in72)))))),(cat((cat((cat((cat(id734in_in73,id734in_in74)),(cat(id734in_in75,id734in_in76)))),(cat((cat(id734in_in77,id734in_in78)),id734in_in79)))),(cat((cat((cat(id734in_in80,id734in_in81)),(cat(id734in_in82,id734in_in83)))),(cat((cat(id734in_in84,id734in_in85)),id734in_in86)))))))),(cat((cat((cat((cat((cat(id734in_in87,id734in_in88)),(cat(id734in_in89,id734in_in90)))),(cat((cat(id734in_in91,id734in_in92)),(cat(id734in_in93,id734in_in94)))))),(cat((cat((cat(id734in_in95,id734in_in96)),(cat(id734in_in97,id734in_in98)))),(cat((cat(id734in_in99,id734in_in100)),id734in_in101)))))),(cat((cat((cat((cat(id734in_in102,id734in_in103)),(cat(id734in_in104,id734in_in105)))),(cat((cat(id734in_in106,id734in_in107)),id734in_in108)))),(cat((cat((cat(id734in_in109,id734in_in110)),(cat(id734in_in111,id734in_in112)))),(cat((cat(id734in_in113,id734in_in114)),id734in_in115))))))))))));
    id734out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id734in_in0_doubt,id734in_in1_doubt)),id734in_in2_doubt)),id734in_in3_doubt)),id734in_in4_doubt)),id734in_in5_doubt)),id734in_in6_doubt)),id734in_in7_doubt)),id734in_in8_doubt)),id734in_in9_doubt)),id734in_in10_doubt)),id734in_in11_doubt)),id734in_in12_doubt)),id734in_in13_doubt)),id734in_in14_doubt)),id734in_in15_doubt)),id734in_in16_doubt)),id734in_in17_doubt)),id734in_in18_doubt)),id734in_in19_doubt)),id734in_in20_doubt)),id734in_in21_doubt)),id734in_in22_doubt)),id734in_in23_doubt)),id734in_in24_doubt)),id734in_in25_doubt)),id734in_in26_doubt)),id734in_in27_doubt)),id734in_in28_doubt)),id734in_in29_doubt)),id734in_in30_doubt)),id734in_in31_doubt)),id734in_in32_doubt)),id734in_in33_doubt)),id734in_in34_doubt)),id734in_in35_doubt)),id734in_in36_doubt)),id734in_in37_doubt)),id734in_in38_doubt)),id734in_in39_doubt)),id734in_in40_doubt)),id734in_in41_doubt)),id734in_in42_doubt)),id734in_in43_doubt)),id734in_in44_doubt)),id734in_in45_doubt)),id734in_in46_doubt)),id734in_in47_doubt)),id734in_in48_doubt)),id734in_in49_doubt)),id734in_in50_doubt)),id734in_in51_doubt)),id734in_in52_doubt)),id734in_in53_doubt)),id734in_in54_doubt)),id734in_in55_doubt)),id734in_in56_doubt)),id734in_in57_doubt)),id734in_in58_doubt)),id734in_in59_doubt)),id734in_in60_doubt)),id734in_in61_doubt)),id734in_in62_doubt)),id734in_in63_doubt)),id734in_in64_doubt)),id734in_in65_doubt)),id734in_in66_doubt)),id734in_in67_doubt)),id734in_in68_doubt)),id734in_in69_doubt)),id734in_in70_doubt)),id734in_in71_doubt)),id734in_in72_doubt)),id734in_in73_doubt)),id734in_in74_doubt)),id734in_in75_doubt)),id734in_in76_doubt)),id734in_in77_doubt)),id734in_in78_doubt)),id734in_in79_doubt)),id734in_in80_doubt)),id734in_in81_doubt)),id734in_in82_doubt)),id734in_in83_doubt)),id734in_in84_doubt)),id734in_in85_doubt)),id734in_in86_doubt)),id734in_in87_doubt)),id734in_in88_doubt)),id734in_in89_doubt)),id734in_in90_doubt)),id734in_in91_doubt)),id734in_in92_doubt)),id734in_in93_doubt)),id734in_in94_doubt)),id734in_in95_doubt)),id734in_in96_doubt)),id734in_in97_doubt)),id734in_in98_doubt)),id734in_in99_doubt)),id734in_in100_doubt)),id734in_in101_doubt)),id734in_in102_doubt)),id734in_in103_doubt)),id734in_in104_doubt)),id734in_in105_doubt)),id734in_in106_doubt)),id734in_in107_doubt)),id734in_in108_doubt)),id734in_in109_doubt)),id734in_in110_doubt)),id734in_in111_doubt)),id734in_in112_doubt)),id734in_in113_doubt)),id734in_in114_doubt)),id734in_in115_doubt));
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id237out_output;
  HWOffsetFix<1,0,UNSIGNED> id237out_output_doubt;

  { // Node ID: 237 (NodeReinterpret)
    const HWRawBits<116> &id237in_input = id734out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id237in_input_doubt = id734out_result_doubt;

    id237out_output = (cast_bits2fixed<116,-102,TWOSCOMPLEMENT>(id237in_input));
    id237out_output_doubt = id237in_input_doubt;
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id238out_result;
  HWOffsetFix<1,0,UNSIGNED> id238out_result_doubt;

  { // Node ID: 238 (NodeXor)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id238in_a = id119out_value;
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id238in_b = id237out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id238in_b_doubt = id237out_output_doubt;

    HWOffsetFix<116,-102,TWOSCOMPLEMENT> id238x_1;

    (id238x_1) = (xor_fixed(id238in_a,id238in_b));
    id238out_result = (id238x_1);
    id238out_result_doubt = id238in_b_doubt;
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id239out_result;
  HWOffsetFix<1,0,UNSIGNED> id239out_result_doubt;

  { // Node ID: 239 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id239in_sel = id116out_exception;
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id239in_option0 = id116out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id239in_option0_doubt = id116out_result_doubt;
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id239in_option1 = id238out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id239in_option1_doubt = id238out_result_doubt;

    HWOffsetFix<116,-102,TWOSCOMPLEMENT> id239x_1;
    HWOffsetFix<1,0,UNSIGNED> id239x_2;

    switch((id239in_sel.getValueAsLong())) {
      case 0l:
        id239x_1 = id239in_option0;
        break;
      case 1l:
        id239x_1 = id239in_option1;
        break;
      default:
        id239x_1 = (c_hw_fix_116_n102_sgn_undef);
        break;
    }
    switch((id239in_sel.getValueAsLong())) {
      case 0l:
        id239x_2 = id239in_option0_doubt;
        break;
      case 1l:
        id239x_2 = id239in_option1_doubt;
        break;
      default:
        id239x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id239out_result = (id239x_1);
    id239out_result_doubt = (id239x_2);
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id240out_o;
  HWOffsetFix<1,0,UNSIGNED> id240out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id240out_exception;

  { // Node ID: 240 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id240in_i = id239out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id240in_i_doubt = id239out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id240x_1;

    id240out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id240in_i,(&(id240x_1))));
    id240out_o_doubt = (or_fixed((neq_fixed((id240x_1),(c_hw_fix_1_0_uns_bits_1))),id240in_i_doubt));
    id240out_exception = (id240x_1);
  }
  { // Node ID: 242 (NodeConstantRawBits)
  }
  HWRawBits<1> id735out_result;
  HWOffsetFix<1,0,UNSIGNED> id735out_result_doubt;

  { // Node ID: 735 (NodeSlice)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id735in_a = id240out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id735in_a_doubt = id240out_o_doubt;

    id735out_result = (slice<63,1>(id735in_a));
    id735out_result_doubt = id735in_a_doubt;
  }
  HWRawBits<1> id736out_result;
  HWOffsetFix<1,0,UNSIGNED> id736out_result_doubt;

  { // Node ID: 736 (NodeNot)
    const HWRawBits<1> &id736in_a = id735out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id736in_a_doubt = id735out_result_doubt;

    id736out_result = (not_bits(id736in_a));
    id736out_result_doubt = id736in_a_doubt;
  }
  HWRawBits<64> id799out_result;
  HWOffsetFix<1,0,UNSIGNED> id799out_result_doubt;

  { // Node ID: 799 (NodeCat)
    const HWRawBits<1> &id799in_in0 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in0_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in1 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in1_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in2 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in2_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in3 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in3_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in4 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in4_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in5 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in5_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in6 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in6_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in7 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in7_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in8 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in8_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in9 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in9_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in10 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in10_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in11 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in11_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in12 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in12_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in13 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in13_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in14 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in14_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in15 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in15_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in16 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in16_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in17 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in17_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in18 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in18_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in19 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in19_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in20 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in20_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in21 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in21_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in22 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in22_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in23 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in23_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in24 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in24_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in25 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in25_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in26 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in26_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in27 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in27_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in28 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in28_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in29 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in29_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in30 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in30_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in31 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in31_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in32 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in32_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in33 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in33_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in34 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in34_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in35 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in35_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in36 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in36_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in37 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in37_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in38 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in38_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in39 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in39_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in40 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in40_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in41 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in41_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in42 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in42_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in43 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in43_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in44 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in44_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in45 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in45_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in46 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in46_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in47 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in47_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in48 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in48_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in49 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in49_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in50 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in50_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in51 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in51_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in52 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in52_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in53 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in53_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in54 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in54_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in55 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in55_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in56 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in56_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in57 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in57_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in58 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in58_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in59 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in59_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in60 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in60_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in61 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in61_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in62 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in62_doubt = id736out_result_doubt;
    const HWRawBits<1> &id799in_in63 = id736out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_in63_doubt = id736out_result_doubt;

    id799out_result = (cat((cat((cat((cat((cat((cat(id799in_in0,id799in_in1)),(cat(id799in_in2,id799in_in3)))),(cat((cat(id799in_in4,id799in_in5)),(cat(id799in_in6,id799in_in7)))))),(cat((cat((cat(id799in_in8,id799in_in9)),(cat(id799in_in10,id799in_in11)))),(cat((cat(id799in_in12,id799in_in13)),(cat(id799in_in14,id799in_in15)))))))),(cat((cat((cat((cat(id799in_in16,id799in_in17)),(cat(id799in_in18,id799in_in19)))),(cat((cat(id799in_in20,id799in_in21)),(cat(id799in_in22,id799in_in23)))))),(cat((cat((cat(id799in_in24,id799in_in25)),(cat(id799in_in26,id799in_in27)))),(cat((cat(id799in_in28,id799in_in29)),(cat(id799in_in30,id799in_in31)))))))))),(cat((cat((cat((cat((cat(id799in_in32,id799in_in33)),(cat(id799in_in34,id799in_in35)))),(cat((cat(id799in_in36,id799in_in37)),(cat(id799in_in38,id799in_in39)))))),(cat((cat((cat(id799in_in40,id799in_in41)),(cat(id799in_in42,id799in_in43)))),(cat((cat(id799in_in44,id799in_in45)),(cat(id799in_in46,id799in_in47)))))))),(cat((cat((cat((cat(id799in_in48,id799in_in49)),(cat(id799in_in50,id799in_in51)))),(cat((cat(id799in_in52,id799in_in53)),(cat(id799in_in54,id799in_in55)))))),(cat((cat((cat(id799in_in56,id799in_in57)),(cat(id799in_in58,id799in_in59)))),(cat((cat(id799in_in60,id799in_in61)),(cat(id799in_in62,id799in_in63))))))))))));
    id799out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id799in_in0_doubt,id799in_in1_doubt)),id799in_in2_doubt)),id799in_in3_doubt)),id799in_in4_doubt)),id799in_in5_doubt)),id799in_in6_doubt)),id799in_in7_doubt)),id799in_in8_doubt)),id799in_in9_doubt)),id799in_in10_doubt)),id799in_in11_doubt)),id799in_in12_doubt)),id799in_in13_doubt)),id799in_in14_doubt)),id799in_in15_doubt)),id799in_in16_doubt)),id799in_in17_doubt)),id799in_in18_doubt)),id799in_in19_doubt)),id799in_in20_doubt)),id799in_in21_doubt)),id799in_in22_doubt)),id799in_in23_doubt)),id799in_in24_doubt)),id799in_in25_doubt)),id799in_in26_doubt)),id799in_in27_doubt)),id799in_in28_doubt)),id799in_in29_doubt)),id799in_in30_doubt)),id799in_in31_doubt)),id799in_in32_doubt)),id799in_in33_doubt)),id799in_in34_doubt)),id799in_in35_doubt)),id799in_in36_doubt)),id799in_in37_doubt)),id799in_in38_doubt)),id799in_in39_doubt)),id799in_in40_doubt)),id799in_in41_doubt)),id799in_in42_doubt)),id799in_in43_doubt)),id799in_in44_doubt)),id799in_in45_doubt)),id799in_in46_doubt)),id799in_in47_doubt)),id799in_in48_doubt)),id799in_in49_doubt)),id799in_in50_doubt)),id799in_in51_doubt)),id799in_in52_doubt)),id799in_in53_doubt)),id799in_in54_doubt)),id799in_in55_doubt)),id799in_in56_doubt)),id799in_in57_doubt)),id799in_in58_doubt)),id799in_in59_doubt)),id799in_in60_doubt)),id799in_in61_doubt)),id799in_in62_doubt)),id799in_in63_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id308out_output;
  HWOffsetFix<1,0,UNSIGNED> id308out_output_doubt;

  { // Node ID: 308 (NodeReinterpret)
    const HWRawBits<64> &id308in_input = id799out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id308in_input_doubt = id799out_result_doubt;

    id308out_output = (cast_bits2fixed<64,-51,TWOSCOMPLEMENT>(id308in_input));
    id308out_output_doubt = id308in_input_doubt;
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id309out_result;
  HWOffsetFix<1,0,UNSIGNED> id309out_result_doubt;

  { // Node ID: 309 (NodeXor)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id309in_a = id242out_value;
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id309in_b = id308out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id309in_b_doubt = id308out_output_doubt;

    HWOffsetFix<64,-51,TWOSCOMPLEMENT> id309x_1;

    (id309x_1) = (xor_fixed(id309in_a,id309in_b));
    id309out_result = (id309x_1);
    id309out_result_doubt = id309in_b_doubt;
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id310out_result;
  HWOffsetFix<1,0,UNSIGNED> id310out_result_doubt;

  { // Node ID: 310 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id310in_sel = id240out_exception;
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id310in_option0 = id240out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_option0_doubt = id240out_o_doubt;
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id310in_option1 = id309out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_option1_doubt = id309out_result_doubt;

    HWOffsetFix<64,-51,TWOSCOMPLEMENT> id310x_1;
    HWOffsetFix<1,0,UNSIGNED> id310x_2;

    switch((id310in_sel.getValueAsLong())) {
      case 0l:
        id310x_1 = id310in_option0;
        break;
      case 1l:
        id310x_1 = id310in_option1;
        break;
      default:
        id310x_1 = (c_hw_fix_64_n51_sgn_undef);
        break;
    }
    switch((id310in_sel.getValueAsLong())) {
      case 0l:
        id310x_2 = id310in_option0_doubt;
        break;
      case 1l:
        id310x_2 = id310in_option1_doubt;
        break;
      default:
        id310x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id310out_result = (id310x_1);
    id310out_result_doubt = (id310x_2);
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id319out_output;

  { // Node ID: 319 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id319in_input = id310out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id319in_input_doubt = id310out_result_doubt;

    id319out_output = id319in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id320out_o;

  { // Node ID: 320 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id320in_i = id319out_output;

    id320out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id320in_i));
  }
  { // Node ID: 892 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id892in_input = id320out_o;

    id892out_output[(getCycle()+2)%3] = id892in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id383out_o;

  { // Node ID: 383 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id383in_i = id892out_output[getCycle()%3];

    id383out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id383in_i));
  }
  { // Node ID: 925 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id385out_result;
  HWOffsetFix<1,0,UNSIGNED> id385out_exception;

  { // Node ID: 385 (NodeAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id385in_a = id383out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id385in_b = id925out_value;

    HWOffsetFix<1,0,UNSIGNED> id385x_1;

    id385out_result = (add_fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id385in_a,id385in_b,(&(id385x_1))));
    id385out_exception = (id385x_1);
  }
  { // Node ID: 387 (NodeConstantRawBits)
  }
  HWRawBits<1> id800out_result;

  { // Node ID: 800 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id800in_a = id385out_result;

    id800out_result = (slice<13,1>(id800in_a));
  }
  HWRawBits<1> id801out_result;

  { // Node ID: 801 (NodeNot)
    const HWRawBits<1> &id801in_a = id800out_result;

    id801out_result = (not_bits(id801in_a));
  }
  HWRawBits<14> id814out_result;

  { // Node ID: 814 (NodeCat)
    const HWRawBits<1> &id814in_in0 = id801out_result;
    const HWRawBits<1> &id814in_in1 = id801out_result;
    const HWRawBits<1> &id814in_in2 = id801out_result;
    const HWRawBits<1> &id814in_in3 = id801out_result;
    const HWRawBits<1> &id814in_in4 = id801out_result;
    const HWRawBits<1> &id814in_in5 = id801out_result;
    const HWRawBits<1> &id814in_in6 = id801out_result;
    const HWRawBits<1> &id814in_in7 = id801out_result;
    const HWRawBits<1> &id814in_in8 = id801out_result;
    const HWRawBits<1> &id814in_in9 = id801out_result;
    const HWRawBits<1> &id814in_in10 = id801out_result;
    const HWRawBits<1> &id814in_in11 = id801out_result;
    const HWRawBits<1> &id814in_in12 = id801out_result;
    const HWRawBits<1> &id814in_in13 = id801out_result;

    id814out_result = (cat((cat((cat((cat(id814in_in0,id814in_in1)),(cat(id814in_in2,id814in_in3)))),(cat((cat(id814in_in4,id814in_in5)),id814in_in6)))),(cat((cat((cat(id814in_in7,id814in_in8)),(cat(id814in_in9,id814in_in10)))),(cat((cat(id814in_in11,id814in_in12)),id814in_in13))))));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id403out_output;

  { // Node ID: 403 (NodeReinterpret)
    const HWRawBits<14> &id403in_input = id814out_result;

    id403out_output = (cast_bits2fixed<14,0,TWOSCOMPLEMENT>(id403in_input));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id404out_result;

  { // Node ID: 404 (NodeXor)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id404in_a = id387out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id404in_b = id403out_output;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id404x_1;

    (id404x_1) = (xor_fixed(id404in_a,id404in_b));
    id404out_result = (id404x_1);
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id405out_result;

  { // Node ID: 405 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id405in_sel = id385out_exception;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id405in_option0 = id385out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id405in_option1 = id404out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id405x_1;

    switch((id405in_sel.getValueAsLong())) {
      case 0l:
        id405x_1 = id405in_option0;
        break;
      case 1l:
        id405x_1 = id405in_option1;
        break;
      default:
        id405x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    id405out_result = (id405x_1);
  }
  { // Node ID: 406 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id323out_o;

  { // Node ID: 323 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id323in_i = id319out_output;

    id323out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id323in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id488out_output;

  { // Node ID: 488 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id488in_input = id323out_o;

    id488out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id488in_input))));
  }
  { // Node ID: 489 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id489in_addr = id488out_output;

    HWOffsetFix<38,-53,UNSIGNED> id489x_1;

    switch(((long)((id489in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id489x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id489x_1 = (id489sta_rom_store[(id489in_addr.getValueAsLong())]);
        break;
      default:
        id489x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id489out_dout[(getCycle()+2)%3] = (id489x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id322out_o;

  { // Node ID: 322 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id322in_i = id319out_output;

    id322out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id322in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id485out_output;

  { // Node ID: 485 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id485in_input = id322out_o;

    id485out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id485in_input))));
  }
  { // Node ID: 486 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id486in_addr = id485out_output;

    HWOffsetFix<48,-53,UNSIGNED> id486x_1;

    switch(((long)((id486in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id486x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id486x_1 = (id486sta_rom_store[(id486in_addr.getValueAsLong())]);
        break;
      default:
        id486x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id486out_dout[(getCycle()+2)%3] = (id486x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id321out_o;

  { // Node ID: 321 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id321in_i = id319out_output;

    id321out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id321in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id482out_output;

  { // Node ID: 482 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id482in_input = id321out_o;

    id482out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id482in_input))));
  }
  { // Node ID: 483 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id483in_addr = id482out_output;

    HWOffsetFix<53,-53,UNSIGNED> id483x_1;

    switch(((long)((id483in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id483x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id483x_1 = (id483sta_rom_store[(id483in_addr.getValueAsLong())]);
        break;
      default:
        id483x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id483out_dout[(getCycle()+2)%3] = (id483x_1);
  }
  { // Node ID: 327 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id324out_o;

  { // Node ID: 324 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id324in_i = id319out_output;

    id324out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id324in_i));
  }
  { // Node ID: 893 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id893in_input = id324out_o;

    id893out_output[(getCycle()+2)%3] = id893in_input;
  }
  HWOffsetFix<47,-72,UNSIGNED> id326out_result;

  { // Node ID: 326 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id326in_a = id327out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id326in_b = id893out_output[getCycle()%3];

    id326out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id326in_a,id326in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id328out_o;
  HWOffsetFix<1,0,UNSIGNED> id328out_exception;

  { // Node ID: 328 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id328in_i = id326out_result;

    HWOffsetFix<1,0,UNSIGNED> id328x_1;

    id328out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id328in_i,(&(id328x_1))));
    id328out_exception = (id328x_1);
  }
  { // Node ID: 330 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id331out_result;

  { // Node ID: 331 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_sel = id328out_exception;
    const HWOffsetFix<26,-51,UNSIGNED> &id331in_option0 = id328out_o;
    const HWOffsetFix<26,-51,UNSIGNED> &id331in_option1 = id330out_value;

    HWOffsetFix<26,-51,UNSIGNED> id331x_1;

    switch((id331in_sel.getValueAsLong())) {
      case 0l:
        id331x_1 = id331in_option0;
        break;
      case 1l:
        id331x_1 = id331in_option1;
        break;
      default:
        id331x_1 = (c_hw_fix_26_n51_uns_undef);
        break;
    }
    id331out_result = (id331x_1);
  }
  HWOffsetFix<54,-53,UNSIGNED> id332out_result;

  { // Node ID: 332 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id332in_a = id483out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id332in_b = id331out_result;

    id332out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id332in_a,id332in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id333out_result;
  HWOffsetFix<1,0,UNSIGNED> id333out_exception;

  { // Node ID: 333 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id333in_a = id331out_result;
    const HWOffsetFix<53,-53,UNSIGNED> &id333in_b = id483out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id333x_1;

    id333out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id333in_a,id333in_b,(&(id333x_1))));
    id333out_exception = (id333x_1);
  }
  { // Node ID: 335 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-89,UNSIGNED> id336out_result;

  { // Node ID: 336 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id336in_sel = id333out_exception;
    const HWOffsetFix<64,-89,UNSIGNED> &id336in_option0 = id333out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id336in_option1 = id335out_value;

    HWOffsetFix<64,-89,UNSIGNED> id336x_1;

    switch((id336in_sel.getValueAsLong())) {
      case 0l:
        id336x_1 = id336in_option0;
        break;
      case 1l:
        id336x_1 = id336in_option1;
        break;
      default:
        id336x_1 = (c_hw_fix_64_n89_uns_undef);
        break;
    }
    id336out_result = (id336x_1);
  }
  HWOffsetFix<64,-63,UNSIGNED> id337out_result;
  HWOffsetFix<1,0,UNSIGNED> id337out_exception;

  { // Node ID: 337 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id337in_a = id332out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id337in_b = id336out_result;

    HWOffsetFix<1,0,UNSIGNED> id337x_1;

    id337out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id337in_a,id337in_b,(&(id337x_1))));
    id337out_exception = (id337x_1);
  }
  { // Node ID: 339 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-63,UNSIGNED> id340out_result;

  { // Node ID: 340 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id340in_sel = id337out_exception;
    const HWOffsetFix<64,-63,UNSIGNED> &id340in_option0 = id337out_result;
    const HWOffsetFix<64,-63,UNSIGNED> &id340in_option1 = id339out_value;

    HWOffsetFix<64,-63,UNSIGNED> id340x_1;

    switch((id340in_sel.getValueAsLong())) {
      case 0l:
        id340x_1 = id340in_option0;
        break;
      case 1l:
        id340x_1 = id340in_option1;
        break;
      default:
        id340x_1 = (c_hw_fix_64_n63_uns_undef);
        break;
    }
    id340out_result = (id340x_1);
  }
  HWOffsetFix<58,-57,UNSIGNED> id341out_o;
  HWOffsetFix<1,0,UNSIGNED> id341out_exception;

  { // Node ID: 341 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id341in_i = id340out_result;

    HWOffsetFix<1,0,UNSIGNED> id341x_1;

    id341out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id341in_i,(&(id341x_1))));
    id341out_exception = (id341x_1);
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  HWOffsetFix<58,-57,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id341out_exception;
    const HWOffsetFix<58,-57,UNSIGNED> &id344in_option0 = id341out_o;
    const HWOffsetFix<58,-57,UNSIGNED> &id344in_option1 = id343out_value;

    HWOffsetFix<58,-57,UNSIGNED> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_fix_58_n57_uns_undef);
        break;
    }
    id344out_result = (id344x_1);
  }
  HWOffsetFix<59,-57,UNSIGNED> id345out_result;

  { // Node ID: 345 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id345in_a = id486out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id345in_b = id344out_result;

    id345out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id345in_a,id345in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id346out_result;
  HWOffsetFix<1,0,UNSIGNED> id346out_exception;

  { // Node ID: 346 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id346in_a = id344out_result;
    const HWOffsetFix<48,-53,UNSIGNED> &id346in_b = id486out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id346x_1;

    id346out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id346in_a,id346in_b,(&(id346x_1))));
    id346out_exception = (id346x_1);
  }
  { // Node ID: 348 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-68,UNSIGNED> id349out_result;

  { // Node ID: 349 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id349in_sel = id346out_exception;
    const HWOffsetFix<64,-68,UNSIGNED> &id349in_option0 = id346out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id349in_option1 = id348out_value;

    HWOffsetFix<64,-68,UNSIGNED> id349x_1;

    switch((id349in_sel.getValueAsLong())) {
      case 0l:
        id349x_1 = id349in_option0;
        break;
      case 1l:
        id349x_1 = id349in_option1;
        break;
      default:
        id349x_1 = (c_hw_fix_64_n68_uns_undef);
        break;
    }
    id349out_result = (id349x_1);
  }
  HWOffsetFix<64,-62,UNSIGNED> id350out_result;
  HWOffsetFix<1,0,UNSIGNED> id350out_exception;

  { // Node ID: 350 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id350in_a = id345out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id350in_b = id349out_result;

    HWOffsetFix<1,0,UNSIGNED> id350x_1;

    id350out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id350in_a,id350in_b,(&(id350x_1))));
    id350out_exception = (id350x_1);
  }
  { // Node ID: 352 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-62,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id353in_sel = id350out_exception;
    const HWOffsetFix<64,-62,UNSIGNED> &id353in_option0 = id350out_result;
    const HWOffsetFix<64,-62,UNSIGNED> &id353in_option1 = id352out_value;

    HWOffsetFix<64,-62,UNSIGNED> id353x_1;

    switch((id353in_sel.getValueAsLong())) {
      case 0l:
        id353x_1 = id353in_option0;
        break;
      case 1l:
        id353x_1 = id353in_option1;
        break;
      default:
        id353x_1 = (c_hw_fix_64_n62_uns_undef);
        break;
    }
    id353out_result = (id353x_1);
  }
  HWOffsetFix<59,-57,UNSIGNED> id354out_o;
  HWOffsetFix<1,0,UNSIGNED> id354out_exception;

  { // Node ID: 354 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id354in_i = id353out_result;

    HWOffsetFix<1,0,UNSIGNED> id354x_1;

    id354out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id354in_i,(&(id354x_1))));
    id354out_exception = (id354x_1);
  }
  { // Node ID: 356 (NodeConstantRawBits)
  }
  HWOffsetFix<59,-57,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id357in_sel = id354out_exception;
    const HWOffsetFix<59,-57,UNSIGNED> &id357in_option0 = id354out_o;
    const HWOffsetFix<59,-57,UNSIGNED> &id357in_option1 = id356out_value;

    HWOffsetFix<59,-57,UNSIGNED> id357x_1;

    switch((id357in_sel.getValueAsLong())) {
      case 0l:
        id357x_1 = id357in_option0;
        break;
      case 1l:
        id357x_1 = id357in_option1;
        break;
      default:
        id357x_1 = (c_hw_fix_59_n57_uns_undef);
        break;
    }
    id357out_result = (id357x_1);
  }
  HWOffsetFix<60,-57,UNSIGNED> id358out_result;

  { // Node ID: 358 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id358in_a = id489out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id358in_b = id357out_result;

    id358out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id358in_a,id358in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id359out_result;
  HWOffsetFix<1,0,UNSIGNED> id359out_exception;

  { // Node ID: 359 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id359in_a = id357out_result;
    const HWOffsetFix<38,-53,UNSIGNED> &id359in_b = id489out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id359x_1;

    id359out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id359in_a,id359in_b,(&(id359x_1))));
    id359out_exception = (id359x_1);
  }
  { // Node ID: 361 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-77,UNSIGNED> id362out_result;

  { // Node ID: 362 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id362in_sel = id359out_exception;
    const HWOffsetFix<64,-77,UNSIGNED> &id362in_option0 = id359out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id362in_option1 = id361out_value;

    HWOffsetFix<64,-77,UNSIGNED> id362x_1;

    switch((id362in_sel.getValueAsLong())) {
      case 0l:
        id362x_1 = id362in_option0;
        break;
      case 1l:
        id362x_1 = id362in_option1;
        break;
      default:
        id362x_1 = (c_hw_fix_64_n77_uns_undef);
        break;
    }
    id362out_result = (id362x_1);
  }
  HWOffsetFix<64,-61,UNSIGNED> id363out_result;
  HWOffsetFix<1,0,UNSIGNED> id363out_exception;

  { // Node ID: 363 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id363in_a = id358out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id363in_b = id362out_result;

    HWOffsetFix<1,0,UNSIGNED> id363x_1;

    id363out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id363in_a,id363in_b,(&(id363x_1))));
    id363out_exception = (id363x_1);
  }
  { // Node ID: 365 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-61,UNSIGNED> id366out_result;

  { // Node ID: 366 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id366in_sel = id363out_exception;
    const HWOffsetFix<64,-61,UNSIGNED> &id366in_option0 = id363out_result;
    const HWOffsetFix<64,-61,UNSIGNED> &id366in_option1 = id365out_value;

    HWOffsetFix<64,-61,UNSIGNED> id366x_1;

    switch((id366in_sel.getValueAsLong())) {
      case 0l:
        id366x_1 = id366in_option0;
        break;
      case 1l:
        id366x_1 = id366in_option1;
        break;
      default:
        id366x_1 = (c_hw_fix_64_n61_uns_undef);
        break;
    }
    id366out_result = (id366x_1);
  }
  HWOffsetFix<60,-57,UNSIGNED> id367out_o;
  HWOffsetFix<1,0,UNSIGNED> id367out_exception;

  { // Node ID: 367 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id367in_i = id366out_result;

    HWOffsetFix<1,0,UNSIGNED> id367x_1;

    id367out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id367in_i,(&(id367x_1))));
    id367out_exception = (id367x_1);
  }
  { // Node ID: 369 (NodeConstantRawBits)
  }
  HWOffsetFix<60,-57,UNSIGNED> id370out_result;

  { // Node ID: 370 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id370in_sel = id367out_exception;
    const HWOffsetFix<60,-57,UNSIGNED> &id370in_option0 = id367out_o;
    const HWOffsetFix<60,-57,UNSIGNED> &id370in_option1 = id369out_value;

    HWOffsetFix<60,-57,UNSIGNED> id370x_1;

    switch((id370in_sel.getValueAsLong())) {
      case 0l:
        id370x_1 = id370in_option0;
        break;
      case 1l:
        id370x_1 = id370in_option1;
        break;
      default:
        id370x_1 = (c_hw_fix_60_n57_uns_undef);
        break;
    }
    id370out_result = (id370x_1);
  }
  HWOffsetFix<53,-52,UNSIGNED> id371out_o;
  HWOffsetFix<1,0,UNSIGNED> id371out_exception;

  { // Node ID: 371 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id371in_i = id370out_result;

    HWOffsetFix<1,0,UNSIGNED> id371x_1;

    id371out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id371in_i,(&(id371x_1))));
    id371out_exception = (id371x_1);
  }
  { // Node ID: 373 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id374out_result;

  { // Node ID: 374 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id374in_sel = id371out_exception;
    const HWOffsetFix<53,-52,UNSIGNED> &id374in_option0 = id371out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id374in_option1 = id373out_value;

    HWOffsetFix<53,-52,UNSIGNED> id374x_1;

    switch((id374in_sel.getValueAsLong())) {
      case 0l:
        id374x_1 = id374in_option0;
        break;
      case 1l:
        id374x_1 = id374in_option1;
        break;
      default:
        id374x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id374out_result = (id374x_1);
  }
  { // Node ID: 924 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id815out_result;

  { // Node ID: 815 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id815in_a = id374out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id815in_b = id924out_value;

    id815out_result = (gte_fixed(id815in_a,id815in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id851out_result;
  HWOffsetFix<1,0,UNSIGNED> id851out_exception;

  { // Node ID: 851 (NodeCondAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id851in_a = id405out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id851in_b = id406out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id851in_condb = id815out_result;

    HWOffsetFix<1,0,UNSIGNED> id851x_1;
    HWOffsetFix<1,0,UNSIGNED> id851x_2;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id851x_3;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id851x_4;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id851x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id851x_3 = (c_hw_fix_15_0_sgn_bits);
        break;
      case 1l:
        id851x_3 = (cast_fixed2fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>(id851in_a));
        break;
      default:
        id851x_3 = (c_hw_fix_15_0_sgn_undef);
        break;
    }
    switch((id851in_condb.getValueAsLong())) {
      case 0l:
        id851x_4 = (c_hw_fix_15_0_sgn_bits);
        break;
      case 1l:
        id851x_4 = (cast_fixed2fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>(id851in_b));
        break;
      default:
        id851x_4 = (c_hw_fix_15_0_sgn_undef);
        break;
    }
    (id851x_5) = (add_fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>((id851x_3),(id851x_4),(&(id851x_1))));
    id851out_result = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id851x_5),(&(id851x_2))));
    id851out_exception = (or_fixed((id851x_1),(id851x_2)));
  }
  { // Node ID: 411 (NodeConstantRawBits)
  }
  HWRawBits<1> id816out_result;

  { // Node ID: 816 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id816in_a = id851out_result;

    id816out_result = (slice<13,1>(id816in_a));
  }
  HWRawBits<1> id817out_result;

  { // Node ID: 817 (NodeNot)
    const HWRawBits<1> &id817in_a = id816out_result;

    id817out_result = (not_bits(id817in_a));
  }
  HWRawBits<14> id830out_result;

  { // Node ID: 830 (NodeCat)
    const HWRawBits<1> &id830in_in0 = id817out_result;
    const HWRawBits<1> &id830in_in1 = id817out_result;
    const HWRawBits<1> &id830in_in2 = id817out_result;
    const HWRawBits<1> &id830in_in3 = id817out_result;
    const HWRawBits<1> &id830in_in4 = id817out_result;
    const HWRawBits<1> &id830in_in5 = id817out_result;
    const HWRawBits<1> &id830in_in6 = id817out_result;
    const HWRawBits<1> &id830in_in7 = id817out_result;
    const HWRawBits<1> &id830in_in8 = id817out_result;
    const HWRawBits<1> &id830in_in9 = id817out_result;
    const HWRawBits<1> &id830in_in10 = id817out_result;
    const HWRawBits<1> &id830in_in11 = id817out_result;
    const HWRawBits<1> &id830in_in12 = id817out_result;
    const HWRawBits<1> &id830in_in13 = id817out_result;

    id830out_result = (cat((cat((cat((cat(id830in_in0,id830in_in1)),(cat(id830in_in2,id830in_in3)))),(cat((cat(id830in_in4,id830in_in5)),id830in_in6)))),(cat((cat((cat(id830in_in7,id830in_in8)),(cat(id830in_in9,id830in_in10)))),(cat((cat(id830in_in11,id830in_in12)),id830in_in13))))));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id427out_output;

  { // Node ID: 427 (NodeReinterpret)
    const HWRawBits<14> &id427in_input = id830out_result;

    id427out_output = (cast_bits2fixed<14,0,TWOSCOMPLEMENT>(id427in_input));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id428out_result;

  { // Node ID: 428 (NodeXor)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id428in_a = id411out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id428in_b = id427out_output;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id428x_1;

    (id428x_1) = (xor_fixed(id428in_a,id428in_b));
    id428out_result = (id428x_1);
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id429out_result;

  { // Node ID: 429 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id429in_sel = id851out_exception;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id429in_option0 = id851out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id429in_option1 = id428out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id429x_1;

    switch((id429in_sel.getValueAsLong())) {
      case 0l:
        id429x_1 = id429in_option0;
        break;
      case 1l:
        id429x_1 = id429in_option1;
        break;
      default:
        id429x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    id429out_result = (id429x_1);
  }
  HWRawBits<1> id831out_result;

  { // Node ID: 831 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id831in_a = id429out_result;

    id831out_result = (slice<13,1>(id831in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id832out_output;

  { // Node ID: 832 (NodeReinterpret)
    const HWRawBits<1> &id832in_input = id831out_result;

    id832out_output = (cast_bits2fixed<1,0,UNSIGNED>(id832in_input));
  }
  { // Node ID: 923 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id312out_result;

  { // Node ID: 312 (NodeGt)
    const HWFloat<11,53> &id312in_a = id852out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id312in_b = id923out_value;

    id312out_result = (gt_float(id312in_a,id312in_b));
  }
  { // Node ID: 894 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id894in_input = id312out_result;

    id894out_output[(getCycle()+6)%7] = id894in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_output;

  { // Node ID: 313 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id313in_input = id310out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id313in_input_doubt = id310out_result_doubt;

    id313out_output = id313in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id314out_result;

  { // Node ID: 314 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id314in_a = id894out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id314in_b = id313out_output;

    HWOffsetFix<1,0,UNSIGNED> id314x_1;

    (id314x_1) = (and_fixed(id314in_a,id314in_b));
    id314out_result = (id314x_1);
  }
  { // Node ID: 895 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id895in_input = id314out_result;

    id895out_output[(getCycle()+2)%3] = id895in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id432out_result;

  { // Node ID: 432 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id432in_a = id895out_output[getCycle()%3];

    id432out_result = (not_fixed(id432in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id433out_result;

  { // Node ID: 433 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id433in_a = id832out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id433in_b = id432out_result;

    HWOffsetFix<1,0,UNSIGNED> id433x_1;

    (id433x_1) = (and_fixed(id433in_a,id433in_b));
    id433out_result = (id433x_1);
  }
  { // Node ID: 922 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id316out_result;

  { // Node ID: 316 (NodeLt)
    const HWFloat<11,53> &id316in_a = id852out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id316in_b = id922out_value;

    id316out_result = (lt_float(id316in_a,id316in_b));
  }
  { // Node ID: 896 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id896in_input = id316out_result;

    id896out_output[(getCycle()+6)%7] = id896in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id317out_output;

  { // Node ID: 317 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id317in_input = id310out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id317in_input_doubt = id310out_result_doubt;

    id317out_output = id317in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id318out_result;

  { // Node ID: 318 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id318in_a = id896out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id318in_b = id317out_output;

    HWOffsetFix<1,0,UNSIGNED> id318x_1;

    (id318x_1) = (and_fixed(id318in_a,id318in_b));
    id318out_result = (id318x_1);
  }
  { // Node ID: 897 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id897in_input = id318out_result;

    id897out_output[(getCycle()+2)%3] = id897in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id434out_result;

  { // Node ID: 434 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id434in_a = id433out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id434in_b = id897out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id434x_1;

    (id434x_1) = (or_fixed(id434in_a,id434in_b));
    id434out_result = (id434x_1);
  }
  { // Node ID: 921 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id833out_result;

  { // Node ID: 833 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id833in_a = id429out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id833in_b = id921out_value;

    id833out_result = (gte_fixed(id833in_a,id833in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id458out_result;

  { // Node ID: 458 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id458in_a = id897out_output[getCycle()%3];

    id458out_result = (not_fixed(id458in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id459out_result;

  { // Node ID: 459 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id459in_a = id833out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id459in_b = id458out_result;

    HWOffsetFix<1,0,UNSIGNED> id459x_1;

    (id459x_1) = (and_fixed(id459in_a,id459in_b));
    id459out_result = (id459x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id460out_result;

  { // Node ID: 460 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id460in_a = id459out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id460in_b = id895out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id460x_1;

    (id460x_1) = (or_fixed(id460in_a,id460in_b));
    id460out_result = (id460x_1);
  }
  HWRawBits<2> id461out_result;

  { // Node ID: 461 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id461in_in0 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id461in_in1 = id460out_result;

    id461out_result = (cat(id461in_in0,id461in_in1));
  }
  { // Node ID: 453 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id435out_o;
  HWOffsetFix<1,0,UNSIGNED> id435out_exception;

  { // Node ID: 435 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id435in_i = id429out_result;

    HWOffsetFix<1,0,UNSIGNED> id435x_1;

    id435out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id435in_i,(&(id435x_1))));
    id435out_exception = (id435x_1);
  }
  { // Node ID: 437 (NodeConstantRawBits)
  }
  HWRawBits<1> id834out_result;

  { // Node ID: 834 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id834in_a = id435out_o;

    id834out_result = (slice<10,1>(id834in_a));
  }
  HWRawBits<1> id835out_result;

  { // Node ID: 835 (NodeNot)
    const HWRawBits<1> &id835in_a = id834out_result;

    id835out_result = (not_bits(id835in_a));
  }
  HWRawBits<11> id845out_result;

  { // Node ID: 845 (NodeCat)
    const HWRawBits<1> &id845in_in0 = id835out_result;
    const HWRawBits<1> &id845in_in1 = id835out_result;
    const HWRawBits<1> &id845in_in2 = id835out_result;
    const HWRawBits<1> &id845in_in3 = id835out_result;
    const HWRawBits<1> &id845in_in4 = id835out_result;
    const HWRawBits<1> &id845in_in5 = id835out_result;
    const HWRawBits<1> &id845in_in6 = id835out_result;
    const HWRawBits<1> &id845in_in7 = id835out_result;
    const HWRawBits<1> &id845in_in8 = id835out_result;
    const HWRawBits<1> &id845in_in9 = id835out_result;
    const HWRawBits<1> &id845in_in10 = id835out_result;

    id845out_result = (cat((cat((cat((cat(id845in_in0,id845in_in1)),id845in_in2)),(cat((cat(id845in_in3,id845in_in4)),id845in_in5)))),(cat((cat((cat(id845in_in6,id845in_in7)),id845in_in8)),(cat(id845in_in9,id845in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id450out_output;

  { // Node ID: 450 (NodeReinterpret)
    const HWRawBits<11> &id450in_input = id845out_result;

    id450out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id450in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id451out_result;

  { // Node ID: 451 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id451in_a = id437out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id451in_b = id450out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id451x_1;

    (id451x_1) = (xor_fixed(id451in_a,id451in_b));
    id451out_result = (id451x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id452out_result;

  { // Node ID: 452 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id452in_sel = id435out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id452in_option0 = id435out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id452in_option1 = id451out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id452x_1;

    switch((id452in_sel.getValueAsLong())) {
      case 0l:
        id452x_1 = id452in_option0;
        break;
      case 1l:
        id452x_1 = id452in_option1;
        break;
      default:
        id452x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id452out_result = (id452x_1);
  }
  { // Node ID: 377 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id378out_result;

  { // Node ID: 378 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id378in_sel = id815out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id378in_option0 = id374out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id378in_option1 = id377out_value;

    HWOffsetFix<53,-52,UNSIGNED> id378x_1;

    switch((id378in_sel.getValueAsLong())) {
      case 0l:
        id378x_1 = id378in_option0;
        break;
      case 1l:
        id378x_1 = id378in_option1;
        break;
      default:
        id378x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id378out_result = (id378x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id379out_o;
  HWOffsetFix<1,0,UNSIGNED> id379out_exception;

  { // Node ID: 379 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id379in_i = id378out_result;

    HWOffsetFix<1,0,UNSIGNED> id379x_1;

    id379out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id379in_i,(&(id379x_1))));
    id379out_exception = (id379x_1);
  }
  { // Node ID: 381 (NodeConstantRawBits)
  }
  HWOffsetFix<52,-52,UNSIGNED> id382out_result;

  { // Node ID: 382 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id382in_sel = id379out_exception;
    const HWOffsetFix<52,-52,UNSIGNED> &id382in_option0 = id379out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id382in_option1 = id381out_value;

    HWOffsetFix<52,-52,UNSIGNED> id382x_1;

    switch((id382in_sel.getValueAsLong())) {
      case 0l:
        id382x_1 = id382in_option0;
        break;
      case 1l:
        id382x_1 = id382in_option1;
        break;
      default:
        id382x_1 = (c_hw_fix_52_n52_uns_undef);
        break;
    }
    id382out_result = (id382x_1);
  }
  HWRawBits<64> id454out_result;

  { // Node ID: 454 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id454in_in0 = id453out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id454in_in1 = id452out_result;
    const HWOffsetFix<52,-52,UNSIGNED> &id454in_in2 = id382out_result;

    id454out_result = (cat((cat(id454in_in0,id454in_in1)),id454in_in2));
  }
  HWFloat<11,53> id455out_output;

  { // Node ID: 455 (NodeReinterpret)
    const HWRawBits<64> &id455in_input = id454out_result;

    id455out_output = (cast_bits2float<11,53>(id455in_input));
  }
  { // Node ID: 462 (NodeConstantRawBits)
  }
  { // Node ID: 463 (NodeConstantRawBits)
  }
  { // Node ID: 465 (NodeConstantRawBits)
  }
  HWRawBits<64> id846out_result;

  { // Node ID: 846 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id846in_in0 = id462out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id846in_in1 = id463out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id846in_in2 = id465out_value;

    id846out_result = (cat((cat(id846in_in0,id846in_in1)),id846in_in2));
  }
  HWFloat<11,53> id467out_output;

  { // Node ID: 467 (NodeReinterpret)
    const HWRawBits<64> &id467in_input = id846out_result;

    id467out_output = (cast_bits2float<11,53>(id467in_input));
  }
  { // Node ID: 548 (NodeConstantRawBits)
  }
  HWFloat<11,53> id470out_result;

  { // Node ID: 470 (NodeMux)
    const HWRawBits<2> &id470in_sel = id461out_result;
    const HWFloat<11,53> &id470in_option0 = id455out_output;
    const HWFloat<11,53> &id470in_option1 = id467out_output;
    const HWFloat<11,53> &id470in_option2 = id548out_value;
    const HWFloat<11,53> &id470in_option3 = id467out_output;

    HWFloat<11,53> id470x_1;

    switch((id470in_sel.getValueAsLong())) {
      case 0l:
        id470x_1 = id470in_option0;
        break;
      case 1l:
        id470x_1 = id470in_option1;
        break;
      case 2l:
        id470x_1 = id470in_option2;
        break;
      case 3l:
        id470x_1 = id470in_option3;
        break;
      default:
        id470x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id470out_result = (id470x_1);
  }
  { // Node ID: 920 (NodeConstantRawBits)
  }
  HWFloat<11,53> id480out_result;

  { // Node ID: 480 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id480in_sel = id900out_output[getCycle()%9];
    const HWFloat<11,53> &id480in_option0 = id470out_result;
    const HWFloat<11,53> &id480in_option1 = id920out_value;

    HWFloat<11,53> id480x_1;

    switch((id480in_sel.getValueAsLong())) {
      case 0l:
        id480x_1 = id480in_option0;
        break;
      case 1l:
        id480x_1 = id480in_option1;
        break;
      default:
        id480x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id480out_result = (id480x_1);
  }
  { // Node ID: 919 (NodeConstantRawBits)
  }
  HWFloat<11,53> id491out_result;
  HWOffsetFix<4,0,UNSIGNED> id491out_exception;

  { // Node ID: 491 (NodeAdd)
    const HWFloat<11,53> &id491in_a = id480out_result;
    const HWFloat<11,53> &id491in_b = id919out_value;

    HWOffsetFix<4,0,UNSIGNED> id491x_1;

    id491out_result = (add_float(id491in_a,id491in_b,(&(id491x_1))));
    id491out_exception = (id491x_1);
  }
  HWFloat<11,53> id493out_result;
  HWOffsetFix<4,0,UNSIGNED> id493out_exception;

  { // Node ID: 493 (NodeDiv)
    const HWFloat<11,53> &id493in_a = id927out_value;
    const HWFloat<11,53> &id493in_b = id491out_result;

    HWOffsetFix<4,0,UNSIGNED> id493x_1;

    id493out_result = (div_float(id493in_a,id493in_b,(&(id493x_1))));
    id493out_exception = (id493x_1);
  }
  HWFloat<11,53> id495out_result;
  HWOffsetFix<4,0,UNSIGNED> id495out_exception;

  { // Node ID: 495 (NodeSub)
    const HWFloat<11,53> &id495in_a = id928out_value;
    const HWFloat<11,53> &id495in_b = id493out_result;

    HWOffsetFix<4,0,UNSIGNED> id495x_1;

    id495out_result = (sub_float(id495in_a,id495in_b,(&(id495x_1))));
    id495out_exception = (id495x_1);
  }
  HWFloat<11,53> id498out_result;

  { // Node ID: 498 (NodeSub)
    const HWFloat<11,53> &id498in_a = id929out_value;
    const HWFloat<11,53> &id498in_b = id495out_result;

    id498out_result = (sub_float(id498in_a,id498in_b));
  }
  HWFloat<11,53> id499out_result;

  { // Node ID: 499 (NodeMul)
    const HWFloat<11,53> &id499in_a = id498out_result;
    const HWFloat<11,53> &id499in_b = id901out_output[getCycle()%10];

    id499out_result = (mul_float(id499in_a,id499in_b));
  }
  HWFloat<11,53> id500out_result;

  { // Node ID: 500 (NodeMul)
    const HWFloat<11,53> &id500in_a = id499out_result;
    const HWFloat<11,53> &id500in_b = id901out_output[getCycle()%10];

    id500out_result = (mul_float(id500in_a,id500in_b));
  }
  { // Node ID: 853 (NodePO2FPMult)
    const HWFloat<11,53> &id853in_floatIn = id500out_result;

    id853out_floatOut[(getCycle()+1)%2] = (mul_float(id853in_floatIn,(c_hw_flt_11_53_0_5val)));
  }
  HWFloat<11,53> id505out_result;

  { // Node ID: 505 (NodeMul)
    const HWFloat<11,53> &id505in_a = id504out_result;
    const HWFloat<11,53> &id505in_b = id853out_floatOut[getCycle()%2];

    id505out_result = (mul_float(id505in_a,id505in_b));
  }
  HWFloat<11,53> id847out_result;

  { // Node ID: 847 (NodeSub)
    const HWFloat<11,53> &id847in_a = id505out_result;
    const HWFloat<11,53> &id847in_b = id913out_output[getCycle()%2];

    id847out_result = (sub_float(id847in_a,id847in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<11,53> id512out_result;

  { // Node ID: 512 (NodeMul)
    const HWFloat<11,53> &id512in_a = id847out_result;
    const HWFloat<11,53> &id512in_b = id2out_value;

    id512out_result = (mul_float(id512in_a,id512in_b));
  }
  HWFloat<11,53> id514out_result;

  { // Node ID: 514 (NodeMul)
    const HWFloat<11,53> &id514in_a = id18out_dt;
    const HWFloat<11,53> &id514in_b = id512out_result;

    id514out_result = (mul_float(id514in_a,id514in_b));
  }
  HWFloat<11,53> id515out_result;

  { // Node ID: 515 (NodeAdd)
    const HWFloat<11,53> &id515in_a = id913out_output[getCycle()%2];
    const HWFloat<11,53> &id515in_b = id514out_result;

    id515out_result = (add_float(id515in_a,id515in_b));
  }
  HWFloat<11,53> id885out_output;

  { // Node ID: 885 (NodeStreamOffset)
    const HWFloat<11,53> &id885in_input = id515out_result;

    id885out_output = id885in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id549out_result[getCycle()%2];
    const HWFloat<11,53> &id23in_option0 = id885out_output;
    const HWFloat<11,53> &id23in_option1 = id22out_value;

    HWFloat<11,53> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<11,53> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<11,53> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<11,53> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 852 (NodePO2FPMult)
    const HWFloat<11,53> &id852in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id852x_1;

    id852out_floatOut[(getCycle()+1)%2] = (mul_float(id852in_floatIn,(c_hw_flt_11_53_2_0val),(&(id852x_1))));
    id852out_exception[(getCycle()+1)%2] = (id852x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<11,53> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<11,53> &id42in_input = id852out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<11,53> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 857 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id857in_exceptionVector = id43out_exception[getCycle()%7];

    (id857st_reg) = (or_fixed((id857st_reg),id857in_exceptionVector));
    id857out_exceptionOccurred[(getCycle()+1)%2] = (id857st_reg);
  }
  { // Node ID: 905 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id905in_input = id857out_exceptionOccurred[getCycle()%2];

    id905out_output[(getCycle()+2)%3] = id905in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 858 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_exceptionVector = id116out_exception;

    (id858st_reg) = (or_fixed((id858st_reg),id858in_exceptionVector));
    id858out_exceptionOccurred[(getCycle()+1)%2] = (id858st_reg);
  }
  { // Node ID: 906 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id906in_input = id858out_exceptionOccurred[getCycle()%2];

    id906out_output[(getCycle()+2)%3] = id906in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 859 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id859in_exceptionVector = id240out_exception;

    (id859st_reg) = (or_fixed((id859st_reg),id859in_exceptionVector));
    id859out_exceptionOccurred[(getCycle()+1)%2] = (id859st_reg);
  }
  { // Node ID: 907 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id907in_input = id859out_exceptionOccurred[getCycle()%2];

    id907out_output[(getCycle()+2)%3] = id907in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 861 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id861in_exceptionVector = id328out_exception;

    (id861st_reg) = (or_fixed((id861st_reg),id861in_exceptionVector));
    id861out_exceptionOccurred[(getCycle()+1)%2] = (id861st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 862 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id862in_exceptionVector = id333out_exception;

    (id862st_reg) = (or_fixed((id862st_reg),id862in_exceptionVector));
    id862out_exceptionOccurred[(getCycle()+1)%2] = (id862st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 863 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id863in_exceptionVector = id337out_exception;

    (id863st_reg) = (or_fixed((id863st_reg),id863in_exceptionVector));
    id863out_exceptionOccurred[(getCycle()+1)%2] = (id863st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 864 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id864in_exceptionVector = id341out_exception;

    (id864st_reg) = (or_fixed((id864st_reg),id864in_exceptionVector));
    id864out_exceptionOccurred[(getCycle()+1)%2] = (id864st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 865 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id865in_exceptionVector = id346out_exception;

    (id865st_reg) = (or_fixed((id865st_reg),id865in_exceptionVector));
    id865out_exceptionOccurred[(getCycle()+1)%2] = (id865st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 866 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id866in_exceptionVector = id350out_exception;

    (id866st_reg) = (or_fixed((id866st_reg),id866in_exceptionVector));
    id866out_exceptionOccurred[(getCycle()+1)%2] = (id866st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 867 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id867in_exceptionVector = id354out_exception;

    (id867st_reg) = (or_fixed((id867st_reg),id867in_exceptionVector));
    id867out_exceptionOccurred[(getCycle()+1)%2] = (id867st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 868 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id868in_exceptionVector = id359out_exception;

    (id868st_reg) = (or_fixed((id868st_reg),id868in_exceptionVector));
    id868out_exceptionOccurred[(getCycle()+1)%2] = (id868st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 869 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id869in_exceptionVector = id363out_exception;

    (id869st_reg) = (or_fixed((id869st_reg),id869in_exceptionVector));
    id869out_exceptionOccurred[(getCycle()+1)%2] = (id869st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 870 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id870in_exceptionVector = id367out_exception;

    (id870st_reg) = (or_fixed((id870st_reg),id870in_exceptionVector));
    id870out_exceptionOccurred[(getCycle()+1)%2] = (id870st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 871 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id871in_exceptionVector = id371out_exception;

    (id871st_reg) = (or_fixed((id871st_reg),id871in_exceptionVector));
    id871out_exceptionOccurred[(getCycle()+1)%2] = (id871st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 874 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id874in_exceptionVector = id379out_exception;

    (id874st_reg) = (or_fixed((id874st_reg),id874in_exceptionVector));
    id874out_exceptionOccurred[(getCycle()+1)%2] = (id874st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 860 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id860in_exceptionVector = id385out_exception;

    (id860st_reg) = (or_fixed((id860st_reg),id860in_exceptionVector));
    id860out_exceptionOccurred[(getCycle()+1)%2] = (id860st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 873 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id873in_exceptionVector = id435out_exception;

    (id873st_reg) = (or_fixed((id873st_reg),id873in_exceptionVector));
    id873out_exceptionOccurred[(getCycle()+1)%2] = (id873st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 875 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id875in_exceptionVector = id491out_exception;

    (id875st_reg) = (or_fixed((id875st_reg),id875in_exceptionVector));
    id875out_exceptionOccurred[(getCycle()+1)%2] = (id875st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 876 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id876in_exceptionVector = id493out_exception;

    (id876st_reg) = (or_fixed((id876st_reg),id876in_exceptionVector));
    id876out_exceptionOccurred[(getCycle()+1)%2] = (id876st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 877 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id877in_exceptionVector = id495out_exception;

    (id877st_reg) = (or_fixed((id877st_reg),id877in_exceptionVector));
    id877out_exceptionOccurred[(getCycle()+1)%2] = (id877st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 872 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id872in_exceptionVector = id851out_exception;

    (id872st_reg) = (or_fixed((id872st_reg),id872in_exceptionVector));
    id872out_exceptionOccurred[(getCycle()+1)%2] = (id872st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 856 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id856in_exceptionVector = id852out_exception[getCycle()%2];

    (id856st_reg) = (or_fixed((id856st_reg),id856in_exceptionVector));
    id856out_exceptionOccurred[(getCycle()+1)%2] = (id856st_reg);
  }
  { // Node ID: 908 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id908in_input = id856out_exceptionOccurred[getCycle()%2];

    id908out_output[(getCycle()+8)%9] = id908in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 878 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id878in_n43 = id905out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n116 = id906out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n240 = id907out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n328 = id861out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n333 = id862out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n337 = id863out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n341 = id864out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n346 = id865out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n350 = id866out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n354 = id867out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n359 = id868out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n363 = id869out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n367 = id870out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n371 = id871out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n379 = id874out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n385 = id860out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n435 = id873out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id878in_n491 = id875out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id878in_n493 = id876out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id878in_n495 = id877out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id878in_n851 = id872out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id878in_n852 = id908out_output[getCycle()%9];

    id878out_all_exceptions = (cat((cat((cat((cat((cat(id878in_n43,id878in_n116)),id878in_n240)),(cat((cat(id878in_n328,id878in_n333)),id878in_n337)))),(cat((cat((cat(id878in_n341,id878in_n346)),id878in_n350)),(cat(id878in_n354,id878in_n359)))))),(cat((cat((cat((cat(id878in_n363,id878in_n367)),id878in_n371)),(cat((cat(id878in_n379,id878in_n385)),id878in_n435)))),(cat((cat((cat(id878in_n491,id878in_n493)),id878in_n495)),(cat(id878in_n851,id878in_n852))))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 879 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id879in_load = id880out_value;
    const HWRawBits<37> &id879in_data = id878out_all_exceptions;

    bool id879x_1;

    (id879x_1) = ((id879in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id879x_1)) {
      setMappedRegValue("all_numeric_exceptions", id879in_data);
    }
  }
  { // Node ID: 854 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 855 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id855in_output_control = id854out_value;
    const HWFloat<11,53> &id855in_data = id885out_output;

    bool id855x_1;

    (id855x_1) = ((id855in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id855x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id855in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 909 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id909in_input = id17out_count;

    id909out_output[(getCycle()+1)%2] = id909in_input;
  }
  { // Node ID: 918 (NodeConstantRawBits)
  }
  { // Node ID: 523 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id523in_a = id914out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id523in_b = id918out_value;

    id523out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id523in_a,id523in_b));
  }
  { // Node ID: 848 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id848in_a = id909out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id848in_b = id523out_result[getCycle()%2];

    id848out_result[(getCycle()+1)%2] = (eq_fixed(id848in_a,id848in_b));
  }
  { // Node ID: 525 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id526out_result;

  { // Node ID: 526 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id526in_a = id525out_io_u_out_force_disabled;

    id526out_result = (not_fixed(id526in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id527out_result;

  { // Node ID: 527 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id527in_a = id848out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id527in_b = id526out_result;

    HWOffsetFix<1,0,UNSIGNED> id527x_1;

    (id527x_1) = (and_fixed(id527in_a,id527in_b));
    id527out_result = (id527x_1);
  }
  { // Node ID: 910 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id910in_input = id527out_result;

    id910out_output[(getCycle()+10)%11] = id910in_input;
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 528 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id528in_output_control = id910out_output[getCycle()%11];
    const HWFloat<11,53> &id528in_data = id515out_result;

    bool id528x_1;

    (id528x_1) = ((id528in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id528x_1)) {
      writeOutput(m_u_out, id528in_data);
    }
  }
  { // Node ID: 917 (NodeConstantRawBits)
  }
  { // Node ID: 530 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id530in_a = id914out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id530in_b = id917out_value;

    id530out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id530in_a,id530in_b));
  }
  { // Node ID: 849 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id849in_a = id909out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id849in_b = id530out_result[getCycle()%2];

    id849out_result[(getCycle()+1)%2] = (eq_fixed(id849in_a,id849in_b));
  }
  { // Node ID: 532 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id533out_result;

  { // Node ID: 533 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id533in_a = id532out_io_v_out_force_disabled;

    id533out_result = (not_fixed(id533in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id534out_result;

  { // Node ID: 534 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id534in_a = id849out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id534in_b = id533out_result;

    HWOffsetFix<1,0,UNSIGNED> id534x_1;

    (id534x_1) = (and_fixed(id534in_a,id534in_b));
    id534out_result = (id534x_1);
  }
  { // Node ID: 912 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id912in_input = id534out_result;

    id912out_output[(getCycle()+10)%11] = id912in_input;
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 535 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id535in_output_control = id912out_output[getCycle()%11];
    const HWFloat<11,53> &id535in_data = id517out_result;

    bool id535x_1;

    (id535x_1) = ((id535in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id535x_1)) {
      writeOutput(m_v_out, id535in_data);
    }
  }
  { // Node ID: 540 (NodeConstantRawBits)
  }
  { // Node ID: 916 (NodeConstantRawBits)
  }
  { // Node ID: 537 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 538 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id538in_enable = id916out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id538in_max = id537out_value;

    HWOffsetFix<49,0,UNSIGNED> id538x_1;
    HWOffsetFix<1,0,UNSIGNED> id538x_2;
    HWOffsetFix<1,0,UNSIGNED> id538x_3;
    HWOffsetFix<49,0,UNSIGNED> id538x_4t_1e_1;

    id538out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id538st_count)));
    (id538x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id538st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id538x_2) = (gte_fixed((id538x_1),id538in_max));
    (id538x_3) = (and_fixed((id538x_2),id538in_enable));
    id538out_wrap = (id538x_3);
    if((id538in_enable.getValueAsBool())) {
      if(((id538x_3).getValueAsBool())) {
        (id538st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id538x_4t_1e_1) = (id538x_1);
        (id538st_count) = (id538x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id539out_output;

  { // Node ID: 539 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id539in_input = id538out_count;

    id539out_output = id539in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 541 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id541in_load = id540out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id541in_data = id539out_output;

    bool id541x_1;

    (id541x_1) = ((id541in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id541x_1)) {
      setMappedRegValue("current_run_cycle_count", id541in_data);
    }
  }
  { // Node ID: 915 (NodeConstantRawBits)
  }
  { // Node ID: 543 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 544 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id544in_enable = id915out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id544in_max = id543out_value;

    HWOffsetFix<49,0,UNSIGNED> id544x_1;
    HWOffsetFix<1,0,UNSIGNED> id544x_2;
    HWOffsetFix<1,0,UNSIGNED> id544x_3;
    HWOffsetFix<49,0,UNSIGNED> id544x_4t_1e_1;

    id544out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id544st_count)));
    (id544x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id544st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id544x_2) = (gte_fixed((id544x_1),id544in_max));
    (id544x_3) = (and_fixed((id544x_2),id544in_enable));
    id544out_wrap = (id544x_3);
    if((id544in_enable.getValueAsBool())) {
      if(((id544x_3).getValueAsBool())) {
        (id544st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id544x_4t_1e_1) = (id544x_1);
        (id544st_count) = (id544x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 546 (NodeInputMappedReg)
  }
  { // Node ID: 850 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id850in_a = id544out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id850in_b = id546out_run_cycle_count;

    id850out_result[(getCycle()+1)%2] = (eq_fixed(id850in_a,id850in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 545 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id545in_start = id850out_result[getCycle()%2];

    if((id545in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
