#ifndef KARMADFEKERNEL_H_
#define KARMADFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaDFEKernel : public KernelManagerBlockSync {
public:
  KarmaDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id880out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id914out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id932out_value;

  HWOffsetFix<1,0,UNSIGNED> id549out_result[2];

  HWFloat<11,53> id901out_output[10];

  HWFloat<11,53> id913out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<11,53> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id931out_value;

  HWOffsetFix<1,0,UNSIGNED> id550out_result[2];

  HWFloat<11,53> id930out_value;

  HWFloat<11,53> id5out_value;

  HWFloat<11,53> id3out_value;

  HWFloat<11,53> id26out_value;

  HWFloat<11,53> id27out_result[2];

  HWFloat<11,53> id887out_output[11];

  HWFloat<11,53> id929out_value;

  HWFloat<11,53> id928out_value;

  HWFloat<11,53> id927out_value;

  HWRawBits<11> id474out_value;

  HWRawBits<52> id926out_value;

  HWOffsetFix<1,0,UNSIGNED> id900out_output[9];

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<52,-51,UNSIGNED> id117out_value;

  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id119out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id242out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id892out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id925out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id387out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id406out_value;

  HWOffsetFix<38,-53,UNSIGNED> id489out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id489sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id486out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id486sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id483out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id483sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id327out_value;

  HWOffsetFix<26,-51,UNSIGNED> id893out_output[3];

  HWOffsetFix<26,-51,UNSIGNED> id330out_value;

  HWOffsetFix<64,-89,UNSIGNED> id335out_value;

  HWOffsetFix<64,-63,UNSIGNED> id339out_value;

  HWOffsetFix<58,-57,UNSIGNED> id343out_value;

  HWOffsetFix<64,-68,UNSIGNED> id348out_value;

  HWOffsetFix<64,-62,UNSIGNED> id352out_value;

  HWOffsetFix<59,-57,UNSIGNED> id356out_value;

  HWOffsetFix<64,-77,UNSIGNED> id361out_value;

  HWOffsetFix<64,-61,UNSIGNED> id365out_value;

  HWOffsetFix<60,-57,UNSIGNED> id369out_value;

  HWOffsetFix<53,-52,UNSIGNED> id373out_value;

  HWOffsetFix<53,-52,UNSIGNED> id924out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id411out_value;

  HWFloat<11,53> id923out_value;

  HWOffsetFix<1,0,UNSIGNED> id894out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id895out_output[3];

  HWFloat<11,53> id922out_value;

  HWOffsetFix<1,0,UNSIGNED> id896out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id897out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id921out_value;

  HWOffsetFix<1,0,UNSIGNED> id453out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id437out_value;

  HWOffsetFix<53,-52,UNSIGNED> id377out_value;

  HWOffsetFix<52,-52,UNSIGNED> id381out_value;

  HWOffsetFix<1,0,UNSIGNED> id462out_value;

  HWOffsetFix<11,0,UNSIGNED> id463out_value;

  HWOffsetFix<52,0,UNSIGNED> id465out_value;

  HWFloat<11,53> id548out_value;

  HWFloat<11,53> id920out_value;

  HWFloat<11,53> id919out_value;

  HWFloat<11,53> id853out_floatOut[2];

  HWFloat<11,53> id2out_value;

  HWFloat<11,53> id22out_value;

  HWFloat<11,53> id23out_result[2];

  HWFloat<11,53> id4out_value;

  HWFloat<11,53> id852out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id852out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id857out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id857st_reg;

  HWOffsetFix<4,0,UNSIGNED> id905out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id858out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id858st_reg;

  HWOffsetFix<1,0,UNSIGNED> id906out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id859out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id859st_reg;

  HWOffsetFix<1,0,UNSIGNED> id907out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id861out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id861st_reg;

  HWOffsetFix<1,0,UNSIGNED> id862out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id862st_reg;

  HWOffsetFix<1,0,UNSIGNED> id863out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id863st_reg;

  HWOffsetFix<1,0,UNSIGNED> id864out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id864st_reg;

  HWOffsetFix<1,0,UNSIGNED> id865out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id865st_reg;

  HWOffsetFix<1,0,UNSIGNED> id866out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id866st_reg;

  HWOffsetFix<1,0,UNSIGNED> id867out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id867st_reg;

  HWOffsetFix<1,0,UNSIGNED> id868out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id868st_reg;

  HWOffsetFix<1,0,UNSIGNED> id869out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id869st_reg;

  HWOffsetFix<1,0,UNSIGNED> id870out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id870st_reg;

  HWOffsetFix<1,0,UNSIGNED> id871out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id871st_reg;

  HWOffsetFix<1,0,UNSIGNED> id874out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id874st_reg;

  HWOffsetFix<1,0,UNSIGNED> id860out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id860st_reg;

  HWOffsetFix<1,0,UNSIGNED> id873out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id873st_reg;

  HWOffsetFix<4,0,UNSIGNED> id875out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id875st_reg;

  HWOffsetFix<4,0,UNSIGNED> id876out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id876st_reg;

  HWOffsetFix<4,0,UNSIGNED> id877out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id877st_reg;

  HWOffsetFix<1,0,UNSIGNED> id872out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id872st_reg;

  HWOffsetFix<4,0,UNSIGNED> id856out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id856st_reg;

  HWOffsetFix<4,0,UNSIGNED> id908out_output[9];

  HWRawBits<37> id878out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id854out_value;

  HWOffsetFix<11,0,UNSIGNED> id909out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id918out_value;

  HWOffsetFix<11,0,UNSIGNED> id523out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id848out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id525out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id910out_output[11];

  HWOffsetFix<11,0,UNSIGNED> id917out_value;

  HWOffsetFix<11,0,UNSIGNED> id530out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id849out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id532out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id912out_output[11];

  HWOffsetFix<1,0,UNSIGNED> id540out_value;

  HWOffsetFix<1,0,UNSIGNED> id916out_value;

  HWOffsetFix<49,0,UNSIGNED> id537out_value;

  HWOffsetFix<48,0,UNSIGNED> id538out_count;
  HWOffsetFix<1,0,UNSIGNED> id538out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id538st_count;

  HWOffsetFix<1,0,UNSIGNED> id915out_value;

  HWOffsetFix<49,0,UNSIGNED> id543out_value;

  HWOffsetFix<48,0,UNSIGNED> id544out_count;
  HWOffsetFix<1,0,UNSIGNED> id544out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id544st_count;

  HWOffsetFix<48,0,UNSIGNED> id546out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id850out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_1;
  const HWFloat<11,53> c_hw_flt_11_53_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_4;
  const HWFloat<11,53> c_hw_flt_11_53_bits_5;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<52> c_hw_bit_52_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<64,-51,TWOSCOMPLEMENT> c_hw_fix_64_n51_sgn_bits;
  const HWOffsetFix<64,-51,TWOSCOMPLEMENT> c_hw_fix_64_n51_sgn_undef;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<116,-102,TWOSCOMPLEMENT> c_hw_fix_116_n102_sgn_bits;
  const HWOffsetFix<116,-102,TWOSCOMPLEMENT> c_hw_fix_116_n102_sgn_undef;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<38,-53,UNSIGNED> c_hw_fix_38_n53_uns_undef;
  const HWOffsetFix<48,-53,UNSIGNED> c_hw_fix_48_n53_uns_undef;
  const HWOffsetFix<53,-53,UNSIGNED> c_hw_fix_53_n53_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_undef;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_bits;
  const HWOffsetFix<64,-89,UNSIGNED> c_hw_fix_64_n89_uns_bits;
  const HWOffsetFix<64,-89,UNSIGNED> c_hw_fix_64_n89_uns_undef;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_bits;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_undef;
  const HWOffsetFix<58,-57,UNSIGNED> c_hw_fix_58_n57_uns_bits;
  const HWOffsetFix<58,-57,UNSIGNED> c_hw_fix_58_n57_uns_undef;
  const HWOffsetFix<64,-68,UNSIGNED> c_hw_fix_64_n68_uns_bits;
  const HWOffsetFix<64,-68,UNSIGNED> c_hw_fix_64_n68_uns_undef;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_bits;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_undef;
  const HWOffsetFix<59,-57,UNSIGNED> c_hw_fix_59_n57_uns_bits;
  const HWOffsetFix<59,-57,UNSIGNED> c_hw_fix_59_n57_uns_undef;
  const HWOffsetFix<64,-77,UNSIGNED> c_hw_fix_64_n77_uns_bits;
  const HWOffsetFix<64,-77,UNSIGNED> c_hw_fix_64_n77_uns_undef;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_bits;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_undef;
  const HWOffsetFix<60,-57,UNSIGNED> c_hw_fix_60_n57_uns_bits;
  const HWOffsetFix<60,-57,UNSIGNED> c_hw_fix_60_n57_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_1;
  const HWOffsetFix<15,0,TWOSCOMPLEMENT> c_hw_fix_15_0_sgn_bits;
  const HWOffsetFix<15,0,TWOSCOMPLEMENT> c_hw_fix_15_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_2;
  const HWOffsetFix<52,-52,UNSIGNED> c_hw_fix_52_n52_uns_bits;
  const HWOffsetFix<52,-52,UNSIGNED> c_hw_fix_52_n52_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<52,0,UNSIGNED> c_hw_fix_52_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_6;
  const HWFloat<11,53> c_hw_flt_11_53_0_5val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_7;
  const HWFloat<11,53> c_hw_flt_11_53_bits_8;
  const HWFloat<11,53> c_hw_flt_11_53_bits_9;
  const HWFloat<11,53> c_hw_flt_11_53_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMADFEKERNEL_H_ */
