#include "stdsimheader.h"
#include "KarmaDFEKernel.h"

namespace maxcompilersim {

KarmaDFEKernel::KarmaDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 16, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x00bl))))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_12_undef((HWFloat<8,12>()))
, c_hw_flt_8_12_bits((HWFloat<8,12>(varint_u<20>(0x3f4cdl))))
, c_hw_flt_8_12_bits_1((HWFloat<8,12>(varint_u<20>(0x3fb6el))))
, c_hw_flt_8_12_bits_2((HWFloat<8,12>(varint_u<20>(0x3f800l))))
, c_hw_flt_8_12_bits_3((HWFloat<8,12>(varint_u<20>(0x3b831l))))
, c_hw_flt_8_12_bits_4((HWFloat<8,12>(varint_u<20>(0x00000l))))
, c_hw_flt_8_12_bits_5((HWFloat<8,12>(varint_u<20>(0x40000l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0xffl))))
, c_hw_bit_11_bits((HWRawBits<11>(varint_u<11>(0x000l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_24_n14_sgn_bits((HWOffsetFix<24,-14,TWOSCOMPLEMENT>(varint_u<24>(0x7fffffl))))
, c_hw_fix_24_n14_sgn_undef((HWOffsetFix<24,-14,TWOSCOMPLEMENT>()))
, c_hw_fix_24_n23_uns_bits((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0xb8aa3bl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_48_n37_sgn_bits((HWOffsetFix<48,-37,TWOSCOMPLEMENT>(varint_u<48>(0x7fffffffffffl))))
, c_hw_fix_48_n37_sgn_undef((HWOffsetFix<48,-37,TWOSCOMPLEMENT>()))
, c_hw_fix_10_0_sgn_undef((HWOffsetFix<10,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x07fl))))
, c_hw_fix_11_0_sgn_bits_1((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x3ffl))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_2((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x001l))))
, c_hw_fix_12_n12_uns_undef((HWOffsetFix<12,-12,UNSIGNED>()))
, c_hw_fix_8_n8_uns_bits((HWOffsetFix<8,-8,UNSIGNED>(varint_u<8>(0xb1l))))
, c_hw_fix_8_n14_uns_undef((HWOffsetFix<8,-14,UNSIGNED>()))
, c_hw_fix_8_n14_uns_bits((HWOffsetFix<8,-14,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_27_n26_uns_bits((HWOffsetFix<27,-26,UNSIGNED>(varint_u<27>(0x7ffffffl))))
, c_hw_fix_27_n26_uns_undef((HWOffsetFix<27,-26,UNSIGNED>()))
, c_hw_fix_15_n14_uns_bits((HWOffsetFix<15,-14,UNSIGNED>(varint_u<15>(0x7fffl))))
, c_hw_fix_15_n14_uns_undef((HWOffsetFix<15,-14,UNSIGNED>()))
, c_hw_fix_12_n11_uns_bits((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0xfffl))))
, c_hw_fix_12_n11_uns_undef((HWOffsetFix<12,-11,UNSIGNED>()))
, c_hw_fix_12_n11_uns_bits_1((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x800l))))
, c_hw_fix_12_0_sgn_bits((HWOffsetFix<12,0,TWOSCOMPLEMENT>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_sgn_undef((HWOffsetFix<12,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_3((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x0ffl))))
, c_hw_fix_8_0_sgn_bits((HWOffsetFix<8,0,TWOSCOMPLEMENT>(varint_u<8>(0x7fl))))
, c_hw_fix_8_0_sgn_undef((HWOffsetFix<8,0,TWOSCOMPLEMENT>()))
, c_hw_fix_12_n11_uns_bits_2((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_11_n11_uns_bits((HWOffsetFix<11,-11,UNSIGNED>(varint_u<11>(0x7ffl))))
, c_hw_fix_11_n11_uns_undef((HWOffsetFix<11,-11,UNSIGNED>()))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x000l))))
, c_hw_flt_8_12_bits_6((HWFloat<8,12>(varint_u<20>(0x7fc00l))))
, c_hw_flt_8_12_0_5val((HWFloat<8,12>(varint_u<20>(0x3f000l))))
, c_hw_flt_8_12_bits_7((HWFloat<8,12>(varint_u<20>(0x3eccdl))))
, c_hw_flt_8_12_bits_8((HWFloat<8,12>(varint_u<20>(0x3fc00l))))
, c_hw_flt_8_12_bits_9((HWFloat<8,12>(varint_u<20>(0x40400l))))
, c_hw_flt_8_12_2_0val((HWFloat<8,12>(varint_u<20>(0x40000l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_4_0_uns_undef((HWOffsetFix<4,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_2((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 522 (NodeConstantRawBits)
    id522out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 14 (NodeConstantRawBits)
    id14out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 556 (NodeConstantRawBits)
    id556out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 574 (NodeConstantRawBits)
    id574out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 18 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 6 (NodeConstantRawBits)
    id6out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 573 (NodeConstantRawBits)
    id573out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 572 (NodeConstantRawBits)
    id572out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 5 (NodeConstantRawBits)
    id5out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 26 (NodeConstantRawBits)
    id26out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 571 (NodeConstantRawBits)
    id571out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 570 (NodeConstantRawBits)
    id570out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 569 (NodeConstantRawBits)
    id569out_value = (c_hw_flt_8_12_bits_5);
  }
  { // Node ID: 286 (NodeConstantRawBits)
    id286out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 568 (NodeConstantRawBits)
    id568out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 46 (NodeConstantRawBits)
    id46out_value = (c_hw_fix_24_n14_sgn_bits);
  }
  { // Node ID: 77 (NodeConstantRawBits)
    id77out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 79 (NodeConstantRawBits)
    id79out_value = (c_hw_fix_48_n37_sgn_bits);
  }
  { // Node ID: 134 (NodeConstantRawBits)
    id134out_value = (c_hw_fix_24_n14_sgn_bits);
  }
  { // Node ID: 567 (NodeConstantRawBits)
    id567out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 208 (NodeConstantRawBits)
    id208out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 224 (NodeConstantRawBits)
    id224out_value = (c_hw_fix_11_0_sgn_bits_2);
  }
  { // Node ID: 295 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id295sta_rom_store, 12, 64); 
  }
  { // Node ID: 177 (NodeConstantRawBits)
    id177out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 180 (NodeConstantRawBits)
    id180out_value = (c_hw_fix_8_n14_uns_bits);
  }
  { // Node ID: 186 (NodeConstantRawBits)
    id186out_value = (c_hw_fix_27_n26_uns_bits);
  }
  { // Node ID: 190 (NodeConstantRawBits)
    id190out_value = (c_hw_fix_15_n14_uns_bits);
  }
  { // Node ID: 194 (NodeConstantRawBits)
    id194out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 566 (NodeConstantRawBits)
    id566out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 229 (NodeConstantRawBits)
    id229out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 565 (NodeConstantRawBits)
    id565out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 564 (NodeConstantRawBits)
    id564out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 563 (NodeConstantRawBits)
    id563out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 265 (NodeConstantRawBits)
    id265out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 252 (NodeConstantRawBits)
    id252out_value = (c_hw_fix_8_0_sgn_bits);
  }
  { // Node ID: 198 (NodeConstantRawBits)
    id198out_value = (c_hw_fix_12_n11_uns_bits_2);
  }
  { // Node ID: 202 (NodeConstantRawBits)
    id202out_value = (c_hw_fix_11_n11_uns_bits);
  }
  { // Node ID: 274 (NodeConstantRawBits)
    id274out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 275 (NodeConstantRawBits)
    id275out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 277 (NodeConstantRawBits)
    id277out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 354 (NodeConstantRawBits)
    id354out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 562 (NodeConstantRawBits)
    id562out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 561 (NodeConstantRawBits)
    id561out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_8_12_bits_7);
  }
  { // Node ID: 22 (NodeConstantRawBits)
    id22out_value = (c_hw_flt_8_12_bits_8);
  }
  { // Node ID: 4 (NodeConstantRawBits)
    id4out_value = (c_hw_flt_8_12_bits_9);
  }
  { // Node ID: 41 (NodeConstantRawBits)
    id41out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 521 (NodeOutputMappedReg)
    registerMappedRegister("all_numeric_exceptions", Data(30), false);
  }
  { // Node ID: 503 (NodeConstantRawBits)
    id503out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 504 (NodeOutput)
    m_internal_watch_carriedu_output = registerOutput("internal_watch_carriedu_output",2 );
  }
  { // Node ID: 560 (NodeConstantRawBits)
    id560out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 331 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 334 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 559 (NodeConstantRawBits)
    id559out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 338 (NodeInputMappedReg)
    registerMappedRegister("io_v_out_force_disabled", Data(1));
  }
  { // Node ID: 341 (NodeOutput)
    m_v_out = registerOutput("v_out",1 );
  }
  { // Node ID: 346 (NodeConstantRawBits)
    id346out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 558 (NodeConstantRawBits)
    id558out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 343 (NodeConstantRawBits)
    id343out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 347 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 557 (NodeConstantRawBits)
    id557out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 349 (NodeConstantRawBits)
    id349out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 352 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void KarmaDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void KarmaDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 17 (NodeCounter)

    (id17st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    (id16st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 543 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id543out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 555 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id555out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 529 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id529out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 542 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id542out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 534 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id534out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 535 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id535out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 536 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id536out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 537 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id537out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 538 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id538out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 539 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id539out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 506 (NodeExceptionMask)

    (id506st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 547 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id547out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 507 (NodeExceptionMask)

    (id507st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 548 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id548out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 508 (NodeExceptionMask)

    (id508st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 549 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id549out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 510 (NodeExceptionMask)

    (id510st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 511 (NodeExceptionMask)

    (id511st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 512 (NodeExceptionMask)

    (id512st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 513 (NodeExceptionMask)

    (id513st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 516 (NodeExceptionMask)

    (id516st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 509 (NodeExceptionMask)

    (id509st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 515 (NodeExceptionMask)

    (id515st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 517 (NodeExceptionMask)

    (id517st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 518 (NodeExceptionMask)

    (id518st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 519 (NodeExceptionMask)

    (id519st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 514 (NodeExceptionMask)

    (id514st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 505 (NodeExceptionMask)

    (id505st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 550 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id550out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 551 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id551out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 331 (NodeInputMappedReg)
    id331out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 552 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id552out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 338 (NodeInputMappedReg)
    id338out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 554 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id554out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 344 (NodeCounter)

    (id344st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 350 (NodeCounter)

    (id350st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 352 (NodeInputMappedReg)
    id352out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::updateState() {
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 331 (NodeInputMappedReg)
    id331out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 338 (NodeInputMappedReg)
    id338out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 352 (NodeInputMappedReg)
    id352out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::preExecute() {
}

void KarmaDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "KarmaDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int KarmaDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
