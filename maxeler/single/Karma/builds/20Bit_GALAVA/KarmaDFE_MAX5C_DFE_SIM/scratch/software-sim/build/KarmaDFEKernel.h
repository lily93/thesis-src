#ifndef KARMADFEKERNEL_H_
#define KARMADFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaDFEKernel : public KernelManagerBlockSync {
public:
  KarmaDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id522out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id556out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id574out_value;

  HWOffsetFix<1,0,UNSIGNED> id355out_result[2];

  HWFloat<8,12> id543out_output[10];

  HWFloat<8,12> id555out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<8,12> id19out_o[4];

  HWFloat<8,12> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id573out_value;

  HWOffsetFix<1,0,UNSIGNED> id356out_result[2];

  HWFloat<8,12> id572out_value;

  HWFloat<8,12> id5out_value;

  HWFloat<8,12> id3out_value;

  HWFloat<8,12> id26out_value;

  HWFloat<8,12> id27out_result[2];

  HWFloat<8,12> id529out_output[11];

  HWFloat<8,12> id571out_value;

  HWFloat<8,12> id570out_value;

  HWFloat<8,12> id569out_value;

  HWRawBits<8> id286out_value;

  HWRawBits<11> id568out_value;

  HWOffsetFix<1,0,UNSIGNED> id542out_output[9];

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<24,-23,UNSIGNED> id77out_value;

  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id79out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id134out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id534out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id567out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id208out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id224out_value;

  HWOffsetFix<12,-12,UNSIGNED> id295out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id295sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id177out_value;

  HWOffsetFix<8,-14,UNSIGNED> id535out_output[3];

  HWOffsetFix<8,-14,UNSIGNED> id180out_value;

  HWOffsetFix<27,-26,UNSIGNED> id186out_value;

  HWOffsetFix<15,-14,UNSIGNED> id190out_value;

  HWOffsetFix<12,-11,UNSIGNED> id194out_value;

  HWOffsetFix<12,-11,UNSIGNED> id566out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id229out_value;

  HWFloat<8,12> id565out_value;

  HWOffsetFix<1,0,UNSIGNED> id536out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id537out_output[3];

  HWFloat<8,12> id564out_value;

  HWOffsetFix<1,0,UNSIGNED> id538out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id539out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id563out_value;

  HWOffsetFix<1,0,UNSIGNED> id265out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id252out_value;

  HWOffsetFix<12,-11,UNSIGNED> id198out_value;

  HWOffsetFix<11,-11,UNSIGNED> id202out_value;

  HWOffsetFix<1,0,UNSIGNED> id274out_value;

  HWOffsetFix<8,0,UNSIGNED> id275out_value;

  HWOffsetFix<11,0,UNSIGNED> id277out_value;

  HWFloat<8,12> id354out_value;

  HWFloat<8,12> id562out_value;

  HWFloat<8,12> id561out_value;

  HWFloat<8,12> id502out_floatOut[2];

  HWFloat<8,12> id2out_value;

  HWFloat<8,12> id22out_value;

  HWFloat<8,12> id23out_result[2];

  HWFloat<8,12> id4out_value;

  HWFloat<8,12> id501out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id501out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id506out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id506st_reg;

  HWOffsetFix<4,0,UNSIGNED> id547out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id507out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id507st_reg;

  HWOffsetFix<1,0,UNSIGNED> id548out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id508out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id508st_reg;

  HWOffsetFix<1,0,UNSIGNED> id549out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id510out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id510st_reg;

  HWOffsetFix<1,0,UNSIGNED> id511out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id511st_reg;

  HWOffsetFix<1,0,UNSIGNED> id512out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id512st_reg;

  HWOffsetFix<1,0,UNSIGNED> id513out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id513st_reg;

  HWOffsetFix<1,0,UNSIGNED> id516out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id516st_reg;

  HWOffsetFix<1,0,UNSIGNED> id509out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id509st_reg;

  HWOffsetFix<1,0,UNSIGNED> id515out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id515st_reg;

  HWOffsetFix<4,0,UNSIGNED> id517out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id517st_reg;

  HWOffsetFix<4,0,UNSIGNED> id518out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id518st_reg;

  HWOffsetFix<4,0,UNSIGNED> id519out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id519st_reg;

  HWOffsetFix<1,0,UNSIGNED> id514out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id514st_reg;

  HWOffsetFix<4,0,UNSIGNED> id505out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id505st_reg;

  HWOffsetFix<4,0,UNSIGNED> id550out_output[9];

  HWRawBits<30> id520out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id503out_value;

  HWOffsetFix<11,0,UNSIGNED> id551out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id560out_value;

  HWOffsetFix<11,0,UNSIGNED> id329out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id497out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id331out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id552out_output[11];

  HWOffsetFix<11,0,UNSIGNED> id559out_value;

  HWOffsetFix<11,0,UNSIGNED> id336out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id498out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id338out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id554out_output[11];

  HWOffsetFix<1,0,UNSIGNED> id346out_value;

  HWOffsetFix<1,0,UNSIGNED> id558out_value;

  HWOffsetFix<49,0,UNSIGNED> id343out_value;

  HWOffsetFix<48,0,UNSIGNED> id344out_count;
  HWOffsetFix<1,0,UNSIGNED> id344out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id344st_count;

  HWOffsetFix<1,0,UNSIGNED> id557out_value;

  HWOffsetFix<49,0,UNSIGNED> id349out_value;

  HWOffsetFix<48,0,UNSIGNED> id350out_count;
  HWOffsetFix<1,0,UNSIGNED> id350out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id350st_count;

  HWOffsetFix<48,0,UNSIGNED> id352out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id499out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,12> c_hw_flt_8_12_undef;
  const HWFloat<8,12> c_hw_flt_8_12_bits;
  const HWFloat<8,12> c_hw_flt_8_12_bits_1;
  const HWFloat<8,12> c_hw_flt_8_12_bits_2;
  const HWFloat<8,12> c_hw_flt_8_12_bits_3;
  const HWFloat<8,12> c_hw_flt_8_12_bits_4;
  const HWFloat<8,12> c_hw_flt_8_12_bits_5;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<24,-14,TWOSCOMPLEMENT> c_hw_fix_24_n14_sgn_bits;
  const HWOffsetFix<24,-14,TWOSCOMPLEMENT> c_hw_fix_24_n14_sgn_undef;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<48,-37,TWOSCOMPLEMENT> c_hw_fix_48_n37_sgn_bits;
  const HWOffsetFix<48,-37,TWOSCOMPLEMENT> c_hw_fix_48_n37_sgn_undef;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<12,-12,UNSIGNED> c_hw_fix_12_n12_uns_undef;
  const HWOffsetFix<8,-8,UNSIGNED> c_hw_fix_8_n8_uns_bits;
  const HWOffsetFix<8,-14,UNSIGNED> c_hw_fix_8_n14_uns_undef;
  const HWOffsetFix<8,-14,UNSIGNED> c_hw_fix_8_n14_uns_bits;
  const HWOffsetFix<27,-26,UNSIGNED> c_hw_fix_27_n26_uns_bits;
  const HWOffsetFix<27,-26,UNSIGNED> c_hw_fix_27_n26_uns_undef;
  const HWOffsetFix<15,-14,UNSIGNED> c_hw_fix_15_n14_uns_bits;
  const HWOffsetFix<15,-14,UNSIGNED> c_hw_fix_15_n14_uns_undef;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_undef;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits_1;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_bits;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_undef;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits_2;
  const HWOffsetFix<11,-11,UNSIGNED> c_hw_fix_11_n11_uns_bits;
  const HWOffsetFix<11,-11,UNSIGNED> c_hw_fix_11_n11_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWFloat<8,12> c_hw_flt_8_12_bits_6;
  const HWFloat<8,12> c_hw_flt_8_12_0_5val;
  const HWFloat<8,12> c_hw_flt_8_12_bits_7;
  const HWFloat<8,12> c_hw_flt_8_12_bits_8;
  const HWFloat<8,12> c_hw_flt_8_12_bits_9;
  const HWFloat<8,12> c_hw_flt_8_12_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMADFEKERNEL_H_ */
