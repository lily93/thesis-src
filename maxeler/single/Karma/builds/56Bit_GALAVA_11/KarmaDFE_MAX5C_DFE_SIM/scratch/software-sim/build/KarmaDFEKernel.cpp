#include "stdsimheader.h"
#include "KarmaDFEKernel.h"

namespace maxcompilersim {

KarmaDFEKernel::KarmaDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 16, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x00bl))))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_11_45_undef((HWFloat<11,45>()))
, c_hw_flt_11_45_bits((HWFloat<11,45>(varint_u<56>(0x3fe9999999999al))))
, c_hw_flt_11_45_bits_1((HWFloat<11,45>(varint_u<56>(0x3ff6db5c4140dfl))))
, c_hw_flt_11_45_bits_2((HWFloat<11,45>(varint_u<56>(0x3ff00000000000l))))
, c_hw_flt_11_45_bits_3((HWFloat<11,45>(varint_u<56>(0x3f70624dd2f1aal))))
, c_hw_flt_11_45_bits_4((HWFloat<11,45>(varint_u<56>(0x00000000000000l))))
, c_hw_flt_11_45_bits_5((HWFloat<11,45>(varint_u<56>(0x40000000000000l))))
, c_hw_bit_11_bits((HWRawBits<11>(varint_u<11>(0x7ffl))))
, c_hw_bit_44_bits((HWRawBits<44>(varint_u<44>(0x00000000000l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_61_n48_sgn_bits((HWOffsetFix<61,-48,TWOSCOMPLEMENT>(varint_u<61>(0x0fffffffffffffffl))))
, c_hw_fix_61_n48_sgn_undef((HWOffsetFix<61,-48,TWOSCOMPLEMENT>()))
, c_hw_fix_52_n51_uns_bits((HWOffsetFix<52,-51,UNSIGNED>(varint_u<52>(0xb8aa3b295c17fl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_113_n99_sgn_bits((HWOffsetFix<113,-99,TWOSCOMPLEMENT>(varint_u<113>::init(2, 0xffffffffffffffffl, 0x0ffffffffffffl))))
, c_hw_fix_113_n99_sgn_undef((HWOffsetFix<113,-99,TWOSCOMPLEMENT>()))
, c_hw_fix_13_0_sgn_undef((HWOffsetFix<13,0,TWOSCOMPLEMENT>()))
, c_hw_fix_14_0_sgn_bits((HWOffsetFix<14,0,TWOSCOMPLEMENT>(varint_u<14>(0x03ffl))))
, c_hw_fix_14_0_sgn_bits_1((HWOffsetFix<14,0,TWOSCOMPLEMENT>(varint_u<14>(0x1fffl))))
, c_hw_fix_14_0_sgn_undef((HWOffsetFix<14,0,TWOSCOMPLEMENT>()))
, c_hw_fix_14_0_sgn_bits_2((HWOffsetFix<14,0,TWOSCOMPLEMENT>(varint_u<14>(0x0001l))))
, c_hw_fix_32_n45_uns_undef((HWOffsetFix<32,-45,UNSIGNED>()))
, c_hw_fix_42_n45_uns_undef((HWOffsetFix<42,-45,UNSIGNED>()))
, c_hw_fix_45_n45_uns_undef((HWOffsetFix<45,-45,UNSIGNED>()))
, c_hw_fix_21_n21_uns_bits((HWOffsetFix<21,-21,UNSIGNED>(varint_u<21>(0x162e43l))))
, c_hw_fix_25_n48_uns_undef((HWOffsetFix<25,-48,UNSIGNED>()))
, c_hw_fix_25_n48_uns_bits((HWOffsetFix<25,-48,UNSIGNED>(varint_u<25>(0x1ffffffl))))
, c_hw_fix_64_n87_uns_bits((HWOffsetFix<64,-87,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n87_uns_undef((HWOffsetFix<64,-87,UNSIGNED>()))
, c_hw_fix_64_n63_uns_bits((HWOffsetFix<64,-63,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n63_uns_undef((HWOffsetFix<64,-63,UNSIGNED>()))
, c_hw_fix_49_n48_uns_bits((HWOffsetFix<49,-48,UNSIGNED>(varint_u<49>(0x1ffffffffffffl))))
, c_hw_fix_49_n48_uns_undef((HWOffsetFix<49,-48,UNSIGNED>()))
, c_hw_fix_64_n66_uns_bits((HWOffsetFix<64,-66,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n66_uns_undef((HWOffsetFix<64,-66,UNSIGNED>()))
, c_hw_fix_64_n62_uns_bits((HWOffsetFix<64,-62,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n62_uns_undef((HWOffsetFix<64,-62,UNSIGNED>()))
, c_hw_fix_50_n48_uns_bits((HWOffsetFix<50,-48,UNSIGNED>(varint_u<50>(0x3ffffffffffffl))))
, c_hw_fix_50_n48_uns_undef((HWOffsetFix<50,-48,UNSIGNED>()))
, c_hw_fix_64_n75_uns_bits((HWOffsetFix<64,-75,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n75_uns_undef((HWOffsetFix<64,-75,UNSIGNED>()))
, c_hw_fix_64_n61_uns_bits((HWOffsetFix<64,-61,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n61_uns_undef((HWOffsetFix<64,-61,UNSIGNED>()))
, c_hw_fix_51_n48_uns_bits((HWOffsetFix<51,-48,UNSIGNED>(varint_u<51>(0x7ffffffffffffl))))
, c_hw_fix_51_n48_uns_undef((HWOffsetFix<51,-48,UNSIGNED>()))
, c_hw_fix_45_n44_uns_bits((HWOffsetFix<45,-44,UNSIGNED>(varint_u<45>(0x1fffffffffffl))))
, c_hw_fix_45_n44_uns_undef((HWOffsetFix<45,-44,UNSIGNED>()))
, c_hw_fix_45_n44_uns_bits_1((HWOffsetFix<45,-44,UNSIGNED>(varint_u<45>(0x100000000000l))))
, c_hw_fix_15_0_sgn_bits((HWOffsetFix<15,0,TWOSCOMPLEMENT>(varint_u<15>(0x0000l))))
, c_hw_fix_15_0_sgn_undef((HWOffsetFix<15,0,TWOSCOMPLEMENT>()))
, c_hw_fix_14_0_sgn_bits_3((HWOffsetFix<14,0,TWOSCOMPLEMENT>(varint_u<14>(0x07ffl))))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x3ffl))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_45_n44_uns_bits_2((HWOffsetFix<45,-44,UNSIGNED>(varint_u<45>(0x000000000000l))))
, c_hw_fix_44_n44_uns_bits((HWOffsetFix<44,-44,UNSIGNED>(varint_u<44>(0xfffffffffffl))))
, c_hw_fix_44_n44_uns_undef((HWOffsetFix<44,-44,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x7ffl))))
, c_hw_fix_44_0_uns_bits((HWOffsetFix<44,0,UNSIGNED>(varint_u<44>(0x00000000000l))))
, c_hw_flt_11_45_bits_6((HWFloat<11,45>(varint_u<56>(0x7ff80000000000l))))
, c_hw_flt_11_45_0_5val((HWFloat<11,45>(varint_u<56>(0x3fe00000000000l))))
, c_hw_flt_11_45_bits_7((HWFloat<11,45>(varint_u<56>(0x3fd9999999999al))))
, c_hw_flt_11_45_bits_8((HWFloat<11,45>(varint_u<56>(0x3ff80000000000l))))
, c_hw_flt_11_45_bits_9((HWFloat<11,45>(varint_u<56>(0x40080000000000l))))
, c_hw_flt_11_45_2_0val((HWFloat<11,45>(varint_u<56>(0x40000000000000l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_4_0_uns_undef((HWOffsetFix<4,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_2((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 862 (NodeConstantRawBits)
    id862out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 14 (NodeConstantRawBits)
    id14out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 896 (NodeConstantRawBits)
    id896out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 914 (NodeConstantRawBits)
    id914out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 18 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 6 (NodeConstantRawBits)
    id6out_value = (c_hw_flt_11_45_bits);
  }
  { // Node ID: 913 (NodeConstantRawBits)
    id913out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 912 (NodeConstantRawBits)
    id912out_value = (c_hw_flt_11_45_bits_1);
  }
  { // Node ID: 5 (NodeConstantRawBits)
    id5out_value = (c_hw_flt_11_45_bits_2);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_11_45_bits_3);
  }
  { // Node ID: 26 (NodeConstantRawBits)
    id26out_value = (c_hw_flt_11_45_bits_4);
  }
  { // Node ID: 911 (NodeConstantRawBits)
    id911out_value = (c_hw_flt_11_45_bits_2);
  }
  { // Node ID: 910 (NodeConstantRawBits)
    id910out_value = (c_hw_flt_11_45_bits_2);
  }
  { // Node ID: 909 (NodeConstantRawBits)
    id909out_value = (c_hw_flt_11_45_bits_5);
  }
  { // Node ID: 465 (NodeConstantRawBits)
    id465out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 908 (NodeConstantRawBits)
    id908out_value = (c_hw_bit_44_bits);
  }
  { // Node ID: 46 (NodeConstantRawBits)
    id46out_value = (c_hw_fix_61_n48_sgn_bits);
  }
  { // Node ID: 114 (NodeConstantRawBits)
    id114out_value = (c_hw_fix_52_n51_uns_bits);
  }
  { // Node ID: 116 (NodeConstantRawBits)
    id116out_value = (c_hw_fix_113_n99_sgn_bits);
  }
  { // Node ID: 236 (NodeConstantRawBits)
    id236out_value = (c_hw_fix_61_n48_sgn_bits);
  }
  { // Node ID: 907 (NodeConstantRawBits)
    id907out_value = (c_hw_fix_14_0_sgn_bits);
  }
  { // Node ID: 378 (NodeConstantRawBits)
    id378out_value = (c_hw_fix_14_0_sgn_bits_1);
  }
  { // Node ID: 397 (NodeConstantRawBits)
    id397out_value = (c_hw_fix_14_0_sgn_bits_2);
  }
  { // Node ID: 480 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2c5c86,
      0x58b90c,
      0x851593,
      0xb1721a,
      0xddcea1,
      0x10a2b28,
      0x13687b0,
      0x162e438,
      0x18f40c0,
      0x1bb9d48,
      0x1e7f9d0,
      0x2145659,
      0x240b2e2,
      0x26d0f6b,
      0x2996bf5,
      0x2c5c87f,
      0x2f22509,
      0x31e8193,
      0x34ade1d,
      0x3773aa8,
      0x3a39733,
      0x3cff3be,
      0x3fc5049,
      0x428acd5,
      0x4550961,
      0x48165ed,
      0x4adc279,
      0x4da1f06,
      0x5067b93,
      0x532d820,
      0x55f34ad,
      0x58b913b,
      0x5b7edc9,
      0x5e44a57,
      0x610a6e5,
      0x63d0373,
      0x6696002,
      0x695bc91,
      0x6c21920,
      0x6ee75b0,
      0x71ad240,
      0x7472ed0,
      0x7738b60,
      0x79fe7f0,
      0x7cc4481,
      0x7f8a112,
      0x824fda3,
      0x8515a34,
      0x87db6c6,
      0x8aa1358,
      0x8d66fea,
      0x902cc7c,
      0x92f290f,
      0x95b85a2,
      0x987e235,
      0x9b43ec8,
      0x9e09b5c,
      0xa0cf7f0,
      0xa395484,
      0xa65b118,
      0xa920dac,
      0xabe6a41,
      0xaeac6d6,
      0xb17236b,
      0xb438001,
      0xb6fdc97,
      0xb9c392d,
      0xbc895c3,
      0xbf4f259,
      0xc214ef0,
      0xc4dab87,
      0xc7a081e,
      0xca664b6,
      0xcd2c14d,
      0xcff1de5,
      0xd2b7a7d,
      0xd57d716,
      0xd8433ae,
      0xdb09047,
      0xddcece0,
      0xe094979,
      0xe35a613,
      0xe6202ad,
      0xe8e5f47,
      0xebabbe1,
      0xee7187c,
      0xf137516,
      0xf3fd1b1,
      0xf6c2e4d,
      0xf988ae8,
      0xfc4e784,
      0xff14420,
      0x101da0bc,
      0x1049fd59,
      0x107659f5,
      0x10a2b692,
      0x10cf132f,
      0x10fb6fcd,
      0x1127cc6a,
      0x11542908,
      0x118085a6,
      0x11ace245,
      0x11d93ee3,
      0x12059b82,
      0x1231f821,
      0x125e54c1,
      0x128ab160,
      0x12b70e00,
      0x12e36aa0,
      0x130fc740,
      0x133c23e1,
      0x13688082,
      0x1394dd23,
      0x13c139c4,
      0x13ed9666,
      0x1419f307,
      0x14464fa9,
      0x1472ac4c,
      0x149f08ee,
      0x14cb6591,
      0x14f7c234,
      0x15241ed7,
      0x15507b7a,
      0x157cd81e,
      0x15a934c2,
      0x15d59166,
      0x1601ee0a,
      0x162e4aaf,
      0x165aa754,
      0x168703f9,
      0x16b3609e,
      0x16dfbd44,
      0x170c19ea,
      0x17387690,
      0x1764d336,
      0x17912fdd,
      0x17bd8c83,
      0x17e9e92a,
      0x181645d2,
      0x1842a279,
      0x186eff21,
      0x189b5bc9,
      0x18c7b871,
      0x18f4151a,
      0x192071c2,
      0x194cce6b,
      0x19792b14,
      0x19a587be,
      0x19d1e467,
      0x19fe4111,
      0x1a2a9dbc,
      0x1a56fa66,
      0x1a835711,
      0x1aafb3bb,
      0x1adc1066,
      0x1b086d12,
      0x1b34c9bd,
      0x1b612669,
      0x1b8d8315,
      0x1bb9dfc2,
      0x1be63c6e,
      0x1c12991b,
      0x1c3ef5c8,
      0x1c6b5275,
      0x1c97af23,
      0x1cc40bd1,
      0x1cf0687f,
      0x1d1cc52d,
      0x1d4921db,
      0x1d757e8a,
      0x1da1db39,
      0x1dce37e8,
      0x1dfa9498,
      0x1e26f147,
      0x1e534df7,
      0x1e7faaa7,
      0x1eac0758,
      0x1ed86408,
      0x1f04c0b9,
      0x1f311d6a,
      0x1f5d7a1c,
      0x1f89d6cd,
      0x1fb6337f,
      0x1fe29031,
      0x200eece3,
      0x203b4996,
      0x2067a649,
      0x209402fc,
      0x20c05faf,
      0x20ecbc63,
      0x21191916,
      0x214575ca,
      0x2171d27f,
      0x219e2f33,
      0x21ca8be8,
      0x21f6e89d,
      0x22234552,
      0x224fa207,
      0x227bfebd,
      0x22a85b73,
      0x22d4b829,
      0x230114e0,
      0x232d7196,
      0x2359ce4d,
      0x23862b04,
      0x23b287bc,
      0x23dee473,
      0x240b412b,
      0x24379de3,
      0x2463fa9b,
      0x24905754,
      0x24bcb40d,
      0x24e910c6,
      0x25156d7f,
      0x2541ca39,
      0x256e26f2,
      0x259a83ac,
      0x25c6e067,
      0x25f33d21,
      0x261f99dc,
      0x264bf697,
      0x26785352,
      0x26a4b00d,
      0x26d10cc9,
      0x26fd6985,
      0x2729c641,
      0x275622fe,
      0x27827fba,
      0x27aedc77,
      0x27db3934,
      0x280795f2,
      0x2833f2af,
      0x28604f6d,
      0x288cac2b,
      0x28b908e9,
      0x28e565a8,
      0x2911c267,
      0x293e1f26,
      0x296a7be5,
      0x2996d8a5,
      0x29c33564,
      0x29ef9224,
      0x2a1beee5,
      0x2a484ba5,
      0x2a74a866,
      0x2aa10527,
      0x2acd61e8,
      0x2af9beaa,
      0x2b261b6b,
      0x2b52782d,
      0x2b7ed4ef,
      0x2bab31b2,
      0x2bd78e74,
      0x2c03eb37,
      0x2c3047fa,
      0x2c5ca4be,
      0x2c890181,
      0x2cb55e45,
      0x2ce1bb09,
      0x2d0e17ce,
      0x2d3a7492,
      0x2d66d157,
      0x2d932e1c,
      0x2dbf8ae1,
      0x2debe7a7,
      0x2e18446d,
      0x2e44a133,
      0x2e70fdf9,
      0x2e9d5abf,
      0x2ec9b786,
      0x2ef6144d,
      0x2f227114,
      0x2f4ecddc,
      0x2f7b2aa4,
      0x2fa7876b,
      0x2fd3e434,
      0x300040fc,
      0x302c9dc5,
      0x3058fa8e,
      0x30855757,
      0x30b1b420,
      0x30de10ea,
      0x310a6db4,
      0x3136ca7e,
      0x31632748,
      0x318f8413,
      0x31bbe0dd,
      0x31e83da8,
      0x32149a74,
      0x3240f73f,
      0x326d540b,
      0x3299b0d7,
      0x32c60da3,
      0x32f26a70,
      0x331ec73d,
      0x334b240a,
      0x337780d7,
      0x33a3dda4,
      0x33d03a72,
      0x33fc9740,
      0x3428f40e,
      0x345550dc,
      0x3481adab,
      0x34ae0a7a,
      0x34da6749,
      0x3506c419,
      0x353320e8,
      0x355f7db8,
      0x358bda88,
      0x35b83759,
      0x35e49429,
      0x3610f0fa,
      0x363d4dcb,
      0x3669aa9c,
      0x3696076e,
      0x36c26440,
      0x36eec112,
      0x371b1de4,
      0x37477ab6,
      0x3773d789,
      0x37a0345c,
      0x37cc912f,
      0x37f8ee03,
      0x38254ad7,
      0x3851a7aa,
      0x387e047f,
      0x38aa6153,
      0x38d6be28,
      0x39031afd,
      0x392f77d2,
      0x395bd4a7,
      0x3988317d,
      0x39b48e53,
      0x39e0eb29,
      0x3a0d47ff,
      0x3a39a4d6,
      0x3a6601ad,
      0x3a925e84,
      0x3abebb5b,
      0x3aeb1833,
      0x3b17750a,
      0x3b43d1e2,
      0x3b702ebb,
      0x3b9c8b93,
      0x3bc8e86c,
      0x3bf54545,
      0x3c21a21e,
      0x3c4dfef8,
      0x3c7a5bd1,
      0x3ca6b8ab,
      0x3cd31585,
      0x3cff7260,
      0x3d2bcf3b,
      0x3d582c15,
      0x3d8488f1,
      0x3db0e5cc,
      0x3ddd42a8,
      0x3e099f83,
      0x3e35fc60,
      0x3e62593c,
      0x3e8eb619,
      0x3ebb12f5,
      0x3ee76fd2,
      0x3f13ccb0,
      0x3f40298d,
      0x3f6c866b,
      0x3f98e349,
      0x3fc54027,
      0x3ff19d06,
      0x401df9e5,
      0x404a56c4,
      0x4076b3a3,
      0x40a31082,
      0x40cf6d62,
      0x40fbca42,
      0x41282722,
      0x41548403,
      0x4180e0e3,
      0x41ad3dc4,
      0x41d99aa5,
      0x4205f787,
      0x42325469,
      0x425eb14a,
      0x428b0e2d,
      0x42b76b0f,
      0x42e3c7f2,
      0x431024d4,
      0x433c81b7,
      0x4368de9b,
      0x43953b7e,
      0x43c19862,
      0x43edf546,
      0x441a522a,
      0x4446af0f,
      0x44730bf4,
      0x449f68d9,
      0x44cbc5be,
      0x44f822a3,
      0x45247f89,
      0x4550dc6f,
      0x457d3955,
      0x45a9963c,
      0x45d5f322,
      0x46025009,
      0x462eacf1,
      0x465b09d8,
      0x468766c0,
      0x46b3c3a7,
      0x46e02090,
      0x470c7d78,
      0x4738da61,
      0x47653749,
      0x47919433,
      0x47bdf11c,
      0x47ea4e05,
      0x4816aaef,
      0x484307d9,
      0x486f64c4,
      0x489bc1ae,
      0x48c81e99,
      0x48f47b84,
      0x4920d86f,
      0x494d355b,
      0x49799246,
      0x49a5ef32,
      0x49d24c1f,
      0x49fea90b,
      0x4a2b05f8,
      0x4a5762e5,
      0x4a83bfd2,
      0x4ab01cbf,
      0x4adc79ad,
      0x4b08d69b,
      0x4b353389,
      0x4b619077,
      0x4b8ded66,
      0x4bba4a55,
      0x4be6a744,
      0x4c130433,
      0x4c3f6123,
      0x4c6bbe13,
      0x4c981b03,
      0x4cc477f3,
      0x4cf0d4e3,
      0x4d1d31d4,
      0x4d498ec5,
      0x4d75ebb6,
      0x4da248a8,
      0x4dcea59a,
      0x4dfb028c,
      0x4e275f7e,
      0x4e53bc70,
      0x4e801963,
      0x4eac7656,
      0x4ed8d349,
      0x4f05303d,
      0x4f318d30,
      0x4f5dea24,
      0x4f8a4718,
      0x4fb6a40d,
      0x4fe30101,
      0x500f5df6,
      0x503bbaeb,
      0x506817e1,
      0x509474d6,
      0x50c0d1cc,
      0x50ed2ec2,
      0x51198bb8,
      0x5145e8af,
      0x517245a6,
      0x519ea29d,
      0x51caff94,
      0x51f75c8b,
      0x5223b983,
      0x5250167b,
      0x527c7373,
      0x52a8d06c,
      0x52d52d64,
      0x53018a5d,
      0x532de757,
      0x535a4450,
      0x5386a14a,
      0x53b2fe44,
      0x53df5b3e,
      0x540bb838,
      0x54381533,
      0x5464722e,
      0x5490cf29,
      0x54bd2c24,
      0x54e98920,
      0x5515e61b,
      0x55424318,
      0x556ea014,
      0x559afd10,
      0x55c75a0d,
      0x55f3b70a,
      0x56201407,
      0x564c7105,
      0x5678ce03,
      0x56a52b01,
      0x56d187ff,
      0x56fde4fd,
      0x572a41fc,
      0x57569efb,
      0x5782fbfa,
      0x57af58fa,
      0x57dbb5f9,
      0x580812f9,
      0x58346ff9,
      0x5860ccfa,
      0x588d29fa,
      0x58b986fb,
      0x58e5e3fc,
      0x591240fe,
      0x593e9dff,
      0x596afb01,
      0x59975803,
      0x59c3b506,
      0x59f01208,
      0x5a1c6f0b,
      0x5a48cc0e,
      0x5a752911,
      0x5aa18615,
      0x5acde319,
      0x5afa401d,
      0x5b269d21,
      0x5b52fa25,
      0x5b7f572a,
      0x5babb42f,
      0x5bd81134,
      0x5c046e3a,
      0x5c30cb3f,
      0x5c5d2845,
      0x5c89854b,
      0x5cb5e252,
      0x5ce23f58,
      0x5d0e9c5f,
      0x5d3af966,
      0x5d67566e,
      0x5d93b375,
      0x5dc0107d,
      0x5dec6d85,
      0x5e18ca8d,
      0x5e452796,
      0x5e71849f,
      0x5e9de1a8,
      0x5eca3eb1,
      0x5ef69bbb,
      0x5f22f8c4,
      0x5f4f55ce,
      0x5f7bb2d9,
      0x5fa80fe3,
      0x5fd46cee,
      0x6000c9f9,
      0x602d2704,
      0x6059840f,
      0x6085e11b,
      0x60b23e27,
      0x60de9b33,
      0x610af840,
      0x6137554c,
      0x6163b259,
      0x61900f66,
      0x61bc6c74,
      0x61e8c981,
      0x6215268f,
      0x6241839d,
      0x626de0ab,
      0x629a3dba,
      0x62c69ac9,
      0x62f2f7d8,
      0x631f54e7,
      0x634bb1f7,
      0x63780f06,
      0x63a46c16,
      0x63d0c927,
      0x63fd2637,
      0x64298348,
      0x6455e059,
      0x64823d6a,
      0x64ae9a7c,
      0x64daf78d,
      0x6507549f,
      0x6533b1b1,
      0x65600ec4,
      0x658c6bd6,
      0x65b8c8e9,
      0x65e525fc,
      0x66118310,
      0x663de023,
      0x666a3d37,
      0x66969a4b,
      0x66c2f760,
      0x66ef5474,
      0x671bb189,
      0x67480e9e,
      0x67746bb3,
      0x67a0c8c9,
      0x67cd25df,
      0x67f982f5,
      0x6825e00b,
      0x68523d21,
      0x687e9a38,
      0x68aaf74f,
      0x68d75466,
      0x6903b17e,
      0x69300e95,
      0x695c6bad,
      0x6988c8c6,
      0x69b525de,
      0x69e182f7,
      0x6a0de010,
      0x6a3a3d29,
      0x6a669a42,
      0x6a92f75c,
      0x6abf5476,
      0x6aebb190,
      0x6b180eaa,
      0x6b446bc5,
      0x6b70c8df,
      0x6b9d25fa,
      0x6bc98316,
      0x6bf5e031,
      0x6c223d4d,
      0x6c4e9a69,
      0x6c7af785,
      0x6ca754a2,
      0x6cd3b1bf,
      0x6d000edc,
      0x6d2c6bf9,
      0x6d58c916,
      0x6d852634,
      0x6db18352,
      0x6ddde070,
      0x6e0a3d8e,
      0x6e369aad,
      0x6e62f7cc,
      0x6e8f54eb,
      0x6ebbb20b,
      0x6ee80f2a,
      0x6f146c4a,
      0x6f40c96a,
      0x6f6d268a,
      0x6f9983ab,
      0x6fc5e0cc,
      0x6ff23ded,
      0x701e9b0e,
      0x704af830,
      0x70775552,
      0x70a3b274,
      0x70d00f96,
      0x70fc6cb8,
      0x7128c9db,
      0x715526fe,
      0x71818421,
      0x71ade145,
      0x71da3e68,
      0x72069b8c,
      0x7232f8b1,
      0x725f55d5,
      0x728bb2fa,
      0x72b8101f,
      0x72e46d44,
      0x7310ca69,
      0x733d278f,
      0x736984b5,
      0x7395e1db,
      0x73c23f01,
      0x73ee9c28,
      0x741af94e,
      0x74475676,
      0x7473b39d,
      0x74a010c4,
      0x74cc6dec,
      0x74f8cb14,
      0x7525283c,
      0x75518565,
      0x757de28e,
      0x75aa3fb7,
      0x75d69ce0,
      0x7602fa09,
      0x762f5733,
      0x765bb45d,
      0x76881187,
      0x76b46eb2,
      0x76e0cbdc,
      0x770d2907,
      0x77398632,
      0x7765e35e,
      0x77924089,
      0x77be9db5,
      0x77eafae1,
      0x7817580e,
      0x7843b53a,
      0x78701267,
      0x789c6f94,
      0x78c8ccc2,
      0x78f529ef,
      0x7921871d,
      0x794de44b,
      0x797a4179,
      0x79a69ea8,
      0x79d2fbd7,
      0x79ff5906,
      0x7a2bb635,
      0x7a581364,
      0x7a847094,
      0x7ab0cdc4,
      0x7add2af4,
      0x7b098825,
      0x7b35e555,
      0x7b624286,
      0x7b8e9fb7,
      0x7bbafce9,
      0x7be75a1a,
      0x7c13b74c,
      0x7c40147e,
      0x7c6c71b1,
      0x7c98cee3,
      0x7cc52c16,
      0x7cf18949,
      0x7d1de67d,
      0x7d4a43b0,
      0x7d76a0e4,
      0x7da2fe18,
      0x7dcf5b4c,
      0x7dfbb881,
      0x7e2815b6,
      0x7e5472eb,
      0x7e80d020,
      0x7ead2d55,
      0x7ed98a8b,
      0x7f05e7c1,
      0x7f3244f7,
      0x7f5ea22e,
      0x7f8aff64,
      0x7fb75c9b,
      0x7fe3b9d2,
      0x8010170a,
      0x803c7441,
      0x8068d179,
      0x80952eb1,
      0x80c18bea,
      0x80ede922,
      0x811a465b,
      0x8146a394,
      0x817300ce,
      0x819f5e07,
      0x81cbbb41,
      0x81f8187b,
      0x822475b5,
      0x8250d2f0,
      0x827d302b,
      0x82a98d66,
      0x82d5eaa1,
      0x830247dc,
      0x832ea518,
      0x835b0254,
      0x83875f90,
      0x83b3bccd,
      0x83e01a09,
      0x840c7746,
      0x8438d484,
      0x846531c1,
      0x84918eff,
      0x84bdec3d,
      0x84ea497b,
      0x8516a6b9,
      0x854303f8,
      0x856f6137,
      0x859bbe76,
      0x85c81bb5,
      0x85f478f5,
      0x8620d634,
      0x864d3374,
      0x867990b5,
      0x86a5edf5,
      0x86d24b36,
      0x86fea877,
      0x872b05b8,
      0x875762fa,
      0x8783c03b,
      0x87b01d7d,
      0x87dc7ac0,
      0x8808d802,
      0x88353545,
      0x88619288,
      0x888defcb,
      0x88ba4d0e,
      0x88e6aa52,
      0x89130796,
      0x893f64da,
      0x896bc21e,
      0x89981f63,
      0x89c47ca8,
      0x89f0d9ed,
      0x8a1d3732,
      0x8a499478,
      0x8a75f1be,
      0x8aa24f04,
      0x8aceac4a,
      0x8afb0991,
      0x8b2766d7,
      0x8b53c41e,
      0x8b802166,
      0x8bac7ead,
      0x8bd8dbf5,
      0x8c05393d,
      0x8c319685,
      0x8c5df3ce,
      0x8c8a5116,
      0x8cb6ae5f,
      0x8ce30ba8,
      0x8d0f68f2,
      0x8d3bc63c,
      0x8d682385,
      0x8d9480d0,
      0x8dc0de1a,
      0x8ded3b65,
      0x8e1998af,
      0x8e45f5fb,
      0x8e725346,
      0x8e9eb091,
      0x8ecb0ddd,
      0x8ef76b29,
      0x8f23c876,
      0x8f5025c2,
      0x8f7c830f,
      0x8fa8e05c,
      0x8fd53da9,
      0x90019af7,
      0x902df845,
      0x905a5593,
      0x9086b2e1,
      0x90b3102f,
      0x90df6d7e,
      0x910bcacd,
      0x9138281c,
      0x9164856b,
      0x9190e2bb,
      0x91bd400b,
      0x91e99d5b,
      0x9215faac,
      0x924257fc,
      0x926eb54d,
      0x929b129e,
      0x92c76ff0,
      0x92f3cd41,
      0x93202a93,
      0x934c87e5,
      0x9378e537,
      0x93a5428a,
      0x93d19fdd,
      0x93fdfd30,
      0x942a5a83,
      0x9456b7d7,
      0x9483152a,
      0x94af727e,
      0x94dbcfd3,
      0x95082d27,
      0x95348a7c,
      0x9560e7d1,
      0x958d4526,
      0x95b9a27b,
      0x95e5ffd1,
      0x96125d27,
      0x963eba7d,
      0x966b17d3,
      0x9697752a,
      0x96c3d281,
      0x96f02fd8,
      0x971c8d2f,
      0x9748ea87,
      0x977547df,
      0x97a1a537,
      0x97ce028f,
      0x97fa5fe8,
      0x9826bd41,
      0x98531a9a,
      0x987f77f3,
      0x98abd54c,
      0x98d832a6,
      0x99049000,
      0x9930ed5a,
      0x995d4ab5,
      0x9989a810,
      0x99b6056b,
      0x99e262c6,
      0x9a0ec021,
      0x9a3b1d7d,
      0x9a677ad9,
      0x9a93d835,
      0x9ac03591,
      0x9aec92ee,
      0x9b18f04b,
      0x9b454da8,
      0x9b71ab05,
      0x9b9e0863,
      0x9bca65c1,
      0x9bf6c31f,
      0x9c23207d,
      0x9c4f7ddc,
      0x9c7bdb3b,
      0x9ca8389a,
      0x9cd495f9,
      0x9d00f359,
      0x9d2d50b8,
      0x9d59ae18,
      0x9d860b79,
      0x9db268d9,
      0x9ddec63a,
      0x9e0b239b,
      0x9e3780fc,
      0x9e63de5d,
      0x9e903bbf,
      0x9ebc9921,
      0x9ee8f683,
      0x9f1553e6,
      0x9f41b148,
      0x9f6e0eab,
      0x9f9a6c0e,
      0x9fc6c972,
      0x9ff326d5,
      0xa01f8439,
      0xa04be19d,
      0xa0783f01,
      0xa0a49c66,
      0xa0d0f9cb,
      0xa0fd5730,
      0xa129b495,
      0xa15611fb,
      0xa1826f60,
      0xa1aeccc6,
      0xa1db2a2d,
      0xa2078793,
      0xa233e4fa,
      0xa2604261,
      0xa28c9fc8,
      0xa2b8fd30,
      0xa2e55a97,
      0xa311b7ff,
      0xa33e1567,
      0xa36a72d0,
      0xa396d038,
      0xa3c32da1,
      0xa3ef8b0b,
      0xa41be874,
      0xa44845de,
      0xa474a347,
      0xa4a100b1,
      0xa4cd5e1c,
      0xa4f9bb86,
      0xa52618f1,
      0xa552765c,
      0xa57ed3c8,
      0xa5ab3133,
      0xa5d78e9f,
      0xa603ec0b,
      0xa6304977,
      0xa65ca6e4,
      0xa6890450,
      0xa6b561bd,
      0xa6e1bf2b,
      0xa70e1c98,
      0xa73a7a06,
      0xa766d774,
      0xa79334e2,
      0xa7bf9250,
      0xa7ebefbf,
      0xa8184d2e,
      0xa844aa9d,
      0xa871080c,
      0xa89d657c,
      0xa8c9c2ec,
      0xa8f6205c,
      0xa9227dcc,
      0xa94edb3d,
      0xa97b38ae,
      0xa9a7961f,
      0xa9d3f390,
      0xaa005102,
      0xaa2cae73,
      0xaa590be5,
      0xaa856958,
      0xaab1c6ca,
      0xaade243d,
      0xab0a81b0,
      0xab36df23,
      0xab633c97,
      0xab8f9a0a,
      0xabbbf77e,
      0xabe854f2,
      0xac14b267,
      0xac410fdc,
      0xac6d6d50,
      0xac99cac6,
      0xacc6283b,
      0xacf285b1,
      0xad1ee326,
      0xad4b409d,
      0xad779e13,
      0xada3fb8a,
      0xadd05900,
      0xadfcb677,
      0xae2913ef,
      0xae557166,
      0xae81cede,
      0xaeae2c56,
      0xaeda89ce,
      0xaf06e747,
      0xaf3344c0,
      0xaf5fa239,
      0xaf8bffb2,
      0xafb85d2b,
      0xafe4baa5,
      0xb011181f,
      0xb03d7599,
      0xb069d314,
      0xb096308e,
      0xb0c28e09,
      0xb0eeeb84,
      0xb11b4900,
      0xb147a67b,
    };
    setRom< HWOffsetFix<32,-45,UNSIGNED> > (data, id480sta_rom_store, 32, 1024); 
  }
  { // Node ID: 477 (NodeROM)
    uint64_t data[] = {
      0x0,
      0xb17403f7,
      0x162ebdffc,
      0x214679422,
      0x2c5e72081,
      0x3776a852c,
      0x428f1c23a,
      0x4da7cd7bf,
      0x58c0bc5d2,
      0x63d9e8c86,
      0x6ef352bf3,
      0x7a0cfa42c,
      0x8526df548,
      0x904101f5b,
      0x9b5b6227b,
      0xa675ffebe,
      0xb190db438,
      0xbcabf42ff,
      0xc7c74ab29,
      0xd2e2decca,
      0xddfeb07f8,
      0xe91abfcc9,
      0xf4370cb52,
      0xff53973a7,
      0x10a705f5df,
      0x1158d6520f,
      0x120aaa884c,
      0x12bc8298ab,
      0x136e5e8342,
      0x14203e4827,
      0x14d221e76e,
      0x158409612d,
      0x1635f4b579,
      0x16e7e3e469,
      0x1799d6ee10,
      0x184bcdd285,
      0x18fdc891dc,
      0x19afc72c2c,
      0x1a61c9a189,
      0x1b13cff209,
      0x1bc5da1dc2,
      0x1c77e824c8,
      0x1d29fa0732,
      0x1ddc0fc514,
      0x1e8e295e84,
      0x1f4046d397,
      0x1ff2682463,
      0x20a48d50fd,
      0x2156b6597a,
      0x2208e33df0,
      0x22bb13fe75,
      0x236d489b1d,
      0x241f8113ff,
      0x24d1bd692f,
      0x2583fd9ac2,
      0x263641a8d0,
      0x26e889936b,
      0x279ad55aab,
      0x284d24fea5,
      0x28ff787f6d,
      0x29b1cfdd1a,
      0x2a642b17c1,
      0x2b168a2f77,
      0x2bc8ed2452,
      0x2c7b53f666,
      0x2d2dbea5cb,
      0x2de02d3294,
      0x2e929f9cd7,
      0x2f4515e4ab,
      0x2ff7900a23,
      0x30aa0e0d57,
      0x315c8fee5a,
      0x320f15ad43,
      0x32c19f4a27,
      0x33742cc51c,
      0x3426be1e36,
      0x34d953558c,
      0x358bec6b32,
      0x363e895f3f,
      0x36f12a31c8,
      0x37a3cee2e1,
      0x38567772a2,
      0x390923e11e,
      0x39bbd42e6c,
      0x3a6e885aa1,
      0x3b214065d2,
      0x3bd3fc5016,
      0x3c86bc1980,
      0x3d397fc228,
      0x3dec474a22,
      0x3e9f12b184,
      0x3f51e1f863,
      0x4004b51ed4,
      0x40b78c24ee,
      0x416a670ac6,
      0x421d45d071,
      0x42d0287605,
      0x43830efb97,
      0x4435f9613c,
      0x44e8e7a70b,
      0x459bd9cd19,
      0x464ecfd37a,
      0x4701c9ba45,
      0x47b4c78190,
      0x4867c9296f,
      0x491aceb1f8,
      0x49cdd81b41,
      0x4a80e5655e,
      0x4b33f69067,
      0x4be70b9c70,
      0x4c9a24898e,
      0x4d4d4157d8,
      0x4e00620762,
      0x4eb3869843,
      0x4f66af0a8f,
      0x5019db5e5c,
      0x50cd0b93c1,
      0x51803faad1,
      0x523377a3a4,
      0x52e6b37e4d,
      0x5399f33ae4,
      0x544d36d97d,
      0x55007e5a2e,
      0x55b3c9bd0d,
      0x566719022e,
      0x571a6c29a8,
      0x57cdc33390,
      0x58811e1ffc,
      0x59347cef01,
      0x59e7dfa0b4,
      0x5a9b46352c,
      0x5b4eb0ac7e,
      0x5c021f06bf,
      0x5cb5914405,
      0x5d69076465,
      0x5e1c8167f6,
      0x5ecfff4ecc,
      0x5f838118fd,
      0x603706c69f,
      0x60ea9057c7,
      0x619e1dcc8b,
      0x6251af2500,
      0x630544613d,
      0x63b8dd8155,
      0x646c7a8560,
      0x65201b6d73,
      0x65d3c039a3,
      0x668768ea06,
      0x673b157eb1,
      0x67eec5f7ba,
      0x68a27a5536,
      0x695632973c,
      0x6a09eebde1,
      0x6abdaec93a,
      0x6b7172b95d,
      0x6c253a8e5f,
      0x6cd9064857,
      0x6d8cd5e759,
      0x6e40a96b7b,
      0x6ef480d4d4,
      0x6fa85c2378,
      0x705c3b577d,
      0x71101e70f8,
      0x71c4057000,
      0x7277f054aa,
      0x732bdf1f0c,
      0x73dfd1cf3b,
      0x7493c8654c,
      0x7547c2e156,
      0x75fbc1436e,
      0x76afc38ba9,
      0x7763c9ba1e,
      0x7817d3cee2,
      0x78cbe1ca0a,
      0x797ff3abac,
      0x7a340973de,
      0x7ae82322b5,
      0x7b9c40b847,
      0x7c506234aa,
      0x7d048797f4,
      0x7db8b0e239,
      0x7e6cde138f,
      0x7f210f2c0d,
      0x7fd5442bc8,
      0x80897d12d5,
      0x813db9e14b,
      0x81f1fa973e,
      0x82a63f34c5,
      0x835a87b9f4,
      0x840ed426e3,
      0x84c3247ba6,
      0x857778b852,
      0x862bd0dcff,
      0x86e02ce9c1,
      0x87948cdeae,
      0x8848f0bbdc,
      0x88fd588160,
      0x89b1c42f50,
      0x8a6633c5c2,
      0x8b1aa744cb,
      0x8bcf1eac81,
      0x8c8399fcfa,
      0x8d3819364c,
      0x8dec9c588c,
      0x8ea12363cf,
      0x8f55ae582c,
      0x900a3d35b8,
      0x90becffc88,
      0x917366acb4,
      0x922801464f,
      0x92dc9fc971,
      0x939142362e,
      0x9445e88c9c,
      0x94fa92ccd1,
      0x95af40f6e3,
      0x9663f30ae8,
      0x9718a908f4,
      0x97cd62f11f,
      0x988220c37d,
      0x9936e28024,
      0x99eba8272a,
      0x9aa071b8a4,
      0x9b553f34a9,
      0x9c0a109b4e,
      0x9cbee5eca9,
      0x9d73bf28d0,
      0x9e289c4fd8,
      0x9edd7d61d6,
      0x9f92625ee2,
      0xa0474b470f,
      0xa0fc381a75,
      0xa1b128d929,
      0xa2661d8340,
      0xa31b1618d1,
      0xa3d01299f1,
      0xa4851306b6,
      0xa53a175f35,
      0xa5ef1fa385,
      0xa6a42bd3ba,
      0xa7593befec,
      0xa80e4ff82f,
      0xa8c367ec99,
      0xa97883cd40,
      0xaa2da39a3b,
      0xaae2c7539d,
      0xab97eef97e,
      0xac4d1a8bf3,
      0xad024a0b12,
      0xadb77d76f1,
      0xae6cb4cfa5,
      0xaf21f01544,
      0xafd72f47e4,
      0xb08c72679a,
      0xb141b9747d,
      0xb1f7046ea3,
      0xb2ac535620,
      0xb361a62b0b,
      0xb416fced79,
      0xb4cc579d81,
      0xb581b63b38,
      0xb63718c6b4,
      0xb6ec7f400b,
      0xb7a1e9a752,
      0xb85757fc9f,
      0xb90cca4009,
      0xb9c24071a4,
      0xba77ba9187,
      0xbb2d389fc7,
      0xbbe2ba9c7b,
      0xbc984087b7,
      0xbd4dca6193,
      0xbe03582a23,
      0xbeb8e9e17e,
      0xbf6e7f87b8,
      0xc024191ce9,
      0xc0d9b6a126,
      0xc18f581485,
      0xc244fd771b,
      0xc2faa6c8fe,
      0xc3b0540a45,
      0xc466053b04,
      0xc51bba5b53,
      0xc5d1736b45,
      0xc687306af3,
      0xc73cf15a71,
      0xc7f2b639d4,
      0xc8a87f0934,
      0xc95e4bc8a6,
      0xca141c783f,
      0xcac9f11816,
      0xcb7fc9a840,
      0xcc35a628d3,
      0xcceb8699e5,
      0xcda16afb8c,
      0xce57534ddd,
      0xcf0d3f90f0,
      0xcfc32fc4d8,
      0xd07923e9ad,
      0xd12f1bff84,
      0xd1e5180673,
      0xd29b17fe8f,
      0xd3511be7ef,
      0xd40723c2a9,
      0xd4bd2f8ed3,
      0xd5733f4c81,
      0xd62952fbcb,
      0xd6df6a9cc5,
      0xd795862f87,
      0xd84ba5b425,
      0xd901c92ab6,
      0xd9b7f0934f,
      0xda6e1bee07,
      0xdb244b3af3,
      0xdbda7e7a29,
      0xdc90b5abbf,
      0xdd46f0cfcb,
      0xddfd2fe663,
      0xdeb372ef9c,
      0xdf69b9eb8d,
      0xe02004da4c,
      0xe0d653bbee,
      0xe18ca69089,
      0xe242fd5833,
      0xe2f9581303,
      0xe3afb6c10d,
      0xe466196268,
      0xe51c7ff72b,
      0xe5d2ea7f69,
      0xe68958fb3b,
      0xe73fcb6ab5,
      0xe7f641cded,
      0xe8acbc24f9,
      0xe9633a6ff0,
      0xea19bcaee8,
      0xead042e1f5,
      0xeb86cd092e,
      0xec3d5b24a9,
      0xecf3ed347c,
      0xedaa8338bd,
      0xee611d3182,
      0xef17bb1ee0,
      0xefce5d00ee,
      0xf08502d7c1,
      0xf13baca370,
      0xf1f25a6411,
      0xf2a90c19b8,
      0xf35fc1c47e,
      0xf4167b6476,
      0xf4cd38f9b7,
      0xf583fa8458,
      0xf63ac0046e,
      0xf6f1897a0f,
      0xf7a856e550,
      0xf85f284649,
      0xf915fd9d0f,
      0xf9ccd6e9b8,
      0xfa83b42c59,
      0xfb3a956509,
      0xfbf17a93de,
      0xfca863b8ee,
      0xfd5f50d44f,
      0xfe1641e616,
      0xfecd36ee5a,
      0xff842fed31,
      0x1003b2ce2b0,
      0x100f22dceee,
      0x101a932b200,
      0x102603b8bfd,
      0x10317485cfa,
      0x103ce59250e,
      0x104856de44f,
      0x1053c869ad2,
      0x105f3a348ae,
      0x106aac3edf8,
      0x10761e88ac6,
      0x10819111f2f,
      0x108d03dab49,
      0x109876e2f29,
      0x10a3ea2aae5,
      0x10af5db1e94,
      0x10bad178a4c,
      0x10c6457ee22,
      0x10d1b9c4a2d,
      0x10dd2e49e82,
      0x10e8a30eb38,
      0x10f41813064,
      0x10ff8d56e1d,
      0x110b02da478,
      0x1116789d38c,
      0x1121ee9fb6f,
      0x112d64e1c37,
      0x1138db635f9,
      0x114452248cc,
      0x114fc9254c6,
      0x115b40659fc,
      0x1166b7e5886,
      0x11722fa5078,
      0x117da7a41e9,
      0x11891fe2cef,
      0x119498611a0,
      0x11a0111f012,
      0x11ab8a1c85c,
      0x11b70359a92,
      0x11c27cd66cc,
      0x11cdf692d1f,
      0x11d9708eda1,
      0x11e4eaca868,
      0x11f06545d8b,
      0x11fbe000d20,
      0x12075afb73c,
      0x1212d635bf5,
      0x121e51afb62,
      0x1229cd69599,
      0x12354962ab0,
      0x1240c59babd,
      0x124c42145d5,
      0x1257beccc10,
      0x12633bc4d82,
      0x126eb8fca43,
      0x127a3674268,
      0x1285b42b608,
      0x12913222537,
      0x129cb05900e,
      0x12a82ecf6a1,
      0x12b3ad85906,
      0x12bf2c7b754,
      0x12caabb11a2,
      0x12d62b26804,
      0x12e1aadba91,
      0x12ed2ad095f,
      0x12f8ab05485,
      0x13042b79c18,
      0x130fac2e02e,
      0x131b2d220de,
      0x1326ae55e3e,
      0x13322fc9863,
      0x133db17cf64,
      0x13493370358,
      0x1354b5a3453,
      0x1360381626d,
      0x136bbac8dbb,
      0x13773dbb653,
      0x1382c0edc4d,
      0x138e445ffbc,
      0x1399c8120b9,
      0x13a54c03f59,
      0x13b0d035bb2,
      0x13bc54a75da,
      0x13c7d958de7,
      0x13d35e4a3f0,
      0x13dee37b80b,
      0x13ea68eca4d,
      0x13f5ee9dacd,
      0x1401748e9a2,
      0x140cfabf6e0,
      0x1418813029f,
      0x142407e0cf5,
      0x142f8ed15f7,
      0x143b1601dbc,
      0x14469d7245a,
      0x145225229e7,
      0x145dad12e7a,
      0x14693543227,
      0x1474bdb3507,
      0x1480466372e,
      0x148bcf538b3,
      0x149758839ad,
      0x14a2e1f3a30,
      0x14ae6ba3a54,
      0x14b9f593a2f,
      0x14c57fc39d7,
      0x14d10a33961,
      0x14dc94e38e5,
      0x14e81fd3878,
      0x14f3ab03830,
      0x14ff3673825,
      0x150ac22386b,
      0x15164e13919,
      0x1521da43a46,
      0x152d66b3c07,
      0x1538f363e72,
      0x1544805419f,
      0x15500d845a2,
      0x155b9af4a93,
      0x156728a5087,
      0x1572b695795,
      0x157e44c5fd3,
      0x1589d336957,
      0x159561e7437,
      0x15a0f0d808a,
      0x15ac8008e65,
      0x15b80f79de0,
      0x15c39f2af0f,
      0x15cf2f1c20a,
      0x15dabf4d6e6,
      0x15e64fbedba,
      0x15f1e07069d,
      0x15fd71621a3,
      0x16090293ee4,
      0x16149405e76,
      0x162025b806e,
      0x162bb7aa4e4,
      0x163749dcbed,
      0x1642dc4f5a0,
      0x164e6f02213,
      0x165a01f515c,
      0x16659528391,
      0x1671289b8c9,
      0x167cbc4f11a,
      0x16885042c99,
      0x1693e476b5f,
      0x169f78ead7f,
      0x16ab0d9f312,
      0x16b6a293c2d,
      0x16c237c88e6,
      0x16cdcd3d954,
      0x16d962f2d8d,
      0x16e4f8e85a7,
      0x16f08f1e1b8,
      0x16fc25941d7,
      0x1707bc4a61a,
      0x17135340e97,
      0x171eea77b65,
      0x172a81eec9a,
      0x173619a624b,
      0x1741b19dc91,
      0x174d49d5b7f,
      0x1758e24df2e,
      0x17647b067b2,
      0x177013ff524,
      0x177bad38798,
      0x178746b1f25,
      0x1792e06bbe1,
      0x179e7a65de3,
      0x17aa14a0541,
      0x17b5af1b212,
      0x17c149d646a,
      0x17cce4d1c62,
      0x17d8800da0f,
      0x17e41b89d87,
      0x17efb7466e1,
      0x17fb5343633,
      0x1806ef80b94,
      0x18128bfe719,
      0x181e28bc8d9,
      0x1829c5bb0eb,
      0x183562f9f64,
      0x1841007945b,
      0x184c9e38fe6,
      0x18583c3921c,
      0x1863da79b13,
      0x186f78faae1,
      0x187b17bc19d,
      0x1886b6bdf5d,
      0x18925600437,
      0x189df583041,
      0x18a99546393,
      0x18b53549e41,
      0x18c0d58e064,
      0x18cc7612a10,
      0x18d816d7b5d,
      0x18e3b7dd460,
      0x18ef5923530,
      0x18fafaa9de4,
      0x19069c70e91,
      0x19123e7874e,
      0x191de0c0832,
      0x19298349153,
      0x193526122c6,
      0x1940c91bca3,
      0x194c6c65f00,
      0x19580ff09f4,
      0x1963b3bbd93,
      0x196f57c79f6,
      0x197afc13f32,
      0x1986a0a0d5e,
      0x1992456e490,
      0x199dea7c4de,
      0x19a98fcae5f,
      0x19b5355a12a,
      0x19c0db29d54,
      0x19cc813a2f4,
      0x19d8278b220,
      0x19e3ce1caef,
      0x19ef74eed77,
      0x19fb1c019cf,
      0x1a06c35500d,
      0x1a126ae9047,
      0x1a1e12bda93,
      0x1a29bad2f09,
      0x1a356328dbe,
      0x1a410bbf6ca,
      0x1a4cb496a41,
      0x1a585dae83c,
      0x1a6407070cf,
      0x1a6fb0a0412,
      0x1a7b5a7a21c,
      0x1a870494b01,
      0x1a92aeefed9,
      0x1a9e598bdbb,
      0x1aaa04687bc,
      0x1ab5af85cf3,
      0x1ac15ae3d77,
      0x1acd068295e,
      0x1ad8b2620be,
      0x1ae45e823ad,
      0x1af00ae3243,
      0x1afbb784c96,
      0x1b0764672bc,
      0x1b13118a4cb,
      0x1b1ebeee2da,
      0x1b2a6c92d00,
      0x1b361a78352,
      0x1b41c89e5e8,
      0x1b4d77054d8,
      0x1b5925ad037,
      0x1b64d49581e,
      0x1b7083beca1,
      0x1b7c3328dd8,
      0x1b87e2d3bd9,
      0x1b9392bf6ba,
      0x1b9f42ebe93,
      0x1baaf359379,
      0x1bb6a407582,
      0x1bc254f64c6,
      0x1bce062615b,
      0x1bd9b796b57,
      0x1be569482d0,
      0x1bf11b3a7de,
      0x1bfccd6da97,
      0x1c087fe1b10,
      0x1c143296962,
      0x1c1fe58c5a1,
      0x1c2b98c2fe5,
      0x1c374c3a844,
      0x1c42fff2ed5,
      0x1c4eb3ec3ae,
      0x1c5a68266e5,
      0x1c661ca1891,
      0x1c71d15d8c9,
      0x1c7d865a7a3,
      0x1c893b98536,
      0x1c94f117197,
      0x1ca0a6d6cde,
      0x1cac5cd7721,
      0x1cb81319077,
      0x1cc3c99b8f6,
      0x1ccf805f0b4,
      0x1cdb37637c8,
      0x1ce6eea8e49,
      0x1cf2a62f44d,
      0x1cfe5df69eb,
      0x1d0a15fef39,
      0x1d15ce4844d,
      0x1d2186d293f,
      0x1d2d3f9de24,
      0x1d38f8aa314,
      0x1d44b1f7824,
      0x1d506b85d6c,
      0x1d5c2555301,
      0x1d67df658fb,
      0x1d7399b6f70,
      0x1d7f5449676,
      0x1d8b0f1ce24,
      0x1d96ca31691,
      0x1da28586fd3,
      0x1dae411da00,
      0x1db9fcf5530,
      0x1dc5b90e178,
      0x1dd17567ef0,
      0x1ddd3202dad,
      0x1de8eededc7,
      0x1df4abfbf54,
      0x1e00695a26a,
      0x1e0c26f9720,
      0x1e17e4d9d8d,
      0x1e23a2fb5c7,
      0x1e2f615dfe5,
      0x1e3b2001bfd,
      0x1e46dee6a26,
      0x1e529e0ca77,
      0x1e5e5d73d05,
      0x1e6a1d1c1e8,
      0x1e75dd05937,
      0x1e819d30307,
      0x1e8d5d9bf6f,
      0x1e991e48e87,
      0x1ea4df37064,
      0x1eb0a06651d,
      0x1ebc61d6cc9,
      0x1ec8238877e,
      0x1ed3e57b553,
      0x1edfa7af65e,
      0x1eeb6a24ab7,
      0x1ef72cdb273,
      0x1f02efd2daa,
      0x1f0eb30bc71,
      0x1f1a7685ee0,
      0x1f263a4150d,
      0x1f31fe3df0f,
      0x1f3dc27bcfc,
      0x1f4986faeeb,
      0x1f554bbb4f3,
      0x1f6110bcf2a,
      0x1f6cd5ffda6,
      0x1f789b8407f,
      0x1f8461497cb,
      0x1f9027503a1,
      0x1f9bed98417,
      0x1fa7b421944,
      0x1fb37aec33e,
      0x1fbf41f821d,
      0x1fcb09455f6,
      0x1fd6d0d3ee1,
      0x1fe298a3cf4,
      0x1fee60b5046,
      0x1ffa29078ed,
      0x2005f19b700,
      0x2011ba70a96,
      0x201d83873c5,
      0x20294cdf2a4,
      0x20351678749,
      0x2040e0531cc,
      0x204caa6f243,
      0x205874cc8c4,
      0x20643f6b567,
      0x20700a4b841,
      0x207bd56d16a,
      0x2087a0d00f8,
      0x20936c74702,
      0x209f385a39f,
      0x20ab04816e5,
      0x20b6d0ea0eb,
      0x20c29d941c7,
      0x20ce6a7f991,
      0x20da37ac85e,
      0x20e6051ae46,
      0x20f1d2cab60,
      0x20fda0bbfc1,
      0x21096eeeb81,
      0x21153d62eb7,
      0x21210c18978,
      0x212cdb0fbdc,
      0x2138aa485fa,
      0x214479c27e8,
      0x2150497e1bc,
      0x215c197b38e,
      0x2167e9b9d75,
      0x2173ba39f86,
      0x217f8afb9d9,
      0x218b5bfec84,
      0x21972d4379e,
      0x21a2fec9b3e,
      0x21aed09177a,
      0x21baa29ac6a,
      0x21c674e5a23,
      0x21d247720bd,
      0x21de1a4004f,
      0x21e9ed4f8ee,
      0x21f5c0a0ab3,
      0x220194335b2,
      0x220d6807a04,
      0x22193c1d7bf,
      0x22251074efa,
      0x2230e50dfcb,
      0x223cb9e8a49,
      0x22488f04e8c,
      0x22546462ca8,
      0x22603a024b7,
      0x226c0fe36cd,
      0x2277e606302,
      0x2283bc6a96d,
      0x228f9310a24,
      0x229b69f853f,
      0x22a74121ad3,
      0x22b3188caf8,
      0x22bef0395c4,
      0x22cac827b4e,
      0x22d6a057bae,
      0x22e278c96f9,
      0x22ee517cd46,
      0x22fa2a71ead,
      0x230603a8b43,
      0x2311dd21321,
      0x231db6db65c,
      0x232990d750b,
      0x23356b14f45,
      0x23414594521,
      0x234d20556b6,
      0x2358fb5841a,
      0x2364d69cd64,
      0x2370b2232ab,
      0x237c8deb406,
      0x238869f518c,
      0x23944640b53,
      0x23a022ce172,
      0x23abff9d400,
      0x23b7dcae314,
      0x23c3ba00ec4,
      0x23cf9795728,
      0x23db756bc55,
      0x23e75383e64,
      0x23f331ddd6b,
      0x23ff1079980,
      0x240aef572ba,
      0x2416ce76931,
      0x2422add7cfa,
      0x242e8d7ae2e,
      0x243a6d5fce2,
      0x24464d8692d,
      0x24522def327,
      0x245e0e99ae6,
      0x2469ef86080,
      0x2475d0b440e,
      0x2481b2245a5,
      0x248d93d655d,
      0x249975ca34c,
      0x24a557fff88,
      0x24b13a77a2a,
      0x24bd1d31348,
      0x24c9002caf8,
      0x24d4e36a152,
      0x24e0c6e966c,
      0x24ecaaaaa5d,
      0x24f88eadd3c,
      0x250472f2f20,
      0x2510577a020,
      0x251c3c43052,
      0x2528214dfce,
      0x2534069aeaa,
      0x253fec29cfd,
      0x254bd1faade,
      0x2557b80d864,
      0x25639e625a5,
      0x256f84f92ba,
      0x257b6bd1fb7,
      0x258752eccb5,
      0x25933a499cb,
      0x259f21e870e,
      0x25ab09c9496,
      0x25b6f1ec27b,
      0x25c2da510d2,
      0x25cec2f7fb2,
      0x25daabe0f33,
      0x25e6950bf6c,
      0x25f27e79073,
      0x25fe682825f,
      0x260a5219547,
      0x26163c4c942,
      0x262226c1e67,
      0x262e11794cc,
      0x2639fc72c8a,
      0x2645e7ae5b6,
      0x2651d32c067,
      0x265dbeebcb5,
      0x2669aaedab6,
      0x26759731a81,
      0x268183b7c2e,
      0x268d707ffd2,
      0x26995d8a586,
      0x26a54ad6d5f,
      0x26b13865775,
      0x26bd26363df,
      0x26c914492b3,
      0x26d5029e409,
      0x26e0f1357f8,
      0x26ece00ee96,
      0x26f8cf2a7fa,
      0x2704be8843c,
      0x2710ae28372,
      0x271c9e0a5b3,
      0x27288e2eb16,
      0x27347e953b2,
      0x27406f3df9f,
      0x274c6028ef2,
      0x275851561c3,
      0x276442c5829,
      0x2770347723b,
      0x277c266b00f,
      0x278818a11bd,
      0x27940b1975c,
      0x279ffdd4103,
      0x27abf0d0ec8,
      0x27b7e4100c3,
      0x27c3d79170a,
      0x27cfcb551b5,
      0x27dbbf5b0da,
      0x27e7b3a3490,
      0x27f3a82dcef,
      0x27ff9cfaa0e,
      0x280b9209c03,
      0x2817875b2e5,
      0x28237ceeecb,
      0x282f72c4fcd,
      0x283b68dd601,
      0x28475f3817e,
      0x285355d525c,
      0x285f4cb48b1,
      0x286b43d6494,
      0x28773b3a61c,
      0x288332e0d61,
      0x288f2ac9a78,
      0x289b22f4d7b,
      0x28a71b6267e,
      0x28b31412599,
      0x28bf0d04ae4,
      0x28cb0639675,
      0x28d6ffb0864,
      0x28e2f96a0c6,
      0x28eef365fb4,
      0x28faeda4545,
      0x2906e82518e,
      0x2912e2e84a8,
      0x291eddedeaa,
      0x292ad935faa,
      0x2936d4c07bf,
      0x2942d08d701,
      0x294ecc9cd86,
      0x295ac8eeb66,
      0x2966c5830b7,
      0x2972c259d91,
      0x297ebf7320a,
      0x298abccee3a,
      0x2996ba6d238,
      0x29a2b84de1b,
      0x29aeb6711fa,
      0x29bab4d6deb,
      0x29c6b37f206,
      0x29d2b269e62,
      0x29deb197317,
      0x29eab10703a,
      0x29f6b0b95e3,
      0x2a02b0ae42a,
      0x2a0eb0e5b25,
      0x2a1ab15faeb,
      0x2a26b21c393,
      0x2a32b31b535,
      0x2a3eb45cfe8,
      0x2a4ab5e13c2,
      0x2a56b7a80da,
      0x2a62b9b1749,
      0x2a6ebbfd724,
      0x2a7abe8c083,
      0x2a86c15d37d,
      0x2a92c471029,
      0x2a9ec7c769e,
      0x2aaacb606f4,
      0x2ab6cf3c141,
      0x2ac2d35a59c,
      0x2aced7bb41d,
      0x2adadc5ecda,
      0x2ae6e144feb,
      0x2af2e66dd66,
      0x2afeebd9564,
      0x2b0af1877fb,
      0x2b16f778541,
      0x2b22fdabd4f,
      0x2b2f042203b,
      0x2b3b0adae1d,
      0x2b4711d670b,
      0x2b531914b1d,
      0x2b5f2095a69,
      0x2b6b2859507,
      0x2b77305fb0f,
      0x2b8338a8c96,
      0x2b8f41349b5,
      0x2b9b4a03282,
      0x2ba75314715,
      0x2bb35c68784,
      0x2bbf65ff3e7,
      0x2bcb6fd8c55,
      0x2bd779f50e5,
      0x2be384541ae,
      0x2bef8ef5ec7,
      0x2bfb99da848,
      0x2c07a501e47,
      0x2c13b06c0dc,
      0x2c1fbc1901e,
      0x2c2bc808c24,
      0x2c37d43b505,
      0x2c43e0b0ad9,
      0x2c4fed68db6,
      0x2c5bfa63db3,
      0x2c6807a1ae9,
      0x2c74152256d,
      0x2c8022e5d58,
      0x2c8c30ec2c0,
      0x2c983f355bc,
      0x2ca44dc1664,
      0x2cb05c904cf,
      0x2cbc6ba2113,
      0x2cc87af6b49,
      0x2cd48a8e388,
      0x2ce09a689e5,
      0x2cecaa85e7a,
      0x2cf8bae615c,
      0x2d04cb892a3,
      0x2d10dc6f267,
      0x2d1ced980be,
      0x2d28ff03dbf,
      0x2d3510b2983,
      0x2d4122a441f,
      0x2d4d34d8dac,
      0x2d594750640,
      0x2d655a0adf2,
      0x2d716d084db,
      0x2d7d8048b10,
      0x2d8993cc0aa,
      0x2d95a7925bf,
      0x2da1bb9ba67,
      0x2dadcfe7eb8,
      0x2db9e4772cb,
      0x2dc5f9496b6,
      0x2dd20e5ea90,
      0x2dde23b6e71,
      0x2dea3952270,
      0x2df64f306a4,
      0x2e026551b25,
      0x2e0e7bb6009,
      0x2e1a925d568,
      0x2e26a947b59,
      0x2e32c0751f4,
      0x2e3ed7e594e,
      0x2e4aef99181,
    };
    setRom< HWOffsetFix<42,-45,UNSIGNED> > (data, id477sta_rom_store, 42, 1024); 
  }
  { // Node ID: 474 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2e57078faa3,
      0x60dfc14636e,
      0x97fb5aa6c54,
      0xd413cccfe78,
      0x1159ca845542,
      0x15d13f32b5a7,
      0x1ab031b9f749,
    };
    setRom< HWOffsetFix<45,-45,UNSIGNED> > (data, id474sta_rom_store, 45, 8); 
  }
  { // Node ID: 318 (NodeConstantRawBits)
    id318out_value = (c_hw_fix_21_n21_uns_bits);
  }
  { // Node ID: 321 (NodeConstantRawBits)
    id321out_value = (c_hw_fix_25_n48_uns_bits);
  }
  { // Node ID: 326 (NodeConstantRawBits)
    id326out_value = (c_hw_fix_64_n87_uns_bits);
  }
  { // Node ID: 330 (NodeConstantRawBits)
    id330out_value = (c_hw_fix_64_n63_uns_bits);
  }
  { // Node ID: 334 (NodeConstantRawBits)
    id334out_value = (c_hw_fix_49_n48_uns_bits);
  }
  { // Node ID: 339 (NodeConstantRawBits)
    id339out_value = (c_hw_fix_64_n66_uns_bits);
  }
  { // Node ID: 343 (NodeConstantRawBits)
    id343out_value = (c_hw_fix_64_n62_uns_bits);
  }
  { // Node ID: 347 (NodeConstantRawBits)
    id347out_value = (c_hw_fix_50_n48_uns_bits);
  }
  { // Node ID: 352 (NodeConstantRawBits)
    id352out_value = (c_hw_fix_64_n75_uns_bits);
  }
  { // Node ID: 356 (NodeConstantRawBits)
    id356out_value = (c_hw_fix_64_n61_uns_bits);
  }
  { // Node ID: 360 (NodeConstantRawBits)
    id360out_value = (c_hw_fix_51_n48_uns_bits);
  }
  { // Node ID: 364 (NodeConstantRawBits)
    id364out_value = (c_hw_fix_45_n44_uns_bits);
  }
  { // Node ID: 906 (NodeConstantRawBits)
    id906out_value = (c_hw_fix_45_n44_uns_bits_1);
  }
  { // Node ID: 402 (NodeConstantRawBits)
    id402out_value = (c_hw_fix_14_0_sgn_bits_1);
  }
  { // Node ID: 905 (NodeConstantRawBits)
    id905out_value = (c_hw_flt_11_45_bits_4);
  }
  { // Node ID: 904 (NodeConstantRawBits)
    id904out_value = (c_hw_flt_11_45_bits_4);
  }
  { // Node ID: 903 (NodeConstantRawBits)
    id903out_value = (c_hw_fix_14_0_sgn_bits_3);
  }
  { // Node ID: 444 (NodeConstantRawBits)
    id444out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 428 (NodeConstantRawBits)
    id428out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 368 (NodeConstantRawBits)
    id368out_value = (c_hw_fix_45_n44_uns_bits_2);
  }
  { // Node ID: 372 (NodeConstantRawBits)
    id372out_value = (c_hw_fix_44_n44_uns_bits);
  }
  { // Node ID: 453 (NodeConstantRawBits)
    id453out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 454 (NodeConstantRawBits)
    id454out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 456 (NodeConstantRawBits)
    id456out_value = (c_hw_fix_44_0_uns_bits);
  }
  { // Node ID: 539 (NodeConstantRawBits)
    id539out_value = (c_hw_flt_11_45_bits_4);
  }
  { // Node ID: 902 (NodeConstantRawBits)
    id902out_value = (c_hw_flt_11_45_bits_6);
  }
  { // Node ID: 901 (NodeConstantRawBits)
    id901out_value = (c_hw_flt_11_45_bits_2);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_11_45_bits_7);
  }
  { // Node ID: 22 (NodeConstantRawBits)
    id22out_value = (c_hw_flt_11_45_bits_8);
  }
  { // Node ID: 4 (NodeConstantRawBits)
    id4out_value = (c_hw_flt_11_45_bits_9);
  }
  { // Node ID: 41 (NodeConstantRawBits)
    id41out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 861 (NodeOutputMappedReg)
    registerMappedRegister("all_numeric_exceptions", Data(37), false);
  }
  { // Node ID: 836 (NodeConstantRawBits)
    id836out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 837 (NodeOutput)
    m_internal_watch_carriedu_output = registerOutput("internal_watch_carriedu_output",2 );
  }
  { // Node ID: 900 (NodeConstantRawBits)
    id900out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 516 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 519 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 899 (NodeConstantRawBits)
    id899out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 523 (NodeInputMappedReg)
    registerMappedRegister("io_v_out_force_disabled", Data(1));
  }
  { // Node ID: 526 (NodeOutput)
    m_v_out = registerOutput("v_out",1 );
  }
  { // Node ID: 531 (NodeConstantRawBits)
    id531out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 898 (NodeConstantRawBits)
    id898out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 528 (NodeConstantRawBits)
    id528out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 532 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 897 (NodeConstantRawBits)
    id897out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 534 (NodeConstantRawBits)
    id534out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 537 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void KarmaDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void KarmaDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 17 (NodeCounter)

    (id17st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    (id16st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 883 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id883out_output[i] = (c_hw_flt_11_45_undef);
    }
  }
  { // Node ID: 895 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id895out_output[i] = (c_hw_flt_11_45_undef);
    }
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 869 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id869out_output[i] = (c_hw_flt_11_45_undef);
    }
  }
  { // Node ID: 882 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id882out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 874 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id874out_output[i] = (c_hw_fix_13_0_sgn_undef);
    }
  }
  { // Node ID: 875 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id875out_output[i] = (c_hw_fix_25_n48_uns_undef);
    }
  }
  { // Node ID: 876 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id876out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 877 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id877out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 878 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id878out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 879 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id879out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 839 (NodeExceptionMask)

    (id839st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 887 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id887out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 840 (NodeExceptionMask)

    (id840st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 888 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id888out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 841 (NodeExceptionMask)

    (id841st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 889 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id889out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 843 (NodeExceptionMask)

    (id843st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 844 (NodeExceptionMask)

    (id844st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 845 (NodeExceptionMask)

    (id845st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 846 (NodeExceptionMask)

    (id846st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 847 (NodeExceptionMask)

    (id847st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 848 (NodeExceptionMask)

    (id848st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 849 (NodeExceptionMask)

    (id849st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 850 (NodeExceptionMask)

    (id850st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 851 (NodeExceptionMask)

    (id851st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 852 (NodeExceptionMask)

    (id852st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 853 (NodeExceptionMask)

    (id853st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 856 (NodeExceptionMask)

    (id856st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 842 (NodeExceptionMask)

    (id842st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 855 (NodeExceptionMask)

    (id855st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 857 (NodeExceptionMask)

    (id857st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 858 (NodeExceptionMask)

    (id858st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 859 (NodeExceptionMask)

    (id859st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 854 (NodeExceptionMask)

    (id854st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 838 (NodeExceptionMask)

    (id838st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 890 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id890out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 891 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id891out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 516 (NodeInputMappedReg)
    id516out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 892 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id892out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 523 (NodeInputMappedReg)
    id523out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 894 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id894out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 529 (NodeCounter)

    (id529st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 535 (NodeCounter)

    (id535st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 537 (NodeInputMappedReg)
    id537out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::updateState() {
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 516 (NodeInputMappedReg)
    id516out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 523 (NodeInputMappedReg)
    id523out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 537 (NodeInputMappedReg)
    id537out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::preExecute() {
}

void KarmaDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "KarmaDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int KarmaDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
