#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaDFEKernel::execute0() {
  { // Node ID: 862 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 896 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id896out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 914 (NodeConstantRawBits)
  }
  { // Node ID: 540 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id540in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id540in_b = id914out_value;

    id540out_result[(getCycle()+1)%2] = (eq_fixed(id540in_a,id540in_b));
  }
  { // Node ID: 883 (NodeFIFO)
    const HWFloat<11,45> &id883in_input = id23out_result[getCycle()%2];

    id883out_output[(getCycle()+9)%10] = id883in_input;
  }
  { // Node ID: 895 (NodeFIFO)
    const HWFloat<11,45> &id895in_input = id883out_output[getCycle()%10];

    id895out_output[(getCycle()+1)%2] = id895in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 19 (NodeCast)
    const HWFloat<11,53> &id19in_i = id18out_dt;

    id19out_o[(getCycle()+3)%4] = (cast_float2float<11,45>(id19in_i));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 913 (NodeConstantRawBits)
  }
  { // Node ID: 541 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id541in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id541in_b = id913out_value;

    id541out_result[(getCycle()+1)%2] = (eq_fixed(id541in_a,id541in_b));
  }
  { // Node ID: 912 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id498out_result;

  { // Node ID: 498 (NodeGt)
    const HWFloat<11,45> &id498in_a = id895out_output[getCycle()%2];
    const HWFloat<11,45> &id498in_b = id5out_value;

    id498out_result = (gt_float(id498in_a,id498in_b));
  }
  HWFloat<11,45> id499out_o;

  { // Node ID: 499 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id499in_i = id498out_result;

    id499out_o = (cast_fixed2float<11,45>(id499in_i));
  }
  HWFloat<11,45> id501out_result;

  { // Node ID: 501 (NodeMul)
    const HWFloat<11,45> &id501in_a = id912out_value;
    const HWFloat<11,45> &id501in_b = id499out_o;

    id501out_result = (mul_float(id501in_a,id501in_b));
  }
  HWFloat<11,45> id502out_result;

  { // Node ID: 502 (NodeSub)
    const HWFloat<11,45> &id502in_a = id501out_result;
    const HWFloat<11,45> &id502in_b = id869out_output[getCycle()%11];

    id502out_result = (sub_float(id502in_a,id502in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<11,45> id504out_result;

  { // Node ID: 504 (NodeMul)
    const HWFloat<11,45> &id504in_a = id502out_result;
    const HWFloat<11,45> &id504in_b = id3out_value;

    id504out_result = (mul_float(id504in_a,id504in_b));
  }
  HWFloat<11,45> id507out_result;

  { // Node ID: 507 (NodeMul)
    const HWFloat<11,45> &id507in_a = id19out_o[getCycle()%4];
    const HWFloat<11,45> &id507in_b = id504out_result;

    id507out_result = (mul_float(id507in_a,id507in_b));
  }
  HWFloat<11,45> id508out_result;

  { // Node ID: 508 (NodeAdd)
    const HWFloat<11,45> &id508in_a = id869out_output[getCycle()%11];
    const HWFloat<11,45> &id508in_b = id507out_result;

    id508out_result = (add_float(id508in_a,id508in_b));
  }
  HWFloat<11,45> id866out_output;

  { // Node ID: 866 (NodeStreamOffset)
    const HWFloat<11,45> &id866in_input = id508out_result;

    id866out_output = id866in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id541out_result[getCycle()%2];
    const HWFloat<11,45> &id27in_option0 = id866out_output;
    const HWFloat<11,45> &id27in_option1 = id26out_value;

    HWFloat<11,45> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 869 (NodeFIFO)
    const HWFloat<11,45> &id869in_input = id27out_result[getCycle()%2];

    id869out_output[(getCycle()+10)%11] = id869in_input;
  }
  HWFloat<11,45> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<11,45> &id32in_a = id869out_output[getCycle()%11];
    const HWFloat<11,45> &id32in_b = id869out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<11,45> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<11,45> &id33in_a = id869out_output[getCycle()%11];
    const HWFloat<11,45> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<11,45> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<11,45> &id34in_a = id33out_result;
    const HWFloat<11,45> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<11,45> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<11,45> &id35in_a = id34out_result;
    const HWFloat<11,45> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<11,45> id495out_result;

  { // Node ID: 495 (NodeSub)
    const HWFloat<11,45> &id495in_a = id6out_value;
    const HWFloat<11,45> &id495in_b = id35out_result;

    id495out_result = (sub_float(id495in_a,id495in_b));
  }
  { // Node ID: 911 (NodeConstantRawBits)
  }
  { // Node ID: 910 (NodeConstantRawBits)
  }
  { // Node ID: 909 (NodeConstantRawBits)
  }
  HWRawBits<11> id464out_result;

  { // Node ID: 464 (NodeSlice)
    const HWFloat<11,45> &id464in_a = id834out_floatOut[getCycle()%2];

    id464out_result = (slice<44,11>(id464in_a));
  }
  { // Node ID: 465 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id542out_result;

  { // Node ID: 542 (NodeEqInlined)
    const HWRawBits<11> &id542in_a = id464out_result;
    const HWRawBits<11> &id542in_b = id465out_value;

    id542out_result = (eq_bits(id542in_a,id542in_b));
  }
  HWRawBits<44> id463out_result;

  { // Node ID: 463 (NodeSlice)
    const HWFloat<11,45> &id463in_a = id834out_floatOut[getCycle()%2];

    id463out_result = (slice<0,44>(id463in_a));
  }
  { // Node ID: 908 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id543out_result;

  { // Node ID: 543 (NodeNeqInlined)
    const HWRawBits<44> &id543in_a = id463out_result;
    const HWRawBits<44> &id543in_b = id908out_value;

    id543out_result = (neq_bits(id543in_a,id543in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id469out_result;

  { // Node ID: 469 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id469in_a = id542out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id469in_b = id543out_result;

    HWOffsetFix<1,0,UNSIGNED> id469x_1;

    (id469x_1) = (and_fixed(id469in_a,id469in_b));
    id469out_result = (id469x_1);
  }
  { // Node ID: 882 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id882in_input = id469out_result;

    id882out_output[(getCycle()+8)%9] = id882in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id544out_result;
  HWOffsetFix<1,0,UNSIGNED> id544out_result_doubt;

  { // Node ID: 544 (NodeSlice)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id544in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id544in_a_doubt = id43out_o_doubt[getCycle()%7];

    id544out_result = (slice<60,1>(id544in_a));
    id544out_result_doubt = id544in_a_doubt;
  }
  HWRawBits<1> id545out_result;
  HWOffsetFix<1,0,UNSIGNED> id545out_result_doubt;

  { // Node ID: 545 (NodeNot)
    const HWRawBits<1> &id545in_a = id544out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id545in_a_doubt = id544out_result_doubt;

    id545out_result = (not_bits(id545in_a));
    id545out_result_doubt = id545in_a_doubt;
  }
  HWRawBits<61> id605out_result;
  HWOffsetFix<1,0,UNSIGNED> id605out_result_doubt;

  { // Node ID: 605 (NodeCat)
    const HWRawBits<1> &id605in_in0 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in0_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in1 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in1_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in2 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in2_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in3 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in3_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in4 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in4_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in5 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in5_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in6 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in6_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in7 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in7_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in8 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in8_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in9 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in9_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in10 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in10_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in11 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in11_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in12 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in12_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in13 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in13_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in14 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in14_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in15 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in15_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in16 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in16_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in17 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in17_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in18 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in18_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in19 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in19_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in20 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in20_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in21 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in21_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in22 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in22_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in23 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in23_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in24 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in24_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in25 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in25_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in26 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in26_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in27 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in27_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in28 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in28_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in29 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in29_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in30 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in30_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in31 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in31_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in32 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in32_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in33 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in33_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in34 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in34_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in35 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in35_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in36 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in36_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in37 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in37_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in38 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in38_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in39 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in39_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in40 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in40_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in41 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in41_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in42 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in42_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in43 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in43_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in44 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in44_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in45 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in45_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in46 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in46_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in47 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in47_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in48 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in48_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in49 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in49_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in50 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in50_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in51 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in51_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in52 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in52_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in53 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in53_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in54 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in54_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in55 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in55_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in56 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in56_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in57 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in57_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in58 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in58_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in59 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in59_doubt = id545out_result_doubt;
    const HWRawBits<1> &id605in_in60 = id545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in60_doubt = id545out_result_doubt;

    id605out_result = (cat((cat((cat((cat((cat((cat(id605in_in0,id605in_in1)),(cat(id605in_in2,id605in_in3)))),(cat((cat(id605in_in4,id605in_in5)),(cat(id605in_in6,id605in_in7)))))),(cat((cat((cat(id605in_in8,id605in_in9)),(cat(id605in_in10,id605in_in11)))),(cat((cat(id605in_in12,id605in_in13)),(cat(id605in_in14,id605in_in15)))))))),(cat((cat((cat((cat(id605in_in16,id605in_in17)),(cat(id605in_in18,id605in_in19)))),(cat((cat(id605in_in20,id605in_in21)),(cat(id605in_in22,id605in_in23)))))),(cat((cat((cat(id605in_in24,id605in_in25)),(cat(id605in_in26,id605in_in27)))),(cat((cat(id605in_in28,id605in_in29)),id605in_in30)))))))),(cat((cat((cat((cat((cat(id605in_in31,id605in_in32)),(cat(id605in_in33,id605in_in34)))),(cat((cat(id605in_in35,id605in_in36)),(cat(id605in_in37,id605in_in38)))))),(cat((cat((cat(id605in_in39,id605in_in40)),(cat(id605in_in41,id605in_in42)))),(cat((cat(id605in_in43,id605in_in44)),id605in_in45)))))),(cat((cat((cat((cat(id605in_in46,id605in_in47)),(cat(id605in_in48,id605in_in49)))),(cat((cat(id605in_in50,id605in_in51)),(cat(id605in_in52,id605in_in53)))))),(cat((cat((cat(id605in_in54,id605in_in55)),(cat(id605in_in56,id605in_in57)))),(cat((cat(id605in_in58,id605in_in59)),id605in_in60))))))))));
    id605out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id605in_in0_doubt,id605in_in1_doubt)),id605in_in2_doubt)),id605in_in3_doubt)),id605in_in4_doubt)),id605in_in5_doubt)),id605in_in6_doubt)),id605in_in7_doubt)),id605in_in8_doubt)),id605in_in9_doubt)),id605in_in10_doubt)),id605in_in11_doubt)),id605in_in12_doubt)),id605in_in13_doubt)),id605in_in14_doubt)),id605in_in15_doubt)),id605in_in16_doubt)),id605in_in17_doubt)),id605in_in18_doubt)),id605in_in19_doubt)),id605in_in20_doubt)),id605in_in21_doubt)),id605in_in22_doubt)),id605in_in23_doubt)),id605in_in24_doubt)),id605in_in25_doubt)),id605in_in26_doubt)),id605in_in27_doubt)),id605in_in28_doubt)),id605in_in29_doubt)),id605in_in30_doubt)),id605in_in31_doubt)),id605in_in32_doubt)),id605in_in33_doubt)),id605in_in34_doubt)),id605in_in35_doubt)),id605in_in36_doubt)),id605in_in37_doubt)),id605in_in38_doubt)),id605in_in39_doubt)),id605in_in40_doubt)),id605in_in41_doubt)),id605in_in42_doubt)),id605in_in43_doubt)),id605in_in44_doubt)),id605in_in45_doubt)),id605in_in46_doubt)),id605in_in47_doubt)),id605in_in48_doubt)),id605in_in49_doubt)),id605in_in50_doubt)),id605in_in51_doubt)),id605in_in52_doubt)),id605in_in53_doubt)),id605in_in54_doubt)),id605in_in55_doubt)),id605in_in56_doubt)),id605in_in57_doubt)),id605in_in58_doubt)),id605in_in59_doubt)),id605in_in60_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id109out_output;
  HWOffsetFix<1,0,UNSIGNED> id109out_output_doubt;

  { // Node ID: 109 (NodeReinterpret)
    const HWRawBits<61> &id109in_input = id605out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id109in_input_doubt = id605out_result_doubt;

    id109out_output = (cast_bits2fixed<61,-48,TWOSCOMPLEMENT>(id109in_input));
    id109out_output_doubt = id109in_input_doubt;
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id110out_result;
  HWOffsetFix<1,0,UNSIGNED> id110out_result_doubt;

  { // Node ID: 110 (NodeXor)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id110in_a = id46out_value;
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id110in_b = id109out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id110in_b_doubt = id109out_output_doubt;

    HWOffsetFix<61,-48,TWOSCOMPLEMENT> id110x_1;

    (id110x_1) = (xor_fixed(id110in_a,id110in_b));
    id110out_result = (id110x_1);
    id110out_result_doubt = id110in_b_doubt;
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id111out_result;
  HWOffsetFix<1,0,UNSIGNED> id111out_result_doubt;

  { // Node ID: 111 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_sel = id45out_output;
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id111in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id111in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id111in_option1 = id110out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id111in_option1_doubt = id110out_result_doubt;

    HWOffsetFix<61,-48,TWOSCOMPLEMENT> id111x_1;
    HWOffsetFix<1,0,UNSIGNED> id111x_2;

    switch((id111in_sel.getValueAsLong())) {
      case 0l:
        id111x_1 = id111in_option0;
        break;
      case 1l:
        id111x_1 = id111in_option1;
        break;
      default:
        id111x_1 = (c_hw_fix_61_n48_sgn_undef);
        break;
    }
    switch((id111in_sel.getValueAsLong())) {
      case 0l:
        id111x_2 = id111in_option0_doubt;
        break;
      case 1l:
        id111x_2 = id111in_option1_doubt;
        break;
      default:
        id111x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id111out_result = (id111x_1);
    id111out_result_doubt = (id111x_2);
  }
  { // Node ID: 114 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id113out_result;
  HWOffsetFix<1,0,UNSIGNED> id113out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id113out_exception;

  { // Node ID: 113 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id113in_a = id111out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id113in_a_doubt = id111out_result_doubt;
    const HWOffsetFix<52,-51,UNSIGNED> &id113in_b = id114out_value;

    HWOffsetFix<1,0,UNSIGNED> id113x_1;

    id113out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id113in_a,id113in_b,(&(id113x_1))));
    id113out_result_doubt = (or_fixed((neq_fixed((id113x_1),(c_hw_fix_1_0_uns_bits_1))),id113in_a_doubt));
    id113out_exception = (id113x_1);
  }
  { // Node ID: 116 (NodeConstantRawBits)
  }
  HWRawBits<1> id606out_result;
  HWOffsetFix<1,0,UNSIGNED> id606out_result_doubt;

  { // Node ID: 606 (NodeSlice)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id606in_a = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id606in_a_doubt = id113out_result_doubt;

    id606out_result = (slice<112,1>(id606in_a));
    id606out_result_doubt = id606in_a_doubt;
  }
  HWRawBits<1> id607out_result;
  HWOffsetFix<1,0,UNSIGNED> id607out_result_doubt;

  { // Node ID: 607 (NodeNot)
    const HWRawBits<1> &id607in_a = id606out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id607in_a_doubt = id606out_result_doubt;

    id607out_result = (not_bits(id607in_a));
    id607out_result_doubt = id607in_a_doubt;
  }
  HWRawBits<113> id719out_result;
  HWOffsetFix<1,0,UNSIGNED> id719out_result_doubt;

  { // Node ID: 719 (NodeCat)
    const HWRawBits<1> &id719in_in0 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in0_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in1 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in1_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in2 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in2_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in3 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in3_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in4 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in4_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in5 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in5_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in6 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in6_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in7 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in7_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in8 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in8_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in9 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in9_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in10 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in10_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in11 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in11_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in12 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in12_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in13 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in13_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in14 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in14_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in15 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in15_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in16 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in16_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in17 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in17_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in18 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in18_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in19 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in19_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in20 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in20_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in21 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in21_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in22 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in22_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in23 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in23_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in24 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in24_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in25 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in25_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in26 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in26_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in27 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in27_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in28 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in28_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in29 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in29_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in30 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in30_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in31 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in31_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in32 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in32_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in33 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in33_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in34 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in34_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in35 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in35_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in36 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in36_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in37 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in37_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in38 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in38_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in39 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in39_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in40 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in40_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in41 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in41_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in42 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in42_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in43 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in43_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in44 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in44_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in45 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in45_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in46 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in46_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in47 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in47_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in48 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in48_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in49 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in49_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in50 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in50_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in51 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in51_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in52 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in52_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in53 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in53_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in54 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in54_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in55 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in55_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in56 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in56_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in57 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in57_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in58 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in58_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in59 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in59_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in60 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in60_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in61 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in61_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in62 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in62_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in63 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in63_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in64 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in64_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in65 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in65_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in66 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in66_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in67 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in67_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in68 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in68_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in69 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in69_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in70 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in70_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in71 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in71_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in72 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in72_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in73 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in73_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in74 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in74_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in75 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in75_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in76 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in76_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in77 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in77_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in78 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in78_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in79 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in79_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in80 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in80_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in81 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in81_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in82 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in82_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in83 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in83_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in84 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in84_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in85 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in85_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in86 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in86_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in87 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in87_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in88 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in88_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in89 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in89_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in90 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in90_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in91 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in91_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in92 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in92_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in93 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in93_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in94 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in94_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in95 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in95_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in96 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in96_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in97 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in97_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in98 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in98_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in99 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in99_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in100 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in100_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in101 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in101_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in102 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in102_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in103 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in103_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in104 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in104_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in105 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in105_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in106 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in106_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in107 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in107_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in108 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in108_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in109 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in109_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in110 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in110_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in111 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in111_doubt = id607out_result_doubt;
    const HWRawBits<1> &id719in_in112 = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id719in_in112_doubt = id607out_result_doubt;

    id719out_result = (cat((cat((cat((cat((cat((cat((cat(id719in_in0,id719in_in1)),(cat(id719in_in2,id719in_in3)))),(cat((cat(id719in_in4,id719in_in5)),(cat(id719in_in6,id719in_in7)))))),(cat((cat((cat(id719in_in8,id719in_in9)),(cat(id719in_in10,id719in_in11)))),(cat((cat(id719in_in12,id719in_in13)),id719in_in14)))))),(cat((cat((cat((cat(id719in_in15,id719in_in16)),(cat(id719in_in17,id719in_in18)))),(cat((cat(id719in_in19,id719in_in20)),id719in_in21)))),(cat((cat((cat(id719in_in22,id719in_in23)),(cat(id719in_in24,id719in_in25)))),(cat((cat(id719in_in26,id719in_in27)),id719in_in28)))))))),(cat((cat((cat((cat((cat(id719in_in29,id719in_in30)),(cat(id719in_in31,id719in_in32)))),(cat((cat(id719in_in33,id719in_in34)),id719in_in35)))),(cat((cat((cat(id719in_in36,id719in_in37)),(cat(id719in_in38,id719in_in39)))),(cat((cat(id719in_in40,id719in_in41)),id719in_in42)))))),(cat((cat((cat((cat(id719in_in43,id719in_in44)),(cat(id719in_in45,id719in_in46)))),(cat((cat(id719in_in47,id719in_in48)),id719in_in49)))),(cat((cat((cat(id719in_in50,id719in_in51)),(cat(id719in_in52,id719in_in53)))),(cat((cat(id719in_in54,id719in_in55)),id719in_in56)))))))))),(cat((cat((cat((cat((cat((cat(id719in_in57,id719in_in58)),(cat(id719in_in59,id719in_in60)))),(cat((cat(id719in_in61,id719in_in62)),id719in_in63)))),(cat((cat((cat(id719in_in64,id719in_in65)),(cat(id719in_in66,id719in_in67)))),(cat((cat(id719in_in68,id719in_in69)),id719in_in70)))))),(cat((cat((cat((cat(id719in_in71,id719in_in72)),(cat(id719in_in73,id719in_in74)))),(cat((cat(id719in_in75,id719in_in76)),id719in_in77)))),(cat((cat((cat(id719in_in78,id719in_in79)),(cat(id719in_in80,id719in_in81)))),(cat((cat(id719in_in82,id719in_in83)),id719in_in84)))))))),(cat((cat((cat((cat((cat(id719in_in85,id719in_in86)),(cat(id719in_in87,id719in_in88)))),(cat((cat(id719in_in89,id719in_in90)),id719in_in91)))),(cat((cat((cat(id719in_in92,id719in_in93)),(cat(id719in_in94,id719in_in95)))),(cat((cat(id719in_in96,id719in_in97)),id719in_in98)))))),(cat((cat((cat((cat(id719in_in99,id719in_in100)),(cat(id719in_in101,id719in_in102)))),(cat((cat(id719in_in103,id719in_in104)),id719in_in105)))),(cat((cat((cat(id719in_in106,id719in_in107)),(cat(id719in_in108,id719in_in109)))),(cat((cat(id719in_in110,id719in_in111)),id719in_in112))))))))))));
    id719out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id719in_in0_doubt,id719in_in1_doubt)),id719in_in2_doubt)),id719in_in3_doubt)),id719in_in4_doubt)),id719in_in5_doubt)),id719in_in6_doubt)),id719in_in7_doubt)),id719in_in8_doubt)),id719in_in9_doubt)),id719in_in10_doubt)),id719in_in11_doubt)),id719in_in12_doubt)),id719in_in13_doubt)),id719in_in14_doubt)),id719in_in15_doubt)),id719in_in16_doubt)),id719in_in17_doubt)),id719in_in18_doubt)),id719in_in19_doubt)),id719in_in20_doubt)),id719in_in21_doubt)),id719in_in22_doubt)),id719in_in23_doubt)),id719in_in24_doubt)),id719in_in25_doubt)),id719in_in26_doubt)),id719in_in27_doubt)),id719in_in28_doubt)),id719in_in29_doubt)),id719in_in30_doubt)),id719in_in31_doubt)),id719in_in32_doubt)),id719in_in33_doubt)),id719in_in34_doubt)),id719in_in35_doubt)),id719in_in36_doubt)),id719in_in37_doubt)),id719in_in38_doubt)),id719in_in39_doubt)),id719in_in40_doubt)),id719in_in41_doubt)),id719in_in42_doubt)),id719in_in43_doubt)),id719in_in44_doubt)),id719in_in45_doubt)),id719in_in46_doubt)),id719in_in47_doubt)),id719in_in48_doubt)),id719in_in49_doubt)),id719in_in50_doubt)),id719in_in51_doubt)),id719in_in52_doubt)),id719in_in53_doubt)),id719in_in54_doubt)),id719in_in55_doubt)),id719in_in56_doubt)),id719in_in57_doubt)),id719in_in58_doubt)),id719in_in59_doubt)),id719in_in60_doubt)),id719in_in61_doubt)),id719in_in62_doubt)),id719in_in63_doubt)),id719in_in64_doubt)),id719in_in65_doubt)),id719in_in66_doubt)),id719in_in67_doubt)),id719in_in68_doubt)),id719in_in69_doubt)),id719in_in70_doubt)),id719in_in71_doubt)),id719in_in72_doubt)),id719in_in73_doubt)),id719in_in74_doubt)),id719in_in75_doubt)),id719in_in76_doubt)),id719in_in77_doubt)),id719in_in78_doubt)),id719in_in79_doubt)),id719in_in80_doubt)),id719in_in81_doubt)),id719in_in82_doubt)),id719in_in83_doubt)),id719in_in84_doubt)),id719in_in85_doubt)),id719in_in86_doubt)),id719in_in87_doubt)),id719in_in88_doubt)),id719in_in89_doubt)),id719in_in90_doubt)),id719in_in91_doubt)),id719in_in92_doubt)),id719in_in93_doubt)),id719in_in94_doubt)),id719in_in95_doubt)),id719in_in96_doubt)),id719in_in97_doubt)),id719in_in98_doubt)),id719in_in99_doubt)),id719in_in100_doubt)),id719in_in101_doubt)),id719in_in102_doubt)),id719in_in103_doubt)),id719in_in104_doubt)),id719in_in105_doubt)),id719in_in106_doubt)),id719in_in107_doubt)),id719in_in108_doubt)),id719in_in109_doubt)),id719in_in110_doubt)),id719in_in111_doubt)),id719in_in112_doubt));
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id231out_output;
  HWOffsetFix<1,0,UNSIGNED> id231out_output_doubt;

  { // Node ID: 231 (NodeReinterpret)
    const HWRawBits<113> &id231in_input = id719out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id231in_input_doubt = id719out_result_doubt;

    id231out_output = (cast_bits2fixed<113,-99,TWOSCOMPLEMENT>(id231in_input));
    id231out_output_doubt = id231in_input_doubt;
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id232out_result;
  HWOffsetFix<1,0,UNSIGNED> id232out_result_doubt;

  { // Node ID: 232 (NodeXor)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id232in_a = id116out_value;
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id232in_b = id231out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id232in_b_doubt = id231out_output_doubt;

    HWOffsetFix<113,-99,TWOSCOMPLEMENT> id232x_1;

    (id232x_1) = (xor_fixed(id232in_a,id232in_b));
    id232out_result = (id232x_1);
    id232out_result_doubt = id232in_b_doubt;
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id233out_result;
  HWOffsetFix<1,0,UNSIGNED> id233out_result_doubt;

  { // Node ID: 233 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id233in_sel = id113out_exception;
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id233in_option0 = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_option0_doubt = id113out_result_doubt;
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id233in_option1 = id232out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_option1_doubt = id232out_result_doubt;

    HWOffsetFix<113,-99,TWOSCOMPLEMENT> id233x_1;
    HWOffsetFix<1,0,UNSIGNED> id233x_2;

    switch((id233in_sel.getValueAsLong())) {
      case 0l:
        id233x_1 = id233in_option0;
        break;
      case 1l:
        id233x_1 = id233in_option1;
        break;
      default:
        id233x_1 = (c_hw_fix_113_n99_sgn_undef);
        break;
    }
    switch((id233in_sel.getValueAsLong())) {
      case 0l:
        id233x_2 = id233in_option0_doubt;
        break;
      case 1l:
        id233x_2 = id233in_option1_doubt;
        break;
      default:
        id233x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id233out_result = (id233x_1);
    id233out_result_doubt = (id233x_2);
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id234out_o;
  HWOffsetFix<1,0,UNSIGNED> id234out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id234out_exception;

  { // Node ID: 234 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id234in_i = id233out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id234in_i_doubt = id233out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id234x_1;

    id234out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id234in_i,(&(id234x_1))));
    id234out_o_doubt = (or_fixed((neq_fixed((id234x_1),(c_hw_fix_1_0_uns_bits_1))),id234in_i_doubt));
    id234out_exception = (id234x_1);
  }
  { // Node ID: 236 (NodeConstantRawBits)
  }
  HWRawBits<1> id720out_result;
  HWOffsetFix<1,0,UNSIGNED> id720out_result_doubt;

  { // Node ID: 720 (NodeSlice)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id720in_a = id234out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id720in_a_doubt = id234out_o_doubt;

    id720out_result = (slice<60,1>(id720in_a));
    id720out_result_doubt = id720in_a_doubt;
  }
  HWRawBits<1> id721out_result;
  HWOffsetFix<1,0,UNSIGNED> id721out_result_doubt;

  { // Node ID: 721 (NodeNot)
    const HWRawBits<1> &id721in_a = id720out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_a_doubt = id720out_result_doubt;

    id721out_result = (not_bits(id721in_a));
    id721out_result_doubt = id721in_a_doubt;
  }
  HWRawBits<61> id781out_result;
  HWOffsetFix<1,0,UNSIGNED> id781out_result_doubt;

  { // Node ID: 781 (NodeCat)
    const HWRawBits<1> &id781in_in0 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in0_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in1 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in1_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in2 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in2_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in3 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in3_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in4 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in4_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in5 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in5_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in6 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in6_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in7 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in7_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in8 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in8_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in9 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in9_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in10 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in10_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in11 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in11_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in12 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in12_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in13 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in13_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in14 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in14_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in15 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in15_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in16 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in16_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in17 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in17_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in18 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in18_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in19 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in19_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in20 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in20_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in21 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in21_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in22 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in22_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in23 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in23_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in24 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in24_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in25 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in25_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in26 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in26_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in27 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in27_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in28 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in28_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in29 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in29_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in30 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in30_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in31 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in31_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in32 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in32_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in33 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in33_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in34 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in34_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in35 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in35_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in36 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in36_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in37 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in37_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in38 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in38_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in39 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in39_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in40 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in40_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in41 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in41_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in42 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in42_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in43 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in43_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in44 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in44_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in45 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in45_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in46 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in46_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in47 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in47_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in48 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in48_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in49 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in49_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in50 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in50_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in51 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in51_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in52 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in52_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in53 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in53_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in54 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in54_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in55 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in55_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in56 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in56_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in57 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in57_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in58 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in58_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in59 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in59_doubt = id721out_result_doubt;
    const HWRawBits<1> &id781in_in60 = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_in60_doubt = id721out_result_doubt;

    id781out_result = (cat((cat((cat((cat((cat((cat(id781in_in0,id781in_in1)),(cat(id781in_in2,id781in_in3)))),(cat((cat(id781in_in4,id781in_in5)),(cat(id781in_in6,id781in_in7)))))),(cat((cat((cat(id781in_in8,id781in_in9)),(cat(id781in_in10,id781in_in11)))),(cat((cat(id781in_in12,id781in_in13)),(cat(id781in_in14,id781in_in15)))))))),(cat((cat((cat((cat(id781in_in16,id781in_in17)),(cat(id781in_in18,id781in_in19)))),(cat((cat(id781in_in20,id781in_in21)),(cat(id781in_in22,id781in_in23)))))),(cat((cat((cat(id781in_in24,id781in_in25)),(cat(id781in_in26,id781in_in27)))),(cat((cat(id781in_in28,id781in_in29)),id781in_in30)))))))),(cat((cat((cat((cat((cat(id781in_in31,id781in_in32)),(cat(id781in_in33,id781in_in34)))),(cat((cat(id781in_in35,id781in_in36)),(cat(id781in_in37,id781in_in38)))))),(cat((cat((cat(id781in_in39,id781in_in40)),(cat(id781in_in41,id781in_in42)))),(cat((cat(id781in_in43,id781in_in44)),id781in_in45)))))),(cat((cat((cat((cat(id781in_in46,id781in_in47)),(cat(id781in_in48,id781in_in49)))),(cat((cat(id781in_in50,id781in_in51)),(cat(id781in_in52,id781in_in53)))))),(cat((cat((cat(id781in_in54,id781in_in55)),(cat(id781in_in56,id781in_in57)))),(cat((cat(id781in_in58,id781in_in59)),id781in_in60))))))))));
    id781out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id781in_in0_doubt,id781in_in1_doubt)),id781in_in2_doubt)),id781in_in3_doubt)),id781in_in4_doubt)),id781in_in5_doubt)),id781in_in6_doubt)),id781in_in7_doubt)),id781in_in8_doubt)),id781in_in9_doubt)),id781in_in10_doubt)),id781in_in11_doubt)),id781in_in12_doubt)),id781in_in13_doubt)),id781in_in14_doubt)),id781in_in15_doubt)),id781in_in16_doubt)),id781in_in17_doubt)),id781in_in18_doubt)),id781in_in19_doubt)),id781in_in20_doubt)),id781in_in21_doubt)),id781in_in22_doubt)),id781in_in23_doubt)),id781in_in24_doubt)),id781in_in25_doubt)),id781in_in26_doubt)),id781in_in27_doubt)),id781in_in28_doubt)),id781in_in29_doubt)),id781in_in30_doubt)),id781in_in31_doubt)),id781in_in32_doubt)),id781in_in33_doubt)),id781in_in34_doubt)),id781in_in35_doubt)),id781in_in36_doubt)),id781in_in37_doubt)),id781in_in38_doubt)),id781in_in39_doubt)),id781in_in40_doubt)),id781in_in41_doubt)),id781in_in42_doubt)),id781in_in43_doubt)),id781in_in44_doubt)),id781in_in45_doubt)),id781in_in46_doubt)),id781in_in47_doubt)),id781in_in48_doubt)),id781in_in49_doubt)),id781in_in50_doubt)),id781in_in51_doubt)),id781in_in52_doubt)),id781in_in53_doubt)),id781in_in54_doubt)),id781in_in55_doubt)),id781in_in56_doubt)),id781in_in57_doubt)),id781in_in58_doubt)),id781in_in59_doubt)),id781in_in60_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id299out_output;
  HWOffsetFix<1,0,UNSIGNED> id299out_output_doubt;

  { // Node ID: 299 (NodeReinterpret)
    const HWRawBits<61> &id299in_input = id781out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id299in_input_doubt = id781out_result_doubt;

    id299out_output = (cast_bits2fixed<61,-48,TWOSCOMPLEMENT>(id299in_input));
    id299out_output_doubt = id299in_input_doubt;
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id300out_result;
  HWOffsetFix<1,0,UNSIGNED> id300out_result_doubt;

  { // Node ID: 300 (NodeXor)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id300in_a = id236out_value;
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id300in_b = id299out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id300in_b_doubt = id299out_output_doubt;

    HWOffsetFix<61,-48,TWOSCOMPLEMENT> id300x_1;

    (id300x_1) = (xor_fixed(id300in_a,id300in_b));
    id300out_result = (id300x_1);
    id300out_result_doubt = id300in_b_doubt;
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id301out_result;
  HWOffsetFix<1,0,UNSIGNED> id301out_result_doubt;

  { // Node ID: 301 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id301in_sel = id234out_exception;
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id301in_option0 = id234out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id301in_option0_doubt = id234out_o_doubt;
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id301in_option1 = id300out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id301in_option1_doubt = id300out_result_doubt;

    HWOffsetFix<61,-48,TWOSCOMPLEMENT> id301x_1;
    HWOffsetFix<1,0,UNSIGNED> id301x_2;

    switch((id301in_sel.getValueAsLong())) {
      case 0l:
        id301x_1 = id301in_option0;
        break;
      case 1l:
        id301x_1 = id301in_option1;
        break;
      default:
        id301x_1 = (c_hw_fix_61_n48_sgn_undef);
        break;
    }
    switch((id301in_sel.getValueAsLong())) {
      case 0l:
        id301x_2 = id301in_option0_doubt;
        break;
      case 1l:
        id301x_2 = id301in_option1_doubt;
        break;
      default:
        id301x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id301out_result = (id301x_1);
    id301out_result_doubt = (id301x_2);
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id310out_output;

  { // Node ID: 310 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id310in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_input_doubt = id301out_result_doubt;

    id310out_output = id310in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id311out_o;

  { // Node ID: 311 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id311in_i = id310out_output;

    id311out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id311in_i));
  }
  { // Node ID: 874 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id874in_input = id311out_o;

    id874out_output[(getCycle()+2)%3] = id874in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id374out_o;

  { // Node ID: 374 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id374in_i = id874out_output[getCycle()%3];

    id374out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id374in_i));
  }
  { // Node ID: 907 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id376out_result;
  HWOffsetFix<1,0,UNSIGNED> id376out_exception;

  { // Node ID: 376 (NodeAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id376in_a = id374out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id376in_b = id907out_value;

    HWOffsetFix<1,0,UNSIGNED> id376x_1;

    id376out_result = (add_fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id376in_a,id376in_b,(&(id376x_1))));
    id376out_exception = (id376x_1);
  }
  { // Node ID: 378 (NodeConstantRawBits)
  }
  HWRawBits<1> id782out_result;

  { // Node ID: 782 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id782in_a = id376out_result;

    id782out_result = (slice<13,1>(id782in_a));
  }
  HWRawBits<1> id783out_result;

  { // Node ID: 783 (NodeNot)
    const HWRawBits<1> &id783in_a = id782out_result;

    id783out_result = (not_bits(id783in_a));
  }
  HWRawBits<14> id796out_result;

  { // Node ID: 796 (NodeCat)
    const HWRawBits<1> &id796in_in0 = id783out_result;
    const HWRawBits<1> &id796in_in1 = id783out_result;
    const HWRawBits<1> &id796in_in2 = id783out_result;
    const HWRawBits<1> &id796in_in3 = id783out_result;
    const HWRawBits<1> &id796in_in4 = id783out_result;
    const HWRawBits<1> &id796in_in5 = id783out_result;
    const HWRawBits<1> &id796in_in6 = id783out_result;
    const HWRawBits<1> &id796in_in7 = id783out_result;
    const HWRawBits<1> &id796in_in8 = id783out_result;
    const HWRawBits<1> &id796in_in9 = id783out_result;
    const HWRawBits<1> &id796in_in10 = id783out_result;
    const HWRawBits<1> &id796in_in11 = id783out_result;
    const HWRawBits<1> &id796in_in12 = id783out_result;
    const HWRawBits<1> &id796in_in13 = id783out_result;

    id796out_result = (cat((cat((cat((cat(id796in_in0,id796in_in1)),(cat(id796in_in2,id796in_in3)))),(cat((cat(id796in_in4,id796in_in5)),id796in_in6)))),(cat((cat((cat(id796in_in7,id796in_in8)),(cat(id796in_in9,id796in_in10)))),(cat((cat(id796in_in11,id796in_in12)),id796in_in13))))));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id394out_output;

  { // Node ID: 394 (NodeReinterpret)
    const HWRawBits<14> &id394in_input = id796out_result;

    id394out_output = (cast_bits2fixed<14,0,TWOSCOMPLEMENT>(id394in_input));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id395out_result;

  { // Node ID: 395 (NodeXor)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id395in_a = id378out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id395in_b = id394out_output;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id395x_1;

    (id395x_1) = (xor_fixed(id395in_a,id395in_b));
    id395out_result = (id395x_1);
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id396out_result;

  { // Node ID: 396 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id396in_sel = id376out_exception;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id396in_option0 = id376out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id396in_option1 = id395out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id396x_1;

    switch((id396in_sel.getValueAsLong())) {
      case 0l:
        id396x_1 = id396in_option0;
        break;
      case 1l:
        id396x_1 = id396in_option1;
        break;
      default:
        id396x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    id396out_result = (id396x_1);
  }
  { // Node ID: 397 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id314out_o;

  { // Node ID: 314 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id314in_i = id310out_output;

    id314out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id314in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id479out_output;

  { // Node ID: 479 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id479in_input = id314out_o;

    id479out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id479in_input))));
  }
  { // Node ID: 480 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id480in_addr = id479out_output;

    HWOffsetFix<32,-45,UNSIGNED> id480x_1;

    switch(((long)((id480in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id480x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id480x_1 = (id480sta_rom_store[(id480in_addr.getValueAsLong())]);
        break;
      default:
        id480x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id480out_dout[(getCycle()+2)%3] = (id480x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id313out_o;

  { // Node ID: 313 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id313in_i = id310out_output;

    id313out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id313in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id476out_output;

  { // Node ID: 476 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id476in_input = id313out_o;

    id476out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id476in_input))));
  }
  { // Node ID: 477 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id477in_addr = id476out_output;

    HWOffsetFix<42,-45,UNSIGNED> id477x_1;

    switch(((long)((id477in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id477x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id477x_1 = (id477sta_rom_store[(id477in_addr.getValueAsLong())]);
        break;
      default:
        id477x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id477out_dout[(getCycle()+2)%3] = (id477x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id312out_o;

  { // Node ID: 312 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id312in_i = id310out_output;

    id312out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id312in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id473out_output;

  { // Node ID: 473 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id473in_input = id312out_o;

    id473out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id473in_input))));
  }
  { // Node ID: 474 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id474in_addr = id473out_output;

    HWOffsetFix<45,-45,UNSIGNED> id474x_1;

    switch(((long)((id474in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id474x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id474x_1 = (id474sta_rom_store[(id474in_addr.getValueAsLong())]);
        break;
      default:
        id474x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id474out_dout[(getCycle()+2)%3] = (id474x_1);
  }
  { // Node ID: 318 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id315out_o;

  { // Node ID: 315 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id315in_i = id310out_output;

    id315out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id315in_i));
  }
  { // Node ID: 875 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id875in_input = id315out_o;

    id875out_output[(getCycle()+2)%3] = id875in_input;
  }
  HWOffsetFix<46,-69,UNSIGNED> id317out_result;

  { // Node ID: 317 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id317in_a = id318out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id317in_b = id875out_output[getCycle()%3];

    id317out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id317in_a,id317in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id319out_o;
  HWOffsetFix<1,0,UNSIGNED> id319out_exception;

  { // Node ID: 319 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id319in_i = id317out_result;

    HWOffsetFix<1,0,UNSIGNED> id319x_1;

    id319out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id319in_i,(&(id319x_1))));
    id319out_exception = (id319x_1);
  }
  { // Node ID: 321 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id322out_result;

  { // Node ID: 322 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id322in_sel = id319out_exception;
    const HWOffsetFix<25,-48,UNSIGNED> &id322in_option0 = id319out_o;
    const HWOffsetFix<25,-48,UNSIGNED> &id322in_option1 = id321out_value;

    HWOffsetFix<25,-48,UNSIGNED> id322x_1;

    switch((id322in_sel.getValueAsLong())) {
      case 0l:
        id322x_1 = id322in_option0;
        break;
      case 1l:
        id322x_1 = id322in_option1;
        break;
      default:
        id322x_1 = (c_hw_fix_25_n48_uns_undef);
        break;
    }
    id322out_result = (id322x_1);
  }
  HWOffsetFix<49,-48,UNSIGNED> id323out_result;

  { // Node ID: 323 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id323in_a = id474out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id323in_b = id322out_result;

    id323out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id323in_a,id323in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id324out_result;
  HWOffsetFix<1,0,UNSIGNED> id324out_exception;

  { // Node ID: 324 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id324in_a = id322out_result;
    const HWOffsetFix<45,-45,UNSIGNED> &id324in_b = id474out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id324x_1;

    id324out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id324in_a,id324in_b,(&(id324x_1))));
    id324out_exception = (id324x_1);
  }
  { // Node ID: 326 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-87,UNSIGNED> id327out_result;

  { // Node ID: 327 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id327in_sel = id324out_exception;
    const HWOffsetFix<64,-87,UNSIGNED> &id327in_option0 = id324out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id327in_option1 = id326out_value;

    HWOffsetFix<64,-87,UNSIGNED> id327x_1;

    switch((id327in_sel.getValueAsLong())) {
      case 0l:
        id327x_1 = id327in_option0;
        break;
      case 1l:
        id327x_1 = id327in_option1;
        break;
      default:
        id327x_1 = (c_hw_fix_64_n87_uns_undef);
        break;
    }
    id327out_result = (id327x_1);
  }
  HWOffsetFix<64,-63,UNSIGNED> id328out_result;
  HWOffsetFix<1,0,UNSIGNED> id328out_exception;

  { // Node ID: 328 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id328in_a = id323out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id328in_b = id327out_result;

    HWOffsetFix<1,0,UNSIGNED> id328x_1;

    id328out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id328in_a,id328in_b,(&(id328x_1))));
    id328out_exception = (id328x_1);
  }
  { // Node ID: 330 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-63,UNSIGNED> id331out_result;

  { // Node ID: 331 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_sel = id328out_exception;
    const HWOffsetFix<64,-63,UNSIGNED> &id331in_option0 = id328out_result;
    const HWOffsetFix<64,-63,UNSIGNED> &id331in_option1 = id330out_value;

    HWOffsetFix<64,-63,UNSIGNED> id331x_1;

    switch((id331in_sel.getValueAsLong())) {
      case 0l:
        id331x_1 = id331in_option0;
        break;
      case 1l:
        id331x_1 = id331in_option1;
        break;
      default:
        id331x_1 = (c_hw_fix_64_n63_uns_undef);
        break;
    }
    id331out_result = (id331x_1);
  }
  HWOffsetFix<49,-48,UNSIGNED> id332out_o;
  HWOffsetFix<1,0,UNSIGNED> id332out_exception;

  { // Node ID: 332 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id332in_i = id331out_result;

    HWOffsetFix<1,0,UNSIGNED> id332x_1;

    id332out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id332in_i,(&(id332x_1))));
    id332out_exception = (id332x_1);
  }
  { // Node ID: 334 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id335out_result;

  { // Node ID: 335 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id335in_sel = id332out_exception;
    const HWOffsetFix<49,-48,UNSIGNED> &id335in_option0 = id332out_o;
    const HWOffsetFix<49,-48,UNSIGNED> &id335in_option1 = id334out_value;

    HWOffsetFix<49,-48,UNSIGNED> id335x_1;

    switch((id335in_sel.getValueAsLong())) {
      case 0l:
        id335x_1 = id335in_option0;
        break;
      case 1l:
        id335x_1 = id335in_option1;
        break;
      default:
        id335x_1 = (c_hw_fix_49_n48_uns_undef);
        break;
    }
    id335out_result = (id335x_1);
  }
  HWOffsetFix<50,-48,UNSIGNED> id336out_result;

  { // Node ID: 336 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id336in_a = id477out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id336in_b = id335out_result;

    id336out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id336in_a,id336in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id337out_result;
  HWOffsetFix<1,0,UNSIGNED> id337out_exception;

  { // Node ID: 337 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id337in_a = id335out_result;
    const HWOffsetFix<42,-45,UNSIGNED> &id337in_b = id477out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id337x_1;

    id337out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id337in_a,id337in_b,(&(id337x_1))));
    id337out_exception = (id337x_1);
  }
  { // Node ID: 339 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-66,UNSIGNED> id340out_result;

  { // Node ID: 340 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id340in_sel = id337out_exception;
    const HWOffsetFix<64,-66,UNSIGNED> &id340in_option0 = id337out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id340in_option1 = id339out_value;

    HWOffsetFix<64,-66,UNSIGNED> id340x_1;

    switch((id340in_sel.getValueAsLong())) {
      case 0l:
        id340x_1 = id340in_option0;
        break;
      case 1l:
        id340x_1 = id340in_option1;
        break;
      default:
        id340x_1 = (c_hw_fix_64_n66_uns_undef);
        break;
    }
    id340out_result = (id340x_1);
  }
  HWOffsetFix<64,-62,UNSIGNED> id341out_result;
  HWOffsetFix<1,0,UNSIGNED> id341out_exception;

  { // Node ID: 341 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id341in_a = id336out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id341in_b = id340out_result;

    HWOffsetFix<1,0,UNSIGNED> id341x_1;

    id341out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id341in_a,id341in_b,(&(id341x_1))));
    id341out_exception = (id341x_1);
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-62,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id341out_exception;
    const HWOffsetFix<64,-62,UNSIGNED> &id344in_option0 = id341out_result;
    const HWOffsetFix<64,-62,UNSIGNED> &id344in_option1 = id343out_value;

    HWOffsetFix<64,-62,UNSIGNED> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_fix_64_n62_uns_undef);
        break;
    }
    id344out_result = (id344x_1);
  }
  HWOffsetFix<50,-48,UNSIGNED> id345out_o;
  HWOffsetFix<1,0,UNSIGNED> id345out_exception;

  { // Node ID: 345 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id345in_i = id344out_result;

    HWOffsetFix<1,0,UNSIGNED> id345x_1;

    id345out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id345in_i,(&(id345x_1))));
    id345out_exception = (id345x_1);
  }
  { // Node ID: 347 (NodeConstantRawBits)
  }
  HWOffsetFix<50,-48,UNSIGNED> id348out_result;

  { // Node ID: 348 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id348in_sel = id345out_exception;
    const HWOffsetFix<50,-48,UNSIGNED> &id348in_option0 = id345out_o;
    const HWOffsetFix<50,-48,UNSIGNED> &id348in_option1 = id347out_value;

    HWOffsetFix<50,-48,UNSIGNED> id348x_1;

    switch((id348in_sel.getValueAsLong())) {
      case 0l:
        id348x_1 = id348in_option0;
        break;
      case 1l:
        id348x_1 = id348in_option1;
        break;
      default:
        id348x_1 = (c_hw_fix_50_n48_uns_undef);
        break;
    }
    id348out_result = (id348x_1);
  }
  HWOffsetFix<51,-48,UNSIGNED> id349out_result;

  { // Node ID: 349 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id349in_a = id480out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id349in_b = id348out_result;

    id349out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id349in_a,id349in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id350out_result;
  HWOffsetFix<1,0,UNSIGNED> id350out_exception;

  { // Node ID: 350 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id350in_a = id348out_result;
    const HWOffsetFix<32,-45,UNSIGNED> &id350in_b = id480out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id350x_1;

    id350out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id350in_a,id350in_b,(&(id350x_1))));
    id350out_exception = (id350x_1);
  }
  { // Node ID: 352 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-75,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id353in_sel = id350out_exception;
    const HWOffsetFix<64,-75,UNSIGNED> &id353in_option0 = id350out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id353in_option1 = id352out_value;

    HWOffsetFix<64,-75,UNSIGNED> id353x_1;

    switch((id353in_sel.getValueAsLong())) {
      case 0l:
        id353x_1 = id353in_option0;
        break;
      case 1l:
        id353x_1 = id353in_option1;
        break;
      default:
        id353x_1 = (c_hw_fix_64_n75_uns_undef);
        break;
    }
    id353out_result = (id353x_1);
  }
  HWOffsetFix<64,-61,UNSIGNED> id354out_result;
  HWOffsetFix<1,0,UNSIGNED> id354out_exception;

  { // Node ID: 354 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id354in_a = id349out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id354in_b = id353out_result;

    HWOffsetFix<1,0,UNSIGNED> id354x_1;

    id354out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id354in_a,id354in_b,(&(id354x_1))));
    id354out_exception = (id354x_1);
  }
  { // Node ID: 356 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-61,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id357in_sel = id354out_exception;
    const HWOffsetFix<64,-61,UNSIGNED> &id357in_option0 = id354out_result;
    const HWOffsetFix<64,-61,UNSIGNED> &id357in_option1 = id356out_value;

    HWOffsetFix<64,-61,UNSIGNED> id357x_1;

    switch((id357in_sel.getValueAsLong())) {
      case 0l:
        id357x_1 = id357in_option0;
        break;
      case 1l:
        id357x_1 = id357in_option1;
        break;
      default:
        id357x_1 = (c_hw_fix_64_n61_uns_undef);
        break;
    }
    id357out_result = (id357x_1);
  }
  HWOffsetFix<51,-48,UNSIGNED> id358out_o;
  HWOffsetFix<1,0,UNSIGNED> id358out_exception;

  { // Node ID: 358 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id358in_i = id357out_result;

    HWOffsetFix<1,0,UNSIGNED> id358x_1;

    id358out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id358in_i,(&(id358x_1))));
    id358out_exception = (id358x_1);
  }
  { // Node ID: 360 (NodeConstantRawBits)
  }
  HWOffsetFix<51,-48,UNSIGNED> id361out_result;

  { // Node ID: 361 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id361in_sel = id358out_exception;
    const HWOffsetFix<51,-48,UNSIGNED> &id361in_option0 = id358out_o;
    const HWOffsetFix<51,-48,UNSIGNED> &id361in_option1 = id360out_value;

    HWOffsetFix<51,-48,UNSIGNED> id361x_1;

    switch((id361in_sel.getValueAsLong())) {
      case 0l:
        id361x_1 = id361in_option0;
        break;
      case 1l:
        id361x_1 = id361in_option1;
        break;
      default:
        id361x_1 = (c_hw_fix_51_n48_uns_undef);
        break;
    }
    id361out_result = (id361x_1);
  }
  HWOffsetFix<45,-44,UNSIGNED> id362out_o;
  HWOffsetFix<1,0,UNSIGNED> id362out_exception;

  { // Node ID: 362 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id362in_i = id361out_result;

    HWOffsetFix<1,0,UNSIGNED> id362x_1;

    id362out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id362in_i,(&(id362x_1))));
    id362out_exception = (id362x_1);
  }
  { // Node ID: 364 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id365out_result;

  { // Node ID: 365 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id365in_sel = id362out_exception;
    const HWOffsetFix<45,-44,UNSIGNED> &id365in_option0 = id362out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id365in_option1 = id364out_value;

    HWOffsetFix<45,-44,UNSIGNED> id365x_1;

    switch((id365in_sel.getValueAsLong())) {
      case 0l:
        id365x_1 = id365in_option0;
        break;
      case 1l:
        id365x_1 = id365in_option1;
        break;
      default:
        id365x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id365out_result = (id365x_1);
  }
  { // Node ID: 906 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id797out_result;

  { // Node ID: 797 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id797in_a = id365out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id797in_b = id906out_value;

    id797out_result = (gte_fixed(id797in_a,id797in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id833out_result;
  HWOffsetFix<1,0,UNSIGNED> id833out_exception;

  { // Node ID: 833 (NodeCondAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id833in_a = id396out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id833in_b = id397out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id833in_condb = id797out_result;

    HWOffsetFix<1,0,UNSIGNED> id833x_1;
    HWOffsetFix<1,0,UNSIGNED> id833x_2;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id833x_3;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id833x_4;
    HWOffsetFix<15,0,TWOSCOMPLEMENT> id833x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id833x_3 = (c_hw_fix_15_0_sgn_bits);
        break;
      case 1l:
        id833x_3 = (cast_fixed2fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>(id833in_a));
        break;
      default:
        id833x_3 = (c_hw_fix_15_0_sgn_undef);
        break;
    }
    switch((id833in_condb.getValueAsLong())) {
      case 0l:
        id833x_4 = (c_hw_fix_15_0_sgn_bits);
        break;
      case 1l:
        id833x_4 = (cast_fixed2fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>(id833in_b));
        break;
      default:
        id833x_4 = (c_hw_fix_15_0_sgn_undef);
        break;
    }
    (id833x_5) = (add_fixed<15,0,TWOSCOMPLEMENT,TRUNCATE>((id833x_3),(id833x_4),(&(id833x_1))));
    id833out_result = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id833x_5),(&(id833x_2))));
    id833out_exception = (or_fixed((id833x_1),(id833x_2)));
  }
  { // Node ID: 402 (NodeConstantRawBits)
  }
  HWRawBits<1> id798out_result;

  { // Node ID: 798 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id798in_a = id833out_result;

    id798out_result = (slice<13,1>(id798in_a));
  }
  HWRawBits<1> id799out_result;

  { // Node ID: 799 (NodeNot)
    const HWRawBits<1> &id799in_a = id798out_result;

    id799out_result = (not_bits(id799in_a));
  }
  HWRawBits<14> id812out_result;

  { // Node ID: 812 (NodeCat)
    const HWRawBits<1> &id812in_in0 = id799out_result;
    const HWRawBits<1> &id812in_in1 = id799out_result;
    const HWRawBits<1> &id812in_in2 = id799out_result;
    const HWRawBits<1> &id812in_in3 = id799out_result;
    const HWRawBits<1> &id812in_in4 = id799out_result;
    const HWRawBits<1> &id812in_in5 = id799out_result;
    const HWRawBits<1> &id812in_in6 = id799out_result;
    const HWRawBits<1> &id812in_in7 = id799out_result;
    const HWRawBits<1> &id812in_in8 = id799out_result;
    const HWRawBits<1> &id812in_in9 = id799out_result;
    const HWRawBits<1> &id812in_in10 = id799out_result;
    const HWRawBits<1> &id812in_in11 = id799out_result;
    const HWRawBits<1> &id812in_in12 = id799out_result;
    const HWRawBits<1> &id812in_in13 = id799out_result;

    id812out_result = (cat((cat((cat((cat(id812in_in0,id812in_in1)),(cat(id812in_in2,id812in_in3)))),(cat((cat(id812in_in4,id812in_in5)),id812in_in6)))),(cat((cat((cat(id812in_in7,id812in_in8)),(cat(id812in_in9,id812in_in10)))),(cat((cat(id812in_in11,id812in_in12)),id812in_in13))))));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id418out_output;

  { // Node ID: 418 (NodeReinterpret)
    const HWRawBits<14> &id418in_input = id812out_result;

    id418out_output = (cast_bits2fixed<14,0,TWOSCOMPLEMENT>(id418in_input));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id419out_result;

  { // Node ID: 419 (NodeXor)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id419in_a = id402out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id419in_b = id418out_output;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id419x_1;

    (id419x_1) = (xor_fixed(id419in_a,id419in_b));
    id419out_result = (id419x_1);
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id420out_result;

  { // Node ID: 420 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id420in_sel = id833out_exception;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id420in_option0 = id833out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id420in_option1 = id419out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id420x_1;

    switch((id420in_sel.getValueAsLong())) {
      case 0l:
        id420x_1 = id420in_option0;
        break;
      case 1l:
        id420x_1 = id420in_option1;
        break;
      default:
        id420x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    id420out_result = (id420x_1);
  }
  HWRawBits<1> id813out_result;

  { // Node ID: 813 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id813in_a = id420out_result;

    id813out_result = (slice<13,1>(id813in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id814out_output;

  { // Node ID: 814 (NodeReinterpret)
    const HWRawBits<1> &id814in_input = id813out_result;

    id814out_output = (cast_bits2fixed<1,0,UNSIGNED>(id814in_input));
  }
  { // Node ID: 905 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id303out_result;

  { // Node ID: 303 (NodeGt)
    const HWFloat<11,45> &id303in_a = id834out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id303in_b = id905out_value;

    id303out_result = (gt_float(id303in_a,id303in_b));
  }
  { // Node ID: 876 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id876in_input = id303out_result;

    id876out_output[(getCycle()+6)%7] = id876in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id304out_output;

  { // Node ID: 304 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id304in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id304in_input_doubt = id301out_result_doubt;

    id304out_output = id304in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id305out_result;

  { // Node ID: 305 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id305in_a = id876out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id305in_b = id304out_output;

    HWOffsetFix<1,0,UNSIGNED> id305x_1;

    (id305x_1) = (and_fixed(id305in_a,id305in_b));
    id305out_result = (id305x_1);
  }
  { // Node ID: 877 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id877in_input = id305out_result;

    id877out_output[(getCycle()+2)%3] = id877in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id423out_result;

  { // Node ID: 423 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id423in_a = id877out_output[getCycle()%3];

    id423out_result = (not_fixed(id423in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id424out_result;

  { // Node ID: 424 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id424in_a = id814out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id424in_b = id423out_result;

    HWOffsetFix<1,0,UNSIGNED> id424x_1;

    (id424x_1) = (and_fixed(id424in_a,id424in_b));
    id424out_result = (id424x_1);
  }
  { // Node ID: 904 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id307out_result;

  { // Node ID: 307 (NodeLt)
    const HWFloat<11,45> &id307in_a = id834out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id307in_b = id904out_value;

    id307out_result = (lt_float(id307in_a,id307in_b));
  }
  { // Node ID: 878 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id878in_input = id307out_result;

    id878out_output[(getCycle()+6)%7] = id878in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id308out_output;

  { // Node ID: 308 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id308in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id308in_input_doubt = id301out_result_doubt;

    id308out_output = id308in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id309out_result;

  { // Node ID: 309 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id309in_a = id878out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id309in_b = id308out_output;

    HWOffsetFix<1,0,UNSIGNED> id309x_1;

    (id309x_1) = (and_fixed(id309in_a,id309in_b));
    id309out_result = (id309x_1);
  }
  { // Node ID: 879 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id879in_input = id309out_result;

    id879out_output[(getCycle()+2)%3] = id879in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id425out_result;

  { // Node ID: 425 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id425in_a = id424out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id425in_b = id879out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id425x_1;

    (id425x_1) = (or_fixed(id425in_a,id425in_b));
    id425out_result = (id425x_1);
  }
  { // Node ID: 903 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id815out_result;

  { // Node ID: 815 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id815in_a = id420out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id815in_b = id903out_value;

    id815out_result = (gte_fixed(id815in_a,id815in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id449out_result;

  { // Node ID: 449 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id449in_a = id879out_output[getCycle()%3];

    id449out_result = (not_fixed(id449in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id450out_result;

  { // Node ID: 450 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id450in_a = id815out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id450in_b = id449out_result;

    HWOffsetFix<1,0,UNSIGNED> id450x_1;

    (id450x_1) = (and_fixed(id450in_a,id450in_b));
    id450out_result = (id450x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id451out_result;

  { // Node ID: 451 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id451in_a = id450out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id451in_b = id877out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id451x_1;

    (id451x_1) = (or_fixed(id451in_a,id451in_b));
    id451out_result = (id451x_1);
  }
  HWRawBits<2> id452out_result;

  { // Node ID: 452 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id452in_in0 = id425out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id452in_in1 = id451out_result;

    id452out_result = (cat(id452in_in0,id452in_in1));
  }
  { // Node ID: 444 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id426out_o;
  HWOffsetFix<1,0,UNSIGNED> id426out_exception;

  { // Node ID: 426 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id426in_i = id420out_result;

    HWOffsetFix<1,0,UNSIGNED> id426x_1;

    id426out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id426in_i,(&(id426x_1))));
    id426out_exception = (id426x_1);
  }
  { // Node ID: 428 (NodeConstantRawBits)
  }
  HWRawBits<1> id816out_result;

  { // Node ID: 816 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id816in_a = id426out_o;

    id816out_result = (slice<10,1>(id816in_a));
  }
  HWRawBits<1> id817out_result;

  { // Node ID: 817 (NodeNot)
    const HWRawBits<1> &id817in_a = id816out_result;

    id817out_result = (not_bits(id817in_a));
  }
  HWRawBits<11> id827out_result;

  { // Node ID: 827 (NodeCat)
    const HWRawBits<1> &id827in_in0 = id817out_result;
    const HWRawBits<1> &id827in_in1 = id817out_result;
    const HWRawBits<1> &id827in_in2 = id817out_result;
    const HWRawBits<1> &id827in_in3 = id817out_result;
    const HWRawBits<1> &id827in_in4 = id817out_result;
    const HWRawBits<1> &id827in_in5 = id817out_result;
    const HWRawBits<1> &id827in_in6 = id817out_result;
    const HWRawBits<1> &id827in_in7 = id817out_result;
    const HWRawBits<1> &id827in_in8 = id817out_result;
    const HWRawBits<1> &id827in_in9 = id817out_result;
    const HWRawBits<1> &id827in_in10 = id817out_result;

    id827out_result = (cat((cat((cat((cat(id827in_in0,id827in_in1)),id827in_in2)),(cat((cat(id827in_in3,id827in_in4)),id827in_in5)))),(cat((cat((cat(id827in_in6,id827in_in7)),id827in_in8)),(cat(id827in_in9,id827in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id441out_output;

  { // Node ID: 441 (NodeReinterpret)
    const HWRawBits<11> &id441in_input = id827out_result;

    id441out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id441in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id442out_result;

  { // Node ID: 442 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id442in_a = id428out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id442in_b = id441out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id442x_1;

    (id442x_1) = (xor_fixed(id442in_a,id442in_b));
    id442out_result = (id442x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id443out_result;

  { // Node ID: 443 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_sel = id426out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id443in_option0 = id426out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id443in_option1 = id442out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id443x_1;

    switch((id443in_sel.getValueAsLong())) {
      case 0l:
        id443x_1 = id443in_option0;
        break;
      case 1l:
        id443x_1 = id443in_option1;
        break;
      default:
        id443x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id443out_result = (id443x_1);
  }
  { // Node ID: 368 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id369out_result;

  { // Node ID: 369 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id369in_sel = id797out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id369in_option0 = id365out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id369in_option1 = id368out_value;

    HWOffsetFix<45,-44,UNSIGNED> id369x_1;

    switch((id369in_sel.getValueAsLong())) {
      case 0l:
        id369x_1 = id369in_option0;
        break;
      case 1l:
        id369x_1 = id369in_option1;
        break;
      default:
        id369x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id369out_result = (id369x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id370out_o;
  HWOffsetFix<1,0,UNSIGNED> id370out_exception;

  { // Node ID: 370 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id370in_i = id369out_result;

    HWOffsetFix<1,0,UNSIGNED> id370x_1;

    id370out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id370in_i,(&(id370x_1))));
    id370out_exception = (id370x_1);
  }
  { // Node ID: 372 (NodeConstantRawBits)
  }
  HWOffsetFix<44,-44,UNSIGNED> id373out_result;

  { // Node ID: 373 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id373in_sel = id370out_exception;
    const HWOffsetFix<44,-44,UNSIGNED> &id373in_option0 = id370out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id373in_option1 = id372out_value;

    HWOffsetFix<44,-44,UNSIGNED> id373x_1;

    switch((id373in_sel.getValueAsLong())) {
      case 0l:
        id373x_1 = id373in_option0;
        break;
      case 1l:
        id373x_1 = id373in_option1;
        break;
      default:
        id373x_1 = (c_hw_fix_44_n44_uns_undef);
        break;
    }
    id373out_result = (id373x_1);
  }
  HWRawBits<56> id445out_result;

  { // Node ID: 445 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id445in_in0 = id444out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id445in_in1 = id443out_result;
    const HWOffsetFix<44,-44,UNSIGNED> &id445in_in2 = id373out_result;

    id445out_result = (cat((cat(id445in_in0,id445in_in1)),id445in_in2));
  }
  HWFloat<11,45> id446out_output;

  { // Node ID: 446 (NodeReinterpret)
    const HWRawBits<56> &id446in_input = id445out_result;

    id446out_output = (cast_bits2float<11,45>(id446in_input));
  }
  { // Node ID: 453 (NodeConstantRawBits)
  }
  { // Node ID: 454 (NodeConstantRawBits)
  }
  { // Node ID: 456 (NodeConstantRawBits)
  }
  HWRawBits<56> id828out_result;

  { // Node ID: 828 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id828in_in0 = id453out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id828in_in1 = id454out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id828in_in2 = id456out_value;

    id828out_result = (cat((cat(id828in_in0,id828in_in1)),id828in_in2));
  }
  HWFloat<11,45> id458out_output;

  { // Node ID: 458 (NodeReinterpret)
    const HWRawBits<56> &id458in_input = id828out_result;

    id458out_output = (cast_bits2float<11,45>(id458in_input));
  }
  { // Node ID: 539 (NodeConstantRawBits)
  }
  HWFloat<11,45> id461out_result;

  { // Node ID: 461 (NodeMux)
    const HWRawBits<2> &id461in_sel = id452out_result;
    const HWFloat<11,45> &id461in_option0 = id446out_output;
    const HWFloat<11,45> &id461in_option1 = id458out_output;
    const HWFloat<11,45> &id461in_option2 = id539out_value;
    const HWFloat<11,45> &id461in_option3 = id458out_output;

    HWFloat<11,45> id461x_1;

    switch((id461in_sel.getValueAsLong())) {
      case 0l:
        id461x_1 = id461in_option0;
        break;
      case 1l:
        id461x_1 = id461in_option1;
        break;
      case 2l:
        id461x_1 = id461in_option2;
        break;
      case 3l:
        id461x_1 = id461in_option3;
        break;
      default:
        id461x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id461out_result = (id461x_1);
  }
  { // Node ID: 902 (NodeConstantRawBits)
  }
  HWFloat<11,45> id471out_result;

  { // Node ID: 471 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id471in_sel = id882out_output[getCycle()%9];
    const HWFloat<11,45> &id471in_option0 = id461out_result;
    const HWFloat<11,45> &id471in_option1 = id902out_value;

    HWFloat<11,45> id471x_1;

    switch((id471in_sel.getValueAsLong())) {
      case 0l:
        id471x_1 = id471in_option0;
        break;
      case 1l:
        id471x_1 = id471in_option1;
        break;
      default:
        id471x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id471out_result = (id471x_1);
  }
  { // Node ID: 901 (NodeConstantRawBits)
  }
  HWFloat<11,45> id482out_result;
  HWOffsetFix<4,0,UNSIGNED> id482out_exception;

  { // Node ID: 482 (NodeAdd)
    const HWFloat<11,45> &id482in_a = id471out_result;
    const HWFloat<11,45> &id482in_b = id901out_value;

    HWOffsetFix<4,0,UNSIGNED> id482x_1;

    id482out_result = (add_float(id482in_a,id482in_b,(&(id482x_1))));
    id482out_exception = (id482x_1);
  }
  HWFloat<11,45> id484out_result;
  HWOffsetFix<4,0,UNSIGNED> id484out_exception;

  { // Node ID: 484 (NodeDiv)
    const HWFloat<11,45> &id484in_a = id909out_value;
    const HWFloat<11,45> &id484in_b = id482out_result;

    HWOffsetFix<4,0,UNSIGNED> id484x_1;

    id484out_result = (div_float(id484in_a,id484in_b,(&(id484x_1))));
    id484out_exception = (id484x_1);
  }
  HWFloat<11,45> id486out_result;
  HWOffsetFix<4,0,UNSIGNED> id486out_exception;

  { // Node ID: 486 (NodeSub)
    const HWFloat<11,45> &id486in_a = id910out_value;
    const HWFloat<11,45> &id486in_b = id484out_result;

    HWOffsetFix<4,0,UNSIGNED> id486x_1;

    id486out_result = (sub_float(id486in_a,id486in_b,(&(id486x_1))));
    id486out_exception = (id486x_1);
  }
  HWFloat<11,45> id489out_result;

  { // Node ID: 489 (NodeSub)
    const HWFloat<11,45> &id489in_a = id911out_value;
    const HWFloat<11,45> &id489in_b = id486out_result;

    id489out_result = (sub_float(id489in_a,id489in_b));
  }
  HWFloat<11,45> id490out_result;

  { // Node ID: 490 (NodeMul)
    const HWFloat<11,45> &id490in_a = id489out_result;
    const HWFloat<11,45> &id490in_b = id883out_output[getCycle()%10];

    id490out_result = (mul_float(id490in_a,id490in_b));
  }
  HWFloat<11,45> id491out_result;

  { // Node ID: 491 (NodeMul)
    const HWFloat<11,45> &id491in_a = id490out_result;
    const HWFloat<11,45> &id491in_b = id883out_output[getCycle()%10];

    id491out_result = (mul_float(id491in_a,id491in_b));
  }
  { // Node ID: 835 (NodePO2FPMult)
    const HWFloat<11,45> &id835in_floatIn = id491out_result;

    id835out_floatOut[(getCycle()+1)%2] = (mul_float(id835in_floatIn,(c_hw_flt_11_45_0_5val)));
  }
  HWFloat<11,45> id496out_result;

  { // Node ID: 496 (NodeMul)
    const HWFloat<11,45> &id496in_a = id495out_result;
    const HWFloat<11,45> &id496in_b = id835out_floatOut[getCycle()%2];

    id496out_result = (mul_float(id496in_a,id496in_b));
  }
  HWFloat<11,45> id829out_result;

  { // Node ID: 829 (NodeSub)
    const HWFloat<11,45> &id829in_a = id496out_result;
    const HWFloat<11,45> &id829in_b = id895out_output[getCycle()%2];

    id829out_result = (sub_float(id829in_a,id829in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<11,45> id503out_result;

  { // Node ID: 503 (NodeMul)
    const HWFloat<11,45> &id503in_a = id829out_result;
    const HWFloat<11,45> &id503in_b = id2out_value;

    id503out_result = (mul_float(id503in_a,id503in_b));
  }
  HWFloat<11,45> id505out_result;

  { // Node ID: 505 (NodeMul)
    const HWFloat<11,45> &id505in_a = id19out_o[getCycle()%4];
    const HWFloat<11,45> &id505in_b = id503out_result;

    id505out_result = (mul_float(id505in_a,id505in_b));
  }
  HWFloat<11,45> id506out_result;

  { // Node ID: 506 (NodeAdd)
    const HWFloat<11,45> &id506in_a = id895out_output[getCycle()%2];
    const HWFloat<11,45> &id506in_b = id505out_result;

    id506out_result = (add_float(id506in_a,id506in_b));
  }
  HWFloat<11,45> id867out_output;

  { // Node ID: 867 (NodeStreamOffset)
    const HWFloat<11,45> &id867in_input = id506out_result;

    id867out_output = id867in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id540out_result[getCycle()%2];
    const HWFloat<11,45> &id23in_option0 = id867out_output;
    const HWFloat<11,45> &id23in_option1 = id22out_value;

    HWFloat<11,45> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<11,45> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<11,45> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<11,45> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 834 (NodePO2FPMult)
    const HWFloat<11,45> &id834in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id834x_1;

    id834out_floatOut[(getCycle()+1)%2] = (mul_float(id834in_floatIn,(c_hw_flt_11_45_2_0val),(&(id834x_1))));
    id834out_exception[(getCycle()+1)%2] = (id834x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<11,45> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<11,45> &id42in_input = id834out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<11,45> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 839 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id839in_exceptionVector = id43out_exception[getCycle()%7];

    (id839st_reg) = (or_fixed((id839st_reg),id839in_exceptionVector));
    id839out_exceptionOccurred[(getCycle()+1)%2] = (id839st_reg);
  }
  { // Node ID: 887 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id887in_input = id839out_exceptionOccurred[getCycle()%2];

    id887out_output[(getCycle()+2)%3] = id887in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 840 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id840in_exceptionVector = id113out_exception;

    (id840st_reg) = (or_fixed((id840st_reg),id840in_exceptionVector));
    id840out_exceptionOccurred[(getCycle()+1)%2] = (id840st_reg);
  }
  { // Node ID: 888 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id888in_input = id840out_exceptionOccurred[getCycle()%2];

    id888out_output[(getCycle()+2)%3] = id888in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 841 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id841in_exceptionVector = id234out_exception;

    (id841st_reg) = (or_fixed((id841st_reg),id841in_exceptionVector));
    id841out_exceptionOccurred[(getCycle()+1)%2] = (id841st_reg);
  }
  { // Node ID: 889 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id889in_input = id841out_exceptionOccurred[getCycle()%2];

    id889out_output[(getCycle()+2)%3] = id889in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 843 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id843in_exceptionVector = id319out_exception;

    (id843st_reg) = (or_fixed((id843st_reg),id843in_exceptionVector));
    id843out_exceptionOccurred[(getCycle()+1)%2] = (id843st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 844 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id844in_exceptionVector = id324out_exception;

    (id844st_reg) = (or_fixed((id844st_reg),id844in_exceptionVector));
    id844out_exceptionOccurred[(getCycle()+1)%2] = (id844st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 845 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id845in_exceptionVector = id328out_exception;

    (id845st_reg) = (or_fixed((id845st_reg),id845in_exceptionVector));
    id845out_exceptionOccurred[(getCycle()+1)%2] = (id845st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 846 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id846in_exceptionVector = id332out_exception;

    (id846st_reg) = (or_fixed((id846st_reg),id846in_exceptionVector));
    id846out_exceptionOccurred[(getCycle()+1)%2] = (id846st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 847 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id847in_exceptionVector = id337out_exception;

    (id847st_reg) = (or_fixed((id847st_reg),id847in_exceptionVector));
    id847out_exceptionOccurred[(getCycle()+1)%2] = (id847st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 848 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id848in_exceptionVector = id341out_exception;

    (id848st_reg) = (or_fixed((id848st_reg),id848in_exceptionVector));
    id848out_exceptionOccurred[(getCycle()+1)%2] = (id848st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 849 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id849in_exceptionVector = id345out_exception;

    (id849st_reg) = (or_fixed((id849st_reg),id849in_exceptionVector));
    id849out_exceptionOccurred[(getCycle()+1)%2] = (id849st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 850 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id850in_exceptionVector = id350out_exception;

    (id850st_reg) = (or_fixed((id850st_reg),id850in_exceptionVector));
    id850out_exceptionOccurred[(getCycle()+1)%2] = (id850st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 851 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id851in_exceptionVector = id354out_exception;

    (id851st_reg) = (or_fixed((id851st_reg),id851in_exceptionVector));
    id851out_exceptionOccurred[(getCycle()+1)%2] = (id851st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 852 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id852in_exceptionVector = id358out_exception;

    (id852st_reg) = (or_fixed((id852st_reg),id852in_exceptionVector));
    id852out_exceptionOccurred[(getCycle()+1)%2] = (id852st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 853 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id853in_exceptionVector = id362out_exception;

    (id853st_reg) = (or_fixed((id853st_reg),id853in_exceptionVector));
    id853out_exceptionOccurred[(getCycle()+1)%2] = (id853st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 856 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id856in_exceptionVector = id370out_exception;

    (id856st_reg) = (or_fixed((id856st_reg),id856in_exceptionVector));
    id856out_exceptionOccurred[(getCycle()+1)%2] = (id856st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 842 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id842in_exceptionVector = id376out_exception;

    (id842st_reg) = (or_fixed((id842st_reg),id842in_exceptionVector));
    id842out_exceptionOccurred[(getCycle()+1)%2] = (id842st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 855 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id855in_exceptionVector = id426out_exception;

    (id855st_reg) = (or_fixed((id855st_reg),id855in_exceptionVector));
    id855out_exceptionOccurred[(getCycle()+1)%2] = (id855st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 857 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id857in_exceptionVector = id482out_exception;

    (id857st_reg) = (or_fixed((id857st_reg),id857in_exceptionVector));
    id857out_exceptionOccurred[(getCycle()+1)%2] = (id857st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 858 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id858in_exceptionVector = id484out_exception;

    (id858st_reg) = (or_fixed((id858st_reg),id858in_exceptionVector));
    id858out_exceptionOccurred[(getCycle()+1)%2] = (id858st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 859 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id859in_exceptionVector = id486out_exception;

    (id859st_reg) = (or_fixed((id859st_reg),id859in_exceptionVector));
    id859out_exceptionOccurred[(getCycle()+1)%2] = (id859st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 854 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id854in_exceptionVector = id833out_exception;

    (id854st_reg) = (or_fixed((id854st_reg),id854in_exceptionVector));
    id854out_exceptionOccurred[(getCycle()+1)%2] = (id854st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 838 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id838in_exceptionVector = id834out_exception[getCycle()%2];

    (id838st_reg) = (or_fixed((id838st_reg),id838in_exceptionVector));
    id838out_exceptionOccurred[(getCycle()+1)%2] = (id838st_reg);
  }
  { // Node ID: 890 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id890in_input = id838out_exceptionOccurred[getCycle()%2];

    id890out_output[(getCycle()+8)%9] = id890in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 860 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id860in_n43 = id887out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n113 = id888out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n234 = id889out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n319 = id843out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n324 = id844out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n328 = id845out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n332 = id846out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n337 = id847out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n341 = id848out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n345 = id849out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n350 = id850out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n354 = id851out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n358 = id852out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n362 = id853out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n370 = id856out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n376 = id842out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n426 = id855out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id860in_n482 = id857out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id860in_n484 = id858out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id860in_n486 = id859out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_n833 = id854out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id860in_n834 = id890out_output[getCycle()%9];

    id860out_all_exceptions = (cat((cat((cat((cat((cat(id860in_n43,id860in_n113)),id860in_n234)),(cat((cat(id860in_n319,id860in_n324)),id860in_n328)))),(cat((cat((cat(id860in_n332,id860in_n337)),id860in_n341)),(cat(id860in_n345,id860in_n350)))))),(cat((cat((cat((cat(id860in_n354,id860in_n358)),id860in_n362)),(cat((cat(id860in_n370,id860in_n376)),id860in_n426)))),(cat((cat((cat(id860in_n482,id860in_n484)),id860in_n486)),(cat(id860in_n833,id860in_n834))))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 861 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id861in_load = id862out_value;
    const HWRawBits<37> &id861in_data = id860out_all_exceptions;

    bool id861x_1;

    (id861x_1) = ((id861in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id861x_1)) {
      setMappedRegValue("all_numeric_exceptions", id861in_data);
    }
  }
  { // Node ID: 836 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 837 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id837in_output_control = id836out_value;
    const HWFloat<11,45> &id837in_data = id867out_output;

    bool id837x_1;

    (id837x_1) = ((id837in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id837x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id837in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 891 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id891in_input = id17out_count;

    id891out_output[(getCycle()+1)%2] = id891in_input;
  }
  { // Node ID: 900 (NodeConstantRawBits)
  }
  { // Node ID: 514 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id514in_a = id896out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id514in_b = id900out_value;

    id514out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id514in_a,id514in_b));
  }
  { // Node ID: 830 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id830in_a = id891out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id830in_b = id514out_result[getCycle()%2];

    id830out_result[(getCycle()+1)%2] = (eq_fixed(id830in_a,id830in_b));
  }
  { // Node ID: 516 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id517out_result;

  { // Node ID: 517 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id517in_a = id516out_io_u_out_force_disabled;

    id517out_result = (not_fixed(id517in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id518out_result;

  { // Node ID: 518 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id518in_a = id830out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id518in_b = id517out_result;

    HWOffsetFix<1,0,UNSIGNED> id518x_1;

    (id518x_1) = (and_fixed(id518in_a,id518in_b));
    id518out_result = (id518x_1);
  }
  { // Node ID: 892 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id892in_input = id518out_result;

    id892out_output[(getCycle()+10)%11] = id892in_input;
  }
  HWFloat<11,53> id511out_o;

  { // Node ID: 511 (NodeCast)
    const HWFloat<11,45> &id511in_i = id506out_result;

    id511out_o = (cast_float2float<11,53>(id511in_i));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 519 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id519in_output_control = id892out_output[getCycle()%11];
    const HWFloat<11,53> &id519in_data = id511out_o;

    bool id519x_1;

    (id519x_1) = ((id519in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id519x_1)) {
      writeOutput(m_u_out, id519in_data);
    }
  }
  { // Node ID: 899 (NodeConstantRawBits)
  }
  { // Node ID: 521 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id521in_a = id896out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id521in_b = id899out_value;

    id521out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id521in_a,id521in_b));
  }
  { // Node ID: 831 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id831in_a = id891out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id831in_b = id521out_result[getCycle()%2];

    id831out_result[(getCycle()+1)%2] = (eq_fixed(id831in_a,id831in_b));
  }
  { // Node ID: 523 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id524out_result;

  { // Node ID: 524 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id524in_a = id523out_io_v_out_force_disabled;

    id524out_result = (not_fixed(id524in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id525out_result;

  { // Node ID: 525 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id525in_a = id831out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id525in_b = id524out_result;

    HWOffsetFix<1,0,UNSIGNED> id525x_1;

    (id525x_1) = (and_fixed(id525in_a,id525in_b));
    id525out_result = (id525x_1);
  }
  { // Node ID: 894 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id894in_input = id525out_result;

    id894out_output[(getCycle()+10)%11] = id894in_input;
  }
  HWFloat<11,53> id512out_o;

  { // Node ID: 512 (NodeCast)
    const HWFloat<11,45> &id512in_i = id508out_result;

    id512out_o = (cast_float2float<11,53>(id512in_i));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 526 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id526in_output_control = id894out_output[getCycle()%11];
    const HWFloat<11,53> &id526in_data = id512out_o;

    bool id526x_1;

    (id526x_1) = ((id526in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id526x_1)) {
      writeOutput(m_v_out, id526in_data);
    }
  }
  { // Node ID: 531 (NodeConstantRawBits)
  }
  { // Node ID: 898 (NodeConstantRawBits)
  }
  { // Node ID: 528 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 529 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id529in_enable = id898out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id529in_max = id528out_value;

    HWOffsetFix<49,0,UNSIGNED> id529x_1;
    HWOffsetFix<1,0,UNSIGNED> id529x_2;
    HWOffsetFix<1,0,UNSIGNED> id529x_3;
    HWOffsetFix<49,0,UNSIGNED> id529x_4t_1e_1;

    id529out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id529st_count)));
    (id529x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id529st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id529x_2) = (gte_fixed((id529x_1),id529in_max));
    (id529x_3) = (and_fixed((id529x_2),id529in_enable));
    id529out_wrap = (id529x_3);
    if((id529in_enable.getValueAsBool())) {
      if(((id529x_3).getValueAsBool())) {
        (id529st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id529x_4t_1e_1) = (id529x_1);
        (id529st_count) = (id529x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id530out_output;

  { // Node ID: 530 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id530in_input = id529out_count;

    id530out_output = id530in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 532 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id532in_load = id531out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id532in_data = id530out_output;

    bool id532x_1;

    (id532x_1) = ((id532in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id532x_1)) {
      setMappedRegValue("current_run_cycle_count", id532in_data);
    }
  }
  { // Node ID: 897 (NodeConstantRawBits)
  }
  { // Node ID: 534 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 535 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id535in_enable = id897out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id535in_max = id534out_value;

    HWOffsetFix<49,0,UNSIGNED> id535x_1;
    HWOffsetFix<1,0,UNSIGNED> id535x_2;
    HWOffsetFix<1,0,UNSIGNED> id535x_3;
    HWOffsetFix<49,0,UNSIGNED> id535x_4t_1e_1;

    id535out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id535st_count)));
    (id535x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id535st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id535x_2) = (gte_fixed((id535x_1),id535in_max));
    (id535x_3) = (and_fixed((id535x_2),id535in_enable));
    id535out_wrap = (id535x_3);
    if((id535in_enable.getValueAsBool())) {
      if(((id535x_3).getValueAsBool())) {
        (id535st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id535x_4t_1e_1) = (id535x_1);
        (id535st_count) = (id535x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 537 (NodeInputMappedReg)
  }
  { // Node ID: 832 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id832in_a = id535out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id832in_b = id537out_run_cycle_count;

    id832out_result[(getCycle()+1)%2] = (eq_fixed(id832in_a,id832in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 536 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id536in_start = id832out_result[getCycle()%2];

    if((id536in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
