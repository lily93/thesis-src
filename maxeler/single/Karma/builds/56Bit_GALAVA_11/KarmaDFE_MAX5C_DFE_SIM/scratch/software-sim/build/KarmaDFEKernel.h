#ifndef KARMADFEKERNEL_H_
#define KARMADFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaDFEKernel : public KernelManagerBlockSync {
public:
  KarmaDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id862out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id896out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id914out_value;

  HWOffsetFix<1,0,UNSIGNED> id540out_result[2];

  HWFloat<11,45> id883out_output[10];

  HWFloat<11,45> id895out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<11,45> id19out_o[4];

  HWFloat<11,45> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id913out_value;

  HWOffsetFix<1,0,UNSIGNED> id541out_result[2];

  HWFloat<11,45> id912out_value;

  HWFloat<11,45> id5out_value;

  HWFloat<11,45> id3out_value;

  HWFloat<11,45> id26out_value;

  HWFloat<11,45> id27out_result[2];

  HWFloat<11,45> id869out_output[11];

  HWFloat<11,45> id911out_value;

  HWFloat<11,45> id910out_value;

  HWFloat<11,45> id909out_value;

  HWRawBits<11> id465out_value;

  HWRawBits<44> id908out_value;

  HWOffsetFix<1,0,UNSIGNED> id882out_output[9];

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<52,-51,UNSIGNED> id114out_value;

  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id116out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id236out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id874out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id907out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id378out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id397out_value;

  HWOffsetFix<32,-45,UNSIGNED> id480out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id480sta_rom_store[1024];

  HWOffsetFix<42,-45,UNSIGNED> id477out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id477sta_rom_store[1024];

  HWOffsetFix<45,-45,UNSIGNED> id474out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id474sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id318out_value;

  HWOffsetFix<25,-48,UNSIGNED> id875out_output[3];

  HWOffsetFix<25,-48,UNSIGNED> id321out_value;

  HWOffsetFix<64,-87,UNSIGNED> id326out_value;

  HWOffsetFix<64,-63,UNSIGNED> id330out_value;

  HWOffsetFix<49,-48,UNSIGNED> id334out_value;

  HWOffsetFix<64,-66,UNSIGNED> id339out_value;

  HWOffsetFix<64,-62,UNSIGNED> id343out_value;

  HWOffsetFix<50,-48,UNSIGNED> id347out_value;

  HWOffsetFix<64,-75,UNSIGNED> id352out_value;

  HWOffsetFix<64,-61,UNSIGNED> id356out_value;

  HWOffsetFix<51,-48,UNSIGNED> id360out_value;

  HWOffsetFix<45,-44,UNSIGNED> id364out_value;

  HWOffsetFix<45,-44,UNSIGNED> id906out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id402out_value;

  HWFloat<11,45> id905out_value;

  HWOffsetFix<1,0,UNSIGNED> id876out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id877out_output[3];

  HWFloat<11,45> id904out_value;

  HWOffsetFix<1,0,UNSIGNED> id878out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id879out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id903out_value;

  HWOffsetFix<1,0,UNSIGNED> id444out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id428out_value;

  HWOffsetFix<45,-44,UNSIGNED> id368out_value;

  HWOffsetFix<44,-44,UNSIGNED> id372out_value;

  HWOffsetFix<1,0,UNSIGNED> id453out_value;

  HWOffsetFix<11,0,UNSIGNED> id454out_value;

  HWOffsetFix<44,0,UNSIGNED> id456out_value;

  HWFloat<11,45> id539out_value;

  HWFloat<11,45> id902out_value;

  HWFloat<11,45> id901out_value;

  HWFloat<11,45> id835out_floatOut[2];

  HWFloat<11,45> id2out_value;

  HWFloat<11,45> id22out_value;

  HWFloat<11,45> id23out_result[2];

  HWFloat<11,45> id4out_value;

  HWFloat<11,45> id834out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id834out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id839out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id839st_reg;

  HWOffsetFix<4,0,UNSIGNED> id887out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id840out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id840st_reg;

  HWOffsetFix<1,0,UNSIGNED> id888out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id841out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id841st_reg;

  HWOffsetFix<1,0,UNSIGNED> id889out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id843out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id843st_reg;

  HWOffsetFix<1,0,UNSIGNED> id844out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id844st_reg;

  HWOffsetFix<1,0,UNSIGNED> id845out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id845st_reg;

  HWOffsetFix<1,0,UNSIGNED> id846out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id846st_reg;

  HWOffsetFix<1,0,UNSIGNED> id847out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id847st_reg;

  HWOffsetFix<1,0,UNSIGNED> id848out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id848st_reg;

  HWOffsetFix<1,0,UNSIGNED> id849out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id849st_reg;

  HWOffsetFix<1,0,UNSIGNED> id850out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id850st_reg;

  HWOffsetFix<1,0,UNSIGNED> id851out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id851st_reg;

  HWOffsetFix<1,0,UNSIGNED> id852out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id852st_reg;

  HWOffsetFix<1,0,UNSIGNED> id853out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id853st_reg;

  HWOffsetFix<1,0,UNSIGNED> id856out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id856st_reg;

  HWOffsetFix<1,0,UNSIGNED> id842out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id842st_reg;

  HWOffsetFix<1,0,UNSIGNED> id855out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id855st_reg;

  HWOffsetFix<4,0,UNSIGNED> id857out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id857st_reg;

  HWOffsetFix<4,0,UNSIGNED> id858out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id858st_reg;

  HWOffsetFix<4,0,UNSIGNED> id859out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id859st_reg;

  HWOffsetFix<1,0,UNSIGNED> id854out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id854st_reg;

  HWOffsetFix<4,0,UNSIGNED> id838out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id838st_reg;

  HWOffsetFix<4,0,UNSIGNED> id890out_output[9];

  HWRawBits<37> id860out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id836out_value;

  HWOffsetFix<11,0,UNSIGNED> id891out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id900out_value;

  HWOffsetFix<11,0,UNSIGNED> id514out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id830out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id516out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id892out_output[11];

  HWOffsetFix<11,0,UNSIGNED> id899out_value;

  HWOffsetFix<11,0,UNSIGNED> id521out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id831out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id523out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id894out_output[11];

  HWOffsetFix<1,0,UNSIGNED> id531out_value;

  HWOffsetFix<1,0,UNSIGNED> id898out_value;

  HWOffsetFix<49,0,UNSIGNED> id528out_value;

  HWOffsetFix<48,0,UNSIGNED> id529out_count;
  HWOffsetFix<1,0,UNSIGNED> id529out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id529st_count;

  HWOffsetFix<1,0,UNSIGNED> id897out_value;

  HWOffsetFix<49,0,UNSIGNED> id534out_value;

  HWOffsetFix<48,0,UNSIGNED> id535out_count;
  HWOffsetFix<1,0,UNSIGNED> id535out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id535st_count;

  HWOffsetFix<48,0,UNSIGNED> id537out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id832out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<11,45> c_hw_flt_11_45_undef;
  const HWFloat<11,45> c_hw_flt_11_45_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits_1;
  const HWFloat<11,45> c_hw_flt_11_45_bits_2;
  const HWFloat<11,45> c_hw_flt_11_45_bits_3;
  const HWFloat<11,45> c_hw_flt_11_45_bits_4;
  const HWFloat<11,45> c_hw_flt_11_45_bits_5;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<44> c_hw_bit_44_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<61,-48,TWOSCOMPLEMENT> c_hw_fix_61_n48_sgn_bits;
  const HWOffsetFix<61,-48,TWOSCOMPLEMENT> c_hw_fix_61_n48_sgn_undef;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<113,-99,TWOSCOMPLEMENT> c_hw_fix_113_n99_sgn_bits;
  const HWOffsetFix<113,-99,TWOSCOMPLEMENT> c_hw_fix_113_n99_sgn_undef;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<32,-45,UNSIGNED> c_hw_fix_32_n45_uns_undef;
  const HWOffsetFix<42,-45,UNSIGNED> c_hw_fix_42_n45_uns_undef;
  const HWOffsetFix<45,-45,UNSIGNED> c_hw_fix_45_n45_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<25,-48,UNSIGNED> c_hw_fix_25_n48_uns_undef;
  const HWOffsetFix<25,-48,UNSIGNED> c_hw_fix_25_n48_uns_bits;
  const HWOffsetFix<64,-87,UNSIGNED> c_hw_fix_64_n87_uns_bits;
  const HWOffsetFix<64,-87,UNSIGNED> c_hw_fix_64_n87_uns_undef;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_bits;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_undef;
  const HWOffsetFix<49,-48,UNSIGNED> c_hw_fix_49_n48_uns_bits;
  const HWOffsetFix<49,-48,UNSIGNED> c_hw_fix_49_n48_uns_undef;
  const HWOffsetFix<64,-66,UNSIGNED> c_hw_fix_64_n66_uns_bits;
  const HWOffsetFix<64,-66,UNSIGNED> c_hw_fix_64_n66_uns_undef;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_bits;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_undef;
  const HWOffsetFix<50,-48,UNSIGNED> c_hw_fix_50_n48_uns_bits;
  const HWOffsetFix<50,-48,UNSIGNED> c_hw_fix_50_n48_uns_undef;
  const HWOffsetFix<64,-75,UNSIGNED> c_hw_fix_64_n75_uns_bits;
  const HWOffsetFix<64,-75,UNSIGNED> c_hw_fix_64_n75_uns_undef;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_bits;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_undef;
  const HWOffsetFix<51,-48,UNSIGNED> c_hw_fix_51_n48_uns_bits;
  const HWOffsetFix<51,-48,UNSIGNED> c_hw_fix_51_n48_uns_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits_1;
  const HWOffsetFix<15,0,TWOSCOMPLEMENT> c_hw_fix_15_0_sgn_bits;
  const HWOffsetFix<15,0,TWOSCOMPLEMENT> c_hw_fix_15_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits_2;
  const HWOffsetFix<44,-44,UNSIGNED> c_hw_fix_44_n44_uns_bits;
  const HWOffsetFix<44,-44,UNSIGNED> c_hw_fix_44_n44_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<44,0,UNSIGNED> c_hw_fix_44_0_uns_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits_6;
  const HWFloat<11,45> c_hw_flt_11_45_0_5val;
  const HWFloat<11,45> c_hw_flt_11_45_bits_7;
  const HWFloat<11,45> c_hw_flt_11_45_bits_8;
  const HWFloat<11,45> c_hw_flt_11_45_bits_9;
  const HWFloat<11,45> c_hw_flt_11_45_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMADFEKERNEL_H_ */
