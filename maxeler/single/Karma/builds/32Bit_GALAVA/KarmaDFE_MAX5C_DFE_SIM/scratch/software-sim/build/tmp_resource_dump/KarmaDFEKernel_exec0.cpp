#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaDFEKernel::execute0() {
  { // Node ID: 632 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 666 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id666out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 684 (NodeConstantRawBits)
  }
  { // Node ID: 416 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id416in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id416in_b = id684out_value;

    id416out_result[(getCycle()+1)%2] = (eq_fixed(id416in_a,id416in_b));
  }
  { // Node ID: 653 (NodeFIFO)
    const HWFloat<8,24> &id653in_input = id23out_result[getCycle()%2];

    id653out_output[(getCycle()+9)%10] = id653in_input;
  }
  { // Node ID: 665 (NodeFIFO)
    const HWFloat<8,24> &id665in_input = id653out_output[getCycle()%10];

    id665out_output[(getCycle()+1)%2] = id665in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 19 (NodeCast)
    const HWFloat<11,53> &id19in_i = id18out_dt;

    id19out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id19in_i));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 683 (NodeConstantRawBits)
  }
  { // Node ID: 417 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id417in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id417in_b = id683out_value;

    id417out_result[(getCycle()+1)%2] = (eq_fixed(id417in_a,id417in_b));
  }
  { // Node ID: 682 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id374out_result;

  { // Node ID: 374 (NodeGt)
    const HWFloat<8,24> &id374in_a = id665out_output[getCycle()%2];
    const HWFloat<8,24> &id374in_b = id5out_value;

    id374out_result = (gt_float(id374in_a,id374in_b));
  }
  HWFloat<8,24> id375out_o;

  { // Node ID: 375 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id375in_i = id374out_result;

    id375out_o = (cast_fixed2float<8,24>(id375in_i));
  }
  HWFloat<8,24> id377out_result;

  { // Node ID: 377 (NodeMul)
    const HWFloat<8,24> &id377in_a = id682out_value;
    const HWFloat<8,24> &id377in_b = id375out_o;

    id377out_result = (mul_float(id377in_a,id377in_b));
  }
  HWFloat<8,24> id378out_result;

  { // Node ID: 378 (NodeSub)
    const HWFloat<8,24> &id378in_a = id377out_result;
    const HWFloat<8,24> &id378in_b = id639out_output[getCycle()%11];

    id378out_result = (sub_float(id378in_a,id378in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,24> id380out_result;

  { // Node ID: 380 (NodeMul)
    const HWFloat<8,24> &id380in_a = id378out_result;
    const HWFloat<8,24> &id380in_b = id3out_value;

    id380out_result = (mul_float(id380in_a,id380in_b));
  }
  HWFloat<8,24> id383out_result;

  { // Node ID: 383 (NodeMul)
    const HWFloat<8,24> &id383in_a = id19out_o[getCycle()%4];
    const HWFloat<8,24> &id383in_b = id380out_result;

    id383out_result = (mul_float(id383in_a,id383in_b));
  }
  HWFloat<8,24> id384out_result;

  { // Node ID: 384 (NodeAdd)
    const HWFloat<8,24> &id384in_a = id639out_output[getCycle()%11];
    const HWFloat<8,24> &id384in_b = id383out_result;

    id384out_result = (add_float(id384in_a,id384in_b));
  }
  HWFloat<8,24> id636out_output;

  { // Node ID: 636 (NodeStreamOffset)
    const HWFloat<8,24> &id636in_input = id384out_result;

    id636out_output = id636in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id417out_result[getCycle()%2];
    const HWFloat<8,24> &id27in_option0 = id636out_output;
    const HWFloat<8,24> &id27in_option1 = id26out_value;

    HWFloat<8,24> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 639 (NodeFIFO)
    const HWFloat<8,24> &id639in_input = id27out_result[getCycle()%2];

    id639out_output[(getCycle()+10)%11] = id639in_input;
  }
  HWFloat<8,24> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<8,24> &id32in_a = id639out_output[getCycle()%11];
    const HWFloat<8,24> &id32in_b = id639out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<8,24> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<8,24> &id33in_a = id639out_output[getCycle()%11];
    const HWFloat<8,24> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<8,24> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<8,24> &id34in_a = id33out_result;
    const HWFloat<8,24> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<8,24> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<8,24> &id35in_a = id34out_result;
    const HWFloat<8,24> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<8,24> id371out_result;

  { // Node ID: 371 (NodeSub)
    const HWFloat<8,24> &id371in_a = id6out_value;
    const HWFloat<8,24> &id371in_b = id35out_result;

    id371out_result = (sub_float(id371in_a,id371in_b));
  }
  { // Node ID: 681 (NodeConstantRawBits)
  }
  { // Node ID: 680 (NodeConstantRawBits)
  }
  { // Node ID: 679 (NodeConstantRawBits)
  }
  HWRawBits<8> id343out_result;

  { // Node ID: 343 (NodeSlice)
    const HWFloat<8,24> &id343in_a = id609out_floatOut[getCycle()%2];

    id343out_result = (slice<23,8>(id343in_a));
  }
  { // Node ID: 344 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id418out_result;

  { // Node ID: 418 (NodeEqInlined)
    const HWRawBits<8> &id418in_a = id343out_result;
    const HWRawBits<8> &id418in_b = id344out_value;

    id418out_result = (eq_bits(id418in_a,id418in_b));
  }
  HWRawBits<23> id342out_result;

  { // Node ID: 342 (NodeSlice)
    const HWFloat<8,24> &id342in_a = id609out_floatOut[getCycle()%2];

    id342out_result = (slice<0,23>(id342in_a));
  }
  { // Node ID: 678 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id419out_result;

  { // Node ID: 419 (NodeNeqInlined)
    const HWRawBits<23> &id419in_a = id342out_result;
    const HWRawBits<23> &id419in_b = id678out_value;

    id419out_result = (neq_bits(id419in_a,id419in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id348out_result;

  { // Node ID: 348 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id348in_a = id418out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id348in_b = id419out_result;

    HWOffsetFix<1,0,UNSIGNED> id348x_1;

    (id348x_1) = (and_fixed(id348in_a,id348in_b));
    id348out_result = (id348x_1);
  }
  { // Node ID: 652 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id652in_input = id348out_result;

    id652out_output[(getCycle()+8)%9] = id652in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id420out_result;
  HWOffsetFix<1,0,UNSIGNED> id420out_result_doubt;

  { // Node ID: 420 (NodeSlice)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id420in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id420in_a_doubt = id43out_o_doubt[getCycle()%7];

    id420out_result = (slice<35,1>(id420in_a));
    id420out_result_doubt = id420in_a_doubt;
  }
  HWRawBits<1> id421out_result;
  HWOffsetFix<1,0,UNSIGNED> id421out_result_doubt;

  { // Node ID: 421 (NodeNot)
    const HWRawBits<1> &id421in_a = id420out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id421in_a_doubt = id420out_result_doubt;

    id421out_result = (not_bits(id421in_a));
    id421out_result_doubt = id421in_a_doubt;
  }
  HWRawBits<36> id456out_result;
  HWOffsetFix<1,0,UNSIGNED> id456out_result_doubt;

  { // Node ID: 456 (NodeCat)
    const HWRawBits<1> &id456in_in0 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in0_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in1 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in1_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in2 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in2_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in3 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in3_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in4 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in4_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in5 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in5_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in6 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in6_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in7 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in7_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in8 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in8_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in9 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in9_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in10 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in10_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in11 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in11_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in12 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in12_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in13 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in13_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in14 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in14_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in15 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in15_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in16 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in16_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in17 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in17_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in18 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in18_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in19 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in19_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in20 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in20_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in21 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in21_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in22 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in22_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in23 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in23_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in24 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in24_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in25 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in25_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in26 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in26_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in27 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in27_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in28 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in28_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in29 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in29_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in30 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in30_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in31 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in31_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in32 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in32_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in33 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in33_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in34 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in34_doubt = id421out_result_doubt;
    const HWRawBits<1> &id456in_in35 = id421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id456in_in35_doubt = id421out_result_doubt;

    id456out_result = (cat((cat((cat((cat((cat((cat(id456in_in0,id456in_in1)),id456in_in2)),(cat(id456in_in3,id456in_in4)))),(cat((cat(id456in_in5,id456in_in6)),(cat(id456in_in7,id456in_in8)))))),(cat((cat((cat((cat(id456in_in9,id456in_in10)),id456in_in11)),(cat(id456in_in12,id456in_in13)))),(cat((cat(id456in_in14,id456in_in15)),(cat(id456in_in16,id456in_in17)))))))),(cat((cat((cat((cat((cat(id456in_in18,id456in_in19)),id456in_in20)),(cat(id456in_in21,id456in_in22)))),(cat((cat(id456in_in23,id456in_in24)),(cat(id456in_in25,id456in_in26)))))),(cat((cat((cat((cat(id456in_in27,id456in_in28)),id456in_in29)),(cat(id456in_in30,id456in_in31)))),(cat((cat(id456in_in32,id456in_in33)),(cat(id456in_in34,id456in_in35))))))))));
    id456out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id456in_in0_doubt,id456in_in1_doubt)),id456in_in2_doubt)),id456in_in3_doubt)),id456in_in4_doubt)),id456in_in5_doubt)),id456in_in6_doubt)),id456in_in7_doubt)),id456in_in8_doubt)),id456in_in9_doubt)),id456in_in10_doubt)),id456in_in11_doubt)),id456in_in12_doubt)),id456in_in13_doubt)),id456in_in14_doubt)),id456in_in15_doubt)),id456in_in16_doubt)),id456in_in17_doubt)),id456in_in18_doubt)),id456in_in19_doubt)),id456in_in20_doubt)),id456in_in21_doubt)),id456in_in22_doubt)),id456in_in23_doubt)),id456in_in24_doubt)),id456in_in25_doubt)),id456in_in26_doubt)),id456in_in27_doubt)),id456in_in28_doubt)),id456in_in29_doubt)),id456in_in30_doubt)),id456in_in31_doubt)),id456in_in32_doubt)),id456in_in33_doubt)),id456in_in34_doubt)),id456in_in35_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id84out_output;
  HWOffsetFix<1,0,UNSIGNED> id84out_output_doubt;

  { // Node ID: 84 (NodeReinterpret)
    const HWRawBits<36> &id84in_input = id456out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id84in_input_doubt = id456out_result_doubt;

    id84out_output = (cast_bits2fixed<36,-26,TWOSCOMPLEMENT>(id84in_input));
    id84out_output_doubt = id84in_input_doubt;
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id85out_result;
  HWOffsetFix<1,0,UNSIGNED> id85out_result_doubt;

  { // Node ID: 85 (NodeXor)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id85in_a = id46out_value;
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id85in_b = id84out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id85in_b_doubt = id84out_output_doubt;

    HWOffsetFix<36,-26,TWOSCOMPLEMENT> id85x_1;

    (id85x_1) = (xor_fixed(id85in_a,id85in_b));
    id85out_result = (id85x_1);
    id85out_result_doubt = id85in_b_doubt;
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id86out_result;
  HWOffsetFix<1,0,UNSIGNED> id86out_result_doubt;

  { // Node ID: 86 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id86in_sel = id45out_output;
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id86in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id86in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id86in_option1 = id85out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id86in_option1_doubt = id85out_result_doubt;

    HWOffsetFix<36,-26,TWOSCOMPLEMENT> id86x_1;
    HWOffsetFix<1,0,UNSIGNED> id86x_2;

    switch((id86in_sel.getValueAsLong())) {
      case 0l:
        id86x_1 = id86in_option0;
        break;
      case 1l:
        id86x_1 = id86in_option1;
        break;
      default:
        id86x_1 = (c_hw_fix_36_n26_sgn_undef);
        break;
    }
    switch((id86in_sel.getValueAsLong())) {
      case 0l:
        id86x_2 = id86in_option0_doubt;
        break;
      case 1l:
        id86x_2 = id86in_option1_doubt;
        break;
      default:
        id86x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id86out_result = (id86x_1);
    id86out_result_doubt = (id86x_2);
  }
  { // Node ID: 89 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id88out_result;
  HWOffsetFix<1,0,UNSIGNED> id88out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id88out_exception;

  { // Node ID: 88 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id88in_a = id86out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id88in_a_doubt = id86out_result_doubt;
    const HWOffsetFix<35,-34,UNSIGNED> &id88in_b = id89out_value;

    HWOffsetFix<1,0,UNSIGNED> id88x_1;

    id88out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id88in_a,id88in_b,(&(id88x_1))));
    id88out_result_doubt = (or_fixed((neq_fixed((id88x_1),(c_hw_fix_1_0_uns_bits_1))),id88in_a_doubt));
    id88out_exception = (id88x_1);
  }
  { // Node ID: 91 (NodeConstantRawBits)
  }
  HWRawBits<1> id457out_result;
  HWOffsetFix<1,0,UNSIGNED> id457out_result_doubt;

  { // Node ID: 457 (NodeSlice)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id457in_a = id88out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_a_doubt = id88out_result_doubt;

    id457out_result = (slice<70,1>(id457in_a));
    id457out_result_doubt = id457in_a_doubt;
  }
  HWRawBits<1> id458out_result;
  HWOffsetFix<1,0,UNSIGNED> id458out_result_doubt;

  { // Node ID: 458 (NodeNot)
    const HWRawBits<1> &id458in_a = id457out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id458in_a_doubt = id457out_result_doubt;

    id458out_result = (not_bits(id458in_a));
    id458out_result_doubt = id458in_a_doubt;
  }
  HWRawBits<71> id528out_result;
  HWOffsetFix<1,0,UNSIGNED> id528out_result_doubt;

  { // Node ID: 528 (NodeCat)
    const HWRawBits<1> &id528in_in0 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in0_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in1 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in1_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in2 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in2_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in3 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in3_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in4 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in4_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in5 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in5_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in6 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in6_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in7 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in7_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in8 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in8_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in9 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in9_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in10 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in10_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in11 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in11_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in12 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in12_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in13 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in13_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in14 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in14_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in15 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in15_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in16 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in16_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in17 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in17_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in18 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in18_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in19 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in19_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in20 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in20_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in21 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in21_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in22 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in22_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in23 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in23_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in24 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in24_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in25 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in25_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in26 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in26_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in27 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in27_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in28 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in28_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in29 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in29_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in30 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in30_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in31 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in31_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in32 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in32_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in33 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in33_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in34 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in34_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in35 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in35_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in36 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in36_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in37 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in37_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in38 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in38_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in39 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in39_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in40 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in40_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in41 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in41_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in42 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in42_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in43 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in43_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in44 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in44_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in45 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in45_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in46 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in46_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in47 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in47_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in48 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in48_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in49 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in49_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in50 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in50_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in51 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in51_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in52 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in52_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in53 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in53_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in54 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in54_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in55 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in55_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in56 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in56_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in57 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in57_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in58 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in58_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in59 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in59_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in60 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in60_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in61 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in61_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in62 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in62_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in63 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in63_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in64 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in64_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in65 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in65_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in66 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in66_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in67 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in67_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in68 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in68_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in69 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in69_doubt = id458out_result_doubt;
    const HWRawBits<1> &id528in_in70 = id458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in70_doubt = id458out_result_doubt;

    id528out_result = (cat((cat((cat((cat((cat((cat((cat(id528in_in0,id528in_in1)),id528in_in2)),(cat(id528in_in3,id528in_in4)))),(cat((cat(id528in_in5,id528in_in6)),(cat(id528in_in7,id528in_in8)))))),(cat((cat((cat((cat(id528in_in9,id528in_in10)),id528in_in11)),(cat(id528in_in12,id528in_in13)))),(cat((cat(id528in_in14,id528in_in15)),(cat(id528in_in16,id528in_in17)))))))),(cat((cat((cat((cat((cat(id528in_in18,id528in_in19)),id528in_in20)),(cat(id528in_in21,id528in_in22)))),(cat((cat(id528in_in23,id528in_in24)),(cat(id528in_in25,id528in_in26)))))),(cat((cat((cat((cat(id528in_in27,id528in_in28)),id528in_in29)),(cat(id528in_in30,id528in_in31)))),(cat((cat(id528in_in32,id528in_in33)),(cat(id528in_in34,id528in_in35)))))))))),(cat((cat((cat((cat((cat((cat(id528in_in36,id528in_in37)),id528in_in38)),(cat(id528in_in39,id528in_in40)))),(cat((cat(id528in_in41,id528in_in42)),(cat(id528in_in43,id528in_in44)))))),(cat((cat((cat((cat(id528in_in45,id528in_in46)),id528in_in47)),(cat(id528in_in48,id528in_in49)))),(cat((cat(id528in_in50,id528in_in51)),(cat(id528in_in52,id528in_in53)))))))),(cat((cat((cat((cat((cat(id528in_in54,id528in_in55)),id528in_in56)),(cat(id528in_in57,id528in_in58)))),(cat((cat(id528in_in59,id528in_in60)),(cat(id528in_in61,id528in_in62)))))),(cat((cat((cat(id528in_in63,id528in_in64)),(cat(id528in_in65,id528in_in66)))),(cat((cat(id528in_in67,id528in_in68)),(cat(id528in_in69,id528in_in70))))))))))));
    id528out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id528in_in0_doubt,id528in_in1_doubt)),id528in_in2_doubt)),id528in_in3_doubt)),id528in_in4_doubt)),id528in_in5_doubt)),id528in_in6_doubt)),id528in_in7_doubt)),id528in_in8_doubt)),id528in_in9_doubt)),id528in_in10_doubt)),id528in_in11_doubt)),id528in_in12_doubt)),id528in_in13_doubt)),id528in_in14_doubt)),id528in_in15_doubt)),id528in_in16_doubt)),id528in_in17_doubt)),id528in_in18_doubt)),id528in_in19_doubt)),id528in_in20_doubt)),id528in_in21_doubt)),id528in_in22_doubt)),id528in_in23_doubt)),id528in_in24_doubt)),id528in_in25_doubt)),id528in_in26_doubt)),id528in_in27_doubt)),id528in_in28_doubt)),id528in_in29_doubt)),id528in_in30_doubt)),id528in_in31_doubt)),id528in_in32_doubt)),id528in_in33_doubt)),id528in_in34_doubt)),id528in_in35_doubt)),id528in_in36_doubt)),id528in_in37_doubt)),id528in_in38_doubt)),id528in_in39_doubt)),id528in_in40_doubt)),id528in_in41_doubt)),id528in_in42_doubt)),id528in_in43_doubt)),id528in_in44_doubt)),id528in_in45_doubt)),id528in_in46_doubt)),id528in_in47_doubt)),id528in_in48_doubt)),id528in_in49_doubt)),id528in_in50_doubt)),id528in_in51_doubt)),id528in_in52_doubt)),id528in_in53_doubt)),id528in_in54_doubt)),id528in_in55_doubt)),id528in_in56_doubt)),id528in_in57_doubt)),id528in_in58_doubt)),id528in_in59_doubt)),id528in_in60_doubt)),id528in_in61_doubt)),id528in_in62_doubt)),id528in_in63_doubt)),id528in_in64_doubt)),id528in_in65_doubt)),id528in_in66_doubt)),id528in_in67_doubt)),id528in_in68_doubt)),id528in_in69_doubt)),id528in_in70_doubt));
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id164out_output;
  HWOffsetFix<1,0,UNSIGNED> id164out_output_doubt;

  { // Node ID: 164 (NodeReinterpret)
    const HWRawBits<71> &id164in_input = id528out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id164in_input_doubt = id528out_result_doubt;

    id164out_output = (cast_bits2fixed<71,-60,TWOSCOMPLEMENT>(id164in_input));
    id164out_output_doubt = id164in_input_doubt;
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id165out_result;
  HWOffsetFix<1,0,UNSIGNED> id165out_result_doubt;

  { // Node ID: 165 (NodeXor)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id165in_a = id91out_value;
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id165in_b = id164out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id165in_b_doubt = id164out_output_doubt;

    HWOffsetFix<71,-60,TWOSCOMPLEMENT> id165x_1;

    (id165x_1) = (xor_fixed(id165in_a,id165in_b));
    id165out_result = (id165x_1);
    id165out_result_doubt = id165in_b_doubt;
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id166out_result;
  HWOffsetFix<1,0,UNSIGNED> id166out_result_doubt;

  { // Node ID: 166 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id166in_sel = id88out_exception;
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id166in_option0 = id88out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id166in_option0_doubt = id88out_result_doubt;
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id166in_option1 = id165out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id166in_option1_doubt = id165out_result_doubt;

    HWOffsetFix<71,-60,TWOSCOMPLEMENT> id166x_1;
    HWOffsetFix<1,0,UNSIGNED> id166x_2;

    switch((id166in_sel.getValueAsLong())) {
      case 0l:
        id166x_1 = id166in_option0;
        break;
      case 1l:
        id166x_1 = id166in_option1;
        break;
      default:
        id166x_1 = (c_hw_fix_71_n60_sgn_undef);
        break;
    }
    switch((id166in_sel.getValueAsLong())) {
      case 0l:
        id166x_2 = id166in_option0_doubt;
        break;
      case 1l:
        id166x_2 = id166in_option1_doubt;
        break;
      default:
        id166x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id166out_result = (id166x_1);
    id166out_result_doubt = (id166x_2);
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id167out_o;
  HWOffsetFix<1,0,UNSIGNED> id167out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id167out_exception;

  { // Node ID: 167 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id167in_i = id166out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id167in_i_doubt = id166out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id167x_1;

    id167out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id167in_i,(&(id167x_1))));
    id167out_o_doubt = (or_fixed((neq_fixed((id167x_1),(c_hw_fix_1_0_uns_bits_1))),id167in_i_doubt));
    id167out_exception = (id167x_1);
  }
  { // Node ID: 169 (NodeConstantRawBits)
  }
  HWRawBits<1> id529out_result;
  HWOffsetFix<1,0,UNSIGNED> id529out_result_doubt;

  { // Node ID: 529 (NodeSlice)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id529in_a = id167out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id529in_a_doubt = id167out_o_doubt;

    id529out_result = (slice<35,1>(id529in_a));
    id529out_result_doubt = id529in_a_doubt;
  }
  HWRawBits<1> id530out_result;
  HWOffsetFix<1,0,UNSIGNED> id530out_result_doubt;

  { // Node ID: 530 (NodeNot)
    const HWRawBits<1> &id530in_a = id529out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id530in_a_doubt = id529out_result_doubt;

    id530out_result = (not_bits(id530in_a));
    id530out_result_doubt = id530in_a_doubt;
  }
  HWRawBits<36> id565out_result;
  HWOffsetFix<1,0,UNSIGNED> id565out_result_doubt;

  { // Node ID: 565 (NodeCat)
    const HWRawBits<1> &id565in_in0 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in0_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in1 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in1_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in2 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in2_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in3 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in3_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in4 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in4_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in5 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in5_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in6 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in6_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in7 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in7_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in8 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in8_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in9 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in9_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in10 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in10_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in11 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in11_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in12 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in12_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in13 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in13_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in14 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in14_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in15 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in15_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in16 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in16_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in17 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in17_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in18 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in18_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in19 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in19_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in20 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in20_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in21 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in21_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in22 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in22_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in23 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in23_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in24 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in24_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in25 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in25_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in26 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in26_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in27 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in27_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in28 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in28_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in29 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in29_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in30 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in30_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in31 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in31_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in32 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in32_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in33 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in33_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in34 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in34_doubt = id530out_result_doubt;
    const HWRawBits<1> &id565in_in35 = id530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id565in_in35_doubt = id530out_result_doubt;

    id565out_result = (cat((cat((cat((cat((cat((cat(id565in_in0,id565in_in1)),id565in_in2)),(cat(id565in_in3,id565in_in4)))),(cat((cat(id565in_in5,id565in_in6)),(cat(id565in_in7,id565in_in8)))))),(cat((cat((cat((cat(id565in_in9,id565in_in10)),id565in_in11)),(cat(id565in_in12,id565in_in13)))),(cat((cat(id565in_in14,id565in_in15)),(cat(id565in_in16,id565in_in17)))))))),(cat((cat((cat((cat((cat(id565in_in18,id565in_in19)),id565in_in20)),(cat(id565in_in21,id565in_in22)))),(cat((cat(id565in_in23,id565in_in24)),(cat(id565in_in25,id565in_in26)))))),(cat((cat((cat((cat(id565in_in27,id565in_in28)),id565in_in29)),(cat(id565in_in30,id565in_in31)))),(cat((cat(id565in_in32,id565in_in33)),(cat(id565in_in34,id565in_in35))))))))));
    id565out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id565in_in0_doubt,id565in_in1_doubt)),id565in_in2_doubt)),id565in_in3_doubt)),id565in_in4_doubt)),id565in_in5_doubt)),id565in_in6_doubt)),id565in_in7_doubt)),id565in_in8_doubt)),id565in_in9_doubt)),id565in_in10_doubt)),id565in_in11_doubt)),id565in_in12_doubt)),id565in_in13_doubt)),id565in_in14_doubt)),id565in_in15_doubt)),id565in_in16_doubt)),id565in_in17_doubt)),id565in_in18_doubt)),id565in_in19_doubt)),id565in_in20_doubt)),id565in_in21_doubt)),id565in_in22_doubt)),id565in_in23_doubt)),id565in_in24_doubt)),id565in_in25_doubt)),id565in_in26_doubt)),id565in_in27_doubt)),id565in_in28_doubt)),id565in_in29_doubt)),id565in_in30_doubt)),id565in_in31_doubt)),id565in_in32_doubt)),id565in_in33_doubt)),id565in_in34_doubt)),id565in_in35_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id207out_output;
  HWOffsetFix<1,0,UNSIGNED> id207out_output_doubt;

  { // Node ID: 207 (NodeReinterpret)
    const HWRawBits<36> &id207in_input = id565out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id207in_input_doubt = id565out_result_doubt;

    id207out_output = (cast_bits2fixed<36,-26,TWOSCOMPLEMENT>(id207in_input));
    id207out_output_doubt = id207in_input_doubt;
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id208out_result;
  HWOffsetFix<1,0,UNSIGNED> id208out_result_doubt;

  { // Node ID: 208 (NodeXor)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id208in_a = id169out_value;
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id208in_b = id207out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id208in_b_doubt = id207out_output_doubt;

    HWOffsetFix<36,-26,TWOSCOMPLEMENT> id208x_1;

    (id208x_1) = (xor_fixed(id208in_a,id208in_b));
    id208out_result = (id208x_1);
    id208out_result_doubt = id208in_b_doubt;
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id209out_result;
  HWOffsetFix<1,0,UNSIGNED> id209out_result_doubt;

  { // Node ID: 209 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id209in_sel = id167out_exception;
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id209in_option0 = id167out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id209in_option0_doubt = id167out_o_doubt;
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id209in_option1 = id208out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id209in_option1_doubt = id208out_result_doubt;

    HWOffsetFix<36,-26,TWOSCOMPLEMENT> id209x_1;
    HWOffsetFix<1,0,UNSIGNED> id209x_2;

    switch((id209in_sel.getValueAsLong())) {
      case 0l:
        id209x_1 = id209in_option0;
        break;
      case 1l:
        id209x_1 = id209in_option1;
        break;
      default:
        id209x_1 = (c_hw_fix_36_n26_sgn_undef);
        break;
    }
    switch((id209in_sel.getValueAsLong())) {
      case 0l:
        id209x_2 = id209in_option0_doubt;
        break;
      case 1l:
        id209x_2 = id209in_option1_doubt;
        break;
      default:
        id209x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id209out_result = (id209x_1);
    id209out_result_doubt = (id209x_2);
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id218out_output;

  { // Node ID: 218 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id218in_input = id209out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id218in_input_doubt = id209out_result_doubt;

    id218out_output = id218in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id219out_o;

  { // Node ID: 219 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id219in_i = id218out_output;

    id219out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id219in_i));
  }
  { // Node ID: 644 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id644in_input = id219out_o;

    id644out_output[(getCycle()+2)%3] = id644in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id262out_o;

  { // Node ID: 262 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id262in_i = id644out_output[getCycle()%3];

    id262out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id262in_i));
  }
  { // Node ID: 677 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id264out_result;
  HWOffsetFix<1,0,UNSIGNED> id264out_exception;

  { // Node ID: 264 (NodeAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id264in_a = id262out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id264in_b = id677out_value;

    HWOffsetFix<1,0,UNSIGNED> id264x_1;

    id264out_result = (add_fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id264in_a,id264in_b,(&(id264x_1))));
    id264out_exception = (id264x_1);
  }
  { // Node ID: 266 (NodeConstantRawBits)
  }
  HWRawBits<1> id566out_result;

  { // Node ID: 566 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id566in_a = id264out_result;

    id566out_result = (slice<10,1>(id566in_a));
  }
  HWRawBits<1> id567out_result;

  { // Node ID: 567 (NodeNot)
    const HWRawBits<1> &id567in_a = id566out_result;

    id567out_result = (not_bits(id567in_a));
  }
  HWRawBits<11> id577out_result;

  { // Node ID: 577 (NodeCat)
    const HWRawBits<1> &id577in_in0 = id567out_result;
    const HWRawBits<1> &id577in_in1 = id567out_result;
    const HWRawBits<1> &id577in_in2 = id567out_result;
    const HWRawBits<1> &id577in_in3 = id567out_result;
    const HWRawBits<1> &id577in_in4 = id567out_result;
    const HWRawBits<1> &id577in_in5 = id567out_result;
    const HWRawBits<1> &id577in_in6 = id567out_result;
    const HWRawBits<1> &id577in_in7 = id567out_result;
    const HWRawBits<1> &id577in_in8 = id567out_result;
    const HWRawBits<1> &id577in_in9 = id567out_result;
    const HWRawBits<1> &id577in_in10 = id567out_result;

    id577out_result = (cat((cat((cat((cat(id577in_in0,id577in_in1)),id577in_in2)),(cat((cat(id577in_in3,id577in_in4)),id577in_in5)))),(cat((cat((cat(id577in_in6,id577in_in7)),id577in_in8)),(cat(id577in_in9,id577in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id279out_output;

  { // Node ID: 279 (NodeReinterpret)
    const HWRawBits<11> &id279in_input = id577out_result;

    id279out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id279in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id280out_result;

  { // Node ID: 280 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id280in_a = id266out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id280in_b = id279out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id280x_1;

    (id280x_1) = (xor_fixed(id280in_a,id280in_b));
    id280out_result = (id280x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id281out_result;

  { // Node ID: 281 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id281in_sel = id264out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id281in_option0 = id264out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id281in_option1 = id280out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id281x_1;

    switch((id281in_sel.getValueAsLong())) {
      case 0l:
        id281x_1 = id281in_option0;
        break;
      case 1l:
        id281x_1 = id281in_option1;
        break;
      default:
        id281x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id281out_result = (id281x_1);
  }
  { // Node ID: 282 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id221out_o;

  { // Node ID: 221 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id221in_i = id218out_output;

    id221out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id221in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id355out_output;

  { // Node ID: 355 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id355in_input = id221out_o;

    id355out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id355in_input))));
  }
  { // Node ID: 356 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id356in_addr = id355out_output;

    HWOffsetFix<22,-24,UNSIGNED> id356x_1;

    switch(((long)((id356in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id356x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id356x_1 = (id356sta_rom_store[(id356in_addr.getValueAsLong())]);
        break;
      default:
        id356x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id356out_dout[(getCycle()+2)%3] = (id356x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id220out_o;

  { // Node ID: 220 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id220in_i = id218out_output;

    id220out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id220in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id352out_output;

  { // Node ID: 352 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id352in_input = id220out_o;

    id352out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id352in_input))));
  }
  { // Node ID: 353 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id353in_addr = id352out_output;

    HWOffsetFix<24,-24,UNSIGNED> id353x_1;

    switch(((long)((id353in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id353x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id353x_1 = (id353sta_rom_store[(id353in_addr.getValueAsLong())]);
        break;
      default:
        id353x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id353out_dout[(getCycle()+2)%3] = (id353x_1);
  }
  { // Node ID: 225 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id222out_o;

  { // Node ID: 222 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id222in_i = id218out_output;

    id222out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id222in_i));
  }
  { // Node ID: 645 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id645in_input = id222out_o;

    id645out_output[(getCycle()+2)%3] = id645in_input;
  }
  HWOffsetFix<28,-40,UNSIGNED> id224out_result;

  { // Node ID: 224 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id224in_a = id225out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id224in_b = id645out_output[getCycle()%3];

    id224out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id224in_a,id224in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id226out_o;
  HWOffsetFix<1,0,UNSIGNED> id226out_exception;

  { // Node ID: 226 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id226in_i = id224out_result;

    HWOffsetFix<1,0,UNSIGNED> id226x_1;

    id226out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id226in_i,(&(id226x_1))));
    id226out_exception = (id226x_1);
  }
  { // Node ID: 228 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id229out_result;

  { // Node ID: 229 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id229in_sel = id226out_exception;
    const HWOffsetFix<14,-26,UNSIGNED> &id229in_option0 = id226out_o;
    const HWOffsetFix<14,-26,UNSIGNED> &id229in_option1 = id228out_value;

    HWOffsetFix<14,-26,UNSIGNED> id229x_1;

    switch((id229in_sel.getValueAsLong())) {
      case 0l:
        id229x_1 = id229in_option0;
        break;
      case 1l:
        id229x_1 = id229in_option1;
        break;
      default:
        id229x_1 = (c_hw_fix_14_n26_uns_undef);
        break;
    }
    id229out_result = (id229x_1);
  }
  HWOffsetFix<27,-26,UNSIGNED> id230out_result;

  { // Node ID: 230 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id230in_a = id353out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id230in_b = id229out_result;

    id230out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id230in_a,id230in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id231out_result;

  { // Node ID: 231 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id231in_a = id229out_result;
    const HWOffsetFix<24,-24,UNSIGNED> &id231in_b = id353out_dout[getCycle()%3];

    id231out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id231in_a,id231in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id232out_result;
  HWOffsetFix<1,0,UNSIGNED> id232out_exception;

  { // Node ID: 232 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id232in_a = id230out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id232in_b = id231out_result;

    HWOffsetFix<1,0,UNSIGNED> id232x_1;

    id232out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id232in_a,id232in_b,(&(id232x_1))));
    id232out_exception = (id232x_1);
  }
  { // Node ID: 234 (NodeConstantRawBits)
  }
  HWOffsetFix<51,-50,UNSIGNED> id235out_result;

  { // Node ID: 235 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id235in_sel = id232out_exception;
    const HWOffsetFix<51,-50,UNSIGNED> &id235in_option0 = id232out_result;
    const HWOffsetFix<51,-50,UNSIGNED> &id235in_option1 = id234out_value;

    HWOffsetFix<51,-50,UNSIGNED> id235x_1;

    switch((id235in_sel.getValueAsLong())) {
      case 0l:
        id235x_1 = id235in_option0;
        break;
      case 1l:
        id235x_1 = id235in_option1;
        break;
      default:
        id235x_1 = (c_hw_fix_51_n50_uns_undef);
        break;
    }
    id235out_result = (id235x_1);
  }
  HWOffsetFix<27,-26,UNSIGNED> id236out_o;
  HWOffsetFix<1,0,UNSIGNED> id236out_exception;

  { // Node ID: 236 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id236in_i = id235out_result;

    HWOffsetFix<1,0,UNSIGNED> id236x_1;

    id236out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id236in_i,(&(id236x_1))));
    id236out_exception = (id236x_1);
  }
  { // Node ID: 238 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id239out_result;

  { // Node ID: 239 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id239in_sel = id236out_exception;
    const HWOffsetFix<27,-26,UNSIGNED> &id239in_option0 = id236out_o;
    const HWOffsetFix<27,-26,UNSIGNED> &id239in_option1 = id238out_value;

    HWOffsetFix<27,-26,UNSIGNED> id239x_1;

    switch((id239in_sel.getValueAsLong())) {
      case 0l:
        id239x_1 = id239in_option0;
        break;
      case 1l:
        id239x_1 = id239in_option1;
        break;
      default:
        id239x_1 = (c_hw_fix_27_n26_uns_undef);
        break;
    }
    id239out_result = (id239x_1);
  }
  HWOffsetFix<28,-26,UNSIGNED> id240out_result;

  { // Node ID: 240 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id240in_a = id356out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id240in_b = id239out_result;

    id240out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id240in_a,id240in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id241out_result;

  { // Node ID: 241 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id241in_a = id239out_result;
    const HWOffsetFix<22,-24,UNSIGNED> &id241in_b = id356out_dout[getCycle()%3];

    id241out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id241in_a,id241in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id242out_result;
  HWOffsetFix<1,0,UNSIGNED> id242out_exception;

  { // Node ID: 242 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id242in_a = id240out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id242in_b = id241out_result;

    HWOffsetFix<1,0,UNSIGNED> id242x_1;

    id242out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id242in_a,id242in_b,(&(id242x_1))));
    id242out_exception = (id242x_1);
  }
  { // Node ID: 244 (NodeConstantRawBits)
  }
  HWOffsetFix<52,-50,UNSIGNED> id245out_result;

  { // Node ID: 245 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id245in_sel = id242out_exception;
    const HWOffsetFix<52,-50,UNSIGNED> &id245in_option0 = id242out_result;
    const HWOffsetFix<52,-50,UNSIGNED> &id245in_option1 = id244out_value;

    HWOffsetFix<52,-50,UNSIGNED> id245x_1;

    switch((id245in_sel.getValueAsLong())) {
      case 0l:
        id245x_1 = id245in_option0;
        break;
      case 1l:
        id245x_1 = id245in_option1;
        break;
      default:
        id245x_1 = (c_hw_fix_52_n50_uns_undef);
        break;
    }
    id245out_result = (id245x_1);
  }
  HWOffsetFix<28,-26,UNSIGNED> id246out_o;
  HWOffsetFix<1,0,UNSIGNED> id246out_exception;

  { // Node ID: 246 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id246in_i = id245out_result;

    HWOffsetFix<1,0,UNSIGNED> id246x_1;

    id246out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id246in_i,(&(id246x_1))));
    id246out_exception = (id246x_1);
  }
  { // Node ID: 248 (NodeConstantRawBits)
  }
  HWOffsetFix<28,-26,UNSIGNED> id249out_result;

  { // Node ID: 249 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id249in_sel = id246out_exception;
    const HWOffsetFix<28,-26,UNSIGNED> &id249in_option0 = id246out_o;
    const HWOffsetFix<28,-26,UNSIGNED> &id249in_option1 = id248out_value;

    HWOffsetFix<28,-26,UNSIGNED> id249x_1;

    switch((id249in_sel.getValueAsLong())) {
      case 0l:
        id249x_1 = id249in_option0;
        break;
      case 1l:
        id249x_1 = id249in_option1;
        break;
      default:
        id249x_1 = (c_hw_fix_28_n26_uns_undef);
        break;
    }
    id249out_result = (id249x_1);
  }
  HWOffsetFix<24,-23,UNSIGNED> id250out_o;
  HWOffsetFix<1,0,UNSIGNED> id250out_exception;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id250in_i = id249out_result;

    HWOffsetFix<1,0,UNSIGNED> id250x_1;

    id250out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id250in_i,(&(id250x_1))));
    id250out_exception = (id250x_1);
  }
  { // Node ID: 252 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id253out_result;

  { // Node ID: 253 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id253in_sel = id250out_exception;
    const HWOffsetFix<24,-23,UNSIGNED> &id253in_option0 = id250out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id253in_option1 = id252out_value;

    HWOffsetFix<24,-23,UNSIGNED> id253x_1;

    switch((id253in_sel.getValueAsLong())) {
      case 0l:
        id253x_1 = id253in_option0;
        break;
      case 1l:
        id253x_1 = id253in_option1;
        break;
      default:
        id253x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id253out_result = (id253x_1);
  }
  { // Node ID: 676 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id578out_result;

  { // Node ID: 578 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id578in_a = id253out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id578in_b = id676out_value;

    id578out_result = (gte_fixed(id578in_a,id578in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id608out_result;
  HWOffsetFix<1,0,UNSIGNED> id608out_exception;

  { // Node ID: 608 (NodeCondAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id608in_a = id281out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id608in_b = id282out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id608in_condb = id578out_result;

    HWOffsetFix<1,0,UNSIGNED> id608x_1;
    HWOffsetFix<1,0,UNSIGNED> id608x_2;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id608x_3;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id608x_4;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id608x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id608x_3 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id608x_3 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id608in_a));
        break;
      default:
        id608x_3 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    switch((id608in_condb.getValueAsLong())) {
      case 0l:
        id608x_4 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id608x_4 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id608in_b));
        break;
      default:
        id608x_4 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    (id608x_5) = (add_fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>((id608x_3),(id608x_4),(&(id608x_1))));
    id608out_result = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id608x_5),(&(id608x_2))));
    id608out_exception = (or_fixed((id608x_1),(id608x_2)));
  }
  { // Node ID: 287 (NodeConstantRawBits)
  }
  HWRawBits<1> id579out_result;

  { // Node ID: 579 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id579in_a = id608out_result;

    id579out_result = (slice<10,1>(id579in_a));
  }
  HWRawBits<1> id580out_result;

  { // Node ID: 580 (NodeNot)
    const HWRawBits<1> &id580in_a = id579out_result;

    id580out_result = (not_bits(id580in_a));
  }
  HWRawBits<11> id590out_result;

  { // Node ID: 590 (NodeCat)
    const HWRawBits<1> &id590in_in0 = id580out_result;
    const HWRawBits<1> &id590in_in1 = id580out_result;
    const HWRawBits<1> &id590in_in2 = id580out_result;
    const HWRawBits<1> &id590in_in3 = id580out_result;
    const HWRawBits<1> &id590in_in4 = id580out_result;
    const HWRawBits<1> &id590in_in5 = id580out_result;
    const HWRawBits<1> &id590in_in6 = id580out_result;
    const HWRawBits<1> &id590in_in7 = id580out_result;
    const HWRawBits<1> &id590in_in8 = id580out_result;
    const HWRawBits<1> &id590in_in9 = id580out_result;
    const HWRawBits<1> &id590in_in10 = id580out_result;

    id590out_result = (cat((cat((cat((cat(id590in_in0,id590in_in1)),id590in_in2)),(cat((cat(id590in_in3,id590in_in4)),id590in_in5)))),(cat((cat((cat(id590in_in6,id590in_in7)),id590in_in8)),(cat(id590in_in9,id590in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id300out_output;

  { // Node ID: 300 (NodeReinterpret)
    const HWRawBits<11> &id300in_input = id590out_result;

    id300out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id300in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id301out_result;

  { // Node ID: 301 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id301in_a = id287out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id301in_b = id300out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id301x_1;

    (id301x_1) = (xor_fixed(id301in_a,id301in_b));
    id301out_result = (id301x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id302out_result;

  { // Node ID: 302 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id302in_sel = id608out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id302in_option0 = id608out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id302in_option1 = id301out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id302x_1;

    switch((id302in_sel.getValueAsLong())) {
      case 0l:
        id302x_1 = id302in_option0;
        break;
      case 1l:
        id302x_1 = id302in_option1;
        break;
      default:
        id302x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id302out_result = (id302x_1);
  }
  HWRawBits<1> id591out_result;

  { // Node ID: 591 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id591in_a = id302out_result;

    id591out_result = (slice<10,1>(id591in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id592out_output;

  { // Node ID: 592 (NodeReinterpret)
    const HWRawBits<1> &id592in_input = id591out_result;

    id592out_output = (cast_bits2fixed<1,0,UNSIGNED>(id592in_input));
  }
  { // Node ID: 675 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id211out_result;

  { // Node ID: 211 (NodeGt)
    const HWFloat<8,24> &id211in_a = id609out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id211in_b = id675out_value;

    id211out_result = (gt_float(id211in_a,id211in_b));
  }
  { // Node ID: 646 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id646in_input = id211out_result;

    id646out_output[(getCycle()+6)%7] = id646in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id212out_output;

  { // Node ID: 212 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id212in_input = id209out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id212in_input_doubt = id209out_result_doubt;

    id212out_output = id212in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id213out_result;

  { // Node ID: 213 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id213in_a = id646out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id213in_b = id212out_output;

    HWOffsetFix<1,0,UNSIGNED> id213x_1;

    (id213x_1) = (and_fixed(id213in_a,id213in_b));
    id213out_result = (id213x_1);
  }
  { // Node ID: 647 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id647in_input = id213out_result;

    id647out_output[(getCycle()+2)%3] = id647in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id305out_result;

  { // Node ID: 305 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id305in_a = id647out_output[getCycle()%3];

    id305out_result = (not_fixed(id305in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id306out_result;

  { // Node ID: 306 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id306in_a = id592out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id306in_b = id305out_result;

    HWOffsetFix<1,0,UNSIGNED> id306x_1;

    (id306x_1) = (and_fixed(id306in_a,id306in_b));
    id306out_result = (id306x_1);
  }
  { // Node ID: 674 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id215out_result;

  { // Node ID: 215 (NodeLt)
    const HWFloat<8,24> &id215in_a = id609out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id215in_b = id674out_value;

    id215out_result = (lt_float(id215in_a,id215in_b));
  }
  { // Node ID: 648 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id648in_input = id215out_result;

    id648out_output[(getCycle()+6)%7] = id648in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id216out_output;

  { // Node ID: 216 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id216in_input = id209out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id216in_input_doubt = id209out_result_doubt;

    id216out_output = id216in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id217out_result;

  { // Node ID: 217 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id217in_a = id648out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id217in_b = id216out_output;

    HWOffsetFix<1,0,UNSIGNED> id217x_1;

    (id217x_1) = (and_fixed(id217in_a,id217in_b));
    id217out_result = (id217x_1);
  }
  { // Node ID: 649 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id649in_input = id217out_result;

    id649out_output[(getCycle()+2)%3] = id649in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id307out_result;

  { // Node ID: 307 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id307in_a = id306out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id307in_b = id649out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id307x_1;

    (id307x_1) = (or_fixed(id307in_a,id307in_b));
    id307out_result = (id307x_1);
  }
  { // Node ID: 673 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id593out_result;

  { // Node ID: 593 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id593in_a = id302out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id593in_b = id673out_value;

    id593out_result = (gte_fixed(id593in_a,id593in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id328out_result;

  { // Node ID: 328 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id328in_a = id649out_output[getCycle()%3];

    id328out_result = (not_fixed(id328in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id329out_result;

  { // Node ID: 329 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id329in_a = id593out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id329in_b = id328out_result;

    HWOffsetFix<1,0,UNSIGNED> id329x_1;

    (id329x_1) = (and_fixed(id329in_a,id329in_b));
    id329out_result = (id329x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id330out_result;

  { // Node ID: 330 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id330in_a = id329out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id330in_b = id647out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id330x_1;

    (id330x_1) = (or_fixed(id330in_a,id330in_b));
    id330out_result = (id330x_1);
  }
  HWRawBits<2> id331out_result;

  { // Node ID: 331 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_in0 = id307out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id331in_in1 = id330out_result;

    id331out_result = (cat(id331in_in0,id331in_in1));
  }
  { // Node ID: 323 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id308out_o;
  HWOffsetFix<1,0,UNSIGNED> id308out_exception;

  { // Node ID: 308 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id308in_i = id302out_result;

    HWOffsetFix<1,0,UNSIGNED> id308x_1;

    id308out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id308in_i,(&(id308x_1))));
    id308out_exception = (id308x_1);
  }
  { // Node ID: 310 (NodeConstantRawBits)
  }
  HWRawBits<1> id594out_result;

  { // Node ID: 594 (NodeSlice)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id594in_a = id308out_o;

    id594out_result = (slice<7,1>(id594in_a));
  }
  HWRawBits<1> id595out_result;

  { // Node ID: 595 (NodeNot)
    const HWRawBits<1> &id595in_a = id594out_result;

    id595out_result = (not_bits(id595in_a));
  }
  HWRawBits<8> id602out_result;

  { // Node ID: 602 (NodeCat)
    const HWRawBits<1> &id602in_in0 = id595out_result;
    const HWRawBits<1> &id602in_in1 = id595out_result;
    const HWRawBits<1> &id602in_in2 = id595out_result;
    const HWRawBits<1> &id602in_in3 = id595out_result;
    const HWRawBits<1> &id602in_in4 = id595out_result;
    const HWRawBits<1> &id602in_in5 = id595out_result;
    const HWRawBits<1> &id602in_in6 = id595out_result;
    const HWRawBits<1> &id602in_in7 = id595out_result;

    id602out_result = (cat((cat((cat(id602in_in0,id602in_in1)),(cat(id602in_in2,id602in_in3)))),(cat((cat(id602in_in4,id602in_in5)),(cat(id602in_in6,id602in_in7))))));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id320out_output;

  { // Node ID: 320 (NodeReinterpret)
    const HWRawBits<8> &id320in_input = id602out_result;

    id320out_output = (cast_bits2fixed<8,0,TWOSCOMPLEMENT>(id320in_input));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id321out_result;

  { // Node ID: 321 (NodeXor)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id321in_a = id310out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id321in_b = id320out_output;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id321x_1;

    (id321x_1) = (xor_fixed(id321in_a,id321in_b));
    id321out_result = (id321x_1);
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id322out_result;

  { // Node ID: 322 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id322in_sel = id308out_exception;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id322in_option0 = id308out_o;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id322in_option1 = id321out_result;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id322x_1;

    switch((id322in_sel.getValueAsLong())) {
      case 0l:
        id322x_1 = id322in_option0;
        break;
      case 1l:
        id322x_1 = id322in_option1;
        break;
      default:
        id322x_1 = (c_hw_fix_8_0_sgn_undef);
        break;
    }
    id322out_result = (id322x_1);
  }
  { // Node ID: 256 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id257out_result;

  { // Node ID: 257 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id257in_sel = id578out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id257in_option0 = id253out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id257in_option1 = id256out_value;

    HWOffsetFix<24,-23,UNSIGNED> id257x_1;

    switch((id257in_sel.getValueAsLong())) {
      case 0l:
        id257x_1 = id257in_option0;
        break;
      case 1l:
        id257x_1 = id257in_option1;
        break;
      default:
        id257x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id257out_result = (id257x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id258out_o;
  HWOffsetFix<1,0,UNSIGNED> id258out_exception;

  { // Node ID: 258 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id258in_i = id257out_result;

    HWOffsetFix<1,0,UNSIGNED> id258x_1;

    id258out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id258in_i,(&(id258x_1))));
    id258out_exception = (id258x_1);
  }
  { // Node ID: 260 (NodeConstantRawBits)
  }
  HWOffsetFix<23,-23,UNSIGNED> id261out_result;

  { // Node ID: 261 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id261in_sel = id258out_exception;
    const HWOffsetFix<23,-23,UNSIGNED> &id261in_option0 = id258out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id261in_option1 = id260out_value;

    HWOffsetFix<23,-23,UNSIGNED> id261x_1;

    switch((id261in_sel.getValueAsLong())) {
      case 0l:
        id261x_1 = id261in_option0;
        break;
      case 1l:
        id261x_1 = id261in_option1;
        break;
      default:
        id261x_1 = (c_hw_fix_23_n23_uns_undef);
        break;
    }
    id261out_result = (id261x_1);
  }
  HWRawBits<32> id324out_result;

  { // Node ID: 324 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id324in_in0 = id323out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id324in_in1 = id322out_result;
    const HWOffsetFix<23,-23,UNSIGNED> &id324in_in2 = id261out_result;

    id324out_result = (cat((cat(id324in_in0,id324in_in1)),id324in_in2));
  }
  HWFloat<8,24> id325out_output;

  { // Node ID: 325 (NodeReinterpret)
    const HWRawBits<32> &id325in_input = id324out_result;

    id325out_output = (cast_bits2float<8,24>(id325in_input));
  }
  { // Node ID: 332 (NodeConstantRawBits)
  }
  { // Node ID: 333 (NodeConstantRawBits)
  }
  { // Node ID: 335 (NodeConstantRawBits)
  }
  HWRawBits<32> id603out_result;

  { // Node ID: 603 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id603in_in0 = id332out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id603in_in1 = id333out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id603in_in2 = id335out_value;

    id603out_result = (cat((cat(id603in_in0,id603in_in1)),id603in_in2));
  }
  HWFloat<8,24> id337out_output;

  { // Node ID: 337 (NodeReinterpret)
    const HWRawBits<32> &id337in_input = id603out_result;

    id337out_output = (cast_bits2float<8,24>(id337in_input));
  }
  { // Node ID: 415 (NodeConstantRawBits)
  }
  HWFloat<8,24> id340out_result;

  { // Node ID: 340 (NodeMux)
    const HWRawBits<2> &id340in_sel = id331out_result;
    const HWFloat<8,24> &id340in_option0 = id325out_output;
    const HWFloat<8,24> &id340in_option1 = id337out_output;
    const HWFloat<8,24> &id340in_option2 = id415out_value;
    const HWFloat<8,24> &id340in_option3 = id337out_output;

    HWFloat<8,24> id340x_1;

    switch((id340in_sel.getValueAsLong())) {
      case 0l:
        id340x_1 = id340in_option0;
        break;
      case 1l:
        id340x_1 = id340in_option1;
        break;
      case 2l:
        id340x_1 = id340in_option2;
        break;
      case 3l:
        id340x_1 = id340in_option3;
        break;
      default:
        id340x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id340out_result = (id340x_1);
  }
  { // Node ID: 672 (NodeConstantRawBits)
  }
  HWFloat<8,24> id350out_result;

  { // Node ID: 350 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id350in_sel = id652out_output[getCycle()%9];
    const HWFloat<8,24> &id350in_option0 = id340out_result;
    const HWFloat<8,24> &id350in_option1 = id672out_value;

    HWFloat<8,24> id350x_1;

    switch((id350in_sel.getValueAsLong())) {
      case 0l:
        id350x_1 = id350in_option0;
        break;
      case 1l:
        id350x_1 = id350in_option1;
        break;
      default:
        id350x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id350out_result = (id350x_1);
  }
  { // Node ID: 671 (NodeConstantRawBits)
  }
  HWFloat<8,24> id358out_result;
  HWOffsetFix<4,0,UNSIGNED> id358out_exception;

  { // Node ID: 358 (NodeAdd)
    const HWFloat<8,24> &id358in_a = id350out_result;
    const HWFloat<8,24> &id358in_b = id671out_value;

    HWOffsetFix<4,0,UNSIGNED> id358x_1;

    id358out_result = (add_float(id358in_a,id358in_b,(&(id358x_1))));
    id358out_exception = (id358x_1);
  }
  HWFloat<8,24> id360out_result;
  HWOffsetFix<4,0,UNSIGNED> id360out_exception;

  { // Node ID: 360 (NodeDiv)
    const HWFloat<8,24> &id360in_a = id679out_value;
    const HWFloat<8,24> &id360in_b = id358out_result;

    HWOffsetFix<4,0,UNSIGNED> id360x_1;

    id360out_result = (div_float(id360in_a,id360in_b,(&(id360x_1))));
    id360out_exception = (id360x_1);
  }
  HWFloat<8,24> id362out_result;
  HWOffsetFix<4,0,UNSIGNED> id362out_exception;

  { // Node ID: 362 (NodeSub)
    const HWFloat<8,24> &id362in_a = id680out_value;
    const HWFloat<8,24> &id362in_b = id360out_result;

    HWOffsetFix<4,0,UNSIGNED> id362x_1;

    id362out_result = (sub_float(id362in_a,id362in_b,(&(id362x_1))));
    id362out_exception = (id362x_1);
  }
  HWFloat<8,24> id365out_result;

  { // Node ID: 365 (NodeSub)
    const HWFloat<8,24> &id365in_a = id681out_value;
    const HWFloat<8,24> &id365in_b = id362out_result;

    id365out_result = (sub_float(id365in_a,id365in_b));
  }
  HWFloat<8,24> id366out_result;

  { // Node ID: 366 (NodeMul)
    const HWFloat<8,24> &id366in_a = id365out_result;
    const HWFloat<8,24> &id366in_b = id653out_output[getCycle()%10];

    id366out_result = (mul_float(id366in_a,id366in_b));
  }
  HWFloat<8,24> id367out_result;

  { // Node ID: 367 (NodeMul)
    const HWFloat<8,24> &id367in_a = id366out_result;
    const HWFloat<8,24> &id367in_b = id653out_output[getCycle()%10];

    id367out_result = (mul_float(id367in_a,id367in_b));
  }
  { // Node ID: 610 (NodePO2FPMult)
    const HWFloat<8,24> &id610in_floatIn = id367out_result;

    id610out_floatOut[(getCycle()+1)%2] = (mul_float(id610in_floatIn,(c_hw_flt_8_24_0_5val)));
  }
  HWFloat<8,24> id372out_result;

  { // Node ID: 372 (NodeMul)
    const HWFloat<8,24> &id372in_a = id371out_result;
    const HWFloat<8,24> &id372in_b = id610out_floatOut[getCycle()%2];

    id372out_result = (mul_float(id372in_a,id372in_b));
  }
  HWFloat<8,24> id604out_result;

  { // Node ID: 604 (NodeSub)
    const HWFloat<8,24> &id604in_a = id372out_result;
    const HWFloat<8,24> &id604in_b = id665out_output[getCycle()%2];

    id604out_result = (sub_float(id604in_a,id604in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,24> id379out_result;

  { // Node ID: 379 (NodeMul)
    const HWFloat<8,24> &id379in_a = id604out_result;
    const HWFloat<8,24> &id379in_b = id2out_value;

    id379out_result = (mul_float(id379in_a,id379in_b));
  }
  HWFloat<8,24> id381out_result;

  { // Node ID: 381 (NodeMul)
    const HWFloat<8,24> &id381in_a = id19out_o[getCycle()%4];
    const HWFloat<8,24> &id381in_b = id379out_result;

    id381out_result = (mul_float(id381in_a,id381in_b));
  }
  HWFloat<8,24> id382out_result;

  { // Node ID: 382 (NodeAdd)
    const HWFloat<8,24> &id382in_a = id665out_output[getCycle()%2];
    const HWFloat<8,24> &id382in_b = id381out_result;

    id382out_result = (add_float(id382in_a,id382in_b));
  }
  HWFloat<8,24> id637out_output;

  { // Node ID: 637 (NodeStreamOffset)
    const HWFloat<8,24> &id637in_input = id382out_result;

    id637out_output = id637in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id416out_result[getCycle()%2];
    const HWFloat<8,24> &id23in_option0 = id637out_output;
    const HWFloat<8,24> &id23in_option1 = id22out_value;

    HWFloat<8,24> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<8,24> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<8,24> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<8,24> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 609 (NodePO2FPMult)
    const HWFloat<8,24> &id609in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id609x_1;

    id609out_floatOut[(getCycle()+1)%2] = (mul_float(id609in_floatIn,(c_hw_flt_8_24_2_0val),(&(id609x_1))));
    id609out_exception[(getCycle()+1)%2] = (id609x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<8,24> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<8,24> &id42in_input = id609out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<8,24> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 614 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id614in_exceptionVector = id43out_exception[getCycle()%7];

    (id614st_reg) = (or_fixed((id614st_reg),id614in_exceptionVector));
    id614out_exceptionOccurred[(getCycle()+1)%2] = (id614st_reg);
  }
  { // Node ID: 657 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id657in_input = id614out_exceptionOccurred[getCycle()%2];

    id657out_output[(getCycle()+2)%3] = id657in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 615 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id615in_exceptionVector = id88out_exception;

    (id615st_reg) = (or_fixed((id615st_reg),id615in_exceptionVector));
    id615out_exceptionOccurred[(getCycle()+1)%2] = (id615st_reg);
  }
  { // Node ID: 658 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_input = id615out_exceptionOccurred[getCycle()%2];

    id658out_output[(getCycle()+2)%3] = id658in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 616 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id616in_exceptionVector = id167out_exception;

    (id616st_reg) = (or_fixed((id616st_reg),id616in_exceptionVector));
    id616out_exceptionOccurred[(getCycle()+1)%2] = (id616st_reg);
  }
  { // Node ID: 659 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id659in_input = id616out_exceptionOccurred[getCycle()%2];

    id659out_output[(getCycle()+2)%3] = id659in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 618 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id618in_exceptionVector = id226out_exception;

    (id618st_reg) = (or_fixed((id618st_reg),id618in_exceptionVector));
    id618out_exceptionOccurred[(getCycle()+1)%2] = (id618st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 619 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id619in_exceptionVector = id232out_exception;

    (id619st_reg) = (or_fixed((id619st_reg),id619in_exceptionVector));
    id619out_exceptionOccurred[(getCycle()+1)%2] = (id619st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 620 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id620in_exceptionVector = id236out_exception;

    (id620st_reg) = (or_fixed((id620st_reg),id620in_exceptionVector));
    id620out_exceptionOccurred[(getCycle()+1)%2] = (id620st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 621 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id621in_exceptionVector = id242out_exception;

    (id621st_reg) = (or_fixed((id621st_reg),id621in_exceptionVector));
    id621out_exceptionOccurred[(getCycle()+1)%2] = (id621st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 622 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id622in_exceptionVector = id246out_exception;

    (id622st_reg) = (or_fixed((id622st_reg),id622in_exceptionVector));
    id622out_exceptionOccurred[(getCycle()+1)%2] = (id622st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 623 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id623in_exceptionVector = id250out_exception;

    (id623st_reg) = (or_fixed((id623st_reg),id623in_exceptionVector));
    id623out_exceptionOccurred[(getCycle()+1)%2] = (id623st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 626 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id626in_exceptionVector = id258out_exception;

    (id626st_reg) = (or_fixed((id626st_reg),id626in_exceptionVector));
    id626out_exceptionOccurred[(getCycle()+1)%2] = (id626st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 617 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id617in_exceptionVector = id264out_exception;

    (id617st_reg) = (or_fixed((id617st_reg),id617in_exceptionVector));
    id617out_exceptionOccurred[(getCycle()+1)%2] = (id617st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 625 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id625in_exceptionVector = id308out_exception;

    (id625st_reg) = (or_fixed((id625st_reg),id625in_exceptionVector));
    id625out_exceptionOccurred[(getCycle()+1)%2] = (id625st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 627 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id627in_exceptionVector = id358out_exception;

    (id627st_reg) = (or_fixed((id627st_reg),id627in_exceptionVector));
    id627out_exceptionOccurred[(getCycle()+1)%2] = (id627st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 628 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id628in_exceptionVector = id360out_exception;

    (id628st_reg) = (or_fixed((id628st_reg),id628in_exceptionVector));
    id628out_exceptionOccurred[(getCycle()+1)%2] = (id628st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 629 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id629in_exceptionVector = id362out_exception;

    (id629st_reg) = (or_fixed((id629st_reg),id629in_exceptionVector));
    id629out_exceptionOccurred[(getCycle()+1)%2] = (id629st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 624 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id624in_exceptionVector = id608out_exception;

    (id624st_reg) = (or_fixed((id624st_reg),id624in_exceptionVector));
    id624out_exceptionOccurred[(getCycle()+1)%2] = (id624st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 613 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id613in_exceptionVector = id609out_exception[getCycle()%2];

    (id613st_reg) = (or_fixed((id613st_reg),id613in_exceptionVector));
    id613out_exceptionOccurred[(getCycle()+1)%2] = (id613st_reg);
  }
  { // Node ID: 660 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id660in_input = id613out_exceptionOccurred[getCycle()%2];

    id660out_output[(getCycle()+8)%9] = id660in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 630 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id630in_n43 = id657out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n88 = id658out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n167 = id659out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n226 = id618out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n232 = id619out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n236 = id620out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n242 = id621out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n246 = id622out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n250 = id623out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n258 = id626out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n264 = id617out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n308 = id625out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id630in_n358 = id627out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id630in_n360 = id628out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id630in_n362 = id629out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id630in_n608 = id624out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id630in_n609 = id660out_output[getCycle()%9];

    id630out_all_exceptions = (cat((cat((cat((cat((cat(id630in_n43,id630in_n88)),id630in_n167)),(cat(id630in_n226,id630in_n232)))),(cat((cat(id630in_n236,id630in_n242)),(cat(id630in_n246,id630in_n250)))))),(cat((cat((cat(id630in_n258,id630in_n264)),(cat(id630in_n308,id630in_n358)))),(cat((cat(id630in_n360,id630in_n362)),(cat(id630in_n608,id630in_n609))))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 631 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id631in_load = id632out_value;
    const HWRawBits<32> &id631in_data = id630out_all_exceptions;

    bool id631x_1;

    (id631x_1) = ((id631in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id631x_1)) {
      setMappedRegValue("all_numeric_exceptions", id631in_data);
    }
  }
  { // Node ID: 611 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 612 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id612in_output_control = id611out_value;
    const HWFloat<8,24> &id612in_data = id637out_output;

    bool id612x_1;

    (id612x_1) = ((id612in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id612x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id612in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 661 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id661in_input = id17out_count;

    id661out_output[(getCycle()+1)%2] = id661in_input;
  }
  { // Node ID: 670 (NodeConstantRawBits)
  }
  { // Node ID: 390 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id390in_a = id666out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id390in_b = id670out_value;

    id390out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id390in_a,id390in_b));
  }
  { // Node ID: 605 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id605in_a = id661out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id605in_b = id390out_result[getCycle()%2];

    id605out_result[(getCycle()+1)%2] = (eq_fixed(id605in_a,id605in_b));
  }
  { // Node ID: 392 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id393out_result;

  { // Node ID: 393 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id393in_a = id392out_io_u_out_force_disabled;

    id393out_result = (not_fixed(id393in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id394out_result;

  { // Node ID: 394 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id394in_a = id605out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id394in_b = id393out_result;

    HWOffsetFix<1,0,UNSIGNED> id394x_1;

    (id394x_1) = (and_fixed(id394in_a,id394in_b));
    id394out_result = (id394x_1);
  }
  { // Node ID: 662 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id662in_input = id394out_result;

    id662out_output[(getCycle()+10)%11] = id662in_input;
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 395 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id395in_output_control = id662out_output[getCycle()%11];
    const HWFloat<8,24> &id395in_data = id382out_result;

    bool id395x_1;

    (id395x_1) = ((id395in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id395x_1)) {
      writeOutput(m_u_out, id395in_data);
    }
  }
  { // Node ID: 669 (NodeConstantRawBits)
  }
  { // Node ID: 397 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id397in_a = id666out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id397in_b = id669out_value;

    id397out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id397in_a,id397in_b));
  }
  { // Node ID: 606 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id606in_a = id661out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id606in_b = id397out_result[getCycle()%2];

    id606out_result[(getCycle()+1)%2] = (eq_fixed(id606in_a,id606in_b));
  }
  { // Node ID: 399 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id400out_result;

  { // Node ID: 400 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id399out_io_v_out_force_disabled;

    id400out_result = (not_fixed(id400in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_a = id606out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id401in_b = id400out_result;

    HWOffsetFix<1,0,UNSIGNED> id401x_1;

    (id401x_1) = (and_fixed(id401in_a,id401in_b));
    id401out_result = (id401x_1);
  }
  { // Node ID: 664 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_input = id401out_result;

    id664out_output[(getCycle()+10)%11] = id664in_input;
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 402 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_output_control = id664out_output[getCycle()%11];
    const HWFloat<8,24> &id402in_data = id384out_result;

    bool id402x_1;

    (id402x_1) = ((id402in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id402x_1)) {
      writeOutput(m_v_out, id402in_data);
    }
  }
  { // Node ID: 407 (NodeConstantRawBits)
  }
  { // Node ID: 668 (NodeConstantRawBits)
  }
  { // Node ID: 404 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 405 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id405in_enable = id668out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id405in_max = id404out_value;

    HWOffsetFix<49,0,UNSIGNED> id405x_1;
    HWOffsetFix<1,0,UNSIGNED> id405x_2;
    HWOffsetFix<1,0,UNSIGNED> id405x_3;
    HWOffsetFix<49,0,UNSIGNED> id405x_4t_1e_1;

    id405out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id405st_count)));
    (id405x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id405st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id405x_2) = (gte_fixed((id405x_1),id405in_max));
    (id405x_3) = (and_fixed((id405x_2),id405in_enable));
    id405out_wrap = (id405x_3);
    if((id405in_enable.getValueAsBool())) {
      if(((id405x_3).getValueAsBool())) {
        (id405st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id405x_4t_1e_1) = (id405x_1);
        (id405st_count) = (id405x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id406out_output;

  { // Node ID: 406 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id406in_input = id405out_count;

    id406out_output = id406in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 408 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id408in_load = id407out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id408in_data = id406out_output;

    bool id408x_1;

    (id408x_1) = ((id408in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id408x_1)) {
      setMappedRegValue("current_run_cycle_count", id408in_data);
    }
  }
  { // Node ID: 667 (NodeConstantRawBits)
  }
  { // Node ID: 410 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 411 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id411in_enable = id667out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id411in_max = id410out_value;

    HWOffsetFix<49,0,UNSIGNED> id411x_1;
    HWOffsetFix<1,0,UNSIGNED> id411x_2;
    HWOffsetFix<1,0,UNSIGNED> id411x_3;
    HWOffsetFix<49,0,UNSIGNED> id411x_4t_1e_1;

    id411out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id411st_count)));
    (id411x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id411st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id411x_2) = (gte_fixed((id411x_1),id411in_max));
    (id411x_3) = (and_fixed((id411x_2),id411in_enable));
    id411out_wrap = (id411x_3);
    if((id411in_enable.getValueAsBool())) {
      if(((id411x_3).getValueAsBool())) {
        (id411st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id411x_4t_1e_1) = (id411x_1);
        (id411st_count) = (id411x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 413 (NodeInputMappedReg)
  }
  { // Node ID: 607 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id607in_a = id411out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id607in_b = id413out_run_cycle_count;

    id607out_result[(getCycle()+1)%2] = (eq_fixed(id607in_a,id607in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 412 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id412in_start = id607out_result[getCycle()%2];

    if((id412in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
