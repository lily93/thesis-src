Top 10 largest FIFOs
     Costs                                              From                                                To
         2                               KarmaKernel.maxj:62                               KarmaKernel.maxj:70
         2                              KarmaKernel.maxj:120                              KarmaKernel.maxj:120
         2                               KarmaKernel.maxj:63                              KarmaKernel.maxj:109
         1                               KarmaKernel.maxj:62                               KarmaKernel.maxj:71
         1                               KarmaKernel.maxj:63                              KarmaKernel.maxj:111
         1                               KarmaKernel.maxj:63                               KarmaKernel.maxj:80
         1                              KarmaKernel.maxj:119                              KarmaManager.maxj:17
         1                               KarmaKernel.maxj:63                               KarmaKernel.maxj:75

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
         2                              KarmaKernel.maxj:120         2
         2                               KarmaKernel.maxj:70         1
         2                              KarmaKernel.maxj:109         1
         1                               KarmaKernel.maxj:71         1
         1                              KarmaKernel.maxj:111         1
         1                               KarmaKernel.maxj:80         1
         1                               KarmaKernel.maxj:75         1
         1                              KarmaManager.maxj:17         1

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
         5                               KarmaKernel.maxj:63         4
         3                               KarmaKernel.maxj:62         2
         2                              KarmaKernel.maxj:120         2
         1                              KarmaKernel.maxj:119         1
