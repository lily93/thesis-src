#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaDFEKernel::execute0() {
  { // Node ID: 792 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 826 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id826out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 844 (NodeConstantRawBits)
  }
  { // Node ID: 504 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id504in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id504in_b = id844out_value;

    id504out_result[(getCycle()+1)%2] = (eq_fixed(id504in_a,id504in_b));
  }
  { // Node ID: 813 (NodeFIFO)
    const HWFloat<8,40> &id813in_input = id23out_result[getCycle()%2];

    id813out_output[(getCycle()+9)%10] = id813in_input;
  }
  { // Node ID: 825 (NodeFIFO)
    const HWFloat<8,40> &id825in_input = id813out_output[getCycle()%10];

    id825out_output[(getCycle()+1)%2] = id825in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 19 (NodeCast)
    const HWFloat<11,53> &id19in_i = id18out_dt;

    id19out_o[(getCycle()+3)%4] = (cast_float2float<8,40>(id19in_i));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 843 (NodeConstantRawBits)
  }
  { // Node ID: 505 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id505in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id505in_b = id843out_value;

    id505out_result[(getCycle()+1)%2] = (eq_fixed(id505in_a,id505in_b));
  }
  { // Node ID: 842 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id462out_result;

  { // Node ID: 462 (NodeGt)
    const HWFloat<8,40> &id462in_a = id825out_output[getCycle()%2];
    const HWFloat<8,40> &id462in_b = id5out_value;

    id462out_result = (gt_float(id462in_a,id462in_b));
  }
  HWFloat<8,40> id463out_o;

  { // Node ID: 463 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id463in_i = id462out_result;

    id463out_o = (cast_fixed2float<8,40>(id463in_i));
  }
  HWFloat<8,40> id465out_result;

  { // Node ID: 465 (NodeMul)
    const HWFloat<8,40> &id465in_a = id842out_value;
    const HWFloat<8,40> &id465in_b = id463out_o;

    id465out_result = (mul_float(id465in_a,id465in_b));
  }
  HWFloat<8,40> id466out_result;

  { // Node ID: 466 (NodeSub)
    const HWFloat<8,40> &id466in_a = id465out_result;
    const HWFloat<8,40> &id466in_b = id799out_output[getCycle()%11];

    id466out_result = (sub_float(id466in_a,id466in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,40> id468out_result;

  { // Node ID: 468 (NodeMul)
    const HWFloat<8,40> &id468in_a = id466out_result;
    const HWFloat<8,40> &id468in_b = id3out_value;

    id468out_result = (mul_float(id468in_a,id468in_b));
  }
  HWFloat<8,40> id471out_result;

  { // Node ID: 471 (NodeMul)
    const HWFloat<8,40> &id471in_a = id19out_o[getCycle()%4];
    const HWFloat<8,40> &id471in_b = id468out_result;

    id471out_result = (mul_float(id471in_a,id471in_b));
  }
  HWFloat<8,40> id472out_result;

  { // Node ID: 472 (NodeAdd)
    const HWFloat<8,40> &id472in_a = id799out_output[getCycle()%11];
    const HWFloat<8,40> &id472in_b = id471out_result;

    id472out_result = (add_float(id472in_a,id472in_b));
  }
  HWFloat<8,40> id796out_output;

  { // Node ID: 796 (NodeStreamOffset)
    const HWFloat<8,40> &id796in_input = id472out_result;

    id796out_output = id796in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id505out_result[getCycle()%2];
    const HWFloat<8,40> &id27in_option0 = id796out_output;
    const HWFloat<8,40> &id27in_option1 = id26out_value;

    HWFloat<8,40> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 799 (NodeFIFO)
    const HWFloat<8,40> &id799in_input = id27out_result[getCycle()%2];

    id799out_output[(getCycle()+10)%11] = id799in_input;
  }
  HWFloat<8,40> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<8,40> &id32in_a = id799out_output[getCycle()%11];
    const HWFloat<8,40> &id32in_b = id799out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<8,40> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<8,40> &id33in_a = id799out_output[getCycle()%11];
    const HWFloat<8,40> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<8,40> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<8,40> &id34in_a = id33out_result;
    const HWFloat<8,40> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<8,40> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<8,40> &id35in_a = id34out_result;
    const HWFloat<8,40> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<8,40> id459out_result;

  { // Node ID: 459 (NodeSub)
    const HWFloat<8,40> &id459in_a = id6out_value;
    const HWFloat<8,40> &id459in_b = id35out_result;

    id459out_result = (sub_float(id459in_a,id459in_b));
  }
  { // Node ID: 841 (NodeConstantRawBits)
  }
  { // Node ID: 840 (NodeConstantRawBits)
  }
  { // Node ID: 839 (NodeConstantRawBits)
  }
  HWRawBits<8> id428out_result;

  { // Node ID: 428 (NodeSlice)
    const HWFloat<8,40> &id428in_a = id765out_floatOut[getCycle()%2];

    id428out_result = (slice<39,8>(id428in_a));
  }
  { // Node ID: 429 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id506out_result;

  { // Node ID: 506 (NodeEqInlined)
    const HWRawBits<8> &id506in_a = id428out_result;
    const HWRawBits<8> &id506in_b = id429out_value;

    id506out_result = (eq_bits(id506in_a,id506in_b));
  }
  HWRawBits<39> id427out_result;

  { // Node ID: 427 (NodeSlice)
    const HWFloat<8,40> &id427in_a = id765out_floatOut[getCycle()%2];

    id427out_result = (slice<0,39>(id427in_a));
  }
  { // Node ID: 838 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id507out_result;

  { // Node ID: 507 (NodeNeqInlined)
    const HWRawBits<39> &id507in_a = id427out_result;
    const HWRawBits<39> &id507in_b = id838out_value;

    id507out_result = (neq_bits(id507in_a,id507in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id433out_result;

  { // Node ID: 433 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id433in_a = id506out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id433in_b = id507out_result;

    HWOffsetFix<1,0,UNSIGNED> id433x_1;

    (id433x_1) = (and_fixed(id433in_a,id433in_b));
    id433out_result = (id433x_1);
  }
  { // Node ID: 812 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id812in_input = id433out_result;

    id812out_output[(getCycle()+8)%9] = id812in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id508out_result;
  HWOffsetFix<1,0,UNSIGNED> id508out_result_doubt;

  { // Node ID: 508 (NodeSlice)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id508in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id508in_a_doubt = id43out_o_doubt[getCycle()%7];

    id508out_result = (slice<52,1>(id508in_a));
    id508out_result_doubt = id508in_a_doubt;
  }
  HWRawBits<1> id509out_result;
  HWOffsetFix<1,0,UNSIGNED> id509out_result_doubt;

  { // Node ID: 509 (NodeNot)
    const HWRawBits<1> &id509in_a = id508out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id509in_a_doubt = id508out_result_doubt;

    id509out_result = (not_bits(id509in_a));
    id509out_result_doubt = id509in_a_doubt;
  }
  HWRawBits<53> id561out_result;
  HWOffsetFix<1,0,UNSIGNED> id561out_result_doubt;

  { // Node ID: 561 (NodeCat)
    const HWRawBits<1> &id561in_in0 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in0_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in1 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in1_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in2 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in2_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in3 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in3_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in4 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in4_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in5 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in5_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in6 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in6_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in7 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in7_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in8 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in8_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in9 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in9_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in10 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in10_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in11 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in11_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in12 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in12_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in13 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in13_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in14 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in14_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in15 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in15_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in16 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in16_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in17 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in17_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in18 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in18_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in19 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in19_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in20 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in20_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in21 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in21_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in22 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in22_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in23 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in23_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in24 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in24_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in25 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in25_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in26 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in26_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in27 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in27_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in28 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in28_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in29 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in29_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in30 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in30_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in31 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in31_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in32 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in32_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in33 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in33_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in34 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in34_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in35 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in35_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in36 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in36_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in37 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in37_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in38 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in38_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in39 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in39_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in40 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in40_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in41 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in41_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in42 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in42_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in43 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in43_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in44 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in44_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in45 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in45_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in46 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in46_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in47 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in47_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in48 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in48_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in49 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in49_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in50 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in50_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in51 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in51_doubt = id509out_result_doubt;
    const HWRawBits<1> &id561in_in52 = id509out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id561in_in52_doubt = id509out_result_doubt;

    id561out_result = (cat((cat((cat((cat((cat((cat(id561in_in0,id561in_in1)),(cat(id561in_in2,id561in_in3)))),(cat((cat(id561in_in4,id561in_in5)),id561in_in6)))),(cat((cat((cat(id561in_in7,id561in_in8)),(cat(id561in_in9,id561in_in10)))),(cat((cat(id561in_in11,id561in_in12)),id561in_in13)))))),(cat((cat((cat((cat(id561in_in14,id561in_in15)),(cat(id561in_in16,id561in_in17)))),(cat((cat(id561in_in18,id561in_in19)),id561in_in20)))),(cat((cat((cat(id561in_in21,id561in_in22)),id561in_in23)),(cat((cat(id561in_in24,id561in_in25)),id561in_in26)))))))),(cat((cat((cat((cat((cat(id561in_in27,id561in_in28)),(cat(id561in_in29,id561in_in30)))),(cat((cat(id561in_in31,id561in_in32)),id561in_in33)))),(cat((cat((cat(id561in_in34,id561in_in35)),id561in_in36)),(cat((cat(id561in_in37,id561in_in38)),id561in_in39)))))),(cat((cat((cat((cat(id561in_in40,id561in_in41)),(cat(id561in_in42,id561in_in43)))),(cat((cat(id561in_in44,id561in_in45)),id561in_in46)))),(cat((cat((cat(id561in_in47,id561in_in48)),id561in_in49)),(cat((cat(id561in_in50,id561in_in51)),id561in_in52))))))))));
    id561out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id561in_in0_doubt,id561in_in1_doubt)),id561in_in2_doubt)),id561in_in3_doubt)),id561in_in4_doubt)),id561in_in5_doubt)),id561in_in6_doubt)),id561in_in7_doubt)),id561in_in8_doubt)),id561in_in9_doubt)),id561in_in10_doubt)),id561in_in11_doubt)),id561in_in12_doubt)),id561in_in13_doubt)),id561in_in14_doubt)),id561in_in15_doubt)),id561in_in16_doubt)),id561in_in17_doubt)),id561in_in18_doubt)),id561in_in19_doubt)),id561in_in20_doubt)),id561in_in21_doubt)),id561in_in22_doubt)),id561in_in23_doubt)),id561in_in24_doubt)),id561in_in25_doubt)),id561in_in26_doubt)),id561in_in27_doubt)),id561in_in28_doubt)),id561in_in29_doubt)),id561in_in30_doubt)),id561in_in31_doubt)),id561in_in32_doubt)),id561in_in33_doubt)),id561in_in34_doubt)),id561in_in35_doubt)),id561in_in36_doubt)),id561in_in37_doubt)),id561in_in38_doubt)),id561in_in39_doubt)),id561in_in40_doubt)),id561in_in41_doubt)),id561in_in42_doubt)),id561in_in43_doubt)),id561in_in44_doubt)),id561in_in45_doubt)),id561in_in46_doubt)),id561in_in47_doubt)),id561in_in48_doubt)),id561in_in49_doubt)),id561in_in50_doubt)),id561in_in51_doubt)),id561in_in52_doubt));
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id101out_output;
  HWOffsetFix<1,0,UNSIGNED> id101out_output_doubt;

  { // Node ID: 101 (NodeReinterpret)
    const HWRawBits<53> &id101in_input = id561out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id101in_input_doubt = id561out_result_doubt;

    id101out_output = (cast_bits2fixed<53,-43,TWOSCOMPLEMENT>(id101in_input));
    id101out_output_doubt = id101in_input_doubt;
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id102out_result;
  HWOffsetFix<1,0,UNSIGNED> id102out_result_doubt;

  { // Node ID: 102 (NodeXor)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id102in_a = id46out_value;
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id102in_b = id101out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id102in_b_doubt = id101out_output_doubt;

    HWOffsetFix<53,-43,TWOSCOMPLEMENT> id102x_1;

    (id102x_1) = (xor_fixed(id102in_a,id102in_b));
    id102out_result = (id102x_1);
    id102out_result_doubt = id102in_b_doubt;
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id103out_result;
  HWOffsetFix<1,0,UNSIGNED> id103out_result_doubt;

  { // Node ID: 103 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id103in_sel = id45out_output;
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id103in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id103in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id103in_option1 = id102out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id103in_option1_doubt = id102out_result_doubt;

    HWOffsetFix<53,-43,TWOSCOMPLEMENT> id103x_1;
    HWOffsetFix<1,0,UNSIGNED> id103x_2;

    switch((id103in_sel.getValueAsLong())) {
      case 0l:
        id103x_1 = id103in_option0;
        break;
      case 1l:
        id103x_1 = id103in_option1;
        break;
      default:
        id103x_1 = (c_hw_fix_53_n43_sgn_undef);
        break;
    }
    switch((id103in_sel.getValueAsLong())) {
      case 0l:
        id103x_2 = id103in_option0_doubt;
        break;
      case 1l:
        id103x_2 = id103in_option1_doubt;
        break;
      default:
        id103x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id103out_result = (id103x_1);
    id103out_result_doubt = (id103x_2);
  }
  { // Node ID: 106 (NodeConstantRawBits)
  }
  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id105out_result;
  HWOffsetFix<1,0,UNSIGNED> id105out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id105out_exception;

  { // Node ID: 105 (NodeMul)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id105in_a = id103out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id105in_a_doubt = id103out_result_doubt;
    const HWOffsetFix<52,-51,UNSIGNED> &id105in_b = id106out_value;

    HWOffsetFix<1,0,UNSIGNED> id105x_1;

    id105out_result = (mul_fixed<105,-94,TWOSCOMPLEMENT,TONEAREVEN>(id105in_a,id105in_b,(&(id105x_1))));
    id105out_result_doubt = (or_fixed((neq_fixed((id105x_1),(c_hw_fix_1_0_uns_bits_1))),id105in_a_doubt));
    id105out_exception = (id105x_1);
  }
  { // Node ID: 108 (NodeConstantRawBits)
  }
  HWRawBits<1> id562out_result;
  HWOffsetFix<1,0,UNSIGNED> id562out_result_doubt;

  { // Node ID: 562 (NodeSlice)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id562in_a = id105out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id562in_a_doubt = id105out_result_doubt;

    id562out_result = (slice<104,1>(id562in_a));
    id562out_result_doubt = id562in_a_doubt;
  }
  HWRawBits<1> id563out_result;
  HWOffsetFix<1,0,UNSIGNED> id563out_result_doubt;

  { // Node ID: 563 (NodeNot)
    const HWRawBits<1> &id563in_a = id562out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id563in_a_doubt = id562out_result_doubt;

    id563out_result = (not_bits(id563in_a));
    id563out_result_doubt = id563in_a_doubt;
  }
  HWRawBits<105> id667out_result;
  HWOffsetFix<1,0,UNSIGNED> id667out_result_doubt;

  { // Node ID: 667 (NodeCat)
    const HWRawBits<1> &id667in_in0 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in0_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in1 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in1_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in2 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in2_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in3 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in3_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in4 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in4_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in5 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in5_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in6 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in6_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in7 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in7_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in8 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in8_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in9 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in9_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in10 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in10_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in11 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in11_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in12 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in12_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in13 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in13_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in14 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in14_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in15 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in15_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in16 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in16_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in17 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in17_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in18 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in18_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in19 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in19_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in20 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in20_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in21 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in21_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in22 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in22_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in23 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in23_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in24 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in24_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in25 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in25_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in26 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in26_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in27 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in27_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in28 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in28_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in29 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in29_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in30 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in30_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in31 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in31_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in32 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in32_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in33 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in33_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in34 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in34_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in35 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in35_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in36 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in36_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in37 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in37_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in38 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in38_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in39 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in39_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in40 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in40_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in41 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in41_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in42 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in42_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in43 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in43_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in44 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in44_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in45 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in45_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in46 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in46_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in47 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in47_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in48 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in48_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in49 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in49_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in50 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in50_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in51 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in51_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in52 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in52_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in53 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in53_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in54 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in54_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in55 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in55_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in56 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in56_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in57 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in57_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in58 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in58_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in59 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in59_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in60 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in60_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in61 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in61_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in62 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in62_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in63 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in63_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in64 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in64_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in65 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in65_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in66 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in66_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in67 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in67_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in68 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in68_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in69 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in69_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in70 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in70_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in71 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in71_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in72 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in72_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in73 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in73_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in74 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in74_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in75 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in75_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in76 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in76_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in77 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in77_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in78 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in78_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in79 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in79_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in80 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in80_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in81 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in81_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in82 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in82_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in83 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in83_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in84 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in84_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in85 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in85_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in86 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in86_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in87 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in87_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in88 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in88_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in89 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in89_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in90 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in90_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in91 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in91_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in92 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in92_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in93 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in93_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in94 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in94_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in95 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in95_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in96 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in96_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in97 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in97_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in98 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in98_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in99 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in99_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in100 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in100_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in101 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in101_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in102 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in102_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in103 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in103_doubt = id563out_result_doubt;
    const HWRawBits<1> &id667in_in104 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_in104_doubt = id563out_result_doubt;

    id667out_result = (cat((cat((cat((cat((cat((cat((cat(id667in_in0,id667in_in1)),(cat(id667in_in2,id667in_in3)))),(cat((cat(id667in_in4,id667in_in5)),id667in_in6)))),(cat((cat((cat(id667in_in7,id667in_in8)),(cat(id667in_in9,id667in_in10)))),(cat((cat(id667in_in11,id667in_in12)),id667in_in13)))))),(cat((cat((cat((cat(id667in_in14,id667in_in15)),(cat(id667in_in16,id667in_in17)))),(cat((cat(id667in_in18,id667in_in19)),id667in_in20)))),(cat((cat((cat(id667in_in21,id667in_in22)),id667in_in23)),(cat((cat(id667in_in24,id667in_in25)),id667in_in26)))))))),(cat((cat((cat((cat((cat(id667in_in27,id667in_in28)),(cat(id667in_in29,id667in_in30)))),(cat((cat(id667in_in31,id667in_in32)),id667in_in33)))),(cat((cat((cat(id667in_in34,id667in_in35)),id667in_in36)),(cat((cat(id667in_in37,id667in_in38)),id667in_in39)))))),(cat((cat((cat((cat(id667in_in40,id667in_in41)),(cat(id667in_in42,id667in_in43)))),(cat((cat(id667in_in44,id667in_in45)),id667in_in46)))),(cat((cat((cat(id667in_in47,id667in_in48)),id667in_in49)),(cat((cat(id667in_in50,id667in_in51)),id667in_in52)))))))))),(cat((cat((cat((cat((cat((cat(id667in_in53,id667in_in54)),(cat(id667in_in55,id667in_in56)))),(cat((cat(id667in_in57,id667in_in58)),id667in_in59)))),(cat((cat((cat(id667in_in60,id667in_in61)),id667in_in62)),(cat((cat(id667in_in63,id667in_in64)),id667in_in65)))))),(cat((cat((cat((cat(id667in_in66,id667in_in67)),(cat(id667in_in68,id667in_in69)))),(cat((cat(id667in_in70,id667in_in71)),id667in_in72)))),(cat((cat((cat(id667in_in73,id667in_in74)),id667in_in75)),(cat((cat(id667in_in76,id667in_in77)),id667in_in78)))))))),(cat((cat((cat((cat((cat(id667in_in79,id667in_in80)),(cat(id667in_in81,id667in_in82)))),(cat((cat(id667in_in83,id667in_in84)),id667in_in85)))),(cat((cat((cat(id667in_in86,id667in_in87)),id667in_in88)),(cat((cat(id667in_in89,id667in_in90)),id667in_in91)))))),(cat((cat((cat((cat(id667in_in92,id667in_in93)),(cat(id667in_in94,id667in_in95)))),(cat((cat(id667in_in96,id667in_in97)),id667in_in98)))),(cat((cat((cat(id667in_in99,id667in_in100)),id667in_in101)),(cat((cat(id667in_in102,id667in_in103)),id667in_in104))))))))))));
    id667out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id667in_in0_doubt,id667in_in1_doubt)),id667in_in2_doubt)),id667in_in3_doubt)),id667in_in4_doubt)),id667in_in5_doubt)),id667in_in6_doubt)),id667in_in7_doubt)),id667in_in8_doubt)),id667in_in9_doubt)),id667in_in10_doubt)),id667in_in11_doubt)),id667in_in12_doubt)),id667in_in13_doubt)),id667in_in14_doubt)),id667in_in15_doubt)),id667in_in16_doubt)),id667in_in17_doubt)),id667in_in18_doubt)),id667in_in19_doubt)),id667in_in20_doubt)),id667in_in21_doubt)),id667in_in22_doubt)),id667in_in23_doubt)),id667in_in24_doubt)),id667in_in25_doubt)),id667in_in26_doubt)),id667in_in27_doubt)),id667in_in28_doubt)),id667in_in29_doubt)),id667in_in30_doubt)),id667in_in31_doubt)),id667in_in32_doubt)),id667in_in33_doubt)),id667in_in34_doubt)),id667in_in35_doubt)),id667in_in36_doubt)),id667in_in37_doubt)),id667in_in38_doubt)),id667in_in39_doubt)),id667in_in40_doubt)),id667in_in41_doubt)),id667in_in42_doubt)),id667in_in43_doubt)),id667in_in44_doubt)),id667in_in45_doubt)),id667in_in46_doubt)),id667in_in47_doubt)),id667in_in48_doubt)),id667in_in49_doubt)),id667in_in50_doubt)),id667in_in51_doubt)),id667in_in52_doubt)),id667in_in53_doubt)),id667in_in54_doubt)),id667in_in55_doubt)),id667in_in56_doubt)),id667in_in57_doubt)),id667in_in58_doubt)),id667in_in59_doubt)),id667in_in60_doubt)),id667in_in61_doubt)),id667in_in62_doubt)),id667in_in63_doubt)),id667in_in64_doubt)),id667in_in65_doubt)),id667in_in66_doubt)),id667in_in67_doubt)),id667in_in68_doubt)),id667in_in69_doubt)),id667in_in70_doubt)),id667in_in71_doubt)),id667in_in72_doubt)),id667in_in73_doubt)),id667in_in74_doubt)),id667in_in75_doubt)),id667in_in76_doubt)),id667in_in77_doubt)),id667in_in78_doubt)),id667in_in79_doubt)),id667in_in80_doubt)),id667in_in81_doubt)),id667in_in82_doubt)),id667in_in83_doubt)),id667in_in84_doubt)),id667in_in85_doubt)),id667in_in86_doubt)),id667in_in87_doubt)),id667in_in88_doubt)),id667in_in89_doubt)),id667in_in90_doubt)),id667in_in91_doubt)),id667in_in92_doubt)),id667in_in93_doubt)),id667in_in94_doubt)),id667in_in95_doubt)),id667in_in96_doubt)),id667in_in97_doubt)),id667in_in98_doubt)),id667in_in99_doubt)),id667in_in100_doubt)),id667in_in101_doubt)),id667in_in102_doubt)),id667in_in103_doubt)),id667in_in104_doubt));
  }
  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id215out_output;
  HWOffsetFix<1,0,UNSIGNED> id215out_output_doubt;

  { // Node ID: 215 (NodeReinterpret)
    const HWRawBits<105> &id215in_input = id667out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id215in_input_doubt = id667out_result_doubt;

    id215out_output = (cast_bits2fixed<105,-94,TWOSCOMPLEMENT>(id215in_input));
    id215out_output_doubt = id215in_input_doubt;
  }
  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id216out_result;
  HWOffsetFix<1,0,UNSIGNED> id216out_result_doubt;

  { // Node ID: 216 (NodeXor)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id216in_a = id108out_value;
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id216in_b = id215out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id216in_b_doubt = id215out_output_doubt;

    HWOffsetFix<105,-94,TWOSCOMPLEMENT> id216x_1;

    (id216x_1) = (xor_fixed(id216in_a,id216in_b));
    id216out_result = (id216x_1);
    id216out_result_doubt = id216in_b_doubt;
  }
  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id217out_result;
  HWOffsetFix<1,0,UNSIGNED> id217out_result_doubt;

  { // Node ID: 217 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id217in_sel = id105out_exception;
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id217in_option0 = id105out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id217in_option0_doubt = id105out_result_doubt;
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id217in_option1 = id216out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id217in_option1_doubt = id216out_result_doubt;

    HWOffsetFix<105,-94,TWOSCOMPLEMENT> id217x_1;
    HWOffsetFix<1,0,UNSIGNED> id217x_2;

    switch((id217in_sel.getValueAsLong())) {
      case 0l:
        id217x_1 = id217in_option0;
        break;
      case 1l:
        id217x_1 = id217in_option1;
        break;
      default:
        id217x_1 = (c_hw_fix_105_n94_sgn_undef);
        break;
    }
    switch((id217in_sel.getValueAsLong())) {
      case 0l:
        id217x_2 = id217in_option0_doubt;
        break;
      case 1l:
        id217x_2 = id217in_option1_doubt;
        break;
      default:
        id217x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id217out_result = (id217x_1);
    id217out_result_doubt = (id217x_2);
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id218out_o;
  HWOffsetFix<1,0,UNSIGNED> id218out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id218out_exception;

  { // Node ID: 218 (NodeCast)
    const HWOffsetFix<105,-94,TWOSCOMPLEMENT> &id218in_i = id217out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id218in_i_doubt = id217out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id218x_1;

    id218out_o = (cast_fixed2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id218in_i,(&(id218x_1))));
    id218out_o_doubt = (or_fixed((neq_fixed((id218x_1),(c_hw_fix_1_0_uns_bits_1))),id218in_i_doubt));
    id218out_exception = (id218x_1);
  }
  { // Node ID: 220 (NodeConstantRawBits)
  }
  HWRawBits<1> id668out_result;
  HWOffsetFix<1,0,UNSIGNED> id668out_result_doubt;

  { // Node ID: 668 (NodeSlice)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id668in_a = id218out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id668in_a_doubt = id218out_o_doubt;

    id668out_result = (slice<52,1>(id668in_a));
    id668out_result_doubt = id668in_a_doubt;
  }
  HWRawBits<1> id669out_result;
  HWOffsetFix<1,0,UNSIGNED> id669out_result_doubt;

  { // Node ID: 669 (NodeNot)
    const HWRawBits<1> &id669in_a = id668out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id669in_a_doubt = id668out_result_doubt;

    id669out_result = (not_bits(id669in_a));
    id669out_result_doubt = id669in_a_doubt;
  }
  HWRawBits<53> id721out_result;
  HWOffsetFix<1,0,UNSIGNED> id721out_result_doubt;

  { // Node ID: 721 (NodeCat)
    const HWRawBits<1> &id721in_in0 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in0_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in1 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in1_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in2 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in2_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in3 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in3_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in4 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in4_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in5 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in5_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in6 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in6_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in7 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in7_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in8 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in8_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in9 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in9_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in10 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in10_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in11 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in11_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in12 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in12_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in13 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in13_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in14 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in14_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in15 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in15_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in16 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in16_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in17 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in17_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in18 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in18_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in19 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in19_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in20 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in20_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in21 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in21_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in22 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in22_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in23 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in23_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in24 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in24_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in25 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in25_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in26 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in26_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in27 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in27_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in28 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in28_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in29 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in29_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in30 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in30_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in31 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in31_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in32 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in32_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in33 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in33_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in34 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in34_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in35 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in35_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in36 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in36_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in37 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in37_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in38 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in38_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in39 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in39_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in40 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in40_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in41 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in41_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in42 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in42_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in43 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in43_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in44 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in44_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in45 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in45_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in46 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in46_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in47 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in47_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in48 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in48_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in49 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in49_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in50 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in50_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in51 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in51_doubt = id669out_result_doubt;
    const HWRawBits<1> &id721in_in52 = id669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id721in_in52_doubt = id669out_result_doubt;

    id721out_result = (cat((cat((cat((cat((cat((cat(id721in_in0,id721in_in1)),(cat(id721in_in2,id721in_in3)))),(cat((cat(id721in_in4,id721in_in5)),id721in_in6)))),(cat((cat((cat(id721in_in7,id721in_in8)),(cat(id721in_in9,id721in_in10)))),(cat((cat(id721in_in11,id721in_in12)),id721in_in13)))))),(cat((cat((cat((cat(id721in_in14,id721in_in15)),(cat(id721in_in16,id721in_in17)))),(cat((cat(id721in_in18,id721in_in19)),id721in_in20)))),(cat((cat((cat(id721in_in21,id721in_in22)),id721in_in23)),(cat((cat(id721in_in24,id721in_in25)),id721in_in26)))))))),(cat((cat((cat((cat((cat(id721in_in27,id721in_in28)),(cat(id721in_in29,id721in_in30)))),(cat((cat(id721in_in31,id721in_in32)),id721in_in33)))),(cat((cat((cat(id721in_in34,id721in_in35)),id721in_in36)),(cat((cat(id721in_in37,id721in_in38)),id721in_in39)))))),(cat((cat((cat((cat(id721in_in40,id721in_in41)),(cat(id721in_in42,id721in_in43)))),(cat((cat(id721in_in44,id721in_in45)),id721in_in46)))),(cat((cat((cat(id721in_in47,id721in_in48)),id721in_in49)),(cat((cat(id721in_in50,id721in_in51)),id721in_in52))))))))));
    id721out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id721in_in0_doubt,id721in_in1_doubt)),id721in_in2_doubt)),id721in_in3_doubt)),id721in_in4_doubt)),id721in_in5_doubt)),id721in_in6_doubt)),id721in_in7_doubt)),id721in_in8_doubt)),id721in_in9_doubt)),id721in_in10_doubt)),id721in_in11_doubt)),id721in_in12_doubt)),id721in_in13_doubt)),id721in_in14_doubt)),id721in_in15_doubt)),id721in_in16_doubt)),id721in_in17_doubt)),id721in_in18_doubt)),id721in_in19_doubt)),id721in_in20_doubt)),id721in_in21_doubt)),id721in_in22_doubt)),id721in_in23_doubt)),id721in_in24_doubt)),id721in_in25_doubt)),id721in_in26_doubt)),id721in_in27_doubt)),id721in_in28_doubt)),id721in_in29_doubt)),id721in_in30_doubt)),id721in_in31_doubt)),id721in_in32_doubt)),id721in_in33_doubt)),id721in_in34_doubt)),id721in_in35_doubt)),id721in_in36_doubt)),id721in_in37_doubt)),id721in_in38_doubt)),id721in_in39_doubt)),id721in_in40_doubt)),id721in_in41_doubt)),id721in_in42_doubt)),id721in_in43_doubt)),id721in_in44_doubt)),id721in_in45_doubt)),id721in_in46_doubt)),id721in_in47_doubt)),id721in_in48_doubt)),id721in_in49_doubt)),id721in_in50_doubt)),id721in_in51_doubt)),id721in_in52_doubt));
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id275out_output;
  HWOffsetFix<1,0,UNSIGNED> id275out_output_doubt;

  { // Node ID: 275 (NodeReinterpret)
    const HWRawBits<53> &id275in_input = id721out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id275in_input_doubt = id721out_result_doubt;

    id275out_output = (cast_bits2fixed<53,-43,TWOSCOMPLEMENT>(id275in_input));
    id275out_output_doubt = id275in_input_doubt;
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id276out_result;
  HWOffsetFix<1,0,UNSIGNED> id276out_result_doubt;

  { // Node ID: 276 (NodeXor)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id276in_a = id220out_value;
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id276in_b = id275out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id276in_b_doubt = id275out_output_doubt;

    HWOffsetFix<53,-43,TWOSCOMPLEMENT> id276x_1;

    (id276x_1) = (xor_fixed(id276in_a,id276in_b));
    id276out_result = (id276x_1);
    id276out_result_doubt = id276in_b_doubt;
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id277out_result;
  HWOffsetFix<1,0,UNSIGNED> id277out_result_doubt;

  { // Node ID: 277 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id277in_sel = id218out_exception;
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id277in_option0 = id218out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id277in_option0_doubt = id218out_o_doubt;
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id277in_option1 = id276out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id277in_option1_doubt = id276out_result_doubt;

    HWOffsetFix<53,-43,TWOSCOMPLEMENT> id277x_1;
    HWOffsetFix<1,0,UNSIGNED> id277x_2;

    switch((id277in_sel.getValueAsLong())) {
      case 0l:
        id277x_1 = id277in_option0;
        break;
      case 1l:
        id277x_1 = id277in_option1;
        break;
      default:
        id277x_1 = (c_hw_fix_53_n43_sgn_undef);
        break;
    }
    switch((id277in_sel.getValueAsLong())) {
      case 0l:
        id277x_2 = id277in_option0_doubt;
        break;
      case 1l:
        id277x_2 = id277in_option1_doubt;
        break;
      default:
        id277x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id277out_result = (id277x_1);
    id277out_result_doubt = (id277x_2);
  }
  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id286out_output;

  { // Node ID: 286 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id286in_input = id277out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id286in_input_doubt = id277out_result_doubt;

    id286out_output = id286in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id287out_o;

  { // Node ID: 287 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id287in_i = id286out_output;

    id287out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id287in_i));
  }
  { // Node ID: 804 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id804in_input = id287out_o;

    id804out_output[(getCycle()+2)%3] = id804in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id347out_o;

  { // Node ID: 347 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id347in_i = id804out_output[getCycle()%3];

    id347out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id347in_i));
  }
  { // Node ID: 837 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id349out_result;
  HWOffsetFix<1,0,UNSIGNED> id349out_exception;

  { // Node ID: 349 (NodeAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id349in_a = id347out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id349in_b = id837out_value;

    HWOffsetFix<1,0,UNSIGNED> id349x_1;

    id349out_result = (add_fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id349in_a,id349in_b,(&(id349x_1))));
    id349out_exception = (id349x_1);
  }
  { // Node ID: 351 (NodeConstantRawBits)
  }
  HWRawBits<1> id722out_result;

  { // Node ID: 722 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id722in_a = id349out_result;

    id722out_result = (slice<10,1>(id722in_a));
  }
  HWRawBits<1> id723out_result;

  { // Node ID: 723 (NodeNot)
    const HWRawBits<1> &id723in_a = id722out_result;

    id723out_result = (not_bits(id723in_a));
  }
  HWRawBits<11> id733out_result;

  { // Node ID: 733 (NodeCat)
    const HWRawBits<1> &id733in_in0 = id723out_result;
    const HWRawBits<1> &id733in_in1 = id723out_result;
    const HWRawBits<1> &id733in_in2 = id723out_result;
    const HWRawBits<1> &id733in_in3 = id723out_result;
    const HWRawBits<1> &id733in_in4 = id723out_result;
    const HWRawBits<1> &id733in_in5 = id723out_result;
    const HWRawBits<1> &id733in_in6 = id723out_result;
    const HWRawBits<1> &id733in_in7 = id723out_result;
    const HWRawBits<1> &id733in_in8 = id723out_result;
    const HWRawBits<1> &id733in_in9 = id723out_result;
    const HWRawBits<1> &id733in_in10 = id723out_result;

    id733out_result = (cat((cat((cat((cat(id733in_in0,id733in_in1)),id733in_in2)),(cat((cat(id733in_in3,id733in_in4)),id733in_in5)))),(cat((cat((cat(id733in_in6,id733in_in7)),id733in_in8)),(cat(id733in_in9,id733in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id364out_output;

  { // Node ID: 364 (NodeReinterpret)
    const HWRawBits<11> &id364in_input = id733out_result;

    id364out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id364in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id365out_result;

  { // Node ID: 365 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id365in_a = id351out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id365in_b = id364out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id365x_1;

    (id365x_1) = (xor_fixed(id365in_a,id365in_b));
    id365out_result = (id365x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id366out_result;

  { // Node ID: 366 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id366in_sel = id349out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id366in_option0 = id349out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id366in_option1 = id365out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id366x_1;

    switch((id366in_sel.getValueAsLong())) {
      case 0l:
        id366x_1 = id366in_option0;
        break;
      case 1l:
        id366x_1 = id366in_option1;
        break;
      default:
        id366x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id366out_result = (id366x_1);
  }
  { // Node ID: 367 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-21,UNSIGNED> id290out_o;

  { // Node ID: 290 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id290in_i = id286out_output;

    id290out_o = (cast_fixed2fixed<10,-21,UNSIGNED,TRUNCATE>(id290in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id443out_output;

  { // Node ID: 443 (NodeReinterpret)
    const HWOffsetFix<10,-21,UNSIGNED> &id443in_input = id290out_o;

    id443out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id443in_input))));
  }
  { // Node ID: 444 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id444in_addr = id443out_output;

    HWOffsetFix<29,-40,UNSIGNED> id444x_1;

    switch(((long)((id444in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id444x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
      case 1l:
        id444x_1 = (id444sta_rom_store[(id444in_addr.getValueAsLong())]);
        break;
      default:
        id444x_1 = (c_hw_fix_29_n40_uns_undef);
        break;
    }
    id444out_dout[(getCycle()+2)%3] = (id444x_1);
  }
  HWOffsetFix<10,-11,UNSIGNED> id289out_o;

  { // Node ID: 289 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id289in_i = id286out_output;

    id289out_o = (cast_fixed2fixed<10,-11,UNSIGNED,TRUNCATE>(id289in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id440out_output;

  { // Node ID: 440 (NodeReinterpret)
    const HWOffsetFix<10,-11,UNSIGNED> &id440in_input = id289out_o;

    id440out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id440in_input))));
  }
  { // Node ID: 441 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id441in_addr = id440out_output;

    HWOffsetFix<39,-40,UNSIGNED> id441x_1;

    switch(((long)((id441in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id441x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
      case 1l:
        id441x_1 = (id441sta_rom_store[(id441in_addr.getValueAsLong())]);
        break;
      default:
        id441x_1 = (c_hw_fix_39_n40_uns_undef);
        break;
    }
    id441out_dout[(getCycle()+2)%3] = (id441x_1);
  }
  HWOffsetFix<1,-1,UNSIGNED> id288out_o;

  { // Node ID: 288 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id288in_i = id286out_output;

    id288out_o = (cast_fixed2fixed<1,-1,UNSIGNED,TRUNCATE>(id288in_i));
  }
  HWOffsetFix<1,0,UNSIGNED> id437out_output;

  { // Node ID: 437 (NodeReinterpret)
    const HWOffsetFix<1,-1,UNSIGNED> &id437in_input = id288out_o;

    id437out_output = (cast_bits2fixed<1,0,UNSIGNED>((cast_fixed2bits(id437in_input))));
  }
  { // Node ID: 438 (NodeROM)
    const HWOffsetFix<1,0,UNSIGNED> &id438in_addr = id437out_output;

    HWOffsetFix<40,-40,UNSIGNED> id438x_1;

    switch(((long)((id438in_addr.getValueAsLong())<(2l)))) {
      case 0l:
        id438x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
      case 1l:
        id438x_1 = (id438sta_rom_store[(id438in_addr.getValueAsLong())]);
        break;
      default:
        id438x_1 = (c_hw_fix_40_n40_uns_undef);
        break;
    }
    id438out_dout[(getCycle()+2)%3] = (id438x_1);
  }
  { // Node ID: 294 (NodeConstantRawBits)
  }
  HWOffsetFix<22,-43,UNSIGNED> id291out_o;

  { // Node ID: 291 (NodeCast)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id291in_i = id286out_output;

    id291out_o = (cast_fixed2fixed<22,-43,UNSIGNED,TRUNCATE>(id291in_i));
  }
  { // Node ID: 805 (NodeFIFO)
    const HWOffsetFix<22,-43,UNSIGNED> &id805in_input = id291out_o;

    id805out_output[(getCycle()+2)%3] = id805in_input;
  }
  HWOffsetFix<43,-64,UNSIGNED> id293out_result;

  { // Node ID: 293 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id293in_a = id294out_value;
    const HWOffsetFix<22,-43,UNSIGNED> &id293in_b = id805out_output[getCycle()%3];

    id293out_result = (mul_fixed<43,-64,UNSIGNED,TONEAREVEN>(id293in_a,id293in_b));
  }
  HWOffsetFix<22,-43,UNSIGNED> id295out_o;
  HWOffsetFix<1,0,UNSIGNED> id295out_exception;

  { // Node ID: 295 (NodeCast)
    const HWOffsetFix<43,-64,UNSIGNED> &id295in_i = id293out_result;

    HWOffsetFix<1,0,UNSIGNED> id295x_1;

    id295out_o = (cast_fixed2fixed<22,-43,UNSIGNED,TONEAREVEN>(id295in_i,(&(id295x_1))));
    id295out_exception = (id295x_1);
  }
  { // Node ID: 297 (NodeConstantRawBits)
  }
  HWOffsetFix<22,-43,UNSIGNED> id298out_result;

  { // Node ID: 298 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id298in_sel = id295out_exception;
    const HWOffsetFix<22,-43,UNSIGNED> &id298in_option0 = id295out_o;
    const HWOffsetFix<22,-43,UNSIGNED> &id298in_option1 = id297out_value;

    HWOffsetFix<22,-43,UNSIGNED> id298x_1;

    switch((id298in_sel.getValueAsLong())) {
      case 0l:
        id298x_1 = id298in_option0;
        break;
      case 1l:
        id298x_1 = id298in_option1;
        break;
      default:
        id298x_1 = (c_hw_fix_22_n43_uns_undef);
        break;
    }
    id298out_result = (id298x_1);
  }
  HWOffsetFix<44,-43,UNSIGNED> id299out_result;

  { // Node ID: 299 (NodeAdd)
    const HWOffsetFix<40,-40,UNSIGNED> &id299in_a = id438out_dout[getCycle()%3];
    const HWOffsetFix<22,-43,UNSIGNED> &id299in_b = id298out_result;

    id299out_result = (add_fixed<44,-43,UNSIGNED,TONEAREVEN>(id299in_a,id299in_b));
  }
  HWOffsetFix<62,-83,UNSIGNED> id300out_result;

  { // Node ID: 300 (NodeMul)
    const HWOffsetFix<22,-43,UNSIGNED> &id300in_a = id298out_result;
    const HWOffsetFix<40,-40,UNSIGNED> &id300in_b = id438out_dout[getCycle()%3];

    id300out_result = (mul_fixed<62,-83,UNSIGNED,TONEAREVEN>(id300in_a,id300in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id301out_result;
  HWOffsetFix<1,0,UNSIGNED> id301out_exception;

  { // Node ID: 301 (NodeAdd)
    const HWOffsetFix<44,-43,UNSIGNED> &id301in_a = id299out_result;
    const HWOffsetFix<62,-83,UNSIGNED> &id301in_b = id300out_result;

    HWOffsetFix<1,0,UNSIGNED> id301x_1;

    id301out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id301in_a,id301in_b,(&(id301x_1))));
    id301out_exception = (id301x_1);
  }
  { // Node ID: 303 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-63,UNSIGNED> id304out_result;

  { // Node ID: 304 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id304in_sel = id301out_exception;
    const HWOffsetFix<64,-63,UNSIGNED> &id304in_option0 = id301out_result;
    const HWOffsetFix<64,-63,UNSIGNED> &id304in_option1 = id303out_value;

    HWOffsetFix<64,-63,UNSIGNED> id304x_1;

    switch((id304in_sel.getValueAsLong())) {
      case 0l:
        id304x_1 = id304in_option0;
        break;
      case 1l:
        id304x_1 = id304in_option1;
        break;
      default:
        id304x_1 = (c_hw_fix_64_n63_uns_undef);
        break;
    }
    id304out_result = (id304x_1);
  }
  HWOffsetFix<44,-43,UNSIGNED> id305out_o;
  HWOffsetFix<1,0,UNSIGNED> id305out_exception;

  { // Node ID: 305 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id305in_i = id304out_result;

    HWOffsetFix<1,0,UNSIGNED> id305x_1;

    id305out_o = (cast_fixed2fixed<44,-43,UNSIGNED,TONEAREVEN>(id305in_i,(&(id305x_1))));
    id305out_exception = (id305x_1);
  }
  { // Node ID: 307 (NodeConstantRawBits)
  }
  HWOffsetFix<44,-43,UNSIGNED> id308out_result;

  { // Node ID: 308 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id308in_sel = id305out_exception;
    const HWOffsetFix<44,-43,UNSIGNED> &id308in_option0 = id305out_o;
    const HWOffsetFix<44,-43,UNSIGNED> &id308in_option1 = id307out_value;

    HWOffsetFix<44,-43,UNSIGNED> id308x_1;

    switch((id308in_sel.getValueAsLong())) {
      case 0l:
        id308x_1 = id308in_option0;
        break;
      case 1l:
        id308x_1 = id308in_option1;
        break;
      default:
        id308x_1 = (c_hw_fix_44_n43_uns_undef);
        break;
    }
    id308out_result = (id308x_1);
  }
  HWOffsetFix<45,-43,UNSIGNED> id309out_result;

  { // Node ID: 309 (NodeAdd)
    const HWOffsetFix<39,-40,UNSIGNED> &id309in_a = id441out_dout[getCycle()%3];
    const HWOffsetFix<44,-43,UNSIGNED> &id309in_b = id308out_result;

    id309out_result = (add_fixed<45,-43,UNSIGNED,TONEAREVEN>(id309in_a,id309in_b));
  }
  HWOffsetFix<64,-64,UNSIGNED> id310out_result;
  HWOffsetFix<1,0,UNSIGNED> id310out_exception;

  { // Node ID: 310 (NodeMul)
    const HWOffsetFix<44,-43,UNSIGNED> &id310in_a = id308out_result;
    const HWOffsetFix<39,-40,UNSIGNED> &id310in_b = id441out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id310x_1;

    id310out_result = (mul_fixed<64,-64,UNSIGNED,TONEAREVEN>(id310in_a,id310in_b,(&(id310x_1))));
    id310out_exception = (id310x_1);
  }
  { // Node ID: 312 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-64,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_sel = id310out_exception;
    const HWOffsetFix<64,-64,UNSIGNED> &id313in_option0 = id310out_result;
    const HWOffsetFix<64,-64,UNSIGNED> &id313in_option1 = id312out_value;

    HWOffsetFix<64,-64,UNSIGNED> id313x_1;

    switch((id313in_sel.getValueAsLong())) {
      case 0l:
        id313x_1 = id313in_option0;
        break;
      case 1l:
        id313x_1 = id313in_option1;
        break;
      default:
        id313x_1 = (c_hw_fix_64_n64_uns_undef);
        break;
    }
    id313out_result = (id313x_1);
  }
  HWOffsetFix<64,-62,UNSIGNED> id314out_result;
  HWOffsetFix<1,0,UNSIGNED> id314out_exception;

  { // Node ID: 314 (NodeAdd)
    const HWOffsetFix<45,-43,UNSIGNED> &id314in_a = id309out_result;
    const HWOffsetFix<64,-64,UNSIGNED> &id314in_b = id313out_result;

    HWOffsetFix<1,0,UNSIGNED> id314x_1;

    id314out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id314in_a,id314in_b,(&(id314x_1))));
    id314out_exception = (id314x_1);
  }
  { // Node ID: 316 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-62,UNSIGNED> id317out_result;

  { // Node ID: 317 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id317in_sel = id314out_exception;
    const HWOffsetFix<64,-62,UNSIGNED> &id317in_option0 = id314out_result;
    const HWOffsetFix<64,-62,UNSIGNED> &id317in_option1 = id316out_value;

    HWOffsetFix<64,-62,UNSIGNED> id317x_1;

    switch((id317in_sel.getValueAsLong())) {
      case 0l:
        id317x_1 = id317in_option0;
        break;
      case 1l:
        id317x_1 = id317in_option1;
        break;
      default:
        id317x_1 = (c_hw_fix_64_n62_uns_undef);
        break;
    }
    id317out_result = (id317x_1);
  }
  HWOffsetFix<45,-43,UNSIGNED> id318out_o;
  HWOffsetFix<1,0,UNSIGNED> id318out_exception;

  { // Node ID: 318 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id318in_i = id317out_result;

    HWOffsetFix<1,0,UNSIGNED> id318x_1;

    id318out_o = (cast_fixed2fixed<45,-43,UNSIGNED,TONEAREVEN>(id318in_i,(&(id318x_1))));
    id318out_exception = (id318x_1);
  }
  { // Node ID: 320 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-43,UNSIGNED> id321out_result;

  { // Node ID: 321 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id321in_sel = id318out_exception;
    const HWOffsetFix<45,-43,UNSIGNED> &id321in_option0 = id318out_o;
    const HWOffsetFix<45,-43,UNSIGNED> &id321in_option1 = id320out_value;

    HWOffsetFix<45,-43,UNSIGNED> id321x_1;

    switch((id321in_sel.getValueAsLong())) {
      case 0l:
        id321x_1 = id321in_option0;
        break;
      case 1l:
        id321x_1 = id321in_option1;
        break;
      default:
        id321x_1 = (c_hw_fix_45_n43_uns_undef);
        break;
    }
    id321out_result = (id321x_1);
  }
  HWOffsetFix<46,-43,UNSIGNED> id322out_result;

  { // Node ID: 322 (NodeAdd)
    const HWOffsetFix<29,-40,UNSIGNED> &id322in_a = id444out_dout[getCycle()%3];
    const HWOffsetFix<45,-43,UNSIGNED> &id322in_b = id321out_result;

    id322out_result = (add_fixed<46,-43,UNSIGNED,TONEAREVEN>(id322in_a,id322in_b));
  }
  HWOffsetFix<64,-73,UNSIGNED> id323out_result;
  HWOffsetFix<1,0,UNSIGNED> id323out_exception;

  { // Node ID: 323 (NodeMul)
    const HWOffsetFix<45,-43,UNSIGNED> &id323in_a = id321out_result;
    const HWOffsetFix<29,-40,UNSIGNED> &id323in_b = id444out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id323x_1;

    id323out_result = (mul_fixed<64,-73,UNSIGNED,TONEAREVEN>(id323in_a,id323in_b,(&(id323x_1))));
    id323out_exception = (id323x_1);
  }
  { // Node ID: 325 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-73,UNSIGNED> id326out_result;

  { // Node ID: 326 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id326in_sel = id323out_exception;
    const HWOffsetFix<64,-73,UNSIGNED> &id326in_option0 = id323out_result;
    const HWOffsetFix<64,-73,UNSIGNED> &id326in_option1 = id325out_value;

    HWOffsetFix<64,-73,UNSIGNED> id326x_1;

    switch((id326in_sel.getValueAsLong())) {
      case 0l:
        id326x_1 = id326in_option0;
        break;
      case 1l:
        id326x_1 = id326in_option1;
        break;
      default:
        id326x_1 = (c_hw_fix_64_n73_uns_undef);
        break;
    }
    id326out_result = (id326x_1);
  }
  HWOffsetFix<64,-61,UNSIGNED> id327out_result;
  HWOffsetFix<1,0,UNSIGNED> id327out_exception;

  { // Node ID: 327 (NodeAdd)
    const HWOffsetFix<46,-43,UNSIGNED> &id327in_a = id322out_result;
    const HWOffsetFix<64,-73,UNSIGNED> &id327in_b = id326out_result;

    HWOffsetFix<1,0,UNSIGNED> id327x_1;

    id327out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id327in_a,id327in_b,(&(id327x_1))));
    id327out_exception = (id327x_1);
  }
  { // Node ID: 329 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-61,UNSIGNED> id330out_result;

  { // Node ID: 330 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id330in_sel = id327out_exception;
    const HWOffsetFix<64,-61,UNSIGNED> &id330in_option0 = id327out_result;
    const HWOffsetFix<64,-61,UNSIGNED> &id330in_option1 = id329out_value;

    HWOffsetFix<64,-61,UNSIGNED> id330x_1;

    switch((id330in_sel.getValueAsLong())) {
      case 0l:
        id330x_1 = id330in_option0;
        break;
      case 1l:
        id330x_1 = id330in_option1;
        break;
      default:
        id330x_1 = (c_hw_fix_64_n61_uns_undef);
        break;
    }
    id330out_result = (id330x_1);
  }
  HWOffsetFix<46,-43,UNSIGNED> id331out_o;
  HWOffsetFix<1,0,UNSIGNED> id331out_exception;

  { // Node ID: 331 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id331in_i = id330out_result;

    HWOffsetFix<1,0,UNSIGNED> id331x_1;

    id331out_o = (cast_fixed2fixed<46,-43,UNSIGNED,TONEAREVEN>(id331in_i,(&(id331x_1))));
    id331out_exception = (id331x_1);
  }
  { // Node ID: 333 (NodeConstantRawBits)
  }
  HWOffsetFix<46,-43,UNSIGNED> id334out_result;

  { // Node ID: 334 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id334in_sel = id331out_exception;
    const HWOffsetFix<46,-43,UNSIGNED> &id334in_option0 = id331out_o;
    const HWOffsetFix<46,-43,UNSIGNED> &id334in_option1 = id333out_value;

    HWOffsetFix<46,-43,UNSIGNED> id334x_1;

    switch((id334in_sel.getValueAsLong())) {
      case 0l:
        id334x_1 = id334in_option0;
        break;
      case 1l:
        id334x_1 = id334in_option1;
        break;
      default:
        id334x_1 = (c_hw_fix_46_n43_uns_undef);
        break;
    }
    id334out_result = (id334x_1);
  }
  HWOffsetFix<40,-39,UNSIGNED> id335out_o;
  HWOffsetFix<1,0,UNSIGNED> id335out_exception;

  { // Node ID: 335 (NodeCast)
    const HWOffsetFix<46,-43,UNSIGNED> &id335in_i = id334out_result;

    HWOffsetFix<1,0,UNSIGNED> id335x_1;

    id335out_o = (cast_fixed2fixed<40,-39,UNSIGNED,TONEAREVEN>(id335in_i,(&(id335x_1))));
    id335out_exception = (id335x_1);
  }
  { // Node ID: 337 (NodeConstantRawBits)
  }
  HWOffsetFix<40,-39,UNSIGNED> id338out_result;

  { // Node ID: 338 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id338in_sel = id335out_exception;
    const HWOffsetFix<40,-39,UNSIGNED> &id338in_option0 = id335out_o;
    const HWOffsetFix<40,-39,UNSIGNED> &id338in_option1 = id337out_value;

    HWOffsetFix<40,-39,UNSIGNED> id338x_1;

    switch((id338in_sel.getValueAsLong())) {
      case 0l:
        id338x_1 = id338in_option0;
        break;
      case 1l:
        id338x_1 = id338in_option1;
        break;
      default:
        id338x_1 = (c_hw_fix_40_n39_uns_undef);
        break;
    }
    id338out_result = (id338x_1);
  }
  { // Node ID: 836 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id734out_result;

  { // Node ID: 734 (NodeGteInlined)
    const HWOffsetFix<40,-39,UNSIGNED> &id734in_a = id338out_result;
    const HWOffsetFix<40,-39,UNSIGNED> &id734in_b = id836out_value;

    id734out_result = (gte_fixed(id734in_a,id734in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id764out_result;
  HWOffsetFix<1,0,UNSIGNED> id764out_exception;

  { // Node ID: 764 (NodeCondAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id764in_a = id366out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id764in_b = id367out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id764in_condb = id734out_result;

    HWOffsetFix<1,0,UNSIGNED> id764x_1;
    HWOffsetFix<1,0,UNSIGNED> id764x_2;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id764x_3;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id764x_4;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id764x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id764x_3 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id764x_3 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id764in_a));
        break;
      default:
        id764x_3 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    switch((id764in_condb.getValueAsLong())) {
      case 0l:
        id764x_4 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id764x_4 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id764in_b));
        break;
      default:
        id764x_4 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    (id764x_5) = (add_fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>((id764x_3),(id764x_4),(&(id764x_1))));
    id764out_result = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id764x_5),(&(id764x_2))));
    id764out_exception = (or_fixed((id764x_1),(id764x_2)));
  }
  { // Node ID: 372 (NodeConstantRawBits)
  }
  HWRawBits<1> id735out_result;

  { // Node ID: 735 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id735in_a = id764out_result;

    id735out_result = (slice<10,1>(id735in_a));
  }
  HWRawBits<1> id736out_result;

  { // Node ID: 736 (NodeNot)
    const HWRawBits<1> &id736in_a = id735out_result;

    id736out_result = (not_bits(id736in_a));
  }
  HWRawBits<11> id746out_result;

  { // Node ID: 746 (NodeCat)
    const HWRawBits<1> &id746in_in0 = id736out_result;
    const HWRawBits<1> &id746in_in1 = id736out_result;
    const HWRawBits<1> &id746in_in2 = id736out_result;
    const HWRawBits<1> &id746in_in3 = id736out_result;
    const HWRawBits<1> &id746in_in4 = id736out_result;
    const HWRawBits<1> &id746in_in5 = id736out_result;
    const HWRawBits<1> &id746in_in6 = id736out_result;
    const HWRawBits<1> &id746in_in7 = id736out_result;
    const HWRawBits<1> &id746in_in8 = id736out_result;
    const HWRawBits<1> &id746in_in9 = id736out_result;
    const HWRawBits<1> &id746in_in10 = id736out_result;

    id746out_result = (cat((cat((cat((cat(id746in_in0,id746in_in1)),id746in_in2)),(cat((cat(id746in_in3,id746in_in4)),id746in_in5)))),(cat((cat((cat(id746in_in6,id746in_in7)),id746in_in8)),(cat(id746in_in9,id746in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id385out_output;

  { // Node ID: 385 (NodeReinterpret)
    const HWRawBits<11> &id385in_input = id746out_result;

    id385out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id385in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id386out_result;

  { // Node ID: 386 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id386in_a = id372out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id386in_b = id385out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id386x_1;

    (id386x_1) = (xor_fixed(id386in_a,id386in_b));
    id386out_result = (id386x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id387out_result;

  { // Node ID: 387 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id387in_sel = id764out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id387in_option0 = id764out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id387in_option1 = id386out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id387x_1;

    switch((id387in_sel.getValueAsLong())) {
      case 0l:
        id387x_1 = id387in_option0;
        break;
      case 1l:
        id387x_1 = id387in_option1;
        break;
      default:
        id387x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id387out_result = (id387x_1);
  }
  HWRawBits<1> id747out_result;

  { // Node ID: 747 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id747in_a = id387out_result;

    id747out_result = (slice<10,1>(id747in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id748out_output;

  { // Node ID: 748 (NodeReinterpret)
    const HWRawBits<1> &id748in_input = id747out_result;

    id748out_output = (cast_bits2fixed<1,0,UNSIGNED>(id748in_input));
  }
  { // Node ID: 835 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id279out_result;

  { // Node ID: 279 (NodeGt)
    const HWFloat<8,40> &id279in_a = id765out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id279in_b = id835out_value;

    id279out_result = (gt_float(id279in_a,id279in_b));
  }
  { // Node ID: 806 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id806in_input = id279out_result;

    id806out_output[(getCycle()+6)%7] = id806in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id280out_output;

  { // Node ID: 280 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id280in_input = id277out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id280in_input_doubt = id277out_result_doubt;

    id280out_output = id280in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id281out_result;

  { // Node ID: 281 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id281in_a = id806out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id281in_b = id280out_output;

    HWOffsetFix<1,0,UNSIGNED> id281x_1;

    (id281x_1) = (and_fixed(id281in_a,id281in_b));
    id281out_result = (id281x_1);
  }
  { // Node ID: 807 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id807in_input = id281out_result;

    id807out_output[(getCycle()+2)%3] = id807in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id390out_result;

  { // Node ID: 390 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id390in_a = id807out_output[getCycle()%3];

    id390out_result = (not_fixed(id390in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id391out_result;

  { // Node ID: 391 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id391in_a = id748out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id391in_b = id390out_result;

    HWOffsetFix<1,0,UNSIGNED> id391x_1;

    (id391x_1) = (and_fixed(id391in_a,id391in_b));
    id391out_result = (id391x_1);
  }
  { // Node ID: 834 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeLt)
    const HWFloat<8,40> &id283in_a = id765out_floatOut[getCycle()%2];
    const HWFloat<8,40> &id283in_b = id834out_value;

    id283out_result = (lt_float(id283in_a,id283in_b));
  }
  { // Node ID: 808 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id808in_input = id283out_result;

    id808out_output[(getCycle()+6)%7] = id808in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id284out_output;

  { // Node ID: 284 (NodeDoubtBitOp)
    const HWOffsetFix<53,-43,TWOSCOMPLEMENT> &id284in_input = id277out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id284in_input_doubt = id277out_result_doubt;

    id284out_output = id284in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id285in_a = id808out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id285in_b = id284out_output;

    HWOffsetFix<1,0,UNSIGNED> id285x_1;

    (id285x_1) = (and_fixed(id285in_a,id285in_b));
    id285out_result = (id285x_1);
  }
  { // Node ID: 809 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id809in_input = id285out_result;

    id809out_output[(getCycle()+2)%3] = id809in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id392out_result;

  { // Node ID: 392 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id392in_a = id391out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id392in_b = id809out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id392x_1;

    (id392x_1) = (or_fixed(id392in_a,id392in_b));
    id392out_result = (id392x_1);
  }
  { // Node ID: 833 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id749out_result;

  { // Node ID: 749 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id749in_a = id387out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id749in_b = id833out_value;

    id749out_result = (gte_fixed(id749in_a,id749in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id413out_result;

  { // Node ID: 413 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id413in_a = id809out_output[getCycle()%3];

    id413out_result = (not_fixed(id413in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id414out_result;

  { // Node ID: 414 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id414in_a = id749out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id414in_b = id413out_result;

    HWOffsetFix<1,0,UNSIGNED> id414x_1;

    (id414x_1) = (and_fixed(id414in_a,id414in_b));
    id414out_result = (id414x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id415out_result;

  { // Node ID: 415 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id415in_a = id414out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id415in_b = id807out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id415x_1;

    (id415x_1) = (or_fixed(id415in_a,id415in_b));
    id415out_result = (id415x_1);
  }
  HWRawBits<2> id416out_result;

  { // Node ID: 416 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id416in_in0 = id392out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id416in_in1 = id415out_result;

    id416out_result = (cat(id416in_in0,id416in_in1));
  }
  { // Node ID: 408 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id393out_o;
  HWOffsetFix<1,0,UNSIGNED> id393out_exception;

  { // Node ID: 393 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id393in_i = id387out_result;

    HWOffsetFix<1,0,UNSIGNED> id393x_1;

    id393out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id393in_i,(&(id393x_1))));
    id393out_exception = (id393x_1);
  }
  { // Node ID: 395 (NodeConstantRawBits)
  }
  HWRawBits<1> id750out_result;

  { // Node ID: 750 (NodeSlice)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id750in_a = id393out_o;

    id750out_result = (slice<7,1>(id750in_a));
  }
  HWRawBits<1> id751out_result;

  { // Node ID: 751 (NodeNot)
    const HWRawBits<1> &id751in_a = id750out_result;

    id751out_result = (not_bits(id751in_a));
  }
  HWRawBits<8> id758out_result;

  { // Node ID: 758 (NodeCat)
    const HWRawBits<1> &id758in_in0 = id751out_result;
    const HWRawBits<1> &id758in_in1 = id751out_result;
    const HWRawBits<1> &id758in_in2 = id751out_result;
    const HWRawBits<1> &id758in_in3 = id751out_result;
    const HWRawBits<1> &id758in_in4 = id751out_result;
    const HWRawBits<1> &id758in_in5 = id751out_result;
    const HWRawBits<1> &id758in_in6 = id751out_result;
    const HWRawBits<1> &id758in_in7 = id751out_result;

    id758out_result = (cat((cat((cat(id758in_in0,id758in_in1)),(cat(id758in_in2,id758in_in3)))),(cat((cat(id758in_in4,id758in_in5)),(cat(id758in_in6,id758in_in7))))));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id405out_output;

  { // Node ID: 405 (NodeReinterpret)
    const HWRawBits<8> &id405in_input = id758out_result;

    id405out_output = (cast_bits2fixed<8,0,TWOSCOMPLEMENT>(id405in_input));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id406out_result;

  { // Node ID: 406 (NodeXor)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id406in_a = id395out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id406in_b = id405out_output;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id406x_1;

    (id406x_1) = (xor_fixed(id406in_a,id406in_b));
    id406out_result = (id406x_1);
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id407out_result;

  { // Node ID: 407 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id407in_sel = id393out_exception;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id407in_option0 = id393out_o;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id407in_option1 = id406out_result;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id407x_1;

    switch((id407in_sel.getValueAsLong())) {
      case 0l:
        id407x_1 = id407in_option0;
        break;
      case 1l:
        id407x_1 = id407in_option1;
        break;
      default:
        id407x_1 = (c_hw_fix_8_0_sgn_undef);
        break;
    }
    id407out_result = (id407x_1);
  }
  { // Node ID: 341 (NodeConstantRawBits)
  }
  HWOffsetFix<40,-39,UNSIGNED> id342out_result;

  { // Node ID: 342 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id342in_sel = id734out_result;
    const HWOffsetFix<40,-39,UNSIGNED> &id342in_option0 = id338out_result;
    const HWOffsetFix<40,-39,UNSIGNED> &id342in_option1 = id341out_value;

    HWOffsetFix<40,-39,UNSIGNED> id342x_1;

    switch((id342in_sel.getValueAsLong())) {
      case 0l:
        id342x_1 = id342in_option0;
        break;
      case 1l:
        id342x_1 = id342in_option1;
        break;
      default:
        id342x_1 = (c_hw_fix_40_n39_uns_undef);
        break;
    }
    id342out_result = (id342x_1);
  }
  HWOffsetFix<39,-39,UNSIGNED> id343out_o;
  HWOffsetFix<1,0,UNSIGNED> id343out_exception;

  { // Node ID: 343 (NodeCast)
    const HWOffsetFix<40,-39,UNSIGNED> &id343in_i = id342out_result;

    HWOffsetFix<1,0,UNSIGNED> id343x_1;

    id343out_o = (cast_fixed2fixed<39,-39,UNSIGNED,TONEAREVEN>(id343in_i,(&(id343x_1))));
    id343out_exception = (id343x_1);
  }
  { // Node ID: 345 (NodeConstantRawBits)
  }
  HWOffsetFix<39,-39,UNSIGNED> id346out_result;

  { // Node ID: 346 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id346in_sel = id343out_exception;
    const HWOffsetFix<39,-39,UNSIGNED> &id346in_option0 = id343out_o;
    const HWOffsetFix<39,-39,UNSIGNED> &id346in_option1 = id345out_value;

    HWOffsetFix<39,-39,UNSIGNED> id346x_1;

    switch((id346in_sel.getValueAsLong())) {
      case 0l:
        id346x_1 = id346in_option0;
        break;
      case 1l:
        id346x_1 = id346in_option1;
        break;
      default:
        id346x_1 = (c_hw_fix_39_n39_uns_undef);
        break;
    }
    id346out_result = (id346x_1);
  }
  HWRawBits<48> id409out_result;

  { // Node ID: 409 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id409in_in0 = id408out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id409in_in1 = id407out_result;
    const HWOffsetFix<39,-39,UNSIGNED> &id409in_in2 = id346out_result;

    id409out_result = (cat((cat(id409in_in0,id409in_in1)),id409in_in2));
  }
  HWFloat<8,40> id410out_output;

  { // Node ID: 410 (NodeReinterpret)
    const HWRawBits<48> &id410in_input = id409out_result;

    id410out_output = (cast_bits2float<8,40>(id410in_input));
  }
  { // Node ID: 417 (NodeConstantRawBits)
  }
  { // Node ID: 418 (NodeConstantRawBits)
  }
  { // Node ID: 420 (NodeConstantRawBits)
  }
  HWRawBits<48> id759out_result;

  { // Node ID: 759 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id759in_in0 = id417out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id759in_in1 = id418out_value;
    const HWOffsetFix<39,0,UNSIGNED> &id759in_in2 = id420out_value;

    id759out_result = (cat((cat(id759in_in0,id759in_in1)),id759in_in2));
  }
  HWFloat<8,40> id422out_output;

  { // Node ID: 422 (NodeReinterpret)
    const HWRawBits<48> &id422in_input = id759out_result;

    id422out_output = (cast_bits2float<8,40>(id422in_input));
  }
  { // Node ID: 503 (NodeConstantRawBits)
  }
  HWFloat<8,40> id425out_result;

  { // Node ID: 425 (NodeMux)
    const HWRawBits<2> &id425in_sel = id416out_result;
    const HWFloat<8,40> &id425in_option0 = id410out_output;
    const HWFloat<8,40> &id425in_option1 = id422out_output;
    const HWFloat<8,40> &id425in_option2 = id503out_value;
    const HWFloat<8,40> &id425in_option3 = id422out_output;

    HWFloat<8,40> id425x_1;

    switch((id425in_sel.getValueAsLong())) {
      case 0l:
        id425x_1 = id425in_option0;
        break;
      case 1l:
        id425x_1 = id425in_option1;
        break;
      case 2l:
        id425x_1 = id425in_option2;
        break;
      case 3l:
        id425x_1 = id425in_option3;
        break;
      default:
        id425x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id425out_result = (id425x_1);
  }
  { // Node ID: 832 (NodeConstantRawBits)
  }
  HWFloat<8,40> id435out_result;

  { // Node ID: 435 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id435in_sel = id812out_output[getCycle()%9];
    const HWFloat<8,40> &id435in_option0 = id425out_result;
    const HWFloat<8,40> &id435in_option1 = id832out_value;

    HWFloat<8,40> id435x_1;

    switch((id435in_sel.getValueAsLong())) {
      case 0l:
        id435x_1 = id435in_option0;
        break;
      case 1l:
        id435x_1 = id435in_option1;
        break;
      default:
        id435x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id435out_result = (id435x_1);
  }
  { // Node ID: 831 (NodeConstantRawBits)
  }
  HWFloat<8,40> id446out_result;
  HWOffsetFix<4,0,UNSIGNED> id446out_exception;

  { // Node ID: 446 (NodeAdd)
    const HWFloat<8,40> &id446in_a = id435out_result;
    const HWFloat<8,40> &id446in_b = id831out_value;

    HWOffsetFix<4,0,UNSIGNED> id446x_1;

    id446out_result = (add_float(id446in_a,id446in_b,(&(id446x_1))));
    id446out_exception = (id446x_1);
  }
  HWFloat<8,40> id448out_result;
  HWOffsetFix<4,0,UNSIGNED> id448out_exception;

  { // Node ID: 448 (NodeDiv)
    const HWFloat<8,40> &id448in_a = id839out_value;
    const HWFloat<8,40> &id448in_b = id446out_result;

    HWOffsetFix<4,0,UNSIGNED> id448x_1;

    id448out_result = (div_float(id448in_a,id448in_b,(&(id448x_1))));
    id448out_exception = (id448x_1);
  }
  HWFloat<8,40> id450out_result;
  HWOffsetFix<4,0,UNSIGNED> id450out_exception;

  { // Node ID: 450 (NodeSub)
    const HWFloat<8,40> &id450in_a = id840out_value;
    const HWFloat<8,40> &id450in_b = id448out_result;

    HWOffsetFix<4,0,UNSIGNED> id450x_1;

    id450out_result = (sub_float(id450in_a,id450in_b,(&(id450x_1))));
    id450out_exception = (id450x_1);
  }
  HWFloat<8,40> id453out_result;

  { // Node ID: 453 (NodeSub)
    const HWFloat<8,40> &id453in_a = id841out_value;
    const HWFloat<8,40> &id453in_b = id450out_result;

    id453out_result = (sub_float(id453in_a,id453in_b));
  }
  HWFloat<8,40> id454out_result;

  { // Node ID: 454 (NodeMul)
    const HWFloat<8,40> &id454in_a = id453out_result;
    const HWFloat<8,40> &id454in_b = id813out_output[getCycle()%10];

    id454out_result = (mul_float(id454in_a,id454in_b));
  }
  HWFloat<8,40> id455out_result;

  { // Node ID: 455 (NodeMul)
    const HWFloat<8,40> &id455in_a = id454out_result;
    const HWFloat<8,40> &id455in_b = id813out_output[getCycle()%10];

    id455out_result = (mul_float(id455in_a,id455in_b));
  }
  { // Node ID: 766 (NodePO2FPMult)
    const HWFloat<8,40> &id766in_floatIn = id455out_result;

    id766out_floatOut[(getCycle()+1)%2] = (mul_float(id766in_floatIn,(c_hw_flt_8_40_0_5val)));
  }
  HWFloat<8,40> id460out_result;

  { // Node ID: 460 (NodeMul)
    const HWFloat<8,40> &id460in_a = id459out_result;
    const HWFloat<8,40> &id460in_b = id766out_floatOut[getCycle()%2];

    id460out_result = (mul_float(id460in_a,id460in_b));
  }
  HWFloat<8,40> id760out_result;

  { // Node ID: 760 (NodeSub)
    const HWFloat<8,40> &id760in_a = id460out_result;
    const HWFloat<8,40> &id760in_b = id825out_output[getCycle()%2];

    id760out_result = (sub_float(id760in_a,id760in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,40> id467out_result;

  { // Node ID: 467 (NodeMul)
    const HWFloat<8,40> &id467in_a = id760out_result;
    const HWFloat<8,40> &id467in_b = id2out_value;

    id467out_result = (mul_float(id467in_a,id467in_b));
  }
  HWFloat<8,40> id469out_result;

  { // Node ID: 469 (NodeMul)
    const HWFloat<8,40> &id469in_a = id19out_o[getCycle()%4];
    const HWFloat<8,40> &id469in_b = id467out_result;

    id469out_result = (mul_float(id469in_a,id469in_b));
  }
  HWFloat<8,40> id470out_result;

  { // Node ID: 470 (NodeAdd)
    const HWFloat<8,40> &id470in_a = id825out_output[getCycle()%2];
    const HWFloat<8,40> &id470in_b = id469out_result;

    id470out_result = (add_float(id470in_a,id470in_b));
  }
  HWFloat<8,40> id797out_output;

  { // Node ID: 797 (NodeStreamOffset)
    const HWFloat<8,40> &id797in_input = id470out_result;

    id797out_output = id797in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id504out_result[getCycle()%2];
    const HWFloat<8,40> &id23in_option0 = id797out_output;
    const HWFloat<8,40> &id23in_option1 = id22out_value;

    HWFloat<8,40> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_8_40_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<8,40> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<8,40> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<8,40> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 765 (NodePO2FPMult)
    const HWFloat<8,40> &id765in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id765x_1;

    id765out_floatOut[(getCycle()+1)%2] = (mul_float(id765in_floatIn,(c_hw_flt_8_40_2_0val),(&(id765x_1))));
    id765out_exception[(getCycle()+1)%2] = (id765x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<8,40> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<8,40> &id42in_input = id765out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<8,40> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<53,-43,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 770 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id770in_exceptionVector = id43out_exception[getCycle()%7];

    (id770st_reg) = (or_fixed((id770st_reg),id770in_exceptionVector));
    id770out_exceptionOccurred[(getCycle()+1)%2] = (id770st_reg);
  }
  { // Node ID: 817 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id817in_input = id770out_exceptionOccurred[getCycle()%2];

    id817out_output[(getCycle()+2)%3] = id817in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 771 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id771in_exceptionVector = id105out_exception;

    (id771st_reg) = (or_fixed((id771st_reg),id771in_exceptionVector));
    id771out_exceptionOccurred[(getCycle()+1)%2] = (id771st_reg);
  }
  { // Node ID: 818 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id818in_input = id771out_exceptionOccurred[getCycle()%2];

    id818out_output[(getCycle()+2)%3] = id818in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 772 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id772in_exceptionVector = id218out_exception;

    (id772st_reg) = (or_fixed((id772st_reg),id772in_exceptionVector));
    id772out_exceptionOccurred[(getCycle()+1)%2] = (id772st_reg);
  }
  { // Node ID: 819 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id819in_input = id772out_exceptionOccurred[getCycle()%2];

    id819out_output[(getCycle()+2)%3] = id819in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 774 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id774in_exceptionVector = id295out_exception;

    (id774st_reg) = (or_fixed((id774st_reg),id774in_exceptionVector));
    id774out_exceptionOccurred[(getCycle()+1)%2] = (id774st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 775 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id775in_exceptionVector = id301out_exception;

    (id775st_reg) = (or_fixed((id775st_reg),id775in_exceptionVector));
    id775out_exceptionOccurred[(getCycle()+1)%2] = (id775st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 776 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id776in_exceptionVector = id305out_exception;

    (id776st_reg) = (or_fixed((id776st_reg),id776in_exceptionVector));
    id776out_exceptionOccurred[(getCycle()+1)%2] = (id776st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 777 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id777in_exceptionVector = id310out_exception;

    (id777st_reg) = (or_fixed((id777st_reg),id777in_exceptionVector));
    id777out_exceptionOccurred[(getCycle()+1)%2] = (id777st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 778 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id778in_exceptionVector = id314out_exception;

    (id778st_reg) = (or_fixed((id778st_reg),id778in_exceptionVector));
    id778out_exceptionOccurred[(getCycle()+1)%2] = (id778st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 779 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id779in_exceptionVector = id318out_exception;

    (id779st_reg) = (or_fixed((id779st_reg),id779in_exceptionVector));
    id779out_exceptionOccurred[(getCycle()+1)%2] = (id779st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 780 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id780in_exceptionVector = id323out_exception;

    (id780st_reg) = (or_fixed((id780st_reg),id780in_exceptionVector));
    id780out_exceptionOccurred[(getCycle()+1)%2] = (id780st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 781 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id781in_exceptionVector = id327out_exception;

    (id781st_reg) = (or_fixed((id781st_reg),id781in_exceptionVector));
    id781out_exceptionOccurred[(getCycle()+1)%2] = (id781st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 782 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id782in_exceptionVector = id331out_exception;

    (id782st_reg) = (or_fixed((id782st_reg),id782in_exceptionVector));
    id782out_exceptionOccurred[(getCycle()+1)%2] = (id782st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 783 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id783in_exceptionVector = id335out_exception;

    (id783st_reg) = (or_fixed((id783st_reg),id783in_exceptionVector));
    id783out_exceptionOccurred[(getCycle()+1)%2] = (id783st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 786 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id786in_exceptionVector = id343out_exception;

    (id786st_reg) = (or_fixed((id786st_reg),id786in_exceptionVector));
    id786out_exceptionOccurred[(getCycle()+1)%2] = (id786st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 773 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id773in_exceptionVector = id349out_exception;

    (id773st_reg) = (or_fixed((id773st_reg),id773in_exceptionVector));
    id773out_exceptionOccurred[(getCycle()+1)%2] = (id773st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 785 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id785in_exceptionVector = id393out_exception;

    (id785st_reg) = (or_fixed((id785st_reg),id785in_exceptionVector));
    id785out_exceptionOccurred[(getCycle()+1)%2] = (id785st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 787 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id787in_exceptionVector = id446out_exception;

    (id787st_reg) = (or_fixed((id787st_reg),id787in_exceptionVector));
    id787out_exceptionOccurred[(getCycle()+1)%2] = (id787st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 788 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id788in_exceptionVector = id448out_exception;

    (id788st_reg) = (or_fixed((id788st_reg),id788in_exceptionVector));
    id788out_exceptionOccurred[(getCycle()+1)%2] = (id788st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 789 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id789in_exceptionVector = id450out_exception;

    (id789st_reg) = (or_fixed((id789st_reg),id789in_exceptionVector));
    id789out_exceptionOccurred[(getCycle()+1)%2] = (id789st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 784 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id784in_exceptionVector = id764out_exception;

    (id784st_reg) = (or_fixed((id784st_reg),id784in_exceptionVector));
    id784out_exceptionOccurred[(getCycle()+1)%2] = (id784st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 769 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id769in_exceptionVector = id765out_exception[getCycle()%2];

    (id769st_reg) = (or_fixed((id769st_reg),id769in_exceptionVector));
    id769out_exceptionOccurred[(getCycle()+1)%2] = (id769st_reg);
  }
  { // Node ID: 820 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id820in_input = id769out_exceptionOccurred[getCycle()%2];

    id820out_output[(getCycle()+8)%9] = id820in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 790 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id790in_n43 = id817out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n105 = id818out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n218 = id819out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n295 = id774out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n301 = id775out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n305 = id776out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n310 = id777out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n314 = id778out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n318 = id779out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n323 = id780out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n327 = id781out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n331 = id782out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n335 = id783out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n343 = id786out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n349 = id773out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n393 = id785out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id790in_n446 = id787out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id790in_n448 = id788out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id790in_n450 = id789out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id790in_n764 = id784out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id790in_n765 = id820out_output[getCycle()%9];

    id790out_all_exceptions = (cat((cat((cat((cat((cat(id790in_n43,id790in_n105)),id790in_n218)),(cat((cat(id790in_n295,id790in_n301)),id790in_n305)))),(cat((cat((cat(id790in_n310,id790in_n314)),id790in_n318)),(cat(id790in_n323,id790in_n327)))))),(cat((cat((cat((cat(id790in_n331,id790in_n335)),id790in_n343)),(cat(id790in_n349,id790in_n393)))),(cat((cat((cat(id790in_n446,id790in_n448)),id790in_n450)),(cat(id790in_n764,id790in_n765))))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 791 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id791in_load = id792out_value;
    const HWRawBits<36> &id791in_data = id790out_all_exceptions;

    bool id791x_1;

    (id791x_1) = ((id791in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id791x_1)) {
      setMappedRegValue("all_numeric_exceptions", id791in_data);
    }
  }
  { // Node ID: 767 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 768 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id768in_output_control = id767out_value;
    const HWFloat<8,40> &id768in_data = id797out_output;

    bool id768x_1;

    (id768x_1) = ((id768in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id768x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id768in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 821 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id821in_input = id17out_count;

    id821out_output[(getCycle()+1)%2] = id821in_input;
  }
  { // Node ID: 830 (NodeConstantRawBits)
  }
  { // Node ID: 478 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id478in_a = id826out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id478in_b = id830out_value;

    id478out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id478in_a,id478in_b));
  }
  { // Node ID: 761 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id761in_a = id821out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id761in_b = id478out_result[getCycle()%2];

    id761out_result[(getCycle()+1)%2] = (eq_fixed(id761in_a,id761in_b));
  }
  { // Node ID: 480 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id481out_result;

  { // Node ID: 481 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id481in_a = id480out_io_u_out_force_disabled;

    id481out_result = (not_fixed(id481in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id482out_result;

  { // Node ID: 482 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id482in_a = id761out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id482in_b = id481out_result;

    HWOffsetFix<1,0,UNSIGNED> id482x_1;

    (id482x_1) = (and_fixed(id482in_a,id482in_b));
    id482out_result = (id482x_1);
  }
  { // Node ID: 822 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id822in_input = id482out_result;

    id822out_output[(getCycle()+13)%14] = id822in_input;
  }
  { // Node ID: 475 (NodeCast)
    const HWFloat<8,40> &id475in_i = id470out_result;

    id475out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id475in_i));
  }
  if ( (getFillLevel() >= (18l)) && (getFlushLevel() < (18l)|| !isFlushingActive() ))
  { // Node ID: 483 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id483in_output_control = id822out_output[getCycle()%14];
    const HWFloat<8,24> &id483in_data = id475out_o[getCycle()%4];

    bool id483x_1;

    (id483x_1) = ((id483in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(18l))&(isFlushingActive()))));
    if((id483x_1)) {
      writeOutput(m_u_out, id483in_data);
    }
  }
  { // Node ID: 829 (NodeConstantRawBits)
  }
  { // Node ID: 485 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id485in_a = id826out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id485in_b = id829out_value;

    id485out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id485in_a,id485in_b));
  }
  { // Node ID: 762 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id762in_a = id821out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id762in_b = id485out_result[getCycle()%2];

    id762out_result[(getCycle()+1)%2] = (eq_fixed(id762in_a,id762in_b));
  }
  { // Node ID: 487 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id488out_result;

  { // Node ID: 488 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id488in_a = id487out_io_v_out_force_disabled;

    id488out_result = (not_fixed(id488in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id489out_result;

  { // Node ID: 489 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id489in_a = id762out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id489in_b = id488out_result;

    HWOffsetFix<1,0,UNSIGNED> id489x_1;

    (id489x_1) = (and_fixed(id489in_a,id489in_b));
    id489out_result = (id489x_1);
  }
  { // Node ID: 824 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id824in_input = id489out_result;

    id824out_output[(getCycle()+13)%14] = id824in_input;
  }
  { // Node ID: 476 (NodeCast)
    const HWFloat<8,40> &id476in_i = id472out_result;

    id476out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id476in_i));
  }
  if ( (getFillLevel() >= (18l)) && (getFlushLevel() < (18l)|| !isFlushingActive() ))
  { // Node ID: 490 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id490in_output_control = id824out_output[getCycle()%14];
    const HWFloat<8,24> &id490in_data = id476out_o[getCycle()%4];

    bool id490x_1;

    (id490x_1) = ((id490in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(18l))&(isFlushingActive()))));
    if((id490x_1)) {
      writeOutput(m_v_out, id490in_data);
    }
  }
  { // Node ID: 495 (NodeConstantRawBits)
  }
  { // Node ID: 828 (NodeConstantRawBits)
  }
  { // Node ID: 492 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 493 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id493in_enable = id828out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id493in_max = id492out_value;

    HWOffsetFix<49,0,UNSIGNED> id493x_1;
    HWOffsetFix<1,0,UNSIGNED> id493x_2;
    HWOffsetFix<1,0,UNSIGNED> id493x_3;
    HWOffsetFix<49,0,UNSIGNED> id493x_4t_1e_1;

    id493out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id493st_count)));
    (id493x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id493st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id493x_2) = (gte_fixed((id493x_1),id493in_max));
    (id493x_3) = (and_fixed((id493x_2),id493in_enable));
    id493out_wrap = (id493x_3);
    if((id493in_enable.getValueAsBool())) {
      if(((id493x_3).getValueAsBool())) {
        (id493st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id493x_4t_1e_1) = (id493x_1);
        (id493st_count) = (id493x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id494out_output;

  { // Node ID: 494 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id494in_input = id493out_count;

    id494out_output = id494in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 496 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id496in_load = id495out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id496in_data = id494out_output;

    bool id496x_1;

    (id496x_1) = ((id496in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id496x_1)) {
      setMappedRegValue("current_run_cycle_count", id496in_data);
    }
  }
  { // Node ID: 827 (NodeConstantRawBits)
  }
  { // Node ID: 498 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 499 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id499in_enable = id827out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id499in_max = id498out_value;

    HWOffsetFix<49,0,UNSIGNED> id499x_1;
    HWOffsetFix<1,0,UNSIGNED> id499x_2;
    HWOffsetFix<1,0,UNSIGNED> id499x_3;
    HWOffsetFix<49,0,UNSIGNED> id499x_4t_1e_1;

    id499out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id499st_count)));
    (id499x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id499st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id499x_2) = (gte_fixed((id499x_1),id499in_max));
    (id499x_3) = (and_fixed((id499x_2),id499in_enable));
    id499out_wrap = (id499x_3);
    if((id499in_enable.getValueAsBool())) {
      if(((id499x_3).getValueAsBool())) {
        (id499st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id499x_4t_1e_1) = (id499x_1);
        (id499st_count) = (id499x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 501 (NodeInputMappedReg)
  }
  { // Node ID: 763 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id763in_a = id499out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id763in_b = id501out_run_cycle_count;

    id763out_result[(getCycle()+1)%2] = (eq_fixed(id763in_a,id763in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 500 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id500in_start = id763out_result[getCycle()%2];

    if((id500in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
