#include "stdsimheader.h"
#include "KarmaDFEKernel.h"

namespace maxcompilersim {

KarmaDFEKernel::KarmaDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 18, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x00bl))))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_40_undef((HWFloat<8,40>()))
, c_hw_flt_8_40_bits((HWFloat<8,40>(varint_u<48>(0x3f4ccccccccdl))))
, c_hw_flt_8_40_bits_1((HWFloat<8,40>(varint_u<48>(0x3fb6dae20a07l))))
, c_hw_flt_8_40_bits_2((HWFloat<8,40>(varint_u<48>(0x3f8000000000l))))
, c_hw_flt_8_40_bits_3((HWFloat<8,40>(varint_u<48>(0x3b83126e978dl))))
, c_hw_flt_8_40_bits_4((HWFloat<8,40>(varint_u<48>(0x000000000000l))))
, c_hw_flt_8_40_bits_5((HWFloat<8,40>(varint_u<48>(0x400000000000l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0xffl))))
, c_hw_bit_39_bits((HWRawBits<39>(varint_u<39>(0x0000000000l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_53_n43_sgn_bits((HWOffsetFix<53,-43,TWOSCOMPLEMENT>(varint_u<53>(0x0fffffffffffffl))))
, c_hw_fix_53_n43_sgn_undef((HWOffsetFix<53,-43,TWOSCOMPLEMENT>()))
, c_hw_fix_52_n51_uns_bits((HWOffsetFix<52,-51,UNSIGNED>(varint_u<52>(0xb8aa3b295c17fl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_105_n94_sgn_bits((HWOffsetFix<105,-94,TWOSCOMPLEMENT>(varint_u<105>::init(2, 0xffffffffffffffffl, 0x0ffffffffffl))))
, c_hw_fix_105_n94_sgn_undef((HWOffsetFix<105,-94,TWOSCOMPLEMENT>()))
, c_hw_fix_10_0_sgn_undef((HWOffsetFix<10,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x07fl))))
, c_hw_fix_11_0_sgn_bits_1((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x3ffl))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_2((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x001l))))
, c_hw_fix_29_n40_uns_undef((HWOffsetFix<29,-40,UNSIGNED>()))
, c_hw_fix_39_n40_uns_undef((HWOffsetFix<39,-40,UNSIGNED>()))
, c_hw_fix_40_n40_uns_undef((HWOffsetFix<40,-40,UNSIGNED>()))
, c_hw_fix_21_n21_uns_bits((HWOffsetFix<21,-21,UNSIGNED>(varint_u<21>(0x162e43l))))
, c_hw_fix_22_n43_uns_undef((HWOffsetFix<22,-43,UNSIGNED>()))
, c_hw_fix_22_n43_uns_bits((HWOffsetFix<22,-43,UNSIGNED>(varint_u<22>(0x3fffffl))))
, c_hw_fix_64_n63_uns_bits((HWOffsetFix<64,-63,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n63_uns_undef((HWOffsetFix<64,-63,UNSIGNED>()))
, c_hw_fix_44_n43_uns_bits((HWOffsetFix<44,-43,UNSIGNED>(varint_u<44>(0xfffffffffffl))))
, c_hw_fix_44_n43_uns_undef((HWOffsetFix<44,-43,UNSIGNED>()))
, c_hw_fix_64_n64_uns_bits((HWOffsetFix<64,-64,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n64_uns_undef((HWOffsetFix<64,-64,UNSIGNED>()))
, c_hw_fix_64_n62_uns_bits((HWOffsetFix<64,-62,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n62_uns_undef((HWOffsetFix<64,-62,UNSIGNED>()))
, c_hw_fix_45_n43_uns_bits((HWOffsetFix<45,-43,UNSIGNED>(varint_u<45>(0x1fffffffffffl))))
, c_hw_fix_45_n43_uns_undef((HWOffsetFix<45,-43,UNSIGNED>()))
, c_hw_fix_64_n73_uns_bits((HWOffsetFix<64,-73,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n73_uns_undef((HWOffsetFix<64,-73,UNSIGNED>()))
, c_hw_fix_64_n61_uns_bits((HWOffsetFix<64,-61,UNSIGNED>(varint_u<64>(0xffffffffffffffffl))))
, c_hw_fix_64_n61_uns_undef((HWOffsetFix<64,-61,UNSIGNED>()))
, c_hw_fix_46_n43_uns_bits((HWOffsetFix<46,-43,UNSIGNED>(varint_u<46>(0x3fffffffffffl))))
, c_hw_fix_46_n43_uns_undef((HWOffsetFix<46,-43,UNSIGNED>()))
, c_hw_fix_40_n39_uns_bits((HWOffsetFix<40,-39,UNSIGNED>(varint_u<40>(0xffffffffffl))))
, c_hw_fix_40_n39_uns_undef((HWOffsetFix<40,-39,UNSIGNED>()))
, c_hw_fix_40_n39_uns_bits_1((HWOffsetFix<40,-39,UNSIGNED>(varint_u<40>(0x8000000000l))))
, c_hw_fix_12_0_sgn_bits((HWOffsetFix<12,0,TWOSCOMPLEMENT>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_sgn_undef((HWOffsetFix<12,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_3((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x0ffl))))
, c_hw_fix_8_0_sgn_bits((HWOffsetFix<8,0,TWOSCOMPLEMENT>(varint_u<8>(0x7fl))))
, c_hw_fix_8_0_sgn_undef((HWOffsetFix<8,0,TWOSCOMPLEMENT>()))
, c_hw_fix_40_n39_uns_bits_2((HWOffsetFix<40,-39,UNSIGNED>(varint_u<40>(0x0000000000l))))
, c_hw_fix_39_n39_uns_bits((HWOffsetFix<39,-39,UNSIGNED>(varint_u<39>(0x7fffffffffl))))
, c_hw_fix_39_n39_uns_undef((HWOffsetFix<39,-39,UNSIGNED>()))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_39_0_uns_bits((HWOffsetFix<39,0,UNSIGNED>(varint_u<39>(0x0000000000l))))
, c_hw_flt_8_40_bits_6((HWFloat<8,40>(varint_u<48>(0x7fc000000000l))))
, c_hw_flt_8_40_0_5val((HWFloat<8,40>(varint_u<48>(0x3f0000000000l))))
, c_hw_flt_8_40_bits_7((HWFloat<8,40>(varint_u<48>(0x3ecccccccccdl))))
, c_hw_flt_8_40_bits_8((HWFloat<8,40>(varint_u<48>(0x3fc000000000l))))
, c_hw_flt_8_40_bits_9((HWFloat<8,40>(varint_u<48>(0x404000000000l))))
, c_hw_flt_8_40_2_0val((HWFloat<8,40>(varint_u<48>(0x400000000000l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_4_0_uns_undef((HWOffsetFix<4,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 792 (NodeConstantRawBits)
    id792out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 14 (NodeConstantRawBits)
    id14out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 826 (NodeConstantRawBits)
    id826out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 844 (NodeConstantRawBits)
    id844out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 18 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 6 (NodeConstantRawBits)
    id6out_value = (c_hw_flt_8_40_bits);
  }
  { // Node ID: 843 (NodeConstantRawBits)
    id843out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 842 (NodeConstantRawBits)
    id842out_value = (c_hw_flt_8_40_bits_1);
  }
  { // Node ID: 5 (NodeConstantRawBits)
    id5out_value = (c_hw_flt_8_40_bits_2);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_8_40_bits_3);
  }
  { // Node ID: 26 (NodeConstantRawBits)
    id26out_value = (c_hw_flt_8_40_bits_4);
  }
  { // Node ID: 841 (NodeConstantRawBits)
    id841out_value = (c_hw_flt_8_40_bits_2);
  }
  { // Node ID: 840 (NodeConstantRawBits)
    id840out_value = (c_hw_flt_8_40_bits_2);
  }
  { // Node ID: 839 (NodeConstantRawBits)
    id839out_value = (c_hw_flt_8_40_bits_5);
  }
  { // Node ID: 429 (NodeConstantRawBits)
    id429out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 838 (NodeConstantRawBits)
    id838out_value = (c_hw_bit_39_bits);
  }
  { // Node ID: 46 (NodeConstantRawBits)
    id46out_value = (c_hw_fix_53_n43_sgn_bits);
  }
  { // Node ID: 106 (NodeConstantRawBits)
    id106out_value = (c_hw_fix_52_n51_uns_bits);
  }
  { // Node ID: 108 (NodeConstantRawBits)
    id108out_value = (c_hw_fix_105_n94_sgn_bits);
  }
  { // Node ID: 220 (NodeConstantRawBits)
    id220out_value = (c_hw_fix_53_n43_sgn_bits);
  }
  { // Node ID: 837 (NodeConstantRawBits)
    id837out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 351 (NodeConstantRawBits)
    id351out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 367 (NodeConstantRawBits)
    id367out_value = (c_hw_fix_11_0_sgn_bits_2);
  }
  { // Node ID: 444 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x58b91,
      0xb1722,
      0x10a2b3,
      0x162e44,
      0x1bb9d5,
      0x214567,
      0x26d0f8,
      0x2c5c8a,
      0x31e81c,
      0x3773ad,
      0x3cff40,
      0x428ad2,
      0x481664,
      0x4da1f6,
      0x532d89,
      0x58b91b,
      0x5e44ae,
      0x63d041,
      0x695bd4,
      0x6ee767,
      0x7472fa,
      0x79fe8e,
      0x7f8a21,
      0x8515b5,
      0x8aa148,
      0x902cdc,
      0x95b870,
      0x9b4404,
      0xa0cf98,
      0xa65b2d,
      0xabe6c1,
      0xb17255,
      0xb6fdea,
      0xbc897f,
      0xc21514,
      0xc7a0a9,
      0xcd2c3e,
      0xd2b7d3,
      0xd84369,
      0xddcefe,
      0xe35a94,
      0xe8e629,
      0xee71bf,
      0xf3fd55,
      0xf988eb,
      0xff1482,
      0x104a018,
      0x10a2bae,
      0x10fb745,
      0x11542dc,
      0x11ace72,
      0x1205a09,
      0x125e5a0,
      0x12b7138,
      0x130fccf,
      0x1368866,
      0x13c13fe,
      0x1419f95,
      0x1472b2d,
      0x14cb6c5,
      0x152425d,
      0x157cdf5,
      0x15d598e,
      0x162e526,
      0x16870be,
      0x16dfc57,
      0x17387f0,
      0x1791389,
      0x17e9f22,
      0x1842abb,
      0x189b654,
      0x18f41ed,
      0x194cd87,
      0x19a5920,
      0x19fe4ba,
      0x1a57054,
      0x1aafbee,
      0x1b08788,
      0x1b61322,
      0x1bb9ebc,
      0x1c12a57,
      0x1c6b5f1,
      0x1cc418c,
      0x1d1cd27,
      0x1d758c2,
      0x1dce45d,
      0x1e26ff8,
      0x1e7fb93,
      0x1ed872e,
      0x1f312ca,
      0x1f89e65,
      0x1fe2a01,
      0x203b59d,
      0x2094139,
      0x20eccd5,
      0x2145871,
      0x219e40e,
      0x21f6faa,
      0x224fb47,
      0x22a86e3,
      0x2301280,
      0x2359e1d,
      0x23b29ba,
      0x240b557,
      0x24640f5,
      0x24bcc92,
      0x2515830,
      0x256e3cd,
      0x25c6f6b,
      0x261fb09,
      0x26786a7,
      0x26d1245,
      0x2729de4,
      0x2782982,
      0x27db520,
      0x28340bf,
      0x288cc5e,
      0x28e57fd,
      0x293e39c,
      0x2996f3b,
      0x29efada,
      0x2a48679,
      0x2aa1219,
      0x2af9db8,
      0x2b52958,
      0x2bab4f8,
      0x2c04098,
      0x2c5cc38,
      0x2cb57d8,
      0x2d0e378,
      0x2d66f19,
      0x2dbfab9,
      0x2e1865a,
      0x2e711fb,
      0x2ec9d9c,
      0x2f2293d,
      0x2f7b4de,
      0x2fd407f,
      0x302cc20,
      0x30857c2,
      0x30de364,
      0x3136f05,
      0x318faa7,
      0x31e8649,
      0x32411eb,
      0x3299d8e,
      0x32f2930,
      0x334b4d2,
      0x33a4075,
      0x33fcc18,
      0x34557ba,
      0x34ae35d,
      0x3506f00,
      0x355faa4,
      0x35b8647,
      0x36111ea,
      0x3669d8e,
      0x36c2932,
      0x371b4d5,
      0x3774079,
      0x37ccc1d,
      0x38257c1,
      0x387e366,
      0x38d6f0a,
      0x392faaf,
      0x3988653,
      0x39e11f8,
      0x3a39d9d,
      0x3a92942,
      0x3aeb4e7,
      0x3b4408c,
      0x3b9cc32,
      0x3bf57d7,
      0x3c4e37d,
      0x3ca6f22,
      0x3cffac8,
      0x3d5866e,
      0x3db1214,
      0x3e09dba,
      0x3e62961,
      0x3ebb507,
      0x3f140ae,
      0x3f6cc54,
      0x3fc57fb,
      0x401e3a2,
      0x4076f49,
      0x40cfaf0,
      0x4128697,
      0x418123f,
      0x41d9de6,
      0x423298e,
      0x428b536,
      0x42e40de,
      0x433cc86,
      0x439582e,
      0x43ee3d6,
      0x4446f7e,
      0x449fb27,
      0x44f86cf,
      0x4551278,
      0x45a9e21,
      0x46029ca,
      0x465b573,
      0x46b411c,
      0x470ccc5,
      0x476586f,
      0x47be418,
      0x4816fc2,
      0x486fb6c,
      0x48c8716,
      0x49212c0,
      0x4979e6a,
      0x49d2a14,
      0x4a2b5bf,
      0x4a84169,
      0x4adcd14,
      0x4b358bf,
      0x4b8e469,
      0x4be7014,
      0x4c3fbc0,
      0x4c9876b,
      0x4cf1316,
      0x4d49ec2,
      0x4da2a6d,
      0x4dfb619,
      0x4e541c5,
      0x4eacd71,
      0x4f0591d,
      0x4f5e4c9,
      0x4fb7075,
      0x500fc22,
      0x50687ce,
      0x50c137b,
      0x5119f28,
      0x5172ad5,
      0x51cb682,
      0x522422f,
      0x527cddc,
      0x52d598a,
      0x532e537,
      0x53870e5,
      0x53dfc93,
      0x5438840,
      0x54913ee,
      0x54e9f9d,
      0x5542b4b,
      0x559b6f9,
      0x55f42a8,
      0x564ce56,
      0x56a5a05,
      0x56fe5b4,
      0x5757163,
      0x57afd12,
      0x58088c1,
      0x5861470,
      0x58ba020,
      0x5912bcf,
      0x596b77f,
      0x59c432f,
      0x5a1cedf,
      0x5a75a8f,
      0x5ace63f,
      0x5b271ef,
      0x5b7fda0,
      0x5bd8950,
      0x5c31501,
      0x5c8a0b1,
      0x5ce2c62,
      0x5d3b813,
      0x5d943c4,
      0x5decf76,
      0x5e45b27,
      0x5e9e6d9,
      0x5ef728a,
      0x5f4fe3c,
      0x5fa89ee,
      0x60015a0,
      0x605a152,
      0x60b2d04,
      0x610b8b6,
      0x6164469,
      0x61bd01b,
      0x6215bce,
      0x626e781,
      0x62c7334,
      0x631fee7,
      0x6378a9a,
      0x63d164d,
      0x642a201,
      0x6482db4,
      0x64db968,
      0x653451c,
      0x658d0cf,
      0x65e5c83,
      0x663e838,
      0x66973ec,
      0x66effa0,
      0x6748b55,
      0x67a1709,
      0x67fa2be,
      0x6852e73,
      0x68aba28,
      0x69045dd,
      0x695d192,
      0x69b5d47,
      0x6a0e8fd,
      0x6a674b2,
      0x6ac0068,
      0x6b18c1e,
      0x6b717d4,
      0x6bca38a,
      0x6c22f40,
      0x6c7baf6,
      0x6cd46ad,
      0x6d2d263,
      0x6d85e1a,
      0x6dde9d1,
      0x6e37588,
      0x6e9013f,
      0x6ee8cf6,
      0x6f418ad,
      0x6f9a464,
      0x6ff301c,
      0x704bbd3,
      0x70a478b,
      0x70fd343,
      0x7155efb,
      0x71aeab3,
      0x720766b,
      0x7260224,
      0x72b8ddc,
      0x7311995,
      0x736a54d,
      0x73c3106,
      0x741bcbf,
      0x7474878,
      0x74cd431,
      0x7525feb,
      0x757eba4,
      0x75d775d,
      0x7630317,
      0x7688ed1,
      0x76e1a8b,
      0x773a645,
      0x77931ff,
      0x77ebdb9,
      0x7844974,
      0x789d52e,
      0x78f60e9,
      0x794eca3,
      0x79a785e,
      0x7a00419,
      0x7a58fd4,
      0x7ab1b8f,
      0x7b0a74b,
      0x7b63306,
      0x7bbbec2,
      0x7c14a7e,
      0x7c6d639,
      0x7cc61f5,
      0x7d1edb1,
      0x7d7796e,
      0x7dd052a,
      0x7e290e6,
      0x7e81ca3,
      0x7eda85f,
      0x7f3341c,
      0x7f8bfd9,
      0x7fe4b96,
      0x803d753,
      0x8096310,
      0x80eeece,
      0x8147a8b,
      0x81a0649,
      0x81f9207,
      0x8251dc5,
      0x82aa983,
      0x8303541,
      0x835c0ff,
      0x83b4cbd,
      0x840d87c,
      0x846643a,
      0x84beff9,
      0x8517bb8,
      0x8570777,
      0x85c9336,
      0x8621ef5,
      0x867aab4,
      0x86d3674,
      0x872c233,
      0x8784df3,
      0x87dd9b3,
      0x8836572,
      0x888f132,
      0x88e7cf3,
      0x89408b3,
      0x8999473,
      0x89f2034,
      0x8a4abf4,
      0x8aa37b5,
      0x8afc376,
      0x8b54f37,
      0x8badaf8,
      0x8c066b9,
      0x8c5f27b,
      0x8cb7e3c,
      0x8d109fe,
      0x8d695bf,
      0x8dc2181,
      0x8e1ad43,
      0x8e73905,
      0x8ecc4c7,
      0x8f2508a,
      0x8f7dc4c,
      0x8fd680f,
      0x902f3d1,
      0x9087f94,
      0x90e0b57,
      0x913971a,
      0x91922dd,
      0x91eaea0,
      0x9243a64,
      0x929c627,
      0x92f51eb,
      0x934ddaf,
      0x93a6972,
      0x93ff536,
      0x94580fb,
      0x94b0cbf,
      0x9509883,
      0x9562448,
      0x95bb00c,
      0x9613bd1,
      0x966c796,
      0x96c535b,
      0x971df20,
      0x9776ae5,
      0x97cf6aa,
      0x9828270,
      0x9880e35,
      0x98d99fb,
      0x99325c1,
      0x998b186,
      0x99e3d4c,
      0x9a3c913,
      0x9a954d9,
      0x9aee09f,
      0x9b46c66,
      0x9b9f82c,
      0x9bf83f3,
      0x9c50fba,
      0x9ca9b81,
      0x9d02748,
      0x9d5b30f,
      0x9db3ed7,
      0x9e0ca9e,
      0x9e65666,
      0x9ebe22d,
      0x9f16df5,
      0x9f6f9bd,
      0x9fc8585,
      0xa02114d,
      0xa079d16,
      0xa0d28de,
      0xa12b4a7,
      0xa18406f,
      0xa1dcc38,
      0xa235801,
      0xa28e3ca,
      0xa2e6f93,
      0xa33fb5c,
      0xa398726,
      0xa3f12ef,
      0xa449eb9,
      0xa4a2a83,
      0xa4fb64d,
      0xa554217,
      0xa5acde1,
      0xa6059ab,
      0xa65e575,
      0xa6b7140,
      0xa70fd0a,
      0xa7688d5,
      0xa7c14a0,
      0xa81a06b,
      0xa872c36,
      0xa8cb801,
      0xa9243cc,
      0xa97cf98,
      0xa9d5b63,
      0xaa2e72f,
      0xaa872fb,
      0xaadfec7,
      0xab38a93,
      0xab9165f,
      0xabea22b,
      0xac42df8,
      0xac9b9c4,
      0xacf4591,
      0xad4d15d,
      0xada5d2a,
      0xadfe8f7,
      0xae574c4,
      0xaeb0092,
      0xaf08c5f,
      0xaf6182d,
      0xafba3fa,
      0xb012fc8,
      0xb06bb96,
      0xb0c4764,
      0xb11d332,
      0xb175f00,
      0xb1ceace,
      0xb22769d,
      0xb28026b,
      0xb2d8e3a,
      0xb331a09,
      0xb38a5d8,
      0xb3e31a7,
      0xb43bd76,
      0xb494945,
      0xb4ed514,
      0xb5460e4,
      0xb59ecb4,
      0xb5f7883,
      0xb650453,
      0xb6a9023,
      0xb701bf3,
      0xb75a7c4,
      0xb7b3394,
      0xb80bf64,
      0xb864b35,
      0xb8bd706,
      0xb9162d6,
      0xb96eea7,
      0xb9c7a79,
      0xba2064a,
      0xba7921b,
      0xbad1dec,
      0xbb2a9be,
      0xbb83590,
      0xbbdc161,
      0xbc34d33,
      0xbc8d905,
      0xbce64d8,
      0xbd3f0aa,
      0xbd97c7c,
      0xbdf084f,
      0xbe49421,
      0xbea1ff4,
      0xbefabc7,
      0xbf5379a,
      0xbfac36d,
      0xc004f40,
      0xc05db14,
      0xc0b66e7,
      0xc10f2bb,
      0xc167e8f,
      0xc1c0a62,
      0xc219636,
      0xc27220a,
      0xc2caddf,
      0xc3239b3,
      0xc37c587,
      0xc3d515c,
      0xc42dd31,
      0xc486905,
      0xc4df4da,
      0xc5380af,
      0xc590c85,
      0xc5e985a,
      0xc64242f,
      0xc69b005,
      0xc6f3bda,
      0xc74c7b0,
      0xc7a5386,
      0xc7fdf5c,
      0xc856b32,
      0xc8af708,
      0xc9082df,
      0xc960eb5,
      0xc9b9a8c,
      0xca12663,
      0xca6b239,
      0xcac3e10,
      0xcb1c9e7,
      0xcb755bf,
      0xcbce196,
      0xcc26d6d,
      0xcc7f945,
      0xccd851d,
      0xcd310f4,
      0xcd89ccc,
      0xcde28a4,
      0xce3b47d,
      0xce94055,
      0xceecc2d,
      0xcf45806,
      0xcf9e3de,
      0xcff6fb7,
      0xd04fb90,
      0xd0a8769,
      0xd101342,
      0xd159f1b,
      0xd1b2af5,
      0xd20b6ce,
      0xd2642a8,
      0xd2bce82,
      0xd315a5b,
      0xd36e635,
      0xd3c7210,
      0xd41fdea,
      0xd4789c4,
      0xd4d159f,
      0xd52a179,
      0xd582d54,
      0xd5db92f,
      0xd63450a,
      0xd68d0e5,
      0xd6e5cc0,
      0xd73e89b,
      0xd797476,
      0xd7f0052,
      0xd848c2e,
      0xd8a1809,
      0xd8fa3e5,
      0xd952fc1,
      0xd9abb9d,
      0xda0477a,
      0xda5d356,
      0xdab5f32,
      0xdb0eb0f,
      0xdb676ec,
      0xdbc02c9,
      0xdc18ea6,
      0xdc71a83,
      0xdcca660,
      0xdd2323d,
      0xdd7be1b,
      0xddd49f8,
      0xde2d5d6,
      0xde861b4,
      0xdeded92,
      0xdf37970,
      0xdf9054e,
      0xdfe912c,
      0xe041d0b,
      0xe09a8e9,
      0xe0f34c8,
      0xe14c0a7,
      0xe1a4c85,
      0xe1fd864,
      0xe256444,
      0xe2af023,
      0xe307c02,
      0xe3607e2,
      0xe3b93c1,
      0xe411fa1,
      0xe46ab81,
      0xe4c3761,
      0xe51c341,
      0xe574f21,
      0xe5cdb02,
      0xe6266e2,
      0xe67f2c3,
      0xe6d7ea3,
      0xe730a84,
      0xe789665,
      0xe7e2246,
      0xe83ae27,
      0xe893a09,
      0xe8ec5ea,
      0xe9451cb,
      0xe99ddad,
      0xe9f698f,
      0xea4f571,
      0xeaa8153,
      0xeb00d35,
      0xeb59917,
      0xebb24fa,
      0xec0b0dc,
      0xec63cbf,
      0xecbc8a1,
      0xed15484,
      0xed6e067,
      0xedc6c4a,
      0xee1f82e,
      0xee78411,
      0xeed0ff4,
      0xef29bd8,
      0xef827bc,
      0xefdb39f,
      0xf033f83,
      0xf08cb67,
      0xf0e574c,
      0xf13e330,
      0xf196f14,
      0xf1efaf9,
      0xf2486de,
      0xf2a12c2,
      0xf2f9ea7,
      0xf352a8c,
      0xf3ab671,
      0xf404257,
      0xf45ce3c,
      0xf4b5a22,
      0xf50e607,
      0xf5671ed,
      0xf5bfdd3,
      0xf6189b9,
      0xf67159f,
      0xf6ca185,
      0xf722d6b,
      0xf77b952,
      0xf7d4538,
      0xf82d11f,
      0xf885d06,
      0xf8de8ed,
      0xf9374d4,
      0xf9900bb,
      0xf9e8ca2,
      0xfa4188a,
      0xfa9a471,
      0xfaf3059,
      0xfb4bc41,
      0xfba4829,
      0xfbfd411,
      0xfc55ff9,
      0xfcaebe1,
      0xfd077ca,
      0xfd603b2,
      0xfdb8f9b,
      0xfe11b83,
      0xfe6a76c,
      0xfec3355,
      0xff1bf3e,
      0xff74b28,
      0xffcd711,
      0x100262fa,
      0x1007eee4,
      0x100d7ace,
      0x101306b7,
      0x101892a1,
      0x101e1e8b,
      0x1023aa76,
      0x10293660,
      0x102ec24a,
      0x10344e35,
      0x1039da20,
      0x103f660a,
      0x1044f1f5,
      0x104a7de0,
      0x105009cb,
      0x105595b7,
      0x105b21a2,
      0x1060ad8e,
      0x10663979,
      0x106bc565,
      0x10715151,
      0x1076dd3d,
      0x107c6929,
      0x1081f515,
      0x10878102,
      0x108d0cee,
      0x109298db,
      0x109824c7,
      0x109db0b4,
      0x10a33ca1,
      0x10a8c88e,
      0x10ae547b,
      0x10b3e069,
      0x10b96c56,
      0x10bef844,
      0x10c48431,
      0x10ca101f,
      0x10cf9c0d,
      0x10d527fb,
      0x10dab3e9,
      0x10e03fd7,
      0x10e5cbc6,
      0x10eb57b4,
      0x10f0e3a3,
      0x10f66f92,
      0x10fbfb81,
      0x11018770,
      0x1107135f,
      0x110c9f4e,
      0x11122b3d,
      0x1117b72d,
      0x111d431c,
      0x1122cf0c,
      0x11285afc,
      0x112de6ec,
      0x113372dc,
      0x1138fecc,
      0x113e8abc,
      0x114416ad,
      0x1149a29d,
      0x114f2e8e,
      0x1154ba7f,
      0x115a4670,
      0x115fd261,
      0x11655e52,
      0x116aea43,
      0x11707635,
      0x11760226,
      0x117b8e18,
      0x11811a09,
      0x1186a5fb,
      0x118c31ed,
      0x1191bddf,
      0x119749d2,
      0x119cd5c4,
      0x11a261b7,
      0x11a7eda9,
      0x11ad799c,
      0x11b3058f,
      0x11b89182,
      0x11be1d75,
      0x11c3a968,
      0x11c9355b,
      0x11cec14f,
      0x11d44d42,
      0x11d9d936,
      0x11df652a,
      0x11e4f11e,
      0x11ea7d12,
      0x11f00906,
      0x11f594fa,
      0x11fb20ee,
      0x1200ace3,
      0x120638d8,
      0x120bc4cc,
      0x121150c1,
      0x1216dcb6,
      0x121c68ab,
      0x1221f4a1,
      0x12278096,
      0x122d0c8b,
      0x12329881,
      0x12382477,
      0x123db06d,
      0x12433c63,
      0x1248c859,
      0x124e544f,
      0x1253e045,
      0x12596c3c,
      0x125ef832,
      0x12648429,
      0x126a1020,
      0x126f9c17,
      0x1275280e,
      0x127ab405,
      0x12803ffc,
      0x1285cbf3,
      0x128b57eb,
      0x1290e3e3,
      0x12966fda,
      0x129bfbd2,
      0x12a187ca,
      0x12a713c2,
      0x12ac9fbb,
      0x12b22bb3,
      0x12b7b7ab,
      0x12bd43a4,
      0x12c2cf9d,
      0x12c85b96,
      0x12cde78f,
      0x12d37388,
      0x12d8ff81,
      0x12de8b7a,
      0x12e41774,
      0x12e9a36d,
      0x12ef2f67,
      0x12f4bb61,
      0x12fa475b,
      0x12ffd355,
      0x13055f4f,
      0x130aeb49,
      0x13107743,
      0x1316033e,
      0x131b8f39,
      0x13211b33,
      0x1326a72e,
      0x132c3329,
      0x1331bf24,
      0x13374b20,
      0x133cd71b,
      0x13426317,
      0x1347ef12,
      0x134d7b0e,
      0x1353070a,
      0x13589306,
      0x135e1f02,
      0x1363aafe,
      0x136936fa,
      0x136ec2f7,
      0x13744ef3,
      0x1379daf0,
      0x137f66ed,
      0x1384f2ea,
      0x138a7ee7,
      0x13900ae4,
      0x139596e1,
      0x139b22df,
      0x13a0aedc,
      0x13a63ada,
      0x13abc6d8,
      0x13b152d5,
      0x13b6ded3,
      0x13bc6ad2,
      0x13c1f6d0,
      0x13c782ce,
      0x13cd0ecd,
      0x13d29acb,
      0x13d826ca,
      0x13ddb2c9,
      0x13e33ec8,
      0x13e8cac7,
      0x13ee56c6,
      0x13f3e2c5,
      0x13f96ec5,
      0x13fefac5,
      0x140486c4,
      0x140a12c4,
      0x140f9ec4,
      0x14152ac4,
      0x141ab6c4,
      0x142042c4,
      0x1425cec5,
      0x142b5ac5,
      0x1430e6c6,
      0x143672c7,
      0x143bfec8,
      0x14418ac9,
      0x144716ca,
      0x144ca2cb,
      0x14522ecc,
      0x1457bace,
      0x145d46d0,
      0x1462d2d1,
      0x14685ed3,
      0x146dead5,
      0x147376d7,
      0x147902d9,
      0x147e8edc,
      0x14841ade,
      0x1489a6e1,
      0x148f32e4,
      0x1494bee6,
      0x149a4ae9,
      0x149fd6ec,
      0x14a562ef,
      0x14aaeef3,
      0x14b07af6,
      0x14b606fa,
      0x14bb92fd,
      0x14c11f01,
      0x14c6ab05,
      0x14cc3709,
      0x14d1c30d,
      0x14d74f11,
      0x14dcdb16,
      0x14e2671a,
      0x14e7f31f,
      0x14ed7f24,
      0x14f30b29,
      0x14f8972e,
      0x14fe2333,
      0x1503af38,
      0x15093b3d,
      0x150ec743,
      0x15145348,
      0x1519df4e,
      0x151f6b54,
      0x1524f75a,
      0x152a8360,
      0x15300f66,
      0x15359b6c,
      0x153b2773,
      0x1540b379,
      0x15463f80,
      0x154bcb87,
      0x1551578e,
      0x1556e395,
      0x155c6f9c,
      0x1561fba3,
      0x156787aa,
      0x156d13b2,
      0x15729fb9,
      0x15782bc1,
      0x157db7c9,
      0x158343d1,
      0x1588cfd9,
      0x158e5be1,
      0x1593e7ea,
      0x159973f2,
      0x159efffb,
      0x15a48c03,
      0x15aa180c,
      0x15afa415,
      0x15b5301e,
      0x15babc27,
      0x15c04831,
      0x15c5d43a,
      0x15cb6044,
      0x15d0ec4d,
      0x15d67857,
      0x15dc0461,
      0x15e1906b,
      0x15e71c75,
      0x15eca87f,
      0x15f2348a,
      0x15f7c094,
      0x15fd4c9f,
      0x1602d8aa,
      0x160864b5,
      0x160df0c0,
      0x16137ccb,
      0x161908d6,
      0x161e94e1,
      0x162420ed,
      0x1629acf8,
    };
    setRom< HWOffsetFix<29,-40,UNSIGNED> > (data, id444sta_rom_store, 29, 1024); 
  }
  { // Node ID: 441 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x162f3904,
      0x2c605e2f,
      0x42936faa,
      0x58c86da2,
      0x6eff5840,
      0x85382faf,
      0x9b72f41a,
      0xb1afa5ac,
      0xc7ee448f,
      0xde2ed0ee,
      0xf4714af4,
      0x10ab5b2cc,
      0x120fc08a0,
      0x137444c9b,
      0x14d8e7ee9,
      0x163da9fb3,
      0x17a28af25,
      0x19078ad6a,
      0x1a6ca9aac,
      0x1bd1e7717,
      0x1d37442d5,
      0x1e9cbfe11,
      0x20025a8f7,
      0x2168143b0,
      0x22cdece69,
      0x2433e494b,
      0x2599fb483,
      0x27003103b,
      0x286685c9e,
      0x29ccf99d7,
      0x2b338c811,
      0x2c9a3e778,
      0x2e010f836,
      0x2f67ffa76,
      0x30cf0ee64,
      0x32363d42b,
      0x339d8abf6,
      0x3504f75ef,
      0x366c83243,
      0x37d42e11c,
      0x393bf82a5,
      0x3aa3e170b,
      0x3c0be9e77,
      0x3d7411916,
      0x3edc58712,
      0x4044be897,
      0x41ad43dd0,
      0x4315e86e8,
      0x447eac40b,
      0x45e78f564,
      0x475091b1e,
      0x48b9b3566,
      0x4a22f4465,
      0x4b8c54848,
      0x4cf5d4139,
      0x4e5f72f65,
      0x4fc9312f7,
      0x51330ec1a,
      0x529d0bafa,
      0x540727fc1,
      0x557163a9d,
      0x56dbbebb8,
      0x58463933d,
      0x59b0d3158,
      0x5b1b8c636,
      0x5c8665200,
      0x5df15d4e4,
      0x5f5c74f0c,
      0x60c7ac0a4,
      0x6233029d8,
      0x639e78ad4,
      0x650a0e3c2,
      0x6675c34cf,
      0x67e197e27,
      0x694d8bff4,
      0x6ab99fa64,
      0x6c25d2da1,
      0x6d92259d8,
      0x6efe97f33,
      0x706b29ddf,
      0x71d7db608,
      0x7344ac7da,
      0x74b19d380,
      0x761ead925,
      0x778bdd8f7,
      0x78f92d321,
      0x7a669c7ce,
      0x7bd42b72b,
      0x7d41da163,
      0x7eafa86a2,
      0x801d96715,
      0x818ba42e8,
      0x82f9d1a45,
      0x84681ed5a,
      0x85d68bc52,
      0x87451875a,
      0x88b3c4e9c,
      0x8a2291246,
      0x8b917d284,
      0x8d0088f81,
      0x8e6fb4969,
      0x8fdf00069,
      0x914e6b4ad,
      0x92bdf6660,
      0x942da15b0,
      0x959d6c2c8,
      0x970d56dd5,
      0x987d61701,
      0x99ed8be7b,
      0x9b5dd646e,
      0x9cce40906,
      0x9e3ecac6f,
      0x9faf74ed6,
      0xa1203f068,
      0xa2912914f,
      0xa402331b9,
      0xa5735d1d3,
      0xa6e4a71c7,
      0xa856111c3,
      0xa9c79b1f4,
      0xab3945284,
      0xacab0f3a2,
      0xae1cf9578,
      0xaf8f03835,
      0xb1012dc03,
      0xb27378111,
      0xb3e5e2789,
      0xb5586cf99,
      0xb6cb1796c,
      0xb83de2531,
      0xb9b0cd312,
      0xbb23d833e,
      0xbc97035df,
      0xbe0a4eb23,
      0xbf7dba337,
      0xc0f145e47,
      0xc264f1c7f,
      0xc3d8bde0d,
      0xc54caa31d,
      0xc6c0b6bdb,
      0xc834e3875,
      0xc9a930916,
      0xcb1d9dded,
      0xcc922b724,
      0xce06d94eb,
      0xcf7ba776c,
      0xd0f095ed5,
      0xd265a4b52,
      0xd3dad3d11,
      0xd5502343e,
      0xd6c593106,
      0xd83b23396,
      0xd9b0d3c1b,
      0xdb26a4ac1,
      0xdc9c95fb6,
      0xde12a7b26,
      0xdf88d9d3f,
      0xe0ff2c62d,
      0xe2759f61d,
      0xe3ec32d3d,
      0xe562e6bb9,
      0xe6d9bb1be,
      0xe850aff7a,
      0xe9c7c5519,
      0xeb3efb2c8,
      0xecb6518b5,
      0xee2dc870c,
      0xefa55fdfb,
      0xf11d17dae,
      0xf294f0654,
      0xf40ce9818,
      0xf58503329,
      0xf6fd3d7b3,
      0xf875985e4,
      0xf9ee13de8,
      0xfb66affed,
      0xfcdf6cc21,
      0xfe584a2b0,
      0xffd1483c7,
      0x1014a66f95,
      0x102c3a6646,
      0x1043d06808,
      0x105b687507,
      0x1073028d72,
      0x108a9eb176,
      0x10a23ce13f,
      0x10b9dd1cfc,
      0x10d17f64da,
      0x10e923b906,
      0x1100ca19ae,
      0x11187286fe,
      0x11301d0126,
      0x1147c98851,
      0x115f781cae,
      0x117728be6a,
      0x118edb6db3,
      0x11a6902ab6,
      0x11be46f5a0,
      0x11d5ffcea0,
      0x11edbab5e3,
      0x120577ab96,
      0x121d36afe7,
      0x1234f7c304,
      0x124cbae51a,
      0x1264801658,
      0x127c4756ea,
      0x129410a6fe,
      0x12abdc06c3,
      0x12c3a97666,
      0x12db78f614,
      0x12f34a85fb,
      0x130b1e264a,
      0x1322f3d72e,
      0x133acb98d4,
      0x1352a56b6b,
      0x136a814f20,
      0x13825f4422,
      0x139a3f4a9e,
      0x13b22162c1,
      0x13ca058cbb,
      0x13e1ebc8b8,
      0x13f9d416e7,
      0x1411be7776,
      0x1429aaea93,
      0x144199706b,
      0x14598a092d,
      0x14717cb506,
      0x1489717425,
      0x14a16846b8,
      0x14b9612ced,
      0x14d15c26f1,
      0x14e95934f3,
      0x1501585721,
      0x1519598daa,
      0x15315cd8ba,
      0x1549623881,
      0x156169ad2d,
      0x15797336eb,
      0x15917ed5ea,
      0x15a98c8a59,
      0x15c19c5465,
      0x15d9ae343c,
      0x15f1c22a0d,
      0x1609d83607,
      0x1621f05857,
      0x163a0a912b,
      0x165226e0b3,
      0x166a45471c,
      0x168265c495,
      0x169a88594c,
      0x16b2ad0570,
      0x16cad3c92e,
      0x16e2fca4b6,
      0x16fb279835,
      0x171354a3db,
      0x172b83c7d5,
      0x1743b50453,
      0x175be85982,
      0x17741dc791,
      0x178c554eaf,
      0x17a48eef0a,
      0x17bccaa8d1,
      0x17d5087c32,
      0x17ed48695c,
      0x18058a707d,
      0x181dce91c5,
      0x183614cd62,
      0x184e5d2381,
      0x1866a79453,
      0x187ef42006,
      0x189742c6c8,
      0x18af9388c9,
      0x18c7e66636,
      0x18e03b5f3f,
      0x18f8927413,
      0x1910eba4df,
      0x192946f1d4,
      0x1941a45b1f,
      0x195a03e0f0,
      0x1972658376,
      0x198ac942df,
      0x19a32f1f5a,
      0x19bb971916,
      0x19d4013042,
      0x19ec6d650d,
      0x1a04dbb7a6,
      0x1a1d4c283b,
      0x1a35beb6fd,
      0x1a4e336419,
      0x1a66aa2fbf,
      0x1a7f231a1d,
      0x1a979e2364,
      0x1ab01b4bc1,
      0x1ac89a9364,
      0x1ae11bfa7c,
      0x1af99f8139,
      0x1b122527c8,
      0x1b2aacee5a,
      0x1b4336d51d,
      0x1b5bc2dc41,
      0x1b745103f4,
      0x1b8ce14c67,
      0x1ba573b5c8,
      0x1bbe084046,
      0x1bd69eec11,
      0x1bef37b957,
      0x1c07d2a849,
      0x1c206fb916,
      0x1c390eebec,
      0x1c51b040fb,
      0x1c6a53b872,
      0x1c82f95282,
      0x1c9ba10f58,
      0x1cb44aef25,
      0x1cccf6f218,
      0x1ce5a51860,
      0x1cfe55622d,
      0x1d1707cfaf,
      0x1d2fbc6114,
      0x1d4873168c,
      0x1d612bf046,
      0x1d79e6ee73,
      0x1d92a41142,
      0x1dab6358e1,
      0x1dc424c582,
      0x1ddce85753,
      0x1df5ae0e84,
      0x1e0e75eb44,
      0x1e273fedc4,
      0x1e400c1632,
      0x1e58da64bf,
      0x1e71aad99a,
      0x1e8a7d74f3,
      0x1ea35236f9,
      0x1ebc291fdd,
      0x1ed5022fce,
      0x1eeddd66fb,
      0x1f06bac595,
      0x1f1f9a4bcb,
      0x1f387bf9ce,
      0x1f515fcfcc,
      0x1f6a45cdf6,
      0x1f832df47c,
      0x1f9c18438d,
      0x1fb504bb59,
      0x1fcdf35c11,
      0x1fe6e425e4,
      0x1fffd71902,
      0x2018cc359b,
      0x2031c37be0,
      0x204abcebff,
      0x2063b88629,
      0x207cb64a8e,
      0x2095b6395e,
      0x20aeb852c9,
      0x20c7bc9700,
      0x20e0c30631,
      0x20f9cba08e,
      0x2112d66647,
      0x212be3578b,
      0x2144f2748a,
      0x215e03bd76,
      0x217717327d,
      0x21902cd3d1,
      0x21a944a1a1,
      0x21c25e9c1d,
      0x21db7ac377,
      0x21f49917de,
      0x220db99982,
      0x2226dc4894,
      0x2240012544,
      0x2259282fc2,
      0x227251683f,
      0x228b7cceeb,
      0x22a4aa63f6,
      0x22bdda2791,
      0x22d70c19ec,
      0x22f0403b38,
      0x2309768ba5,
      0x2322af0b64,
      0x233be9baa4,
      0x2355269997,
      0x236e65a86d,
      0x2387a6e756,
      0x23a0ea5683,
      0x23ba2ff625,
      0x23d377c66c,
      0x23ecc1c789,
      0x24060df9ac,
      0x241f5c5d06,
      0x2438acf1c8,
      0x2451ffb821,
      0x246b54b044,
      0x2484abda60,
      0x249e0536a7,
      0x24b760c548,
      0x24d0be8675,
      0x24ea1e7a5f,
      0x250380a136,
      0x251ce4fb2a,
      0x25364b886e,
      0x254fb44931,
      0x25691f3da5,
      0x25828c65fa,
      0x259bfbc261,
      0x25b56d530c,
      0x25cee1182a,
      0x25e85711ed,
      0x2601cf4086,
      0x261b49a425,
      0x2634c63cfd,
      0x264e450b3d,
      0x2667c60f16,
      0x26814948bb,
      0x269aceb85b,
      0x26b4565e28,
      0x26cde03a53,
      0x26e76c4d0c,
      0x2700fa9686,
      0x271a8b16f1,
      0x27341dce7e,
      0x274db2bd5e,
      0x276749e3c3,
      0x2780e341de,
      0x279a7ed7e0,
      0x27b41ca5fa,
      0x27cdbcac5d,
      0x27e75eeb3b,
      0x28010362c5,
      0x281aaa132c,
      0x283452fca1,
      0x284dfe1f56,
      0x2867ab7b7d,
      0x28815b1145,
      0x289b0ce0e2,
      0x28b4c0ea84,
      0x28ce772e5c,
      0x28e82fac9d,
      0x2901ea6577,
      0x291ba7591c,
      0x29356687bd,
      0x294f27f18c,
      0x2968eb96ba,
      0x2982b1777a,
      0x299c7993fb,
      0x29b643ec71,
      0x29d010810c,
      0x29e9df51fe,
      0x2a03b05f79,
      0x2a1d83a9ae,
      0x2a375930cf,
      0x2a5130f50d,
      0x2a6b0af69b,
      0x2a84e735aa,
      0x2a9ec5b26b,
      0x2ab8a66d11,
      0x2ad28965cd,
      0x2aec6e9cd0,
      0x2b0656124d,
      0x2b203fc676,
      0x2b3a2bb97b,
      0x2b5419eb90,
      0x2b6e0a5ce6,
      0x2b87fd0dae,
      0x2ba1f1fe1a,
      0x2bbbe92e5d,
      0x2bd5e29ea9,
      0x2befde4f2e,
      0x2c09dc4020,
      0x2c23dc71b0,
      0x2c3ddee410,
      0x2c57e39772,
      0x2c71ea8c08,
      0x2c8bf3c204,
      0x2ca5ff3998,
      0x2cc00cf2f7,
      0x2cda1cee51,
      0x2cf42f2bdb,
      0x2d0e43abc4,
      0x2d285a6e40,
      0x2d42737381,
      0x2d5c8ebbb9,
      0x2d76ac4719,
      0x2d90cc15d5,
      0x2daaee281f,
      0x2dc5127e27,
      0x2ddf391822,
      0x2df961f641,
      0x2e138d18b7,
      0x2e2dba7fb5,
      0x2e47ea2b6e,
      0x2e621c1c15,
      0x2e7c5051db,
      0x2e9686ccf3,
      0x2eb0bf8d90,
      0x2ecafa93e3,
      0x2ee537e020,
      0x2eff777278,
      0x2f19b94b1e,
      0x2f33fd6a45,
      0x2f4e43d01f,
      0x2f688c7cdf,
      0x2f82d770b6,
      0x2f9d24abd9,
      0x2fb7742e78,
      0x2fd1c5f8c7,
      0x2fec1a0af8,
      0x300670653e,
      0x3020c907cc,
      0x303b23f2d3,
      0x3055812688,
      0x306fe0a31b,
      0x308a4268c1,
      0x30a4a677ac,
      0x30bf0cd00e,
      0x30d975721b,
      0x30f3e05e05,
      0x310e4d93fe,
      0x3128bd143a,
      0x31432edeeb,
      0x315da2f445,
      0x3178195479,
      0x319291ffbc,
      0x31ad0cf63f,
      0x31c78a3836,
      0x31e209c5d3,
      0x31fc8b9f4a,
      0x32170fc4ce,
      0x3231963690,
      0x324c1ef4c5,
      0x3266a9ffa0,
      0x3281375753,
      0x329bc6fc11,
      0x32b658ee0e,
      0x32d0ed2d7c,
      0x32eb83ba8f,
      0x33061c9579,
      0x3320b7be6e,
      0x333b5535a2,
      0x3355f4fb46,
      0x3370970f8e,
      0x338b3b72ae,
      0x33a5e224d9,
      0x33c08b2641,
      0x33db36771b,
      0x33f5e41799,
      0x34109407ee,
      0x342b46484f,
      0x3445fad8ed,
      0x3460b1b9fd,
      0x347b6aebb2,
      0x3496266e40,
      0x34b0e441d8,
      0x34cba466b0,
      0x34e666dcfa,
      0x35012ba4ea,
      0x351bf2beb4,
      0x3536bc2a8a,
      0x355187e8a0,
      0x356c55f92a,
      0x3587265c5b,
      0x35a1f91267,
      0x35bcce1b81,
      0x35d7a577dd,
      0x35f27f27af,
      0x360d5b2b2a,
      0x3628398281,
      0x36431a2de9,
      0x365dfd2d94,
      0x3678e281b7,
      0x3693ca2a86,
      0x36aeb42833,
      0x36c9a07af4,
      0x36e48f22fa,
      0x36ff80207c,
      0x371a7373ab,
      0x3735691cbc,
      0x3750611be2,
      0x376b5b7152,
      0x3786581d3f,
      0x37a1571fde,
      0x37bc587961,
      0x37d75c29fe,
      0x37f26231e7,
      0x380d6a9152,
      0x3828754870,
      0x3843825778,
      0x385e91be9d,
      0x3879a37e12,
      0x3894b7960b,
      0x38afce06be,
      0x38cae6d05e,
      0x38e601f31e,
      0x39011f6f33,
      0x391c3f44d2,
      0x393761742e,
      0x395285fd7b,
      0x396dace0ed,
      0x3988d61eba,
      0x39a401b714,
      0x39bf2faa31,
      0x39da5ff843,
      0x39f592a181,
      0x3a10c7a61d,
      0x3a2bff064d,
      0x3a4738c244,
      0x3a6274da37,
      0x3a7db34e5a,
      0x3a98f41ee2,
      0x3ab4374c02,
      0x3acf7cd5f0,
      0x3aeac4bcdf,
      0x3b060f0105,
      0x3b215ba295,
      0x3b3caaa1c4,
      0x3b57fbfec7,
      0x3b734fb9d2,
      0x3b8ea5d319,
      0x3ba9fe4ad1,
      0x3bc559212f,
      0x3be0b65667,
      0x3bfc15eaae,
      0x3c1777de38,
      0x3c32dc313b,
      0x3c4e42e3ea,
      0x3c69abf67a,
      0x3c85176920,
      0x3ca0853c11,
      0x3cbbf56f81,
      0x3cd76803a6,
      0x3cf2dcf8b3,
      0x3d0e544ede,
      0x3d29ce065c,
      0x3d454a1f60,
      0x3d60c89a21,
      0x3d7c4976d2,
      0x3d97ccb5aa,
      0x3db35256dc,
      0x3dceda5a9d,
      0x3dea64c123,
      0x3e05f18aa3,
      0x3e2180b750,
      0x3e3d124761,
      0x3e58a63b0a,
      0x3e743c9280,
      0x3e8fd54df9,
      0x3eab706da9,
      0x3ec70df1c5,
      0x3ee2adda83,
      0x3efe502817,
      0x3f19f4dab7,
      0x3f359bf297,
      0x3f51456fee,
      0x3f6cf152ef,
      0x3f889f9bd1,
      0x3fa4504ac8,
      0x3fc003600a,
      0x3fdbb8dbcb,
      0x3ff770be42,
      0x40132b07a3,
      0x402ee7b824,
      0x404aa6cffa,
      0x4066684f5a,
      0x40822c367a,
      0x409df2858f,
      0x40b9bb3cce,
      0x40d5865c6d,
      0x40f153e4a1,
      0x410d23d5a0,
      0x4128f62f9f,
      0x4144caf2d3,
      0x4160a21f73,
      0x417c7bb5b3,
      0x419857b5c9,
      0x41b4361feb,
      0x41d016f44e,
      0x41ebfa3327,
      0x4207dfdcad,
      0x4223c7f115,
      0x423fb27094,
      0x425b9f5b61,
      0x42778eb1b1,
      0x42938073b9,
      0x42af74a1af,
      0x42cb6b3bca,
      0x42e764423e,
      0x43035fb542,
      0x431f5d950b,
      0x433b5de1cf,
      0x4357609bc4,
      0x437365c31f,
      0x438f6d5817,
      0x43ab775ae2,
      0x43c783cbb5,
      0x43e392aac6,
      0x43ffa3f84c,
      0x441bb7b47b,
      0x4437cddf8b,
      0x4453e679b0,
      0x4470018322,
      0x448c1efc15,
      0x44a83ee4c1,
      0x44c4613d5b,
      0x44e0860619,
      0x44fcad3f31,
      0x4518d6e8d9,
      0x4535030348,
      0x4551318eb4,
      0x456d628b53,
      0x458995f95a,
      0x45a5cbd901,
      0x45c2042a7d,
      0x45de3eee05,
      0x45fa7c23ce,
      0x4616bbcc10,
      0x4632fde700,
      0x464f4274d5,
      0x466b8975c5,
      0x4687d2ea07,
      0x46a41ed1d0,
      0x46c06d2d57,
      0x46dcbdfcd3,
      0x46f911407a,
      0x471566f882,
      0x4731bf2523,
      0x474e19c691,
      0x476a76dd04,
      0x4786d668b3,
      0x47a33869d4,
      0x47bf9ce09d,
      0x47dc03cd45,
      0x47f86d3002,
      0x4814d9090c,
      0x4831475898,
      0x484db81ede,
      0x486a2b5c14,
      0x4886a11070,
      0x48a3193c2b,
      0x48bf93df79,
      0x48dc10fa92,
      0x48f8908dad,
      0x4915129900,
      0x4931971cc3,
      0x494e1e192b,
      0x496aa78e70,
      0x4987337cc9,
      0x49a3c1e46d,
      0x49c052c591,
      0x49dce6206f,
      0x49f97bf53b,
      0x4a1614442e,
      0x4a32af0d7d,
      0x4a4f4c5161,
      0x4a6bec1010,
      0x4a888e49c1,
      0x4aa532feab,
      0x4ac1da2f05,
      0x4ade83db07,
      0x4afb3002e6,
      0x4b17dea6db,
      0x4b348fc71d,
      0x4b514363e3,
      0x4b6df97d63,
      0x4b8ab213d5,
      0x4ba76d2771,
      0x4bc42ab86d,
      0x4be0eac700,
      0x4bfdad5363,
      0x4c1a725dcb,
      0x4c3739e671,
      0x4c5403ed8c,
      0x4c70d07353,
      0x4c8d9f77fe,
      0x4caa70fbc3,
      0x4cc744fedb,
      0x4ce41b817c,
      0x4d00f483de,
      0x4d1dd00639,
      0x4d3aae08c4,
      0x4d578e8bb6,
      0x4d74718f46,
      0x4d915713ae,
      0x4dae3f1923,
      0x4dcb299fde,
      0x4de816a816,
      0x4e05063202,
      0x4e21f83ddb,
      0x4e3eeccbd8,
      0x4e5be3dc30,
      0x4e78dd6f1b,
      0x4e95d984d2,
      0x4eb2d81d8b,
      0x4ecfd9397e,
      0x4eecdcd8e3,
      0x4f09e2fbf3,
      0x4f26eba2e3,
      0x4f43f6cdee,
      0x4f61047d49,
      0x4f7e14b12d,
      0x4f9b2769d3,
      0x4fb83ca771,
      0x4fd5546a40,
      0x4ff26eb277,
      0x500f8b804f,
      0x502caad3ff,
      0x5049ccadc0,
      0x5066f10dc9,
      0x508417f453,
      0x50a1416195,
      0x50be6d55c8,
      0x50db9bd123,
      0x50f8ccd3df,
      0x5116005e33,
      0x5133367059,
      0x51506f0a87,
      0x516daa2cf6,
      0x518ae7d7df,
      0x51a8280b7a,
      0x51c56ac7fe,
      0x51e2b00da4,
      0x51fff7dca4,
      0x521d423537,
      0x523a8f1794,
      0x5257de83f5,
      0x5275307a91,
      0x529284fba1,
      0x52afdc075d,
      0x52cd359dfd,
      0x52ea91bfbb,
      0x5307f06cce,
      0x532551a56e,
      0x5342b569d5,
      0x53601bba3b,
      0x537d8496d7,
      0x539aefffe4,
      0x53b85df599,
      0x53d5ce782f,
      0x53f34187de,
      0x5410b724df,
      0x542e2f4f6b,
      0x544baa07ba,
      0x5469274e05,
      0x5486a72285,
      0x54a4298572,
      0x54c1ae7705,
      0x54df35f776,
      0x54fcc00700,
      0x551a4ca5d9,
      0x5537dbd43c,
      0x55556d9260,
      0x557301e07f,
      0x559098bed2,
      0x55ae322d91,
      0x55cbce2cf5,
      0x55e96cbd37,
      0x56070dde91,
      0x5624b1913b,
      0x564257d56d,
      0x566000ab62,
      0x567dac1352,
      0x569b5a0d75,
      0x56b90a9a06,
      0x56d6bdb93d,
      0x56f4736b52,
      0x57122bb081,
      0x572fe68900,
      0x574da3f50a,
      0x576b63f4d8,
      0x57892688a3,
      0x57a6ebb0a4,
      0x57c4b36d14,
      0x57e27dbe2c,
      0x58004aa427,
      0x581e1a1f3c,
      0x583bec2fa5,
      0x5859c0d59d,
      0x587798115b,
      0x589571e319,
      0x58b34e4b11,
      0x58d12d497c,
      0x58ef0ede94,
      0x590cf30a92,
      0x592ad9cdae,
      0x5948c32824,
      0x5966af1a2c,
      0x59849da400,
      0x59a28ec5d9,
      0x59c0827ff0,
      0x59de78d281,
      0x59fc71bdc3,
      0x5a1a6d41f1,
      0x5a386b5f44,
      0x5a566c15f6,
      0x5a746f6640,
      0x5a9275505d,
      0x5ab07dd485,
      0x5ace88f2f3,
      0x5aec96abe1,
      0x5b0aa6ff87,
      0x5b28b9ee21,
      0x5b46cf77e7,
      0x5b64e79d14,
      0x5b83025de0,
      0x5ba11fba88,
      0x5bbf3fb343,
      0x5bdd62484c,
      0x5bfb8779dd,
      0x5c19af4830,
      0x5c37d9b37e,
      0x5c5606bc03,
      0x5c743661f7,
      0x5c9268a594,
      0x5cb09d8716,
      0x5cced506b5,
      0x5ced0f24ac,
      0x5d0b4be136,
      0x5d298b3c8b,
      0x5d47cd36e6,
      0x5d6611d082,
      0x5d84590999,
      0x5da2a2e264,
      0x5dc0ef5b1e,
      0x5ddf3e7401,
      0x5dfd902d48,
      0x5e1be4872c,
      0x5e3a3b81e8,
      0x5e58951db7,
      0x5e76f15ad2,
      0x5e95503974,
      0x5eb3b1b9d8,
      0x5ed215dc37,
      0x5ef07ca0cc,
      0x5f0ee607d2,
      0x5f2d521182,
      0x5f4bc0be19,
      0x5f6a320dcf,
      0x5f88a600df,
      0x5fa71c9785,
      0x5fc595d1fa,
      0x5fe411b079,
      0x600290333d,
      0x6021115a80,
      0x603f95267c,
      0x605e1b976e,
      0x607ca4ad8e,
      0x609b306919,
      0x60b9beca48,
      0x60d84fd156,
      0x60f6e37e7e,
      0x611579d1fc,
      0x613412cc08,
      0x6152ae6cdf,
      0x61714cb4bc,
      0x618feda3d8,
      0x61ae913a6f,
      0x61cd3778bd,
      0x61ebe05efa,
      0x620a8bed64,
      0x62293a2434,
      0x6247eb03a5,
      0x62669e8bf3,
      0x628554bd59,
      0x62a40d9811,
      0x62c2c91c57,
      0x62e1874a65,
      0x6300482277,
      0x631f0ba4c8,
      0x633dd1d193,
      0x635c9aa912,
      0x637b662b83,
      0x639a34591e,
      0x63b9053220,
      0x63d7d8b6c4,
      0x63f6aee746,
      0x641587c3df,
      0x6434634ccc,
      0x6453418248,
      0x647222648f,
      0x649105f3db,
      0x64afec3068,
      0x64ced51a71,
      0x64edc0b232,
      0x650caef7e6,
      0x652b9febc9,
      0x654a938e16,
      0x656989df08,
      0x658882dedc,
      0x65a77e8dcc,
      0x65c67cec15,
      0x65e57df9f1,
      0x660481b79c,
      0x6623882552,
      0x664291434f,
      0x66619d11ce,
      0x6680ab910b,
      0x669fbcc141,
      0x66bed0a2ac,
      0x66dde73589,
      0x66fd007a12,
      0x671c1c7083,
      0x673b3b1919,
      0x675a5c740f,
      0x67798081a1,
      0x6798a7420a,
      0x67b7d0b587,
      0x67d6fcdc54,
      0x67f62bb6ab,
      0x68155d44cb,
      0x68349186ed,
      0x6853c87d4f,
      0x687302282c,
      0x68923e87c0,
      0x68b17d9c47,
      0x68d0bf65fe,
      0x68f003e520,
      0x690f4b19e9,
      0x692e950496,
      0x694de1a563,
      0x696d30fc8c,
      0x698c830a4d,
      0x69abd7cee1,
      0x69cb2f4a87,
      0x69ea897d79,
    };
    setRom< HWOffsetFix<39,-40,UNSIGNED> > (data, id441sta_rom_store, 39, 1024); 
  }
  { // Node ID: 438 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x6a09e667f4,
    };
    setRom< HWOffsetFix<40,-40,UNSIGNED> > (data, id438sta_rom_store, 40, 2); 
  }
  { // Node ID: 294 (NodeConstantRawBits)
    id294out_value = (c_hw_fix_21_n21_uns_bits);
  }
  { // Node ID: 297 (NodeConstantRawBits)
    id297out_value = (c_hw_fix_22_n43_uns_bits);
  }
  { // Node ID: 303 (NodeConstantRawBits)
    id303out_value = (c_hw_fix_64_n63_uns_bits);
  }
  { // Node ID: 307 (NodeConstantRawBits)
    id307out_value = (c_hw_fix_44_n43_uns_bits);
  }
  { // Node ID: 312 (NodeConstantRawBits)
    id312out_value = (c_hw_fix_64_n64_uns_bits);
  }
  { // Node ID: 316 (NodeConstantRawBits)
    id316out_value = (c_hw_fix_64_n62_uns_bits);
  }
  { // Node ID: 320 (NodeConstantRawBits)
    id320out_value = (c_hw_fix_45_n43_uns_bits);
  }
  { // Node ID: 325 (NodeConstantRawBits)
    id325out_value = (c_hw_fix_64_n73_uns_bits);
  }
  { // Node ID: 329 (NodeConstantRawBits)
    id329out_value = (c_hw_fix_64_n61_uns_bits);
  }
  { // Node ID: 333 (NodeConstantRawBits)
    id333out_value = (c_hw_fix_46_n43_uns_bits);
  }
  { // Node ID: 337 (NodeConstantRawBits)
    id337out_value = (c_hw_fix_40_n39_uns_bits);
  }
  { // Node ID: 836 (NodeConstantRawBits)
    id836out_value = (c_hw_fix_40_n39_uns_bits_1);
  }
  { // Node ID: 372 (NodeConstantRawBits)
    id372out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 835 (NodeConstantRawBits)
    id835out_value = (c_hw_flt_8_40_bits_4);
  }
  { // Node ID: 834 (NodeConstantRawBits)
    id834out_value = (c_hw_flt_8_40_bits_4);
  }
  { // Node ID: 833 (NodeConstantRawBits)
    id833out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 408 (NodeConstantRawBits)
    id408out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 395 (NodeConstantRawBits)
    id395out_value = (c_hw_fix_8_0_sgn_bits);
  }
  { // Node ID: 341 (NodeConstantRawBits)
    id341out_value = (c_hw_fix_40_n39_uns_bits_2);
  }
  { // Node ID: 345 (NodeConstantRawBits)
    id345out_value = (c_hw_fix_39_n39_uns_bits);
  }
  { // Node ID: 417 (NodeConstantRawBits)
    id417out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 418 (NodeConstantRawBits)
    id418out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 420 (NodeConstantRawBits)
    id420out_value = (c_hw_fix_39_0_uns_bits);
  }
  { // Node ID: 503 (NodeConstantRawBits)
    id503out_value = (c_hw_flt_8_40_bits_4);
  }
  { // Node ID: 832 (NodeConstantRawBits)
    id832out_value = (c_hw_flt_8_40_bits_6);
  }
  { // Node ID: 831 (NodeConstantRawBits)
    id831out_value = (c_hw_flt_8_40_bits_2);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_8_40_bits_7);
  }
  { // Node ID: 22 (NodeConstantRawBits)
    id22out_value = (c_hw_flt_8_40_bits_8);
  }
  { // Node ID: 4 (NodeConstantRawBits)
    id4out_value = (c_hw_flt_8_40_bits_9);
  }
  { // Node ID: 41 (NodeConstantRawBits)
    id41out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 791 (NodeOutputMappedReg)
    registerMappedRegister("all_numeric_exceptions", Data(36), false);
  }
  { // Node ID: 767 (NodeConstantRawBits)
    id767out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 768 (NodeOutput)
    m_internal_watch_carriedu_output = registerOutput("internal_watch_carriedu_output",2 );
  }
  { // Node ID: 830 (NodeConstantRawBits)
    id830out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 480 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 483 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 829 (NodeConstantRawBits)
    id829out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 487 (NodeInputMappedReg)
    registerMappedRegister("io_v_out_force_disabled", Data(1));
  }
  { // Node ID: 490 (NodeOutput)
    m_v_out = registerOutput("v_out",1 );
  }
  { // Node ID: 495 (NodeConstantRawBits)
    id495out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 828 (NodeConstantRawBits)
    id828out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 492 (NodeConstantRawBits)
    id492out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 496 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 827 (NodeConstantRawBits)
    id827out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 498 (NodeConstantRawBits)
    id498out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 501 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void KarmaDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void KarmaDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 17 (NodeCounter)

    (id17st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    (id16st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 813 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id813out_output[i] = (c_hw_flt_8_40_undef);
    }
  }
  { // Node ID: 825 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id825out_output[i] = (c_hw_flt_8_40_undef);
    }
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 799 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id799out_output[i] = (c_hw_flt_8_40_undef);
    }
  }
  { // Node ID: 812 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id812out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 804 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id804out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 805 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id805out_output[i] = (c_hw_fix_22_n43_uns_undef);
    }
  }
  { // Node ID: 806 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id806out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 807 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id807out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 808 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id808out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 809 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id809out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 770 (NodeExceptionMask)

    (id770st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 817 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id817out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 771 (NodeExceptionMask)

    (id771st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 818 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id818out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 772 (NodeExceptionMask)

    (id772st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 819 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id819out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 774 (NodeExceptionMask)

    (id774st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 775 (NodeExceptionMask)

    (id775st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 776 (NodeExceptionMask)

    (id776st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 777 (NodeExceptionMask)

    (id777st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 778 (NodeExceptionMask)

    (id778st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 779 (NodeExceptionMask)

    (id779st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 780 (NodeExceptionMask)

    (id780st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 781 (NodeExceptionMask)

    (id781st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 782 (NodeExceptionMask)

    (id782st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 783 (NodeExceptionMask)

    (id783st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 786 (NodeExceptionMask)

    (id786st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 773 (NodeExceptionMask)

    (id773st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 785 (NodeExceptionMask)

    (id785st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 787 (NodeExceptionMask)

    (id787st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 788 (NodeExceptionMask)

    (id788st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 789 (NodeExceptionMask)

    (id789st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 784 (NodeExceptionMask)

    (id784st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 769 (NodeExceptionMask)

    (id769st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 820 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id820out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 821 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id821out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 480 (NodeInputMappedReg)
    id480out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 822 (NodeFIFO)

    for(int i=0; i<14; i++)
    {
      id822out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 487 (NodeInputMappedReg)
    id487out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 824 (NodeFIFO)

    for(int i=0; i<14; i++)
    {
      id824out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 493 (NodeCounter)

    (id493st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 499 (NodeCounter)

    (id499st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 501 (NodeInputMappedReg)
    id501out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::updateState() {
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 480 (NodeInputMappedReg)
    id480out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 487 (NodeInputMappedReg)
    id487out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 501 (NodeInputMappedReg)
    id501out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaDFEKernel::preExecute() {
}

void KarmaDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "KarmaDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int KarmaDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
