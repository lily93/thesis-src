#ifndef KARMADFEKERNEL_H_
#define KARMADFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaDFEKernel : public KernelManagerBlockSync {
public:
  KarmaDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id844out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id878out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id896out_value;

  HWOffsetFix<1,0,UNSIGNED> id531out_result[2];

  HWFloat<8,48> id865out_output[10];

  HWFloat<8,48> id877out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<8,48> id19out_o[4];

  HWFloat<8,48> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id895out_value;

  HWOffsetFix<1,0,UNSIGNED> id532out_result[2];

  HWFloat<8,48> id894out_value;

  HWFloat<8,48> id5out_value;

  HWFloat<8,48> id3out_value;

  HWFloat<8,48> id26out_value;

  HWFloat<8,48> id27out_result[2];

  HWFloat<8,48> id851out_output[11];

  HWFloat<8,48> id893out_value;

  HWFloat<8,48> id892out_value;

  HWFloat<8,48> id891out_value;

  HWRawBits<8> id456out_value;

  HWRawBits<47> id890out_value;

  HWOffsetFix<1,0,UNSIGNED> id864out_output[9];

  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<52,-51,UNSIGNED> id114out_value;

  HWOffsetFix<113,-102,TWOSCOMPLEMENT> id116out_value;

  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id236out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id856out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id889out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id378out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id394out_value;

  HWOffsetFix<33,-48,UNSIGNED> id471out_dout[3];

  HWOffsetFix<33,-48,UNSIGNED> id471sta_rom_store[1024];

  HWOffsetFix<43,-48,UNSIGNED> id468out_dout[3];

  HWOffsetFix<43,-48,UNSIGNED> id468sta_rom_store[1024];

  HWOffsetFix<48,-48,UNSIGNED> id465out_dout[3];

  HWOffsetFix<48,-48,UNSIGNED> id465sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id318out_value;

  HWOffsetFix<26,-51,UNSIGNED> id857out_output[3];

  HWOffsetFix<26,-51,UNSIGNED> id321out_value;

  HWOffsetFix<64,-89,UNSIGNED> id326out_value;

  HWOffsetFix<64,-63,UNSIGNED> id330out_value;

  HWOffsetFix<52,-51,UNSIGNED> id334out_value;

  HWOffsetFix<64,-68,UNSIGNED> id339out_value;

  HWOffsetFix<64,-62,UNSIGNED> id343out_value;

  HWOffsetFix<53,-51,UNSIGNED> id347out_value;

  HWOffsetFix<64,-77,UNSIGNED> id352out_value;

  HWOffsetFix<64,-61,UNSIGNED> id356out_value;

  HWOffsetFix<54,-51,UNSIGNED> id360out_value;

  HWOffsetFix<48,-47,UNSIGNED> id364out_value;

  HWOffsetFix<48,-47,UNSIGNED> id888out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id399out_value;

  HWFloat<8,48> id887out_value;

  HWOffsetFix<1,0,UNSIGNED> id858out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id859out_output[3];

  HWFloat<8,48> id886out_value;

  HWOffsetFix<1,0,UNSIGNED> id860out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id861out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id885out_value;

  HWOffsetFix<1,0,UNSIGNED> id435out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id422out_value;

  HWOffsetFix<48,-47,UNSIGNED> id368out_value;

  HWOffsetFix<47,-47,UNSIGNED> id372out_value;

  HWOffsetFix<1,0,UNSIGNED> id444out_value;

  HWOffsetFix<8,0,UNSIGNED> id445out_value;

  HWOffsetFix<47,0,UNSIGNED> id447out_value;

  HWFloat<8,48> id530out_value;

  HWFloat<8,48> id884out_value;

  HWFloat<8,48> id883out_value;

  HWFloat<8,48> id817out_floatOut[2];

  HWFloat<8,48> id2out_value;

  HWFloat<8,48> id22out_value;

  HWFloat<8,48> id23out_result[2];

  HWFloat<8,48> id4out_value;

  HWFloat<8,48> id816out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id816out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id821out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id821st_reg;

  HWOffsetFix<4,0,UNSIGNED> id869out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id822out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id822st_reg;

  HWOffsetFix<1,0,UNSIGNED> id870out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id823out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id823st_reg;

  HWOffsetFix<1,0,UNSIGNED> id871out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id825out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id825st_reg;

  HWOffsetFix<1,0,UNSIGNED> id826out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id826st_reg;

  HWOffsetFix<1,0,UNSIGNED> id827out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id827st_reg;

  HWOffsetFix<1,0,UNSIGNED> id828out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id828st_reg;

  HWOffsetFix<1,0,UNSIGNED> id829out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id829st_reg;

  HWOffsetFix<1,0,UNSIGNED> id830out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id830st_reg;

  HWOffsetFix<1,0,UNSIGNED> id831out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id831st_reg;

  HWOffsetFix<1,0,UNSIGNED> id832out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id832st_reg;

  HWOffsetFix<1,0,UNSIGNED> id833out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id833st_reg;

  HWOffsetFix<1,0,UNSIGNED> id834out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id834st_reg;

  HWOffsetFix<1,0,UNSIGNED> id835out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id835st_reg;

  HWOffsetFix<1,0,UNSIGNED> id838out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id838st_reg;

  HWOffsetFix<1,0,UNSIGNED> id824out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id824st_reg;

  HWOffsetFix<1,0,UNSIGNED> id837out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id837st_reg;

  HWOffsetFix<4,0,UNSIGNED> id839out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id839st_reg;

  HWOffsetFix<4,0,UNSIGNED> id840out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id840st_reg;

  HWOffsetFix<4,0,UNSIGNED> id841out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id841st_reg;

  HWOffsetFix<1,0,UNSIGNED> id836out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id836st_reg;

  HWOffsetFix<4,0,UNSIGNED> id820out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id820st_reg;

  HWOffsetFix<4,0,UNSIGNED> id872out_output[9];

  HWRawBits<37> id842out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id818out_value;

  HWOffsetFix<11,0,UNSIGNED> id873out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id882out_value;

  HWOffsetFix<11,0,UNSIGNED> id505out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id812out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id507out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id874out_output[13];

  HWFloat<11,53> id502out_o[3];

  HWOffsetFix<11,0,UNSIGNED> id881out_value;

  HWOffsetFix<11,0,UNSIGNED> id512out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id813out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id514out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id876out_output[13];

  HWFloat<11,53> id503out_o[3];

  HWOffsetFix<1,0,UNSIGNED> id522out_value;

  HWOffsetFix<1,0,UNSIGNED> id880out_value;

  HWOffsetFix<49,0,UNSIGNED> id519out_value;

  HWOffsetFix<48,0,UNSIGNED> id520out_count;
  HWOffsetFix<1,0,UNSIGNED> id520out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id520st_count;

  HWOffsetFix<1,0,UNSIGNED> id879out_value;

  HWOffsetFix<49,0,UNSIGNED> id525out_value;

  HWOffsetFix<48,0,UNSIGNED> id526out_count;
  HWOffsetFix<1,0,UNSIGNED> id526out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id526st_count;

  HWOffsetFix<48,0,UNSIGNED> id528out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id814out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,48> c_hw_flt_8_48_undef;
  const HWFloat<8,48> c_hw_flt_8_48_bits;
  const HWFloat<8,48> c_hw_flt_8_48_bits_1;
  const HWFloat<8,48> c_hw_flt_8_48_bits_2;
  const HWFloat<8,48> c_hw_flt_8_48_bits_3;
  const HWFloat<8,48> c_hw_flt_8_48_bits_4;
  const HWFloat<8,48> c_hw_flt_8_48_bits_5;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<47> c_hw_bit_47_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<61,-51,TWOSCOMPLEMENT> c_hw_fix_61_n51_sgn_bits;
  const HWOffsetFix<61,-51,TWOSCOMPLEMENT> c_hw_fix_61_n51_sgn_undef;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<113,-102,TWOSCOMPLEMENT> c_hw_fix_113_n102_sgn_bits;
  const HWOffsetFix<113,-102,TWOSCOMPLEMENT> c_hw_fix_113_n102_sgn_undef;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<33,-48,UNSIGNED> c_hw_fix_33_n48_uns_undef;
  const HWOffsetFix<43,-48,UNSIGNED> c_hw_fix_43_n48_uns_undef;
  const HWOffsetFix<48,-48,UNSIGNED> c_hw_fix_48_n48_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_undef;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_bits;
  const HWOffsetFix<64,-89,UNSIGNED> c_hw_fix_64_n89_uns_bits;
  const HWOffsetFix<64,-89,UNSIGNED> c_hw_fix_64_n89_uns_undef;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_bits;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_undef;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits_1;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_undef;
  const HWOffsetFix<64,-68,UNSIGNED> c_hw_fix_64_n68_uns_bits;
  const HWOffsetFix<64,-68,UNSIGNED> c_hw_fix_64_n68_uns_undef;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_bits;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_undef;
  const HWOffsetFix<53,-51,UNSIGNED> c_hw_fix_53_n51_uns_bits;
  const HWOffsetFix<53,-51,UNSIGNED> c_hw_fix_53_n51_uns_undef;
  const HWOffsetFix<64,-77,UNSIGNED> c_hw_fix_64_n77_uns_bits;
  const HWOffsetFix<64,-77,UNSIGNED> c_hw_fix_64_n77_uns_undef;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_bits;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_undef;
  const HWOffsetFix<54,-51,UNSIGNED> c_hw_fix_54_n51_uns_bits;
  const HWOffsetFix<54,-51,UNSIGNED> c_hw_fix_54_n51_uns_undef;
  const HWOffsetFix<48,-47,UNSIGNED> c_hw_fix_48_n47_uns_bits;
  const HWOffsetFix<48,-47,UNSIGNED> c_hw_fix_48_n47_uns_undef;
  const HWOffsetFix<48,-47,UNSIGNED> c_hw_fix_48_n47_uns_bits_1;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_bits;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_undef;
  const HWOffsetFix<48,-47,UNSIGNED> c_hw_fix_48_n47_uns_bits_2;
  const HWOffsetFix<47,-47,UNSIGNED> c_hw_fix_47_n47_uns_bits;
  const HWOffsetFix<47,-47,UNSIGNED> c_hw_fix_47_n47_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<47,0,UNSIGNED> c_hw_fix_47_0_uns_bits;
  const HWFloat<8,48> c_hw_flt_8_48_bits_6;
  const HWFloat<8,48> c_hw_flt_8_48_0_5val;
  const HWFloat<8,48> c_hw_flt_8_48_bits_7;
  const HWFloat<8,48> c_hw_flt_8_48_bits_8;
  const HWFloat<8,48> c_hw_flt_8_48_bits_9;
  const HWFloat<8,48> c_hw_flt_8_48_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMADFEKERNEL_H_ */
