#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaDFEKernel::execute0() {
  { // Node ID: 844 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 878 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id878out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 896 (NodeConstantRawBits)
  }
  { // Node ID: 531 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id531in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id531in_b = id896out_value;

    id531out_result[(getCycle()+1)%2] = (eq_fixed(id531in_a,id531in_b));
  }
  { // Node ID: 865 (NodeFIFO)
    const HWFloat<8,48> &id865in_input = id23out_result[getCycle()%2];

    id865out_output[(getCycle()+9)%10] = id865in_input;
  }
  { // Node ID: 877 (NodeFIFO)
    const HWFloat<8,48> &id877in_input = id865out_output[getCycle()%10];

    id877out_output[(getCycle()+1)%2] = id877in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 19 (NodeCast)
    const HWFloat<11,53> &id19in_i = id18out_dt;

    id19out_o[(getCycle()+3)%4] = (cast_float2float<8,48>(id19in_i));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 895 (NodeConstantRawBits)
  }
  { // Node ID: 532 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id532in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id532in_b = id895out_value;

    id532out_result[(getCycle()+1)%2] = (eq_fixed(id532in_a,id532in_b));
  }
  { // Node ID: 894 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id489out_result;

  { // Node ID: 489 (NodeGt)
    const HWFloat<8,48> &id489in_a = id877out_output[getCycle()%2];
    const HWFloat<8,48> &id489in_b = id5out_value;

    id489out_result = (gt_float(id489in_a,id489in_b));
  }
  HWFloat<8,48> id490out_o;

  { // Node ID: 490 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id490in_i = id489out_result;

    id490out_o = (cast_fixed2float<8,48>(id490in_i));
  }
  HWFloat<8,48> id492out_result;

  { // Node ID: 492 (NodeMul)
    const HWFloat<8,48> &id492in_a = id894out_value;
    const HWFloat<8,48> &id492in_b = id490out_o;

    id492out_result = (mul_float(id492in_a,id492in_b));
  }
  HWFloat<8,48> id493out_result;

  { // Node ID: 493 (NodeSub)
    const HWFloat<8,48> &id493in_a = id492out_result;
    const HWFloat<8,48> &id493in_b = id851out_output[getCycle()%11];

    id493out_result = (sub_float(id493in_a,id493in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,48> id495out_result;

  { // Node ID: 495 (NodeMul)
    const HWFloat<8,48> &id495in_a = id493out_result;
    const HWFloat<8,48> &id495in_b = id3out_value;

    id495out_result = (mul_float(id495in_a,id495in_b));
  }
  HWFloat<8,48> id498out_result;

  { // Node ID: 498 (NodeMul)
    const HWFloat<8,48> &id498in_a = id19out_o[getCycle()%4];
    const HWFloat<8,48> &id498in_b = id495out_result;

    id498out_result = (mul_float(id498in_a,id498in_b));
  }
  HWFloat<8,48> id499out_result;

  { // Node ID: 499 (NodeAdd)
    const HWFloat<8,48> &id499in_a = id851out_output[getCycle()%11];
    const HWFloat<8,48> &id499in_b = id498out_result;

    id499out_result = (add_float(id499in_a,id499in_b));
  }
  HWFloat<8,48> id848out_output;

  { // Node ID: 848 (NodeStreamOffset)
    const HWFloat<8,48> &id848in_input = id499out_result;

    id848out_output = id848in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id532out_result[getCycle()%2];
    const HWFloat<8,48> &id27in_option0 = id848out_output;
    const HWFloat<8,48> &id27in_option1 = id26out_value;

    HWFloat<8,48> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_8_48_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 851 (NodeFIFO)
    const HWFloat<8,48> &id851in_input = id27out_result[getCycle()%2];

    id851out_output[(getCycle()+10)%11] = id851in_input;
  }
  HWFloat<8,48> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<8,48> &id32in_a = id851out_output[getCycle()%11];
    const HWFloat<8,48> &id32in_b = id851out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<8,48> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<8,48> &id33in_a = id851out_output[getCycle()%11];
    const HWFloat<8,48> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<8,48> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<8,48> &id34in_a = id33out_result;
    const HWFloat<8,48> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<8,48> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<8,48> &id35in_a = id34out_result;
    const HWFloat<8,48> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<8,48> id486out_result;

  { // Node ID: 486 (NodeSub)
    const HWFloat<8,48> &id486in_a = id6out_value;
    const HWFloat<8,48> &id486in_b = id35out_result;

    id486out_result = (sub_float(id486in_a,id486in_b));
  }
  { // Node ID: 893 (NodeConstantRawBits)
  }
  { // Node ID: 892 (NodeConstantRawBits)
  }
  { // Node ID: 891 (NodeConstantRawBits)
  }
  HWRawBits<8> id455out_result;

  { // Node ID: 455 (NodeSlice)
    const HWFloat<8,48> &id455in_a = id816out_floatOut[getCycle()%2];

    id455out_result = (slice<47,8>(id455in_a));
  }
  { // Node ID: 456 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id533out_result;

  { // Node ID: 533 (NodeEqInlined)
    const HWRawBits<8> &id533in_a = id455out_result;
    const HWRawBits<8> &id533in_b = id456out_value;

    id533out_result = (eq_bits(id533in_a,id533in_b));
  }
  HWRawBits<47> id454out_result;

  { // Node ID: 454 (NodeSlice)
    const HWFloat<8,48> &id454in_a = id816out_floatOut[getCycle()%2];

    id454out_result = (slice<0,47>(id454in_a));
  }
  { // Node ID: 890 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id534out_result;

  { // Node ID: 534 (NodeNeqInlined)
    const HWRawBits<47> &id534in_a = id454out_result;
    const HWRawBits<47> &id534in_b = id890out_value;

    id534out_result = (neq_bits(id534in_a,id534in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id460out_result;

  { // Node ID: 460 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id460in_a = id533out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id460in_b = id534out_result;

    HWOffsetFix<1,0,UNSIGNED> id460x_1;

    (id460x_1) = (and_fixed(id460in_a,id460in_b));
    id460out_result = (id460x_1);
  }
  { // Node ID: 864 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id864in_input = id460out_result;

    id864out_output[(getCycle()+8)%9] = id864in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id535out_result;
  HWOffsetFix<1,0,UNSIGNED> id535out_result_doubt;

  { // Node ID: 535 (NodeSlice)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id535in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id535in_a_doubt = id43out_o_doubt[getCycle()%7];

    id535out_result = (slice<60,1>(id535in_a));
    id535out_result_doubt = id535in_a_doubt;
  }
  HWRawBits<1> id536out_result;
  HWOffsetFix<1,0,UNSIGNED> id536out_result_doubt;

  { // Node ID: 536 (NodeNot)
    const HWRawBits<1> &id536in_a = id535out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id536in_a_doubt = id535out_result_doubt;

    id536out_result = (not_bits(id536in_a));
    id536out_result_doubt = id536in_a_doubt;
  }
  HWRawBits<61> id596out_result;
  HWOffsetFix<1,0,UNSIGNED> id596out_result_doubt;

  { // Node ID: 596 (NodeCat)
    const HWRawBits<1> &id596in_in0 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in0_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in1 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in1_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in2 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in2_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in3 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in3_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in4 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in4_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in5 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in5_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in6 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in6_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in7 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in7_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in8 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in8_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in9 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in9_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in10 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in10_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in11 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in11_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in12 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in12_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in13 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in13_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in14 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in14_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in15 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in15_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in16 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in16_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in17 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in17_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in18 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in18_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in19 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in19_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in20 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in20_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in21 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in21_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in22 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in22_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in23 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in23_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in24 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in24_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in25 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in25_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in26 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in26_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in27 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in27_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in28 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in28_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in29 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in29_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in30 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in30_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in31 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in31_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in32 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in32_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in33 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in33_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in34 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in34_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in35 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in35_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in36 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in36_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in37 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in37_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in38 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in38_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in39 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in39_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in40 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in40_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in41 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in41_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in42 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in42_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in43 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in43_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in44 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in44_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in45 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in45_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in46 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in46_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in47 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in47_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in48 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in48_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in49 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in49_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in50 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in50_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in51 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in51_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in52 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in52_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in53 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in53_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in54 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in54_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in55 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in55_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in56 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in56_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in57 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in57_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in58 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in58_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in59 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in59_doubt = id536out_result_doubt;
    const HWRawBits<1> &id596in_in60 = id536out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id596in_in60_doubt = id536out_result_doubt;

    id596out_result = (cat((cat((cat((cat((cat((cat(id596in_in0,id596in_in1)),(cat(id596in_in2,id596in_in3)))),(cat((cat(id596in_in4,id596in_in5)),(cat(id596in_in6,id596in_in7)))))),(cat((cat((cat(id596in_in8,id596in_in9)),(cat(id596in_in10,id596in_in11)))),(cat((cat(id596in_in12,id596in_in13)),(cat(id596in_in14,id596in_in15)))))))),(cat((cat((cat((cat(id596in_in16,id596in_in17)),(cat(id596in_in18,id596in_in19)))),(cat((cat(id596in_in20,id596in_in21)),(cat(id596in_in22,id596in_in23)))))),(cat((cat((cat(id596in_in24,id596in_in25)),(cat(id596in_in26,id596in_in27)))),(cat((cat(id596in_in28,id596in_in29)),id596in_in30)))))))),(cat((cat((cat((cat((cat(id596in_in31,id596in_in32)),(cat(id596in_in33,id596in_in34)))),(cat((cat(id596in_in35,id596in_in36)),(cat(id596in_in37,id596in_in38)))))),(cat((cat((cat(id596in_in39,id596in_in40)),(cat(id596in_in41,id596in_in42)))),(cat((cat(id596in_in43,id596in_in44)),id596in_in45)))))),(cat((cat((cat((cat(id596in_in46,id596in_in47)),(cat(id596in_in48,id596in_in49)))),(cat((cat(id596in_in50,id596in_in51)),(cat(id596in_in52,id596in_in53)))))),(cat((cat((cat(id596in_in54,id596in_in55)),(cat(id596in_in56,id596in_in57)))),(cat((cat(id596in_in58,id596in_in59)),id596in_in60))))))))));
    id596out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id596in_in0_doubt,id596in_in1_doubt)),id596in_in2_doubt)),id596in_in3_doubt)),id596in_in4_doubt)),id596in_in5_doubt)),id596in_in6_doubt)),id596in_in7_doubt)),id596in_in8_doubt)),id596in_in9_doubt)),id596in_in10_doubt)),id596in_in11_doubt)),id596in_in12_doubt)),id596in_in13_doubt)),id596in_in14_doubt)),id596in_in15_doubt)),id596in_in16_doubt)),id596in_in17_doubt)),id596in_in18_doubt)),id596in_in19_doubt)),id596in_in20_doubt)),id596in_in21_doubt)),id596in_in22_doubt)),id596in_in23_doubt)),id596in_in24_doubt)),id596in_in25_doubt)),id596in_in26_doubt)),id596in_in27_doubt)),id596in_in28_doubt)),id596in_in29_doubt)),id596in_in30_doubt)),id596in_in31_doubt)),id596in_in32_doubt)),id596in_in33_doubt)),id596in_in34_doubt)),id596in_in35_doubt)),id596in_in36_doubt)),id596in_in37_doubt)),id596in_in38_doubt)),id596in_in39_doubt)),id596in_in40_doubt)),id596in_in41_doubt)),id596in_in42_doubt)),id596in_in43_doubt)),id596in_in44_doubt)),id596in_in45_doubt)),id596in_in46_doubt)),id596in_in47_doubt)),id596in_in48_doubt)),id596in_in49_doubt)),id596in_in50_doubt)),id596in_in51_doubt)),id596in_in52_doubt)),id596in_in53_doubt)),id596in_in54_doubt)),id596in_in55_doubt)),id596in_in56_doubt)),id596in_in57_doubt)),id596in_in58_doubt)),id596in_in59_doubt)),id596in_in60_doubt));
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id109out_output;
  HWOffsetFix<1,0,UNSIGNED> id109out_output_doubt;

  { // Node ID: 109 (NodeReinterpret)
    const HWRawBits<61> &id109in_input = id596out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id109in_input_doubt = id596out_result_doubt;

    id109out_output = (cast_bits2fixed<61,-51,TWOSCOMPLEMENT>(id109in_input));
    id109out_output_doubt = id109in_input_doubt;
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id110out_result;
  HWOffsetFix<1,0,UNSIGNED> id110out_result_doubt;

  { // Node ID: 110 (NodeXor)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id110in_a = id46out_value;
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id110in_b = id109out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id110in_b_doubt = id109out_output_doubt;

    HWOffsetFix<61,-51,TWOSCOMPLEMENT> id110x_1;

    (id110x_1) = (xor_fixed(id110in_a,id110in_b));
    id110out_result = (id110x_1);
    id110out_result_doubt = id110in_b_doubt;
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id111out_result;
  HWOffsetFix<1,0,UNSIGNED> id111out_result_doubt;

  { // Node ID: 111 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_sel = id45out_output;
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id111in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id111in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id111in_option1 = id110out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id111in_option1_doubt = id110out_result_doubt;

    HWOffsetFix<61,-51,TWOSCOMPLEMENT> id111x_1;
    HWOffsetFix<1,0,UNSIGNED> id111x_2;

    switch((id111in_sel.getValueAsLong())) {
      case 0l:
        id111x_1 = id111in_option0;
        break;
      case 1l:
        id111x_1 = id111in_option1;
        break;
      default:
        id111x_1 = (c_hw_fix_61_n51_sgn_undef);
        break;
    }
    switch((id111in_sel.getValueAsLong())) {
      case 0l:
        id111x_2 = id111in_option0_doubt;
        break;
      case 1l:
        id111x_2 = id111in_option1_doubt;
        break;
      default:
        id111x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id111out_result = (id111x_1);
    id111out_result_doubt = (id111x_2);
  }
  { // Node ID: 114 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-102,TWOSCOMPLEMENT> id113out_result;
  HWOffsetFix<1,0,UNSIGNED> id113out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id113out_exception;

  { // Node ID: 113 (NodeMul)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id113in_a = id111out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id113in_a_doubt = id111out_result_doubt;
    const HWOffsetFix<52,-51,UNSIGNED> &id113in_b = id114out_value;

    HWOffsetFix<1,0,UNSIGNED> id113x_1;

    id113out_result = (mul_fixed<113,-102,TWOSCOMPLEMENT,TONEAREVEN>(id113in_a,id113in_b,(&(id113x_1))));
    id113out_result_doubt = (or_fixed((neq_fixed((id113x_1),(c_hw_fix_1_0_uns_bits_1))),id113in_a_doubt));
    id113out_exception = (id113x_1);
  }
  { // Node ID: 116 (NodeConstantRawBits)
  }
  HWRawBits<1> id597out_result;
  HWOffsetFix<1,0,UNSIGNED> id597out_result_doubt;

  { // Node ID: 597 (NodeSlice)
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id597in_a = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id597in_a_doubt = id113out_result_doubt;

    id597out_result = (slice<112,1>(id597in_a));
    id597out_result_doubt = id597in_a_doubt;
  }
  HWRawBits<1> id598out_result;
  HWOffsetFix<1,0,UNSIGNED> id598out_result_doubt;

  { // Node ID: 598 (NodeNot)
    const HWRawBits<1> &id598in_a = id597out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id598in_a_doubt = id597out_result_doubt;

    id598out_result = (not_bits(id598in_a));
    id598out_result_doubt = id598in_a_doubt;
  }
  HWRawBits<113> id710out_result;
  HWOffsetFix<1,0,UNSIGNED> id710out_result_doubt;

  { // Node ID: 710 (NodeCat)
    const HWRawBits<1> &id710in_in0 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in0_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in1 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in1_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in2 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in2_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in3 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in3_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in4 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in4_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in5 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in5_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in6 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in6_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in7 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in7_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in8 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in8_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in9 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in9_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in10 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in10_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in11 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in11_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in12 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in12_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in13 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in13_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in14 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in14_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in15 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in15_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in16 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in16_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in17 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in17_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in18 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in18_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in19 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in19_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in20 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in20_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in21 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in21_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in22 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in22_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in23 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in23_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in24 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in24_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in25 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in25_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in26 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in26_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in27 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in27_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in28 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in28_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in29 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in29_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in30 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in30_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in31 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in31_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in32 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in32_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in33 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in33_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in34 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in34_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in35 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in35_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in36 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in36_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in37 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in37_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in38 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in38_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in39 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in39_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in40 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in40_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in41 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in41_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in42 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in42_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in43 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in43_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in44 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in44_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in45 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in45_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in46 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in46_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in47 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in47_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in48 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in48_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in49 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in49_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in50 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in50_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in51 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in51_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in52 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in52_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in53 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in53_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in54 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in54_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in55 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in55_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in56 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in56_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in57 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in57_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in58 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in58_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in59 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in59_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in60 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in60_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in61 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in61_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in62 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in62_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in63 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in63_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in64 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in64_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in65 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in65_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in66 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in66_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in67 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in67_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in68 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in68_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in69 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in69_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in70 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in70_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in71 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in71_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in72 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in72_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in73 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in73_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in74 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in74_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in75 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in75_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in76 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in76_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in77 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in77_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in78 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in78_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in79 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in79_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in80 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in80_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in81 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in81_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in82 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in82_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in83 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in83_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in84 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in84_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in85 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in85_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in86 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in86_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in87 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in87_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in88 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in88_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in89 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in89_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in90 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in90_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in91 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in91_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in92 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in92_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in93 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in93_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in94 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in94_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in95 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in95_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in96 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in96_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in97 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in97_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in98 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in98_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in99 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in99_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in100 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in100_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in101 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in101_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in102 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in102_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in103 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in103_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in104 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in104_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in105 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in105_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in106 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in106_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in107 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in107_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in108 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in108_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in109 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in109_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in110 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in110_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in111 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in111_doubt = id598out_result_doubt;
    const HWRawBits<1> &id710in_in112 = id598out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id710in_in112_doubt = id598out_result_doubt;

    id710out_result = (cat((cat((cat((cat((cat((cat((cat(id710in_in0,id710in_in1)),(cat(id710in_in2,id710in_in3)))),(cat((cat(id710in_in4,id710in_in5)),(cat(id710in_in6,id710in_in7)))))),(cat((cat((cat(id710in_in8,id710in_in9)),(cat(id710in_in10,id710in_in11)))),(cat((cat(id710in_in12,id710in_in13)),id710in_in14)))))),(cat((cat((cat((cat(id710in_in15,id710in_in16)),(cat(id710in_in17,id710in_in18)))),(cat((cat(id710in_in19,id710in_in20)),id710in_in21)))),(cat((cat((cat(id710in_in22,id710in_in23)),(cat(id710in_in24,id710in_in25)))),(cat((cat(id710in_in26,id710in_in27)),id710in_in28)))))))),(cat((cat((cat((cat((cat(id710in_in29,id710in_in30)),(cat(id710in_in31,id710in_in32)))),(cat((cat(id710in_in33,id710in_in34)),id710in_in35)))),(cat((cat((cat(id710in_in36,id710in_in37)),(cat(id710in_in38,id710in_in39)))),(cat((cat(id710in_in40,id710in_in41)),id710in_in42)))))),(cat((cat((cat((cat(id710in_in43,id710in_in44)),(cat(id710in_in45,id710in_in46)))),(cat((cat(id710in_in47,id710in_in48)),id710in_in49)))),(cat((cat((cat(id710in_in50,id710in_in51)),(cat(id710in_in52,id710in_in53)))),(cat((cat(id710in_in54,id710in_in55)),id710in_in56)))))))))),(cat((cat((cat((cat((cat((cat(id710in_in57,id710in_in58)),(cat(id710in_in59,id710in_in60)))),(cat((cat(id710in_in61,id710in_in62)),id710in_in63)))),(cat((cat((cat(id710in_in64,id710in_in65)),(cat(id710in_in66,id710in_in67)))),(cat((cat(id710in_in68,id710in_in69)),id710in_in70)))))),(cat((cat((cat((cat(id710in_in71,id710in_in72)),(cat(id710in_in73,id710in_in74)))),(cat((cat(id710in_in75,id710in_in76)),id710in_in77)))),(cat((cat((cat(id710in_in78,id710in_in79)),(cat(id710in_in80,id710in_in81)))),(cat((cat(id710in_in82,id710in_in83)),id710in_in84)))))))),(cat((cat((cat((cat((cat(id710in_in85,id710in_in86)),(cat(id710in_in87,id710in_in88)))),(cat((cat(id710in_in89,id710in_in90)),id710in_in91)))),(cat((cat((cat(id710in_in92,id710in_in93)),(cat(id710in_in94,id710in_in95)))),(cat((cat(id710in_in96,id710in_in97)),id710in_in98)))))),(cat((cat((cat((cat(id710in_in99,id710in_in100)),(cat(id710in_in101,id710in_in102)))),(cat((cat(id710in_in103,id710in_in104)),id710in_in105)))),(cat((cat((cat(id710in_in106,id710in_in107)),(cat(id710in_in108,id710in_in109)))),(cat((cat(id710in_in110,id710in_in111)),id710in_in112))))))))))));
    id710out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id710in_in0_doubt,id710in_in1_doubt)),id710in_in2_doubt)),id710in_in3_doubt)),id710in_in4_doubt)),id710in_in5_doubt)),id710in_in6_doubt)),id710in_in7_doubt)),id710in_in8_doubt)),id710in_in9_doubt)),id710in_in10_doubt)),id710in_in11_doubt)),id710in_in12_doubt)),id710in_in13_doubt)),id710in_in14_doubt)),id710in_in15_doubt)),id710in_in16_doubt)),id710in_in17_doubt)),id710in_in18_doubt)),id710in_in19_doubt)),id710in_in20_doubt)),id710in_in21_doubt)),id710in_in22_doubt)),id710in_in23_doubt)),id710in_in24_doubt)),id710in_in25_doubt)),id710in_in26_doubt)),id710in_in27_doubt)),id710in_in28_doubt)),id710in_in29_doubt)),id710in_in30_doubt)),id710in_in31_doubt)),id710in_in32_doubt)),id710in_in33_doubt)),id710in_in34_doubt)),id710in_in35_doubt)),id710in_in36_doubt)),id710in_in37_doubt)),id710in_in38_doubt)),id710in_in39_doubt)),id710in_in40_doubt)),id710in_in41_doubt)),id710in_in42_doubt)),id710in_in43_doubt)),id710in_in44_doubt)),id710in_in45_doubt)),id710in_in46_doubt)),id710in_in47_doubt)),id710in_in48_doubt)),id710in_in49_doubt)),id710in_in50_doubt)),id710in_in51_doubt)),id710in_in52_doubt)),id710in_in53_doubt)),id710in_in54_doubt)),id710in_in55_doubt)),id710in_in56_doubt)),id710in_in57_doubt)),id710in_in58_doubt)),id710in_in59_doubt)),id710in_in60_doubt)),id710in_in61_doubt)),id710in_in62_doubt)),id710in_in63_doubt)),id710in_in64_doubt)),id710in_in65_doubt)),id710in_in66_doubt)),id710in_in67_doubt)),id710in_in68_doubt)),id710in_in69_doubt)),id710in_in70_doubt)),id710in_in71_doubt)),id710in_in72_doubt)),id710in_in73_doubt)),id710in_in74_doubt)),id710in_in75_doubt)),id710in_in76_doubt)),id710in_in77_doubt)),id710in_in78_doubt)),id710in_in79_doubt)),id710in_in80_doubt)),id710in_in81_doubt)),id710in_in82_doubt)),id710in_in83_doubt)),id710in_in84_doubt)),id710in_in85_doubt)),id710in_in86_doubt)),id710in_in87_doubt)),id710in_in88_doubt)),id710in_in89_doubt)),id710in_in90_doubt)),id710in_in91_doubt)),id710in_in92_doubt)),id710in_in93_doubt)),id710in_in94_doubt)),id710in_in95_doubt)),id710in_in96_doubt)),id710in_in97_doubt)),id710in_in98_doubt)),id710in_in99_doubt)),id710in_in100_doubt)),id710in_in101_doubt)),id710in_in102_doubt)),id710in_in103_doubt)),id710in_in104_doubt)),id710in_in105_doubt)),id710in_in106_doubt)),id710in_in107_doubt)),id710in_in108_doubt)),id710in_in109_doubt)),id710in_in110_doubt)),id710in_in111_doubt)),id710in_in112_doubt));
  }
  HWOffsetFix<113,-102,TWOSCOMPLEMENT> id231out_output;
  HWOffsetFix<1,0,UNSIGNED> id231out_output_doubt;

  { // Node ID: 231 (NodeReinterpret)
    const HWRawBits<113> &id231in_input = id710out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id231in_input_doubt = id710out_result_doubt;

    id231out_output = (cast_bits2fixed<113,-102,TWOSCOMPLEMENT>(id231in_input));
    id231out_output_doubt = id231in_input_doubt;
  }
  HWOffsetFix<113,-102,TWOSCOMPLEMENT> id232out_result;
  HWOffsetFix<1,0,UNSIGNED> id232out_result_doubt;

  { // Node ID: 232 (NodeXor)
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id232in_a = id116out_value;
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id232in_b = id231out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id232in_b_doubt = id231out_output_doubt;

    HWOffsetFix<113,-102,TWOSCOMPLEMENT> id232x_1;

    (id232x_1) = (xor_fixed(id232in_a,id232in_b));
    id232out_result = (id232x_1);
    id232out_result_doubt = id232in_b_doubt;
  }
  HWOffsetFix<113,-102,TWOSCOMPLEMENT> id233out_result;
  HWOffsetFix<1,0,UNSIGNED> id233out_result_doubt;

  { // Node ID: 233 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id233in_sel = id113out_exception;
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id233in_option0 = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_option0_doubt = id113out_result_doubt;
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id233in_option1 = id232out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_option1_doubt = id232out_result_doubt;

    HWOffsetFix<113,-102,TWOSCOMPLEMENT> id233x_1;
    HWOffsetFix<1,0,UNSIGNED> id233x_2;

    switch((id233in_sel.getValueAsLong())) {
      case 0l:
        id233x_1 = id233in_option0;
        break;
      case 1l:
        id233x_1 = id233in_option1;
        break;
      default:
        id233x_1 = (c_hw_fix_113_n102_sgn_undef);
        break;
    }
    switch((id233in_sel.getValueAsLong())) {
      case 0l:
        id233x_2 = id233in_option0_doubt;
        break;
      case 1l:
        id233x_2 = id233in_option1_doubt;
        break;
      default:
        id233x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id233out_result = (id233x_1);
    id233out_result_doubt = (id233x_2);
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id234out_o;
  HWOffsetFix<1,0,UNSIGNED> id234out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id234out_exception;

  { // Node ID: 234 (NodeCast)
    const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &id234in_i = id233out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id234in_i_doubt = id233out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id234x_1;

    id234out_o = (cast_fixed2fixed<61,-51,TWOSCOMPLEMENT,TONEAREVEN>(id234in_i,(&(id234x_1))));
    id234out_o_doubt = (or_fixed((neq_fixed((id234x_1),(c_hw_fix_1_0_uns_bits_1))),id234in_i_doubt));
    id234out_exception = (id234x_1);
  }
  { // Node ID: 236 (NodeConstantRawBits)
  }
  HWRawBits<1> id711out_result;
  HWOffsetFix<1,0,UNSIGNED> id711out_result_doubt;

  { // Node ID: 711 (NodeSlice)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id711in_a = id234out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id711in_a_doubt = id234out_o_doubt;

    id711out_result = (slice<60,1>(id711in_a));
    id711out_result_doubt = id711in_a_doubt;
  }
  HWRawBits<1> id712out_result;
  HWOffsetFix<1,0,UNSIGNED> id712out_result_doubt;

  { // Node ID: 712 (NodeNot)
    const HWRawBits<1> &id712in_a = id711out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id712in_a_doubt = id711out_result_doubt;

    id712out_result = (not_bits(id712in_a));
    id712out_result_doubt = id712in_a_doubt;
  }
  HWRawBits<61> id772out_result;
  HWOffsetFix<1,0,UNSIGNED> id772out_result_doubt;

  { // Node ID: 772 (NodeCat)
    const HWRawBits<1> &id772in_in0 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in0_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in1 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in1_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in2 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in2_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in3 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in3_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in4 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in4_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in5 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in5_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in6 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in6_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in7 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in7_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in8 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in8_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in9 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in9_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in10 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in10_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in11 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in11_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in12 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in12_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in13 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in13_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in14 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in14_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in15 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in15_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in16 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in16_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in17 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in17_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in18 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in18_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in19 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in19_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in20 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in20_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in21 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in21_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in22 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in22_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in23 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in23_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in24 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in24_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in25 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in25_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in26 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in26_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in27 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in27_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in28 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in28_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in29 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in29_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in30 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in30_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in31 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in31_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in32 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in32_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in33 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in33_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in34 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in34_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in35 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in35_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in36 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in36_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in37 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in37_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in38 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in38_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in39 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in39_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in40 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in40_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in41 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in41_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in42 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in42_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in43 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in43_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in44 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in44_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in45 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in45_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in46 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in46_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in47 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in47_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in48 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in48_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in49 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in49_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in50 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in50_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in51 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in51_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in52 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in52_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in53 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in53_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in54 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in54_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in55 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in55_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in56 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in56_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in57 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in57_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in58 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in58_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in59 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in59_doubt = id712out_result_doubt;
    const HWRawBits<1> &id772in_in60 = id712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_in60_doubt = id712out_result_doubt;

    id772out_result = (cat((cat((cat((cat((cat((cat(id772in_in0,id772in_in1)),(cat(id772in_in2,id772in_in3)))),(cat((cat(id772in_in4,id772in_in5)),(cat(id772in_in6,id772in_in7)))))),(cat((cat((cat(id772in_in8,id772in_in9)),(cat(id772in_in10,id772in_in11)))),(cat((cat(id772in_in12,id772in_in13)),(cat(id772in_in14,id772in_in15)))))))),(cat((cat((cat((cat(id772in_in16,id772in_in17)),(cat(id772in_in18,id772in_in19)))),(cat((cat(id772in_in20,id772in_in21)),(cat(id772in_in22,id772in_in23)))))),(cat((cat((cat(id772in_in24,id772in_in25)),(cat(id772in_in26,id772in_in27)))),(cat((cat(id772in_in28,id772in_in29)),id772in_in30)))))))),(cat((cat((cat((cat((cat(id772in_in31,id772in_in32)),(cat(id772in_in33,id772in_in34)))),(cat((cat(id772in_in35,id772in_in36)),(cat(id772in_in37,id772in_in38)))))),(cat((cat((cat(id772in_in39,id772in_in40)),(cat(id772in_in41,id772in_in42)))),(cat((cat(id772in_in43,id772in_in44)),id772in_in45)))))),(cat((cat((cat((cat(id772in_in46,id772in_in47)),(cat(id772in_in48,id772in_in49)))),(cat((cat(id772in_in50,id772in_in51)),(cat(id772in_in52,id772in_in53)))))),(cat((cat((cat(id772in_in54,id772in_in55)),(cat(id772in_in56,id772in_in57)))),(cat((cat(id772in_in58,id772in_in59)),id772in_in60))))))))));
    id772out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id772in_in0_doubt,id772in_in1_doubt)),id772in_in2_doubt)),id772in_in3_doubt)),id772in_in4_doubt)),id772in_in5_doubt)),id772in_in6_doubt)),id772in_in7_doubt)),id772in_in8_doubt)),id772in_in9_doubt)),id772in_in10_doubt)),id772in_in11_doubt)),id772in_in12_doubt)),id772in_in13_doubt)),id772in_in14_doubt)),id772in_in15_doubt)),id772in_in16_doubt)),id772in_in17_doubt)),id772in_in18_doubt)),id772in_in19_doubt)),id772in_in20_doubt)),id772in_in21_doubt)),id772in_in22_doubt)),id772in_in23_doubt)),id772in_in24_doubt)),id772in_in25_doubt)),id772in_in26_doubt)),id772in_in27_doubt)),id772in_in28_doubt)),id772in_in29_doubt)),id772in_in30_doubt)),id772in_in31_doubt)),id772in_in32_doubt)),id772in_in33_doubt)),id772in_in34_doubt)),id772in_in35_doubt)),id772in_in36_doubt)),id772in_in37_doubt)),id772in_in38_doubt)),id772in_in39_doubt)),id772in_in40_doubt)),id772in_in41_doubt)),id772in_in42_doubt)),id772in_in43_doubt)),id772in_in44_doubt)),id772in_in45_doubt)),id772in_in46_doubt)),id772in_in47_doubt)),id772in_in48_doubt)),id772in_in49_doubt)),id772in_in50_doubt)),id772in_in51_doubt)),id772in_in52_doubt)),id772in_in53_doubt)),id772in_in54_doubt)),id772in_in55_doubt)),id772in_in56_doubt)),id772in_in57_doubt)),id772in_in58_doubt)),id772in_in59_doubt)),id772in_in60_doubt));
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id299out_output;
  HWOffsetFix<1,0,UNSIGNED> id299out_output_doubt;

  { // Node ID: 299 (NodeReinterpret)
    const HWRawBits<61> &id299in_input = id772out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id299in_input_doubt = id772out_result_doubt;

    id299out_output = (cast_bits2fixed<61,-51,TWOSCOMPLEMENT>(id299in_input));
    id299out_output_doubt = id299in_input_doubt;
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id300out_result;
  HWOffsetFix<1,0,UNSIGNED> id300out_result_doubt;

  { // Node ID: 300 (NodeXor)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id300in_a = id236out_value;
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id300in_b = id299out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id300in_b_doubt = id299out_output_doubt;

    HWOffsetFix<61,-51,TWOSCOMPLEMENT> id300x_1;

    (id300x_1) = (xor_fixed(id300in_a,id300in_b));
    id300out_result = (id300x_1);
    id300out_result_doubt = id300in_b_doubt;
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id301out_result;
  HWOffsetFix<1,0,UNSIGNED> id301out_result_doubt;

  { // Node ID: 301 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id301in_sel = id234out_exception;
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id301in_option0 = id234out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id301in_option0_doubt = id234out_o_doubt;
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id301in_option1 = id300out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id301in_option1_doubt = id300out_result_doubt;

    HWOffsetFix<61,-51,TWOSCOMPLEMENT> id301x_1;
    HWOffsetFix<1,0,UNSIGNED> id301x_2;

    switch((id301in_sel.getValueAsLong())) {
      case 0l:
        id301x_1 = id301in_option0;
        break;
      case 1l:
        id301x_1 = id301in_option1;
        break;
      default:
        id301x_1 = (c_hw_fix_61_n51_sgn_undef);
        break;
    }
    switch((id301in_sel.getValueAsLong())) {
      case 0l:
        id301x_2 = id301in_option0_doubt;
        break;
      case 1l:
        id301x_2 = id301in_option1_doubt;
        break;
      default:
        id301x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id301out_result = (id301x_1);
    id301out_result_doubt = (id301x_2);
  }
  HWOffsetFix<61,-51,TWOSCOMPLEMENT> id310out_output;

  { // Node ID: 310 (NodeDoubtBitOp)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id310in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_input_doubt = id301out_result_doubt;

    id310out_output = id310in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id311out_o;

  { // Node ID: 311 (NodeCast)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id311in_i = id310out_output;

    id311out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id311in_i));
  }
  { // Node ID: 856 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id856in_input = id311out_o;

    id856out_output[(getCycle()+2)%3] = id856in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id374out_o;

  { // Node ID: 374 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id374in_i = id856out_output[getCycle()%3];

    id374out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id374in_i));
  }
  { // Node ID: 889 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id376out_result;
  HWOffsetFix<1,0,UNSIGNED> id376out_exception;

  { // Node ID: 376 (NodeAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id376in_a = id374out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id376in_b = id889out_value;

    HWOffsetFix<1,0,UNSIGNED> id376x_1;

    id376out_result = (add_fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id376in_a,id376in_b,(&(id376x_1))));
    id376out_exception = (id376x_1);
  }
  { // Node ID: 378 (NodeConstantRawBits)
  }
  HWRawBits<1> id773out_result;

  { // Node ID: 773 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id773in_a = id376out_result;

    id773out_result = (slice<10,1>(id773in_a));
  }
  HWRawBits<1> id774out_result;

  { // Node ID: 774 (NodeNot)
    const HWRawBits<1> &id774in_a = id773out_result;

    id774out_result = (not_bits(id774in_a));
  }
  HWRawBits<11> id784out_result;

  { // Node ID: 784 (NodeCat)
    const HWRawBits<1> &id784in_in0 = id774out_result;
    const HWRawBits<1> &id784in_in1 = id774out_result;
    const HWRawBits<1> &id784in_in2 = id774out_result;
    const HWRawBits<1> &id784in_in3 = id774out_result;
    const HWRawBits<1> &id784in_in4 = id774out_result;
    const HWRawBits<1> &id784in_in5 = id774out_result;
    const HWRawBits<1> &id784in_in6 = id774out_result;
    const HWRawBits<1> &id784in_in7 = id774out_result;
    const HWRawBits<1> &id784in_in8 = id774out_result;
    const HWRawBits<1> &id784in_in9 = id774out_result;
    const HWRawBits<1> &id784in_in10 = id774out_result;

    id784out_result = (cat((cat((cat((cat(id784in_in0,id784in_in1)),id784in_in2)),(cat((cat(id784in_in3,id784in_in4)),id784in_in5)))),(cat((cat((cat(id784in_in6,id784in_in7)),id784in_in8)),(cat(id784in_in9,id784in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id391out_output;

  { // Node ID: 391 (NodeReinterpret)
    const HWRawBits<11> &id391in_input = id784out_result;

    id391out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id391in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id392out_result;

  { // Node ID: 392 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id392in_a = id378out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id392in_b = id391out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id392x_1;

    (id392x_1) = (xor_fixed(id392in_a,id392in_b));
    id392out_result = (id392x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id393out_result;

  { // Node ID: 393 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id393in_sel = id376out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id393in_option0 = id376out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id393in_option1 = id392out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id393x_1;

    switch((id393in_sel.getValueAsLong())) {
      case 0l:
        id393x_1 = id393in_option0;
        break;
      case 1l:
        id393x_1 = id393in_option1;
        break;
      default:
        id393x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id393out_result = (id393x_1);
  }
  { // Node ID: 394 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id314out_o;

  { // Node ID: 314 (NodeCast)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id314in_i = id310out_output;

    id314out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id314in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id470out_output;

  { // Node ID: 470 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id470in_input = id314out_o;

    id470out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id470in_input))));
  }
  { // Node ID: 471 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id471in_addr = id470out_output;

    HWOffsetFix<33,-48,UNSIGNED> id471x_1;

    switch(((long)((id471in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id471x_1 = (c_hw_fix_33_n48_uns_undef);
        break;
      case 1l:
        id471x_1 = (id471sta_rom_store[(id471in_addr.getValueAsLong())]);
        break;
      default:
        id471x_1 = (c_hw_fix_33_n48_uns_undef);
        break;
    }
    id471out_dout[(getCycle()+2)%3] = (id471x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id313out_o;

  { // Node ID: 313 (NodeCast)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id313in_i = id310out_output;

    id313out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id313in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id467out_output;

  { // Node ID: 467 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id467in_input = id313out_o;

    id467out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id467in_input))));
  }
  { // Node ID: 468 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id468in_addr = id467out_output;

    HWOffsetFix<43,-48,UNSIGNED> id468x_1;

    switch(((long)((id468in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id468x_1 = (c_hw_fix_43_n48_uns_undef);
        break;
      case 1l:
        id468x_1 = (id468sta_rom_store[(id468in_addr.getValueAsLong())]);
        break;
      default:
        id468x_1 = (c_hw_fix_43_n48_uns_undef);
        break;
    }
    id468out_dout[(getCycle()+2)%3] = (id468x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id312out_o;

  { // Node ID: 312 (NodeCast)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id312in_i = id310out_output;

    id312out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id312in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id464out_output;

  { // Node ID: 464 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id464in_input = id312out_o;

    id464out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id464in_input))));
  }
  { // Node ID: 465 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id465in_addr = id464out_output;

    HWOffsetFix<48,-48,UNSIGNED> id465x_1;

    switch(((long)((id465in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id465x_1 = (c_hw_fix_48_n48_uns_undef);
        break;
      case 1l:
        id465x_1 = (id465sta_rom_store[(id465in_addr.getValueAsLong())]);
        break;
      default:
        id465x_1 = (c_hw_fix_48_n48_uns_undef);
        break;
    }
    id465out_dout[(getCycle()+2)%3] = (id465x_1);
  }
  { // Node ID: 318 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id315out_o;

  { // Node ID: 315 (NodeCast)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id315in_i = id310out_output;

    id315out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id315in_i));
  }
  { // Node ID: 857 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id857in_input = id315out_o;

    id857out_output[(getCycle()+2)%3] = id857in_input;
  }
  HWOffsetFix<47,-72,UNSIGNED> id317out_result;

  { // Node ID: 317 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id317in_a = id318out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id317in_b = id857out_output[getCycle()%3];

    id317out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id317in_a,id317in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id319out_o;
  HWOffsetFix<1,0,UNSIGNED> id319out_exception;

  { // Node ID: 319 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id319in_i = id317out_result;

    HWOffsetFix<1,0,UNSIGNED> id319x_1;

    id319out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id319in_i,(&(id319x_1))));
    id319out_exception = (id319x_1);
  }
  { // Node ID: 321 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id322out_result;

  { // Node ID: 322 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id322in_sel = id319out_exception;
    const HWOffsetFix<26,-51,UNSIGNED> &id322in_option0 = id319out_o;
    const HWOffsetFix<26,-51,UNSIGNED> &id322in_option1 = id321out_value;

    HWOffsetFix<26,-51,UNSIGNED> id322x_1;

    switch((id322in_sel.getValueAsLong())) {
      case 0l:
        id322x_1 = id322in_option0;
        break;
      case 1l:
        id322x_1 = id322in_option1;
        break;
      default:
        id322x_1 = (c_hw_fix_26_n51_uns_undef);
        break;
    }
    id322out_result = (id322x_1);
  }
  HWOffsetFix<52,-51,UNSIGNED> id323out_result;

  { // Node ID: 323 (NodeAdd)
    const HWOffsetFix<48,-48,UNSIGNED> &id323in_a = id465out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id323in_b = id322out_result;

    id323out_result = (add_fixed<52,-51,UNSIGNED,TONEAREVEN>(id323in_a,id323in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id324out_result;
  HWOffsetFix<1,0,UNSIGNED> id324out_exception;

  { // Node ID: 324 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id324in_a = id322out_result;
    const HWOffsetFix<48,-48,UNSIGNED> &id324in_b = id465out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id324x_1;

    id324out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id324in_a,id324in_b,(&(id324x_1))));
    id324out_exception = (id324x_1);
  }
  { // Node ID: 326 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-89,UNSIGNED> id327out_result;

  { // Node ID: 327 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id327in_sel = id324out_exception;
    const HWOffsetFix<64,-89,UNSIGNED> &id327in_option0 = id324out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id327in_option1 = id326out_value;

    HWOffsetFix<64,-89,UNSIGNED> id327x_1;

    switch((id327in_sel.getValueAsLong())) {
      case 0l:
        id327x_1 = id327in_option0;
        break;
      case 1l:
        id327x_1 = id327in_option1;
        break;
      default:
        id327x_1 = (c_hw_fix_64_n89_uns_undef);
        break;
    }
    id327out_result = (id327x_1);
  }
  HWOffsetFix<64,-63,UNSIGNED> id328out_result;
  HWOffsetFix<1,0,UNSIGNED> id328out_exception;

  { // Node ID: 328 (NodeAdd)
    const HWOffsetFix<52,-51,UNSIGNED> &id328in_a = id323out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id328in_b = id327out_result;

    HWOffsetFix<1,0,UNSIGNED> id328x_1;

    id328out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id328in_a,id328in_b,(&(id328x_1))));
    id328out_exception = (id328x_1);
  }
  { // Node ID: 330 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-63,UNSIGNED> id331out_result;

  { // Node ID: 331 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_sel = id328out_exception;
    const HWOffsetFix<64,-63,UNSIGNED> &id331in_option0 = id328out_result;
    const HWOffsetFix<64,-63,UNSIGNED> &id331in_option1 = id330out_value;

    HWOffsetFix<64,-63,UNSIGNED> id331x_1;

    switch((id331in_sel.getValueAsLong())) {
      case 0l:
        id331x_1 = id331in_option0;
        break;
      case 1l:
        id331x_1 = id331in_option1;
        break;
      default:
        id331x_1 = (c_hw_fix_64_n63_uns_undef);
        break;
    }
    id331out_result = (id331x_1);
  }
  HWOffsetFix<52,-51,UNSIGNED> id332out_o;
  HWOffsetFix<1,0,UNSIGNED> id332out_exception;

  { // Node ID: 332 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id332in_i = id331out_result;

    HWOffsetFix<1,0,UNSIGNED> id332x_1;

    id332out_o = (cast_fixed2fixed<52,-51,UNSIGNED,TONEAREVEN>(id332in_i,(&(id332x_1))));
    id332out_exception = (id332x_1);
  }
  { // Node ID: 334 (NodeConstantRawBits)
  }
  HWOffsetFix<52,-51,UNSIGNED> id335out_result;

  { // Node ID: 335 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id335in_sel = id332out_exception;
    const HWOffsetFix<52,-51,UNSIGNED> &id335in_option0 = id332out_o;
    const HWOffsetFix<52,-51,UNSIGNED> &id335in_option1 = id334out_value;

    HWOffsetFix<52,-51,UNSIGNED> id335x_1;

    switch((id335in_sel.getValueAsLong())) {
      case 0l:
        id335x_1 = id335in_option0;
        break;
      case 1l:
        id335x_1 = id335in_option1;
        break;
      default:
        id335x_1 = (c_hw_fix_52_n51_uns_undef);
        break;
    }
    id335out_result = (id335x_1);
  }
  HWOffsetFix<53,-51,UNSIGNED> id336out_result;

  { // Node ID: 336 (NodeAdd)
    const HWOffsetFix<43,-48,UNSIGNED> &id336in_a = id468out_dout[getCycle()%3];
    const HWOffsetFix<52,-51,UNSIGNED> &id336in_b = id335out_result;

    id336out_result = (add_fixed<53,-51,UNSIGNED,TONEAREVEN>(id336in_a,id336in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id337out_result;
  HWOffsetFix<1,0,UNSIGNED> id337out_exception;

  { // Node ID: 337 (NodeMul)
    const HWOffsetFix<52,-51,UNSIGNED> &id337in_a = id335out_result;
    const HWOffsetFix<43,-48,UNSIGNED> &id337in_b = id468out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id337x_1;

    id337out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id337in_a,id337in_b,(&(id337x_1))));
    id337out_exception = (id337x_1);
  }
  { // Node ID: 339 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-68,UNSIGNED> id340out_result;

  { // Node ID: 340 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id340in_sel = id337out_exception;
    const HWOffsetFix<64,-68,UNSIGNED> &id340in_option0 = id337out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id340in_option1 = id339out_value;

    HWOffsetFix<64,-68,UNSIGNED> id340x_1;

    switch((id340in_sel.getValueAsLong())) {
      case 0l:
        id340x_1 = id340in_option0;
        break;
      case 1l:
        id340x_1 = id340in_option1;
        break;
      default:
        id340x_1 = (c_hw_fix_64_n68_uns_undef);
        break;
    }
    id340out_result = (id340x_1);
  }
  HWOffsetFix<64,-62,UNSIGNED> id341out_result;
  HWOffsetFix<1,0,UNSIGNED> id341out_exception;

  { // Node ID: 341 (NodeAdd)
    const HWOffsetFix<53,-51,UNSIGNED> &id341in_a = id336out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id341in_b = id340out_result;

    HWOffsetFix<1,0,UNSIGNED> id341x_1;

    id341out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id341in_a,id341in_b,(&(id341x_1))));
    id341out_exception = (id341x_1);
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-62,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id341out_exception;
    const HWOffsetFix<64,-62,UNSIGNED> &id344in_option0 = id341out_result;
    const HWOffsetFix<64,-62,UNSIGNED> &id344in_option1 = id343out_value;

    HWOffsetFix<64,-62,UNSIGNED> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_fix_64_n62_uns_undef);
        break;
    }
    id344out_result = (id344x_1);
  }
  HWOffsetFix<53,-51,UNSIGNED> id345out_o;
  HWOffsetFix<1,0,UNSIGNED> id345out_exception;

  { // Node ID: 345 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id345in_i = id344out_result;

    HWOffsetFix<1,0,UNSIGNED> id345x_1;

    id345out_o = (cast_fixed2fixed<53,-51,UNSIGNED,TONEAREVEN>(id345in_i,(&(id345x_1))));
    id345out_exception = (id345x_1);
  }
  { // Node ID: 347 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-51,UNSIGNED> id348out_result;

  { // Node ID: 348 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id348in_sel = id345out_exception;
    const HWOffsetFix<53,-51,UNSIGNED> &id348in_option0 = id345out_o;
    const HWOffsetFix<53,-51,UNSIGNED> &id348in_option1 = id347out_value;

    HWOffsetFix<53,-51,UNSIGNED> id348x_1;

    switch((id348in_sel.getValueAsLong())) {
      case 0l:
        id348x_1 = id348in_option0;
        break;
      case 1l:
        id348x_1 = id348in_option1;
        break;
      default:
        id348x_1 = (c_hw_fix_53_n51_uns_undef);
        break;
    }
    id348out_result = (id348x_1);
  }
  HWOffsetFix<54,-51,UNSIGNED> id349out_result;

  { // Node ID: 349 (NodeAdd)
    const HWOffsetFix<33,-48,UNSIGNED> &id349in_a = id471out_dout[getCycle()%3];
    const HWOffsetFix<53,-51,UNSIGNED> &id349in_b = id348out_result;

    id349out_result = (add_fixed<54,-51,UNSIGNED,TONEAREVEN>(id349in_a,id349in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id350out_result;
  HWOffsetFix<1,0,UNSIGNED> id350out_exception;

  { // Node ID: 350 (NodeMul)
    const HWOffsetFix<53,-51,UNSIGNED> &id350in_a = id348out_result;
    const HWOffsetFix<33,-48,UNSIGNED> &id350in_b = id471out_dout[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id350x_1;

    id350out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id350in_a,id350in_b,(&(id350x_1))));
    id350out_exception = (id350x_1);
  }
  { // Node ID: 352 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-77,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id353in_sel = id350out_exception;
    const HWOffsetFix<64,-77,UNSIGNED> &id353in_option0 = id350out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id353in_option1 = id352out_value;

    HWOffsetFix<64,-77,UNSIGNED> id353x_1;

    switch((id353in_sel.getValueAsLong())) {
      case 0l:
        id353x_1 = id353in_option0;
        break;
      case 1l:
        id353x_1 = id353in_option1;
        break;
      default:
        id353x_1 = (c_hw_fix_64_n77_uns_undef);
        break;
    }
    id353out_result = (id353x_1);
  }
  HWOffsetFix<64,-61,UNSIGNED> id354out_result;
  HWOffsetFix<1,0,UNSIGNED> id354out_exception;

  { // Node ID: 354 (NodeAdd)
    const HWOffsetFix<54,-51,UNSIGNED> &id354in_a = id349out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id354in_b = id353out_result;

    HWOffsetFix<1,0,UNSIGNED> id354x_1;

    id354out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id354in_a,id354in_b,(&(id354x_1))));
    id354out_exception = (id354x_1);
  }
  { // Node ID: 356 (NodeConstantRawBits)
  }
  HWOffsetFix<64,-61,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id357in_sel = id354out_exception;
    const HWOffsetFix<64,-61,UNSIGNED> &id357in_option0 = id354out_result;
    const HWOffsetFix<64,-61,UNSIGNED> &id357in_option1 = id356out_value;

    HWOffsetFix<64,-61,UNSIGNED> id357x_1;

    switch((id357in_sel.getValueAsLong())) {
      case 0l:
        id357x_1 = id357in_option0;
        break;
      case 1l:
        id357x_1 = id357in_option1;
        break;
      default:
        id357x_1 = (c_hw_fix_64_n61_uns_undef);
        break;
    }
    id357out_result = (id357x_1);
  }
  HWOffsetFix<54,-51,UNSIGNED> id358out_o;
  HWOffsetFix<1,0,UNSIGNED> id358out_exception;

  { // Node ID: 358 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id358in_i = id357out_result;

    HWOffsetFix<1,0,UNSIGNED> id358x_1;

    id358out_o = (cast_fixed2fixed<54,-51,UNSIGNED,TONEAREVEN>(id358in_i,(&(id358x_1))));
    id358out_exception = (id358x_1);
  }
  { // Node ID: 360 (NodeConstantRawBits)
  }
  HWOffsetFix<54,-51,UNSIGNED> id361out_result;

  { // Node ID: 361 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id361in_sel = id358out_exception;
    const HWOffsetFix<54,-51,UNSIGNED> &id361in_option0 = id358out_o;
    const HWOffsetFix<54,-51,UNSIGNED> &id361in_option1 = id360out_value;

    HWOffsetFix<54,-51,UNSIGNED> id361x_1;

    switch((id361in_sel.getValueAsLong())) {
      case 0l:
        id361x_1 = id361in_option0;
        break;
      case 1l:
        id361x_1 = id361in_option1;
        break;
      default:
        id361x_1 = (c_hw_fix_54_n51_uns_undef);
        break;
    }
    id361out_result = (id361x_1);
  }
  HWOffsetFix<48,-47,UNSIGNED> id362out_o;
  HWOffsetFix<1,0,UNSIGNED> id362out_exception;

  { // Node ID: 362 (NodeCast)
    const HWOffsetFix<54,-51,UNSIGNED> &id362in_i = id361out_result;

    HWOffsetFix<1,0,UNSIGNED> id362x_1;

    id362out_o = (cast_fixed2fixed<48,-47,UNSIGNED,TONEAREVEN>(id362in_i,(&(id362x_1))));
    id362out_exception = (id362x_1);
  }
  { // Node ID: 364 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-47,UNSIGNED> id365out_result;

  { // Node ID: 365 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id365in_sel = id362out_exception;
    const HWOffsetFix<48,-47,UNSIGNED> &id365in_option0 = id362out_o;
    const HWOffsetFix<48,-47,UNSIGNED> &id365in_option1 = id364out_value;

    HWOffsetFix<48,-47,UNSIGNED> id365x_1;

    switch((id365in_sel.getValueAsLong())) {
      case 0l:
        id365x_1 = id365in_option0;
        break;
      case 1l:
        id365x_1 = id365in_option1;
        break;
      default:
        id365x_1 = (c_hw_fix_48_n47_uns_undef);
        break;
    }
    id365out_result = (id365x_1);
  }
  { // Node ID: 888 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id785out_result;

  { // Node ID: 785 (NodeGteInlined)
    const HWOffsetFix<48,-47,UNSIGNED> &id785in_a = id365out_result;
    const HWOffsetFix<48,-47,UNSIGNED> &id785in_b = id888out_value;

    id785out_result = (gte_fixed(id785in_a,id785in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id815out_result;
  HWOffsetFix<1,0,UNSIGNED> id815out_exception;

  { // Node ID: 815 (NodeCondAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id815in_a = id393out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id815in_b = id394out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id815in_condb = id785out_result;

    HWOffsetFix<1,0,UNSIGNED> id815x_1;
    HWOffsetFix<1,0,UNSIGNED> id815x_2;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id815x_3;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id815x_4;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id815x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id815x_3 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id815x_3 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id815in_a));
        break;
      default:
        id815x_3 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    switch((id815in_condb.getValueAsLong())) {
      case 0l:
        id815x_4 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id815x_4 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id815in_b));
        break;
      default:
        id815x_4 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    (id815x_5) = (add_fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>((id815x_3),(id815x_4),(&(id815x_1))));
    id815out_result = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id815x_5),(&(id815x_2))));
    id815out_exception = (or_fixed((id815x_1),(id815x_2)));
  }
  { // Node ID: 399 (NodeConstantRawBits)
  }
  HWRawBits<1> id786out_result;

  { // Node ID: 786 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id786in_a = id815out_result;

    id786out_result = (slice<10,1>(id786in_a));
  }
  HWRawBits<1> id787out_result;

  { // Node ID: 787 (NodeNot)
    const HWRawBits<1> &id787in_a = id786out_result;

    id787out_result = (not_bits(id787in_a));
  }
  HWRawBits<11> id797out_result;

  { // Node ID: 797 (NodeCat)
    const HWRawBits<1> &id797in_in0 = id787out_result;
    const HWRawBits<1> &id797in_in1 = id787out_result;
    const HWRawBits<1> &id797in_in2 = id787out_result;
    const HWRawBits<1> &id797in_in3 = id787out_result;
    const HWRawBits<1> &id797in_in4 = id787out_result;
    const HWRawBits<1> &id797in_in5 = id787out_result;
    const HWRawBits<1> &id797in_in6 = id787out_result;
    const HWRawBits<1> &id797in_in7 = id787out_result;
    const HWRawBits<1> &id797in_in8 = id787out_result;
    const HWRawBits<1> &id797in_in9 = id787out_result;
    const HWRawBits<1> &id797in_in10 = id787out_result;

    id797out_result = (cat((cat((cat((cat(id797in_in0,id797in_in1)),id797in_in2)),(cat((cat(id797in_in3,id797in_in4)),id797in_in5)))),(cat((cat((cat(id797in_in6,id797in_in7)),id797in_in8)),(cat(id797in_in9,id797in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id412out_output;

  { // Node ID: 412 (NodeReinterpret)
    const HWRawBits<11> &id412in_input = id797out_result;

    id412out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id412in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id413out_result;

  { // Node ID: 413 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id413in_a = id399out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id413in_b = id412out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id413x_1;

    (id413x_1) = (xor_fixed(id413in_a,id413in_b));
    id413out_result = (id413x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id414out_result;

  { // Node ID: 414 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id414in_sel = id815out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id414in_option0 = id815out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id414in_option1 = id413out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id414x_1;

    switch((id414in_sel.getValueAsLong())) {
      case 0l:
        id414x_1 = id414in_option0;
        break;
      case 1l:
        id414x_1 = id414in_option1;
        break;
      default:
        id414x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id414out_result = (id414x_1);
  }
  HWRawBits<1> id798out_result;

  { // Node ID: 798 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id798in_a = id414out_result;

    id798out_result = (slice<10,1>(id798in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id799out_output;

  { // Node ID: 799 (NodeReinterpret)
    const HWRawBits<1> &id799in_input = id798out_result;

    id799out_output = (cast_bits2fixed<1,0,UNSIGNED>(id799in_input));
  }
  { // Node ID: 887 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id303out_result;

  { // Node ID: 303 (NodeGt)
    const HWFloat<8,48> &id303in_a = id816out_floatOut[getCycle()%2];
    const HWFloat<8,48> &id303in_b = id887out_value;

    id303out_result = (gt_float(id303in_a,id303in_b));
  }
  { // Node ID: 858 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_input = id303out_result;

    id858out_output[(getCycle()+6)%7] = id858in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id304out_output;

  { // Node ID: 304 (NodeDoubtBitOp)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id304in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id304in_input_doubt = id301out_result_doubt;

    id304out_output = id304in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id305out_result;

  { // Node ID: 305 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id305in_a = id858out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id305in_b = id304out_output;

    HWOffsetFix<1,0,UNSIGNED> id305x_1;

    (id305x_1) = (and_fixed(id305in_a,id305in_b));
    id305out_result = (id305x_1);
  }
  { // Node ID: 859 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id859in_input = id305out_result;

    id859out_output[(getCycle()+2)%3] = id859in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id417out_result;

  { // Node ID: 417 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id417in_a = id859out_output[getCycle()%3];

    id417out_result = (not_fixed(id417in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id418out_result;

  { // Node ID: 418 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id418in_a = id799out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id418in_b = id417out_result;

    HWOffsetFix<1,0,UNSIGNED> id418x_1;

    (id418x_1) = (and_fixed(id418in_a,id418in_b));
    id418out_result = (id418x_1);
  }
  { // Node ID: 886 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id307out_result;

  { // Node ID: 307 (NodeLt)
    const HWFloat<8,48> &id307in_a = id816out_floatOut[getCycle()%2];
    const HWFloat<8,48> &id307in_b = id886out_value;

    id307out_result = (lt_float(id307in_a,id307in_b));
  }
  { // Node ID: 860 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id860in_input = id307out_result;

    id860out_output[(getCycle()+6)%7] = id860in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id308out_output;

  { // Node ID: 308 (NodeDoubtBitOp)
    const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &id308in_input = id301out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id308in_input_doubt = id301out_result_doubt;

    id308out_output = id308in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id309out_result;

  { // Node ID: 309 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id309in_a = id860out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id309in_b = id308out_output;

    HWOffsetFix<1,0,UNSIGNED> id309x_1;

    (id309x_1) = (and_fixed(id309in_a,id309in_b));
    id309out_result = (id309x_1);
  }
  { // Node ID: 861 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id861in_input = id309out_result;

    id861out_output[(getCycle()+2)%3] = id861in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id419out_result;

  { // Node ID: 419 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id419in_a = id418out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id419in_b = id861out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id419x_1;

    (id419x_1) = (or_fixed(id419in_a,id419in_b));
    id419out_result = (id419x_1);
  }
  { // Node ID: 885 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id800out_result;

  { // Node ID: 800 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id800in_a = id414out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id800in_b = id885out_value;

    id800out_result = (gte_fixed(id800in_a,id800in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id440out_result;

  { // Node ID: 440 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id440in_a = id861out_output[getCycle()%3];

    id440out_result = (not_fixed(id440in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id441out_result;

  { // Node ID: 441 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id441in_a = id800out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id441in_b = id440out_result;

    HWOffsetFix<1,0,UNSIGNED> id441x_1;

    (id441x_1) = (and_fixed(id441in_a,id441in_b));
    id441out_result = (id441x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id442out_result;

  { // Node ID: 442 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id442in_a = id441out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id442in_b = id859out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id442x_1;

    (id442x_1) = (or_fixed(id442in_a,id442in_b));
    id442out_result = (id442x_1);
  }
  HWRawBits<2> id443out_result;

  { // Node ID: 443 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_in0 = id419out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id443in_in1 = id442out_result;

    id443out_result = (cat(id443in_in0,id443in_in1));
  }
  { // Node ID: 435 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id420out_o;
  HWOffsetFix<1,0,UNSIGNED> id420out_exception;

  { // Node ID: 420 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id420in_i = id414out_result;

    HWOffsetFix<1,0,UNSIGNED> id420x_1;

    id420out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id420in_i,(&(id420x_1))));
    id420out_exception = (id420x_1);
  }
  { // Node ID: 422 (NodeConstantRawBits)
  }
  HWRawBits<1> id801out_result;

  { // Node ID: 801 (NodeSlice)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id801in_a = id420out_o;

    id801out_result = (slice<7,1>(id801in_a));
  }
  HWRawBits<1> id802out_result;

  { // Node ID: 802 (NodeNot)
    const HWRawBits<1> &id802in_a = id801out_result;

    id802out_result = (not_bits(id802in_a));
  }
  HWRawBits<8> id809out_result;

  { // Node ID: 809 (NodeCat)
    const HWRawBits<1> &id809in_in0 = id802out_result;
    const HWRawBits<1> &id809in_in1 = id802out_result;
    const HWRawBits<1> &id809in_in2 = id802out_result;
    const HWRawBits<1> &id809in_in3 = id802out_result;
    const HWRawBits<1> &id809in_in4 = id802out_result;
    const HWRawBits<1> &id809in_in5 = id802out_result;
    const HWRawBits<1> &id809in_in6 = id802out_result;
    const HWRawBits<1> &id809in_in7 = id802out_result;

    id809out_result = (cat((cat((cat(id809in_in0,id809in_in1)),(cat(id809in_in2,id809in_in3)))),(cat((cat(id809in_in4,id809in_in5)),(cat(id809in_in6,id809in_in7))))));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id432out_output;

  { // Node ID: 432 (NodeReinterpret)
    const HWRawBits<8> &id432in_input = id809out_result;

    id432out_output = (cast_bits2fixed<8,0,TWOSCOMPLEMENT>(id432in_input));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id433out_result;

  { // Node ID: 433 (NodeXor)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id433in_a = id422out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id433in_b = id432out_output;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id433x_1;

    (id433x_1) = (xor_fixed(id433in_a,id433in_b));
    id433out_result = (id433x_1);
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id434out_result;

  { // Node ID: 434 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id434in_sel = id420out_exception;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id434in_option0 = id420out_o;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id434in_option1 = id433out_result;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id434x_1;

    switch((id434in_sel.getValueAsLong())) {
      case 0l:
        id434x_1 = id434in_option0;
        break;
      case 1l:
        id434x_1 = id434in_option1;
        break;
      default:
        id434x_1 = (c_hw_fix_8_0_sgn_undef);
        break;
    }
    id434out_result = (id434x_1);
  }
  { // Node ID: 368 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-47,UNSIGNED> id369out_result;

  { // Node ID: 369 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id369in_sel = id785out_result;
    const HWOffsetFix<48,-47,UNSIGNED> &id369in_option0 = id365out_result;
    const HWOffsetFix<48,-47,UNSIGNED> &id369in_option1 = id368out_value;

    HWOffsetFix<48,-47,UNSIGNED> id369x_1;

    switch((id369in_sel.getValueAsLong())) {
      case 0l:
        id369x_1 = id369in_option0;
        break;
      case 1l:
        id369x_1 = id369in_option1;
        break;
      default:
        id369x_1 = (c_hw_fix_48_n47_uns_undef);
        break;
    }
    id369out_result = (id369x_1);
  }
  HWOffsetFix<47,-47,UNSIGNED> id370out_o;
  HWOffsetFix<1,0,UNSIGNED> id370out_exception;

  { // Node ID: 370 (NodeCast)
    const HWOffsetFix<48,-47,UNSIGNED> &id370in_i = id369out_result;

    HWOffsetFix<1,0,UNSIGNED> id370x_1;

    id370out_o = (cast_fixed2fixed<47,-47,UNSIGNED,TONEAREVEN>(id370in_i,(&(id370x_1))));
    id370out_exception = (id370x_1);
  }
  { // Node ID: 372 (NodeConstantRawBits)
  }
  HWOffsetFix<47,-47,UNSIGNED> id373out_result;

  { // Node ID: 373 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id373in_sel = id370out_exception;
    const HWOffsetFix<47,-47,UNSIGNED> &id373in_option0 = id370out_o;
    const HWOffsetFix<47,-47,UNSIGNED> &id373in_option1 = id372out_value;

    HWOffsetFix<47,-47,UNSIGNED> id373x_1;

    switch((id373in_sel.getValueAsLong())) {
      case 0l:
        id373x_1 = id373in_option0;
        break;
      case 1l:
        id373x_1 = id373in_option1;
        break;
      default:
        id373x_1 = (c_hw_fix_47_n47_uns_undef);
        break;
    }
    id373out_result = (id373x_1);
  }
  HWRawBits<56> id436out_result;

  { // Node ID: 436 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id436in_in0 = id435out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id436in_in1 = id434out_result;
    const HWOffsetFix<47,-47,UNSIGNED> &id436in_in2 = id373out_result;

    id436out_result = (cat((cat(id436in_in0,id436in_in1)),id436in_in2));
  }
  HWFloat<8,48> id437out_output;

  { // Node ID: 437 (NodeReinterpret)
    const HWRawBits<56> &id437in_input = id436out_result;

    id437out_output = (cast_bits2float<8,48>(id437in_input));
  }
  { // Node ID: 444 (NodeConstantRawBits)
  }
  { // Node ID: 445 (NodeConstantRawBits)
  }
  { // Node ID: 447 (NodeConstantRawBits)
  }
  HWRawBits<56> id810out_result;

  { // Node ID: 810 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id810in_in0 = id444out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id810in_in1 = id445out_value;
    const HWOffsetFix<47,0,UNSIGNED> &id810in_in2 = id447out_value;

    id810out_result = (cat((cat(id810in_in0,id810in_in1)),id810in_in2));
  }
  HWFloat<8,48> id449out_output;

  { // Node ID: 449 (NodeReinterpret)
    const HWRawBits<56> &id449in_input = id810out_result;

    id449out_output = (cast_bits2float<8,48>(id449in_input));
  }
  { // Node ID: 530 (NodeConstantRawBits)
  }
  HWFloat<8,48> id452out_result;

  { // Node ID: 452 (NodeMux)
    const HWRawBits<2> &id452in_sel = id443out_result;
    const HWFloat<8,48> &id452in_option0 = id437out_output;
    const HWFloat<8,48> &id452in_option1 = id449out_output;
    const HWFloat<8,48> &id452in_option2 = id530out_value;
    const HWFloat<8,48> &id452in_option3 = id449out_output;

    HWFloat<8,48> id452x_1;

    switch((id452in_sel.getValueAsLong())) {
      case 0l:
        id452x_1 = id452in_option0;
        break;
      case 1l:
        id452x_1 = id452in_option1;
        break;
      case 2l:
        id452x_1 = id452in_option2;
        break;
      case 3l:
        id452x_1 = id452in_option3;
        break;
      default:
        id452x_1 = (c_hw_flt_8_48_undef);
        break;
    }
    id452out_result = (id452x_1);
  }
  { // Node ID: 884 (NodeConstantRawBits)
  }
  HWFloat<8,48> id462out_result;

  { // Node ID: 462 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id462in_sel = id864out_output[getCycle()%9];
    const HWFloat<8,48> &id462in_option0 = id452out_result;
    const HWFloat<8,48> &id462in_option1 = id884out_value;

    HWFloat<8,48> id462x_1;

    switch((id462in_sel.getValueAsLong())) {
      case 0l:
        id462x_1 = id462in_option0;
        break;
      case 1l:
        id462x_1 = id462in_option1;
        break;
      default:
        id462x_1 = (c_hw_flt_8_48_undef);
        break;
    }
    id462out_result = (id462x_1);
  }
  { // Node ID: 883 (NodeConstantRawBits)
  }
  HWFloat<8,48> id473out_result;
  HWOffsetFix<4,0,UNSIGNED> id473out_exception;

  { // Node ID: 473 (NodeAdd)
    const HWFloat<8,48> &id473in_a = id462out_result;
    const HWFloat<8,48> &id473in_b = id883out_value;

    HWOffsetFix<4,0,UNSIGNED> id473x_1;

    id473out_result = (add_float(id473in_a,id473in_b,(&(id473x_1))));
    id473out_exception = (id473x_1);
  }
  HWFloat<8,48> id475out_result;
  HWOffsetFix<4,0,UNSIGNED> id475out_exception;

  { // Node ID: 475 (NodeDiv)
    const HWFloat<8,48> &id475in_a = id891out_value;
    const HWFloat<8,48> &id475in_b = id473out_result;

    HWOffsetFix<4,0,UNSIGNED> id475x_1;

    id475out_result = (div_float(id475in_a,id475in_b,(&(id475x_1))));
    id475out_exception = (id475x_1);
  }
  HWFloat<8,48> id477out_result;
  HWOffsetFix<4,0,UNSIGNED> id477out_exception;

  { // Node ID: 477 (NodeSub)
    const HWFloat<8,48> &id477in_a = id892out_value;
    const HWFloat<8,48> &id477in_b = id475out_result;

    HWOffsetFix<4,0,UNSIGNED> id477x_1;

    id477out_result = (sub_float(id477in_a,id477in_b,(&(id477x_1))));
    id477out_exception = (id477x_1);
  }
  HWFloat<8,48> id480out_result;

  { // Node ID: 480 (NodeSub)
    const HWFloat<8,48> &id480in_a = id893out_value;
    const HWFloat<8,48> &id480in_b = id477out_result;

    id480out_result = (sub_float(id480in_a,id480in_b));
  }
  HWFloat<8,48> id481out_result;

  { // Node ID: 481 (NodeMul)
    const HWFloat<8,48> &id481in_a = id480out_result;
    const HWFloat<8,48> &id481in_b = id865out_output[getCycle()%10];

    id481out_result = (mul_float(id481in_a,id481in_b));
  }
  HWFloat<8,48> id482out_result;

  { // Node ID: 482 (NodeMul)
    const HWFloat<8,48> &id482in_a = id481out_result;
    const HWFloat<8,48> &id482in_b = id865out_output[getCycle()%10];

    id482out_result = (mul_float(id482in_a,id482in_b));
  }
  { // Node ID: 817 (NodePO2FPMult)
    const HWFloat<8,48> &id817in_floatIn = id482out_result;

    id817out_floatOut[(getCycle()+1)%2] = (mul_float(id817in_floatIn,(c_hw_flt_8_48_0_5val)));
  }
  HWFloat<8,48> id487out_result;

  { // Node ID: 487 (NodeMul)
    const HWFloat<8,48> &id487in_a = id486out_result;
    const HWFloat<8,48> &id487in_b = id817out_floatOut[getCycle()%2];

    id487out_result = (mul_float(id487in_a,id487in_b));
  }
  HWFloat<8,48> id811out_result;

  { // Node ID: 811 (NodeSub)
    const HWFloat<8,48> &id811in_a = id487out_result;
    const HWFloat<8,48> &id811in_b = id877out_output[getCycle()%2];

    id811out_result = (sub_float(id811in_a,id811in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,48> id494out_result;

  { // Node ID: 494 (NodeMul)
    const HWFloat<8,48> &id494in_a = id811out_result;
    const HWFloat<8,48> &id494in_b = id2out_value;

    id494out_result = (mul_float(id494in_a,id494in_b));
  }
  HWFloat<8,48> id496out_result;

  { // Node ID: 496 (NodeMul)
    const HWFloat<8,48> &id496in_a = id19out_o[getCycle()%4];
    const HWFloat<8,48> &id496in_b = id494out_result;

    id496out_result = (mul_float(id496in_a,id496in_b));
  }
  HWFloat<8,48> id497out_result;

  { // Node ID: 497 (NodeAdd)
    const HWFloat<8,48> &id497in_a = id877out_output[getCycle()%2];
    const HWFloat<8,48> &id497in_b = id496out_result;

    id497out_result = (add_float(id497in_a,id497in_b));
  }
  HWFloat<8,48> id849out_output;

  { // Node ID: 849 (NodeStreamOffset)
    const HWFloat<8,48> &id849in_input = id497out_result;

    id849out_output = id849in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id531out_result[getCycle()%2];
    const HWFloat<8,48> &id23in_option0 = id849out_output;
    const HWFloat<8,48> &id23in_option1 = id22out_value;

    HWFloat<8,48> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_8_48_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<8,48> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<8,48> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<8,48> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 816 (NodePO2FPMult)
    const HWFloat<8,48> &id816in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id816x_1;

    id816out_floatOut[(getCycle()+1)%2] = (mul_float(id816in_floatIn,(c_hw_flt_8_48_2_0val),(&(id816x_1))));
    id816out_exception[(getCycle()+1)%2] = (id816x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<8,48> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<8,48> &id42in_input = id816out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<8,48> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-51,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 821 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id821in_exceptionVector = id43out_exception[getCycle()%7];

    (id821st_reg) = (or_fixed((id821st_reg),id821in_exceptionVector));
    id821out_exceptionOccurred[(getCycle()+1)%2] = (id821st_reg);
  }
  { // Node ID: 869 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id869in_input = id821out_exceptionOccurred[getCycle()%2];

    id869out_output[(getCycle()+2)%3] = id869in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 822 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id822in_exceptionVector = id113out_exception;

    (id822st_reg) = (or_fixed((id822st_reg),id822in_exceptionVector));
    id822out_exceptionOccurred[(getCycle()+1)%2] = (id822st_reg);
  }
  { // Node ID: 870 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id870in_input = id822out_exceptionOccurred[getCycle()%2];

    id870out_output[(getCycle()+2)%3] = id870in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 823 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id823in_exceptionVector = id234out_exception;

    (id823st_reg) = (or_fixed((id823st_reg),id823in_exceptionVector));
    id823out_exceptionOccurred[(getCycle()+1)%2] = (id823st_reg);
  }
  { // Node ID: 871 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id871in_input = id823out_exceptionOccurred[getCycle()%2];

    id871out_output[(getCycle()+2)%3] = id871in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 825 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id825in_exceptionVector = id319out_exception;

    (id825st_reg) = (or_fixed((id825st_reg),id825in_exceptionVector));
    id825out_exceptionOccurred[(getCycle()+1)%2] = (id825st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 826 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id826in_exceptionVector = id324out_exception;

    (id826st_reg) = (or_fixed((id826st_reg),id826in_exceptionVector));
    id826out_exceptionOccurred[(getCycle()+1)%2] = (id826st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 827 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id827in_exceptionVector = id328out_exception;

    (id827st_reg) = (or_fixed((id827st_reg),id827in_exceptionVector));
    id827out_exceptionOccurred[(getCycle()+1)%2] = (id827st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 828 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id828in_exceptionVector = id332out_exception;

    (id828st_reg) = (or_fixed((id828st_reg),id828in_exceptionVector));
    id828out_exceptionOccurred[(getCycle()+1)%2] = (id828st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 829 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id829in_exceptionVector = id337out_exception;

    (id829st_reg) = (or_fixed((id829st_reg),id829in_exceptionVector));
    id829out_exceptionOccurred[(getCycle()+1)%2] = (id829st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 830 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id830in_exceptionVector = id341out_exception;

    (id830st_reg) = (or_fixed((id830st_reg),id830in_exceptionVector));
    id830out_exceptionOccurred[(getCycle()+1)%2] = (id830st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 831 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id831in_exceptionVector = id345out_exception;

    (id831st_reg) = (or_fixed((id831st_reg),id831in_exceptionVector));
    id831out_exceptionOccurred[(getCycle()+1)%2] = (id831st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 832 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id832in_exceptionVector = id350out_exception;

    (id832st_reg) = (or_fixed((id832st_reg),id832in_exceptionVector));
    id832out_exceptionOccurred[(getCycle()+1)%2] = (id832st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 833 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id833in_exceptionVector = id354out_exception;

    (id833st_reg) = (or_fixed((id833st_reg),id833in_exceptionVector));
    id833out_exceptionOccurred[(getCycle()+1)%2] = (id833st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 834 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id834in_exceptionVector = id358out_exception;

    (id834st_reg) = (or_fixed((id834st_reg),id834in_exceptionVector));
    id834out_exceptionOccurred[(getCycle()+1)%2] = (id834st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 835 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id835in_exceptionVector = id362out_exception;

    (id835st_reg) = (or_fixed((id835st_reg),id835in_exceptionVector));
    id835out_exceptionOccurred[(getCycle()+1)%2] = (id835st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 838 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id838in_exceptionVector = id370out_exception;

    (id838st_reg) = (or_fixed((id838st_reg),id838in_exceptionVector));
    id838out_exceptionOccurred[(getCycle()+1)%2] = (id838st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 824 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id824in_exceptionVector = id376out_exception;

    (id824st_reg) = (or_fixed((id824st_reg),id824in_exceptionVector));
    id824out_exceptionOccurred[(getCycle()+1)%2] = (id824st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 837 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id837in_exceptionVector = id420out_exception;

    (id837st_reg) = (or_fixed((id837st_reg),id837in_exceptionVector));
    id837out_exceptionOccurred[(getCycle()+1)%2] = (id837st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 839 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id839in_exceptionVector = id473out_exception;

    (id839st_reg) = (or_fixed((id839st_reg),id839in_exceptionVector));
    id839out_exceptionOccurred[(getCycle()+1)%2] = (id839st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 840 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id840in_exceptionVector = id475out_exception;

    (id840st_reg) = (or_fixed((id840st_reg),id840in_exceptionVector));
    id840out_exceptionOccurred[(getCycle()+1)%2] = (id840st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 841 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id841in_exceptionVector = id477out_exception;

    (id841st_reg) = (or_fixed((id841st_reg),id841in_exceptionVector));
    id841out_exceptionOccurred[(getCycle()+1)%2] = (id841st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 836 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id836in_exceptionVector = id815out_exception;

    (id836st_reg) = (or_fixed((id836st_reg),id836in_exceptionVector));
    id836out_exceptionOccurred[(getCycle()+1)%2] = (id836st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 820 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id820in_exceptionVector = id816out_exception[getCycle()%2];

    (id820st_reg) = (or_fixed((id820st_reg),id820in_exceptionVector));
    id820out_exceptionOccurred[(getCycle()+1)%2] = (id820st_reg);
  }
  { // Node ID: 872 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id872in_input = id820out_exceptionOccurred[getCycle()%2];

    id872out_output[(getCycle()+8)%9] = id872in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 842 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id842in_n43 = id869out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n113 = id870out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n234 = id871out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n319 = id825out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n324 = id826out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n328 = id827out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n332 = id828out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n337 = id829out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n341 = id830out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n345 = id831out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n350 = id832out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n354 = id833out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n358 = id834out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n362 = id835out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n370 = id838out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n376 = id824out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n420 = id837out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id842in_n473 = id839out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id842in_n475 = id840out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id842in_n477 = id841out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id842in_n815 = id836out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id842in_n816 = id872out_output[getCycle()%9];

    id842out_all_exceptions = (cat((cat((cat((cat((cat(id842in_n43,id842in_n113)),id842in_n234)),(cat((cat(id842in_n319,id842in_n324)),id842in_n328)))),(cat((cat((cat(id842in_n332,id842in_n337)),id842in_n341)),(cat(id842in_n345,id842in_n350)))))),(cat((cat((cat((cat(id842in_n354,id842in_n358)),id842in_n362)),(cat((cat(id842in_n370,id842in_n376)),id842in_n420)))),(cat((cat((cat(id842in_n473,id842in_n475)),id842in_n477)),(cat(id842in_n815,id842in_n816))))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 843 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id843in_load = id844out_value;
    const HWRawBits<37> &id843in_data = id842out_all_exceptions;

    bool id843x_1;

    (id843x_1) = ((id843in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id843x_1)) {
      setMappedRegValue("all_numeric_exceptions", id843in_data);
    }
  }
  { // Node ID: 818 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 819 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id819in_output_control = id818out_value;
    const HWFloat<8,48> &id819in_data = id849out_output;

    bool id819x_1;

    (id819x_1) = ((id819in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id819x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id819in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 873 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id873in_input = id17out_count;

    id873out_output[(getCycle()+1)%2] = id873in_input;
  }
  { // Node ID: 882 (NodeConstantRawBits)
  }
  { // Node ID: 505 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id505in_a = id878out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id505in_b = id882out_value;

    id505out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id505in_a,id505in_b));
  }
  { // Node ID: 812 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id812in_a = id873out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id812in_b = id505out_result[getCycle()%2];

    id812out_result[(getCycle()+1)%2] = (eq_fixed(id812in_a,id812in_b));
  }
  { // Node ID: 507 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id508out_result;

  { // Node ID: 508 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id508in_a = id507out_io_u_out_force_disabled;

    id508out_result = (not_fixed(id508in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id509out_result;

  { // Node ID: 509 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id509in_a = id812out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id509in_b = id508out_result;

    HWOffsetFix<1,0,UNSIGNED> id509x_1;

    (id509x_1) = (and_fixed(id509in_a,id509in_b));
    id509out_result = (id509x_1);
  }
  { // Node ID: 874 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id874in_input = id509out_result;

    id874out_output[(getCycle()+12)%13] = id874in_input;
  }
  { // Node ID: 502 (NodeCast)
    const HWFloat<8,48> &id502in_i = id497out_result;

    id502out_o[(getCycle()+2)%3] = (cast_float2float<11,53>(id502in_i));
  }
  if ( (getFillLevel() >= (17l)) && (getFlushLevel() < (17l)|| !isFlushingActive() ))
  { // Node ID: 510 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id510in_output_control = id874out_output[getCycle()%13];
    const HWFloat<11,53> &id510in_data = id502out_o[getCycle()%3];

    bool id510x_1;

    (id510x_1) = ((id510in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(17l))&(isFlushingActive()))));
    if((id510x_1)) {
      writeOutput(m_u_out, id510in_data);
    }
  }
  { // Node ID: 881 (NodeConstantRawBits)
  }
  { // Node ID: 512 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id512in_a = id878out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id512in_b = id881out_value;

    id512out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id512in_a,id512in_b));
  }
  { // Node ID: 813 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id813in_a = id873out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id813in_b = id512out_result[getCycle()%2];

    id813out_result[(getCycle()+1)%2] = (eq_fixed(id813in_a,id813in_b));
  }
  { // Node ID: 514 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id515out_result;

  { // Node ID: 515 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id515in_a = id514out_io_v_out_force_disabled;

    id515out_result = (not_fixed(id515in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id516out_result;

  { // Node ID: 516 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id516in_a = id813out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id516in_b = id515out_result;

    HWOffsetFix<1,0,UNSIGNED> id516x_1;

    (id516x_1) = (and_fixed(id516in_a,id516in_b));
    id516out_result = (id516x_1);
  }
  { // Node ID: 876 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id876in_input = id516out_result;

    id876out_output[(getCycle()+12)%13] = id876in_input;
  }
  { // Node ID: 503 (NodeCast)
    const HWFloat<8,48> &id503in_i = id499out_result;

    id503out_o[(getCycle()+2)%3] = (cast_float2float<11,53>(id503in_i));
  }
  if ( (getFillLevel() >= (17l)) && (getFlushLevel() < (17l)|| !isFlushingActive() ))
  { // Node ID: 517 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id517in_output_control = id876out_output[getCycle()%13];
    const HWFloat<11,53> &id517in_data = id503out_o[getCycle()%3];

    bool id517x_1;

    (id517x_1) = ((id517in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(17l))&(isFlushingActive()))));
    if((id517x_1)) {
      writeOutput(m_v_out, id517in_data);
    }
  }
  { // Node ID: 522 (NodeConstantRawBits)
  }
  { // Node ID: 880 (NodeConstantRawBits)
  }
  { // Node ID: 519 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 520 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id520in_enable = id880out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id520in_max = id519out_value;

    HWOffsetFix<49,0,UNSIGNED> id520x_1;
    HWOffsetFix<1,0,UNSIGNED> id520x_2;
    HWOffsetFix<1,0,UNSIGNED> id520x_3;
    HWOffsetFix<49,0,UNSIGNED> id520x_4t_1e_1;

    id520out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id520st_count)));
    (id520x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id520st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id520x_2) = (gte_fixed((id520x_1),id520in_max));
    (id520x_3) = (and_fixed((id520x_2),id520in_enable));
    id520out_wrap = (id520x_3);
    if((id520in_enable.getValueAsBool())) {
      if(((id520x_3).getValueAsBool())) {
        (id520st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id520x_4t_1e_1) = (id520x_1);
        (id520st_count) = (id520x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id521out_output;

  { // Node ID: 521 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id521in_input = id520out_count;

    id521out_output = id521in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 523 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id523in_load = id522out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id523in_data = id521out_output;

    bool id523x_1;

    (id523x_1) = ((id523in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id523x_1)) {
      setMappedRegValue("current_run_cycle_count", id523in_data);
    }
  }
  { // Node ID: 879 (NodeConstantRawBits)
  }
  { // Node ID: 525 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 526 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id526in_enable = id879out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id526in_max = id525out_value;

    HWOffsetFix<49,0,UNSIGNED> id526x_1;
    HWOffsetFix<1,0,UNSIGNED> id526x_2;
    HWOffsetFix<1,0,UNSIGNED> id526x_3;
    HWOffsetFix<49,0,UNSIGNED> id526x_4t_1e_1;

    id526out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id526st_count)));
    (id526x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id526st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id526x_2) = (gte_fixed((id526x_1),id526in_max));
    (id526x_3) = (and_fixed((id526x_2),id526in_enable));
    id526out_wrap = (id526x_3);
    if((id526in_enable.getValueAsBool())) {
      if(((id526x_3).getValueAsBool())) {
        (id526st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id526x_4t_1e_1) = (id526x_1);
        (id526st_count) = (id526x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 528 (NodeInputMappedReg)
  }
  { // Node ID: 814 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id814in_a = id526out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id814in_b = id528out_run_cycle_count;

    id814out_result[(getCycle()+1)%2] = (eq_fixed(id814in_a,id814in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 527 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id527in_start = id814out_result[getCycle()%2];

    if((id527in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
