/**
 * Document: MaxCompiler Tutorial (maxcompiler-tutorial)
 * Chapter: 3      Example: 1      Name: Moving Average Simple
 * MaxFile name: MovingAverageSimple
 * Summary:
 *   CPU code for the three point moving average design.
 */
#include "Maxfiles.h" 			// Includes .max files
#include <MaxSLiCInterface.h>	// Simple Live CPU interface
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>


#define NEXT_LINE(fp) while(getc(fp)!='\n');

double dt = 0;
int simTime = 0;
int mode = 0;
int preciscion = 0;
int num_iteration = 0;



const float EPI_TVP   =     1.4506;
  const float EPI_TV1M  =    60.;
 const float  EPI_TV2M  =  1150.;

 const float  EPI_TWP   =   200.0;

 const float  EPI_TW1M  =    60.0;
 const float  EPI_TW2M  =    15.;

 const float  EPI_TS1   =    2.7342;
  const float EPI_TS2   =   16.;
  const float EPI_TFI   =    0.11;
  const float EPI_TO1   =  400.;    //The same with Flavio's paper
 const float  EPI_TO2   =    6.  ;    //The same with Flavio's paper
 const float  EPI_TSO1  =   30.0181; //The same with Flavio's paper
 const float  EPI_TSO2  =    0.9957;  //The same with Flavio's paper

 const float  EPI_TSI   =    1.8875;  // We have TSI1 and TSI2 = TSI in Flavio's paper


  const float EPI_TWINF  =  0.07;
 const float  EPI_THV    =  0.3;     //EPUM % The same of Flavio's paper
  const float EPI_THVM   =  0.006;   //EPUQ % The same of Flavio's paper
 const float  EPI_THVINF =  0.006;  // %EPUQ % The same of Flavio's paper
 const float  EPI_THW    =  0.13;   // %EPUP % The same of Flavio's paper
 const float  EPI_THWINF =  0.006;  //  %EPURR % In Flavio's paper 0.13
 const float  EPI_THSO   =  0.13;   // %EPUP % The same of Flavio's paper
 const float  EPI_THSI   =  0.13;    //%EPUP % The same of Flavio's paper
 const float  EPI_THO    =  0.006;   // %EPURR % The same of Flavio's paper

 const float  EPI_KWM    =  65.;    // %The same of Flavio's paper
 const float  EPI_KS     =  2.0994; // %The same of Flavio's paper
 const float  EPI_KSO    =  2.0458;  //%The same of Flavio's paper

const float   EPI_UWM    =  0.03;   // %The same of Flavio's paper
const float   EPI_US     =  0.9087; // %The same of Flavio's paper
const float   EPI_UO     =  0.;     //% The same of Flavio's paper
 const float  EPI_UU     =  1.55;  // % The same of Flavio's paper
const float   EPI_USO    =  0.65;  // % The same of Flavio's paper

void initValues(float * u, float * v, float * w, float * s) {
	for(int i = 0; i<simTime; i++) {
		u[i] = 0.0;
		v[i] = 1.0;
		w[i] = 1.0;
		s[i] = 0.0;

	}
}

void print(float * u_out, float  * v_out, float * w_out, float * s_out , float * stim_out) {
	char filename[1024];
	sprintf(filename, "minimal_cell_out_maxeler_%f_%d_56.csv", dt , simTime);

	if( access(filename, F_OK) != -1) {
			remove(filename);
		}
	FILE *fp = fopen(filename, "w");

		if(fp==NULL) {
			printf("no file found");
		}
		else {
			printf("u;v;w;s;stim\n");

			//fprintf(fp,"u;v;w;s;stim\n");
			for (int i = 0; i<num_iteration; i++) {

	//printf("%.15f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
	fprintf(fp,"%G;%g;%g;%g;%g\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);

}
			fclose(fp);
			fp = NULL;
	}
}

int random_index(int min, int max)
{
   srand ( time(NULL) );
   return min + rand() % (max+1 - min);
}


void initStim(float * stim_in) {
	int simTimesControlled[3] = {0 , 300, 700};



		if(mode == 1) {
			printf("Normal Mode.. Generating..\n");
			take_step(stim_in,num_iteration,3,simTimesControlled);

		 }else if(mode == 2) {
			printf("Random Mode.. Generating..\n");
			int num = 0;
			int * randm = malloc((sizeof(int)*num));
			srand ( time(NULL) );
			for (int i = 0; i<simTime;) {
				randm[num] = i;
				num++;
				int min = 300;
				int max = simTime;
				int random = rand() % (max+1);
				i += random;


			}
			take_step(stim_in, num_iteration, num, randm);


		} else if(mode == 3) {
			printf("Spaced Mode.. Generating..\n");
			int num = simTime/400;
			int * spaced = malloc((sizeof(int)*num));
			int count = 0;
			for(int i = 0; i<simTime; i+=400) {
				spaced[count] = i;
				count++;

			}
			take_step(stim_in, num_iteration, (num+1), spaced);
		} else {
			printf("FALLBACK\nNormal Mode.. Generating");
			take_step(stim_in,num_iteration,3,simTimesControlled);

		}





}

void reactionTerm(float * u, float *v, float * w, float * s, float stim) {
float jfi, jso, jsi;

 if ((*u) < 0.006) {
	              *w = (*w) + ((1.0 -((*u)/EPI_TWINF) - (*w))/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*((*u)-EPI_UWM)))))*dt;
	              *v = (*v) + ((1.0-(*v))/EPI_TV1M)*dt;
	              *s = (*s) + ((((1.+tanh(EPI_KS*((*u) - EPI_US))) * 0.5) - (*s))/EPI_TS1)*dt;
	              jfi = 0.0;
	              jso = (*u)/EPI_TO1;
	              jsi = 0.0;
	    }

	         else if ((*u) < 0.13) {
	              *w = (*w) + ((0.94-(*w))/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*((*u)-EPI_UWM)))))*dt;
	              *v = (*v) + (-(*v)/EPI_TV2M)*dt;
	              *s = (*s) +((((1.+tanh(EPI_KS*((*u)-EPI_US))) * 0.5) - (*s))/EPI_TS1)*dt;
	              jfi = 0.0;
	              jso = (*u)/EPI_TO2;
	              jsi = 0.0;
	         }

	         else if ((*u) < 0.3){
	              *w = (*w) + (-(*w)/EPI_TWP)*dt;
	              *v = (*v) + (-(*v)/EPI_TV2M)*dt;
	              *s = (*s) + ((((1.+tanh(EPI_KS*((*u)-EPI_US))) * 0.5) - (*s))/EPI_TS2)*dt;
	              jfi = 0.0;
	              jso = 1./(EPI_TSO1+((EPI_TSO2-EPI_TSO1)*(1.+tanh(EPI_KSO*((*u) - EPI_USO)))) * 0.5);
	              jsi = -(*w) * (*s)/EPI_TSI;
	         }  else {
	              *w  = (*w) + (-(*w)/EPI_TWP)*dt;
	              *v  = (*v) + (-(*v)/EPI_TVP)*dt;
	              *s  = (*s) +((((1.+tanh(EPI_KS*((*u) - EPI_US))) * 0.5) - (*s))/EPI_TS2)*dt;

	              jfi = -(*v) * ((*u) - EPI_THV) * (EPI_UU - (*u))/EPI_TFI;
	              jso = 1./(EPI_TSO1+((EPI_TSO2 - EPI_TSO1)*(1.+tanh(EPI_KSO*((*u) - EPI_USO)))) * 0.5);
	              jsi = -(*w) * (*s)/EPI_TSI;
	}
	    *u = (*u)  - (jfi+jso+jsi-stim)*dt;

}

void writeToFile(char * filename, float u, float v, float s, float  w, float stim) {

FILE *fp = fopen(filename, "a");
fprintf(fp, "%f\t%f\t%f\t%f\t%f\n", u, v,  s, w,stim);
fclose(fp);
}

float heavisidefun(float x) {
	//printf("HEVAISIDE: %f\n", x);
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}
void take_step(float *stim_in, const int time,int stimNum, int * stimTimes) {

	int sizeBytes = (stimNum) * sizeof(float);
	float t1 = 0.0;
	float t2 = 0.0;



	for(int i = 0; i<time; i++){


			t1+=dt;
			t2+=dt;

			for(int j = 0; j<stimNum; j++){
			float c1 = t1-stimTimes[j];
			float c2 = t2-(stimTimes[j]+1);
			stim_in[i] += heavisidefun(c1) * (1 - heavisidefun(c2));

			//printf("DT(MS) :%f, STIMULUS: %f\n", dt, stim_in[i]);
			}

	}









}


void initParam(char * params) {

	  FILE *fp;

	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.05;
		  simTime = 1000;
		  mode = 1;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%f",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %f\n", dt);
		fscanf(fp,"%d",&mode);
		printf("READ MODE: %i\n", mode);

	   fclose(fp);

	  }
	  num_iteration = (int)(simTime/dt);
	  preciscion = 1;


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %f\n", dt);
	  printf("mode     = %i\n", mode);



}


void runDFE() {



	int  sizeBytes = (num_iteration) * sizeof(float);
printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
float * u_out = malloc(sizeBytes);
float * v_out = malloc(sizeBytes);
float * w_out = malloc(sizeBytes);
float * s_out =  malloc(sizeBytes);
float * stim = malloc(sizeBytes);
int alive = 0;
struct timeval start, end;
		long mtime, seconds, useconds;


		//	initStim(stim);


max_file_t *myMaxFile = MinimalModelDFE_init();
max_engine_t *myDFE = max_load(myMaxFile, "local:*");


MinimalModelDFE_actions_t actions;

actions.param_length = num_iteration;
actions.param_dt = dt;
actions.param_num_iteration = num_iteration;
//actions.outstream_stim_out = stim;
actions.outstream_u_out = u_out;
actions.outstream_v_out = v_out;
actions.outstream_w_out = w_out;
actions.outstream_s_out = s_out;
//actions.outscalar_SingleCellDFEKernel_alive = &alive;






printf("Running DFE\n");
			gettimeofday(&start, NULL);
			MinimalModelDFE_run(myDFE, &actions);
	//memcpy(us, u_out, sizeBytes);
	gettimeofday(&end, NULL);
		  seconds = end.tv_sec - start.tv_sec;
		  		useconds = end.tv_usec - start.tv_usec;

		  		mtime = ((seconds) * 1000 + useconds / 1000.0);
	printf("DFE elapsed time: %ld milliseconds \n", mtime);
		  		printTimes(mtime);
	printf("Printing Data from DFE\n");

	print(u_out, v_out, w_out, s_out,stim);










	free(u_out);
	u_out = NULL;
	free(v_out);
	v_out = NULL;
	free(w_out);
	w_out = NULL;
	free(s_out);
	s_out = NULL;
	free(stim);
	stim = NULL;
	max_unload(myDFE);




}
void printTimes(long time) {
	char * filename = "DFE_times.dat";
	FILE *fp = fopen(filename, "a");
	fprintf(fp, "dt: %f\nsimTime: %d\nelapsed Time: %ld\n-----------------------------\n", dt,simTime, time);
	fclose(fp);

}


void precisionTest() {
	dt = 0.1/64;
	simTime = 1000;
	num_iteration = (int) simTime/dt;
	runDFE();
}

void test1() {
	char * params = "test1.dat";
	initParam(params);
	runDFE();
}

int main() {



	precisionTest();
	//test1();
	return 0;
}
