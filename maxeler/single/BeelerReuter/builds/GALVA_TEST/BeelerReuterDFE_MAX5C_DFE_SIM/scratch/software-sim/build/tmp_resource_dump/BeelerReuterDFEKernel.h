#ifndef BEELERREUTERDFEKERNEL_H_
#define BEELERREUTERDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class BeelerReuterDFEKernel : public KernelManagerBlockSync {
public:
  BeelerReuterDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_ca_out;
  t_port_number m_x1_out;
  t_port_number m_m_out;
  t_port_number m_f_out;
  t_port_number m_h_out;
  t_port_number m_d_out;
  t_port_number m_j_out;
  HWOffsetFix<1,0,UNSIGNED> id8out_value;

  HWOffsetFix<11,0,UNSIGNED> id3434out_value;

  HWOffsetFix<11,0,UNSIGNED> id11out_count;
  HWOffsetFix<1,0,UNSIGNED> id11out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id11st_count;

  HWOffsetFix<11,0,UNSIGNED> id3112out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id3699out_value;

  HWOffsetFix<11,0,UNSIGNED> id2711out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2819out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3113out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id2713out_io_u_out_force_disabled;

  HWOffsetFix<32,0,UNSIGNED> id9out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id10out_count;
  HWOffsetFix<1,0,UNSIGNED> id10out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id10st_count;

  HWOffsetFix<32,0,UNSIGNED> id3698out_value;

  HWOffsetFix<1,0,UNSIGNED> id2820out_result[2];

  HWFloat<11,53> id12out_dt;

  HWFloat<11,53> id3697out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2128out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3016out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3684out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3681out_value;

  HWOffsetFix<1,0,UNSIGNED> id2138out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2122out_value;

  HWOffsetFix<1,0,UNSIGNED> id2147out_value;

  HWOffsetFix<11,0,UNSIGNED> id2148out_value;

  HWOffsetFix<52,0,UNSIGNED> id2150out_value;

  HWFloat<11,53> id2795out_value;

  HWFloat<11,53> id3680out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2225out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3018out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3676out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3673out_value;

  HWOffsetFix<1,0,UNSIGNED> id2235out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2219out_value;

  HWOffsetFix<1,0,UNSIGNED> id2244out_value;

  HWOffsetFix<11,0,UNSIGNED> id2245out_value;

  HWOffsetFix<52,0,UNSIGNED> id2247out_value;

  HWFloat<11,53> id2796out_value;

  HWFloat<11,53> id3672out_value;

  HWFloat<11,53> id5out_value;

  HWFloat<11,53> id2373out_value;

  HWFloat<11,53> id3671out_value;

  HWFloat<11,53> id3670out_value;

  HWFloat<11,53> id3669out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2323out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3020out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3665out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3662out_value;

  HWOffsetFix<1,0,UNSIGNED> id2333out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2317out_value;

  HWOffsetFix<1,0,UNSIGNED> id2342out_value;

  HWOffsetFix<11,0,UNSIGNED> id2343out_value;

  HWOffsetFix<52,0,UNSIGNED> id2345out_value;

  HWFloat<11,53> id2797out_value;

  HWFloat<11,53> id3661out_value;

  HWFloat<11,53> id2376out_value;

  HWFloat<11,53> id3660out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2538out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3022out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3656out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3653out_value;

  HWOffsetFix<1,0,UNSIGNED> id2548out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2532out_value;

  HWOffsetFix<1,0,UNSIGNED> id2557out_value;

  HWOffsetFix<11,0,UNSIGNED> id2558out_value;

  HWOffsetFix<52,0,UNSIGNED> id2560out_value;

  HWFloat<11,53> id2798out_value;

  HWFloat<11,53> id3652out_value;

  HWFloat<11,53> id3651out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2639out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3024out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3647out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3644out_value;

  HWOffsetFix<1,0,UNSIGNED> id2649out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2633out_value;

  HWOffsetFix<1,0,UNSIGNED> id2658out_value;

  HWOffsetFix<11,0,UNSIGNED> id2659out_value;

  HWOffsetFix<52,0,UNSIGNED> id2661out_value;

  HWFloat<11,53> id2799out_value;

  HWFloat<11,53> id3643out_value;

  HWFloat<11,53> id3582out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id700out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3038out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3579out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3576out_value;

  HWOffsetFix<1,0,UNSIGNED> id710out_value;

  HWOffsetFix<53,-52,UNSIGNED> id694out_value;

  HWOffsetFix<1,0,UNSIGNED> id719out_value;

  HWOffsetFix<11,0,UNSIGNED> id720out_value;

  HWOffsetFix<52,0,UNSIGNED> id722out_value;

  HWFloat<11,53> id2806out_value;

  HWFloat<11,53> id3575out_value;

  HWFloat<11,53> id3574out_value;

  HWFloat<11,53> id3573out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id800out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3040out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3569out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3566out_value;

  HWOffsetFix<1,0,UNSIGNED> id810out_value;

  HWOffsetFix<53,-52,UNSIGNED> id794out_value;

  HWOffsetFix<1,0,UNSIGNED> id819out_value;

  HWOffsetFix<11,0,UNSIGNED> id820out_value;

  HWOffsetFix<52,0,UNSIGNED> id822out_value;

  HWFloat<11,53> id2807out_value;

  HWFloat<11,53> id3565out_value;

  HWFloat<11,53> id3564out_value;

  HWFloat<11,53> id3562out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id902out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3042out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3559out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3556out_value;

  HWOffsetFix<1,0,UNSIGNED> id912out_value;

  HWOffsetFix<53,-52,UNSIGNED> id896out_value;

  HWOffsetFix<1,0,UNSIGNED> id921out_value;

  HWOffsetFix<11,0,UNSIGNED> id922out_value;

  HWOffsetFix<52,0,UNSIGNED> id924out_value;

  HWFloat<11,53> id2808out_value;

  HWFloat<11,53> id3555out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1001out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3044out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3551out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3548out_value;

  HWOffsetFix<1,0,UNSIGNED> id1011out_value;

  HWOffsetFix<53,-52,UNSIGNED> id995out_value;

  HWOffsetFix<1,0,UNSIGNED> id1020out_value;

  HWOffsetFix<11,0,UNSIGNED> id1021out_value;

  HWOffsetFix<52,0,UNSIGNED> id1023out_value;

  HWFloat<11,53> id2809out_value;

  HWFloat<11,53> id3547out_value;

  HWFloat<11,53> id3546out_value;

  HWFloat<11,53> id3545out_value;

  HWFloat<11,53> id3544out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1102out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3046out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3540out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3537out_value;

  HWOffsetFix<1,0,UNSIGNED> id1112out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1096out_value;

  HWOffsetFix<1,0,UNSIGNED> id1121out_value;

  HWOffsetFix<11,0,UNSIGNED> id1122out_value;

  HWOffsetFix<52,0,UNSIGNED> id1124out_value;

  HWFloat<11,53> id2810out_value;

  HWFloat<11,53> id3536out_value;

  HWFloat<11,53> id3535out_value;

  HWFloat<11,53> id3out_value;

  HWFloat<11,53> id2out_value;

  HWFloat<11,53> id3534out_value;

  HWFloat<11,53> id3457out_value;

  HWFloat<11,53> id3456out_value;

  HWFloat<11,53> id3451out_value;

  HWRawBits<11> id2034out_value;

  HWRawBits<52> id3450out_value;

  HWFloat<11,53> id3449out_value;

  HWRawBits<63> id2044out_value;

  HWOffsetFix<1,0,UNSIGNED> id2005out_value;

  HWOffsetFix<12,0,TWOSCOMPLEMENT> id3448out_value;

  HWOffsetFix<12,0,TWOSCOMPLEMENT> id2010out_value;

  HWFloat<11,53> id3446out_value;

  HWFloat<11,53> id3445out_value;

  HWFloat<11,53> id3444out_value;

  HWFloat<11,53> id2057out_value;

  HWFloat<11,53> id16out_u_in;

  HWFloat<11,53> id17out_result[2];

  HWFloat<11,53> id3124out_output[2];

  HWFloat<11,53> id3431out_output[8];

  HWFloat<11,53> id3432out_output[2];

  HWFloat<11,53> id3696out_value;

  HWFloat<11,53> id3695out_value;

  HWRawBits<11> id2459out_value;

  HWRawBits<52> id3694out_value;

  HWOffsetFix<1,0,UNSIGNED> id3123out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2382out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2384out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2384out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2387out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3114out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2428out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3014out_value;

  HWOffsetFix<38,-53,UNSIGNED> id2474out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2474sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2471out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2471sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2468out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2468sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2405out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3115out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3693out_value;

  HWFloat<11,53> id3692out_value;

  HWOffsetFix<1,0,UNSIGNED> id3117out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3118out_output[3];

  HWFloat<11,53> id3691out_value;

  HWOffsetFix<1,0,UNSIGNED> id3119out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3120out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3690out_value;

  HWOffsetFix<1,0,UNSIGNED> id2438out_value;

  HWOffsetFix<53,-52,UNSIGNED> id2422out_value;

  HWOffsetFix<1,0,UNSIGNED> id2447out_value;

  HWOffsetFix<11,0,UNSIGNED> id2448out_value;

  HWOffsetFix<52,0,UNSIGNED> id2450out_value;

  HWFloat<11,53> id2794out_value;

  HWFloat<11,53> id3689out_value;

  HWFloat<11,53> id3688out_value;

  HWFloat<11,53> id3091out_floatOut[2];

  HWFloat<11,53> id3687out_value;

  HWFloat<11,53> id3686out_value;

  HWRawBits<11> id2159out_value;

  HWRawBits<52> id3685out_value;

  HWOffsetFix<1,0,UNSIGNED> id3134out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2082out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2084out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2084out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2087out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3125out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id2174out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2174sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2171out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2171sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2168out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2168sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2105out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3126out_output[3];

  HWFloat<11,53> id3683out_value;

  HWOffsetFix<1,0,UNSIGNED> id3128out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3129out_output[3];

  HWFloat<11,53> id3682out_value;

  HWOffsetFix<1,0,UNSIGNED> id3130out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3131out_output[3];

  HWFloat<11,53> id3679out_value;

  HWFloat<11,53> id3678out_value;

  HWRawBits<11> id2256out_value;

  HWRawBits<52> id3677out_value;

  HWOffsetFix<1,0,UNSIGNED> id3145out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2179out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2181out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2181out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2184out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3136out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id2271out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2271sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2268out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2268sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2265out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2265sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2202out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3137out_output[3];

  HWFloat<11,53> id3675out_value;

  HWOffsetFix<1,0,UNSIGNED> id3139out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3140out_output[3];

  HWFloat<11,53> id3674out_value;

  HWOffsetFix<1,0,UNSIGNED> id3141out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3142out_output[3];

  HWFloat<11,53> id3668out_value;

  HWFloat<11,53> id3667out_value;

  HWRawBits<11> id2354out_value;

  HWRawBits<52> id3666out_value;

  HWOffsetFix<1,0,UNSIGNED> id3157out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2277out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2279out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2279out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2282out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3148out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id2369out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2369sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2366out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2366sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2363out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2363sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2300out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3149out_output[3];

  HWFloat<11,53> id3664out_value;

  HWOffsetFix<1,0,UNSIGNED> id3151out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3152out_output[3];

  HWFloat<11,53> id3663out_value;

  HWOffsetFix<1,0,UNSIGNED> id3153out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3154out_output[3];

  HWFloat<11,53> id3659out_value;

  HWFloat<11,53> id3658out_value;

  HWRawBits<11> id2569out_value;

  HWRawBits<52> id3657out_value;

  HWOffsetFix<1,0,UNSIGNED> id3168out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2492out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2494out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2494out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2497out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3159out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id2584out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2584sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2581out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2581sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2578out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2578sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2515out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3160out_output[3];

  HWFloat<11,53> id3655out_value;

  HWOffsetFix<1,0,UNSIGNED> id3162out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3163out_output[3];

  HWFloat<11,53> id3654out_value;

  HWOffsetFix<1,0,UNSIGNED> id3164out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3165out_output[3];

  HWFloat<11,53> id3650out_value;

  HWFloat<11,53> id3649out_value;

  HWRawBits<11> id2670out_value;

  HWRawBits<52> id3648out_value;

  HWOffsetFix<1,0,UNSIGNED> id3179out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2593out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id2595out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2595out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id2598out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3170out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id2685out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id2685sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id2682out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id2682sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id2679out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id2679sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id2616out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3171out_output[3];

  HWFloat<11,53> id3646out_value;

  HWOffsetFix<1,0,UNSIGNED> id3173out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3174out_output[3];

  HWFloat<11,53> id3645out_value;

  HWOffsetFix<1,0,UNSIGNED> id3175out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3176out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3642out_value;

  HWOffsetFix<1,0,UNSIGNED> id2863out_result[2];

  HWFloat<11,53> id24out_x1_in;

  HWFloat<11,53> id25out_result[2];

  HWFloat<11,53> id3201out_output[9];

  HWFloat<11,53> id3641out_value;

  HWFloat<11,53> id3640out_value;

  HWFloat<11,53> id3639out_value;

  HWRawBits<11> id127out_value;

  HWRawBits<52> id3638out_value;

  HWOffsetFix<1,0,UNSIGNED> id3190out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id50out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id52out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id52out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id55out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3181out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id96out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3026out_value;

  HWOffsetFix<38,-53,UNSIGNED> id142out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id142sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id139out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id139sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id136out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id136sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id73out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3182out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3637out_value;

  HWFloat<11,53> id3636out_value;

  HWOffsetFix<1,0,UNSIGNED> id3184out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3185out_output[3];

  HWFloat<11,53> id3635out_value;

  HWOffsetFix<1,0,UNSIGNED> id3186out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3187out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3634out_value;

  HWOffsetFix<1,0,UNSIGNED> id106out_value;

  HWOffsetFix<53,-52,UNSIGNED> id90out_value;

  HWOffsetFix<1,0,UNSIGNED> id115out_value;

  HWOffsetFix<11,0,UNSIGNED> id116out_value;

  HWOffsetFix<52,0,UNSIGNED> id118out_value;

  HWFloat<11,53> id2800out_value;

  HWFloat<11,53> id3633out_value;

  HWFloat<11,53> id3632out_value;

  HWFloat<11,53> id3631out_value;

  HWRawBits<11> id226out_value;

  HWRawBits<52> id3630out_value;

  HWOffsetFix<1,0,UNSIGNED> id3200out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id149out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id151out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id151out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id154out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3191out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id195out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3028out_value;

  HWOffsetFix<38,-53,UNSIGNED> id241out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id241sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id238out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id238sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id235out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id235sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id172out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3192out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3629out_value;

  HWFloat<11,53> id3628out_value;

  HWOffsetFix<1,0,UNSIGNED> id3194out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3195out_output[3];

  HWFloat<11,53> id3627out_value;

  HWOffsetFix<1,0,UNSIGNED> id3196out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3197out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3626out_value;

  HWOffsetFix<1,0,UNSIGNED> id205out_value;

  HWOffsetFix<53,-52,UNSIGNED> id189out_value;

  HWOffsetFix<1,0,UNSIGNED> id214out_value;

  HWOffsetFix<11,0,UNSIGNED> id215out_value;

  HWOffsetFix<52,0,UNSIGNED> id217out_value;

  HWFloat<11,53> id2801out_value;

  HWFloat<11,53> id3625out_value;

  HWFloat<11,53> id3624out_value;

  HWFloat<11,53> id3623out_value;

  HWFloat<11,53> id3622out_value;

  HWFloat<11,53> id3621out_value;

  HWFloat<11,53> id3620out_value;

  HWRawBits<11> id327out_value;

  HWRawBits<52> id3619out_value;

  HWOffsetFix<1,0,UNSIGNED> id3211out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id250out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id252out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id252out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id255out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3202out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id296out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3030out_value;

  HWOffsetFix<38,-53,UNSIGNED> id342out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id342sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id339out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id339sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id336out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id336sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id273out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3203out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3618out_value;

  HWFloat<11,53> id3617out_value;

  HWOffsetFix<1,0,UNSIGNED> id3205out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3206out_output[3];

  HWFloat<11,53> id3616out_value;

  HWOffsetFix<1,0,UNSIGNED> id3207out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3208out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3615out_value;

  HWOffsetFix<1,0,UNSIGNED> id306out_value;

  HWOffsetFix<53,-52,UNSIGNED> id290out_value;

  HWOffsetFix<1,0,UNSIGNED> id315out_value;

  HWOffsetFix<11,0,UNSIGNED> id316out_value;

  HWOffsetFix<52,0,UNSIGNED> id318out_value;

  HWFloat<11,53> id2802out_value;

  HWFloat<11,53> id3614out_value;

  HWFloat<11,53> id3613out_value;

  HWFloat<11,53> id3612out_value;

  HWRawBits<11> id426out_value;

  HWRawBits<52> id3611out_value;

  HWOffsetFix<1,0,UNSIGNED> id3221out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id349out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id351out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id351out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id354out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3212out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id395out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3032out_value;

  HWOffsetFix<38,-53,UNSIGNED> id441out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id441sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id438out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id438sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id435out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id435sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id372out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3213out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3610out_value;

  HWFloat<11,53> id3609out_value;

  HWOffsetFix<1,0,UNSIGNED> id3215out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3216out_output[3];

  HWFloat<11,53> id3608out_value;

  HWOffsetFix<1,0,UNSIGNED> id3217out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3218out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3607out_value;

  HWOffsetFix<1,0,UNSIGNED> id405out_value;

  HWOffsetFix<53,-52,UNSIGNED> id389out_value;

  HWOffsetFix<1,0,UNSIGNED> id414out_value;

  HWOffsetFix<11,0,UNSIGNED> id415out_value;

  HWOffsetFix<52,0,UNSIGNED> id417out_value;

  HWFloat<11,53> id2803out_value;

  HWFloat<11,53> id3606out_value;

  HWFloat<11,53> id3605out_value;

  HWFloat<11,53> id3180out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3604out_value;

  HWOffsetFix<1,0,UNSIGNED> id2892out_result[2];

  HWFloat<11,53> id3603out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id499out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3034out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3599out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3596out_value;

  HWOffsetFix<1,0,UNSIGNED> id509out_value;

  HWOffsetFix<53,-52,UNSIGNED> id493out_value;

  HWOffsetFix<1,0,UNSIGNED> id518out_value;

  HWOffsetFix<11,0,UNSIGNED> id519out_value;

  HWOffsetFix<52,0,UNSIGNED> id521out_value;

  HWFloat<11,53> id2804out_value;

  HWFloat<11,53> id3595out_value;

  HWFloat<11,53> id3594out_value;

  HWFloat<11,53> id3593out_value;

  HWFloat<11,53> id3592out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id600out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3036out_value;

  HWOffsetFix<53,-52,UNSIGNED> id3588out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3585out_value;

  HWOffsetFix<1,0,UNSIGNED> id610out_value;

  HWOffsetFix<53,-52,UNSIGNED> id594out_value;

  HWOffsetFix<1,0,UNSIGNED> id619out_value;

  HWOffsetFix<11,0,UNSIGNED> id620out_value;

  HWOffsetFix<52,0,UNSIGNED> id622out_value;

  HWFloat<11,53> id2805out_value;

  HWFloat<11,53> id3584out_value;

  HWFloat<11,53> id3225out_output[2];

  HWFloat<11,53> id28out_m_in;

  HWFloat<11,53> id29out_result[2];

  HWFloat<11,53> id3237out_output[9];

  HWFloat<11,53> id3602out_value;

  HWFloat<11,53> id3601out_value;

  HWRawBits<11> id530out_value;

  HWRawBits<52> id3600out_value;

  HWOffsetFix<1,0,UNSIGNED> id3236out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id453out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id455out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id455out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id458out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3227out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id545out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id545sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id542out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id542sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id539out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id539sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id476out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3228out_output[3];

  HWFloat<11,53> id3598out_value;

  HWOffsetFix<1,0,UNSIGNED> id3230out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3231out_output[3];

  HWFloat<11,53> id3597out_value;

  HWOffsetFix<1,0,UNSIGNED> id3232out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3233out_output[3];

  HWFloat<11,53> id3591out_value;

  HWFloat<11,53> id3590out_value;

  HWRawBits<11> id631out_value;

  HWRawBits<52> id3589out_value;

  HWOffsetFix<1,0,UNSIGNED> id3247out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id554out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id556out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id556out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id559out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3238out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id646out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id646sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id643out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id643sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id640out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id640sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id577out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3239out_output[3];

  HWFloat<11,53> id3587out_value;

  HWOffsetFix<1,0,UNSIGNED> id3241out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3242out_output[3];

  HWFloat<11,53> id3586out_value;

  HWOffsetFix<1,0,UNSIGNED> id3243out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3244out_output[3];

  HWFloat<11,53> id3092out_floatOut[2];

  HWOffsetFix<32,0,UNSIGNED> id3583out_value;

  HWOffsetFix<1,0,UNSIGNED> id2907out_result[2];

  HWFloat<11,53> id36out_h_in;

  HWFloat<11,53> id37out_result[2];

  HWFloat<11,53> id3262out_output[10];

  HWFloat<11,53> id3581out_value;

  HWFloat<11,53> id3093out_floatOut[2];

  HWRawBits<11> id731out_value;

  HWRawBits<52> id3580out_value;

  HWOffsetFix<1,0,UNSIGNED> id3261out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id654out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id656out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id656out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id659out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3252out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id746out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id746sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id743out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id743sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id740out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id740sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id677out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3253out_output[3];

  HWFloat<11,53> id3578out_value;

  HWOffsetFix<1,0,UNSIGNED> id3255out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3256out_output[3];

  HWFloat<11,53> id3577out_value;

  HWOffsetFix<1,0,UNSIGNED> id3257out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3258out_output[3];

  HWFloat<11,53> id3572out_value;

  HWFloat<11,53> id3571out_value;

  HWRawBits<11> id831out_value;

  HWRawBits<52> id3570out_value;

  HWOffsetFix<1,0,UNSIGNED> id3273out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id754out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id756out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id756out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id759out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3264out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id846out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id846sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id843out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id843sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id840out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id840sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id777out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3265out_output[3];

  HWFloat<11,53> id3568out_value;

  HWOffsetFix<1,0,UNSIGNED> id3267out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3268out_output[3];

  HWFloat<11,53> id3567out_value;

  HWOffsetFix<1,0,UNSIGNED> id3269out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3270out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3563out_value;

  HWOffsetFix<1,0,UNSIGNED> id2922out_result[2];

  HWFloat<11,53> id44out_j_in;

  HWFloat<11,53> id45out_result[2];

  HWFloat<11,53> id3297out_output[10];

  HWFloat<11,53> id3561out_value;

  HWFloat<11,53> id3094out_floatOut[2];

  HWRawBits<11> id933out_value;

  HWRawBits<52> id3560out_value;

  HWOffsetFix<1,0,UNSIGNED> id3285out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id856out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id858out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id858out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id861out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3276out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id948out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id948sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id945out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id945sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id942out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id942sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id879out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3277out_output[3];

  HWFloat<11,53> id3558out_value;

  HWOffsetFix<1,0,UNSIGNED> id3279out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3280out_output[3];

  HWFloat<11,53> id3557out_value;

  HWOffsetFix<1,0,UNSIGNED> id3281out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3282out_output[3];

  HWFloat<11,53> id3554out_value;

  HWFloat<11,53> id3553out_value;

  HWRawBits<11> id1032out_value;

  HWRawBits<52> id3552out_value;

  HWOffsetFix<1,0,UNSIGNED> id3296out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id955out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id957out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id957out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id960out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3287out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id1047out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1047sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1044out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1044sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1041out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1041sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id978out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3288out_output[3];

  HWFloat<11,53> id3550out_value;

  HWOffsetFix<1,0,UNSIGNED> id3290out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3291out_output[3];

  HWFloat<11,53> id3549out_value;

  HWOffsetFix<1,0,UNSIGNED> id3292out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3293out_output[3];

  HWFloat<11,53> id3543out_value;

  HWFloat<11,53> id3542out_value;

  HWRawBits<11> id1133out_value;

  HWRawBits<52> id3541out_value;

  HWOffsetFix<1,0,UNSIGNED> id3308out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1056out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1058out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1058out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1061out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3299out_output[3];

  HWOffsetFix<38,-53,UNSIGNED> id1148out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1148sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1145out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1145sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1142out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1142sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1079out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3300out_output[3];

  HWFloat<11,53> id3539out_value;

  HWOffsetFix<1,0,UNSIGNED> id3302out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3303out_output[3];

  HWFloat<11,53> id3538out_value;

  HWOffsetFix<1,0,UNSIGNED> id3304out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3305out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3533out_value;

  HWOffsetFix<1,0,UNSIGNED> id2944out_result[2];

  HWFloat<11,53> id40out_d_in;

  HWFloat<11,53> id41out_result[2];

  HWFloat<11,53> id3333out_output[9];

  HWFloat<11,53> id3532out_value;

  HWFloat<11,53> id3531out_value;

  HWFloat<11,53> id3530out_value;

  HWRawBits<11> id1235out_value;

  HWRawBits<52> id3529out_value;

  HWOffsetFix<1,0,UNSIGNED> id3322out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1158out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1160out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1160out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1163out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3313out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1204out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3048out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1250out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1250sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1247out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1247sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1244out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1244sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1181out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3314out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3528out_value;

  HWFloat<11,53> id3527out_value;

  HWOffsetFix<1,0,UNSIGNED> id3316out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3317out_output[3];

  HWFloat<11,53> id3526out_value;

  HWOffsetFix<1,0,UNSIGNED> id3318out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3319out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3525out_value;

  HWOffsetFix<1,0,UNSIGNED> id1214out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1198out_value;

  HWOffsetFix<1,0,UNSIGNED> id1223out_value;

  HWOffsetFix<11,0,UNSIGNED> id1224out_value;

  HWOffsetFix<52,0,UNSIGNED> id1226out_value;

  HWFloat<11,53> id2811out_value;

  HWFloat<11,53> id3524out_value;

  HWFloat<11,53> id3523out_value;

  HWFloat<11,53> id3522out_value;

  HWRawBits<11> id1334out_value;

  HWRawBits<52> id3521out_value;

  HWOffsetFix<1,0,UNSIGNED> id3332out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1257out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1259out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1259out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1262out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3323out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1303out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3050out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1349out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1349sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1346out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1346sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1343out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1343sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1280out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3324out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3520out_value;

  HWFloat<11,53> id3519out_value;

  HWOffsetFix<1,0,UNSIGNED> id3326out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3327out_output[3];

  HWFloat<11,53> id3518out_value;

  HWOffsetFix<1,0,UNSIGNED> id3328out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3329out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3517out_value;

  HWOffsetFix<1,0,UNSIGNED> id1313out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1297out_value;

  HWOffsetFix<1,0,UNSIGNED> id1322out_value;

  HWOffsetFix<11,0,UNSIGNED> id1323out_value;

  HWOffsetFix<52,0,UNSIGNED> id1325out_value;

  HWFloat<11,53> id2812out_value;

  HWFloat<11,53> id3516out_value;

  HWFloat<11,53> id3515out_value;

  HWFloat<11,53> id3514out_value;

  HWFloat<11,53> id3513out_value;

  HWFloat<11,53> id3512out_value;

  HWFloat<11,53> id3511out_value;

  HWRawBits<11> id1435out_value;

  HWRawBits<52> id3510out_value;

  HWOffsetFix<1,0,UNSIGNED> id3343out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1358out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1360out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1360out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1363out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3334out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1404out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3052out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1450out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1450sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1447out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1447sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1444out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1444sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1381out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3335out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3509out_value;

  HWFloat<11,53> id3508out_value;

  HWOffsetFix<1,0,UNSIGNED> id3337out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3338out_output[3];

  HWFloat<11,53> id3507out_value;

  HWOffsetFix<1,0,UNSIGNED> id3339out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3340out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3506out_value;

  HWOffsetFix<1,0,UNSIGNED> id1414out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1398out_value;

  HWOffsetFix<1,0,UNSIGNED> id1423out_value;

  HWOffsetFix<11,0,UNSIGNED> id1424out_value;

  HWOffsetFix<52,0,UNSIGNED> id1426out_value;

  HWFloat<11,53> id2813out_value;

  HWFloat<11,53> id3505out_value;

  HWFloat<11,53> id3504out_value;

  HWFloat<11,53> id3503out_value;

  HWRawBits<11> id1534out_value;

  HWRawBits<52> id3502out_value;

  HWOffsetFix<1,0,UNSIGNED> id3353out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1457out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1459out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1459out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1462out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3344out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1503out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3054out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1549out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1549sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1546out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1546sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1543out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1543sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1480out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3345out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3501out_value;

  HWFloat<11,53> id3500out_value;

  HWOffsetFix<1,0,UNSIGNED> id3347out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3348out_output[3];

  HWFloat<11,53> id3499out_value;

  HWOffsetFix<1,0,UNSIGNED> id3349out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3350out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3498out_value;

  HWOffsetFix<1,0,UNSIGNED> id1513out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1497out_value;

  HWOffsetFix<1,0,UNSIGNED> id1522out_value;

  HWOffsetFix<11,0,UNSIGNED> id1523out_value;

  HWOffsetFix<52,0,UNSIGNED> id1525out_value;

  HWFloat<11,53> id2814out_value;

  HWFloat<11,53> id3497out_value;

  HWFloat<11,53> id3496out_value;

  HWFloat<11,53> id3312out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3495out_value;

  HWOffsetFix<1,0,UNSIGNED> id2973out_result[2];

  HWFloat<11,53> id32out_f_in;

  HWFloat<11,53> id33out_result[2];

  HWFloat<11,53> id3378out_output[9];

  HWFloat<11,53> id3494out_value;

  HWFloat<11,53> id3493out_value;

  HWFloat<11,53> id3492out_value;

  HWRawBits<11> id1635out_value;

  HWRawBits<52> id3491out_value;

  HWOffsetFix<1,0,UNSIGNED> id3367out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1558out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1560out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1560out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1563out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3358out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1604out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3056out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1650out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1650sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1647out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1647sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1644out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1644sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1581out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3359out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3490out_value;

  HWFloat<11,53> id3489out_value;

  HWOffsetFix<1,0,UNSIGNED> id3361out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3362out_output[3];

  HWFloat<11,53> id3488out_value;

  HWOffsetFix<1,0,UNSIGNED> id3363out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3364out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3487out_value;

  HWOffsetFix<1,0,UNSIGNED> id1614out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1598out_value;

  HWOffsetFix<1,0,UNSIGNED> id1623out_value;

  HWOffsetFix<11,0,UNSIGNED> id1624out_value;

  HWOffsetFix<52,0,UNSIGNED> id1626out_value;

  HWFloat<11,53> id2815out_value;

  HWFloat<11,53> id3486out_value;

  HWFloat<11,53> id3485out_value;

  HWFloat<11,53> id3484out_value;

  HWRawBits<11> id1734out_value;

  HWRawBits<52> id3483out_value;

  HWOffsetFix<1,0,UNSIGNED> id3377out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1657out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1659out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1659out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1662out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3368out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1703out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3058out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1749out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1749sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1746out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1746sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1743out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1743sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1680out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3369out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3482out_value;

  HWFloat<11,53> id3481out_value;

  HWOffsetFix<1,0,UNSIGNED> id3371out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3372out_output[3];

  HWFloat<11,53> id3480out_value;

  HWOffsetFix<1,0,UNSIGNED> id3373out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3374out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3479out_value;

  HWOffsetFix<1,0,UNSIGNED> id1713out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1697out_value;

  HWOffsetFix<1,0,UNSIGNED> id1722out_value;

  HWOffsetFix<11,0,UNSIGNED> id1723out_value;

  HWOffsetFix<52,0,UNSIGNED> id1725out_value;

  HWFloat<11,53> id2816out_value;

  HWFloat<11,53> id3478out_value;

  HWFloat<11,53> id3477out_value;

  HWFloat<11,53> id3476out_value;

  HWFloat<11,53> id3475out_value;

  HWFloat<11,53> id3474out_value;

  HWFloat<11,53> id3473out_value;

  HWRawBits<11> id1835out_value;

  HWRawBits<52> id3472out_value;

  HWOffsetFix<1,0,UNSIGNED> id3388out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1758out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1760out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1760out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1763out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3379out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1804out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3060out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1850out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1850sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1847out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1847sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1844out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1844sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1781out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3380out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3471out_value;

  HWFloat<11,53> id3470out_value;

  HWOffsetFix<1,0,UNSIGNED> id3382out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3383out_output[3];

  HWFloat<11,53> id3469out_value;

  HWOffsetFix<1,0,UNSIGNED> id3384out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3385out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3468out_value;

  HWOffsetFix<1,0,UNSIGNED> id1814out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1798out_value;

  HWOffsetFix<1,0,UNSIGNED> id1823out_value;

  HWOffsetFix<11,0,UNSIGNED> id1824out_value;

  HWOffsetFix<52,0,UNSIGNED> id1826out_value;

  HWFloat<11,53> id2817out_value;

  HWFloat<11,53> id3467out_value;

  HWFloat<11,53> id3466out_value;

  HWFloat<11,53> id3465out_value;

  HWRawBits<11> id1934out_value;

  HWRawBits<52> id3464out_value;

  HWOffsetFix<1,0,UNSIGNED> id3398out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1857out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id1859out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1859out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id1862out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id3389out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1903out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3062out_value;

  HWOffsetFix<38,-53,UNSIGNED> id1949out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id1949sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id1946out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id1946sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id1943out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id1943sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id1880out_value;

  HWOffsetFix<26,-51,UNSIGNED> id3390out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id3463out_value;

  HWFloat<11,53> id3462out_value;

  HWOffsetFix<1,0,UNSIGNED> id3392out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3393out_output[3];

  HWFloat<11,53> id3461out_value;

  HWOffsetFix<1,0,UNSIGNED> id3394out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3395out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3460out_value;

  HWOffsetFix<1,0,UNSIGNED> id1913out_value;

  HWOffsetFix<53,-52,UNSIGNED> id1897out_value;

  HWOffsetFix<1,0,UNSIGNED> id1922out_value;

  HWOffsetFix<11,0,UNSIGNED> id1923out_value;

  HWOffsetFix<52,0,UNSIGNED> id1925out_value;

  HWFloat<11,53> id2818out_value;

  HWFloat<11,53> id3459out_value;

  HWFloat<11,53> id3458out_value;

  HWFloat<11,53> id3357out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3455out_value;

  HWOffsetFix<1,0,UNSIGNED> id3002out_result[2];

  HWFloat<11,53> id3454out_value;

  HWFloat<11,53> id3453out_value;

  HWFloat<11,53> id3452out_value;

  HWFloat<11,53> id20out_ca_in;

  HWFloat<11,53> id21out_result[2];

  HWFloat<11,53> id3410out_output[8];

  HWFloat<11,53> id3433out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id1992out_value;

  HWOffsetFix<1,0,UNSIGNED> id3447out_value;

  HWOffsetFix<1,0,UNSIGNED> id3411out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id2000out_value;

  HWRawBits<212> id2773out_dout[3];

  HWRawBits<212> id2773sta_rom_store[2048];

  HWOffsetFix<41,-41,UNSIGNED> id3412out_output[3];

  HWOffsetFix<11,0,UNSIGNED> id3443out_value;

  HWOffsetFix<11,0,UNSIGNED> id2719out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3006out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2721out_io_ca_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3418out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3442out_value;

  HWOffsetFix<11,0,UNSIGNED> id2727out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3007out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2729out_io_x1_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3420out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3441out_value;

  HWOffsetFix<11,0,UNSIGNED> id2735out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3008out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2737out_io_m_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3422out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3440out_value;

  HWOffsetFix<11,0,UNSIGNED> id2743out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3009out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2745out_io_f_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3424out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3439out_value;

  HWOffsetFix<11,0,UNSIGNED> id2751out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3010out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2753out_io_h_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3426out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3438out_value;

  HWOffsetFix<11,0,UNSIGNED> id2759out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3011out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2761out_io_d_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3428out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3437out_value;

  HWOffsetFix<11,0,UNSIGNED> id2767out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id3012out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2769out_io_j_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3430out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id2786out_value;

  HWOffsetFix<1,0,UNSIGNED> id3436out_value;

  HWOffsetFix<49,0,UNSIGNED> id2783out_value;

  HWOffsetFix<48,0,UNSIGNED> id2784out_count;
  HWOffsetFix<1,0,UNSIGNED> id2784out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2784st_count;

  HWOffsetFix<1,0,UNSIGNED> id3435out_value;

  HWOffsetFix<49,0,UNSIGNED> id2789out_value;

  HWOffsetFix<48,0,UNSIGNED> id2790out_count;
  HWOffsetFix<1,0,UNSIGNED> id2790out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2790st_count;

  HWOffsetFix<48,0,UNSIGNED> id2792out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id3013out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_1;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<52,0,UNSIGNED> c_hw_fix_52_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_1;
  const HWFloat<11,53> c_hw_flt_11_53_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_4;
  const HWFloat<11,53> c_hw_flt_11_53_bits_5;
  const HWFloat<11,53> c_hw_flt_11_53_bits_6;
  const HWFloat<11,53> c_hw_flt_11_53_bits_7;
  const HWFloat<11,53> c_hw_flt_11_53_bits_8;
  const HWFloat<11,53> c_hw_flt_11_53_bits_9;
  const HWFloat<11,53> c_hw_flt_11_53_bits_10;
  const HWFloat<11,53> c_hw_flt_11_53_bits_11;
  const HWFloat<11,53> c_hw_flt_11_53_bits_12;
  const HWFloat<11,53> c_hw_flt_11_53_bits_13;
  const HWFloat<11,53> c_hw_flt_11_53_bits_14;
  const HWFloat<11,53> c_hw_flt_11_53_bits_15;
  const HWFloat<11,53> c_hw_flt_11_53_bits_16;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<52> c_hw_bit_52_bits;
  const HWRawBits<63> c_hw_bit_63_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits_1;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits_2;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits_17;
  const HWFloat<11,53> c_hw_flt_11_53_bits_18;
  const HWFloat<11,53> c_hw_flt_11_53_bits_19;
  const HWFloat<11,53> c_hw_flt_11_53_bits_20;
  const HWFloat<11,53> c_hw_flt_11_53_bits_21;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<38,-53,UNSIGNED> c_hw_fix_38_n53_uns_undef;
  const HWOffsetFix<48,-53,UNSIGNED> c_hw_fix_48_n53_uns_undef;
  const HWOffsetFix<53,-53,UNSIGNED> c_hw_fix_53_n53_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_undef;
  const HWFloat<11,53> c_hw_flt_11_53_4_0val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_22;
  const HWFloat<11,53> c_hw_flt_11_53_bits_23;
  const HWFloat<11,53> c_hw_flt_11_53_bits_24;
  const HWFloat<11,53> c_hw_flt_11_53_bits_25;
  const HWFloat<11,53> c_hw_flt_11_53_bits_26;
  const HWFloat<11,53> c_hw_flt_11_53_bits_27;
  const HWFloat<11,53> c_hw_flt_11_53_bits_28;
  const HWFloat<11,53> c_hw_flt_11_53_bits_29;
  const HWFloat<11,53> c_hw_flt_11_53_bits_30;
  const HWFloat<11,53> c_hw_flt_11_53_bits_31;
  const HWFloat<11,53> c_hw_flt_11_53_bits_32;
  const HWFloat<11,53> c_hw_flt_11_53_bits_33;
  const HWFloat<11,53> c_hw_flt_11_53_bits_34;
  const HWFloat<11,53> c_hw_flt_11_53_bits_35;
  const HWFloat<11,53> c_hw_flt_11_53_bits_36;
  const HWFloat<11,53> c_hw_flt_11_53_bits_37;
  const HWFloat<11,53> c_hw_flt_11_53_n0_25val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_38;
  const HWFloat<11,53> c_hw_flt_11_53_bits_39;
  const HWFloat<11,53> c_hw_flt_11_53_bits_40;
  const HWFloat<11,53> c_hw_flt_11_53_bits_41;
  const HWFloat<11,53> c_hw_flt_11_53_bits_42;
  const HWFloat<11,53> c_hw_flt_11_53_bits_43;
  const HWFloat<11,53> c_hw_flt_11_53_bits_44;
  const HWFloat<11,53> c_hw_flt_11_53_bits_45;
  const HWFloat<11,53> c_hw_flt_11_53_bits_46;
  const HWFloat<11,53> c_hw_flt_11_53_bits_47;
  const HWFloat<11,53> c_hw_flt_11_53_bits_48;
  const HWFloat<11,53> c_hw_flt_11_53_bits_49;
  const HWFloat<11,53> c_hw_flt_11_53_bits_50;
  const HWFloat<11,53> c_hw_flt_11_53_bits_51;
  const HWFloat<11,53> c_hw_flt_11_53_bits_52;
  const HWFloat<11,53> c_hw_flt_11_53_bits_53;
  const HWFloat<11,53> c_hw_flt_11_53_bits_54;
  const HWFloat<11,53> c_hw_flt_11_53_bits_55;
  const HWFloat<11,53> c_hw_flt_11_53_bits_56;
  const HWFloat<11,53> c_hw_flt_11_53_bits_57;
  const HWFloat<11,53> c_hw_flt_11_53_bits_58;
  const HWFloat<11,53> c_hw_flt_11_53_bits_59;
  const HWRawBits<212> c_hw_bit_212_undef;
  const HWOffsetFix<41,-41,UNSIGNED> c_hw_fix_41_n41_uns_undef;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* BEELERREUTERDFEKERNEL_H_ */
