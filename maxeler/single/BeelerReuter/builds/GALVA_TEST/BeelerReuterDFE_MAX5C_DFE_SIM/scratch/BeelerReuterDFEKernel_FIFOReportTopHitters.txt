Top 10 largest FIFOs
     Costs                                              From                                                To
         6                        BeelerReuterKernel.maxj:78                       BeelerReuterKernel.maxj:104
         6                        BeelerReuterKernel.maxj:76                       BeelerReuterKernel.maxj:103
         6                        BeelerReuterKernel.maxj:80                       BeelerReuterKernel.maxj:105
         6                        BeelerReuterKernel.maxj:75                       BeelerReuterKernel.maxj:102
         6                        BeelerReuterKernel.maxj:79                       BeelerReuterKernel.maxj:106
         6                        BeelerReuterKernel.maxj:77                       BeelerReuterKernel.maxj:107
         4                        BeelerReuterKernel.maxj:74                       BeelerReuterKernel.maxj:110
         2                        BeelerReuterKernel.maxj:73                       BeelerReuterKernel.maxj:111
         2                       BeelerReuterKernel.maxj:118                       BeelerReuterKernel.maxj:118
         2                       BeelerReuterKernel.maxj:114                       BeelerReuterKernel.maxj:114

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
         6                       BeelerReuterKernel.maxj:105         1
         6                       BeelerReuterKernel.maxj:104         1
         6                       BeelerReuterKernel.maxj:107         1
         6                       BeelerReuterKernel.maxj:106         1
         6                       BeelerReuterKernel.maxj:103         1
         6                       BeelerReuterKernel.maxj:102         1
         4                        BeelerReuterKernel.maxj:87         3
         4                       BeelerReuterKernel.maxj:110         1
         2                        BeelerReuterKernel.maxj:94         2
         2                        BeelerReuterKernel.maxj:84         2

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
         6                        BeelerReuterKernel.maxj:75         1
         6                        BeelerReuterKernel.maxj:76         1
         6                        BeelerReuterKernel.maxj:77         1
         6                        BeelerReuterKernel.maxj:78         1
         6                        BeelerReuterKernel.maxj:79         1
         6                        BeelerReuterKernel.maxj:80         1
         4                        BeelerReuterKernel.maxj:73         2
         4                        BeelerReuterKernel.maxj:74         1
         2                        BeelerReuterKernel.maxj:94         2
         2                        BeelerReuterKernel.maxj:84         2
