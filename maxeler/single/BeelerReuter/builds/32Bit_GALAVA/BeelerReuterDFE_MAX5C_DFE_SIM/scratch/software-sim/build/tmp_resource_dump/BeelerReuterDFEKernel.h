#ifndef BEELERREUTERDFEKERNEL_H_
#define BEELERREUTERDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class BeelerReuterDFEKernel : public KernelManagerBlockSync {
public:
  BeelerReuterDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_ca_out;
  t_port_number m_x1_out;
  t_port_number m_m_out;
  t_port_number m_f_out;
  t_port_number m_h_out;
  t_port_number m_d_out;
  t_port_number m_j_out;
  HWOffsetFix<1,0,UNSIGNED> id8out_value;

  HWOffsetFix<11,0,UNSIGNED> id3234out_value;

  HWOffsetFix<11,0,UNSIGNED> id11out_count;
  HWOffsetFix<1,0,UNSIGNED> id11out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id11st_count;

  HWOffsetFix<11,0,UNSIGNED> id2912out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id3499out_value;

  HWOffsetFix<11,0,UNSIGNED> id2511out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2619out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2913out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id2513out_io_u_out_force_disabled;

  HWOffsetFix<32,0,UNSIGNED> id9out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id10out_count;
  HWOffsetFix<1,0,UNSIGNED> id10out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id10st_count;

  HWOffsetFix<32,0,UNSIGNED> id3498out_value;

  HWOffsetFix<1,0,UNSIGNED> id2620out_result[2];

  HWFloat<8,24> id12out_dt;

  HWFloat<8,24> id3497out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1971out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2816out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3484out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3481out_value;

  HWOffsetFix<1,0,UNSIGNED> id1981out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1965out_value;

  HWOffsetFix<1,0,UNSIGNED> id1990out_value;

  HWOffsetFix<8,0,UNSIGNED> id1991out_value;

  HWOffsetFix<23,0,UNSIGNED> id1993out_value;

  HWFloat<8,24> id2595out_value;

  HWFloat<8,24> id3480out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2060out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2818out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3476out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3473out_value;

  HWOffsetFix<1,0,UNSIGNED> id2070out_value;

  HWOffsetFix<24,-23,UNSIGNED> id2054out_value;

  HWOffsetFix<1,0,UNSIGNED> id2079out_value;

  HWOffsetFix<8,0,UNSIGNED> id2080out_value;

  HWOffsetFix<23,0,UNSIGNED> id2082out_value;

  HWFloat<8,24> id2596out_value;

  HWFloat<8,24> id3472out_value;

  HWFloat<8,24> id5out_value;

  HWFloat<8,24> id2197out_value;

  HWFloat<8,24> id3471out_value;

  HWFloat<8,24> id3470out_value;

  HWFloat<8,24> id3469out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2150out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2820out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3465out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3462out_value;

  HWOffsetFix<1,0,UNSIGNED> id2160out_value;

  HWOffsetFix<24,-23,UNSIGNED> id2144out_value;

  HWOffsetFix<1,0,UNSIGNED> id2169out_value;

  HWOffsetFix<8,0,UNSIGNED> id2170out_value;

  HWOffsetFix<23,0,UNSIGNED> id2172out_value;

  HWFloat<8,24> id2597out_value;

  HWFloat<8,24> id3461out_value;

  HWFloat<8,24> id2200out_value;

  HWFloat<8,24> id3460out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2349out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2822out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3456out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3453out_value;

  HWOffsetFix<1,0,UNSIGNED> id2359out_value;

  HWOffsetFix<24,-23,UNSIGNED> id2343out_value;

  HWOffsetFix<1,0,UNSIGNED> id2368out_value;

  HWOffsetFix<8,0,UNSIGNED> id2369out_value;

  HWOffsetFix<23,0,UNSIGNED> id2371out_value;

  HWFloat<8,24> id2598out_value;

  HWFloat<8,24> id3452out_value;

  HWFloat<8,24> id3451out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2442out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2824out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3447out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3444out_value;

  HWOffsetFix<1,0,UNSIGNED> id2452out_value;

  HWOffsetFix<24,-23,UNSIGNED> id2436out_value;

  HWOffsetFix<1,0,UNSIGNED> id2461out_value;

  HWOffsetFix<8,0,UNSIGNED> id2462out_value;

  HWOffsetFix<23,0,UNSIGNED> id2464out_value;

  HWFloat<8,24> id2599out_value;

  HWFloat<8,24> id3443out_value;

  HWFloat<8,24> id3382out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id647out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2838out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3379out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3376out_value;

  HWOffsetFix<1,0,UNSIGNED> id657out_value;

  HWOffsetFix<24,-23,UNSIGNED> id641out_value;

  HWOffsetFix<1,0,UNSIGNED> id666out_value;

  HWOffsetFix<8,0,UNSIGNED> id667out_value;

  HWOffsetFix<23,0,UNSIGNED> id669out_value;

  HWFloat<8,24> id2606out_value;

  HWFloat<8,24> id3375out_value;

  HWFloat<8,24> id3374out_value;

  HWFloat<8,24> id3373out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id739out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2840out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3369out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3366out_value;

  HWOffsetFix<1,0,UNSIGNED> id749out_value;

  HWOffsetFix<24,-23,UNSIGNED> id733out_value;

  HWOffsetFix<1,0,UNSIGNED> id758out_value;

  HWOffsetFix<8,0,UNSIGNED> id759out_value;

  HWOffsetFix<23,0,UNSIGNED> id761out_value;

  HWFloat<8,24> id2607out_value;

  HWFloat<8,24> id3365out_value;

  HWFloat<8,24> id3364out_value;

  HWFloat<8,24> id3362out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id833out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2842out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3359out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3356out_value;

  HWOffsetFix<1,0,UNSIGNED> id843out_value;

  HWOffsetFix<24,-23,UNSIGNED> id827out_value;

  HWOffsetFix<1,0,UNSIGNED> id852out_value;

  HWOffsetFix<8,0,UNSIGNED> id853out_value;

  HWOffsetFix<23,0,UNSIGNED> id855out_value;

  HWFloat<8,24> id2608out_value;

  HWFloat<8,24> id3355out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id924out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2844out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3351out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3348out_value;

  HWOffsetFix<1,0,UNSIGNED> id934out_value;

  HWOffsetFix<24,-23,UNSIGNED> id918out_value;

  HWOffsetFix<1,0,UNSIGNED> id943out_value;

  HWOffsetFix<8,0,UNSIGNED> id944out_value;

  HWOffsetFix<23,0,UNSIGNED> id946out_value;

  HWFloat<8,24> id2609out_value;

  HWFloat<8,24> id3347out_value;

  HWFloat<8,24> id3346out_value;

  HWFloat<8,24> id3345out_value;

  HWFloat<8,24> id3344out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1017out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2846out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3340out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3337out_value;

  HWOffsetFix<1,0,UNSIGNED> id1027out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1011out_value;

  HWOffsetFix<1,0,UNSIGNED> id1036out_value;

  HWOffsetFix<8,0,UNSIGNED> id1037out_value;

  HWOffsetFix<23,0,UNSIGNED> id1039out_value;

  HWFloat<8,24> id2610out_value;

  HWFloat<8,24> id3336out_value;

  HWFloat<8,24> id3335out_value;

  HWFloat<8,24> id3out_value;

  HWFloat<8,24> id2out_value;

  HWFloat<8,24> id3334out_value;

  HWFloat<8,24> id3257out_value;

  HWFloat<8,24> id3256out_value;

  HWFloat<8,24> id3251out_value;

  HWRawBits<8> id1882out_value;

  HWRawBits<23> id3250out_value;

  HWFloat<8,24> id3249out_value;

  HWRawBits<31> id1892out_value;

  HWOffsetFix<1,0,UNSIGNED> id1853out_value;

  HWOffsetFix<9,0,TWOSCOMPLEMENT> id3248out_value;

  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1858out_value;

  HWFloat<8,24> id3246out_value;

  HWFloat<8,24> id3245out_value;

  HWFloat<8,24> id3244out_value;

  HWFloat<8,24> id1905out_value;

  HWFloat<8,24> id16out_u_in;

  HWFloat<8,24> id17out_result[2];

  HWFloat<8,24> id2924out_output[2];

  HWFloat<8,24> id3231out_output[8];

  HWFloat<8,24> id3232out_output[2];

  HWFloat<8,24> id3496out_value;

  HWFloat<8,24> id3495out_value;

  HWRawBits<8> id2278out_value;

  HWRawBits<23> id3494out_value;

  HWOffsetFix<1,0,UNSIGNED> id2923out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2206out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2208out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2208out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id2211out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2914out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2247out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2814out_value;

  HWOffsetFix<22,-24,UNSIGNED> id2290out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2290sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2287out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2287sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id2228out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2915out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3493out_value;

  HWFloat<8,24> id3492out_value;

  HWOffsetFix<1,0,UNSIGNED> id2917out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2918out_output[3];

  HWFloat<8,24> id3491out_value;

  HWOffsetFix<1,0,UNSIGNED> id2919out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2920out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3490out_value;

  HWOffsetFix<1,0,UNSIGNED> id2257out_value;

  HWOffsetFix<24,-23,UNSIGNED> id2241out_value;

  HWOffsetFix<1,0,UNSIGNED> id2266out_value;

  HWOffsetFix<8,0,UNSIGNED> id2267out_value;

  HWOffsetFix<23,0,UNSIGNED> id2269out_value;

  HWFloat<8,24> id2594out_value;

  HWFloat<8,24> id3489out_value;

  HWFloat<8,24> id3488out_value;

  HWFloat<8,24> id2891out_floatOut[2];

  HWFloat<8,24> id3487out_value;

  HWFloat<8,24> id3486out_value;

  HWRawBits<8> id2002out_value;

  HWRawBits<23> id3485out_value;

  HWOffsetFix<1,0,UNSIGNED> id2934out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1930out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1932out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1932out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1935out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2925out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id2014out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2014sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2011out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2011sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1952out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2926out_output[3];

  HWFloat<8,24> id3483out_value;

  HWOffsetFix<1,0,UNSIGNED> id2928out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2929out_output[3];

  HWFloat<8,24> id3482out_value;

  HWOffsetFix<1,0,UNSIGNED> id2930out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2931out_output[3];

  HWFloat<8,24> id3479out_value;

  HWFloat<8,24> id3478out_value;

  HWRawBits<8> id2091out_value;

  HWRawBits<23> id3477out_value;

  HWOffsetFix<1,0,UNSIGNED> id2945out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2019out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2021out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2021out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id2024out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2936out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id2103out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2103sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2100out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2100sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id2041out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2937out_output[3];

  HWFloat<8,24> id3475out_value;

  HWOffsetFix<1,0,UNSIGNED> id2939out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2940out_output[3];

  HWFloat<8,24> id3474out_value;

  HWOffsetFix<1,0,UNSIGNED> id2941out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2942out_output[3];

  HWFloat<8,24> id3468out_value;

  HWFloat<8,24> id3467out_value;

  HWRawBits<8> id2181out_value;

  HWRawBits<23> id3466out_value;

  HWOffsetFix<1,0,UNSIGNED> id2957out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2109out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2111out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2111out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id2114out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2948out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id2193out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2193sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2190out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2190sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id2131out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2949out_output[3];

  HWFloat<8,24> id3464out_value;

  HWOffsetFix<1,0,UNSIGNED> id2951out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2952out_output[3];

  HWFloat<8,24> id3463out_value;

  HWOffsetFix<1,0,UNSIGNED> id2953out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2954out_output[3];

  HWFloat<8,24> id3459out_value;

  HWFloat<8,24> id3458out_value;

  HWRawBits<8> id2380out_value;

  HWRawBits<23> id3457out_value;

  HWOffsetFix<1,0,UNSIGNED> id2968out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2308out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2310out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2310out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id2313out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2959out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id2392out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2392sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2389out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2389sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id2330out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2960out_output[3];

  HWFloat<8,24> id3455out_value;

  HWOffsetFix<1,0,UNSIGNED> id2962out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2963out_output[3];

  HWFloat<8,24> id3454out_value;

  HWOffsetFix<1,0,UNSIGNED> id2964out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2965out_output[3];

  HWFloat<8,24> id3450out_value;

  HWFloat<8,24> id3449out_value;

  HWRawBits<8> id2473out_value;

  HWRawBits<23> id3448out_value;

  HWOffsetFix<1,0,UNSIGNED> id2979out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2401out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2403out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2403out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id2406out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2970out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id2485out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id2485sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id2482out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id2482sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id2423out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2971out_output[3];

  HWFloat<8,24> id3446out_value;

  HWOffsetFix<1,0,UNSIGNED> id2973out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2974out_output[3];

  HWFloat<8,24> id3445out_value;

  HWOffsetFix<1,0,UNSIGNED> id2975out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2976out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3442out_value;

  HWOffsetFix<1,0,UNSIGNED> id2663out_result[2];

  HWFloat<8,24> id24out_x1_in;

  HWFloat<8,24> id25out_result[2];

  HWFloat<8,24> id3001out_output[9];

  HWFloat<8,24> id3441out_value;

  HWFloat<8,24> id3440out_value;

  HWFloat<8,24> id3439out_value;

  HWRawBits<8> id122out_value;

  HWRawBits<23> id3438out_value;

  HWOffsetFix<1,0,UNSIGNED> id2990out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id50out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id52out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id52out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id55out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2981out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id91out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2826out_value;

  HWOffsetFix<22,-24,UNSIGNED> id134out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id134sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id131out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id131sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id72out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2982out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3437out_value;

  HWFloat<8,24> id3436out_value;

  HWOffsetFix<1,0,UNSIGNED> id2984out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2985out_output[3];

  HWFloat<8,24> id3435out_value;

  HWOffsetFix<1,0,UNSIGNED> id2986out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2987out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3434out_value;

  HWOffsetFix<1,0,UNSIGNED> id101out_value;

  HWOffsetFix<24,-23,UNSIGNED> id85out_value;

  HWOffsetFix<1,0,UNSIGNED> id110out_value;

  HWOffsetFix<8,0,UNSIGNED> id111out_value;

  HWOffsetFix<23,0,UNSIGNED> id113out_value;

  HWFloat<8,24> id2600out_value;

  HWFloat<8,24> id3433out_value;

  HWFloat<8,24> id3432out_value;

  HWFloat<8,24> id3431out_value;

  HWRawBits<8> id213out_value;

  HWRawBits<23> id3430out_value;

  HWOffsetFix<1,0,UNSIGNED> id3000out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id141out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id143out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id143out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id146out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2991out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id182out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2828out_value;

  HWOffsetFix<22,-24,UNSIGNED> id225out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id225sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id222out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id222sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id163out_value;

  HWOffsetFix<14,-26,UNSIGNED> id2992out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3429out_value;

  HWFloat<8,24> id3428out_value;

  HWOffsetFix<1,0,UNSIGNED> id2994out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2995out_output[3];

  HWFloat<8,24> id3427out_value;

  HWOffsetFix<1,0,UNSIGNED> id2996out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2997out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3426out_value;

  HWOffsetFix<1,0,UNSIGNED> id192out_value;

  HWOffsetFix<24,-23,UNSIGNED> id176out_value;

  HWOffsetFix<1,0,UNSIGNED> id201out_value;

  HWOffsetFix<8,0,UNSIGNED> id202out_value;

  HWOffsetFix<23,0,UNSIGNED> id204out_value;

  HWFloat<8,24> id2601out_value;

  HWFloat<8,24> id3425out_value;

  HWFloat<8,24> id3424out_value;

  HWFloat<8,24> id3423out_value;

  HWFloat<8,24> id3422out_value;

  HWFloat<8,24> id3421out_value;

  HWFloat<8,24> id3420out_value;

  HWRawBits<8> id306out_value;

  HWRawBits<23> id3419out_value;

  HWOffsetFix<1,0,UNSIGNED> id3011out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id234out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id236out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id236out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id239out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3002out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id275out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2830out_value;

  HWOffsetFix<22,-24,UNSIGNED> id318out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id318sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id315out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id315sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id256out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3003out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3418out_value;

  HWFloat<8,24> id3417out_value;

  HWOffsetFix<1,0,UNSIGNED> id3005out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3006out_output[3];

  HWFloat<8,24> id3416out_value;

  HWOffsetFix<1,0,UNSIGNED> id3007out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3008out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3415out_value;

  HWOffsetFix<1,0,UNSIGNED> id285out_value;

  HWOffsetFix<24,-23,UNSIGNED> id269out_value;

  HWOffsetFix<1,0,UNSIGNED> id294out_value;

  HWOffsetFix<8,0,UNSIGNED> id295out_value;

  HWOffsetFix<23,0,UNSIGNED> id297out_value;

  HWFloat<8,24> id2602out_value;

  HWFloat<8,24> id3414out_value;

  HWFloat<8,24> id3413out_value;

  HWFloat<8,24> id3412out_value;

  HWRawBits<8> id397out_value;

  HWRawBits<23> id3411out_value;

  HWOffsetFix<1,0,UNSIGNED> id3021out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id325out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id327out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id327out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id330out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3012out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id366out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2832out_value;

  HWOffsetFix<22,-24,UNSIGNED> id409out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id409sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id406out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id406sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id347out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3013out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3410out_value;

  HWFloat<8,24> id3409out_value;

  HWOffsetFix<1,0,UNSIGNED> id3015out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3016out_output[3];

  HWFloat<8,24> id3408out_value;

  HWOffsetFix<1,0,UNSIGNED> id3017out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3018out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3407out_value;

  HWOffsetFix<1,0,UNSIGNED> id376out_value;

  HWOffsetFix<24,-23,UNSIGNED> id360out_value;

  HWOffsetFix<1,0,UNSIGNED> id385out_value;

  HWOffsetFix<8,0,UNSIGNED> id386out_value;

  HWOffsetFix<23,0,UNSIGNED> id388out_value;

  HWFloat<8,24> id2603out_value;

  HWFloat<8,24> id3406out_value;

  HWFloat<8,24> id3405out_value;

  HWFloat<8,24> id2980out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3404out_value;

  HWOffsetFix<1,0,UNSIGNED> id2692out_result[2];

  HWFloat<8,24> id3403out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id462out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2834out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3399out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3396out_value;

  HWOffsetFix<1,0,UNSIGNED> id472out_value;

  HWOffsetFix<24,-23,UNSIGNED> id456out_value;

  HWOffsetFix<1,0,UNSIGNED> id481out_value;

  HWOffsetFix<8,0,UNSIGNED> id482out_value;

  HWOffsetFix<23,0,UNSIGNED> id484out_value;

  HWFloat<8,24> id2604out_value;

  HWFloat<8,24> id3395out_value;

  HWFloat<8,24> id3394out_value;

  HWFloat<8,24> id3393out_value;

  HWFloat<8,24> id3392out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id555out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2836out_value;

  HWOffsetFix<24,-23,UNSIGNED> id3388out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3385out_value;

  HWOffsetFix<1,0,UNSIGNED> id565out_value;

  HWOffsetFix<24,-23,UNSIGNED> id549out_value;

  HWOffsetFix<1,0,UNSIGNED> id574out_value;

  HWOffsetFix<8,0,UNSIGNED> id575out_value;

  HWOffsetFix<23,0,UNSIGNED> id577out_value;

  HWFloat<8,24> id2605out_value;

  HWFloat<8,24> id3384out_value;

  HWFloat<8,24> id3025out_output[2];

  HWFloat<8,24> id28out_m_in;

  HWFloat<8,24> id29out_result[2];

  HWFloat<8,24> id3037out_output[9];

  HWFloat<8,24> id3402out_value;

  HWFloat<8,24> id3401out_value;

  HWRawBits<8> id493out_value;

  HWRawBits<23> id3400out_value;

  HWOffsetFix<1,0,UNSIGNED> id3036out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id421out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id423out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id423out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id426out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3027out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id505out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id505sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id502out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id502sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id443out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3028out_output[3];

  HWFloat<8,24> id3398out_value;

  HWOffsetFix<1,0,UNSIGNED> id3030out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3031out_output[3];

  HWFloat<8,24> id3397out_value;

  HWOffsetFix<1,0,UNSIGNED> id3032out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3033out_output[3];

  HWFloat<8,24> id3391out_value;

  HWFloat<8,24> id3390out_value;

  HWRawBits<8> id586out_value;

  HWRawBits<23> id3389out_value;

  HWOffsetFix<1,0,UNSIGNED> id3047out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id514out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id516out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id516out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id519out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3038out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id598out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id598sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id595out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id595sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id536out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3039out_output[3];

  HWFloat<8,24> id3387out_value;

  HWOffsetFix<1,0,UNSIGNED> id3041out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3042out_output[3];

  HWFloat<8,24> id3386out_value;

  HWOffsetFix<1,0,UNSIGNED> id3043out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3044out_output[3];

  HWFloat<8,24> id2892out_floatOut[2];

  HWOffsetFix<32,0,UNSIGNED> id3383out_value;

  HWOffsetFix<1,0,UNSIGNED> id2707out_result[2];

  HWFloat<8,24> id36out_h_in;

  HWFloat<8,24> id37out_result[2];

  HWFloat<8,24> id3062out_output[10];

  HWFloat<8,24> id3381out_value;

  HWFloat<8,24> id2893out_floatOut[2];

  HWRawBits<8> id678out_value;

  HWRawBits<23> id3380out_value;

  HWOffsetFix<1,0,UNSIGNED> id3061out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id606out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id608out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id608out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id611out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3052out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id690out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id690sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id687out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id687sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id628out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3053out_output[3];

  HWFloat<8,24> id3378out_value;

  HWOffsetFix<1,0,UNSIGNED> id3055out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3056out_output[3];

  HWFloat<8,24> id3377out_value;

  HWOffsetFix<1,0,UNSIGNED> id3057out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3058out_output[3];

  HWFloat<8,24> id3372out_value;

  HWFloat<8,24> id3371out_value;

  HWRawBits<8> id770out_value;

  HWRawBits<23> id3370out_value;

  HWOffsetFix<1,0,UNSIGNED> id3073out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id698out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id700out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id700out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id703out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3064out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id782out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id782sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id779out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id779sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id720out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3065out_output[3];

  HWFloat<8,24> id3368out_value;

  HWOffsetFix<1,0,UNSIGNED> id3067out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3068out_output[3];

  HWFloat<8,24> id3367out_value;

  HWOffsetFix<1,0,UNSIGNED> id3069out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3070out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3363out_value;

  HWOffsetFix<1,0,UNSIGNED> id2722out_result[2];

  HWFloat<8,24> id44out_j_in;

  HWFloat<8,24> id45out_result[2];

  HWFloat<8,24> id3097out_output[10];

  HWFloat<8,24> id3361out_value;

  HWFloat<8,24> id2894out_floatOut[2];

  HWRawBits<8> id864out_value;

  HWRawBits<23> id3360out_value;

  HWOffsetFix<1,0,UNSIGNED> id3085out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id792out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id794out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id794out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id797out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3076out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id876out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id876sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id873out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id873sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id814out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3077out_output[3];

  HWFloat<8,24> id3358out_value;

  HWOffsetFix<1,0,UNSIGNED> id3079out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3080out_output[3];

  HWFloat<8,24> id3357out_value;

  HWOffsetFix<1,0,UNSIGNED> id3081out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3082out_output[3];

  HWFloat<8,24> id3354out_value;

  HWFloat<8,24> id3353out_value;

  HWRawBits<8> id955out_value;

  HWRawBits<23> id3352out_value;

  HWOffsetFix<1,0,UNSIGNED> id3096out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id883out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id885out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id885out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id888out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3087out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id967out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id967sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id964out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id964sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id905out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3088out_output[3];

  HWFloat<8,24> id3350out_value;

  HWOffsetFix<1,0,UNSIGNED> id3090out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3091out_output[3];

  HWFloat<8,24> id3349out_value;

  HWOffsetFix<1,0,UNSIGNED> id3092out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3093out_output[3];

  HWFloat<8,24> id3343out_value;

  HWFloat<8,24> id3342out_value;

  HWRawBits<8> id1048out_value;

  HWRawBits<23> id3341out_value;

  HWOffsetFix<1,0,UNSIGNED> id3108out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id976out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id978out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id978out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id981out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3099out_output[3];

  HWOffsetFix<22,-24,UNSIGNED> id1060out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1060sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1057out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1057sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id998out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3100out_output[3];

  HWFloat<8,24> id3339out_value;

  HWOffsetFix<1,0,UNSIGNED> id3102out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3103out_output[3];

  HWFloat<8,24> id3338out_value;

  HWOffsetFix<1,0,UNSIGNED> id3104out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3105out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3333out_value;

  HWOffsetFix<1,0,UNSIGNED> id2744out_result[2];

  HWFloat<8,24> id40out_d_in;

  HWFloat<8,24> id41out_result[2];

  HWFloat<8,24> id3133out_output[9];

  HWFloat<8,24> id3332out_value;

  HWFloat<8,24> id3331out_value;

  HWFloat<8,24> id3330out_value;

  HWRawBits<8> id1142out_value;

  HWRawBits<23> id3329out_value;

  HWOffsetFix<1,0,UNSIGNED> id3122out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1070out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1072out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1072out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1075out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3113out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1111out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2848out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1154out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1154sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1151out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1151sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1092out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3114out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3328out_value;

  HWFloat<8,24> id3327out_value;

  HWOffsetFix<1,0,UNSIGNED> id3116out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3117out_output[3];

  HWFloat<8,24> id3326out_value;

  HWOffsetFix<1,0,UNSIGNED> id3118out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3119out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3325out_value;

  HWOffsetFix<1,0,UNSIGNED> id1121out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1105out_value;

  HWOffsetFix<1,0,UNSIGNED> id1130out_value;

  HWOffsetFix<8,0,UNSIGNED> id1131out_value;

  HWOffsetFix<23,0,UNSIGNED> id1133out_value;

  HWFloat<8,24> id2611out_value;

  HWFloat<8,24> id3324out_value;

  HWFloat<8,24> id3323out_value;

  HWFloat<8,24> id3322out_value;

  HWRawBits<8> id1233out_value;

  HWRawBits<23> id3321out_value;

  HWOffsetFix<1,0,UNSIGNED> id3132out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1161out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1163out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1163out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1166out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3123out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1202out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2850out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1245out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1245sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1242out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1242sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1183out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3124out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3320out_value;

  HWFloat<8,24> id3319out_value;

  HWOffsetFix<1,0,UNSIGNED> id3126out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3127out_output[3];

  HWFloat<8,24> id3318out_value;

  HWOffsetFix<1,0,UNSIGNED> id3128out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3129out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3317out_value;

  HWOffsetFix<1,0,UNSIGNED> id1212out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1196out_value;

  HWOffsetFix<1,0,UNSIGNED> id1221out_value;

  HWOffsetFix<8,0,UNSIGNED> id1222out_value;

  HWOffsetFix<23,0,UNSIGNED> id1224out_value;

  HWFloat<8,24> id2612out_value;

  HWFloat<8,24> id3316out_value;

  HWFloat<8,24> id3315out_value;

  HWFloat<8,24> id3314out_value;

  HWFloat<8,24> id3313out_value;

  HWFloat<8,24> id3312out_value;

  HWFloat<8,24> id3311out_value;

  HWRawBits<8> id1326out_value;

  HWRawBits<23> id3310out_value;

  HWOffsetFix<1,0,UNSIGNED> id3143out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1254out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1256out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1256out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1259out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3134out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1295out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2852out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1338out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1338sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1335out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1335sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1276out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3135out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3309out_value;

  HWFloat<8,24> id3308out_value;

  HWOffsetFix<1,0,UNSIGNED> id3137out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3138out_output[3];

  HWFloat<8,24> id3307out_value;

  HWOffsetFix<1,0,UNSIGNED> id3139out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3140out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3306out_value;

  HWOffsetFix<1,0,UNSIGNED> id1305out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1289out_value;

  HWOffsetFix<1,0,UNSIGNED> id1314out_value;

  HWOffsetFix<8,0,UNSIGNED> id1315out_value;

  HWOffsetFix<23,0,UNSIGNED> id1317out_value;

  HWFloat<8,24> id2613out_value;

  HWFloat<8,24> id3305out_value;

  HWFloat<8,24> id3304out_value;

  HWFloat<8,24> id3303out_value;

  HWRawBits<8> id1417out_value;

  HWRawBits<23> id3302out_value;

  HWOffsetFix<1,0,UNSIGNED> id3153out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1345out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1347out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1347out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1350out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3144out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1386out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2854out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1429out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1429sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1426out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1426sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1367out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3145out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3301out_value;

  HWFloat<8,24> id3300out_value;

  HWOffsetFix<1,0,UNSIGNED> id3147out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3148out_output[3];

  HWFloat<8,24> id3299out_value;

  HWOffsetFix<1,0,UNSIGNED> id3149out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3150out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3298out_value;

  HWOffsetFix<1,0,UNSIGNED> id1396out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1380out_value;

  HWOffsetFix<1,0,UNSIGNED> id1405out_value;

  HWOffsetFix<8,0,UNSIGNED> id1406out_value;

  HWOffsetFix<23,0,UNSIGNED> id1408out_value;

  HWFloat<8,24> id2614out_value;

  HWFloat<8,24> id3297out_value;

  HWFloat<8,24> id3296out_value;

  HWFloat<8,24> id3112out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3295out_value;

  HWOffsetFix<1,0,UNSIGNED> id2773out_result[2];

  HWFloat<8,24> id32out_f_in;

  HWFloat<8,24> id33out_result[2];

  HWFloat<8,24> id3178out_output[9];

  HWFloat<8,24> id3294out_value;

  HWFloat<8,24> id3293out_value;

  HWFloat<8,24> id3292out_value;

  HWRawBits<8> id1510out_value;

  HWRawBits<23> id3291out_value;

  HWOffsetFix<1,0,UNSIGNED> id3167out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1438out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1440out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1440out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1443out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3158out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1479out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2856out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1522out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1522sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1519out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1519sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1460out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3159out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3290out_value;

  HWFloat<8,24> id3289out_value;

  HWOffsetFix<1,0,UNSIGNED> id3161out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3162out_output[3];

  HWFloat<8,24> id3288out_value;

  HWOffsetFix<1,0,UNSIGNED> id3163out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3164out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3287out_value;

  HWOffsetFix<1,0,UNSIGNED> id1489out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1473out_value;

  HWOffsetFix<1,0,UNSIGNED> id1498out_value;

  HWOffsetFix<8,0,UNSIGNED> id1499out_value;

  HWOffsetFix<23,0,UNSIGNED> id1501out_value;

  HWFloat<8,24> id2615out_value;

  HWFloat<8,24> id3286out_value;

  HWFloat<8,24> id3285out_value;

  HWFloat<8,24> id3284out_value;

  HWRawBits<8> id1601out_value;

  HWRawBits<23> id3283out_value;

  HWOffsetFix<1,0,UNSIGNED> id3177out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1529out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1531out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1531out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1534out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3168out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1570out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2858out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1613out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1613sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1610out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1610sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1551out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3169out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3282out_value;

  HWFloat<8,24> id3281out_value;

  HWOffsetFix<1,0,UNSIGNED> id3171out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3172out_output[3];

  HWFloat<8,24> id3280out_value;

  HWOffsetFix<1,0,UNSIGNED> id3173out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3174out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3279out_value;

  HWOffsetFix<1,0,UNSIGNED> id1580out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1564out_value;

  HWOffsetFix<1,0,UNSIGNED> id1589out_value;

  HWOffsetFix<8,0,UNSIGNED> id1590out_value;

  HWOffsetFix<23,0,UNSIGNED> id1592out_value;

  HWFloat<8,24> id2616out_value;

  HWFloat<8,24> id3278out_value;

  HWFloat<8,24> id3277out_value;

  HWFloat<8,24> id3276out_value;

  HWFloat<8,24> id3275out_value;

  HWFloat<8,24> id3274out_value;

  HWFloat<8,24> id3273out_value;

  HWRawBits<8> id1694out_value;

  HWRawBits<23> id3272out_value;

  HWOffsetFix<1,0,UNSIGNED> id3188out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1622out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1624out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1624out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1627out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3179out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1663out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2860out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1706out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1706sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1703out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1703sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1644out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3180out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3271out_value;

  HWFloat<8,24> id3270out_value;

  HWOffsetFix<1,0,UNSIGNED> id3182out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3183out_output[3];

  HWFloat<8,24> id3269out_value;

  HWOffsetFix<1,0,UNSIGNED> id3184out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3185out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3268out_value;

  HWOffsetFix<1,0,UNSIGNED> id1673out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1657out_value;

  HWOffsetFix<1,0,UNSIGNED> id1682out_value;

  HWOffsetFix<8,0,UNSIGNED> id1683out_value;

  HWOffsetFix<23,0,UNSIGNED> id1685out_value;

  HWFloat<8,24> id2617out_value;

  HWFloat<8,24> id3267out_value;

  HWFloat<8,24> id3266out_value;

  HWFloat<8,24> id3265out_value;

  HWRawBits<8> id1785out_value;

  HWRawBits<23> id3264out_value;

  HWOffsetFix<1,0,UNSIGNED> id3198out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1713out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1715out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1715out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id1718out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id3189out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1754out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2862out_value;

  HWOffsetFix<22,-24,UNSIGNED> id1797out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id1797sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id1794out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id1794sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id1735out_value;

  HWOffsetFix<14,-26,UNSIGNED> id3190out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id3263out_value;

  HWFloat<8,24> id3262out_value;

  HWOffsetFix<1,0,UNSIGNED> id3192out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3193out_output[3];

  HWFloat<8,24> id3261out_value;

  HWOffsetFix<1,0,UNSIGNED> id3194out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id3195out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3260out_value;

  HWOffsetFix<1,0,UNSIGNED> id1764out_value;

  HWOffsetFix<24,-23,UNSIGNED> id1748out_value;

  HWOffsetFix<1,0,UNSIGNED> id1773out_value;

  HWOffsetFix<8,0,UNSIGNED> id1774out_value;

  HWOffsetFix<23,0,UNSIGNED> id1776out_value;

  HWFloat<8,24> id2618out_value;

  HWFloat<8,24> id3259out_value;

  HWFloat<8,24> id3258out_value;

  HWFloat<8,24> id3157out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3255out_value;

  HWOffsetFix<1,0,UNSIGNED> id2802out_result[2];

  HWFloat<8,24> id3254out_value;

  HWFloat<8,24> id3253out_value;

  HWFloat<8,24> id3252out_value;

  HWFloat<8,24> id20out_ca_in;

  HWFloat<8,24> id21out_result[2];

  HWFloat<8,24> id3210out_output[8];

  HWFloat<8,24> id3233out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id1840out_value;

  HWOffsetFix<1,0,UNSIGNED> id3247out_value;

  HWOffsetFix<1,0,UNSIGNED> id3211out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id1848out_value;

  HWRawBits<96> id2573out_dout[3];

  HWRawBits<96> id2573sta_rom_store[512];

  HWOffsetFix<14,-14,UNSIGNED> id3212out_output[3];

  HWOffsetFix<11,0,UNSIGNED> id3243out_value;

  HWOffsetFix<11,0,UNSIGNED> id2519out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2806out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2521out_io_ca_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3218out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3242out_value;

  HWOffsetFix<11,0,UNSIGNED> id2527out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2807out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2529out_io_x1_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3220out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3241out_value;

  HWOffsetFix<11,0,UNSIGNED> id2535out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2808out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2537out_io_m_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3222out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3240out_value;

  HWOffsetFix<11,0,UNSIGNED> id2543out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2809out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2545out_io_f_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3224out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3239out_value;

  HWOffsetFix<11,0,UNSIGNED> id2551out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2810out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2553out_io_h_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3226out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3238out_value;

  HWOffsetFix<11,0,UNSIGNED> id2559out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2811out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2561out_io_d_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3228out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3237out_value;

  HWOffsetFix<11,0,UNSIGNED> id2567out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2812out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2569out_io_j_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3230out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id2586out_value;

  HWOffsetFix<1,0,UNSIGNED> id3236out_value;

  HWOffsetFix<49,0,UNSIGNED> id2583out_value;

  HWOffsetFix<48,0,UNSIGNED> id2584out_count;
  HWOffsetFix<1,0,UNSIGNED> id2584out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2584st_count;

  HWOffsetFix<1,0,UNSIGNED> id3235out_value;

  HWOffsetFix<49,0,UNSIGNED> id2589out_value;

  HWOffsetFix<48,0,UNSIGNED> id2590out_count;
  HWOffsetFix<1,0,UNSIGNED> id2590out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2590st_count;

  HWOffsetFix<48,0,UNSIGNED> id2592out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id2813out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits_1;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<23,0,UNSIGNED> c_hw_fix_23_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits_1;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWFloat<8,24> c_hw_flt_8_24_bits_2;
  const HWFloat<8,24> c_hw_flt_8_24_bits_3;
  const HWFloat<8,24> c_hw_flt_8_24_bits_4;
  const HWFloat<8,24> c_hw_flt_8_24_bits_5;
  const HWFloat<8,24> c_hw_flt_8_24_bits_6;
  const HWFloat<8,24> c_hw_flt_8_24_bits_7;
  const HWFloat<8,24> c_hw_flt_8_24_bits_8;
  const HWFloat<8,24> c_hw_flt_8_24_bits_9;
  const HWFloat<8,24> c_hw_flt_8_24_bits_10;
  const HWFloat<8,24> c_hw_flt_8_24_bits_11;
  const HWFloat<8,24> c_hw_flt_8_24_bits_12;
  const HWFloat<8,24> c_hw_flt_8_24_bits_13;
  const HWFloat<8,24> c_hw_flt_8_24_bits_14;
  const HWFloat<8,24> c_hw_flt_8_24_bits_15;
  const HWFloat<8,24> c_hw_flt_8_24_bits_16;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<23> c_hw_bit_23_bits;
  const HWRawBits<31> c_hw_bit_31_bits;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits_1;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits_2;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_undef;
  const HWFloat<8,24> c_hw_flt_8_24_bits_17;
  const HWFloat<8,24> c_hw_flt_8_24_bits_18;
  const HWFloat<8,24> c_hw_flt_8_24_bits_19;
  const HWFloat<8,24> c_hw_flt_8_24_bits_20;
  const HWFloat<8,24> c_hw_flt_8_24_bits_21;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<35,-34,UNSIGNED> c_hw_fix_35_n34_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<22,-24,UNSIGNED> c_hw_fix_22_n24_uns_undef;
  const HWOffsetFix<24,-24,UNSIGNED> c_hw_fix_24_n24_uns_undef;
  const HWOffsetFix<14,-14,UNSIGNED> c_hw_fix_14_n14_uns_bits;
  const HWOffsetFix<14,-26,UNSIGNED> c_hw_fix_14_n26_uns_undef;
  const HWFloat<8,24> c_hw_flt_8_24_4_0val;
  const HWFloat<8,24> c_hw_flt_8_24_bits_22;
  const HWFloat<8,24> c_hw_flt_8_24_bits_23;
  const HWFloat<8,24> c_hw_flt_8_24_bits_24;
  const HWFloat<8,24> c_hw_flt_8_24_bits_25;
  const HWFloat<8,24> c_hw_flt_8_24_bits_26;
  const HWFloat<8,24> c_hw_flt_8_24_bits_27;
  const HWFloat<8,24> c_hw_flt_8_24_bits_28;
  const HWFloat<8,24> c_hw_flt_8_24_bits_29;
  const HWFloat<8,24> c_hw_flt_8_24_bits_30;
  const HWFloat<8,24> c_hw_flt_8_24_bits_31;
  const HWFloat<8,24> c_hw_flt_8_24_bits_32;
  const HWFloat<8,24> c_hw_flt_8_24_bits_33;
  const HWFloat<8,24> c_hw_flt_8_24_bits_34;
  const HWFloat<8,24> c_hw_flt_8_24_bits_35;
  const HWFloat<8,24> c_hw_flt_8_24_bits_36;
  const HWFloat<8,24> c_hw_flt_8_24_bits_37;
  const HWFloat<8,24> c_hw_flt_8_24_n0_25val;
  const HWFloat<8,24> c_hw_flt_8_24_bits_38;
  const HWFloat<8,24> c_hw_flt_8_24_bits_39;
  const HWFloat<8,24> c_hw_flt_8_24_bits_40;
  const HWFloat<8,24> c_hw_flt_8_24_bits_41;
  const HWFloat<8,24> c_hw_flt_8_24_bits_42;
  const HWFloat<8,24> c_hw_flt_8_24_bits_43;
  const HWFloat<8,24> c_hw_flt_8_24_bits_44;
  const HWFloat<8,24> c_hw_flt_8_24_bits_45;
  const HWFloat<8,24> c_hw_flt_8_24_bits_46;
  const HWFloat<8,24> c_hw_flt_8_24_bits_47;
  const HWFloat<8,24> c_hw_flt_8_24_bits_48;
  const HWFloat<8,24> c_hw_flt_8_24_bits_49;
  const HWFloat<8,24> c_hw_flt_8_24_bits_50;
  const HWFloat<8,24> c_hw_flt_8_24_bits_51;
  const HWFloat<8,24> c_hw_flt_8_24_bits_52;
  const HWFloat<8,24> c_hw_flt_8_24_bits_53;
  const HWFloat<8,24> c_hw_flt_8_24_bits_54;
  const HWFloat<8,24> c_hw_flt_8_24_bits_55;
  const HWFloat<8,24> c_hw_flt_8_24_bits_56;
  const HWFloat<8,24> c_hw_flt_8_24_bits_57;
  const HWFloat<8,24> c_hw_flt_8_24_bits_58;
  const HWFloat<8,24> c_hw_flt_8_24_bits_59;
  const HWRawBits<96> c_hw_bit_96_undef;
  const HWOffsetFix<14,-14,UNSIGNED> c_hw_fix_14_n14_uns_undef;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* BEELERREUTERDFEKERNEL_H_ */
