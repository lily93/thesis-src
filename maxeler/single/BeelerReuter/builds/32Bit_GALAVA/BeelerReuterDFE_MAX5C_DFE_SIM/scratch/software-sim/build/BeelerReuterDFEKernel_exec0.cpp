#include "stdsimheader.h"

namespace maxcompilersim {

void BeelerReuterDFEKernel::execute0() {
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 3234 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 11 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id11in_enable = id8out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id11in_max = id3234out_value;

    HWOffsetFix<12,0,UNSIGNED> id11x_1;
    HWOffsetFix<1,0,UNSIGNED> id11x_2;
    HWOffsetFix<1,0,UNSIGNED> id11x_3;
    HWOffsetFix<12,0,UNSIGNED> id11x_4t_1e_1;

    id11out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id11st_count)));
    (id11x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id11st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id11x_2) = (gte_fixed((id11x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id11in_max))));
    (id11x_3) = (and_fixed((id11x_2),id11in_enable));
    id11out_wrap = (id11x_3);
    if((id11in_enable.getValueAsBool())) {
      if(((id11x_3).getValueAsBool())) {
        (id11st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id11x_4t_1e_1) = (id11x_1);
        (id11st_count) = (id11x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 2912 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id2912in_input = id11out_count;

    id2912out_output[(getCycle()+1)%2] = id2912in_input;
  }
  { // Node ID: 3499 (NodeConstantRawBits)
  }
  { // Node ID: 2511 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2511in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2511in_b = id3499out_value;

    id2511out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2511in_a,id2511in_b));
  }
  { // Node ID: 2619 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2619in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2619in_b = id2511out_result[getCycle()%2];

    id2619out_result[(getCycle()+1)%2] = (eq_fixed(id2619in_a,id2619in_b));
  }
  { // Node ID: 2913 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2913in_input = id2619out_result[getCycle()%2];

    id2913out_output[(getCycle()+9)%10] = id2913in_input;
  }
  { // Node ID: 2513 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2514out_result;

  { // Node ID: 2514 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2514in_a = id2513out_io_u_out_force_disabled;

    id2514out_result = (not_fixed(id2514in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2515out_result;

  { // Node ID: 2515 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2515in_a = id2913out_output[getCycle()%10];
    const HWOffsetFix<1,0,UNSIGNED> &id2515in_b = id2514out_result;

    HWOffsetFix<1,0,UNSIGNED> id2515x_1;

    (id2515x_1) = (and_fixed(id2515in_a,id2515in_b));
    id2515out_result = (id2515x_1);
  }
  { // Node ID: 9 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 10 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id10in_enable = id11out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id9out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id10x_1;
    HWOffsetFix<1,0,UNSIGNED> id10x_2;
    HWOffsetFix<1,0,UNSIGNED> id10x_3;
    HWOffsetFix<33,0,UNSIGNED> id10x_4t_1e_1;

    id10out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id10st_count)));
    (id10x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id10st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id10x_2) = (gte_fixed((id10x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id10in_max))));
    (id10x_3) = (and_fixed((id10x_2),id10in_enable));
    id10out_wrap = (id10x_3);
    if((id10in_enable.getValueAsBool())) {
      if(((id10x_3).getValueAsBool())) {
        (id10st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id10x_4t_1e_1) = (id10x_1);
        (id10st_count) = (id10x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 3498 (NodeConstantRawBits)
  }
  { // Node ID: 2620 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2620in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2620in_b = id3498out_value;

    id2620out_result[(getCycle()+1)%2] = (eq_fixed(id2620in_a,id2620in_b));
  }
  { // Node ID: 12 (NodeInputMappedReg)
  }
  { // Node ID: 3497 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1968out_o;

  { // Node ID: 1968 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1968in_i = id2925out_output[getCycle()%3];

    id1968out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1968in_i));
  }
  { // Node ID: 1971 (NodeConstantRawBits)
  }
  { // Node ID: 2816 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id1954out_result;

  { // Node ID: 1954 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1954in_a = id2011out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1954in_b = id2926out_output[getCycle()%3];

    id1954out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1954in_a,id1954in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1955out_result;

  { // Node ID: 1955 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1955in_a = id2926out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1955in_b = id2011out_dout[getCycle()%3];

    id1955out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1955in_a,id1955in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1956out_result;

  { // Node ID: 1956 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1956in_a = id1954out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1956in_b = id1955out_result;

    id1956out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1956in_a,id1956in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1957out_o;

  { // Node ID: 1957 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1957in_i = id1956out_result;

    id1957out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1957in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1958out_result;

  { // Node ID: 1958 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1958in_a = id2014out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1958in_b = id1957out_o;

    id1958out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1958in_a,id1958in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1959out_result;

  { // Node ID: 1959 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1959in_a = id1957out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1959in_b = id2014out_dout[getCycle()%3];

    id1959out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1959in_a,id1959in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1960out_result;

  { // Node ID: 1960 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1960in_a = id1958out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1960in_b = id1959out_result;

    id1960out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1960in_a,id1960in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1961out_o;

  { // Node ID: 1961 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1961in_i = id1960out_result;

    id1961out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1961in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1962out_o;

  { // Node ID: 1962 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1962in_i = id1961out_o;

    id1962out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1962in_i));
  }
  { // Node ID: 3484 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2630out_result;

  { // Node ID: 2630 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2630in_a = id1962out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2630in_b = id3484out_value;

    id2630out_result = (gte_fixed(id2630in_a,id2630in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2889out_result;

  { // Node ID: 2889 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2889in_a = id1968out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2889in_b = id1971out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2889in_c = id2816out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2889in_condb = id2630out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2889x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2889x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2889x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2889x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2889x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2889x_1 = id2889in_a;
        break;
      default:
        id2889x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2889in_condb.getValueAsLong())) {
      case 0l:
        id2889x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2889x_2 = id2889in_b;
        break;
      default:
        id2889x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2889x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2889x_3 = id2889in_c;
        break;
      default:
        id2889x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2889x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2889x_1),(id2889x_2))),(id2889x_3)));
    id2889out_result = (id2889x_4);
  }
  HWRawBits<1> id2631out_result;

  { // Node ID: 2631 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2631in_a = id2889out_result;

    id2631out_result = (slice<10,1>(id2631in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2632out_output;

  { // Node ID: 2632 (NodeReinterpret)
    const HWRawBits<1> &id2632in_input = id2631out_result;

    id2632out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2632in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1977out_result;

  { // Node ID: 1977 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1977in_a = id2929out_output[getCycle()%3];

    id1977out_result = (not_fixed(id1977in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1978out_result;

  { // Node ID: 1978 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1978in_a = id2632out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1978in_b = id1977out_result;

    HWOffsetFix<1,0,UNSIGNED> id1978x_1;

    (id1978x_1) = (and_fixed(id1978in_a,id1978in_b));
    id1978out_result = (id1978x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1979out_result;

  { // Node ID: 1979 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1979in_a = id1978out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1979in_b = id2931out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1979x_1;

    (id1979x_1) = (or_fixed(id1979in_a,id1979in_b));
    id1979out_result = (id1979x_1);
  }
  { // Node ID: 3481 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2633out_result;

  { // Node ID: 2633 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2633in_a = id2889out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2633in_b = id3481out_value;

    id2633out_result = (gte_fixed(id2633in_a,id2633in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1986out_result;

  { // Node ID: 1986 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1986in_a = id2931out_output[getCycle()%3];

    id1986out_result = (not_fixed(id1986in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1987out_result;

  { // Node ID: 1987 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1987in_a = id2633out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1987in_b = id1986out_result;

    HWOffsetFix<1,0,UNSIGNED> id1987x_1;

    (id1987x_1) = (and_fixed(id1987in_a,id1987in_b));
    id1987out_result = (id1987x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1988out_result;

  { // Node ID: 1988 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1988in_a = id1987out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1988in_b = id2929out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1988x_1;

    (id1988x_1) = (or_fixed(id1988in_a,id1988in_b));
    id1988out_result = (id1988x_1);
  }
  HWRawBits<2> id1989out_result;

  { // Node ID: 1989 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1989in_in0 = id1979out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1989in_in1 = id1988out_result;

    id1989out_result = (cat(id1989in_in0,id1989in_in1));
  }
  { // Node ID: 1981 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1980out_o;

  { // Node ID: 1980 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1980in_i = id2889out_result;

    id1980out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1980in_i));
  }
  { // Node ID: 1965 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1966out_result;

  { // Node ID: 1966 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1966in_sel = id2630out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1966in_option0 = id1962out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1966in_option1 = id1965out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1966x_1;

    switch((id1966in_sel.getValueAsLong())) {
      case 0l:
        id1966x_1 = id1966in_option0;
        break;
      case 1l:
        id1966x_1 = id1966in_option1;
        break;
      default:
        id1966x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1966out_result = (id1966x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1967out_o;

  { // Node ID: 1967 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1967in_i = id1966out_result;

    id1967out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1967in_i));
  }
  HWRawBits<32> id1982out_result;

  { // Node ID: 1982 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1982in_in0 = id1981out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1982in_in1 = id1980out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1982in_in2 = id1967out_o;

    id1982out_result = (cat((cat(id1982in_in0,id1982in_in1)),id1982in_in2));
  }
  HWFloat<8,24> id1983out_output;

  { // Node ID: 1983 (NodeReinterpret)
    const HWRawBits<32> &id1983in_input = id1982out_result;

    id1983out_output = (cast_bits2float<8,24>(id1983in_input));
  }
  { // Node ID: 1990 (NodeConstantRawBits)
  }
  { // Node ID: 1991 (NodeConstantRawBits)
  }
  { // Node ID: 1993 (NodeConstantRawBits)
  }
  HWRawBits<32> id2634out_result;

  { // Node ID: 2634 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2634in_in0 = id1990out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2634in_in1 = id1991out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2634in_in2 = id1993out_value;

    id2634out_result = (cat((cat(id2634in_in0,id2634in_in1)),id2634in_in2));
  }
  HWFloat<8,24> id1995out_output;

  { // Node ID: 1995 (NodeReinterpret)
    const HWRawBits<32> &id1995in_input = id2634out_result;

    id1995out_output = (cast_bits2float<8,24>(id1995in_input));
  }
  { // Node ID: 2595 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1998out_result;

  { // Node ID: 1998 (NodeMux)
    const HWRawBits<2> &id1998in_sel = id1989out_result;
    const HWFloat<8,24> &id1998in_option0 = id1983out_output;
    const HWFloat<8,24> &id1998in_option1 = id1995out_output;
    const HWFloat<8,24> &id1998in_option2 = id2595out_value;
    const HWFloat<8,24> &id1998in_option3 = id1995out_output;

    HWFloat<8,24> id1998x_1;

    switch((id1998in_sel.getValueAsLong())) {
      case 0l:
        id1998x_1 = id1998in_option0;
        break;
      case 1l:
        id1998x_1 = id1998in_option1;
        break;
      case 2l:
        id1998x_1 = id1998in_option2;
        break;
      case 3l:
        id1998x_1 = id1998in_option3;
        break;
      default:
        id1998x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1998out_result = (id1998x_1);
  }
  { // Node ID: 3480 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2008out_result;

  { // Node ID: 2008 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2008in_sel = id2934out_output[getCycle()%9];
    const HWFloat<8,24> &id2008in_option0 = id1998out_result;
    const HWFloat<8,24> &id2008in_option1 = id3480out_value;

    HWFloat<8,24> id2008x_1;

    switch((id2008in_sel.getValueAsLong())) {
      case 0l:
        id2008x_1 = id2008in_option0;
        break;
      case 1l:
        id2008x_1 = id2008in_option1;
        break;
      default:
        id2008x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2008out_result = (id2008x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2057out_o;

  { // Node ID: 2057 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2057in_i = id2936out_output[getCycle()%3];

    id2057out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2057in_i));
  }
  { // Node ID: 2060 (NodeConstantRawBits)
  }
  { // Node ID: 2818 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id2043out_result;

  { // Node ID: 2043 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id2043in_a = id2100out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id2043in_b = id2937out_output[getCycle()%3];

    id2043out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2043in_a,id2043in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id2044out_result;

  { // Node ID: 2044 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id2044in_a = id2937out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id2044in_b = id2100out_dout[getCycle()%3];

    id2044out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id2044in_a,id2044in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id2045out_result;

  { // Node ID: 2045 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id2045in_a = id2043out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id2045in_b = id2044out_result;

    id2045out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id2045in_a,id2045in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2046out_o;

  { // Node ID: 2046 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id2046in_i = id2045out_result;

    id2046out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id2046in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2047out_result;

  { // Node ID: 2047 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id2047in_a = id2103out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id2047in_b = id2046out_o;

    id2047out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id2047in_a,id2047in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id2048out_result;

  { // Node ID: 2048 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id2048in_a = id2046out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id2048in_b = id2103out_dout[getCycle()%3];

    id2048out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id2048in_a,id2048in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id2049out_result;

  { // Node ID: 2049 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id2049in_a = id2047out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id2049in_b = id2048out_result;

    id2049out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id2049in_a,id2049in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2050out_o;

  { // Node ID: 2050 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id2050in_i = id2049out_result;

    id2050out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id2050in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id2051out_o;

  { // Node ID: 2051 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id2051in_i = id2050out_o;

    id2051out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id2051in_i));
  }
  { // Node ID: 3476 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2637out_result;

  { // Node ID: 2637 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2637in_a = id2051out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2637in_b = id3476out_value;

    id2637out_result = (gte_fixed(id2637in_a,id2637in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2888out_result;

  { // Node ID: 2888 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2888in_a = id2057out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2888in_b = id2060out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2888in_c = id2818out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2888in_condb = id2637out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2888x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2888x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2888x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2888x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2888x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2888x_1 = id2888in_a;
        break;
      default:
        id2888x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2888in_condb.getValueAsLong())) {
      case 0l:
        id2888x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2888x_2 = id2888in_b;
        break;
      default:
        id2888x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2888x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2888x_3 = id2888in_c;
        break;
      default:
        id2888x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2888x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2888x_1),(id2888x_2))),(id2888x_3)));
    id2888out_result = (id2888x_4);
  }
  HWRawBits<1> id2638out_result;

  { // Node ID: 2638 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2638in_a = id2888out_result;

    id2638out_result = (slice<10,1>(id2638in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2639out_output;

  { // Node ID: 2639 (NodeReinterpret)
    const HWRawBits<1> &id2639in_input = id2638out_result;

    id2639out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2639in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2066out_result;

  { // Node ID: 2066 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2066in_a = id2940out_output[getCycle()%3];

    id2066out_result = (not_fixed(id2066in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2067out_result;

  { // Node ID: 2067 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2067in_a = id2639out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2067in_b = id2066out_result;

    HWOffsetFix<1,0,UNSIGNED> id2067x_1;

    (id2067x_1) = (and_fixed(id2067in_a,id2067in_b));
    id2067out_result = (id2067x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2068out_result;

  { // Node ID: 2068 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2068in_a = id2067out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2068in_b = id2942out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2068x_1;

    (id2068x_1) = (or_fixed(id2068in_a,id2068in_b));
    id2068out_result = (id2068x_1);
  }
  { // Node ID: 3473 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2640out_result;

  { // Node ID: 2640 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2640in_a = id2888out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2640in_b = id3473out_value;

    id2640out_result = (gte_fixed(id2640in_a,id2640in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2075out_result;

  { // Node ID: 2075 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2075in_a = id2942out_output[getCycle()%3];

    id2075out_result = (not_fixed(id2075in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2076out_result;

  { // Node ID: 2076 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2076in_a = id2640out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2076in_b = id2075out_result;

    HWOffsetFix<1,0,UNSIGNED> id2076x_1;

    (id2076x_1) = (and_fixed(id2076in_a,id2076in_b));
    id2076out_result = (id2076x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2077out_result;

  { // Node ID: 2077 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2077in_a = id2076out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2077in_b = id2940out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2077x_1;

    (id2077x_1) = (or_fixed(id2077in_a,id2077in_b));
    id2077out_result = (id2077x_1);
  }
  HWRawBits<2> id2078out_result;

  { // Node ID: 2078 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2078in_in0 = id2068out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2078in_in1 = id2077out_result;

    id2078out_result = (cat(id2078in_in0,id2078in_in1));
  }
  { // Node ID: 2070 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2069out_o;

  { // Node ID: 2069 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2069in_i = id2888out_result;

    id2069out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2069in_i));
  }
  { // Node ID: 2054 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2055out_result;

  { // Node ID: 2055 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2055in_sel = id2637out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id2055in_option0 = id2051out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2055in_option1 = id2054out_value;

    HWOffsetFix<24,-23,UNSIGNED> id2055x_1;

    switch((id2055in_sel.getValueAsLong())) {
      case 0l:
        id2055x_1 = id2055in_option0;
        break;
      case 1l:
        id2055x_1 = id2055in_option1;
        break;
      default:
        id2055x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id2055out_result = (id2055x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id2056out_o;

  { // Node ID: 2056 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id2056in_i = id2055out_result;

    id2056out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id2056in_i));
  }
  HWRawBits<32> id2071out_result;

  { // Node ID: 2071 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2071in_in0 = id2070out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2071in_in1 = id2069out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id2071in_in2 = id2056out_o;

    id2071out_result = (cat((cat(id2071in_in0,id2071in_in1)),id2071in_in2));
  }
  HWFloat<8,24> id2072out_output;

  { // Node ID: 2072 (NodeReinterpret)
    const HWRawBits<32> &id2072in_input = id2071out_result;

    id2072out_output = (cast_bits2float<8,24>(id2072in_input));
  }
  { // Node ID: 2079 (NodeConstantRawBits)
  }
  { // Node ID: 2080 (NodeConstantRawBits)
  }
  { // Node ID: 2082 (NodeConstantRawBits)
  }
  HWRawBits<32> id2641out_result;

  { // Node ID: 2641 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2641in_in0 = id2079out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2641in_in1 = id2080out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2641in_in2 = id2082out_value;

    id2641out_result = (cat((cat(id2641in_in0,id2641in_in1)),id2641in_in2));
  }
  HWFloat<8,24> id2084out_output;

  { // Node ID: 2084 (NodeReinterpret)
    const HWRawBits<32> &id2084in_input = id2641out_result;

    id2084out_output = (cast_bits2float<8,24>(id2084in_input));
  }
  { // Node ID: 2596 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2087out_result;

  { // Node ID: 2087 (NodeMux)
    const HWRawBits<2> &id2087in_sel = id2078out_result;
    const HWFloat<8,24> &id2087in_option0 = id2072out_output;
    const HWFloat<8,24> &id2087in_option1 = id2084out_output;
    const HWFloat<8,24> &id2087in_option2 = id2596out_value;
    const HWFloat<8,24> &id2087in_option3 = id2084out_output;

    HWFloat<8,24> id2087x_1;

    switch((id2087in_sel.getValueAsLong())) {
      case 0l:
        id2087x_1 = id2087in_option0;
        break;
      case 1l:
        id2087x_1 = id2087in_option1;
        break;
      case 2l:
        id2087x_1 = id2087in_option2;
        break;
      case 3l:
        id2087x_1 = id2087in_option3;
        break;
      default:
        id2087x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2087out_result = (id2087x_1);
  }
  { // Node ID: 3472 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2097out_result;

  { // Node ID: 2097 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2097in_sel = id2945out_output[getCycle()%9];
    const HWFloat<8,24> &id2097in_option0 = id2087out_result;
    const HWFloat<8,24> &id2097in_option1 = id3472out_value;

    HWFloat<8,24> id2097x_1;

    switch((id2097in_sel.getValueAsLong())) {
      case 0l:
        id2097x_1 = id2097in_option0;
        break;
      case 1l:
        id2097x_1 = id2097in_option1;
        break;
      default:
        id2097x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2097out_result = (id2097x_1);
  }
  HWFloat<8,24> id2104out_result;

  { // Node ID: 2104 (NodeAdd)
    const HWFloat<8,24> &id2104in_a = id2008out_result;
    const HWFloat<8,24> &id2104in_b = id2097out_result;

    id2104out_result = (add_float(id2104in_a,id2104in_b));
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2196out_result;

  { // Node ID: 2196 (NodeEq)
    const HWFloat<8,24> &id2196in_a = id2104out_result;
    const HWFloat<8,24> &id2196in_b = id5out_value;

    id2196out_result = (eq_float(id2196in_a,id2196in_b));
  }
  { // Node ID: 2197 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2198out_result;

  { // Node ID: 2198 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2198in_sel = id2196out_result;
    const HWFloat<8,24> &id2198in_option0 = id2104out_result;
    const HWFloat<8,24> &id2198in_option1 = id2197out_value;

    HWFloat<8,24> id2198x_1;

    switch((id2198in_sel.getValueAsLong())) {
      case 0l:
        id2198x_1 = id2198in_option0;
        break;
      case 1l:
        id2198x_1 = id2198in_option1;
        break;
      default:
        id2198x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2198out_result = (id2198x_1);
  }
  HWFloat<8,24> id2295out_result;

  { // Node ID: 2295 (NodeDiv)
    const HWFloat<8,24> &id2295in_a = id2891out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id2295in_b = id2198out_result;

    id2295out_result = (div_float(id2295in_a,id2295in_b));
  }
  { // Node ID: 3471 (NodeConstantRawBits)
  }
  { // Node ID: 3470 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2297out_result;

  { // Node ID: 2297 (NodeAdd)
    const HWFloat<8,24> &id2297in_a = id3232out_output[getCycle()%2];
    const HWFloat<8,24> &id2297in_b = id3470out_value;

    id2297out_result = (add_float(id2297in_a,id2297in_b));
  }
  HWFloat<8,24> id2299out_result;

  { // Node ID: 2299 (NodeMul)
    const HWFloat<8,24> &id2299in_a = id3471out_value;
    const HWFloat<8,24> &id2299in_b = id2297out_result;

    id2299out_result = (mul_float(id2299in_a,id2299in_b));
  }
  { // Node ID: 3469 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2147out_o;

  { // Node ID: 2147 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2147in_i = id2948out_output[getCycle()%3];

    id2147out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2147in_i));
  }
  { // Node ID: 2150 (NodeConstantRawBits)
  }
  { // Node ID: 2820 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id2133out_result;

  { // Node ID: 2133 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id2133in_a = id2190out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id2133in_b = id2949out_output[getCycle()%3];

    id2133out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2133in_a,id2133in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id2134out_result;

  { // Node ID: 2134 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id2134in_a = id2949out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id2134in_b = id2190out_dout[getCycle()%3];

    id2134out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id2134in_a,id2134in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id2135out_result;

  { // Node ID: 2135 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id2135in_a = id2133out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id2135in_b = id2134out_result;

    id2135out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id2135in_a,id2135in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2136out_o;

  { // Node ID: 2136 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id2136in_i = id2135out_result;

    id2136out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id2136in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2137out_result;

  { // Node ID: 2137 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id2137in_a = id2193out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id2137in_b = id2136out_o;

    id2137out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id2137in_a,id2137in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id2138out_result;

  { // Node ID: 2138 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id2138in_a = id2136out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id2138in_b = id2193out_dout[getCycle()%3];

    id2138out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id2138in_a,id2138in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id2139out_result;

  { // Node ID: 2139 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id2139in_a = id2137out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id2139in_b = id2138out_result;

    id2139out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id2139in_a,id2139in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2140out_o;

  { // Node ID: 2140 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id2140in_i = id2139out_result;

    id2140out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id2140in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id2141out_o;

  { // Node ID: 2141 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id2141in_i = id2140out_o;

    id2141out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id2141in_i));
  }
  { // Node ID: 3465 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2644out_result;

  { // Node ID: 2644 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2644in_a = id2141out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2644in_b = id3465out_value;

    id2644out_result = (gte_fixed(id2644in_a,id2644in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2887out_result;

  { // Node ID: 2887 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2887in_a = id2147out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2887in_b = id2150out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2887in_c = id2820out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2887in_condb = id2644out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2887x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2887x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2887x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2887x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2887x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2887x_1 = id2887in_a;
        break;
      default:
        id2887x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2887in_condb.getValueAsLong())) {
      case 0l:
        id2887x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2887x_2 = id2887in_b;
        break;
      default:
        id2887x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2887x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2887x_3 = id2887in_c;
        break;
      default:
        id2887x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2887x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2887x_1),(id2887x_2))),(id2887x_3)));
    id2887out_result = (id2887x_4);
  }
  HWRawBits<1> id2645out_result;

  { // Node ID: 2645 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2645in_a = id2887out_result;

    id2645out_result = (slice<10,1>(id2645in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2646out_output;

  { // Node ID: 2646 (NodeReinterpret)
    const HWRawBits<1> &id2646in_input = id2645out_result;

    id2646out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2646in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2156out_result;

  { // Node ID: 2156 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2156in_a = id2952out_output[getCycle()%3];

    id2156out_result = (not_fixed(id2156in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2157out_result;

  { // Node ID: 2157 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2157in_a = id2646out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2157in_b = id2156out_result;

    HWOffsetFix<1,0,UNSIGNED> id2157x_1;

    (id2157x_1) = (and_fixed(id2157in_a,id2157in_b));
    id2157out_result = (id2157x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2158out_result;

  { // Node ID: 2158 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2158in_a = id2157out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2158in_b = id2954out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2158x_1;

    (id2158x_1) = (or_fixed(id2158in_a,id2158in_b));
    id2158out_result = (id2158x_1);
  }
  { // Node ID: 3462 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2647out_result;

  { // Node ID: 2647 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2647in_a = id2887out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2647in_b = id3462out_value;

    id2647out_result = (gte_fixed(id2647in_a,id2647in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2165out_result;

  { // Node ID: 2165 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2165in_a = id2954out_output[getCycle()%3];

    id2165out_result = (not_fixed(id2165in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2166out_result;

  { // Node ID: 2166 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2166in_a = id2647out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2166in_b = id2165out_result;

    HWOffsetFix<1,0,UNSIGNED> id2166x_1;

    (id2166x_1) = (and_fixed(id2166in_a,id2166in_b));
    id2166out_result = (id2166x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2167out_result;

  { // Node ID: 2167 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2167in_a = id2166out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2167in_b = id2952out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2167x_1;

    (id2167x_1) = (or_fixed(id2167in_a,id2167in_b));
    id2167out_result = (id2167x_1);
  }
  HWRawBits<2> id2168out_result;

  { // Node ID: 2168 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2168in_in0 = id2158out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2168in_in1 = id2167out_result;

    id2168out_result = (cat(id2168in_in0,id2168in_in1));
  }
  { // Node ID: 2160 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2159out_o;

  { // Node ID: 2159 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2159in_i = id2887out_result;

    id2159out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2159in_i));
  }
  { // Node ID: 2144 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2145out_result;

  { // Node ID: 2145 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2145in_sel = id2644out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id2145in_option0 = id2141out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2145in_option1 = id2144out_value;

    HWOffsetFix<24,-23,UNSIGNED> id2145x_1;

    switch((id2145in_sel.getValueAsLong())) {
      case 0l:
        id2145x_1 = id2145in_option0;
        break;
      case 1l:
        id2145x_1 = id2145in_option1;
        break;
      default:
        id2145x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id2145out_result = (id2145x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id2146out_o;

  { // Node ID: 2146 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id2146in_i = id2145out_result;

    id2146out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id2146in_i));
  }
  HWRawBits<32> id2161out_result;

  { // Node ID: 2161 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2161in_in0 = id2160out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2161in_in1 = id2159out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id2161in_in2 = id2146out_o;

    id2161out_result = (cat((cat(id2161in_in0,id2161in_in1)),id2161in_in2));
  }
  HWFloat<8,24> id2162out_output;

  { // Node ID: 2162 (NodeReinterpret)
    const HWRawBits<32> &id2162in_input = id2161out_result;

    id2162out_output = (cast_bits2float<8,24>(id2162in_input));
  }
  { // Node ID: 2169 (NodeConstantRawBits)
  }
  { // Node ID: 2170 (NodeConstantRawBits)
  }
  { // Node ID: 2172 (NodeConstantRawBits)
  }
  HWRawBits<32> id2648out_result;

  { // Node ID: 2648 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2648in_in0 = id2169out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2648in_in1 = id2170out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2648in_in2 = id2172out_value;

    id2648out_result = (cat((cat(id2648in_in0,id2648in_in1)),id2648in_in2));
  }
  HWFloat<8,24> id2174out_output;

  { // Node ID: 2174 (NodeReinterpret)
    const HWRawBits<32> &id2174in_input = id2648out_result;

    id2174out_output = (cast_bits2float<8,24>(id2174in_input));
  }
  { // Node ID: 2597 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2177out_result;

  { // Node ID: 2177 (NodeMux)
    const HWRawBits<2> &id2177in_sel = id2168out_result;
    const HWFloat<8,24> &id2177in_option0 = id2162out_output;
    const HWFloat<8,24> &id2177in_option1 = id2174out_output;
    const HWFloat<8,24> &id2177in_option2 = id2597out_value;
    const HWFloat<8,24> &id2177in_option3 = id2174out_output;

    HWFloat<8,24> id2177x_1;

    switch((id2177in_sel.getValueAsLong())) {
      case 0l:
        id2177x_1 = id2177in_option0;
        break;
      case 1l:
        id2177x_1 = id2177in_option1;
        break;
      case 2l:
        id2177x_1 = id2177in_option2;
        break;
      case 3l:
        id2177x_1 = id2177in_option3;
        break;
      default:
        id2177x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2177out_result = (id2177x_1);
  }
  { // Node ID: 3461 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2187out_result;

  { // Node ID: 2187 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2187in_sel = id2957out_output[getCycle()%9];
    const HWFloat<8,24> &id2187in_option0 = id2177out_result;
    const HWFloat<8,24> &id2187in_option1 = id3461out_value;

    HWFloat<8,24> id2187x_1;

    switch((id2187in_sel.getValueAsLong())) {
      case 0l:
        id2187x_1 = id2187in_option0;
        break;
      case 1l:
        id2187x_1 = id2187in_option1;
        break;
      default:
        id2187x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2187out_result = (id2187x_1);
  }
  HWFloat<8,24> id2195out_result;

  { // Node ID: 2195 (NodeSub)
    const HWFloat<8,24> &id2195in_a = id3469out_value;
    const HWFloat<8,24> &id2195in_b = id2187out_result;

    id2195out_result = (sub_float(id2195in_a,id2195in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2199out_result;

  { // Node ID: 2199 (NodeEq)
    const HWFloat<8,24> &id2199in_a = id2195out_result;
    const HWFloat<8,24> &id2199in_b = id5out_value;

    id2199out_result = (eq_float(id2199in_a,id2199in_b));
  }
  { // Node ID: 2200 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2201out_result;

  { // Node ID: 2201 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2201in_sel = id2199out_result;
    const HWFloat<8,24> &id2201in_option0 = id2195out_result;
    const HWFloat<8,24> &id2201in_option1 = id2200out_value;

    HWFloat<8,24> id2201x_1;

    switch((id2201in_sel.getValueAsLong())) {
      case 0l:
        id2201x_1 = id2201in_option0;
        break;
      case 1l:
        id2201x_1 = id2201in_option1;
        break;
      default:
        id2201x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2201out_result = (id2201x_1);
  }
  HWFloat<8,24> id2300out_result;

  { // Node ID: 2300 (NodeDiv)
    const HWFloat<8,24> &id2300in_a = id2299out_result;
    const HWFloat<8,24> &id2300in_b = id2201out_result;

    id2300out_result = (div_float(id2300in_a,id2300in_b));
  }
  HWFloat<8,24> id2301out_result;

  { // Node ID: 2301 (NodeAdd)
    const HWFloat<8,24> &id2301in_a = id2295out_result;
    const HWFloat<8,24> &id2301in_b = id2300out_result;

    id2301out_result = (add_float(id2301in_a,id2301in_b));
  }
  HWFloat<8,24> id2303out_result;

  { // Node ID: 2303 (NodeMul)
    const HWFloat<8,24> &id2303in_a = id3497out_value;
    const HWFloat<8,24> &id2303in_b = id2301out_result;

    id2303out_result = (mul_float(id2303in_a,id2303in_b));
  }
  { // Node ID: 3460 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2346out_o;

  { // Node ID: 2346 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2346in_i = id2959out_output[getCycle()%3];

    id2346out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2346in_i));
  }
  { // Node ID: 2349 (NodeConstantRawBits)
  }
  { // Node ID: 2822 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id2332out_result;

  { // Node ID: 2332 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id2332in_a = id2389out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id2332in_b = id2960out_output[getCycle()%3];

    id2332out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2332in_a,id2332in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id2333out_result;

  { // Node ID: 2333 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id2333in_a = id2960out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id2333in_b = id2389out_dout[getCycle()%3];

    id2333out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id2333in_a,id2333in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id2334out_result;

  { // Node ID: 2334 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id2334in_a = id2332out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id2334in_b = id2333out_result;

    id2334out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id2334in_a,id2334in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2335out_o;

  { // Node ID: 2335 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id2335in_i = id2334out_result;

    id2335out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id2335in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2336out_result;

  { // Node ID: 2336 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id2336in_a = id2392out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id2336in_b = id2335out_o;

    id2336out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id2336in_a,id2336in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id2337out_result;

  { // Node ID: 2337 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id2337in_a = id2335out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id2337in_b = id2392out_dout[getCycle()%3];

    id2337out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id2337in_a,id2337in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id2338out_result;

  { // Node ID: 2338 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id2338in_a = id2336out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id2338in_b = id2337out_result;

    id2338out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id2338in_a,id2338in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2339out_o;

  { // Node ID: 2339 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id2339in_i = id2338out_result;

    id2339out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id2339in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id2340out_o;

  { // Node ID: 2340 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id2340in_i = id2339out_o;

    id2340out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id2340in_i));
  }
  { // Node ID: 3456 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2651out_result;

  { // Node ID: 2651 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2651in_a = id2340out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2651in_b = id3456out_value;

    id2651out_result = (gte_fixed(id2651in_a,id2651in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2886out_result;

  { // Node ID: 2886 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2886in_a = id2346out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2886in_b = id2349out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2886in_c = id2822out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2886in_condb = id2651out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2886x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2886x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2886x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2886x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2886x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2886x_1 = id2886in_a;
        break;
      default:
        id2886x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2886in_condb.getValueAsLong())) {
      case 0l:
        id2886x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2886x_2 = id2886in_b;
        break;
      default:
        id2886x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2886x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2886x_3 = id2886in_c;
        break;
      default:
        id2886x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2886x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2886x_1),(id2886x_2))),(id2886x_3)));
    id2886out_result = (id2886x_4);
  }
  HWRawBits<1> id2652out_result;

  { // Node ID: 2652 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2652in_a = id2886out_result;

    id2652out_result = (slice<10,1>(id2652in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2653out_output;

  { // Node ID: 2653 (NodeReinterpret)
    const HWRawBits<1> &id2653in_input = id2652out_result;

    id2653out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2653in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2355out_result;

  { // Node ID: 2355 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2355in_a = id2963out_output[getCycle()%3];

    id2355out_result = (not_fixed(id2355in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2356out_result;

  { // Node ID: 2356 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2356in_a = id2653out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2356in_b = id2355out_result;

    HWOffsetFix<1,0,UNSIGNED> id2356x_1;

    (id2356x_1) = (and_fixed(id2356in_a,id2356in_b));
    id2356out_result = (id2356x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2357out_result;

  { // Node ID: 2357 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2357in_a = id2356out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2357in_b = id2965out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2357x_1;

    (id2357x_1) = (or_fixed(id2357in_a,id2357in_b));
    id2357out_result = (id2357x_1);
  }
  { // Node ID: 3453 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2654out_result;

  { // Node ID: 2654 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2654in_a = id2886out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2654in_b = id3453out_value;

    id2654out_result = (gte_fixed(id2654in_a,id2654in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2364out_result;

  { // Node ID: 2364 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2364in_a = id2965out_output[getCycle()%3];

    id2364out_result = (not_fixed(id2364in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2365out_result;

  { // Node ID: 2365 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2365in_a = id2654out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2365in_b = id2364out_result;

    HWOffsetFix<1,0,UNSIGNED> id2365x_1;

    (id2365x_1) = (and_fixed(id2365in_a,id2365in_b));
    id2365out_result = (id2365x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2366out_result;

  { // Node ID: 2366 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2366in_a = id2365out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2366in_b = id2963out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2366x_1;

    (id2366x_1) = (or_fixed(id2366in_a,id2366in_b));
    id2366out_result = (id2366x_1);
  }
  HWRawBits<2> id2367out_result;

  { // Node ID: 2367 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2367in_in0 = id2357out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2367in_in1 = id2366out_result;

    id2367out_result = (cat(id2367in_in0,id2367in_in1));
  }
  { // Node ID: 2359 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2358out_o;

  { // Node ID: 2358 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2358in_i = id2886out_result;

    id2358out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2358in_i));
  }
  { // Node ID: 2343 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2344out_result;

  { // Node ID: 2344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2344in_sel = id2651out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id2344in_option0 = id2340out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2344in_option1 = id2343out_value;

    HWOffsetFix<24,-23,UNSIGNED> id2344x_1;

    switch((id2344in_sel.getValueAsLong())) {
      case 0l:
        id2344x_1 = id2344in_option0;
        break;
      case 1l:
        id2344x_1 = id2344in_option1;
        break;
      default:
        id2344x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id2344out_result = (id2344x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id2345out_o;

  { // Node ID: 2345 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id2345in_i = id2344out_result;

    id2345out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id2345in_i));
  }
  HWRawBits<32> id2360out_result;

  { // Node ID: 2360 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2360in_in0 = id2359out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2360in_in1 = id2358out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id2360in_in2 = id2345out_o;

    id2360out_result = (cat((cat(id2360in_in0,id2360in_in1)),id2360in_in2));
  }
  HWFloat<8,24> id2361out_output;

  { // Node ID: 2361 (NodeReinterpret)
    const HWRawBits<32> &id2361in_input = id2360out_result;

    id2361out_output = (cast_bits2float<8,24>(id2361in_input));
  }
  { // Node ID: 2368 (NodeConstantRawBits)
  }
  { // Node ID: 2369 (NodeConstantRawBits)
  }
  { // Node ID: 2371 (NodeConstantRawBits)
  }
  HWRawBits<32> id2655out_result;

  { // Node ID: 2655 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2655in_in0 = id2368out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2655in_in1 = id2369out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2655in_in2 = id2371out_value;

    id2655out_result = (cat((cat(id2655in_in0,id2655in_in1)),id2655in_in2));
  }
  HWFloat<8,24> id2373out_output;

  { // Node ID: 2373 (NodeReinterpret)
    const HWRawBits<32> &id2373in_input = id2655out_result;

    id2373out_output = (cast_bits2float<8,24>(id2373in_input));
  }
  { // Node ID: 2598 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2376out_result;

  { // Node ID: 2376 (NodeMux)
    const HWRawBits<2> &id2376in_sel = id2367out_result;
    const HWFloat<8,24> &id2376in_option0 = id2361out_output;
    const HWFloat<8,24> &id2376in_option1 = id2373out_output;
    const HWFloat<8,24> &id2376in_option2 = id2598out_value;
    const HWFloat<8,24> &id2376in_option3 = id2373out_output;

    HWFloat<8,24> id2376x_1;

    switch((id2376in_sel.getValueAsLong())) {
      case 0l:
        id2376x_1 = id2376in_option0;
        break;
      case 1l:
        id2376x_1 = id2376in_option1;
        break;
      case 2l:
        id2376x_1 = id2376in_option2;
        break;
      case 3l:
        id2376x_1 = id2376in_option3;
        break;
      default:
        id2376x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2376out_result = (id2376x_1);
  }
  { // Node ID: 3452 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2386out_result;

  { // Node ID: 2386 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2386in_sel = id2968out_output[getCycle()%9];
    const HWFloat<8,24> &id2386in_option0 = id2376out_result;
    const HWFloat<8,24> &id2386in_option1 = id3452out_value;

    HWFloat<8,24> id2386x_1;

    switch((id2386in_sel.getValueAsLong())) {
      case 0l:
        id2386x_1 = id2386in_option0;
        break;
      case 1l:
        id2386x_1 = id2386in_option1;
        break;
      default:
        id2386x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2386out_result = (id2386x_1);
  }
  { // Node ID: 3451 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2394out_result;

  { // Node ID: 2394 (NodeSub)
    const HWFloat<8,24> &id2394in_a = id2386out_result;
    const HWFloat<8,24> &id2394in_b = id3451out_value;

    id2394out_result = (sub_float(id2394in_a,id2394in_b));
  }
  HWFloat<8,24> id2396out_result;

  { // Node ID: 2396 (NodeMul)
    const HWFloat<8,24> &id2396in_a = id3460out_value;
    const HWFloat<8,24> &id2396in_b = id2394out_result;

    id2396out_result = (mul_float(id2396in_a,id2396in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2439out_o;

  { // Node ID: 2439 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2439in_i = id2970out_output[getCycle()%3];

    id2439out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2439in_i));
  }
  { // Node ID: 2442 (NodeConstantRawBits)
  }
  { // Node ID: 2824 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id2425out_result;

  { // Node ID: 2425 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id2425in_a = id2482out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id2425in_b = id2971out_output[getCycle()%3];

    id2425out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2425in_a,id2425in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id2426out_result;

  { // Node ID: 2426 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id2426in_a = id2971out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id2426in_b = id2482out_dout[getCycle()%3];

    id2426out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id2426in_a,id2426in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id2427out_result;

  { // Node ID: 2427 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id2427in_a = id2425out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id2427in_b = id2426out_result;

    id2427out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id2427in_a,id2427in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2428out_o;

  { // Node ID: 2428 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id2428in_i = id2427out_result;

    id2428out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id2428in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2429out_result;

  { // Node ID: 2429 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id2429in_a = id2485out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id2429in_b = id2428out_o;

    id2429out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id2429in_a,id2429in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id2430out_result;

  { // Node ID: 2430 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id2430in_a = id2428out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id2430in_b = id2485out_dout[getCycle()%3];

    id2430out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id2430in_a,id2430in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id2431out_result;

  { // Node ID: 2431 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id2431in_a = id2429out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id2431in_b = id2430out_result;

    id2431out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id2431in_a,id2431in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2432out_o;

  { // Node ID: 2432 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id2432in_i = id2431out_result;

    id2432out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id2432in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id2433out_o;

  { // Node ID: 2433 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id2433in_i = id2432out_o;

    id2433out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id2433in_i));
  }
  { // Node ID: 3447 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2658out_result;

  { // Node ID: 2658 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2658in_a = id2433out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2658in_b = id3447out_value;

    id2658out_result = (gte_fixed(id2658in_a,id2658in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2885out_result;

  { // Node ID: 2885 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2885in_a = id2439out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2885in_b = id2442out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2885in_c = id2824out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2885in_condb = id2658out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2885x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2885x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2885x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2885x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2885x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2885x_1 = id2885in_a;
        break;
      default:
        id2885x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2885in_condb.getValueAsLong())) {
      case 0l:
        id2885x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2885x_2 = id2885in_b;
        break;
      default:
        id2885x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2885x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2885x_3 = id2885in_c;
        break;
      default:
        id2885x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2885x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2885x_1),(id2885x_2))),(id2885x_3)));
    id2885out_result = (id2885x_4);
  }
  HWRawBits<1> id2659out_result;

  { // Node ID: 2659 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2659in_a = id2885out_result;

    id2659out_result = (slice<10,1>(id2659in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2660out_output;

  { // Node ID: 2660 (NodeReinterpret)
    const HWRawBits<1> &id2660in_input = id2659out_result;

    id2660out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2660in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2448out_result;

  { // Node ID: 2448 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2448in_a = id2974out_output[getCycle()%3];

    id2448out_result = (not_fixed(id2448in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2449out_result;

  { // Node ID: 2449 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2449in_a = id2660out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2449in_b = id2448out_result;

    HWOffsetFix<1,0,UNSIGNED> id2449x_1;

    (id2449x_1) = (and_fixed(id2449in_a,id2449in_b));
    id2449out_result = (id2449x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2450out_result;

  { // Node ID: 2450 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2450in_a = id2449out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2450in_b = id2976out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2450x_1;

    (id2450x_1) = (or_fixed(id2450in_a,id2450in_b));
    id2450out_result = (id2450x_1);
  }
  { // Node ID: 3444 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2661out_result;

  { // Node ID: 2661 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2661in_a = id2885out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2661in_b = id3444out_value;

    id2661out_result = (gte_fixed(id2661in_a,id2661in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2457out_result;

  { // Node ID: 2457 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2457in_a = id2976out_output[getCycle()%3];

    id2457out_result = (not_fixed(id2457in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2458out_result;

  { // Node ID: 2458 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2458in_a = id2661out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2458in_b = id2457out_result;

    HWOffsetFix<1,0,UNSIGNED> id2458x_1;

    (id2458x_1) = (and_fixed(id2458in_a,id2458in_b));
    id2458out_result = (id2458x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2459out_result;

  { // Node ID: 2459 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2459in_a = id2458out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2459in_b = id2974out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2459x_1;

    (id2459x_1) = (or_fixed(id2459in_a,id2459in_b));
    id2459out_result = (id2459x_1);
  }
  HWRawBits<2> id2460out_result;

  { // Node ID: 2460 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2460in_in0 = id2450out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2460in_in1 = id2459out_result;

    id2460out_result = (cat(id2460in_in0,id2460in_in1));
  }
  { // Node ID: 2452 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2451out_o;

  { // Node ID: 2451 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2451in_i = id2885out_result;

    id2451out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2451in_i));
  }
  { // Node ID: 2436 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2437out_result;

  { // Node ID: 2437 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2437in_sel = id2658out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id2437in_option0 = id2433out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2437in_option1 = id2436out_value;

    HWOffsetFix<24,-23,UNSIGNED> id2437x_1;

    switch((id2437in_sel.getValueAsLong())) {
      case 0l:
        id2437x_1 = id2437in_option0;
        break;
      case 1l:
        id2437x_1 = id2437in_option1;
        break;
      default:
        id2437x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id2437out_result = (id2437x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id2438out_o;

  { // Node ID: 2438 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id2438in_i = id2437out_result;

    id2438out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id2438in_i));
  }
  HWRawBits<32> id2453out_result;

  { // Node ID: 2453 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2453in_in0 = id2452out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2453in_in1 = id2451out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id2453in_in2 = id2438out_o;

    id2453out_result = (cat((cat(id2453in_in0,id2453in_in1)),id2453in_in2));
  }
  HWFloat<8,24> id2454out_output;

  { // Node ID: 2454 (NodeReinterpret)
    const HWRawBits<32> &id2454in_input = id2453out_result;

    id2454out_output = (cast_bits2float<8,24>(id2454in_input));
  }
  { // Node ID: 2461 (NodeConstantRawBits)
  }
  { // Node ID: 2462 (NodeConstantRawBits)
  }
  { // Node ID: 2464 (NodeConstantRawBits)
  }
  HWRawBits<32> id2662out_result;

  { // Node ID: 2662 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2662in_in0 = id2461out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2662in_in1 = id2462out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2662in_in2 = id2464out_value;

    id2662out_result = (cat((cat(id2662in_in0,id2662in_in1)),id2662in_in2));
  }
  HWFloat<8,24> id2466out_output;

  { // Node ID: 2466 (NodeReinterpret)
    const HWRawBits<32> &id2466in_input = id2662out_result;

    id2466out_output = (cast_bits2float<8,24>(id2466in_input));
  }
  { // Node ID: 2599 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2469out_result;

  { // Node ID: 2469 (NodeMux)
    const HWRawBits<2> &id2469in_sel = id2460out_result;
    const HWFloat<8,24> &id2469in_option0 = id2454out_output;
    const HWFloat<8,24> &id2469in_option1 = id2466out_output;
    const HWFloat<8,24> &id2469in_option2 = id2599out_value;
    const HWFloat<8,24> &id2469in_option3 = id2466out_output;

    HWFloat<8,24> id2469x_1;

    switch((id2469in_sel.getValueAsLong())) {
      case 0l:
        id2469x_1 = id2469in_option0;
        break;
      case 1l:
        id2469x_1 = id2469in_option1;
        break;
      case 2l:
        id2469x_1 = id2469in_option2;
        break;
      case 3l:
        id2469x_1 = id2469in_option3;
        break;
      default:
        id2469x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2469out_result = (id2469x_1);
  }
  { // Node ID: 3443 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2479out_result;

  { // Node ID: 2479 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2479in_sel = id2979out_output[getCycle()%9];
    const HWFloat<8,24> &id2479in_option0 = id2469out_result;
    const HWFloat<8,24> &id2479in_option1 = id3443out_value;

    HWFloat<8,24> id2479x_1;

    switch((id2479in_sel.getValueAsLong())) {
      case 0l:
        id2479x_1 = id2479in_option0;
        break;
      case 1l:
        id2479x_1 = id2479in_option1;
        break;
      default:
        id2479x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2479out_result = (id2479x_1);
  }
  HWFloat<8,24> id2486out_result;

  { // Node ID: 2486 (NodeDiv)
    const HWFloat<8,24> &id2486in_a = id2396out_result;
    const HWFloat<8,24> &id2486in_b = id2479out_result;

    id2486out_result = (div_float(id2486in_a,id2486in_b));
  }
  HWFloat<8,24> id2487out_result;

  { // Node ID: 2487 (NodeMul)
    const HWFloat<8,24> &id2487in_a = id2486out_result;
    const HWFloat<8,24> &id2487in_b = id2980out_output[getCycle()%2];

    id2487out_result = (mul_float(id2487in_a,id2487in_b));
  }
  HWFloat<8,24> id2496out_result;

  { // Node ID: 2496 (NodeAdd)
    const HWFloat<8,24> &id2496in_a = id2303out_result;
    const HWFloat<8,24> &id2496in_b = id2487out_result;

    id2496out_result = (add_float(id2496in_a,id2496in_b));
  }
  HWFloat<8,24> id2489out_result;

  { // Node ID: 2489 (NodeMul)
    const HWFloat<8,24> &id2489in_a = id2892out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id2489in_b = id3025out_output[getCycle()%2];

    id2489out_result = (mul_float(id2489in_a,id2489in_b));
  }
  HWFloat<8,24> id2490out_result;

  { // Node ID: 2490 (NodeMul)
    const HWFloat<8,24> &id2490in_a = id2489out_result;
    const HWFloat<8,24> &id2490in_b = id3025out_output[getCycle()%2];

    id2490out_result = (mul_float(id2490in_a,id2490in_b));
  }
  { // Node ID: 3382 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id644out_o;

  { // Node ID: 644 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id644in_i = id3052out_output[getCycle()%3];

    id644out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id644in_i));
  }
  { // Node ID: 647 (NodeConstantRawBits)
  }
  { // Node ID: 2838 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id630out_result;

  { // Node ID: 630 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id630in_a = id687out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id630in_b = id3053out_output[getCycle()%3];

    id630out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id630in_a,id630in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id631out_result;

  { // Node ID: 631 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id631in_a = id3053out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id631in_b = id687out_dout[getCycle()%3];

    id631out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id631in_a,id631in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id632out_result;

  { // Node ID: 632 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id632in_a = id630out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id632in_b = id631out_result;

    id632out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id632in_a,id632in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id633out_o;

  { // Node ID: 633 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id633in_i = id632out_result;

    id633out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id633in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id634out_result;

  { // Node ID: 634 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id634in_a = id690out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id634in_b = id633out_o;

    id634out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id634in_a,id634in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id635out_result;

  { // Node ID: 635 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id635in_a = id633out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id635in_b = id690out_dout[getCycle()%3];

    id635out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id635in_a,id635in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id636out_result;

  { // Node ID: 636 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id636in_a = id634out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id636in_b = id635out_result;

    id636out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id636in_a,id636in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id637out_o;

  { // Node ID: 637 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id637in_i = id636out_result;

    id637out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id637in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id638out_o;

  { // Node ID: 638 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id638in_i = id637out_o;

    id638out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id638in_i));
  }
  { // Node ID: 3379 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2710out_result;

  { // Node ID: 2710 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2710in_a = id638out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2710in_b = id3379out_value;

    id2710out_result = (gte_fixed(id2710in_a,id2710in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2878out_result;

  { // Node ID: 2878 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2878in_a = id644out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2878in_b = id647out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2878in_c = id2838out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2878in_condb = id2710out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2878x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2878x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2878x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2878x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2878x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2878x_1 = id2878in_a;
        break;
      default:
        id2878x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2878in_condb.getValueAsLong())) {
      case 0l:
        id2878x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2878x_2 = id2878in_b;
        break;
      default:
        id2878x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2878x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2878x_3 = id2878in_c;
        break;
      default:
        id2878x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2878x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2878x_1),(id2878x_2))),(id2878x_3)));
    id2878out_result = (id2878x_4);
  }
  HWRawBits<1> id2711out_result;

  { // Node ID: 2711 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2711in_a = id2878out_result;

    id2711out_result = (slice<10,1>(id2711in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2712out_output;

  { // Node ID: 2712 (NodeReinterpret)
    const HWRawBits<1> &id2712in_input = id2711out_result;

    id2712out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2712in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id653out_result;

  { // Node ID: 653 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id653in_a = id3056out_output[getCycle()%3];

    id653out_result = (not_fixed(id653in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id654out_result;

  { // Node ID: 654 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id654in_a = id2712out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id654in_b = id653out_result;

    HWOffsetFix<1,0,UNSIGNED> id654x_1;

    (id654x_1) = (and_fixed(id654in_a,id654in_b));
    id654out_result = (id654x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id655out_result;

  { // Node ID: 655 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id655in_a = id654out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id655in_b = id3058out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id655x_1;

    (id655x_1) = (or_fixed(id655in_a,id655in_b));
    id655out_result = (id655x_1);
  }
  { // Node ID: 3376 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2713out_result;

  { // Node ID: 2713 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2713in_a = id2878out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2713in_b = id3376out_value;

    id2713out_result = (gte_fixed(id2713in_a,id2713in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id662out_result;

  { // Node ID: 662 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id662in_a = id3058out_output[getCycle()%3];

    id662out_result = (not_fixed(id662in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id663out_result;

  { // Node ID: 663 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id663in_a = id2713out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id663in_b = id662out_result;

    HWOffsetFix<1,0,UNSIGNED> id663x_1;

    (id663x_1) = (and_fixed(id663in_a,id663in_b));
    id663out_result = (id663x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id664out_result;

  { // Node ID: 664 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_a = id663out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id664in_b = id3056out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id664x_1;

    (id664x_1) = (or_fixed(id664in_a,id664in_b));
    id664out_result = (id664x_1);
  }
  HWRawBits<2> id665out_result;

  { // Node ID: 665 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id665in_in0 = id655out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id665in_in1 = id664out_result;

    id665out_result = (cat(id665in_in0,id665in_in1));
  }
  { // Node ID: 657 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id656out_o;

  { // Node ID: 656 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id656in_i = id2878out_result;

    id656out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id656in_i));
  }
  { // Node ID: 641 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id642out_result;

  { // Node ID: 642 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id642in_sel = id2710out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id642in_option0 = id638out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id642in_option1 = id641out_value;

    HWOffsetFix<24,-23,UNSIGNED> id642x_1;

    switch((id642in_sel.getValueAsLong())) {
      case 0l:
        id642x_1 = id642in_option0;
        break;
      case 1l:
        id642x_1 = id642in_option1;
        break;
      default:
        id642x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id642out_result = (id642x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id643out_o;

  { // Node ID: 643 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id643in_i = id642out_result;

    id643out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id643in_i));
  }
  HWRawBits<32> id658out_result;

  { // Node ID: 658 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_in0 = id657out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id658in_in1 = id656out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id658in_in2 = id643out_o;

    id658out_result = (cat((cat(id658in_in0,id658in_in1)),id658in_in2));
  }
  HWFloat<8,24> id659out_output;

  { // Node ID: 659 (NodeReinterpret)
    const HWRawBits<32> &id659in_input = id658out_result;

    id659out_output = (cast_bits2float<8,24>(id659in_input));
  }
  { // Node ID: 666 (NodeConstantRawBits)
  }
  { // Node ID: 667 (NodeConstantRawBits)
  }
  { // Node ID: 669 (NodeConstantRawBits)
  }
  HWRawBits<32> id2714out_result;

  { // Node ID: 2714 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2714in_in0 = id666out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2714in_in1 = id667out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2714in_in2 = id669out_value;

    id2714out_result = (cat((cat(id2714in_in0,id2714in_in1)),id2714in_in2));
  }
  HWFloat<8,24> id671out_output;

  { // Node ID: 671 (NodeReinterpret)
    const HWRawBits<32> &id671in_input = id2714out_result;

    id671out_output = (cast_bits2float<8,24>(id671in_input));
  }
  { // Node ID: 2606 (NodeConstantRawBits)
  }
  HWFloat<8,24> id674out_result;

  { // Node ID: 674 (NodeMux)
    const HWRawBits<2> &id674in_sel = id665out_result;
    const HWFloat<8,24> &id674in_option0 = id659out_output;
    const HWFloat<8,24> &id674in_option1 = id671out_output;
    const HWFloat<8,24> &id674in_option2 = id2606out_value;
    const HWFloat<8,24> &id674in_option3 = id671out_output;

    HWFloat<8,24> id674x_1;

    switch((id674in_sel.getValueAsLong())) {
      case 0l:
        id674x_1 = id674in_option0;
        break;
      case 1l:
        id674x_1 = id674in_option1;
        break;
      case 2l:
        id674x_1 = id674in_option2;
        break;
      case 3l:
        id674x_1 = id674in_option3;
        break;
      default:
        id674x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id674out_result = (id674x_1);
  }
  { // Node ID: 3375 (NodeConstantRawBits)
  }
  HWFloat<8,24> id684out_result;

  { // Node ID: 684 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id684in_sel = id3061out_output[getCycle()%9];
    const HWFloat<8,24> &id684in_option0 = id674out_result;
    const HWFloat<8,24> &id684in_option1 = id3375out_value;

    HWFloat<8,24> id684x_1;

    switch((id684in_sel.getValueAsLong())) {
      case 0l:
        id684x_1 = id684in_option0;
        break;
      case 1l:
        id684x_1 = id684in_option1;
        break;
      default:
        id684x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id684out_result = (id684x_1);
  }
  HWFloat<8,24> id692out_result;

  { // Node ID: 692 (NodeMul)
    const HWFloat<8,24> &id692in_a = id3382out_value;
    const HWFloat<8,24> &id692in_b = id684out_result;

    id692out_result = (mul_float(id692in_a,id692in_b));
  }
  HWFloat<8,24> id693out_result;

  { // Node ID: 693 (NodeMul)
    const HWFloat<8,24> &id693in_a = id12out_dt;
    const HWFloat<8,24> &id693in_b = id692out_result;

    id693out_result = (mul_float(id693in_a,id693in_b));
  }
  { // Node ID: 3374 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1815out_result;

  { // Node ID: 1815 (NodeSub)
    const HWFloat<8,24> &id1815in_a = id3374out_value;
    const HWFloat<8,24> &id1815in_b = id3062out_output[getCycle()%10];

    id1815out_result = (sub_float(id1815in_a,id1815in_b));
  }
  HWFloat<8,24> id1816out_result;

  { // Node ID: 1816 (NodeMul)
    const HWFloat<8,24> &id1816in_a = id693out_result;
    const HWFloat<8,24> &id1816in_b = id1815out_result;

    id1816out_result = (mul_float(id1816in_a,id1816in_b));
  }
  { // Node ID: 3373 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id736out_o;

  { // Node ID: 736 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id736in_i = id3064out_output[getCycle()%3];

    id736out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id736in_i));
  }
  { // Node ID: 739 (NodeConstantRawBits)
  }
  { // Node ID: 2840 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id722out_result;

  { // Node ID: 722 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id722in_a = id779out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id722in_b = id3065out_output[getCycle()%3];

    id722out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id722in_a,id722in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id723out_result;

  { // Node ID: 723 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id723in_a = id3065out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id723in_b = id779out_dout[getCycle()%3];

    id723out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id723in_a,id723in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id724out_result;

  { // Node ID: 724 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id724in_a = id722out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id724in_b = id723out_result;

    id724out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id724in_a,id724in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id725out_o;

  { // Node ID: 725 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id725in_i = id724out_result;

    id725out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id725in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id726out_result;

  { // Node ID: 726 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id726in_a = id782out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id726in_b = id725out_o;

    id726out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id726in_a,id726in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id727out_result;

  { // Node ID: 727 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id727in_a = id725out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id727in_b = id782out_dout[getCycle()%3];

    id727out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id727in_a,id727in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id728out_result;

  { // Node ID: 728 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id728in_a = id726out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id728in_b = id727out_result;

    id728out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id728in_a,id728in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id729out_o;

  { // Node ID: 729 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id729in_i = id728out_result;

    id729out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id729in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id730out_o;

  { // Node ID: 730 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id730in_i = id729out_o;

    id730out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id730in_i));
  }
  { // Node ID: 3369 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2717out_result;

  { // Node ID: 2717 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2717in_a = id730out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2717in_b = id3369out_value;

    id2717out_result = (gte_fixed(id2717in_a,id2717in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2877out_result;

  { // Node ID: 2877 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2877in_a = id736out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2877in_b = id739out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2877in_c = id2840out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2877in_condb = id2717out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2877x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2877x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2877x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2877x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2877x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2877x_1 = id2877in_a;
        break;
      default:
        id2877x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2877in_condb.getValueAsLong())) {
      case 0l:
        id2877x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2877x_2 = id2877in_b;
        break;
      default:
        id2877x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2877x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2877x_3 = id2877in_c;
        break;
      default:
        id2877x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2877x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2877x_1),(id2877x_2))),(id2877x_3)));
    id2877out_result = (id2877x_4);
  }
  HWRawBits<1> id2718out_result;

  { // Node ID: 2718 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2718in_a = id2877out_result;

    id2718out_result = (slice<10,1>(id2718in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2719out_output;

  { // Node ID: 2719 (NodeReinterpret)
    const HWRawBits<1> &id2719in_input = id2718out_result;

    id2719out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2719in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id745out_result;

  { // Node ID: 745 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id745in_a = id3068out_output[getCycle()%3];

    id745out_result = (not_fixed(id745in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id746out_result;

  { // Node ID: 746 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id746in_a = id2719out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id746in_b = id745out_result;

    HWOffsetFix<1,0,UNSIGNED> id746x_1;

    (id746x_1) = (and_fixed(id746in_a,id746in_b));
    id746out_result = (id746x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id747out_result;

  { // Node ID: 747 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id747in_a = id746out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id747in_b = id3070out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id747x_1;

    (id747x_1) = (or_fixed(id747in_a,id747in_b));
    id747out_result = (id747x_1);
  }
  { // Node ID: 3366 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2720out_result;

  { // Node ID: 2720 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2720in_a = id2877out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2720in_b = id3366out_value;

    id2720out_result = (gte_fixed(id2720in_a,id2720in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id754out_result;

  { // Node ID: 754 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id754in_a = id3070out_output[getCycle()%3];

    id754out_result = (not_fixed(id754in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id755out_result;

  { // Node ID: 755 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id755in_a = id2720out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id755in_b = id754out_result;

    HWOffsetFix<1,0,UNSIGNED> id755x_1;

    (id755x_1) = (and_fixed(id755in_a,id755in_b));
    id755out_result = (id755x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id756out_result;

  { // Node ID: 756 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id756in_a = id755out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id756in_b = id3068out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id756x_1;

    (id756x_1) = (or_fixed(id756in_a,id756in_b));
    id756out_result = (id756x_1);
  }
  HWRawBits<2> id757out_result;

  { // Node ID: 757 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id757in_in0 = id747out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id757in_in1 = id756out_result;

    id757out_result = (cat(id757in_in0,id757in_in1));
  }
  { // Node ID: 749 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id748out_o;

  { // Node ID: 748 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id748in_i = id2877out_result;

    id748out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id748in_i));
  }
  { // Node ID: 733 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id734out_result;

  { // Node ID: 734 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id734in_sel = id2717out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id734in_option0 = id730out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id734in_option1 = id733out_value;

    HWOffsetFix<24,-23,UNSIGNED> id734x_1;

    switch((id734in_sel.getValueAsLong())) {
      case 0l:
        id734x_1 = id734in_option0;
        break;
      case 1l:
        id734x_1 = id734in_option1;
        break;
      default:
        id734x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id734out_result = (id734x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id735out_o;

  { // Node ID: 735 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id735in_i = id734out_result;

    id735out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id735in_i));
  }
  HWRawBits<32> id750out_result;

  { // Node ID: 750 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id750in_in0 = id749out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id750in_in1 = id748out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id750in_in2 = id735out_o;

    id750out_result = (cat((cat(id750in_in0,id750in_in1)),id750in_in2));
  }
  HWFloat<8,24> id751out_output;

  { // Node ID: 751 (NodeReinterpret)
    const HWRawBits<32> &id751in_input = id750out_result;

    id751out_output = (cast_bits2float<8,24>(id751in_input));
  }
  { // Node ID: 758 (NodeConstantRawBits)
  }
  { // Node ID: 759 (NodeConstantRawBits)
  }
  { // Node ID: 761 (NodeConstantRawBits)
  }
  HWRawBits<32> id2721out_result;

  { // Node ID: 2721 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2721in_in0 = id758out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2721in_in1 = id759out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2721in_in2 = id761out_value;

    id2721out_result = (cat((cat(id2721in_in0,id2721in_in1)),id2721in_in2));
  }
  HWFloat<8,24> id763out_output;

  { // Node ID: 763 (NodeReinterpret)
    const HWRawBits<32> &id763in_input = id2721out_result;

    id763out_output = (cast_bits2float<8,24>(id763in_input));
  }
  { // Node ID: 2607 (NodeConstantRawBits)
  }
  HWFloat<8,24> id766out_result;

  { // Node ID: 766 (NodeMux)
    const HWRawBits<2> &id766in_sel = id757out_result;
    const HWFloat<8,24> &id766in_option0 = id751out_output;
    const HWFloat<8,24> &id766in_option1 = id763out_output;
    const HWFloat<8,24> &id766in_option2 = id2607out_value;
    const HWFloat<8,24> &id766in_option3 = id763out_output;

    HWFloat<8,24> id766x_1;

    switch((id766in_sel.getValueAsLong())) {
      case 0l:
        id766x_1 = id766in_option0;
        break;
      case 1l:
        id766x_1 = id766in_option1;
        break;
      case 2l:
        id766x_1 = id766in_option2;
        break;
      case 3l:
        id766x_1 = id766in_option3;
        break;
      default:
        id766x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id766out_result = (id766x_1);
  }
  { // Node ID: 3365 (NodeConstantRawBits)
  }
  HWFloat<8,24> id776out_result;

  { // Node ID: 776 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id776in_sel = id3073out_output[getCycle()%9];
    const HWFloat<8,24> &id776in_option0 = id766out_result;
    const HWFloat<8,24> &id776in_option1 = id3365out_value;

    HWFloat<8,24> id776x_1;

    switch((id776in_sel.getValueAsLong())) {
      case 0l:
        id776x_1 = id776in_option0;
        break;
      case 1l:
        id776x_1 = id776in_option1;
        break;
      default:
        id776x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id776out_result = (id776x_1);
  }
  { // Node ID: 3364 (NodeConstantRawBits)
  }
  HWFloat<8,24> id784out_result;

  { // Node ID: 784 (NodeAdd)
    const HWFloat<8,24> &id784in_a = id776out_result;
    const HWFloat<8,24> &id784in_b = id3364out_value;

    id784out_result = (add_float(id784in_a,id784in_b));
  }
  HWFloat<8,24> id786out_result;

  { // Node ID: 786 (NodeDiv)
    const HWFloat<8,24> &id786in_a = id3373out_value;
    const HWFloat<8,24> &id786in_b = id784out_result;

    id786out_result = (div_float(id786in_a,id786in_b));
  }
  HWFloat<8,24> id787out_result;

  { // Node ID: 787 (NodeMul)
    const HWFloat<8,24> &id787in_a = id12out_dt;
    const HWFloat<8,24> &id787in_b = id786out_result;

    id787out_result = (mul_float(id787in_a,id787in_b));
  }
  HWFloat<8,24> id1817out_result;

  { // Node ID: 1817 (NodeMul)
    const HWFloat<8,24> &id1817in_a = id787out_result;
    const HWFloat<8,24> &id1817in_b = id3062out_output[getCycle()%10];

    id1817out_result = (mul_float(id1817in_a,id1817in_b));
  }
  HWFloat<8,24> id1818out_result;

  { // Node ID: 1818 (NodeSub)
    const HWFloat<8,24> &id1818in_a = id1816out_result;
    const HWFloat<8,24> &id1818in_b = id1817out_result;

    id1818out_result = (sub_float(id1818in_a,id1818in_b));
  }
  HWFloat<8,24> id1819out_result;

  { // Node ID: 1819 (NodeAdd)
    const HWFloat<8,24> &id1819in_a = id3062out_output[getCycle()%10];
    const HWFloat<8,24> &id1819in_b = id1818out_result;

    id1819out_result = (add_float(id1819in_a,id1819in_b));
  }
  HWFloat<8,24> id2491out_result;

  { // Node ID: 2491 (NodeMul)
    const HWFloat<8,24> &id2491in_a = id2490out_result;
    const HWFloat<8,24> &id2491in_b = id1819out_result;

    id2491out_result = (mul_float(id2491in_a,id2491in_b));
  }
  { // Node ID: 3362 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id830out_o;

  { // Node ID: 830 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id830in_i = id3076out_output[getCycle()%3];

    id830out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id830in_i));
  }
  { // Node ID: 833 (NodeConstantRawBits)
  }
  { // Node ID: 2842 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id816out_result;

  { // Node ID: 816 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id816in_a = id873out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id816in_b = id3077out_output[getCycle()%3];

    id816out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id816in_a,id816in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id817out_result;

  { // Node ID: 817 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id817in_a = id3077out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id817in_b = id873out_dout[getCycle()%3];

    id817out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id817in_a,id817in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id818out_result;

  { // Node ID: 818 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id818in_a = id816out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id818in_b = id817out_result;

    id818out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id818in_a,id818in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id819out_o;

  { // Node ID: 819 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id819in_i = id818out_result;

    id819out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id819in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id820out_result;

  { // Node ID: 820 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id820in_a = id876out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id820in_b = id819out_o;

    id820out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id820in_a,id820in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id821out_result;

  { // Node ID: 821 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id821in_a = id819out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id821in_b = id876out_dout[getCycle()%3];

    id821out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id821in_a,id821in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id822out_result;

  { // Node ID: 822 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id822in_a = id820out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id822in_b = id821out_result;

    id822out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id822in_a,id822in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id823out_o;

  { // Node ID: 823 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id823in_i = id822out_result;

    id823out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id823in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id824out_o;

  { // Node ID: 824 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id824in_i = id823out_o;

    id824out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id824in_i));
  }
  { // Node ID: 3359 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2725out_result;

  { // Node ID: 2725 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2725in_a = id824out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2725in_b = id3359out_value;

    id2725out_result = (gte_fixed(id2725in_a,id2725in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2876out_result;

  { // Node ID: 2876 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2876in_a = id830out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2876in_b = id833out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2876in_c = id2842out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2876in_condb = id2725out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2876x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2876x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2876x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2876x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2876x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2876x_1 = id2876in_a;
        break;
      default:
        id2876x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2876in_condb.getValueAsLong())) {
      case 0l:
        id2876x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2876x_2 = id2876in_b;
        break;
      default:
        id2876x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2876x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2876x_3 = id2876in_c;
        break;
      default:
        id2876x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2876x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2876x_1),(id2876x_2))),(id2876x_3)));
    id2876out_result = (id2876x_4);
  }
  HWRawBits<1> id2726out_result;

  { // Node ID: 2726 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2726in_a = id2876out_result;

    id2726out_result = (slice<10,1>(id2726in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2727out_output;

  { // Node ID: 2727 (NodeReinterpret)
    const HWRawBits<1> &id2727in_input = id2726out_result;

    id2727out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2727in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id839out_result;

  { // Node ID: 839 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id839in_a = id3080out_output[getCycle()%3];

    id839out_result = (not_fixed(id839in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id840out_result;

  { // Node ID: 840 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id840in_a = id2727out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id840in_b = id839out_result;

    HWOffsetFix<1,0,UNSIGNED> id840x_1;

    (id840x_1) = (and_fixed(id840in_a,id840in_b));
    id840out_result = (id840x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id841out_result;

  { // Node ID: 841 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id841in_a = id840out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id841in_b = id3082out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id841x_1;

    (id841x_1) = (or_fixed(id841in_a,id841in_b));
    id841out_result = (id841x_1);
  }
  { // Node ID: 3356 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2728out_result;

  { // Node ID: 2728 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2728in_a = id2876out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2728in_b = id3356out_value;

    id2728out_result = (gte_fixed(id2728in_a,id2728in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id848out_result;

  { // Node ID: 848 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id848in_a = id3082out_output[getCycle()%3];

    id848out_result = (not_fixed(id848in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id849out_result;

  { // Node ID: 849 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id849in_a = id2728out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id849in_b = id848out_result;

    HWOffsetFix<1,0,UNSIGNED> id849x_1;

    (id849x_1) = (and_fixed(id849in_a,id849in_b));
    id849out_result = (id849x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id850out_result;

  { // Node ID: 850 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id850in_a = id849out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id850in_b = id3080out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id850x_1;

    (id850x_1) = (or_fixed(id850in_a,id850in_b));
    id850out_result = (id850x_1);
  }
  HWRawBits<2> id851out_result;

  { // Node ID: 851 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id851in_in0 = id841out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id851in_in1 = id850out_result;

    id851out_result = (cat(id851in_in0,id851in_in1));
  }
  { // Node ID: 843 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id842out_o;

  { // Node ID: 842 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id842in_i = id2876out_result;

    id842out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id842in_i));
  }
  { // Node ID: 827 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id828out_result;

  { // Node ID: 828 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id828in_sel = id2725out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id828in_option0 = id824out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id828in_option1 = id827out_value;

    HWOffsetFix<24,-23,UNSIGNED> id828x_1;

    switch((id828in_sel.getValueAsLong())) {
      case 0l:
        id828x_1 = id828in_option0;
        break;
      case 1l:
        id828x_1 = id828in_option1;
        break;
      default:
        id828x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id828out_result = (id828x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id829out_o;

  { // Node ID: 829 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id829in_i = id828out_result;

    id829out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id829in_i));
  }
  HWRawBits<32> id844out_result;

  { // Node ID: 844 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id844in_in0 = id843out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id844in_in1 = id842out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id844in_in2 = id829out_o;

    id844out_result = (cat((cat(id844in_in0,id844in_in1)),id844in_in2));
  }
  HWFloat<8,24> id845out_output;

  { // Node ID: 845 (NodeReinterpret)
    const HWRawBits<32> &id845in_input = id844out_result;

    id845out_output = (cast_bits2float<8,24>(id845in_input));
  }
  { // Node ID: 852 (NodeConstantRawBits)
  }
  { // Node ID: 853 (NodeConstantRawBits)
  }
  { // Node ID: 855 (NodeConstantRawBits)
  }
  HWRawBits<32> id2729out_result;

  { // Node ID: 2729 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2729in_in0 = id852out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2729in_in1 = id853out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2729in_in2 = id855out_value;

    id2729out_result = (cat((cat(id2729in_in0,id2729in_in1)),id2729in_in2));
  }
  HWFloat<8,24> id857out_output;

  { // Node ID: 857 (NodeReinterpret)
    const HWRawBits<32> &id857in_input = id2729out_result;

    id857out_output = (cast_bits2float<8,24>(id857in_input));
  }
  { // Node ID: 2608 (NodeConstantRawBits)
  }
  HWFloat<8,24> id860out_result;

  { // Node ID: 860 (NodeMux)
    const HWRawBits<2> &id860in_sel = id851out_result;
    const HWFloat<8,24> &id860in_option0 = id845out_output;
    const HWFloat<8,24> &id860in_option1 = id857out_output;
    const HWFloat<8,24> &id860in_option2 = id2608out_value;
    const HWFloat<8,24> &id860in_option3 = id857out_output;

    HWFloat<8,24> id860x_1;

    switch((id860in_sel.getValueAsLong())) {
      case 0l:
        id860x_1 = id860in_option0;
        break;
      case 1l:
        id860x_1 = id860in_option1;
        break;
      case 2l:
        id860x_1 = id860in_option2;
        break;
      case 3l:
        id860x_1 = id860in_option3;
        break;
      default:
        id860x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id860out_result = (id860x_1);
  }
  { // Node ID: 3355 (NodeConstantRawBits)
  }
  HWFloat<8,24> id870out_result;

  { // Node ID: 870 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id870in_sel = id3085out_output[getCycle()%9];
    const HWFloat<8,24> &id870in_option0 = id860out_result;
    const HWFloat<8,24> &id870in_option1 = id3355out_value;

    HWFloat<8,24> id870x_1;

    switch((id870in_sel.getValueAsLong())) {
      case 0l:
        id870x_1 = id870in_option0;
        break;
      case 1l:
        id870x_1 = id870in_option1;
        break;
      default:
        id870x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id870out_result = (id870x_1);
  }
  HWFloat<8,24> id878out_result;

  { // Node ID: 878 (NodeMul)
    const HWFloat<8,24> &id878in_a = id3362out_value;
    const HWFloat<8,24> &id878in_b = id870out_result;

    id878out_result = (mul_float(id878in_a,id878in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id921out_o;

  { // Node ID: 921 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id921in_i = id3087out_output[getCycle()%3];

    id921out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id921in_i));
  }
  { // Node ID: 924 (NodeConstantRawBits)
  }
  { // Node ID: 2844 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id907out_result;

  { // Node ID: 907 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id907in_a = id964out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id907in_b = id3088out_output[getCycle()%3];

    id907out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id907in_a,id907in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id908out_result;

  { // Node ID: 908 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id908in_a = id3088out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id908in_b = id964out_dout[getCycle()%3];

    id908out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id908in_a,id908in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id909out_result;

  { // Node ID: 909 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id909in_a = id907out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id909in_b = id908out_result;

    id909out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id909in_a,id909in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id910out_o;

  { // Node ID: 910 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id910in_i = id909out_result;

    id910out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id910in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id911out_result;

  { // Node ID: 911 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id911in_a = id967out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id911in_b = id910out_o;

    id911out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id911in_a,id911in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id912out_result;

  { // Node ID: 912 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id912in_a = id910out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id912in_b = id967out_dout[getCycle()%3];

    id912out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id912in_a,id912in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id913out_result;

  { // Node ID: 913 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id913in_a = id911out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id913in_b = id912out_result;

    id913out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id913in_a,id913in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id914out_o;

  { // Node ID: 914 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id914in_i = id913out_result;

    id914out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id914in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id915out_o;

  { // Node ID: 915 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id915in_i = id914out_o;

    id915out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id915in_i));
  }
  { // Node ID: 3351 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2732out_result;

  { // Node ID: 2732 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2732in_a = id915out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2732in_b = id3351out_value;

    id2732out_result = (gte_fixed(id2732in_a,id2732in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2875out_result;

  { // Node ID: 2875 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2875in_a = id921out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2875in_b = id924out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2875in_c = id2844out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2875in_condb = id2732out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2875x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2875x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2875x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2875x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2875x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2875x_1 = id2875in_a;
        break;
      default:
        id2875x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2875in_condb.getValueAsLong())) {
      case 0l:
        id2875x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2875x_2 = id2875in_b;
        break;
      default:
        id2875x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2875x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2875x_3 = id2875in_c;
        break;
      default:
        id2875x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2875x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2875x_1),(id2875x_2))),(id2875x_3)));
    id2875out_result = (id2875x_4);
  }
  HWRawBits<1> id2733out_result;

  { // Node ID: 2733 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2733in_a = id2875out_result;

    id2733out_result = (slice<10,1>(id2733in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2734out_output;

  { // Node ID: 2734 (NodeReinterpret)
    const HWRawBits<1> &id2734in_input = id2733out_result;

    id2734out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2734in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id930out_result;

  { // Node ID: 930 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id930in_a = id3091out_output[getCycle()%3];

    id930out_result = (not_fixed(id930in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id931out_result;

  { // Node ID: 931 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id931in_a = id2734out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id931in_b = id930out_result;

    HWOffsetFix<1,0,UNSIGNED> id931x_1;

    (id931x_1) = (and_fixed(id931in_a,id931in_b));
    id931out_result = (id931x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id932out_result;

  { // Node ID: 932 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id932in_a = id931out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id932in_b = id3093out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id932x_1;

    (id932x_1) = (or_fixed(id932in_a,id932in_b));
    id932out_result = (id932x_1);
  }
  { // Node ID: 3348 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2735out_result;

  { // Node ID: 2735 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2735in_a = id2875out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2735in_b = id3348out_value;

    id2735out_result = (gte_fixed(id2735in_a,id2735in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id939out_result;

  { // Node ID: 939 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id939in_a = id3093out_output[getCycle()%3];

    id939out_result = (not_fixed(id939in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id940out_result;

  { // Node ID: 940 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id940in_a = id2735out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id940in_b = id939out_result;

    HWOffsetFix<1,0,UNSIGNED> id940x_1;

    (id940x_1) = (and_fixed(id940in_a,id940in_b));
    id940out_result = (id940x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id941out_result;

  { // Node ID: 941 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id941in_a = id940out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id941in_b = id3091out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id941x_1;

    (id941x_1) = (or_fixed(id941in_a,id941in_b));
    id941out_result = (id941x_1);
  }
  HWRawBits<2> id942out_result;

  { // Node ID: 942 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id942in_in0 = id932out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id942in_in1 = id941out_result;

    id942out_result = (cat(id942in_in0,id942in_in1));
  }
  { // Node ID: 934 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id933out_o;

  { // Node ID: 933 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id933in_i = id2875out_result;

    id933out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id933in_i));
  }
  { // Node ID: 918 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id919out_result;

  { // Node ID: 919 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id919in_sel = id2732out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id919in_option0 = id915out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id919in_option1 = id918out_value;

    HWOffsetFix<24,-23,UNSIGNED> id919x_1;

    switch((id919in_sel.getValueAsLong())) {
      case 0l:
        id919x_1 = id919in_option0;
        break;
      case 1l:
        id919x_1 = id919in_option1;
        break;
      default:
        id919x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id919out_result = (id919x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id920out_o;

  { // Node ID: 920 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id920in_i = id919out_result;

    id920out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id920in_i));
  }
  HWRawBits<32> id935out_result;

  { // Node ID: 935 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id935in_in0 = id934out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id935in_in1 = id933out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id935in_in2 = id920out_o;

    id935out_result = (cat((cat(id935in_in0,id935in_in1)),id935in_in2));
  }
  HWFloat<8,24> id936out_output;

  { // Node ID: 936 (NodeReinterpret)
    const HWRawBits<32> &id936in_input = id935out_result;

    id936out_output = (cast_bits2float<8,24>(id936in_input));
  }
  { // Node ID: 943 (NodeConstantRawBits)
  }
  { // Node ID: 944 (NodeConstantRawBits)
  }
  { // Node ID: 946 (NodeConstantRawBits)
  }
  HWRawBits<32> id2736out_result;

  { // Node ID: 2736 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2736in_in0 = id943out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2736in_in1 = id944out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2736in_in2 = id946out_value;

    id2736out_result = (cat((cat(id2736in_in0,id2736in_in1)),id2736in_in2));
  }
  HWFloat<8,24> id948out_output;

  { // Node ID: 948 (NodeReinterpret)
    const HWRawBits<32> &id948in_input = id2736out_result;

    id948out_output = (cast_bits2float<8,24>(id948in_input));
  }
  { // Node ID: 2609 (NodeConstantRawBits)
  }
  HWFloat<8,24> id951out_result;

  { // Node ID: 951 (NodeMux)
    const HWRawBits<2> &id951in_sel = id942out_result;
    const HWFloat<8,24> &id951in_option0 = id936out_output;
    const HWFloat<8,24> &id951in_option1 = id948out_output;
    const HWFloat<8,24> &id951in_option2 = id2609out_value;
    const HWFloat<8,24> &id951in_option3 = id948out_output;

    HWFloat<8,24> id951x_1;

    switch((id951in_sel.getValueAsLong())) {
      case 0l:
        id951x_1 = id951in_option0;
        break;
      case 1l:
        id951x_1 = id951in_option1;
        break;
      case 2l:
        id951x_1 = id951in_option2;
        break;
      case 3l:
        id951x_1 = id951in_option3;
        break;
      default:
        id951x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id951out_result = (id951x_1);
  }
  { // Node ID: 3347 (NodeConstantRawBits)
  }
  HWFloat<8,24> id961out_result;

  { // Node ID: 961 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id961in_sel = id3096out_output[getCycle()%9];
    const HWFloat<8,24> &id961in_option0 = id951out_result;
    const HWFloat<8,24> &id961in_option1 = id3347out_value;

    HWFloat<8,24> id961x_1;

    switch((id961in_sel.getValueAsLong())) {
      case 0l:
        id961x_1 = id961in_option0;
        break;
      case 1l:
        id961x_1 = id961in_option1;
        break;
      default:
        id961x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id961out_result = (id961x_1);
  }
  { // Node ID: 3346 (NodeConstantRawBits)
  }
  HWFloat<8,24> id969out_result;

  { // Node ID: 969 (NodeAdd)
    const HWFloat<8,24> &id969in_a = id961out_result;
    const HWFloat<8,24> &id969in_b = id3346out_value;

    id969out_result = (add_float(id969in_a,id969in_b));
  }
  HWFloat<8,24> id970out_result;

  { // Node ID: 970 (NodeDiv)
    const HWFloat<8,24> &id970in_a = id878out_result;
    const HWFloat<8,24> &id970in_b = id969out_result;

    id970out_result = (div_float(id970in_a,id970in_b));
  }
  HWFloat<8,24> id971out_result;

  { // Node ID: 971 (NodeMul)
    const HWFloat<8,24> &id971in_a = id12out_dt;
    const HWFloat<8,24> &id971in_b = id970out_result;

    id971out_result = (mul_float(id971in_a,id971in_b));
  }
  { // Node ID: 3345 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1821out_result;

  { // Node ID: 1821 (NodeSub)
    const HWFloat<8,24> &id1821in_a = id3345out_value;
    const HWFloat<8,24> &id1821in_b = id3097out_output[getCycle()%10];

    id1821out_result = (sub_float(id1821in_a,id1821in_b));
  }
  HWFloat<8,24> id1822out_result;

  { // Node ID: 1822 (NodeMul)
    const HWFloat<8,24> &id1822in_a = id971out_result;
    const HWFloat<8,24> &id1822in_b = id1821out_result;

    id1822out_result = (mul_float(id1822in_a,id1822in_b));
  }
  { // Node ID: 3344 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1014out_o;

  { // Node ID: 1014 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1014in_i = id3099out_output[getCycle()%3];

    id1014out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1014in_i));
  }
  { // Node ID: 1017 (NodeConstantRawBits)
  }
  { // Node ID: 2846 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id1000out_result;

  { // Node ID: 1000 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1000in_a = id1057out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1000in_b = id3100out_output[getCycle()%3];

    id1000out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1000in_a,id1000in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1001out_result;

  { // Node ID: 1001 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1001in_a = id3100out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1001in_b = id1057out_dout[getCycle()%3];

    id1001out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1001in_a,id1001in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1002out_result;

  { // Node ID: 1002 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1002in_a = id1000out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1002in_b = id1001out_result;

    id1002out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1002in_a,id1002in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1003out_o;

  { // Node ID: 1003 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1003in_i = id1002out_result;

    id1003out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1003in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1004out_result;

  { // Node ID: 1004 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1004in_a = id1060out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1004in_b = id1003out_o;

    id1004out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1004in_a,id1004in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1005out_result;

  { // Node ID: 1005 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1005in_a = id1003out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1005in_b = id1060out_dout[getCycle()%3];

    id1005out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1005in_a,id1005in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1006out_result;

  { // Node ID: 1006 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1006in_a = id1004out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1006in_b = id1005out_result;

    id1006out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1006in_a,id1006in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1007out_o;

  { // Node ID: 1007 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1007in_i = id1006out_result;

    id1007out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1007in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1008out_o;

  { // Node ID: 1008 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1008in_i = id1007out_o;

    id1008out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1008in_i));
  }
  { // Node ID: 3340 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2739out_result;

  { // Node ID: 2739 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2739in_a = id1008out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2739in_b = id3340out_value;

    id2739out_result = (gte_fixed(id2739in_a,id2739in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2874out_result;

  { // Node ID: 2874 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2874in_a = id1014out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2874in_b = id1017out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2874in_c = id2846out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2874in_condb = id2739out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2874x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2874x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2874x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2874x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2874x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2874x_1 = id2874in_a;
        break;
      default:
        id2874x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2874in_condb.getValueAsLong())) {
      case 0l:
        id2874x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2874x_2 = id2874in_b;
        break;
      default:
        id2874x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2874x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2874x_3 = id2874in_c;
        break;
      default:
        id2874x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2874x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2874x_1),(id2874x_2))),(id2874x_3)));
    id2874out_result = (id2874x_4);
  }
  HWRawBits<1> id2740out_result;

  { // Node ID: 2740 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2740in_a = id2874out_result;

    id2740out_result = (slice<10,1>(id2740in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2741out_output;

  { // Node ID: 2741 (NodeReinterpret)
    const HWRawBits<1> &id2741in_input = id2740out_result;

    id2741out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2741in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1023out_result;

  { // Node ID: 1023 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1023in_a = id3103out_output[getCycle()%3];

    id1023out_result = (not_fixed(id1023in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1024out_result;

  { // Node ID: 1024 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1024in_a = id2741out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1024in_b = id1023out_result;

    HWOffsetFix<1,0,UNSIGNED> id1024x_1;

    (id1024x_1) = (and_fixed(id1024in_a,id1024in_b));
    id1024out_result = (id1024x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1025out_result;

  { // Node ID: 1025 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1025in_a = id1024out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1025in_b = id3105out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1025x_1;

    (id1025x_1) = (or_fixed(id1025in_a,id1025in_b));
    id1025out_result = (id1025x_1);
  }
  { // Node ID: 3337 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2742out_result;

  { // Node ID: 2742 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2742in_a = id2874out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2742in_b = id3337out_value;

    id2742out_result = (gte_fixed(id2742in_a,id2742in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1032out_result;

  { // Node ID: 1032 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1032in_a = id3105out_output[getCycle()%3];

    id1032out_result = (not_fixed(id1032in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1033out_result;

  { // Node ID: 1033 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1033in_a = id2742out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1033in_b = id1032out_result;

    HWOffsetFix<1,0,UNSIGNED> id1033x_1;

    (id1033x_1) = (and_fixed(id1033in_a,id1033in_b));
    id1033out_result = (id1033x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1034out_result;

  { // Node ID: 1034 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1034in_a = id1033out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1034in_b = id3103out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1034x_1;

    (id1034x_1) = (or_fixed(id1034in_a,id1034in_b));
    id1034out_result = (id1034x_1);
  }
  HWRawBits<2> id1035out_result;

  { // Node ID: 1035 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1035in_in0 = id1025out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1035in_in1 = id1034out_result;

    id1035out_result = (cat(id1035in_in0,id1035in_in1));
  }
  { // Node ID: 1027 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1026out_o;

  { // Node ID: 1026 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1026in_i = id2874out_result;

    id1026out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1026in_i));
  }
  { // Node ID: 1011 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1012out_result;

  { // Node ID: 1012 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1012in_sel = id2739out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1012in_option0 = id1008out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1012in_option1 = id1011out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1012x_1;

    switch((id1012in_sel.getValueAsLong())) {
      case 0l:
        id1012x_1 = id1012in_option0;
        break;
      case 1l:
        id1012x_1 = id1012in_option1;
        break;
      default:
        id1012x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1012out_result = (id1012x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1013out_o;

  { // Node ID: 1013 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1013in_i = id1012out_result;

    id1013out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1013in_i));
  }
  HWRawBits<32> id1028out_result;

  { // Node ID: 1028 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1028in_in0 = id1027out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1028in_in1 = id1026out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1028in_in2 = id1013out_o;

    id1028out_result = (cat((cat(id1028in_in0,id1028in_in1)),id1028in_in2));
  }
  HWFloat<8,24> id1029out_output;

  { // Node ID: 1029 (NodeReinterpret)
    const HWRawBits<32> &id1029in_input = id1028out_result;

    id1029out_output = (cast_bits2float<8,24>(id1029in_input));
  }
  { // Node ID: 1036 (NodeConstantRawBits)
  }
  { // Node ID: 1037 (NodeConstantRawBits)
  }
  { // Node ID: 1039 (NodeConstantRawBits)
  }
  HWRawBits<32> id2743out_result;

  { // Node ID: 2743 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2743in_in0 = id1036out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2743in_in1 = id1037out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2743in_in2 = id1039out_value;

    id2743out_result = (cat((cat(id2743in_in0,id2743in_in1)),id2743in_in2));
  }
  HWFloat<8,24> id1041out_output;

  { // Node ID: 1041 (NodeReinterpret)
    const HWRawBits<32> &id1041in_input = id2743out_result;

    id1041out_output = (cast_bits2float<8,24>(id1041in_input));
  }
  { // Node ID: 2610 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1044out_result;

  { // Node ID: 1044 (NodeMux)
    const HWRawBits<2> &id1044in_sel = id1035out_result;
    const HWFloat<8,24> &id1044in_option0 = id1029out_output;
    const HWFloat<8,24> &id1044in_option1 = id1041out_output;
    const HWFloat<8,24> &id1044in_option2 = id2610out_value;
    const HWFloat<8,24> &id1044in_option3 = id1041out_output;

    HWFloat<8,24> id1044x_1;

    switch((id1044in_sel.getValueAsLong())) {
      case 0l:
        id1044x_1 = id1044in_option0;
        break;
      case 1l:
        id1044x_1 = id1044in_option1;
        break;
      case 2l:
        id1044x_1 = id1044in_option2;
        break;
      case 3l:
        id1044x_1 = id1044in_option3;
        break;
      default:
        id1044x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1044out_result = (id1044x_1);
  }
  { // Node ID: 3336 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1054out_result;

  { // Node ID: 1054 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1054in_sel = id3108out_output[getCycle()%9];
    const HWFloat<8,24> &id1054in_option0 = id1044out_result;
    const HWFloat<8,24> &id1054in_option1 = id3336out_value;

    HWFloat<8,24> id1054x_1;

    switch((id1054in_sel.getValueAsLong())) {
      case 0l:
        id1054x_1 = id1054in_option0;
        break;
      case 1l:
        id1054x_1 = id1054in_option1;
        break;
      default:
        id1054x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1054out_result = (id1054x_1);
  }
  { // Node ID: 3335 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1062out_result;

  { // Node ID: 1062 (NodeAdd)
    const HWFloat<8,24> &id1062in_a = id1054out_result;
    const HWFloat<8,24> &id1062in_b = id3335out_value;

    id1062out_result = (add_float(id1062in_a,id1062in_b));
  }
  HWFloat<8,24> id1064out_result;

  { // Node ID: 1064 (NodeDiv)
    const HWFloat<8,24> &id1064in_a = id3344out_value;
    const HWFloat<8,24> &id1064in_b = id1062out_result;

    id1064out_result = (div_float(id1064in_a,id1064in_b));
  }
  HWFloat<8,24> id1065out_result;

  { // Node ID: 1065 (NodeMul)
    const HWFloat<8,24> &id1065in_a = id12out_dt;
    const HWFloat<8,24> &id1065in_b = id1064out_result;

    id1065out_result = (mul_float(id1065in_a,id1065in_b));
  }
  HWFloat<8,24> id1823out_result;

  { // Node ID: 1823 (NodeMul)
    const HWFloat<8,24> &id1823in_a = id1065out_result;
    const HWFloat<8,24> &id1823in_b = id3097out_output[getCycle()%10];

    id1823out_result = (mul_float(id1823in_a,id1823in_b));
  }
  HWFloat<8,24> id1824out_result;

  { // Node ID: 1824 (NodeSub)
    const HWFloat<8,24> &id1824in_a = id1822out_result;
    const HWFloat<8,24> &id1824in_b = id1823out_result;

    id1824out_result = (sub_float(id1824in_a,id1824in_b));
  }
  HWFloat<8,24> id1825out_result;

  { // Node ID: 1825 (NodeAdd)
    const HWFloat<8,24> &id1825in_a = id3097out_output[getCycle()%10];
    const HWFloat<8,24> &id1825in_b = id1824out_result;

    id1825out_result = (add_float(id1825in_a,id1825in_b));
  }
  HWFloat<8,24> id2492out_result;

  { // Node ID: 2492 (NodeMul)
    const HWFloat<8,24> &id2492in_a = id2491out_result;
    const HWFloat<8,24> &id2492in_b = id1825out_result;

    id2492out_result = (mul_float(id2492in_a,id2492in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2493out_result;

  { // Node ID: 2493 (NodeAdd)
    const HWFloat<8,24> &id2493in_a = id2492out_result;
    const HWFloat<8,24> &id2493in_b = id3out_value;

    id2493out_result = (add_float(id2493in_a,id2493in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2494out_result;

  { // Node ID: 2494 (NodeSub)
    const HWFloat<8,24> &id2494in_a = id3232out_output[getCycle()%2];
    const HWFloat<8,24> &id2494in_b = id2out_value;

    id2494out_result = (sub_float(id2494in_a,id2494in_b));
  }
  HWFloat<8,24> id2495out_result;

  { // Node ID: 2495 (NodeMul)
    const HWFloat<8,24> &id2495in_a = id2493out_result;
    const HWFloat<8,24> &id2495in_b = id2494out_result;

    id2495out_result = (mul_float(id2495in_a,id2495in_b));
  }
  HWFloat<8,24> id2497out_result;

  { // Node ID: 2497 (NodeAdd)
    const HWFloat<8,24> &id2497in_a = id2496out_result;
    const HWFloat<8,24> &id2497in_b = id2495out_result;

    id2497out_result = (add_float(id2497in_a,id2497in_b));
  }
  { // Node ID: 3334 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1913out_result;

  { // Node ID: 1913 (NodeMul)
    const HWFloat<8,24> &id1913in_a = id3334out_value;
    const HWFloat<8,24> &id1913in_b = id3112out_output[getCycle()%2];

    id1913out_result = (mul_float(id1913in_a,id1913in_b));
  }
  HWFloat<8,24> id1914out_result;

  { // Node ID: 1914 (NodeMul)
    const HWFloat<8,24> &id1914in_a = id1913out_result;
    const HWFloat<8,24> &id1914in_b = id3157out_output[getCycle()%2];

    id1914out_result = (mul_float(id1914in_a,id1914in_b));
  }
  { // Node ID: 3257 (NodeConstantRawBits)
  }
  { // Node ID: 3256 (NodeConstantRawBits)
  }
  { // Node ID: 3251 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1900out_result;

  { // Node ID: 1900 (NodeLt)
    const HWFloat<8,24> &id1900in_a = id3233out_output[getCycle()%3];
    const HWFloat<8,24> &id1900in_b = id3251out_value;

    id1900out_result = (lt_float(id1900in_a,id1900in_b));
  }
  HWRawBits<8> id1881out_result;

  { // Node ID: 1881 (NodeSlice)
    const HWFloat<8,24> &id1881in_a = id3233out_output[getCycle()%3];

    id1881out_result = (slice<23,8>(id1881in_a));
  }
  { // Node ID: 1882 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2803out_result;

  { // Node ID: 2803 (NodeEqInlined)
    const HWRawBits<8> &id2803in_a = id1881out_result;
    const HWRawBits<8> &id2803in_b = id1882out_value;

    id2803out_result = (eq_bits(id2803in_a,id2803in_b));
  }
  HWRawBits<23> id1880out_result;

  { // Node ID: 1880 (NodeSlice)
    const HWFloat<8,24> &id1880in_a = id3233out_output[getCycle()%3];

    id1880out_result = (slice<0,23>(id1880in_a));
  }
  { // Node ID: 3250 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2804out_result;

  { // Node ID: 2804 (NodeNeqInlined)
    const HWRawBits<23> &id2804in_a = id1880out_result;
    const HWRawBits<23> &id2804in_b = id3250out_value;

    id2804out_result = (neq_bits(id2804in_a,id2804in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1886out_result;

  { // Node ID: 1886 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1886in_a = id2803out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1886in_b = id2804out_result;

    HWOffsetFix<1,0,UNSIGNED> id1886x_1;

    (id1886x_1) = (and_fixed(id1886in_a,id1886in_b));
    id1886out_result = (id1886x_1);
  }
  { // Node ID: 3249 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1888out_result;

  { // Node ID: 1888 (NodeLt)
    const HWFloat<8,24> &id1888in_a = id3233out_output[getCycle()%3];
    const HWFloat<8,24> &id1888in_b = id3249out_value;

    id1888out_result = (lt_float(id1888in_a,id1888in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1889out_result;

  { // Node ID: 1889 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1889in_a = id1886out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1889in_b = id1888out_result;

    HWOffsetFix<1,0,UNSIGNED> id1889x_1;

    (id1889x_1) = (or_fixed(id1889in_a,id1889in_b));
    id1889out_result = (id1889x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1901out_result;

  { // Node ID: 1901 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1901in_a = id1889out_result;

    id1901out_result = (not_fixed(id1901in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1902out_result;

  { // Node ID: 1902 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1902in_a = id1900out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1902in_b = id1901out_result;

    HWOffsetFix<1,0,UNSIGNED> id1902x_1;

    (id1902x_1) = (and_fixed(id1902in_a,id1902in_b));
    id1902out_result = (id1902x_1);
  }
  HWRawBits<31> id1893out_result;

  { // Node ID: 1893 (NodeSlice)
    const HWFloat<8,24> &id1893in_a = id3233out_output[getCycle()%3];

    id1893out_result = (slice<0,31>(id1893in_a));
  }
  { // Node ID: 1892 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2805out_result;

  { // Node ID: 2805 (NodeEqInlined)
    const HWRawBits<31> &id2805in_a = id1893out_result;
    const HWRawBits<31> &id2805in_b = id1892out_value;

    id2805out_result = (eq_bits(id2805in_a,id2805in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1895out_result;

  { // Node ID: 1895 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1895in_a = id1889out_result;

    id1895out_result = (not_fixed(id1895in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1896out_result;

  { // Node ID: 1896 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1896in_a = id2805out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1896in_b = id1895out_result;

    HWOffsetFix<1,0,UNSIGNED> id1896x_1;

    (id1896x_1) = (and_fixed(id1896in_a,id1896in_b));
    id1896out_result = (id1896x_1);
  }
  { // Node ID: 1853 (NodeConstantRawBits)
  }
  HWRawBits<8> id1852out_result;

  { // Node ID: 1852 (NodeSlice)
    const HWFloat<8,24> &id1852in_a = id3233out_output[getCycle()%3];

    id1852out_result = (slice<23,8>(id1852in_a));
  }
  HWRawBits<9> id1854out_result;

  { // Node ID: 1854 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1854in_in0 = id1853out_value;
    const HWRawBits<8> &id1854in_in1 = id1852out_result;

    id1854out_result = (cat(id1854in_in0,id1854in_in1));
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1855out_output;

  { // Node ID: 1855 (NodeReinterpret)
    const HWRawBits<9> &id1855in_input = id1854out_result;

    id1855out_output = (cast_bits2fixed<9,0,TWOSCOMPLEMENT>(id1855in_input));
  }
  { // Node ID: 3248 (NodeConstantRawBits)
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1857out_result;

  { // Node ID: 1857 (NodeSub)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1857in_a = id1855out_output;
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1857in_b = id3248out_value;

    id1857out_result = (sub_fixed<9,0,TWOSCOMPLEMENT,TONEAREVEN>(id1857in_a,id1857in_b));
  }
  { // Node ID: 1858 (NodeConstantRawBits)
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id2865out_result;

  { // Node ID: 2865 (NodeCondAdd)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id2865in_a = id1857out_result;
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id2865in_b = id1858out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2865in_condb = id3211out_output[getCycle()%3];

    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2865x_1;
    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2865x_2;
    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2865x_3;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2865x_1 = (c_hw_fix_9_0_sgn_bits_2);
        break;
      case 1l:
        id2865x_1 = id2865in_a;
        break;
      default:
        id2865x_1 = (c_hw_fix_9_0_sgn_undef);
        break;
    }
    switch((id2865in_condb.getValueAsLong())) {
      case 0l:
        id2865x_2 = (c_hw_fix_9_0_sgn_bits_2);
        break;
      case 1l:
        id2865x_2 = id2865in_b;
        break;
      default:
        id2865x_2 = (c_hw_fix_9_0_sgn_undef);
        break;
    }
    (id2865x_3) = (add_fixed<9,0,TWOSCOMPLEMENT,TRUNCATE>((id2865x_1),(id2865x_2)));
    id2865out_result = (id2865x_3);
  }
  HWRawBits<24> id2580out_result;

  { // Node ID: 2580 (NodeSlice)
    const HWRawBits<96> &id2580in_a = id2573out_dout[getCycle()%3];

    id2580out_result = (slice<72,24>(id2580in_a));
  }
  HWOffsetFix<24,-51,TWOSCOMPLEMENT> id2581out_output;

  { // Node ID: 2581 (NodeReinterpret)
    const HWRawBits<24> &id2581in_input = id2580out_result;

    id2581out_output = (cast_bits2fixed<24,-51,TWOSCOMPLEMENT>(id2581in_input));
  }
  HWOffsetFix<38,-65,TWOSCOMPLEMENT> id1867out_result;

  { // Node ID: 1867 (NodeMul)
    const HWOffsetFix<24,-51,TWOSCOMPLEMENT> &id1867in_a = id2581out_output;
    const HWOffsetFix<14,-14,UNSIGNED> &id1867in_b = id3212out_output[getCycle()%3];

    id1867out_result = (mul_fixed<38,-65,TWOSCOMPLEMENT,TONEAREVEN>(id1867in_a,id1867in_b));
  }
  HWRawBits<24> id2578out_result;

  { // Node ID: 2578 (NodeSlice)
    const HWRawBits<96> &id2578in_a = id2573out_dout[getCycle()%3];

    id2578out_result = (slice<48,24>(id2578in_a));
  }
  HWOffsetFix<24,-41,TWOSCOMPLEMENT> id2579out_output;

  { // Node ID: 2579 (NodeReinterpret)
    const HWRawBits<24> &id2579in_input = id2578out_result;

    id2579out_output = (cast_bits2fixed<24,-41,TWOSCOMPLEMENT>(id2579in_input));
  }
  HWOffsetFix<48,-65,TWOSCOMPLEMENT> id1868out_result;

  { // Node ID: 1868 (NodeAdd)
    const HWOffsetFix<38,-65,TWOSCOMPLEMENT> &id1868in_a = id1867out_result;
    const HWOffsetFix<24,-41,TWOSCOMPLEMENT> &id1868in_b = id2579out_output;

    id1868out_result = (add_fixed<48,-65,TWOSCOMPLEMENT,TONEAREVEN>(id1868in_a,id1868in_b));
  }
  HWOffsetFix<24,-41,TWOSCOMPLEMENT> id1869out_o;

  { // Node ID: 1869 (NodeCast)
    const HWOffsetFix<48,-65,TWOSCOMPLEMENT> &id1869in_i = id1868out_result;

    id1869out_o = (cast_fixed2fixed<24,-41,TWOSCOMPLEMENT,TONEAREVEN>(id1869in_i));
  }
  HWOffsetFix<38,-55,TWOSCOMPLEMENT> id1870out_result;

  { // Node ID: 1870 (NodeMul)
    const HWOffsetFix<24,-41,TWOSCOMPLEMENT> &id1870in_a = id1869out_o;
    const HWOffsetFix<14,-14,UNSIGNED> &id1870in_b = id3212out_output[getCycle()%3];

    id1870out_result = (mul_fixed<38,-55,TWOSCOMPLEMENT,TONEAREVEN>(id1870in_a,id1870in_b));
  }
  HWRawBits<24> id2576out_result;

  { // Node ID: 2576 (NodeSlice)
    const HWRawBits<96> &id2576in_a = id2573out_dout[getCycle()%3];

    id2576out_result = (slice<24,24>(id2576in_a));
  }
  HWOffsetFix<24,-31,TWOSCOMPLEMENT> id2577out_output;

  { // Node ID: 2577 (NodeReinterpret)
    const HWRawBits<24> &id2577in_input = id2576out_result;

    id2577out_output = (cast_bits2fixed<24,-31,TWOSCOMPLEMENT>(id2577in_input));
  }
  HWOffsetFix<48,-55,TWOSCOMPLEMENT> id1871out_result;

  { // Node ID: 1871 (NodeAdd)
    const HWOffsetFix<38,-55,TWOSCOMPLEMENT> &id1871in_a = id1870out_result;
    const HWOffsetFix<24,-31,TWOSCOMPLEMENT> &id1871in_b = id2577out_output;

    id1871out_result = (add_fixed<48,-55,TWOSCOMPLEMENT,TONEAREVEN>(id1871in_a,id1871in_b));
  }
  HWOffsetFix<24,-31,TWOSCOMPLEMENT> id1872out_o;

  { // Node ID: 1872 (NodeCast)
    const HWOffsetFix<48,-55,TWOSCOMPLEMENT> &id1872in_i = id1871out_result;

    id1872out_o = (cast_fixed2fixed<24,-31,TWOSCOMPLEMENT,TONEAREVEN>(id1872in_i));
  }
  HWOffsetFix<38,-45,TWOSCOMPLEMENT> id1873out_result;

  { // Node ID: 1873 (NodeMul)
    const HWOffsetFix<24,-31,TWOSCOMPLEMENT> &id1873in_a = id1872out_o;
    const HWOffsetFix<14,-14,UNSIGNED> &id1873in_b = id3212out_output[getCycle()%3];

    id1873out_result = (mul_fixed<38,-45,TWOSCOMPLEMENT,TONEAREVEN>(id1873in_a,id1873in_b));
  }
  HWRawBits<24> id2574out_result;

  { // Node ID: 2574 (NodeSlice)
    const HWRawBits<96> &id2574in_a = id2573out_dout[getCycle()%3];

    id2574out_result = (slice<0,24>(id2574in_a));
  }
  HWOffsetFix<24,-23,TWOSCOMPLEMENT> id2575out_output;

  { // Node ID: 2575 (NodeReinterpret)
    const HWRawBits<24> &id2575in_input = id2574out_result;

    id2575out_output = (cast_bits2fixed<24,-23,TWOSCOMPLEMENT>(id2575in_input));
  }
  HWOffsetFix<47,-45,TWOSCOMPLEMENT> id1874out_result;

  { // Node ID: 1874 (NodeAdd)
    const HWOffsetFix<38,-45,TWOSCOMPLEMENT> &id1874in_a = id1873out_result;
    const HWOffsetFix<24,-23,TWOSCOMPLEMENT> &id1874in_b = id2575out_output;

    id1874out_result = (add_fixed<47,-45,TWOSCOMPLEMENT,TONEAREVEN>(id1874in_a,id1874in_b));
  }
  HWOffsetFix<24,-22,TWOSCOMPLEMENT> id1875out_o;

  { // Node ID: 1875 (NodeCast)
    const HWOffsetFix<47,-45,TWOSCOMPLEMENT> &id1875in_i = id1874out_result;

    id1875out_o = (cast_fixed2fixed<24,-22,TWOSCOMPLEMENT,TONEAREVEN>(id1875in_i));
  }
  HWOffsetFix<32,-22,TWOSCOMPLEMENT> id1878out_result;

  { // Node ID: 1878 (NodeAdd)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1878in_a = id2865out_result;
    const HWOffsetFix<24,-22,TWOSCOMPLEMENT> &id1878in_b = id1875out_o;

    id1878out_result = (add_fixed<32,-22,TWOSCOMPLEMENT,TONEAREVEN>(id1878in_a,id1878in_b));
  }
  HWFloat<8,24> id1879out_o;

  { // Node ID: 1879 (NodeCast)
    const HWOffsetFix<32,-22,TWOSCOMPLEMENT> &id1879in_i = id1878out_result;

    id1879out_o = (cast_fixed2float<8,24>(id1879in_i));
  }
  { // Node ID: 3246 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1891out_result;

  { // Node ID: 1891 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1891in_sel = id1889out_result;
    const HWFloat<8,24> &id1891in_option0 = id1879out_o;
    const HWFloat<8,24> &id1891in_option1 = id3246out_value;

    HWFloat<8,24> id1891x_1;

    switch((id1891in_sel.getValueAsLong())) {
      case 0l:
        id1891x_1 = id1891in_option0;
        break;
      case 1l:
        id1891x_1 = id1891in_option1;
        break;
      default:
        id1891x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1891out_result = (id1891x_1);
  }
  { // Node ID: 3245 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1898out_result;

  { // Node ID: 1898 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1898in_sel = id1896out_result;
    const HWFloat<8,24> &id1898in_option0 = id1891out_result;
    const HWFloat<8,24> &id1898in_option1 = id3245out_value;

    HWFloat<8,24> id1898x_1;

    switch((id1898in_sel.getValueAsLong())) {
      case 0l:
        id1898x_1 = id1898in_option0;
        break;
      case 1l:
        id1898x_1 = id1898in_option1;
        break;
      default:
        id1898x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1898out_result = (id1898x_1);
  }
  { // Node ID: 3244 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1904out_result;

  { // Node ID: 1904 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1904in_sel = id1902out_result;
    const HWFloat<8,24> &id1904in_option0 = id1898out_result;
    const HWFloat<8,24> &id1904in_option1 = id3244out_value;

    HWFloat<8,24> id1904x_1;

    switch((id1904in_sel.getValueAsLong())) {
      case 0l:
        id1904x_1 = id1904in_option0;
        break;
      case 1l:
        id1904x_1 = id1904in_option1;
        break;
      default:
        id1904x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1904out_result = (id1904x_1);
  }
  { // Node ID: 1905 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1906out_result;

  { // Node ID: 1906 (NodeMul)
    const HWFloat<8,24> &id1906in_a = id1904out_result;
    const HWFloat<8,24> &id1906in_b = id1905out_value;

    id1906out_result = (mul_float(id1906in_a,id1906in_b));
  }
  HWFloat<8,24> id1909out_result;

  { // Node ID: 1909 (NodeMul)
    const HWFloat<8,24> &id1909in_a = id3256out_value;
    const HWFloat<8,24> &id1909in_b = id1906out_result;

    id1909out_result = (mul_float(id1909in_a,id1909in_b));
  }
  HWFloat<8,24> id1911out_result;

  { // Node ID: 1911 (NodeSub)
    const HWFloat<8,24> &id1911in_a = id3257out_value;
    const HWFloat<8,24> &id1911in_b = id1909out_result;

    id1911out_result = (sub_float(id1911in_a,id1911in_b));
  }
  HWFloat<8,24> id1915out_result;

  { // Node ID: 1915 (NodeSub)
    const HWFloat<8,24> &id1915in_a = id3232out_output[getCycle()%2];
    const HWFloat<8,24> &id1915in_b = id1911out_result;

    id1915out_result = (sub_float(id1915in_a,id1915in_b));
  }
  HWFloat<8,24> id1916out_result;

  { // Node ID: 1916 (NodeMul)
    const HWFloat<8,24> &id1916in_a = id1914out_result;
    const HWFloat<8,24> &id1916in_b = id1915out_result;

    id1916out_result = (mul_float(id1916in_a,id1916in_b));
  }
  HWFloat<8,24> id2498out_result;

  { // Node ID: 2498 (NodeAdd)
    const HWFloat<8,24> &id2498in_a = id2497out_result;
    const HWFloat<8,24> &id2498in_b = id1916out_result;

    id2498out_result = (add_float(id2498in_a,id2498in_b));
  }
  HWFloat<8,24> id2499out_result;

  { // Node ID: 2499 (NodeMul)
    const HWFloat<8,24> &id2499in_a = id12out_dt;
    const HWFloat<8,24> &id2499in_b = id2498out_result;

    id2499out_result = (mul_float(id2499in_a,id2499in_b));
  }
  HWFloat<8,24> id2500out_result;

  { // Node ID: 2500 (NodeSub)
    const HWFloat<8,24> &id2500in_a = id3232out_output[getCycle()%2];
    const HWFloat<8,24> &id2500in_b = id2499out_result;

    id2500out_result = (sub_float(id2500in_a,id2500in_b));
  }
  HWFloat<8,24> id2904out_output;

  { // Node ID: 2904 (NodeStreamOffset)
    const HWFloat<8,24> &id2904in_input = id2500out_result;

    id2904out_output = id2904in_input;
  }
  { // Node ID: 16 (NodeInputMappedReg)
  }
  { // Node ID: 17 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_sel = id2620out_result[getCycle()%2];
    const HWFloat<8,24> &id17in_option0 = id2904out_output;
    const HWFloat<8,24> &id17in_option1 = id16out_u_in;

    HWFloat<8,24> id17x_1;

    switch((id17in_sel.getValueAsLong())) {
      case 0l:
        id17x_1 = id17in_option0;
        break;
      case 1l:
        id17x_1 = id17in_option1;
        break;
      default:
        id17x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id17out_result[(getCycle()+1)%2] = (id17x_1);
  }
  { // Node ID: 2924 (NodeFIFO)
    const HWFloat<8,24> &id2924in_input = id17out_result[getCycle()%2];

    id2924out_output[(getCycle()+1)%2] = id2924in_input;
  }
  { // Node ID: 3231 (NodeFIFO)
    const HWFloat<8,24> &id3231in_input = id2924out_output[getCycle()%2];

    id3231out_output[(getCycle()+7)%8] = id3231in_input;
  }
  { // Node ID: 3232 (NodeFIFO)
    const HWFloat<8,24> &id3232in_input = id3231out_output[getCycle()%8];

    id3232out_output[(getCycle()+1)%2] = id3232in_input;
  }
  { // Node ID: 3496 (NodeConstantRawBits)
  }
  { // Node ID: 3495 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2203out_result;

  { // Node ID: 2203 (NodeAdd)
    const HWFloat<8,24> &id2203in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id2203in_b = id3495out_value;

    id2203out_result = (add_float(id2203in_a,id2203in_b));
  }
  HWFloat<8,24> id2205out_result;

  { // Node ID: 2205 (NodeMul)
    const HWFloat<8,24> &id2205in_a = id3496out_value;
    const HWFloat<8,24> &id2205in_b = id2203out_result;

    id2205out_result = (mul_float(id2205in_a,id2205in_b));
  }
  HWRawBits<8> id2277out_result;

  { // Node ID: 2277 (NodeSlice)
    const HWFloat<8,24> &id2277in_a = id2205out_result;

    id2277out_result = (slice<23,8>(id2277in_a));
  }
  { // Node ID: 2278 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2621out_result;

  { // Node ID: 2621 (NodeEqInlined)
    const HWRawBits<8> &id2621in_a = id2277out_result;
    const HWRawBits<8> &id2621in_b = id2278out_value;

    id2621out_result = (eq_bits(id2621in_a,id2621in_b));
  }
  HWRawBits<23> id2276out_result;

  { // Node ID: 2276 (NodeSlice)
    const HWFloat<8,24> &id2276in_a = id2205out_result;

    id2276out_result = (slice<0,23>(id2276in_a));
  }
  { // Node ID: 3494 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2622out_result;

  { // Node ID: 2622 (NodeNeqInlined)
    const HWRawBits<23> &id2622in_a = id2276out_result;
    const HWRawBits<23> &id2622in_b = id3494out_value;

    id2622out_result = (neq_bits(id2622in_a,id2622in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2282out_result;

  { // Node ID: 2282 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2282in_a = id2621out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2282in_b = id2622out_result;

    HWOffsetFix<1,0,UNSIGNED> id2282x_1;

    (id2282x_1) = (and_fixed(id2282in_a,id2282in_b));
    id2282out_result = (id2282x_1);
  }
  { // Node ID: 2923 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2923in_input = id2282out_result;

    id2923out_output[(getCycle()+8)%9] = id2923in_input;
  }
  { // Node ID: 2206 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2207out_output;
  HWOffsetFix<1,0,UNSIGNED> id2207out_output_doubt;

  { // Node ID: 2207 (NodeDoubtBitOp)
    const HWFloat<8,24> &id2207in_input = id2205out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2207in_doubt = id2206out_value;

    id2207out_output = id2207in_input;
    id2207out_output_doubt = id2207in_doubt;
  }
  { // Node ID: 2208 (NodeCast)
    const HWFloat<8,24> &id2208in_i = id2207out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2208in_i_doubt = id2207out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2208x_1;

    id2208out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2208in_i,(&(id2208x_1))));
    id2208out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2208x_1),(c_hw_fix_4_0_uns_bits))),id2208in_i_doubt));
  }
  { // Node ID: 2211 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id2210out_result;
  HWOffsetFix<1,0,UNSIGNED> id2210out_result_doubt;

  { // Node ID: 2210 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2210in_a = id2208out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2210in_a_doubt = id2208out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id2210in_b = id2211out_value;

    HWOffsetFix<1,0,UNSIGNED> id2210x_1;

    id2210out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id2210in_a,id2210in_b,(&(id2210x_1))));
    id2210out_result_doubt = (or_fixed((neq_fixed((id2210x_1),(c_hw_fix_1_0_uns_bits_1))),id2210in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2212out_o;
  HWOffsetFix<1,0,UNSIGNED> id2212out_o_doubt;

  { // Node ID: 2212 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id2212in_i = id2210out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2212in_i_doubt = id2210out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2212x_1;

    id2212out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2212in_i,(&(id2212x_1))));
    id2212out_o_doubt = (or_fixed((neq_fixed((id2212x_1),(c_hw_fix_1_0_uns_bits_1))),id2212in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2221out_output;

  { // Node ID: 2221 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2221in_input = id2212out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2221in_input_doubt = id2212out_o_doubt;

    id2221out_output = id2221in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2222out_o;

  { // Node ID: 2222 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2222in_i = id2221out_output;

    id2222out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2222in_i));
  }
  { // Node ID: 2914 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2914in_input = id2222out_o;

    id2914out_output[(getCycle()+2)%3] = id2914in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2244out_o;

  { // Node ID: 2244 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2244in_i = id2914out_output[getCycle()%3];

    id2244out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2244in_i));
  }
  { // Node ID: 2247 (NodeConstantRawBits)
  }
  { // Node ID: 2814 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id2224out_o;

  { // Node ID: 2224 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2224in_i = id2221out_output;

    id2224out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id2224in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2289out_output;

  { // Node ID: 2289 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2289in_input = id2224out_o;

    id2289out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2289in_input))));
  }
  { // Node ID: 2290 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2290in_addr = id2289out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2290x_1;

    switch(((long)((id2290in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2290x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2290x_1 = (id2290sta_rom_store[(id2290in_addr.getValueAsLong())]);
        break;
      default:
        id2290x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2290out_dout[(getCycle()+2)%3] = (id2290x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id2223out_o;

  { // Node ID: 2223 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2223in_i = id2221out_output;

    id2223out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id2223in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2286out_output;

  { // Node ID: 2286 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2286in_input = id2223out_o;

    id2286out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2286in_input))));
  }
  { // Node ID: 2287 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2287in_addr = id2286out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2287x_1;

    switch(((long)((id2287in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2287x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2287x_1 = (id2287sta_rom_store[(id2287in_addr.getValueAsLong())]);
        break;
      default:
        id2287x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2287out_dout[(getCycle()+2)%3] = (id2287x_1);
  }
  { // Node ID: 2228 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id2225out_o;

  { // Node ID: 2225 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2225in_i = id2221out_output;

    id2225out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id2225in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id2227out_result;

  { // Node ID: 2227 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id2227in_a = id2228out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id2227in_b = id2225out_o;

    id2227out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id2227in_a,id2227in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id2229out_o;

  { // Node ID: 2229 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id2229in_i = id2227out_result;

    id2229out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id2229in_i));
  }
  { // Node ID: 2915 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2915in_input = id2229out_o;

    id2915out_output[(getCycle()+2)%3] = id2915in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id2230out_result;

  { // Node ID: 2230 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id2230in_a = id2287out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id2230in_b = id2915out_output[getCycle()%3];

    id2230out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2230in_a,id2230in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id2231out_result;

  { // Node ID: 2231 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id2231in_a = id2915out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id2231in_b = id2287out_dout[getCycle()%3];

    id2231out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id2231in_a,id2231in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id2232out_result;

  { // Node ID: 2232 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id2232in_a = id2230out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id2232in_b = id2231out_result;

    id2232out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id2232in_a,id2232in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2233out_o;

  { // Node ID: 2233 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id2233in_i = id2232out_result;

    id2233out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id2233in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2234out_result;

  { // Node ID: 2234 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id2234in_a = id2290out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id2234in_b = id2233out_o;

    id2234out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id2234in_a,id2234in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id2235out_result;

  { // Node ID: 2235 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id2235in_a = id2233out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id2235in_b = id2290out_dout[getCycle()%3];

    id2235out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id2235in_a,id2235in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id2236out_result;

  { // Node ID: 2236 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id2236in_a = id2234out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id2236in_b = id2235out_result;

    id2236out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id2236in_a,id2236in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id2237out_o;

  { // Node ID: 2237 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id2237in_i = id2236out_result;

    id2237out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id2237in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id2238out_o;

  { // Node ID: 2238 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id2238in_i = id2237out_o;

    id2238out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id2238in_i));
  }
  { // Node ID: 3493 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2623out_result;

  { // Node ID: 2623 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2623in_a = id2238out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2623in_b = id3493out_value;

    id2623out_result = (gte_fixed(id2623in_a,id2623in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2890out_result;

  { // Node ID: 2890 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2890in_a = id2244out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2890in_b = id2247out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2890in_c = id2814out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2890in_condb = id2623out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2890x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2890x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2890x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2890x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2890x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2890x_1 = id2890in_a;
        break;
      default:
        id2890x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2890in_condb.getValueAsLong())) {
      case 0l:
        id2890x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2890x_2 = id2890in_b;
        break;
      default:
        id2890x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2890x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2890x_3 = id2890in_c;
        break;
      default:
        id2890x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2890x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2890x_1),(id2890x_2))),(id2890x_3)));
    id2890out_result = (id2890x_4);
  }
  HWRawBits<1> id2624out_result;

  { // Node ID: 2624 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2624in_a = id2890out_result;

    id2624out_result = (slice<10,1>(id2624in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2625out_output;

  { // Node ID: 2625 (NodeReinterpret)
    const HWRawBits<1> &id2625in_input = id2624out_result;

    id2625out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2625in_input));
  }
  { // Node ID: 3492 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2214out_result;

  { // Node ID: 2214 (NodeGt)
    const HWFloat<8,24> &id2214in_a = id2205out_result;
    const HWFloat<8,24> &id2214in_b = id3492out_value;

    id2214out_result = (gt_float(id2214in_a,id2214in_b));
  }
  { // Node ID: 2917 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2917in_input = id2214out_result;

    id2917out_output[(getCycle()+6)%7] = id2917in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2215out_output;

  { // Node ID: 2215 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2215in_input = id2212out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2215in_input_doubt = id2212out_o_doubt;

    id2215out_output = id2215in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2216out_result;

  { // Node ID: 2216 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2216in_a = id2917out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2216in_b = id2215out_output;

    HWOffsetFix<1,0,UNSIGNED> id2216x_1;

    (id2216x_1) = (and_fixed(id2216in_a,id2216in_b));
    id2216out_result = (id2216x_1);
  }
  { // Node ID: 2918 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2918in_input = id2216out_result;

    id2918out_output[(getCycle()+2)%3] = id2918in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2253out_result;

  { // Node ID: 2253 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2253in_a = id2918out_output[getCycle()%3];

    id2253out_result = (not_fixed(id2253in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2254out_result;

  { // Node ID: 2254 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2254in_a = id2625out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2254in_b = id2253out_result;

    HWOffsetFix<1,0,UNSIGNED> id2254x_1;

    (id2254x_1) = (and_fixed(id2254in_a,id2254in_b));
    id2254out_result = (id2254x_1);
  }
  { // Node ID: 3491 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2218out_result;

  { // Node ID: 2218 (NodeLt)
    const HWFloat<8,24> &id2218in_a = id2205out_result;
    const HWFloat<8,24> &id2218in_b = id3491out_value;

    id2218out_result = (lt_float(id2218in_a,id2218in_b));
  }
  { // Node ID: 2919 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2919in_input = id2218out_result;

    id2919out_output[(getCycle()+6)%7] = id2919in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2219out_output;

  { // Node ID: 2219 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2219in_input = id2212out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2219in_input_doubt = id2212out_o_doubt;

    id2219out_output = id2219in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2220out_result;

  { // Node ID: 2220 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2220in_a = id2919out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2220in_b = id2219out_output;

    HWOffsetFix<1,0,UNSIGNED> id2220x_1;

    (id2220x_1) = (and_fixed(id2220in_a,id2220in_b));
    id2220out_result = (id2220x_1);
  }
  { // Node ID: 2920 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2920in_input = id2220out_result;

    id2920out_output[(getCycle()+2)%3] = id2920in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2255out_result;

  { // Node ID: 2255 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2255in_a = id2254out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2255in_b = id2920out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2255x_1;

    (id2255x_1) = (or_fixed(id2255in_a,id2255in_b));
    id2255out_result = (id2255x_1);
  }
  { // Node ID: 3490 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2626out_result;

  { // Node ID: 2626 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2626in_a = id2890out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2626in_b = id3490out_value;

    id2626out_result = (gte_fixed(id2626in_a,id2626in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2262out_result;

  { // Node ID: 2262 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2262in_a = id2920out_output[getCycle()%3];

    id2262out_result = (not_fixed(id2262in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2263out_result;

  { // Node ID: 2263 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2263in_a = id2626out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2263in_b = id2262out_result;

    HWOffsetFix<1,0,UNSIGNED> id2263x_1;

    (id2263x_1) = (and_fixed(id2263in_a,id2263in_b));
    id2263out_result = (id2263x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2264out_result;

  { // Node ID: 2264 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2264in_a = id2263out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2264in_b = id2918out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2264x_1;

    (id2264x_1) = (or_fixed(id2264in_a,id2264in_b));
    id2264out_result = (id2264x_1);
  }
  HWRawBits<2> id2265out_result;

  { // Node ID: 2265 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2265in_in0 = id2255out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2265in_in1 = id2264out_result;

    id2265out_result = (cat(id2265in_in0,id2265in_in1));
  }
  { // Node ID: 2257 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2256out_o;

  { // Node ID: 2256 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2256in_i = id2890out_result;

    id2256out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2256in_i));
  }
  { // Node ID: 2241 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2242out_result;

  { // Node ID: 2242 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2242in_sel = id2623out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id2242in_option0 = id2238out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2242in_option1 = id2241out_value;

    HWOffsetFix<24,-23,UNSIGNED> id2242x_1;

    switch((id2242in_sel.getValueAsLong())) {
      case 0l:
        id2242x_1 = id2242in_option0;
        break;
      case 1l:
        id2242x_1 = id2242in_option1;
        break;
      default:
        id2242x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id2242out_result = (id2242x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id2243out_o;

  { // Node ID: 2243 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id2243in_i = id2242out_result;

    id2243out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id2243in_i));
  }
  HWRawBits<32> id2258out_result;

  { // Node ID: 2258 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2258in_in0 = id2257out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2258in_in1 = id2256out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id2258in_in2 = id2243out_o;

    id2258out_result = (cat((cat(id2258in_in0,id2258in_in1)),id2258in_in2));
  }
  HWFloat<8,24> id2259out_output;

  { // Node ID: 2259 (NodeReinterpret)
    const HWRawBits<32> &id2259in_input = id2258out_result;

    id2259out_output = (cast_bits2float<8,24>(id2259in_input));
  }
  { // Node ID: 2266 (NodeConstantRawBits)
  }
  { // Node ID: 2267 (NodeConstantRawBits)
  }
  { // Node ID: 2269 (NodeConstantRawBits)
  }
  HWRawBits<32> id2627out_result;

  { // Node ID: 2627 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2627in_in0 = id2266out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2627in_in1 = id2267out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2627in_in2 = id2269out_value;

    id2627out_result = (cat((cat(id2627in_in0,id2627in_in1)),id2627in_in2));
  }
  HWFloat<8,24> id2271out_output;

  { // Node ID: 2271 (NodeReinterpret)
    const HWRawBits<32> &id2271in_input = id2627out_result;

    id2271out_output = (cast_bits2float<8,24>(id2271in_input));
  }
  { // Node ID: 2594 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2274out_result;

  { // Node ID: 2274 (NodeMux)
    const HWRawBits<2> &id2274in_sel = id2265out_result;
    const HWFloat<8,24> &id2274in_option0 = id2259out_output;
    const HWFloat<8,24> &id2274in_option1 = id2271out_output;
    const HWFloat<8,24> &id2274in_option2 = id2594out_value;
    const HWFloat<8,24> &id2274in_option3 = id2271out_output;

    HWFloat<8,24> id2274x_1;

    switch((id2274in_sel.getValueAsLong())) {
      case 0l:
        id2274x_1 = id2274in_option0;
        break;
      case 1l:
        id2274x_1 = id2274in_option1;
        break;
      case 2l:
        id2274x_1 = id2274in_option2;
        break;
      case 3l:
        id2274x_1 = id2274in_option3;
        break;
      default:
        id2274x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2274out_result = (id2274x_1);
  }
  { // Node ID: 3489 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2284out_result;

  { // Node ID: 2284 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2284in_sel = id2923out_output[getCycle()%9];
    const HWFloat<8,24> &id2284in_option0 = id2274out_result;
    const HWFloat<8,24> &id2284in_option1 = id3489out_value;

    HWFloat<8,24> id2284x_1;

    switch((id2284in_sel.getValueAsLong())) {
      case 0l:
        id2284x_1 = id2284in_option0;
        break;
      case 1l:
        id2284x_1 = id2284in_option1;
        break;
      default:
        id2284x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id2284out_result = (id2284x_1);
  }
  { // Node ID: 3488 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2292out_result;

  { // Node ID: 2292 (NodeSub)
    const HWFloat<8,24> &id2292in_a = id2284out_result;
    const HWFloat<8,24> &id2292in_b = id3488out_value;

    id2292out_result = (sub_float(id2292in_a,id2292in_b));
  }
  { // Node ID: 2891 (NodePO2FPMult)
    const HWFloat<8,24> &id2891in_floatIn = id2292out_result;

    id2891out_floatOut[(getCycle()+1)%2] = (mul_float(id2891in_floatIn,(c_hw_flt_8_24_4_0val)));
  }
  { // Node ID: 3487 (NodeConstantRawBits)
  }
  { // Node ID: 3486 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1927out_result;

  { // Node ID: 1927 (NodeAdd)
    const HWFloat<8,24> &id1927in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id1927in_b = id3486out_value;

    id1927out_result = (add_float(id1927in_a,id1927in_b));
  }
  HWFloat<8,24> id1929out_result;

  { // Node ID: 1929 (NodeMul)
    const HWFloat<8,24> &id1929in_a = id3487out_value;
    const HWFloat<8,24> &id1929in_b = id1927out_result;

    id1929out_result = (mul_float(id1929in_a,id1929in_b));
  }
  HWRawBits<8> id2001out_result;

  { // Node ID: 2001 (NodeSlice)
    const HWFloat<8,24> &id2001in_a = id1929out_result;

    id2001out_result = (slice<23,8>(id2001in_a));
  }
  { // Node ID: 2002 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2628out_result;

  { // Node ID: 2628 (NodeEqInlined)
    const HWRawBits<8> &id2628in_a = id2001out_result;
    const HWRawBits<8> &id2628in_b = id2002out_value;

    id2628out_result = (eq_bits(id2628in_a,id2628in_b));
  }
  HWRawBits<23> id2000out_result;

  { // Node ID: 2000 (NodeSlice)
    const HWFloat<8,24> &id2000in_a = id1929out_result;

    id2000out_result = (slice<0,23>(id2000in_a));
  }
  { // Node ID: 3485 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2629out_result;

  { // Node ID: 2629 (NodeNeqInlined)
    const HWRawBits<23> &id2629in_a = id2000out_result;
    const HWRawBits<23> &id2629in_b = id3485out_value;

    id2629out_result = (neq_bits(id2629in_a,id2629in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2006out_result;

  { // Node ID: 2006 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2006in_a = id2628out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2006in_b = id2629out_result;

    HWOffsetFix<1,0,UNSIGNED> id2006x_1;

    (id2006x_1) = (and_fixed(id2006in_a,id2006in_b));
    id2006out_result = (id2006x_1);
  }
  { // Node ID: 2934 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2934in_input = id2006out_result;

    id2934out_output[(getCycle()+8)%9] = id2934in_input;
  }
  { // Node ID: 1930 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1931out_output;
  HWOffsetFix<1,0,UNSIGNED> id1931out_output_doubt;

  { // Node ID: 1931 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1931in_input = id1929out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1931in_doubt = id1930out_value;

    id1931out_output = id1931in_input;
    id1931out_output_doubt = id1931in_doubt;
  }
  { // Node ID: 1932 (NodeCast)
    const HWFloat<8,24> &id1932in_i = id1931out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1932in_i_doubt = id1931out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1932x_1;

    id1932out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1932in_i,(&(id1932x_1))));
    id1932out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1932x_1),(c_hw_fix_4_0_uns_bits))),id1932in_i_doubt));
  }
  { // Node ID: 1935 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1934out_result;
  HWOffsetFix<1,0,UNSIGNED> id1934out_result_doubt;

  { // Node ID: 1934 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1934in_a = id1932out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1934in_a_doubt = id1932out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1934in_b = id1935out_value;

    HWOffsetFix<1,0,UNSIGNED> id1934x_1;

    id1934out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1934in_a,id1934in_b,(&(id1934x_1))));
    id1934out_result_doubt = (or_fixed((neq_fixed((id1934x_1),(c_hw_fix_1_0_uns_bits_1))),id1934in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1936out_o;
  HWOffsetFix<1,0,UNSIGNED> id1936out_o_doubt;

  { // Node ID: 1936 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1936in_i = id1934out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1936in_i_doubt = id1934out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1936x_1;

    id1936out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1936in_i,(&(id1936x_1))));
    id1936out_o_doubt = (or_fixed((neq_fixed((id1936x_1),(c_hw_fix_1_0_uns_bits_1))),id1936in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1945out_output;

  { // Node ID: 1945 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1945in_input = id1936out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1945in_input_doubt = id1936out_o_doubt;

    id1945out_output = id1945in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1946out_o;

  { // Node ID: 1946 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1946in_i = id1945out_output;

    id1946out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1946in_i));
  }
  { // Node ID: 2925 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2925in_input = id1946out_o;

    id2925out_output[(getCycle()+2)%3] = id2925in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id1948out_o;

  { // Node ID: 1948 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1948in_i = id1945out_output;

    id1948out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1948in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2013out_output;

  { // Node ID: 2013 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2013in_input = id1948out_o;

    id2013out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2013in_input))));
  }
  { // Node ID: 2014 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2014in_addr = id2013out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2014x_1;

    switch(((long)((id2014in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2014x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2014x_1 = (id2014sta_rom_store[(id2014in_addr.getValueAsLong())]);
        break;
      default:
        id2014x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2014out_dout[(getCycle()+2)%3] = (id2014x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1947out_o;

  { // Node ID: 1947 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1947in_i = id1945out_output;

    id1947out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1947in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2010out_output;

  { // Node ID: 2010 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2010in_input = id1947out_o;

    id2010out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2010in_input))));
  }
  { // Node ID: 2011 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2011in_addr = id2010out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2011x_1;

    switch(((long)((id2011in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2011x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2011x_1 = (id2011sta_rom_store[(id2011in_addr.getValueAsLong())]);
        break;
      default:
        id2011x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2011out_dout[(getCycle()+2)%3] = (id2011x_1);
  }
  { // Node ID: 1952 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1949out_o;

  { // Node ID: 1949 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1949in_i = id1945out_output;

    id1949out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1949in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1951out_result;

  { // Node ID: 1951 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1951in_a = id1952out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1951in_b = id1949out_o;

    id1951out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1951in_a,id1951in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1953out_o;

  { // Node ID: 1953 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1953in_i = id1951out_result;

    id1953out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1953in_i));
  }
  { // Node ID: 2926 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2926in_input = id1953out_o;

    id2926out_output[(getCycle()+2)%3] = id2926in_input;
  }
  { // Node ID: 3483 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1938out_result;

  { // Node ID: 1938 (NodeGt)
    const HWFloat<8,24> &id1938in_a = id1929out_result;
    const HWFloat<8,24> &id1938in_b = id3483out_value;

    id1938out_result = (gt_float(id1938in_a,id1938in_b));
  }
  { // Node ID: 2928 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2928in_input = id1938out_result;

    id2928out_output[(getCycle()+6)%7] = id2928in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1939out_output;

  { // Node ID: 1939 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1939in_input = id1936out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1939in_input_doubt = id1936out_o_doubt;

    id1939out_output = id1939in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1940out_result;

  { // Node ID: 1940 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1940in_a = id2928out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1940in_b = id1939out_output;

    HWOffsetFix<1,0,UNSIGNED> id1940x_1;

    (id1940x_1) = (and_fixed(id1940in_a,id1940in_b));
    id1940out_result = (id1940x_1);
  }
  { // Node ID: 2929 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2929in_input = id1940out_result;

    id2929out_output[(getCycle()+2)%3] = id2929in_input;
  }
  { // Node ID: 3482 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1942out_result;

  { // Node ID: 1942 (NodeLt)
    const HWFloat<8,24> &id1942in_a = id1929out_result;
    const HWFloat<8,24> &id1942in_b = id3482out_value;

    id1942out_result = (lt_float(id1942in_a,id1942in_b));
  }
  { // Node ID: 2930 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2930in_input = id1942out_result;

    id2930out_output[(getCycle()+6)%7] = id2930in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1943out_output;

  { // Node ID: 1943 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1943in_input = id1936out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1943in_input_doubt = id1936out_o_doubt;

    id1943out_output = id1943in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1944out_result;

  { // Node ID: 1944 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1944in_a = id2930out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1944in_b = id1943out_output;

    HWOffsetFix<1,0,UNSIGNED> id1944x_1;

    (id1944x_1) = (and_fixed(id1944in_a,id1944in_b));
    id1944out_result = (id1944x_1);
  }
  { // Node ID: 2931 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2931in_input = id1944out_result;

    id2931out_output[(getCycle()+2)%3] = id2931in_input;
  }
  { // Node ID: 3479 (NodeConstantRawBits)
  }
  { // Node ID: 3478 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2016out_result;

  { // Node ID: 2016 (NodeAdd)
    const HWFloat<8,24> &id2016in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id2016in_b = id3478out_value;

    id2016out_result = (add_float(id2016in_a,id2016in_b));
  }
  HWFloat<8,24> id2018out_result;

  { // Node ID: 2018 (NodeMul)
    const HWFloat<8,24> &id2018in_a = id3479out_value;
    const HWFloat<8,24> &id2018in_b = id2016out_result;

    id2018out_result = (mul_float(id2018in_a,id2018in_b));
  }
  HWRawBits<8> id2090out_result;

  { // Node ID: 2090 (NodeSlice)
    const HWFloat<8,24> &id2090in_a = id2018out_result;

    id2090out_result = (slice<23,8>(id2090in_a));
  }
  { // Node ID: 2091 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2635out_result;

  { // Node ID: 2635 (NodeEqInlined)
    const HWRawBits<8> &id2635in_a = id2090out_result;
    const HWRawBits<8> &id2635in_b = id2091out_value;

    id2635out_result = (eq_bits(id2635in_a,id2635in_b));
  }
  HWRawBits<23> id2089out_result;

  { // Node ID: 2089 (NodeSlice)
    const HWFloat<8,24> &id2089in_a = id2018out_result;

    id2089out_result = (slice<0,23>(id2089in_a));
  }
  { // Node ID: 3477 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2636out_result;

  { // Node ID: 2636 (NodeNeqInlined)
    const HWRawBits<23> &id2636in_a = id2089out_result;
    const HWRawBits<23> &id2636in_b = id3477out_value;

    id2636out_result = (neq_bits(id2636in_a,id2636in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2095out_result;

  { // Node ID: 2095 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2095in_a = id2635out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2095in_b = id2636out_result;

    HWOffsetFix<1,0,UNSIGNED> id2095x_1;

    (id2095x_1) = (and_fixed(id2095in_a,id2095in_b));
    id2095out_result = (id2095x_1);
  }
  { // Node ID: 2945 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2945in_input = id2095out_result;

    id2945out_output[(getCycle()+8)%9] = id2945in_input;
  }
  { // Node ID: 2019 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2020out_output;
  HWOffsetFix<1,0,UNSIGNED> id2020out_output_doubt;

  { // Node ID: 2020 (NodeDoubtBitOp)
    const HWFloat<8,24> &id2020in_input = id2018out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2020in_doubt = id2019out_value;

    id2020out_output = id2020in_input;
    id2020out_output_doubt = id2020in_doubt;
  }
  { // Node ID: 2021 (NodeCast)
    const HWFloat<8,24> &id2021in_i = id2020out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2021in_i_doubt = id2020out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2021x_1;

    id2021out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2021in_i,(&(id2021x_1))));
    id2021out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2021x_1),(c_hw_fix_4_0_uns_bits))),id2021in_i_doubt));
  }
  { // Node ID: 2024 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id2023out_result;
  HWOffsetFix<1,0,UNSIGNED> id2023out_result_doubt;

  { // Node ID: 2023 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2023in_a = id2021out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2023in_a_doubt = id2021out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id2023in_b = id2024out_value;

    HWOffsetFix<1,0,UNSIGNED> id2023x_1;

    id2023out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id2023in_a,id2023in_b,(&(id2023x_1))));
    id2023out_result_doubt = (or_fixed((neq_fixed((id2023x_1),(c_hw_fix_1_0_uns_bits_1))),id2023in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2025out_o;
  HWOffsetFix<1,0,UNSIGNED> id2025out_o_doubt;

  { // Node ID: 2025 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id2025in_i = id2023out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2025in_i_doubt = id2023out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2025x_1;

    id2025out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2025in_i,(&(id2025x_1))));
    id2025out_o_doubt = (or_fixed((neq_fixed((id2025x_1),(c_hw_fix_1_0_uns_bits_1))),id2025in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2034out_output;

  { // Node ID: 2034 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2034in_input = id2025out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2034in_input_doubt = id2025out_o_doubt;

    id2034out_output = id2034in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2035out_o;

  { // Node ID: 2035 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2035in_i = id2034out_output;

    id2035out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2035in_i));
  }
  { // Node ID: 2936 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2936in_input = id2035out_o;

    id2936out_output[(getCycle()+2)%3] = id2936in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id2037out_o;

  { // Node ID: 2037 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2037in_i = id2034out_output;

    id2037out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id2037in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2102out_output;

  { // Node ID: 2102 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2102in_input = id2037out_o;

    id2102out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2102in_input))));
  }
  { // Node ID: 2103 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2103in_addr = id2102out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2103x_1;

    switch(((long)((id2103in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2103x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2103x_1 = (id2103sta_rom_store[(id2103in_addr.getValueAsLong())]);
        break;
      default:
        id2103x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2103out_dout[(getCycle()+2)%3] = (id2103x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id2036out_o;

  { // Node ID: 2036 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2036in_i = id2034out_output;

    id2036out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id2036in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2099out_output;

  { // Node ID: 2099 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2099in_input = id2036out_o;

    id2099out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2099in_input))));
  }
  { // Node ID: 2100 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2100in_addr = id2099out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2100x_1;

    switch(((long)((id2100in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2100x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2100x_1 = (id2100sta_rom_store[(id2100in_addr.getValueAsLong())]);
        break;
      default:
        id2100x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2100out_dout[(getCycle()+2)%3] = (id2100x_1);
  }
  { // Node ID: 2041 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id2038out_o;

  { // Node ID: 2038 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2038in_i = id2034out_output;

    id2038out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id2038in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id2040out_result;

  { // Node ID: 2040 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id2040in_a = id2041out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id2040in_b = id2038out_o;

    id2040out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id2040in_a,id2040in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id2042out_o;

  { // Node ID: 2042 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id2042in_i = id2040out_result;

    id2042out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id2042in_i));
  }
  { // Node ID: 2937 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2937in_input = id2042out_o;

    id2937out_output[(getCycle()+2)%3] = id2937in_input;
  }
  { // Node ID: 3475 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2027out_result;

  { // Node ID: 2027 (NodeGt)
    const HWFloat<8,24> &id2027in_a = id2018out_result;
    const HWFloat<8,24> &id2027in_b = id3475out_value;

    id2027out_result = (gt_float(id2027in_a,id2027in_b));
  }
  { // Node ID: 2939 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2939in_input = id2027out_result;

    id2939out_output[(getCycle()+6)%7] = id2939in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2028out_output;

  { // Node ID: 2028 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2028in_input = id2025out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2028in_input_doubt = id2025out_o_doubt;

    id2028out_output = id2028in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2029out_result;

  { // Node ID: 2029 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2029in_a = id2939out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2029in_b = id2028out_output;

    HWOffsetFix<1,0,UNSIGNED> id2029x_1;

    (id2029x_1) = (and_fixed(id2029in_a,id2029in_b));
    id2029out_result = (id2029x_1);
  }
  { // Node ID: 2940 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2940in_input = id2029out_result;

    id2940out_output[(getCycle()+2)%3] = id2940in_input;
  }
  { // Node ID: 3474 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2031out_result;

  { // Node ID: 2031 (NodeLt)
    const HWFloat<8,24> &id2031in_a = id2018out_result;
    const HWFloat<8,24> &id2031in_b = id3474out_value;

    id2031out_result = (lt_float(id2031in_a,id2031in_b));
  }
  { // Node ID: 2941 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2941in_input = id2031out_result;

    id2941out_output[(getCycle()+6)%7] = id2941in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2032out_output;

  { // Node ID: 2032 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2032in_input = id2025out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2032in_input_doubt = id2025out_o_doubt;

    id2032out_output = id2032in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2033out_result;

  { // Node ID: 2033 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2033in_a = id2941out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2033in_b = id2032out_output;

    HWOffsetFix<1,0,UNSIGNED> id2033x_1;

    (id2033x_1) = (and_fixed(id2033in_a,id2033in_b));
    id2033out_result = (id2033x_1);
  }
  { // Node ID: 2942 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2942in_input = id2033out_result;

    id2942out_output[(getCycle()+2)%3] = id2942in_input;
  }
  { // Node ID: 3468 (NodeConstantRawBits)
  }
  { // Node ID: 3467 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2106out_result;

  { // Node ID: 2106 (NodeAdd)
    const HWFloat<8,24> &id2106in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id2106in_b = id3467out_value;

    id2106out_result = (add_float(id2106in_a,id2106in_b));
  }
  HWFloat<8,24> id2108out_result;

  { // Node ID: 2108 (NodeMul)
    const HWFloat<8,24> &id2108in_a = id3468out_value;
    const HWFloat<8,24> &id2108in_b = id2106out_result;

    id2108out_result = (mul_float(id2108in_a,id2108in_b));
  }
  HWRawBits<8> id2180out_result;

  { // Node ID: 2180 (NodeSlice)
    const HWFloat<8,24> &id2180in_a = id2108out_result;

    id2180out_result = (slice<23,8>(id2180in_a));
  }
  { // Node ID: 2181 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2642out_result;

  { // Node ID: 2642 (NodeEqInlined)
    const HWRawBits<8> &id2642in_a = id2180out_result;
    const HWRawBits<8> &id2642in_b = id2181out_value;

    id2642out_result = (eq_bits(id2642in_a,id2642in_b));
  }
  HWRawBits<23> id2179out_result;

  { // Node ID: 2179 (NodeSlice)
    const HWFloat<8,24> &id2179in_a = id2108out_result;

    id2179out_result = (slice<0,23>(id2179in_a));
  }
  { // Node ID: 3466 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2643out_result;

  { // Node ID: 2643 (NodeNeqInlined)
    const HWRawBits<23> &id2643in_a = id2179out_result;
    const HWRawBits<23> &id2643in_b = id3466out_value;

    id2643out_result = (neq_bits(id2643in_a,id2643in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2185out_result;

  { // Node ID: 2185 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2185in_a = id2642out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2185in_b = id2643out_result;

    HWOffsetFix<1,0,UNSIGNED> id2185x_1;

    (id2185x_1) = (and_fixed(id2185in_a,id2185in_b));
    id2185out_result = (id2185x_1);
  }
  { // Node ID: 2957 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2957in_input = id2185out_result;

    id2957out_output[(getCycle()+8)%9] = id2957in_input;
  }
  { // Node ID: 2109 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2110out_output;
  HWOffsetFix<1,0,UNSIGNED> id2110out_output_doubt;

  { // Node ID: 2110 (NodeDoubtBitOp)
    const HWFloat<8,24> &id2110in_input = id2108out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2110in_doubt = id2109out_value;

    id2110out_output = id2110in_input;
    id2110out_output_doubt = id2110in_doubt;
  }
  { // Node ID: 2111 (NodeCast)
    const HWFloat<8,24> &id2111in_i = id2110out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2111in_i_doubt = id2110out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2111x_1;

    id2111out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2111in_i,(&(id2111x_1))));
    id2111out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2111x_1),(c_hw_fix_4_0_uns_bits))),id2111in_i_doubt));
  }
  { // Node ID: 2114 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id2113out_result;
  HWOffsetFix<1,0,UNSIGNED> id2113out_result_doubt;

  { // Node ID: 2113 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2113in_a = id2111out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2113in_a_doubt = id2111out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id2113in_b = id2114out_value;

    HWOffsetFix<1,0,UNSIGNED> id2113x_1;

    id2113out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id2113in_a,id2113in_b,(&(id2113x_1))));
    id2113out_result_doubt = (or_fixed((neq_fixed((id2113x_1),(c_hw_fix_1_0_uns_bits_1))),id2113in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2115out_o;
  HWOffsetFix<1,0,UNSIGNED> id2115out_o_doubt;

  { // Node ID: 2115 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id2115in_i = id2113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2115in_i_doubt = id2113out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2115x_1;

    id2115out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2115in_i,(&(id2115x_1))));
    id2115out_o_doubt = (or_fixed((neq_fixed((id2115x_1),(c_hw_fix_1_0_uns_bits_1))),id2115in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2124out_output;

  { // Node ID: 2124 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2124in_input = id2115out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2124in_input_doubt = id2115out_o_doubt;

    id2124out_output = id2124in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2125out_o;

  { // Node ID: 2125 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2125in_i = id2124out_output;

    id2125out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2125in_i));
  }
  { // Node ID: 2948 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2948in_input = id2125out_o;

    id2948out_output[(getCycle()+2)%3] = id2948in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id2127out_o;

  { // Node ID: 2127 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2127in_i = id2124out_output;

    id2127out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id2127in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2192out_output;

  { // Node ID: 2192 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2192in_input = id2127out_o;

    id2192out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2192in_input))));
  }
  { // Node ID: 2193 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2193in_addr = id2192out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2193x_1;

    switch(((long)((id2193in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2193x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2193x_1 = (id2193sta_rom_store[(id2193in_addr.getValueAsLong())]);
        break;
      default:
        id2193x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2193out_dout[(getCycle()+2)%3] = (id2193x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id2126out_o;

  { // Node ID: 2126 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2126in_i = id2124out_output;

    id2126out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id2126in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2189out_output;

  { // Node ID: 2189 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2189in_input = id2126out_o;

    id2189out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2189in_input))));
  }
  { // Node ID: 2190 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2190in_addr = id2189out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2190x_1;

    switch(((long)((id2190in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2190x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2190x_1 = (id2190sta_rom_store[(id2190in_addr.getValueAsLong())]);
        break;
      default:
        id2190x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2190out_dout[(getCycle()+2)%3] = (id2190x_1);
  }
  { // Node ID: 2131 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id2128out_o;

  { // Node ID: 2128 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2128in_i = id2124out_output;

    id2128out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id2128in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id2130out_result;

  { // Node ID: 2130 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id2130in_a = id2131out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id2130in_b = id2128out_o;

    id2130out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id2130in_a,id2130in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id2132out_o;

  { // Node ID: 2132 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id2132in_i = id2130out_result;

    id2132out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id2132in_i));
  }
  { // Node ID: 2949 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2949in_input = id2132out_o;

    id2949out_output[(getCycle()+2)%3] = id2949in_input;
  }
  { // Node ID: 3464 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2117out_result;

  { // Node ID: 2117 (NodeGt)
    const HWFloat<8,24> &id2117in_a = id2108out_result;
    const HWFloat<8,24> &id2117in_b = id3464out_value;

    id2117out_result = (gt_float(id2117in_a,id2117in_b));
  }
  { // Node ID: 2951 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2951in_input = id2117out_result;

    id2951out_output[(getCycle()+6)%7] = id2951in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2118out_output;

  { // Node ID: 2118 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2118in_input = id2115out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2118in_input_doubt = id2115out_o_doubt;

    id2118out_output = id2118in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2119out_result;

  { // Node ID: 2119 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2119in_a = id2951out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2119in_b = id2118out_output;

    HWOffsetFix<1,0,UNSIGNED> id2119x_1;

    (id2119x_1) = (and_fixed(id2119in_a,id2119in_b));
    id2119out_result = (id2119x_1);
  }
  { // Node ID: 2952 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2952in_input = id2119out_result;

    id2952out_output[(getCycle()+2)%3] = id2952in_input;
  }
  { // Node ID: 3463 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2121out_result;

  { // Node ID: 2121 (NodeLt)
    const HWFloat<8,24> &id2121in_a = id2108out_result;
    const HWFloat<8,24> &id2121in_b = id3463out_value;

    id2121out_result = (lt_float(id2121in_a,id2121in_b));
  }
  { // Node ID: 2953 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2953in_input = id2121out_result;

    id2953out_output[(getCycle()+6)%7] = id2953in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2122out_output;

  { // Node ID: 2122 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2122in_input = id2115out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2122in_input_doubt = id2115out_o_doubt;

    id2122out_output = id2122in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2123out_result;

  { // Node ID: 2123 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2123in_a = id2953out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2123in_b = id2122out_output;

    HWOffsetFix<1,0,UNSIGNED> id2123x_1;

    (id2123x_1) = (and_fixed(id2123in_a,id2123in_b));
    id2123out_result = (id2123x_1);
  }
  { // Node ID: 2954 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2954in_input = id2123out_result;

    id2954out_output[(getCycle()+2)%3] = id2954in_input;
  }
  { // Node ID: 3459 (NodeConstantRawBits)
  }
  { // Node ID: 3458 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2305out_result;

  { // Node ID: 2305 (NodeAdd)
    const HWFloat<8,24> &id2305in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id2305in_b = id3458out_value;

    id2305out_result = (add_float(id2305in_a,id2305in_b));
  }
  HWFloat<8,24> id2307out_result;

  { // Node ID: 2307 (NodeMul)
    const HWFloat<8,24> &id2307in_a = id3459out_value;
    const HWFloat<8,24> &id2307in_b = id2305out_result;

    id2307out_result = (mul_float(id2307in_a,id2307in_b));
  }
  HWRawBits<8> id2379out_result;

  { // Node ID: 2379 (NodeSlice)
    const HWFloat<8,24> &id2379in_a = id2307out_result;

    id2379out_result = (slice<23,8>(id2379in_a));
  }
  { // Node ID: 2380 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2649out_result;

  { // Node ID: 2649 (NodeEqInlined)
    const HWRawBits<8> &id2649in_a = id2379out_result;
    const HWRawBits<8> &id2649in_b = id2380out_value;

    id2649out_result = (eq_bits(id2649in_a,id2649in_b));
  }
  HWRawBits<23> id2378out_result;

  { // Node ID: 2378 (NodeSlice)
    const HWFloat<8,24> &id2378in_a = id2307out_result;

    id2378out_result = (slice<0,23>(id2378in_a));
  }
  { // Node ID: 3457 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2650out_result;

  { // Node ID: 2650 (NodeNeqInlined)
    const HWRawBits<23> &id2650in_a = id2378out_result;
    const HWRawBits<23> &id2650in_b = id3457out_value;

    id2650out_result = (neq_bits(id2650in_a,id2650in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2384out_result;

  { // Node ID: 2384 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2384in_a = id2649out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2384in_b = id2650out_result;

    HWOffsetFix<1,0,UNSIGNED> id2384x_1;

    (id2384x_1) = (and_fixed(id2384in_a,id2384in_b));
    id2384out_result = (id2384x_1);
  }
  { // Node ID: 2968 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2968in_input = id2384out_result;

    id2968out_output[(getCycle()+8)%9] = id2968in_input;
  }
  { // Node ID: 2308 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2309out_output;
  HWOffsetFix<1,0,UNSIGNED> id2309out_output_doubt;

  { // Node ID: 2309 (NodeDoubtBitOp)
    const HWFloat<8,24> &id2309in_input = id2307out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2309in_doubt = id2308out_value;

    id2309out_output = id2309in_input;
    id2309out_output_doubt = id2309in_doubt;
  }
  { // Node ID: 2310 (NodeCast)
    const HWFloat<8,24> &id2310in_i = id2309out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2310in_i_doubt = id2309out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2310x_1;

    id2310out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2310in_i,(&(id2310x_1))));
    id2310out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2310x_1),(c_hw_fix_4_0_uns_bits))),id2310in_i_doubt));
  }
  { // Node ID: 2313 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id2312out_result;
  HWOffsetFix<1,0,UNSIGNED> id2312out_result_doubt;

  { // Node ID: 2312 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2312in_a = id2310out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2312in_a_doubt = id2310out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id2312in_b = id2313out_value;

    HWOffsetFix<1,0,UNSIGNED> id2312x_1;

    id2312out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id2312in_a,id2312in_b,(&(id2312x_1))));
    id2312out_result_doubt = (or_fixed((neq_fixed((id2312x_1),(c_hw_fix_1_0_uns_bits_1))),id2312in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2314out_o;
  HWOffsetFix<1,0,UNSIGNED> id2314out_o_doubt;

  { // Node ID: 2314 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id2314in_i = id2312out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2314in_i_doubt = id2312out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2314x_1;

    id2314out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2314in_i,(&(id2314x_1))));
    id2314out_o_doubt = (or_fixed((neq_fixed((id2314x_1),(c_hw_fix_1_0_uns_bits_1))),id2314in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2323out_output;

  { // Node ID: 2323 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2323in_input = id2314out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2323in_input_doubt = id2314out_o_doubt;

    id2323out_output = id2323in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2324out_o;

  { // Node ID: 2324 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2324in_i = id2323out_output;

    id2324out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2324in_i));
  }
  { // Node ID: 2959 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2959in_input = id2324out_o;

    id2959out_output[(getCycle()+2)%3] = id2959in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id2326out_o;

  { // Node ID: 2326 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2326in_i = id2323out_output;

    id2326out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id2326in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2391out_output;

  { // Node ID: 2391 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2391in_input = id2326out_o;

    id2391out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2391in_input))));
  }
  { // Node ID: 2392 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2392in_addr = id2391out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2392x_1;

    switch(((long)((id2392in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2392x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2392x_1 = (id2392sta_rom_store[(id2392in_addr.getValueAsLong())]);
        break;
      default:
        id2392x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2392out_dout[(getCycle()+2)%3] = (id2392x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id2325out_o;

  { // Node ID: 2325 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2325in_i = id2323out_output;

    id2325out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id2325in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2388out_output;

  { // Node ID: 2388 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2388in_input = id2325out_o;

    id2388out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2388in_input))));
  }
  { // Node ID: 2389 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2389in_addr = id2388out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2389x_1;

    switch(((long)((id2389in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2389x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2389x_1 = (id2389sta_rom_store[(id2389in_addr.getValueAsLong())]);
        break;
      default:
        id2389x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2389out_dout[(getCycle()+2)%3] = (id2389x_1);
  }
  { // Node ID: 2330 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id2327out_o;

  { // Node ID: 2327 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2327in_i = id2323out_output;

    id2327out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id2327in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id2329out_result;

  { // Node ID: 2329 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id2329in_a = id2330out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id2329in_b = id2327out_o;

    id2329out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id2329in_a,id2329in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id2331out_o;

  { // Node ID: 2331 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id2331in_i = id2329out_result;

    id2331out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id2331in_i));
  }
  { // Node ID: 2960 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2960in_input = id2331out_o;

    id2960out_output[(getCycle()+2)%3] = id2960in_input;
  }
  { // Node ID: 3455 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2316out_result;

  { // Node ID: 2316 (NodeGt)
    const HWFloat<8,24> &id2316in_a = id2307out_result;
    const HWFloat<8,24> &id2316in_b = id3455out_value;

    id2316out_result = (gt_float(id2316in_a,id2316in_b));
  }
  { // Node ID: 2962 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2962in_input = id2316out_result;

    id2962out_output[(getCycle()+6)%7] = id2962in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2317out_output;

  { // Node ID: 2317 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2317in_input = id2314out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2317in_input_doubt = id2314out_o_doubt;

    id2317out_output = id2317in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2318out_result;

  { // Node ID: 2318 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2318in_a = id2962out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2318in_b = id2317out_output;

    HWOffsetFix<1,0,UNSIGNED> id2318x_1;

    (id2318x_1) = (and_fixed(id2318in_a,id2318in_b));
    id2318out_result = (id2318x_1);
  }
  { // Node ID: 2963 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2963in_input = id2318out_result;

    id2963out_output[(getCycle()+2)%3] = id2963in_input;
  }
  { // Node ID: 3454 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2320out_result;

  { // Node ID: 2320 (NodeLt)
    const HWFloat<8,24> &id2320in_a = id2307out_result;
    const HWFloat<8,24> &id2320in_b = id3454out_value;

    id2320out_result = (lt_float(id2320in_a,id2320in_b));
  }
  { // Node ID: 2964 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2964in_input = id2320out_result;

    id2964out_output[(getCycle()+6)%7] = id2964in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2321out_output;

  { // Node ID: 2321 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2321in_input = id2314out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2321in_input_doubt = id2314out_o_doubt;

    id2321out_output = id2321in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2322out_result;

  { // Node ID: 2322 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2322in_a = id2964out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2322in_b = id2321out_output;

    HWOffsetFix<1,0,UNSIGNED> id2322x_1;

    (id2322x_1) = (and_fixed(id2322in_a,id2322in_b));
    id2322out_result = (id2322x_1);
  }
  { // Node ID: 2965 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2965in_input = id2322out_result;

    id2965out_output[(getCycle()+2)%3] = id2965in_input;
  }
  { // Node ID: 3450 (NodeConstantRawBits)
  }
  { // Node ID: 3449 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2398out_result;

  { // Node ID: 2398 (NodeAdd)
    const HWFloat<8,24> &id2398in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id2398in_b = id3449out_value;

    id2398out_result = (add_float(id2398in_a,id2398in_b));
  }
  HWFloat<8,24> id2400out_result;

  { // Node ID: 2400 (NodeMul)
    const HWFloat<8,24> &id2400in_a = id3450out_value;
    const HWFloat<8,24> &id2400in_b = id2398out_result;

    id2400out_result = (mul_float(id2400in_a,id2400in_b));
  }
  HWRawBits<8> id2472out_result;

  { // Node ID: 2472 (NodeSlice)
    const HWFloat<8,24> &id2472in_a = id2400out_result;

    id2472out_result = (slice<23,8>(id2472in_a));
  }
  { // Node ID: 2473 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2656out_result;

  { // Node ID: 2656 (NodeEqInlined)
    const HWRawBits<8> &id2656in_a = id2472out_result;
    const HWRawBits<8> &id2656in_b = id2473out_value;

    id2656out_result = (eq_bits(id2656in_a,id2656in_b));
  }
  HWRawBits<23> id2471out_result;

  { // Node ID: 2471 (NodeSlice)
    const HWFloat<8,24> &id2471in_a = id2400out_result;

    id2471out_result = (slice<0,23>(id2471in_a));
  }
  { // Node ID: 3448 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2657out_result;

  { // Node ID: 2657 (NodeNeqInlined)
    const HWRawBits<23> &id2657in_a = id2471out_result;
    const HWRawBits<23> &id2657in_b = id3448out_value;

    id2657out_result = (neq_bits(id2657in_a,id2657in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2477out_result;

  { // Node ID: 2477 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2477in_a = id2656out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2477in_b = id2657out_result;

    HWOffsetFix<1,0,UNSIGNED> id2477x_1;

    (id2477x_1) = (and_fixed(id2477in_a,id2477in_b));
    id2477out_result = (id2477x_1);
  }
  { // Node ID: 2979 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2979in_input = id2477out_result;

    id2979out_output[(getCycle()+8)%9] = id2979in_input;
  }
  { // Node ID: 2401 (NodeConstantRawBits)
  }
  HWFloat<8,24> id2402out_output;
  HWOffsetFix<1,0,UNSIGNED> id2402out_output_doubt;

  { // Node ID: 2402 (NodeDoubtBitOp)
    const HWFloat<8,24> &id2402in_input = id2400out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2402in_doubt = id2401out_value;

    id2402out_output = id2402in_input;
    id2402out_output_doubt = id2402in_doubt;
  }
  { // Node ID: 2403 (NodeCast)
    const HWFloat<8,24> &id2403in_i = id2402out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2403in_i_doubt = id2402out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2403x_1;

    id2403out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2403in_i,(&(id2403x_1))));
    id2403out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2403x_1),(c_hw_fix_4_0_uns_bits))),id2403in_i_doubt));
  }
  { // Node ID: 2406 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id2405out_result;
  HWOffsetFix<1,0,UNSIGNED> id2405out_result_doubt;

  { // Node ID: 2405 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2405in_a = id2403out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2405in_a_doubt = id2403out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id2405in_b = id2406out_value;

    HWOffsetFix<1,0,UNSIGNED> id2405x_1;

    id2405out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id2405in_a,id2405in_b,(&(id2405x_1))));
    id2405out_result_doubt = (or_fixed((neq_fixed((id2405x_1),(c_hw_fix_1_0_uns_bits_1))),id2405in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2407out_o;
  HWOffsetFix<1,0,UNSIGNED> id2407out_o_doubt;

  { // Node ID: 2407 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id2407in_i = id2405out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2407in_i_doubt = id2405out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2407x_1;

    id2407out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id2407in_i,(&(id2407x_1))));
    id2407out_o_doubt = (or_fixed((neq_fixed((id2407x_1),(c_hw_fix_1_0_uns_bits_1))),id2407in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id2416out_output;

  { // Node ID: 2416 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2416in_input = id2407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2416in_input_doubt = id2407out_o_doubt;

    id2416out_output = id2416in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2417out_o;

  { // Node ID: 2417 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2417in_i = id2416out_output;

    id2417out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2417in_i));
  }
  { // Node ID: 2970 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2970in_input = id2417out_o;

    id2970out_output[(getCycle()+2)%3] = id2970in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id2419out_o;

  { // Node ID: 2419 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2419in_i = id2416out_output;

    id2419out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id2419in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2484out_output;

  { // Node ID: 2484 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id2484in_input = id2419out_o;

    id2484out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2484in_input))));
  }
  { // Node ID: 2485 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2485in_addr = id2484out_output;

    HWOffsetFix<22,-24,UNSIGNED> id2485x_1;

    switch(((long)((id2485in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2485x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id2485x_1 = (id2485sta_rom_store[(id2485in_addr.getValueAsLong())]);
        break;
      default:
        id2485x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id2485out_dout[(getCycle()+2)%3] = (id2485x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id2418out_o;

  { // Node ID: 2418 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2418in_i = id2416out_output;

    id2418out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id2418in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id2481out_output;

  { // Node ID: 2481 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id2481in_input = id2418out_o;

    id2481out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id2481in_input))));
  }
  { // Node ID: 2482 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id2482in_addr = id2481out_output;

    HWOffsetFix<24,-24,UNSIGNED> id2482x_1;

    switch(((long)((id2482in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id2482x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id2482x_1 = (id2482sta_rom_store[(id2482in_addr.getValueAsLong())]);
        break;
      default:
        id2482x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id2482out_dout[(getCycle()+2)%3] = (id2482x_1);
  }
  { // Node ID: 2423 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id2420out_o;

  { // Node ID: 2420 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2420in_i = id2416out_output;

    id2420out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id2420in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id2422out_result;

  { // Node ID: 2422 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id2422in_a = id2423out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id2422in_b = id2420out_o;

    id2422out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id2422in_a,id2422in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id2424out_o;

  { // Node ID: 2424 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id2424in_i = id2422out_result;

    id2424out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id2424in_i));
  }
  { // Node ID: 2971 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2971in_input = id2424out_o;

    id2971out_output[(getCycle()+2)%3] = id2971in_input;
  }
  { // Node ID: 3446 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2409out_result;

  { // Node ID: 2409 (NodeGt)
    const HWFloat<8,24> &id2409in_a = id2400out_result;
    const HWFloat<8,24> &id2409in_b = id3446out_value;

    id2409out_result = (gt_float(id2409in_a,id2409in_b));
  }
  { // Node ID: 2973 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2973in_input = id2409out_result;

    id2973out_output[(getCycle()+6)%7] = id2973in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2410out_output;

  { // Node ID: 2410 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2410in_input = id2407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2410in_input_doubt = id2407out_o_doubt;

    id2410out_output = id2410in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2411out_result;

  { // Node ID: 2411 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2411in_a = id2973out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2411in_b = id2410out_output;

    HWOffsetFix<1,0,UNSIGNED> id2411x_1;

    (id2411x_1) = (and_fixed(id2411in_a,id2411in_b));
    id2411out_result = (id2411x_1);
  }
  { // Node ID: 2974 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2974in_input = id2411out_result;

    id2974out_output[(getCycle()+2)%3] = id2974in_input;
  }
  { // Node ID: 3445 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2413out_result;

  { // Node ID: 2413 (NodeLt)
    const HWFloat<8,24> &id2413in_a = id2400out_result;
    const HWFloat<8,24> &id2413in_b = id3445out_value;

    id2413out_result = (lt_float(id2413in_a,id2413in_b));
  }
  { // Node ID: 2975 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2975in_input = id2413out_result;

    id2975out_output[(getCycle()+6)%7] = id2975in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2414out_output;

  { // Node ID: 2414 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id2414in_input = id2407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2414in_input_doubt = id2407out_o_doubt;

    id2414out_output = id2414in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2415out_result;

  { // Node ID: 2415 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2415in_a = id2975out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2415in_b = id2414out_output;

    HWOffsetFix<1,0,UNSIGNED> id2415x_1;

    (id2415x_1) = (and_fixed(id2415in_a,id2415in_b));
    id2415out_result = (id2415x_1);
  }
  { // Node ID: 2976 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2976in_input = id2415out_result;

    id2976out_output[(getCycle()+2)%3] = id2976in_input;
  }
  { // Node ID: 3442 (NodeConstantRawBits)
  }
  { // Node ID: 2663 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2663in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2663in_b = id3442out_value;

    id2663out_result[(getCycle()+1)%2] = (eq_fixed(id2663in_a,id2663in_b));
  }
  HWFloat<8,24> id2905out_output;

  { // Node ID: 2905 (NodeStreamOffset)
    const HWFloat<8,24> &id2905in_input = id2980out_output[getCycle()%2];

    id2905out_output = id2905in_input;
  }
  { // Node ID: 24 (NodeInputMappedReg)
  }
  { // Node ID: 25 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id25in_sel = id2663out_result[getCycle()%2];
    const HWFloat<8,24> &id25in_option0 = id2905out_output;
    const HWFloat<8,24> &id25in_option1 = id24out_x1_in;

    HWFloat<8,24> id25x_1;

    switch((id25in_sel.getValueAsLong())) {
      case 0l:
        id25x_1 = id25in_option0;
        break;
      case 1l:
        id25x_1 = id25in_option1;
        break;
      default:
        id25x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id25out_result[(getCycle()+1)%2] = (id25x_1);
  }
  { // Node ID: 3001 (NodeFIFO)
    const HWFloat<8,24> &id3001in_input = id25out_result[getCycle()%2];

    id3001out_output[(getCycle()+8)%9] = id3001in_input;
  }
  { // Node ID: 3441 (NodeConstantRawBits)
  }
  { // Node ID: 3440 (NodeConstantRawBits)
  }
  { // Node ID: 3439 (NodeConstantRawBits)
  }
  HWFloat<8,24> id47out_result;

  { // Node ID: 47 (NodeAdd)
    const HWFloat<8,24> &id47in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id47in_b = id3439out_value;

    id47out_result = (add_float(id47in_a,id47in_b));
  }
  HWFloat<8,24> id49out_result;

  { // Node ID: 49 (NodeMul)
    const HWFloat<8,24> &id49in_a = id3440out_value;
    const HWFloat<8,24> &id49in_b = id47out_result;

    id49out_result = (mul_float(id49in_a,id49in_b));
  }
  HWRawBits<8> id121out_result;

  { // Node ID: 121 (NodeSlice)
    const HWFloat<8,24> &id121in_a = id49out_result;

    id121out_result = (slice<23,8>(id121in_a));
  }
  { // Node ID: 122 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2664out_result;

  { // Node ID: 2664 (NodeEqInlined)
    const HWRawBits<8> &id2664in_a = id121out_result;
    const HWRawBits<8> &id2664in_b = id122out_value;

    id2664out_result = (eq_bits(id2664in_a,id2664in_b));
  }
  HWRawBits<23> id120out_result;

  { // Node ID: 120 (NodeSlice)
    const HWFloat<8,24> &id120in_a = id49out_result;

    id120out_result = (slice<0,23>(id120in_a));
  }
  { // Node ID: 3438 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2665out_result;

  { // Node ID: 2665 (NodeNeqInlined)
    const HWRawBits<23> &id2665in_a = id120out_result;
    const HWRawBits<23> &id2665in_b = id3438out_value;

    id2665out_result = (neq_bits(id2665in_a,id2665in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id126out_result;

  { // Node ID: 126 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id126in_a = id2664out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id126in_b = id2665out_result;

    HWOffsetFix<1,0,UNSIGNED> id126x_1;

    (id126x_1) = (and_fixed(id126in_a,id126in_b));
    id126out_result = (id126x_1);
  }
  { // Node ID: 2990 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2990in_input = id126out_result;

    id2990out_output[(getCycle()+8)%9] = id2990in_input;
  }
  { // Node ID: 50 (NodeConstantRawBits)
  }
  HWFloat<8,24> id51out_output;
  HWOffsetFix<1,0,UNSIGNED> id51out_output_doubt;

  { // Node ID: 51 (NodeDoubtBitOp)
    const HWFloat<8,24> &id51in_input = id49out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id51in_doubt = id50out_value;

    id51out_output = id51in_input;
    id51out_output_doubt = id51in_doubt;
  }
  { // Node ID: 52 (NodeCast)
    const HWFloat<8,24> &id52in_i = id51out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id52in_i_doubt = id51out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id52x_1;

    id52out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id52in_i,(&(id52x_1))));
    id52out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id52x_1),(c_hw_fix_4_0_uns_bits))),id52in_i_doubt));
  }
  { // Node ID: 55 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id54out_result;
  HWOffsetFix<1,0,UNSIGNED> id54out_result_doubt;

  { // Node ID: 54 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id54in_a = id52out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id54in_a_doubt = id52out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id54in_b = id55out_value;

    HWOffsetFix<1,0,UNSIGNED> id54x_1;

    id54out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id54in_a,id54in_b,(&(id54x_1))));
    id54out_result_doubt = (or_fixed((neq_fixed((id54x_1),(c_hw_fix_1_0_uns_bits_1))),id54in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id56out_o;
  HWOffsetFix<1,0,UNSIGNED> id56out_o_doubt;

  { // Node ID: 56 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id56in_i = id54out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id56in_i_doubt = id54out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id56x_1;

    id56out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id56in_i,(&(id56x_1))));
    id56out_o_doubt = (or_fixed((neq_fixed((id56x_1),(c_hw_fix_1_0_uns_bits_1))),id56in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id65out_output;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id65in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id65in_input_doubt = id56out_o_doubt;

    id65out_output = id65in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id66out_o;

  { // Node ID: 66 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id66in_i = id65out_output;

    id66out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id66in_i));
  }
  { // Node ID: 2981 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2981in_input = id66out_o;

    id2981out_output[(getCycle()+2)%3] = id2981in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id88out_o;

  { // Node ID: 88 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id88in_i = id2981out_output[getCycle()%3];

    id88out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id88in_i));
  }
  { // Node ID: 91 (NodeConstantRawBits)
  }
  { // Node ID: 2826 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id68out_o;

  { // Node ID: 68 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id68in_i = id65out_output;

    id68out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id68in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id133out_output;

  { // Node ID: 133 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id133in_input = id68out_o;

    id133out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id133in_input))));
  }
  { // Node ID: 134 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id134in_addr = id133out_output;

    HWOffsetFix<22,-24,UNSIGNED> id134x_1;

    switch(((long)((id134in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id134x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id134x_1 = (id134sta_rom_store[(id134in_addr.getValueAsLong())]);
        break;
      default:
        id134x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id134out_dout[(getCycle()+2)%3] = (id134x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id67out_o;

  { // Node ID: 67 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id67in_i = id65out_output;

    id67out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id67in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id130out_output;

  { // Node ID: 130 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id130in_input = id67out_o;

    id130out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id130in_input))));
  }
  { // Node ID: 131 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id131in_addr = id130out_output;

    HWOffsetFix<24,-24,UNSIGNED> id131x_1;

    switch(((long)((id131in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id131x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id131x_1 = (id131sta_rom_store[(id131in_addr.getValueAsLong())]);
        break;
      default:
        id131x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id131out_dout[(getCycle()+2)%3] = (id131x_1);
  }
  { // Node ID: 72 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id69out_o;

  { // Node ID: 69 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id69in_i = id65out_output;

    id69out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id69in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id71out_result;

  { // Node ID: 71 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id71in_a = id72out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id71in_b = id69out_o;

    id71out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id71in_a,id71in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id73out_o;

  { // Node ID: 73 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id73in_i = id71out_result;

    id73out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id73in_i));
  }
  { // Node ID: 2982 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2982in_input = id73out_o;

    id2982out_output[(getCycle()+2)%3] = id2982in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id74out_result;

  { // Node ID: 74 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id74in_a = id131out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id74in_b = id2982out_output[getCycle()%3];

    id74out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id74in_a,id74in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id75out_result;

  { // Node ID: 75 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id75in_a = id2982out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id75in_b = id131out_dout[getCycle()%3];

    id75out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id75in_a,id75in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id76in_a = id74out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id76in_b = id75out_result;

    id76out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id76in_a,id76in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id77out_o;

  { // Node ID: 77 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id77in_i = id76out_result;

    id77out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id77in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id78out_result;

  { // Node ID: 78 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id78in_a = id134out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id78in_b = id77out_o;

    id78out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id78in_a,id78in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id79out_result;

  { // Node ID: 79 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id79in_a = id77out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id79in_b = id134out_dout[getCycle()%3];

    id79out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id79in_a,id79in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id80out_result;

  { // Node ID: 80 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id80in_a = id78out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id80in_b = id79out_result;

    id80out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id80in_a,id80in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id81out_o;

  { // Node ID: 81 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id81in_i = id80out_result;

    id81out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id81in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id82in_i = id81out_o;

    id82out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id82in_i));
  }
  { // Node ID: 3437 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2666out_result;

  { // Node ID: 2666 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2666in_a = id82out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2666in_b = id3437out_value;

    id2666out_result = (gte_fixed(id2666in_a,id2666in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2884out_result;

  { // Node ID: 2884 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2884in_a = id88out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2884in_b = id91out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2884in_c = id2826out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2884in_condb = id2666out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2884x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2884x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2884x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2884x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2884x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2884x_1 = id2884in_a;
        break;
      default:
        id2884x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2884in_condb.getValueAsLong())) {
      case 0l:
        id2884x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2884x_2 = id2884in_b;
        break;
      default:
        id2884x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2884x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2884x_3 = id2884in_c;
        break;
      default:
        id2884x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2884x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2884x_1),(id2884x_2))),(id2884x_3)));
    id2884out_result = (id2884x_4);
  }
  HWRawBits<1> id2667out_result;

  { // Node ID: 2667 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2667in_a = id2884out_result;

    id2667out_result = (slice<10,1>(id2667in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2668out_output;

  { // Node ID: 2668 (NodeReinterpret)
    const HWRawBits<1> &id2668in_input = id2667out_result;

    id2668out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2668in_input));
  }
  { // Node ID: 3436 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<8,24> &id58in_a = id49out_result;
    const HWFloat<8,24> &id58in_b = id3436out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 2984 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2984in_input = id58out_result;

    id2984out_output[(getCycle()+6)%7] = id2984in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id59out_output;

  { // Node ID: 59 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id59in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id59in_input_doubt = id56out_o_doubt;

    id59out_output = id59in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id60out_result;

  { // Node ID: 60 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_a = id2984out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id60in_b = id59out_output;

    HWOffsetFix<1,0,UNSIGNED> id60x_1;

    (id60x_1) = (and_fixed(id60in_a,id60in_b));
    id60out_result = (id60x_1);
  }
  { // Node ID: 2985 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2985in_input = id60out_result;

    id2985out_output[(getCycle()+2)%3] = id2985in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id97out_result;

  { // Node ID: 97 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id97in_a = id2985out_output[getCycle()%3];

    id97out_result = (not_fixed(id97in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id98out_result;

  { // Node ID: 98 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id98in_a = id2668out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id98in_b = id97out_result;

    HWOffsetFix<1,0,UNSIGNED> id98x_1;

    (id98x_1) = (and_fixed(id98in_a,id98in_b));
    id98out_result = (id98x_1);
  }
  { // Node ID: 3435 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id62out_result;

  { // Node ID: 62 (NodeLt)
    const HWFloat<8,24> &id62in_a = id49out_result;
    const HWFloat<8,24> &id62in_b = id3435out_value;

    id62out_result = (lt_float(id62in_a,id62in_b));
  }
  { // Node ID: 2986 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2986in_input = id62out_result;

    id2986out_output[(getCycle()+6)%7] = id2986in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id63out_output;

  { // Node ID: 63 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id63in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id63in_input_doubt = id56out_o_doubt;

    id63out_output = id63in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id64out_result;

  { // Node ID: 64 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id64in_a = id2986out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id64in_b = id63out_output;

    HWOffsetFix<1,0,UNSIGNED> id64x_1;

    (id64x_1) = (and_fixed(id64in_a,id64in_b));
    id64out_result = (id64x_1);
  }
  { // Node ID: 2987 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2987in_input = id64out_result;

    id2987out_output[(getCycle()+2)%3] = id2987in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id99out_result;

  { // Node ID: 99 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id99in_a = id98out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id99in_b = id2987out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id99x_1;

    (id99x_1) = (or_fixed(id99in_a,id99in_b));
    id99out_result = (id99x_1);
  }
  { // Node ID: 3434 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2669out_result;

  { // Node ID: 2669 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2669in_a = id2884out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2669in_b = id3434out_value;

    id2669out_result = (gte_fixed(id2669in_a,id2669in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id106out_result;

  { // Node ID: 106 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id106in_a = id2987out_output[getCycle()%3];

    id106out_result = (not_fixed(id106in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id107out_result;

  { // Node ID: 107 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id107in_a = id2669out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id107in_b = id106out_result;

    HWOffsetFix<1,0,UNSIGNED> id107x_1;

    (id107x_1) = (and_fixed(id107in_a,id107in_b));
    id107out_result = (id107x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id108out_result;

  { // Node ID: 108 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id108in_a = id107out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id108in_b = id2985out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id108x_1;

    (id108x_1) = (or_fixed(id108in_a,id108in_b));
    id108out_result = (id108x_1);
  }
  HWRawBits<2> id109out_result;

  { // Node ID: 109 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id109in_in0 = id99out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id109in_in1 = id108out_result;

    id109out_result = (cat(id109in_in0,id109in_in1));
  }
  { // Node ID: 101 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id100out_o;

  { // Node ID: 100 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id100in_i = id2884out_result;

    id100out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id100in_i));
  }
  { // Node ID: 85 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id86out_result;

  { // Node ID: 86 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id86in_sel = id2666out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id86in_option0 = id82out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id86in_option1 = id85out_value;

    HWOffsetFix<24,-23,UNSIGNED> id86x_1;

    switch((id86in_sel.getValueAsLong())) {
      case 0l:
        id86x_1 = id86in_option0;
        break;
      case 1l:
        id86x_1 = id86in_option1;
        break;
      default:
        id86x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id86out_result = (id86x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id87out_o;

  { // Node ID: 87 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id87in_i = id86out_result;

    id87out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id87in_i));
  }
  HWRawBits<32> id102out_result;

  { // Node ID: 102 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id102in_in0 = id101out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id102in_in1 = id100out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id102in_in2 = id87out_o;

    id102out_result = (cat((cat(id102in_in0,id102in_in1)),id102in_in2));
  }
  HWFloat<8,24> id103out_output;

  { // Node ID: 103 (NodeReinterpret)
    const HWRawBits<32> &id103in_input = id102out_result;

    id103out_output = (cast_bits2float<8,24>(id103in_input));
  }
  { // Node ID: 110 (NodeConstantRawBits)
  }
  { // Node ID: 111 (NodeConstantRawBits)
  }
  { // Node ID: 113 (NodeConstantRawBits)
  }
  HWRawBits<32> id2670out_result;

  { // Node ID: 2670 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2670in_in0 = id110out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2670in_in1 = id111out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2670in_in2 = id113out_value;

    id2670out_result = (cat((cat(id2670in_in0,id2670in_in1)),id2670in_in2));
  }
  HWFloat<8,24> id115out_output;

  { // Node ID: 115 (NodeReinterpret)
    const HWRawBits<32> &id115in_input = id2670out_result;

    id115out_output = (cast_bits2float<8,24>(id115in_input));
  }
  { // Node ID: 2600 (NodeConstantRawBits)
  }
  HWFloat<8,24> id118out_result;

  { // Node ID: 118 (NodeMux)
    const HWRawBits<2> &id118in_sel = id109out_result;
    const HWFloat<8,24> &id118in_option0 = id103out_output;
    const HWFloat<8,24> &id118in_option1 = id115out_output;
    const HWFloat<8,24> &id118in_option2 = id2600out_value;
    const HWFloat<8,24> &id118in_option3 = id115out_output;

    HWFloat<8,24> id118x_1;

    switch((id118in_sel.getValueAsLong())) {
      case 0l:
        id118x_1 = id118in_option0;
        break;
      case 1l:
        id118x_1 = id118in_option1;
        break;
      case 2l:
        id118x_1 = id118in_option2;
        break;
      case 3l:
        id118x_1 = id118in_option3;
        break;
      default:
        id118x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id118out_result = (id118x_1);
  }
  { // Node ID: 3433 (NodeConstantRawBits)
  }
  HWFloat<8,24> id128out_result;

  { // Node ID: 128 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id128in_sel = id2990out_output[getCycle()%9];
    const HWFloat<8,24> &id128in_option0 = id118out_result;
    const HWFloat<8,24> &id128in_option1 = id3433out_value;

    HWFloat<8,24> id128x_1;

    switch((id128in_sel.getValueAsLong())) {
      case 0l:
        id128x_1 = id128in_option0;
        break;
      case 1l:
        id128x_1 = id128in_option1;
        break;
      default:
        id128x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id128out_result = (id128x_1);
  }
  HWFloat<8,24> id136out_result;

  { // Node ID: 136 (NodeMul)
    const HWFloat<8,24> &id136in_a = id3441out_value;
    const HWFloat<8,24> &id136in_b = id128out_result;

    id136out_result = (mul_float(id136in_a,id136in_b));
  }
  { // Node ID: 3432 (NodeConstantRawBits)
  }
  { // Node ID: 3431 (NodeConstantRawBits)
  }
  HWFloat<8,24> id138out_result;

  { // Node ID: 138 (NodeAdd)
    const HWFloat<8,24> &id138in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id138in_b = id3431out_value;

    id138out_result = (add_float(id138in_a,id138in_b));
  }
  HWFloat<8,24> id140out_result;

  { // Node ID: 140 (NodeMul)
    const HWFloat<8,24> &id140in_a = id3432out_value;
    const HWFloat<8,24> &id140in_b = id138out_result;

    id140out_result = (mul_float(id140in_a,id140in_b));
  }
  HWRawBits<8> id212out_result;

  { // Node ID: 212 (NodeSlice)
    const HWFloat<8,24> &id212in_a = id140out_result;

    id212out_result = (slice<23,8>(id212in_a));
  }
  { // Node ID: 213 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2671out_result;

  { // Node ID: 2671 (NodeEqInlined)
    const HWRawBits<8> &id2671in_a = id212out_result;
    const HWRawBits<8> &id2671in_b = id213out_value;

    id2671out_result = (eq_bits(id2671in_a,id2671in_b));
  }
  HWRawBits<23> id211out_result;

  { // Node ID: 211 (NodeSlice)
    const HWFloat<8,24> &id211in_a = id140out_result;

    id211out_result = (slice<0,23>(id211in_a));
  }
  { // Node ID: 3430 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2672out_result;

  { // Node ID: 2672 (NodeNeqInlined)
    const HWRawBits<23> &id2672in_a = id211out_result;
    const HWRawBits<23> &id2672in_b = id3430out_value;

    id2672out_result = (neq_bits(id2672in_a,id2672in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id217out_result;

  { // Node ID: 217 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id217in_a = id2671out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id217in_b = id2672out_result;

    HWOffsetFix<1,0,UNSIGNED> id217x_1;

    (id217x_1) = (and_fixed(id217in_a,id217in_b));
    id217out_result = (id217x_1);
  }
  { // Node ID: 3000 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3000in_input = id217out_result;

    id3000out_output[(getCycle()+8)%9] = id3000in_input;
  }
  { // Node ID: 141 (NodeConstantRawBits)
  }
  HWFloat<8,24> id142out_output;
  HWOffsetFix<1,0,UNSIGNED> id142out_output_doubt;

  { // Node ID: 142 (NodeDoubtBitOp)
    const HWFloat<8,24> &id142in_input = id140out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id142in_doubt = id141out_value;

    id142out_output = id142in_input;
    id142out_output_doubt = id142in_doubt;
  }
  { // Node ID: 143 (NodeCast)
    const HWFloat<8,24> &id143in_i = id142out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id143in_i_doubt = id142out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id143x_1;

    id143out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id143in_i,(&(id143x_1))));
    id143out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id143x_1),(c_hw_fix_4_0_uns_bits))),id143in_i_doubt));
  }
  { // Node ID: 146 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id145out_result;
  HWOffsetFix<1,0,UNSIGNED> id145out_result_doubt;

  { // Node ID: 145 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id145in_a = id143out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id145in_a_doubt = id143out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id145in_b = id146out_value;

    HWOffsetFix<1,0,UNSIGNED> id145x_1;

    id145out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id145in_a,id145in_b,(&(id145x_1))));
    id145out_result_doubt = (or_fixed((neq_fixed((id145x_1),(c_hw_fix_1_0_uns_bits_1))),id145in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id147out_o;
  HWOffsetFix<1,0,UNSIGNED> id147out_o_doubt;

  { // Node ID: 147 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id147in_i = id145out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id147in_i_doubt = id145out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id147x_1;

    id147out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id147in_i,(&(id147x_1))));
    id147out_o_doubt = (or_fixed((neq_fixed((id147x_1),(c_hw_fix_1_0_uns_bits_1))),id147in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id156out_output;

  { // Node ID: 156 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id156in_input = id147out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id156in_input_doubt = id147out_o_doubt;

    id156out_output = id156in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id157out_o;

  { // Node ID: 157 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id157in_i = id156out_output;

    id157out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id157in_i));
  }
  { // Node ID: 2991 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2991in_input = id157out_o;

    id2991out_output[(getCycle()+2)%3] = id2991in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id179out_o;

  { // Node ID: 179 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id179in_i = id2991out_output[getCycle()%3];

    id179out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id179in_i));
  }
  { // Node ID: 182 (NodeConstantRawBits)
  }
  { // Node ID: 2828 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id159out_o;

  { // Node ID: 159 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id159in_i = id156out_output;

    id159out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id159in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id224out_output;

  { // Node ID: 224 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id224in_input = id159out_o;

    id224out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id224in_input))));
  }
  { // Node ID: 225 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id225in_addr = id224out_output;

    HWOffsetFix<22,-24,UNSIGNED> id225x_1;

    switch(((long)((id225in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id225x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id225x_1 = (id225sta_rom_store[(id225in_addr.getValueAsLong())]);
        break;
      default:
        id225x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id225out_dout[(getCycle()+2)%3] = (id225x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id158out_o;

  { // Node ID: 158 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id158in_i = id156out_output;

    id158out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id158in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id221out_output;

  { // Node ID: 221 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id221in_input = id158out_o;

    id221out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id221in_input))));
  }
  { // Node ID: 222 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id222in_addr = id221out_output;

    HWOffsetFix<24,-24,UNSIGNED> id222x_1;

    switch(((long)((id222in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id222x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id222x_1 = (id222sta_rom_store[(id222in_addr.getValueAsLong())]);
        break;
      default:
        id222x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id222out_dout[(getCycle()+2)%3] = (id222x_1);
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id160out_o;

  { // Node ID: 160 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id160in_i = id156out_output;

    id160out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id160in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id162out_result;

  { // Node ID: 162 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id162in_a = id163out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id162in_b = id160out_o;

    id162out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id162in_a,id162in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id164out_o;

  { // Node ID: 164 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id164in_i = id162out_result;

    id164out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id164in_i));
  }
  { // Node ID: 2992 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id2992in_input = id164out_o;

    id2992out_output[(getCycle()+2)%3] = id2992in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id165out_result;

  { // Node ID: 165 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id165in_a = id222out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id165in_b = id2992out_output[getCycle()%3];

    id165out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id165in_a,id165in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id166out_result;

  { // Node ID: 166 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id166in_a = id2992out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id166in_b = id222out_dout[getCycle()%3];

    id166out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id166in_a,id166in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id167out_result;

  { // Node ID: 167 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id167in_a = id165out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id167in_b = id166out_result;

    id167out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id167in_a,id167in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id168out_o;

  { // Node ID: 168 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id168in_i = id167out_result;

    id168out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id168in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id169out_result;

  { // Node ID: 169 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id169in_a = id225out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id169in_b = id168out_o;

    id169out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id169in_a,id169in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id170out_result;

  { // Node ID: 170 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id170in_a = id168out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id170in_b = id225out_dout[getCycle()%3];

    id170out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id170in_a,id170in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id171out_result;

  { // Node ID: 171 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id171in_a = id169out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id171in_b = id170out_result;

    id171out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id171in_a,id171in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id172out_o;

  { // Node ID: 172 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id172in_i = id171out_result;

    id172out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id172in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id173out_o;

  { // Node ID: 173 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id173in_i = id172out_o;

    id173out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id173in_i));
  }
  { // Node ID: 3429 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2673out_result;

  { // Node ID: 2673 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2673in_a = id173out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2673in_b = id3429out_value;

    id2673out_result = (gte_fixed(id2673in_a,id2673in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2883out_result;

  { // Node ID: 2883 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2883in_a = id179out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2883in_b = id182out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2883in_c = id2828out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2883in_condb = id2673out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2883x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2883x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2883x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2883x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2883x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2883x_1 = id2883in_a;
        break;
      default:
        id2883x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2883in_condb.getValueAsLong())) {
      case 0l:
        id2883x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2883x_2 = id2883in_b;
        break;
      default:
        id2883x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2883x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2883x_3 = id2883in_c;
        break;
      default:
        id2883x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2883x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2883x_1),(id2883x_2))),(id2883x_3)));
    id2883out_result = (id2883x_4);
  }
  HWRawBits<1> id2674out_result;

  { // Node ID: 2674 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2674in_a = id2883out_result;

    id2674out_result = (slice<10,1>(id2674in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2675out_output;

  { // Node ID: 2675 (NodeReinterpret)
    const HWRawBits<1> &id2675in_input = id2674out_result;

    id2675out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2675in_input));
  }
  { // Node ID: 3428 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id149out_result;

  { // Node ID: 149 (NodeGt)
    const HWFloat<8,24> &id149in_a = id140out_result;
    const HWFloat<8,24> &id149in_b = id3428out_value;

    id149out_result = (gt_float(id149in_a,id149in_b));
  }
  { // Node ID: 2994 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2994in_input = id149out_result;

    id2994out_output[(getCycle()+6)%7] = id2994in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id150out_output;

  { // Node ID: 150 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id150in_input = id147out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id150in_input_doubt = id147out_o_doubt;

    id150out_output = id150in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id151out_result;

  { // Node ID: 151 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id151in_a = id2994out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id151in_b = id150out_output;

    HWOffsetFix<1,0,UNSIGNED> id151x_1;

    (id151x_1) = (and_fixed(id151in_a,id151in_b));
    id151out_result = (id151x_1);
  }
  { // Node ID: 2995 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2995in_input = id151out_result;

    id2995out_output[(getCycle()+2)%3] = id2995in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id188out_result;

  { // Node ID: 188 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id188in_a = id2995out_output[getCycle()%3];

    id188out_result = (not_fixed(id188in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id189out_result;

  { // Node ID: 189 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id189in_a = id2675out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id189in_b = id188out_result;

    HWOffsetFix<1,0,UNSIGNED> id189x_1;

    (id189x_1) = (and_fixed(id189in_a,id189in_b));
    id189out_result = (id189x_1);
  }
  { // Node ID: 3427 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id153out_result;

  { // Node ID: 153 (NodeLt)
    const HWFloat<8,24> &id153in_a = id140out_result;
    const HWFloat<8,24> &id153in_b = id3427out_value;

    id153out_result = (lt_float(id153in_a,id153in_b));
  }
  { // Node ID: 2996 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2996in_input = id153out_result;

    id2996out_output[(getCycle()+6)%7] = id2996in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id154out_output;

  { // Node ID: 154 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id154in_input = id147out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id154in_input_doubt = id147out_o_doubt;

    id154out_output = id154in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id155out_result;

  { // Node ID: 155 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id155in_a = id2996out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id155in_b = id154out_output;

    HWOffsetFix<1,0,UNSIGNED> id155x_1;

    (id155x_1) = (and_fixed(id155in_a,id155in_b));
    id155out_result = (id155x_1);
  }
  { // Node ID: 2997 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2997in_input = id155out_result;

    id2997out_output[(getCycle()+2)%3] = id2997in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id190out_result;

  { // Node ID: 190 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id190in_a = id189out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id190in_b = id2997out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id190x_1;

    (id190x_1) = (or_fixed(id190in_a,id190in_b));
    id190out_result = (id190x_1);
  }
  { // Node ID: 3426 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2676out_result;

  { // Node ID: 2676 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2676in_a = id2883out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2676in_b = id3426out_value;

    id2676out_result = (gte_fixed(id2676in_a,id2676in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id197out_result;

  { // Node ID: 197 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id197in_a = id2997out_output[getCycle()%3];

    id197out_result = (not_fixed(id197in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id198out_result;

  { // Node ID: 198 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id198in_a = id2676out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id198in_b = id197out_result;

    HWOffsetFix<1,0,UNSIGNED> id198x_1;

    (id198x_1) = (and_fixed(id198in_a,id198in_b));
    id198out_result = (id198x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id199out_result;

  { // Node ID: 199 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id199in_a = id198out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id199in_b = id2995out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id199x_1;

    (id199x_1) = (or_fixed(id199in_a,id199in_b));
    id199out_result = (id199x_1);
  }
  HWRawBits<2> id200out_result;

  { // Node ID: 200 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id200in_in0 = id190out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id200in_in1 = id199out_result;

    id200out_result = (cat(id200in_in0,id200in_in1));
  }
  { // Node ID: 192 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id191out_o;

  { // Node ID: 191 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id191in_i = id2883out_result;

    id191out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id191in_i));
  }
  { // Node ID: 176 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id177out_result;

  { // Node ID: 177 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id177in_sel = id2673out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id177in_option0 = id173out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id177in_option1 = id176out_value;

    HWOffsetFix<24,-23,UNSIGNED> id177x_1;

    switch((id177in_sel.getValueAsLong())) {
      case 0l:
        id177x_1 = id177in_option0;
        break;
      case 1l:
        id177x_1 = id177in_option1;
        break;
      default:
        id177x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id177out_result = (id177x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id178out_o;

  { // Node ID: 178 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id178in_i = id177out_result;

    id178out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id178in_i));
  }
  HWRawBits<32> id193out_result;

  { // Node ID: 193 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id193in_in0 = id192out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id193in_in1 = id191out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id193in_in2 = id178out_o;

    id193out_result = (cat((cat(id193in_in0,id193in_in1)),id193in_in2));
  }
  HWFloat<8,24> id194out_output;

  { // Node ID: 194 (NodeReinterpret)
    const HWRawBits<32> &id194in_input = id193out_result;

    id194out_output = (cast_bits2float<8,24>(id194in_input));
  }
  { // Node ID: 201 (NodeConstantRawBits)
  }
  { // Node ID: 202 (NodeConstantRawBits)
  }
  { // Node ID: 204 (NodeConstantRawBits)
  }
  HWRawBits<32> id2677out_result;

  { // Node ID: 2677 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2677in_in0 = id201out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2677in_in1 = id202out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2677in_in2 = id204out_value;

    id2677out_result = (cat((cat(id2677in_in0,id2677in_in1)),id2677in_in2));
  }
  HWFloat<8,24> id206out_output;

  { // Node ID: 206 (NodeReinterpret)
    const HWRawBits<32> &id206in_input = id2677out_result;

    id206out_output = (cast_bits2float<8,24>(id206in_input));
  }
  { // Node ID: 2601 (NodeConstantRawBits)
  }
  HWFloat<8,24> id209out_result;

  { // Node ID: 209 (NodeMux)
    const HWRawBits<2> &id209in_sel = id200out_result;
    const HWFloat<8,24> &id209in_option0 = id194out_output;
    const HWFloat<8,24> &id209in_option1 = id206out_output;
    const HWFloat<8,24> &id209in_option2 = id2601out_value;
    const HWFloat<8,24> &id209in_option3 = id206out_output;

    HWFloat<8,24> id209x_1;

    switch((id209in_sel.getValueAsLong())) {
      case 0l:
        id209x_1 = id209in_option0;
        break;
      case 1l:
        id209x_1 = id209in_option1;
        break;
      case 2l:
        id209x_1 = id209in_option2;
        break;
      case 3l:
        id209x_1 = id209in_option3;
        break;
      default:
        id209x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id209out_result = (id209x_1);
  }
  { // Node ID: 3425 (NodeConstantRawBits)
  }
  HWFloat<8,24> id219out_result;

  { // Node ID: 219 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id219in_sel = id3000out_output[getCycle()%9];
    const HWFloat<8,24> &id219in_option0 = id209out_result;
    const HWFloat<8,24> &id219in_option1 = id3425out_value;

    HWFloat<8,24> id219x_1;

    switch((id219in_sel.getValueAsLong())) {
      case 0l:
        id219x_1 = id219in_option0;
        break;
      case 1l:
        id219x_1 = id219in_option1;
        break;
      default:
        id219x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id219out_result = (id219x_1);
  }
  { // Node ID: 3424 (NodeConstantRawBits)
  }
  HWFloat<8,24> id227out_result;

  { // Node ID: 227 (NodeAdd)
    const HWFloat<8,24> &id227in_a = id219out_result;
    const HWFloat<8,24> &id227in_b = id3424out_value;

    id227out_result = (add_float(id227in_a,id227in_b));
  }
  HWFloat<8,24> id228out_result;

  { // Node ID: 228 (NodeDiv)
    const HWFloat<8,24> &id228in_a = id136out_result;
    const HWFloat<8,24> &id228in_b = id227out_result;

    id228out_result = (div_float(id228in_a,id228in_b));
  }
  HWFloat<8,24> id229out_result;

  { // Node ID: 229 (NodeMul)
    const HWFloat<8,24> &id229in_a = id12out_dt;
    const HWFloat<8,24> &id229in_b = id228out_result;

    id229out_result = (mul_float(id229in_a,id229in_b));
  }
  { // Node ID: 3423 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1803out_result;

  { // Node ID: 1803 (NodeSub)
    const HWFloat<8,24> &id1803in_a = id3423out_value;
    const HWFloat<8,24> &id1803in_b = id3001out_output[getCycle()%9];

    id1803out_result = (sub_float(id1803in_a,id1803in_b));
  }
  HWFloat<8,24> id1804out_result;

  { // Node ID: 1804 (NodeMul)
    const HWFloat<8,24> &id1804in_a = id229out_result;
    const HWFloat<8,24> &id1804in_b = id1803out_result;

    id1804out_result = (mul_float(id1804in_a,id1804in_b));
  }
  { // Node ID: 3422 (NodeConstantRawBits)
  }
  { // Node ID: 3421 (NodeConstantRawBits)
  }
  { // Node ID: 3420 (NodeConstantRawBits)
  }
  HWFloat<8,24> id231out_result;

  { // Node ID: 231 (NodeAdd)
    const HWFloat<8,24> &id231in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id231in_b = id3420out_value;

    id231out_result = (add_float(id231in_a,id231in_b));
  }
  HWFloat<8,24> id233out_result;

  { // Node ID: 233 (NodeMul)
    const HWFloat<8,24> &id233in_a = id3421out_value;
    const HWFloat<8,24> &id233in_b = id231out_result;

    id233out_result = (mul_float(id233in_a,id233in_b));
  }
  HWRawBits<8> id305out_result;

  { // Node ID: 305 (NodeSlice)
    const HWFloat<8,24> &id305in_a = id233out_result;

    id305out_result = (slice<23,8>(id305in_a));
  }
  { // Node ID: 306 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2678out_result;

  { // Node ID: 2678 (NodeEqInlined)
    const HWRawBits<8> &id2678in_a = id305out_result;
    const HWRawBits<8> &id2678in_b = id306out_value;

    id2678out_result = (eq_bits(id2678in_a,id2678in_b));
  }
  HWRawBits<23> id304out_result;

  { // Node ID: 304 (NodeSlice)
    const HWFloat<8,24> &id304in_a = id233out_result;

    id304out_result = (slice<0,23>(id304in_a));
  }
  { // Node ID: 3419 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2679out_result;

  { // Node ID: 2679 (NodeNeqInlined)
    const HWRawBits<23> &id2679in_a = id304out_result;
    const HWRawBits<23> &id2679in_b = id3419out_value;

    id2679out_result = (neq_bits(id2679in_a,id2679in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id310out_result;

  { // Node ID: 310 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id310in_a = id2678out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_b = id2679out_result;

    HWOffsetFix<1,0,UNSIGNED> id310x_1;

    (id310x_1) = (and_fixed(id310in_a,id310in_b));
    id310out_result = (id310x_1);
  }
  { // Node ID: 3011 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3011in_input = id310out_result;

    id3011out_output[(getCycle()+8)%9] = id3011in_input;
  }
  { // Node ID: 234 (NodeConstantRawBits)
  }
  HWFloat<8,24> id235out_output;
  HWOffsetFix<1,0,UNSIGNED> id235out_output_doubt;

  { // Node ID: 235 (NodeDoubtBitOp)
    const HWFloat<8,24> &id235in_input = id233out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id235in_doubt = id234out_value;

    id235out_output = id235in_input;
    id235out_output_doubt = id235in_doubt;
  }
  { // Node ID: 236 (NodeCast)
    const HWFloat<8,24> &id236in_i = id235out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id236in_i_doubt = id235out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id236x_1;

    id236out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id236in_i,(&(id236x_1))));
    id236out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id236x_1),(c_hw_fix_4_0_uns_bits))),id236in_i_doubt));
  }
  { // Node ID: 239 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id238out_result;
  HWOffsetFix<1,0,UNSIGNED> id238out_result_doubt;

  { // Node ID: 238 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id238in_a = id236out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id238in_a_doubt = id236out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id238in_b = id239out_value;

    HWOffsetFix<1,0,UNSIGNED> id238x_1;

    id238out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id238in_a,id238in_b,(&(id238x_1))));
    id238out_result_doubt = (or_fixed((neq_fixed((id238x_1),(c_hw_fix_1_0_uns_bits_1))),id238in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id240out_o;
  HWOffsetFix<1,0,UNSIGNED> id240out_o_doubt;

  { // Node ID: 240 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id240in_i = id238out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id240in_i_doubt = id238out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id240x_1;

    id240out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id240in_i,(&(id240x_1))));
    id240out_o_doubt = (or_fixed((neq_fixed((id240x_1),(c_hw_fix_1_0_uns_bits_1))),id240in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id249out_output;

  { // Node ID: 249 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id249in_input = id240out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id249in_input_doubt = id240out_o_doubt;

    id249out_output = id249in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id250out_o;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id250in_i = id249out_output;

    id250out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id250in_i));
  }
  { // Node ID: 3002 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3002in_input = id250out_o;

    id3002out_output[(getCycle()+2)%3] = id3002in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id272out_o;

  { // Node ID: 272 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id272in_i = id3002out_output[getCycle()%3];

    id272out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id272in_i));
  }
  { // Node ID: 275 (NodeConstantRawBits)
  }
  { // Node ID: 2830 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id252out_o;

  { // Node ID: 252 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id252in_i = id249out_output;

    id252out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id252in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id317out_output;

  { // Node ID: 317 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id317in_input = id252out_o;

    id317out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id317in_input))));
  }
  { // Node ID: 318 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id318in_addr = id317out_output;

    HWOffsetFix<22,-24,UNSIGNED> id318x_1;

    switch(((long)((id318in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id318x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id318x_1 = (id318sta_rom_store[(id318in_addr.getValueAsLong())]);
        break;
      default:
        id318x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id318out_dout[(getCycle()+2)%3] = (id318x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id251out_o;

  { // Node ID: 251 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id251in_i = id249out_output;

    id251out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id251in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id314out_output;

  { // Node ID: 314 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id314in_input = id251out_o;

    id314out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id314in_input))));
  }
  { // Node ID: 315 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id315in_addr = id314out_output;

    HWOffsetFix<24,-24,UNSIGNED> id315x_1;

    switch(((long)((id315in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id315x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id315x_1 = (id315sta_rom_store[(id315in_addr.getValueAsLong())]);
        break;
      default:
        id315x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id315out_dout[(getCycle()+2)%3] = (id315x_1);
  }
  { // Node ID: 256 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id253out_o;

  { // Node ID: 253 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id253in_i = id249out_output;

    id253out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id253in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id255out_result;

  { // Node ID: 255 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id255in_a = id256out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id255in_b = id253out_o;

    id255out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id255in_a,id255in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id257out_o;

  { // Node ID: 257 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id257in_i = id255out_result;

    id257out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id257in_i));
  }
  { // Node ID: 3003 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3003in_input = id257out_o;

    id3003out_output[(getCycle()+2)%3] = id3003in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id258out_result;

  { // Node ID: 258 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id258in_a = id315out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id258in_b = id3003out_output[getCycle()%3];

    id258out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id258in_a,id258in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id259out_result;

  { // Node ID: 259 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id259in_a = id3003out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id259in_b = id315out_dout[getCycle()%3];

    id259out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id259in_a,id259in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id260out_result;

  { // Node ID: 260 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id260in_a = id258out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id260in_b = id259out_result;

    id260out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id260in_a,id260in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id261out_o;

  { // Node ID: 261 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id261in_i = id260out_result;

    id261out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id261in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id262out_result;

  { // Node ID: 262 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id262in_a = id318out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id262in_b = id261out_o;

    id262out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id262in_a,id262in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id263out_result;

  { // Node ID: 263 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id263in_a = id261out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id263in_b = id318out_dout[getCycle()%3];

    id263out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id263in_a,id263in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id264out_result;

  { // Node ID: 264 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id264in_a = id262out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id264in_b = id263out_result;

    id264out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id264in_a,id264in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id265out_o;

  { // Node ID: 265 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id265in_i = id264out_result;

    id265out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id265in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id266out_o;

  { // Node ID: 266 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id266in_i = id265out_o;

    id266out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id266in_i));
  }
  { // Node ID: 3418 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2680out_result;

  { // Node ID: 2680 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2680in_a = id266out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2680in_b = id3418out_value;

    id2680out_result = (gte_fixed(id2680in_a,id2680in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2882out_result;

  { // Node ID: 2882 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2882in_a = id272out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2882in_b = id275out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2882in_c = id2830out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2882in_condb = id2680out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2882x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2882x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2882x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2882x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2882x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2882x_1 = id2882in_a;
        break;
      default:
        id2882x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2882in_condb.getValueAsLong())) {
      case 0l:
        id2882x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2882x_2 = id2882in_b;
        break;
      default:
        id2882x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2882x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2882x_3 = id2882in_c;
        break;
      default:
        id2882x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2882x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2882x_1),(id2882x_2))),(id2882x_3)));
    id2882out_result = (id2882x_4);
  }
  HWRawBits<1> id2681out_result;

  { // Node ID: 2681 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2681in_a = id2882out_result;

    id2681out_result = (slice<10,1>(id2681in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2682out_output;

  { // Node ID: 2682 (NodeReinterpret)
    const HWRawBits<1> &id2682in_input = id2681out_result;

    id2682out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2682in_input));
  }
  { // Node ID: 3417 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id242out_result;

  { // Node ID: 242 (NodeGt)
    const HWFloat<8,24> &id242in_a = id233out_result;
    const HWFloat<8,24> &id242in_b = id3417out_value;

    id242out_result = (gt_float(id242in_a,id242in_b));
  }
  { // Node ID: 3005 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3005in_input = id242out_result;

    id3005out_output[(getCycle()+6)%7] = id3005in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id243out_output;

  { // Node ID: 243 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id243in_input = id240out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id243in_input_doubt = id240out_o_doubt;

    id243out_output = id243in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id244out_result;

  { // Node ID: 244 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id244in_a = id3005out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id244in_b = id243out_output;

    HWOffsetFix<1,0,UNSIGNED> id244x_1;

    (id244x_1) = (and_fixed(id244in_a,id244in_b));
    id244out_result = (id244x_1);
  }
  { // Node ID: 3006 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3006in_input = id244out_result;

    id3006out_output[(getCycle()+2)%3] = id3006in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id281out_result;

  { // Node ID: 281 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id281in_a = id3006out_output[getCycle()%3];

    id281out_result = (not_fixed(id281in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id282out_result;

  { // Node ID: 282 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id282in_a = id2682out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id282in_b = id281out_result;

    HWOffsetFix<1,0,UNSIGNED> id282x_1;

    (id282x_1) = (and_fixed(id282in_a,id282in_b));
    id282out_result = (id282x_1);
  }
  { // Node ID: 3416 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id246out_result;

  { // Node ID: 246 (NodeLt)
    const HWFloat<8,24> &id246in_a = id233out_result;
    const HWFloat<8,24> &id246in_b = id3416out_value;

    id246out_result = (lt_float(id246in_a,id246in_b));
  }
  { // Node ID: 3007 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3007in_input = id246out_result;

    id3007out_output[(getCycle()+6)%7] = id3007in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id247out_output;

  { // Node ID: 247 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id247in_input = id240out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id247in_input_doubt = id240out_o_doubt;

    id247out_output = id247in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id248out_result;

  { // Node ID: 248 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id248in_a = id3007out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id248in_b = id247out_output;

    HWOffsetFix<1,0,UNSIGNED> id248x_1;

    (id248x_1) = (and_fixed(id248in_a,id248in_b));
    id248out_result = (id248x_1);
  }
  { // Node ID: 3008 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3008in_input = id248out_result;

    id3008out_output[(getCycle()+2)%3] = id3008in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id283in_a = id282out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id283in_b = id3008out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id283x_1;

    (id283x_1) = (or_fixed(id283in_a,id283in_b));
    id283out_result = (id283x_1);
  }
  { // Node ID: 3415 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2683out_result;

  { // Node ID: 2683 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2683in_a = id2882out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2683in_b = id3415out_value;

    id2683out_result = (gte_fixed(id2683in_a,id2683in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id290out_result;

  { // Node ID: 290 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id290in_a = id3008out_output[getCycle()%3];

    id290out_result = (not_fixed(id290in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id291out_result;

  { // Node ID: 291 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id291in_a = id2683out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id291in_b = id290out_result;

    HWOffsetFix<1,0,UNSIGNED> id291x_1;

    (id291x_1) = (and_fixed(id291in_a,id291in_b));
    id291out_result = (id291x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id292out_result;

  { // Node ID: 292 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id292in_a = id291out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id292in_b = id3006out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id292x_1;

    (id292x_1) = (or_fixed(id292in_a,id292in_b));
    id292out_result = (id292x_1);
  }
  HWRawBits<2> id293out_result;

  { // Node ID: 293 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id293in_in0 = id283out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id293in_in1 = id292out_result;

    id293out_result = (cat(id293in_in0,id293in_in1));
  }
  { // Node ID: 285 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id284out_o;

  { // Node ID: 284 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id284in_i = id2882out_result;

    id284out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id284in_i));
  }
  { // Node ID: 269 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id270out_result;

  { // Node ID: 270 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id270in_sel = id2680out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id270in_option0 = id266out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id270in_option1 = id269out_value;

    HWOffsetFix<24,-23,UNSIGNED> id270x_1;

    switch((id270in_sel.getValueAsLong())) {
      case 0l:
        id270x_1 = id270in_option0;
        break;
      case 1l:
        id270x_1 = id270in_option1;
        break;
      default:
        id270x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id270out_result = (id270x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id271out_o;

  { // Node ID: 271 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id271in_i = id270out_result;

    id271out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id271in_i));
  }
  HWRawBits<32> id286out_result;

  { // Node ID: 286 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id286in_in0 = id285out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id286in_in1 = id284out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id286in_in2 = id271out_o;

    id286out_result = (cat((cat(id286in_in0,id286in_in1)),id286in_in2));
  }
  HWFloat<8,24> id287out_output;

  { // Node ID: 287 (NodeReinterpret)
    const HWRawBits<32> &id287in_input = id286out_result;

    id287out_output = (cast_bits2float<8,24>(id287in_input));
  }
  { // Node ID: 294 (NodeConstantRawBits)
  }
  { // Node ID: 295 (NodeConstantRawBits)
  }
  { // Node ID: 297 (NodeConstantRawBits)
  }
  HWRawBits<32> id2684out_result;

  { // Node ID: 2684 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2684in_in0 = id294out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2684in_in1 = id295out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2684in_in2 = id297out_value;

    id2684out_result = (cat((cat(id2684in_in0,id2684in_in1)),id2684in_in2));
  }
  HWFloat<8,24> id299out_output;

  { // Node ID: 299 (NodeReinterpret)
    const HWRawBits<32> &id299in_input = id2684out_result;

    id299out_output = (cast_bits2float<8,24>(id299in_input));
  }
  { // Node ID: 2602 (NodeConstantRawBits)
  }
  HWFloat<8,24> id302out_result;

  { // Node ID: 302 (NodeMux)
    const HWRawBits<2> &id302in_sel = id293out_result;
    const HWFloat<8,24> &id302in_option0 = id287out_output;
    const HWFloat<8,24> &id302in_option1 = id299out_output;
    const HWFloat<8,24> &id302in_option2 = id2602out_value;
    const HWFloat<8,24> &id302in_option3 = id299out_output;

    HWFloat<8,24> id302x_1;

    switch((id302in_sel.getValueAsLong())) {
      case 0l:
        id302x_1 = id302in_option0;
        break;
      case 1l:
        id302x_1 = id302in_option1;
        break;
      case 2l:
        id302x_1 = id302in_option2;
        break;
      case 3l:
        id302x_1 = id302in_option3;
        break;
      default:
        id302x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id302out_result = (id302x_1);
  }
  { // Node ID: 3414 (NodeConstantRawBits)
  }
  HWFloat<8,24> id312out_result;

  { // Node ID: 312 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id312in_sel = id3011out_output[getCycle()%9];
    const HWFloat<8,24> &id312in_option0 = id302out_result;
    const HWFloat<8,24> &id312in_option1 = id3414out_value;

    HWFloat<8,24> id312x_1;

    switch((id312in_sel.getValueAsLong())) {
      case 0l:
        id312x_1 = id312in_option0;
        break;
      case 1l:
        id312x_1 = id312in_option1;
        break;
      default:
        id312x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id312out_result = (id312x_1);
  }
  HWFloat<8,24> id320out_result;

  { // Node ID: 320 (NodeMul)
    const HWFloat<8,24> &id320in_a = id3422out_value;
    const HWFloat<8,24> &id320in_b = id312out_result;

    id320out_result = (mul_float(id320in_a,id320in_b));
  }
  { // Node ID: 3413 (NodeConstantRawBits)
  }
  { // Node ID: 3412 (NodeConstantRawBits)
  }
  HWFloat<8,24> id322out_result;

  { // Node ID: 322 (NodeAdd)
    const HWFloat<8,24> &id322in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id322in_b = id3412out_value;

    id322out_result = (add_float(id322in_a,id322in_b));
  }
  HWFloat<8,24> id324out_result;

  { // Node ID: 324 (NodeMul)
    const HWFloat<8,24> &id324in_a = id3413out_value;
    const HWFloat<8,24> &id324in_b = id322out_result;

    id324out_result = (mul_float(id324in_a,id324in_b));
  }
  HWRawBits<8> id396out_result;

  { // Node ID: 396 (NodeSlice)
    const HWFloat<8,24> &id396in_a = id324out_result;

    id396out_result = (slice<23,8>(id396in_a));
  }
  { // Node ID: 397 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2685out_result;

  { // Node ID: 2685 (NodeEqInlined)
    const HWRawBits<8> &id2685in_a = id396out_result;
    const HWRawBits<8> &id2685in_b = id397out_value;

    id2685out_result = (eq_bits(id2685in_a,id2685in_b));
  }
  HWRawBits<23> id395out_result;

  { // Node ID: 395 (NodeSlice)
    const HWFloat<8,24> &id395in_a = id324out_result;

    id395out_result = (slice<0,23>(id395in_a));
  }
  { // Node ID: 3411 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2686out_result;

  { // Node ID: 2686 (NodeNeqInlined)
    const HWRawBits<23> &id2686in_a = id395out_result;
    const HWRawBits<23> &id2686in_b = id3411out_value;

    id2686out_result = (neq_bits(id2686in_a,id2686in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_a = id2685out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id401in_b = id2686out_result;

    HWOffsetFix<1,0,UNSIGNED> id401x_1;

    (id401x_1) = (and_fixed(id401in_a,id401in_b));
    id401out_result = (id401x_1);
  }
  { // Node ID: 3021 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3021in_input = id401out_result;

    id3021out_output[(getCycle()+8)%9] = id3021in_input;
  }
  { // Node ID: 325 (NodeConstantRawBits)
  }
  HWFloat<8,24> id326out_output;
  HWOffsetFix<1,0,UNSIGNED> id326out_output_doubt;

  { // Node ID: 326 (NodeDoubtBitOp)
    const HWFloat<8,24> &id326in_input = id324out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id326in_doubt = id325out_value;

    id326out_output = id326in_input;
    id326out_output_doubt = id326in_doubt;
  }
  { // Node ID: 327 (NodeCast)
    const HWFloat<8,24> &id327in_i = id326out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id327in_i_doubt = id326out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id327x_1;

    id327out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id327in_i,(&(id327x_1))));
    id327out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id327x_1),(c_hw_fix_4_0_uns_bits))),id327in_i_doubt));
  }
  { // Node ID: 330 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id329out_result;
  HWOffsetFix<1,0,UNSIGNED> id329out_result_doubt;

  { // Node ID: 329 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id329in_a = id327out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id329in_a_doubt = id327out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id329in_b = id330out_value;

    HWOffsetFix<1,0,UNSIGNED> id329x_1;

    id329out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id329in_a,id329in_b,(&(id329x_1))));
    id329out_result_doubt = (or_fixed((neq_fixed((id329x_1),(c_hw_fix_1_0_uns_bits_1))),id329in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id331out_o;
  HWOffsetFix<1,0,UNSIGNED> id331out_o_doubt;

  { // Node ID: 331 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id331in_i = id329out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id331in_i_doubt = id329out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id331x_1;

    id331out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id331in_i,(&(id331x_1))));
    id331out_o_doubt = (or_fixed((neq_fixed((id331x_1),(c_hw_fix_1_0_uns_bits_1))),id331in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id340out_output;

  { // Node ID: 340 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id340in_input = id331out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id340in_input_doubt = id331out_o_doubt;

    id340out_output = id340in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id341out_o;

  { // Node ID: 341 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id341in_i = id340out_output;

    id341out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id341in_i));
  }
  { // Node ID: 3012 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3012in_input = id341out_o;

    id3012out_output[(getCycle()+2)%3] = id3012in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id363out_o;

  { // Node ID: 363 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id363in_i = id3012out_output[getCycle()%3];

    id363out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id363in_i));
  }
  { // Node ID: 366 (NodeConstantRawBits)
  }
  { // Node ID: 2832 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id343out_o;

  { // Node ID: 343 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id343in_i = id340out_output;

    id343out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id343in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id408out_output;

  { // Node ID: 408 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id408in_input = id343out_o;

    id408out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id408in_input))));
  }
  { // Node ID: 409 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id409in_addr = id408out_output;

    HWOffsetFix<22,-24,UNSIGNED> id409x_1;

    switch(((long)((id409in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id409x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id409x_1 = (id409sta_rom_store[(id409in_addr.getValueAsLong())]);
        break;
      default:
        id409x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id409out_dout[(getCycle()+2)%3] = (id409x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id342out_o;

  { // Node ID: 342 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id342in_i = id340out_output;

    id342out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id342in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id405out_output;

  { // Node ID: 405 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id405in_input = id342out_o;

    id405out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id405in_input))));
  }
  { // Node ID: 406 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id406in_addr = id405out_output;

    HWOffsetFix<24,-24,UNSIGNED> id406x_1;

    switch(((long)((id406in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id406x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id406x_1 = (id406sta_rom_store[(id406in_addr.getValueAsLong())]);
        break;
      default:
        id406x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id406out_dout[(getCycle()+2)%3] = (id406x_1);
  }
  { // Node ID: 347 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id344out_o;

  { // Node ID: 344 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id344in_i = id340out_output;

    id344out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id344in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id346out_result;

  { // Node ID: 346 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id346in_a = id347out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id346in_b = id344out_o;

    id346out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id346in_a,id346in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id348out_o;

  { // Node ID: 348 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id348in_i = id346out_result;

    id348out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id348in_i));
  }
  { // Node ID: 3013 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3013in_input = id348out_o;

    id3013out_output[(getCycle()+2)%3] = id3013in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id349out_result;

  { // Node ID: 349 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id349in_a = id406out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id349in_b = id3013out_output[getCycle()%3];

    id349out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id349in_a,id349in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id350out_result;

  { // Node ID: 350 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id350in_a = id3013out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id350in_b = id406out_dout[getCycle()%3];

    id350out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id350in_a,id350in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id351out_result;

  { // Node ID: 351 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id351in_a = id349out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id351in_b = id350out_result;

    id351out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id351in_a,id351in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id352out_o;

  { // Node ID: 352 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id352in_i = id351out_result;

    id352out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id352in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id353in_a = id409out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id353in_b = id352out_o;

    id353out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id353in_a,id353in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id354out_result;

  { // Node ID: 354 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id354in_a = id352out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id354in_b = id409out_dout[getCycle()%3];

    id354out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id354in_a,id354in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id355out_result;

  { // Node ID: 355 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id355in_a = id353out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id355in_b = id354out_result;

    id355out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id355in_a,id355in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id356out_o;

  { // Node ID: 356 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id356in_i = id355out_result;

    id356out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id356in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id357out_o;

  { // Node ID: 357 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id357in_i = id356out_o;

    id357out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id357in_i));
  }
  { // Node ID: 3410 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2687out_result;

  { // Node ID: 2687 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2687in_a = id357out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2687in_b = id3410out_value;

    id2687out_result = (gte_fixed(id2687in_a,id2687in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2881out_result;

  { // Node ID: 2881 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2881in_a = id363out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2881in_b = id366out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2881in_c = id2832out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2881in_condb = id2687out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2881x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2881x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2881x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2881x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2881x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2881x_1 = id2881in_a;
        break;
      default:
        id2881x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2881in_condb.getValueAsLong())) {
      case 0l:
        id2881x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2881x_2 = id2881in_b;
        break;
      default:
        id2881x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2881x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2881x_3 = id2881in_c;
        break;
      default:
        id2881x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2881x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2881x_1),(id2881x_2))),(id2881x_3)));
    id2881out_result = (id2881x_4);
  }
  HWRawBits<1> id2688out_result;

  { // Node ID: 2688 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2688in_a = id2881out_result;

    id2688out_result = (slice<10,1>(id2688in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2689out_output;

  { // Node ID: 2689 (NodeReinterpret)
    const HWRawBits<1> &id2689in_input = id2688out_result;

    id2689out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2689in_input));
  }
  { // Node ID: 3409 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id333out_result;

  { // Node ID: 333 (NodeGt)
    const HWFloat<8,24> &id333in_a = id324out_result;
    const HWFloat<8,24> &id333in_b = id3409out_value;

    id333out_result = (gt_float(id333in_a,id333in_b));
  }
  { // Node ID: 3015 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3015in_input = id333out_result;

    id3015out_output[(getCycle()+6)%7] = id3015in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id334out_output;

  { // Node ID: 334 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id334in_input = id331out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id334in_input_doubt = id331out_o_doubt;

    id334out_output = id334in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id335out_result;

  { // Node ID: 335 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id335in_a = id3015out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id335in_b = id334out_output;

    HWOffsetFix<1,0,UNSIGNED> id335x_1;

    (id335x_1) = (and_fixed(id335in_a,id335in_b));
    id335out_result = (id335x_1);
  }
  { // Node ID: 3016 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3016in_input = id335out_result;

    id3016out_output[(getCycle()+2)%3] = id3016in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id372out_result;

  { // Node ID: 372 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id372in_a = id3016out_output[getCycle()%3];

    id372out_result = (not_fixed(id372in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id373out_result;

  { // Node ID: 373 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id373in_a = id2689out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id373in_b = id372out_result;

    HWOffsetFix<1,0,UNSIGNED> id373x_1;

    (id373x_1) = (and_fixed(id373in_a,id373in_b));
    id373out_result = (id373x_1);
  }
  { // Node ID: 3408 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id337out_result;

  { // Node ID: 337 (NodeLt)
    const HWFloat<8,24> &id337in_a = id324out_result;
    const HWFloat<8,24> &id337in_b = id3408out_value;

    id337out_result = (lt_float(id337in_a,id337in_b));
  }
  { // Node ID: 3017 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3017in_input = id337out_result;

    id3017out_output[(getCycle()+6)%7] = id3017in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id338out_output;

  { // Node ID: 338 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id338in_input = id331out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id338in_input_doubt = id331out_o_doubt;

    id338out_output = id338in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id339out_result;

  { // Node ID: 339 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id339in_a = id3017out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id339in_b = id338out_output;

    HWOffsetFix<1,0,UNSIGNED> id339x_1;

    (id339x_1) = (and_fixed(id339in_a,id339in_b));
    id339out_result = (id339x_1);
  }
  { // Node ID: 3018 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3018in_input = id339out_result;

    id3018out_output[(getCycle()+2)%3] = id3018in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id374out_result;

  { // Node ID: 374 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id374in_a = id373out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id374in_b = id3018out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id374x_1;

    (id374x_1) = (or_fixed(id374in_a,id374in_b));
    id374out_result = (id374x_1);
  }
  { // Node ID: 3407 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2690out_result;

  { // Node ID: 2690 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2690in_a = id2881out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2690in_b = id3407out_value;

    id2690out_result = (gte_fixed(id2690in_a,id2690in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id381out_result;

  { // Node ID: 381 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id381in_a = id3018out_output[getCycle()%3];

    id381out_result = (not_fixed(id381in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id382out_result;

  { // Node ID: 382 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id382in_a = id2690out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id382in_b = id381out_result;

    HWOffsetFix<1,0,UNSIGNED> id382x_1;

    (id382x_1) = (and_fixed(id382in_a,id382in_b));
    id382out_result = (id382x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id383out_result;

  { // Node ID: 383 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id383in_a = id382out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_b = id3016out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id383x_1;

    (id383x_1) = (or_fixed(id383in_a,id383in_b));
    id383out_result = (id383x_1);
  }
  HWRawBits<2> id384out_result;

  { // Node ID: 384 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id384in_in0 = id374out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id384in_in1 = id383out_result;

    id384out_result = (cat(id384in_in0,id384in_in1));
  }
  { // Node ID: 376 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id375out_o;

  { // Node ID: 375 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id375in_i = id2881out_result;

    id375out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id375in_i));
  }
  { // Node ID: 360 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id361out_result;

  { // Node ID: 361 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id361in_sel = id2687out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id361in_option0 = id357out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id361in_option1 = id360out_value;

    HWOffsetFix<24,-23,UNSIGNED> id361x_1;

    switch((id361in_sel.getValueAsLong())) {
      case 0l:
        id361x_1 = id361in_option0;
        break;
      case 1l:
        id361x_1 = id361in_option1;
        break;
      default:
        id361x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id361out_result = (id361x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id362out_o;

  { // Node ID: 362 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id362in_i = id361out_result;

    id362out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id362in_i));
  }
  HWRawBits<32> id377out_result;

  { // Node ID: 377 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id377in_in0 = id376out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id377in_in1 = id375out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id377in_in2 = id362out_o;

    id377out_result = (cat((cat(id377in_in0,id377in_in1)),id377in_in2));
  }
  HWFloat<8,24> id378out_output;

  { // Node ID: 378 (NodeReinterpret)
    const HWRawBits<32> &id378in_input = id377out_result;

    id378out_output = (cast_bits2float<8,24>(id378in_input));
  }
  { // Node ID: 385 (NodeConstantRawBits)
  }
  { // Node ID: 386 (NodeConstantRawBits)
  }
  { // Node ID: 388 (NodeConstantRawBits)
  }
  HWRawBits<32> id2691out_result;

  { // Node ID: 2691 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2691in_in0 = id385out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2691in_in1 = id386out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2691in_in2 = id388out_value;

    id2691out_result = (cat((cat(id2691in_in0,id2691in_in1)),id2691in_in2));
  }
  HWFloat<8,24> id390out_output;

  { // Node ID: 390 (NodeReinterpret)
    const HWRawBits<32> &id390in_input = id2691out_result;

    id390out_output = (cast_bits2float<8,24>(id390in_input));
  }
  { // Node ID: 2603 (NodeConstantRawBits)
  }
  HWFloat<8,24> id393out_result;

  { // Node ID: 393 (NodeMux)
    const HWRawBits<2> &id393in_sel = id384out_result;
    const HWFloat<8,24> &id393in_option0 = id378out_output;
    const HWFloat<8,24> &id393in_option1 = id390out_output;
    const HWFloat<8,24> &id393in_option2 = id2603out_value;
    const HWFloat<8,24> &id393in_option3 = id390out_output;

    HWFloat<8,24> id393x_1;

    switch((id393in_sel.getValueAsLong())) {
      case 0l:
        id393x_1 = id393in_option0;
        break;
      case 1l:
        id393x_1 = id393in_option1;
        break;
      case 2l:
        id393x_1 = id393in_option2;
        break;
      case 3l:
        id393x_1 = id393in_option3;
        break;
      default:
        id393x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id393out_result = (id393x_1);
  }
  { // Node ID: 3406 (NodeConstantRawBits)
  }
  HWFloat<8,24> id403out_result;

  { // Node ID: 403 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_sel = id3021out_output[getCycle()%9];
    const HWFloat<8,24> &id403in_option0 = id393out_result;
    const HWFloat<8,24> &id403in_option1 = id3406out_value;

    HWFloat<8,24> id403x_1;

    switch((id403in_sel.getValueAsLong())) {
      case 0l:
        id403x_1 = id403in_option0;
        break;
      case 1l:
        id403x_1 = id403in_option1;
        break;
      default:
        id403x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id403out_result = (id403x_1);
  }
  { // Node ID: 3405 (NodeConstantRawBits)
  }
  HWFloat<8,24> id411out_result;

  { // Node ID: 411 (NodeAdd)
    const HWFloat<8,24> &id411in_a = id403out_result;
    const HWFloat<8,24> &id411in_b = id3405out_value;

    id411out_result = (add_float(id411in_a,id411in_b));
  }
  HWFloat<8,24> id412out_result;

  { // Node ID: 412 (NodeDiv)
    const HWFloat<8,24> &id412in_a = id320out_result;
    const HWFloat<8,24> &id412in_b = id411out_result;

    id412out_result = (div_float(id412in_a,id412in_b));
  }
  HWFloat<8,24> id413out_result;

  { // Node ID: 413 (NodeMul)
    const HWFloat<8,24> &id413in_a = id12out_dt;
    const HWFloat<8,24> &id413in_b = id412out_result;

    id413out_result = (mul_float(id413in_a,id413in_b));
  }
  HWFloat<8,24> id1805out_result;

  { // Node ID: 1805 (NodeMul)
    const HWFloat<8,24> &id1805in_a = id413out_result;
    const HWFloat<8,24> &id1805in_b = id3001out_output[getCycle()%9];

    id1805out_result = (mul_float(id1805in_a,id1805in_b));
  }
  HWFloat<8,24> id1806out_result;

  { // Node ID: 1806 (NodeSub)
    const HWFloat<8,24> &id1806in_a = id1804out_result;
    const HWFloat<8,24> &id1806in_b = id1805out_result;

    id1806out_result = (sub_float(id1806in_a,id1806in_b));
  }
  HWFloat<8,24> id1807out_result;

  { // Node ID: 1807 (NodeAdd)
    const HWFloat<8,24> &id1807in_a = id3001out_output[getCycle()%9];
    const HWFloat<8,24> &id1807in_b = id1806out_result;

    id1807out_result = (add_float(id1807in_a,id1807in_b));
  }
  { // Node ID: 2980 (NodeFIFO)
    const HWFloat<8,24> &id2980in_input = id1807out_result;

    id2980out_output[(getCycle()+1)%2] = id2980in_input;
  }
  { // Node ID: 3404 (NodeConstantRawBits)
  }
  { // Node ID: 2692 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2692in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2692in_b = id3404out_value;

    id2692out_result[(getCycle()+1)%2] = (eq_fixed(id2692in_a,id2692in_b));
  }
  { // Node ID: 3403 (NodeConstantRawBits)
  }
  HWFloat<8,24> id415out_result;

  { // Node ID: 415 (NodeAdd)
    const HWFloat<8,24> &id415in_a = id3231out_output[getCycle()%8];
    const HWFloat<8,24> &id415in_b = id3403out_value;

    id415out_result = (add_float(id415in_a,id415in_b));
  }
  HWFloat<8,24> id416out_result;

  { // Node ID: 416 (NodeNeg)
    const HWFloat<8,24> &id416in_a = id415out_result;

    id416out_result = (neg_float(id416in_a));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id459out_o;

  { // Node ID: 459 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id459in_i = id3027out_output[getCycle()%3];

    id459out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id459in_i));
  }
  { // Node ID: 462 (NodeConstantRawBits)
  }
  { // Node ID: 2834 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id445out_result;

  { // Node ID: 445 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id445in_a = id502out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id445in_b = id3028out_output[getCycle()%3];

    id445out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id445in_a,id445in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id446out_result;

  { // Node ID: 446 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id446in_a = id3028out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id446in_b = id502out_dout[getCycle()%3];

    id446out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id446in_a,id446in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id447out_result;

  { // Node ID: 447 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id447in_a = id445out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id447in_b = id446out_result;

    id447out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id447in_a,id447in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id448out_o;

  { // Node ID: 448 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id448in_i = id447out_result;

    id448out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id448in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id449out_result;

  { // Node ID: 449 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id449in_a = id505out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id449in_b = id448out_o;

    id449out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id449in_a,id449in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id450out_result;

  { // Node ID: 450 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id450in_a = id448out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id450in_b = id505out_dout[getCycle()%3];

    id450out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id450in_a,id450in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id451out_result;

  { // Node ID: 451 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id451in_a = id449out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id451in_b = id450out_result;

    id451out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id451in_a,id451in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id452out_o;

  { // Node ID: 452 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id452in_i = id451out_result;

    id452out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id452in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id453out_o;

  { // Node ID: 453 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id453in_i = id452out_o;

    id453out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id453in_i));
  }
  { // Node ID: 3399 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2695out_result;

  { // Node ID: 2695 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2695in_a = id453out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2695in_b = id3399out_value;

    id2695out_result = (gte_fixed(id2695in_a,id2695in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2880out_result;

  { // Node ID: 2880 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2880in_a = id459out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2880in_b = id462out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2880in_c = id2834out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2880in_condb = id2695out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2880x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2880x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2880x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2880x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2880x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2880x_1 = id2880in_a;
        break;
      default:
        id2880x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2880in_condb.getValueAsLong())) {
      case 0l:
        id2880x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2880x_2 = id2880in_b;
        break;
      default:
        id2880x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2880x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2880x_3 = id2880in_c;
        break;
      default:
        id2880x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2880x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2880x_1),(id2880x_2))),(id2880x_3)));
    id2880out_result = (id2880x_4);
  }
  HWRawBits<1> id2696out_result;

  { // Node ID: 2696 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2696in_a = id2880out_result;

    id2696out_result = (slice<10,1>(id2696in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2697out_output;

  { // Node ID: 2697 (NodeReinterpret)
    const HWRawBits<1> &id2697in_input = id2696out_result;

    id2697out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2697in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id468out_result;

  { // Node ID: 468 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id468in_a = id3031out_output[getCycle()%3];

    id468out_result = (not_fixed(id468in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id469out_result;

  { // Node ID: 469 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id469in_a = id2697out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id469in_b = id468out_result;

    HWOffsetFix<1,0,UNSIGNED> id469x_1;

    (id469x_1) = (and_fixed(id469in_a,id469in_b));
    id469out_result = (id469x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id470out_result;

  { // Node ID: 470 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id470in_a = id469out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id470in_b = id3033out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id470x_1;

    (id470x_1) = (or_fixed(id470in_a,id470in_b));
    id470out_result = (id470x_1);
  }
  { // Node ID: 3396 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2698out_result;

  { // Node ID: 2698 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2698in_a = id2880out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2698in_b = id3396out_value;

    id2698out_result = (gte_fixed(id2698in_a,id2698in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id477out_result;

  { // Node ID: 477 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id477in_a = id3033out_output[getCycle()%3];

    id477out_result = (not_fixed(id477in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id478out_result;

  { // Node ID: 478 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id478in_a = id2698out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id478in_b = id477out_result;

    HWOffsetFix<1,0,UNSIGNED> id478x_1;

    (id478x_1) = (and_fixed(id478in_a,id478in_b));
    id478out_result = (id478x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id479out_result;

  { // Node ID: 479 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id479in_a = id478out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id479in_b = id3031out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id479x_1;

    (id479x_1) = (or_fixed(id479in_a,id479in_b));
    id479out_result = (id479x_1);
  }
  HWRawBits<2> id480out_result;

  { // Node ID: 480 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id480in_in0 = id470out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id480in_in1 = id479out_result;

    id480out_result = (cat(id480in_in0,id480in_in1));
  }
  { // Node ID: 472 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id471out_o;

  { // Node ID: 471 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id471in_i = id2880out_result;

    id471out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id471in_i));
  }
  { // Node ID: 456 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id457out_result;

  { // Node ID: 457 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id457in_sel = id2695out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id457in_option0 = id453out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id457in_option1 = id456out_value;

    HWOffsetFix<24,-23,UNSIGNED> id457x_1;

    switch((id457in_sel.getValueAsLong())) {
      case 0l:
        id457x_1 = id457in_option0;
        break;
      case 1l:
        id457x_1 = id457in_option1;
        break;
      default:
        id457x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id457out_result = (id457x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id458out_o;

  { // Node ID: 458 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id458in_i = id457out_result;

    id458out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id458in_i));
  }
  HWRawBits<32> id473out_result;

  { // Node ID: 473 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id473in_in0 = id472out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id473in_in1 = id471out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id473in_in2 = id458out_o;

    id473out_result = (cat((cat(id473in_in0,id473in_in1)),id473in_in2));
  }
  HWFloat<8,24> id474out_output;

  { // Node ID: 474 (NodeReinterpret)
    const HWRawBits<32> &id474in_input = id473out_result;

    id474out_output = (cast_bits2float<8,24>(id474in_input));
  }
  { // Node ID: 481 (NodeConstantRawBits)
  }
  { // Node ID: 482 (NodeConstantRawBits)
  }
  { // Node ID: 484 (NodeConstantRawBits)
  }
  HWRawBits<32> id2699out_result;

  { // Node ID: 2699 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2699in_in0 = id481out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2699in_in1 = id482out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2699in_in2 = id484out_value;

    id2699out_result = (cat((cat(id2699in_in0,id2699in_in1)),id2699in_in2));
  }
  HWFloat<8,24> id486out_output;

  { // Node ID: 486 (NodeReinterpret)
    const HWRawBits<32> &id486in_input = id2699out_result;

    id486out_output = (cast_bits2float<8,24>(id486in_input));
  }
  { // Node ID: 2604 (NodeConstantRawBits)
  }
  HWFloat<8,24> id489out_result;

  { // Node ID: 489 (NodeMux)
    const HWRawBits<2> &id489in_sel = id480out_result;
    const HWFloat<8,24> &id489in_option0 = id474out_output;
    const HWFloat<8,24> &id489in_option1 = id486out_output;
    const HWFloat<8,24> &id489in_option2 = id2604out_value;
    const HWFloat<8,24> &id489in_option3 = id486out_output;

    HWFloat<8,24> id489x_1;

    switch((id489in_sel.getValueAsLong())) {
      case 0l:
        id489x_1 = id489in_option0;
        break;
      case 1l:
        id489x_1 = id489in_option1;
        break;
      case 2l:
        id489x_1 = id489in_option2;
        break;
      case 3l:
        id489x_1 = id489in_option3;
        break;
      default:
        id489x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id489out_result = (id489x_1);
  }
  { // Node ID: 3395 (NodeConstantRawBits)
  }
  HWFloat<8,24> id499out_result;

  { // Node ID: 499 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id499in_sel = id3036out_output[getCycle()%9];
    const HWFloat<8,24> &id499in_option0 = id489out_result;
    const HWFloat<8,24> &id499in_option1 = id3395out_value;

    HWFloat<8,24> id499x_1;

    switch((id499in_sel.getValueAsLong())) {
      case 0l:
        id499x_1 = id499in_option0;
        break;
      case 1l:
        id499x_1 = id499in_option1;
        break;
      default:
        id499x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id499out_result = (id499x_1);
  }
  { // Node ID: 3394 (NodeConstantRawBits)
  }
  HWFloat<8,24> id507out_result;

  { // Node ID: 507 (NodeSub)
    const HWFloat<8,24> &id507in_a = id499out_result;
    const HWFloat<8,24> &id507in_b = id3394out_value;

    id507out_result = (sub_float(id507in_a,id507in_b));
  }
  HWFloat<8,24> id508out_result;

  { // Node ID: 508 (NodeDiv)
    const HWFloat<8,24> &id508in_a = id416out_result;
    const HWFloat<8,24> &id508in_b = id507out_result;

    id508out_result = (div_float(id508in_a,id508in_b));
  }
  HWFloat<8,24> id509out_result;

  { // Node ID: 509 (NodeMul)
    const HWFloat<8,24> &id509in_a = id12out_dt;
    const HWFloat<8,24> &id509in_b = id508out_result;

    id509out_result = (mul_float(id509in_a,id509in_b));
  }
  { // Node ID: 3393 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1809out_result;

  { // Node ID: 1809 (NodeSub)
    const HWFloat<8,24> &id1809in_a = id3393out_value;
    const HWFloat<8,24> &id1809in_b = id3037out_output[getCycle()%9];

    id1809out_result = (sub_float(id1809in_a,id1809in_b));
  }
  HWFloat<8,24> id1810out_result;

  { // Node ID: 1810 (NodeMul)
    const HWFloat<8,24> &id1810in_a = id509out_result;
    const HWFloat<8,24> &id1810in_b = id1809out_result;

    id1810out_result = (mul_float(id1810in_a,id1810in_b));
  }
  { // Node ID: 3392 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id552out_o;

  { // Node ID: 552 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id552in_i = id3038out_output[getCycle()%3];

    id552out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id552in_i));
  }
  { // Node ID: 555 (NodeConstantRawBits)
  }
  { // Node ID: 2836 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id538out_result;

  { // Node ID: 538 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id538in_a = id595out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id538in_b = id3039out_output[getCycle()%3];

    id538out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id538in_a,id538in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id539out_result;

  { // Node ID: 539 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id539in_a = id3039out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id539in_b = id595out_dout[getCycle()%3];

    id539out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id539in_a,id539in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id540out_result;

  { // Node ID: 540 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id540in_a = id538out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id540in_b = id539out_result;

    id540out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id540in_a,id540in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id541out_o;

  { // Node ID: 541 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id541in_i = id540out_result;

    id541out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id541in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id542out_result;

  { // Node ID: 542 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id542in_a = id598out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id542in_b = id541out_o;

    id542out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id542in_a,id542in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id543out_result;

  { // Node ID: 543 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id543in_a = id541out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id543in_b = id598out_dout[getCycle()%3];

    id543out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id543in_a,id543in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id544out_result;

  { // Node ID: 544 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id544in_a = id542out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id544in_b = id543out_result;

    id544out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id544in_a,id544in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id545out_o;

  { // Node ID: 545 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id545in_i = id544out_result;

    id545out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id545in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id546out_o;

  { // Node ID: 546 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id546in_i = id545out_o;

    id546out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id546in_i));
  }
  { // Node ID: 3388 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2702out_result;

  { // Node ID: 2702 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2702in_a = id546out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2702in_b = id3388out_value;

    id2702out_result = (gte_fixed(id2702in_a,id2702in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2879out_result;

  { // Node ID: 2879 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2879in_a = id552out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2879in_b = id555out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2879in_c = id2836out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2879in_condb = id2702out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2879x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2879x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2879x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2879x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2879x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2879x_1 = id2879in_a;
        break;
      default:
        id2879x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2879in_condb.getValueAsLong())) {
      case 0l:
        id2879x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2879x_2 = id2879in_b;
        break;
      default:
        id2879x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2879x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2879x_3 = id2879in_c;
        break;
      default:
        id2879x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2879x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2879x_1),(id2879x_2))),(id2879x_3)));
    id2879out_result = (id2879x_4);
  }
  HWRawBits<1> id2703out_result;

  { // Node ID: 2703 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2703in_a = id2879out_result;

    id2703out_result = (slice<10,1>(id2703in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2704out_output;

  { // Node ID: 2704 (NodeReinterpret)
    const HWRawBits<1> &id2704in_input = id2703out_result;

    id2704out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2704in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id561out_result;

  { // Node ID: 561 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id561in_a = id3042out_output[getCycle()%3];

    id561out_result = (not_fixed(id561in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id562out_result;

  { // Node ID: 562 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id562in_a = id2704out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id562in_b = id561out_result;

    HWOffsetFix<1,0,UNSIGNED> id562x_1;

    (id562x_1) = (and_fixed(id562in_a,id562in_b));
    id562out_result = (id562x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id563out_result;

  { // Node ID: 563 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id563in_a = id562out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id563in_b = id3044out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id563x_1;

    (id563x_1) = (or_fixed(id563in_a,id563in_b));
    id563out_result = (id563x_1);
  }
  { // Node ID: 3385 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2705out_result;

  { // Node ID: 2705 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2705in_a = id2879out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2705in_b = id3385out_value;

    id2705out_result = (gte_fixed(id2705in_a,id2705in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id570out_result;

  { // Node ID: 570 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id570in_a = id3044out_output[getCycle()%3];

    id570out_result = (not_fixed(id570in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id571out_result;

  { // Node ID: 571 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id571in_a = id2705out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id571in_b = id570out_result;

    HWOffsetFix<1,0,UNSIGNED> id571x_1;

    (id571x_1) = (and_fixed(id571in_a,id571in_b));
    id571out_result = (id571x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id572out_result;

  { // Node ID: 572 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id572in_a = id571out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id572in_b = id3042out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id572x_1;

    (id572x_1) = (or_fixed(id572in_a,id572in_b));
    id572out_result = (id572x_1);
  }
  HWRawBits<2> id573out_result;

  { // Node ID: 573 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id573in_in0 = id563out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id573in_in1 = id572out_result;

    id573out_result = (cat(id573in_in0,id573in_in1));
  }
  { // Node ID: 565 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id564out_o;

  { // Node ID: 564 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id564in_i = id2879out_result;

    id564out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id564in_i));
  }
  { // Node ID: 549 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id550out_result;

  { // Node ID: 550 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id550in_sel = id2702out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id550in_option0 = id546out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id550in_option1 = id549out_value;

    HWOffsetFix<24,-23,UNSIGNED> id550x_1;

    switch((id550in_sel.getValueAsLong())) {
      case 0l:
        id550x_1 = id550in_option0;
        break;
      case 1l:
        id550x_1 = id550in_option1;
        break;
      default:
        id550x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id550out_result = (id550x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id551out_o;

  { // Node ID: 551 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id551in_i = id550out_result;

    id551out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id551in_i));
  }
  HWRawBits<32> id566out_result;

  { // Node ID: 566 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id566in_in0 = id565out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id566in_in1 = id564out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id566in_in2 = id551out_o;

    id566out_result = (cat((cat(id566in_in0,id566in_in1)),id566in_in2));
  }
  HWFloat<8,24> id567out_output;

  { // Node ID: 567 (NodeReinterpret)
    const HWRawBits<32> &id567in_input = id566out_result;

    id567out_output = (cast_bits2float<8,24>(id567in_input));
  }
  { // Node ID: 574 (NodeConstantRawBits)
  }
  { // Node ID: 575 (NodeConstantRawBits)
  }
  { // Node ID: 577 (NodeConstantRawBits)
  }
  HWRawBits<32> id2706out_result;

  { // Node ID: 2706 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2706in_in0 = id574out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2706in_in1 = id575out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2706in_in2 = id577out_value;

    id2706out_result = (cat((cat(id2706in_in0,id2706in_in1)),id2706in_in2));
  }
  HWFloat<8,24> id579out_output;

  { // Node ID: 579 (NodeReinterpret)
    const HWRawBits<32> &id579in_input = id2706out_result;

    id579out_output = (cast_bits2float<8,24>(id579in_input));
  }
  { // Node ID: 2605 (NodeConstantRawBits)
  }
  HWFloat<8,24> id582out_result;

  { // Node ID: 582 (NodeMux)
    const HWRawBits<2> &id582in_sel = id573out_result;
    const HWFloat<8,24> &id582in_option0 = id567out_output;
    const HWFloat<8,24> &id582in_option1 = id579out_output;
    const HWFloat<8,24> &id582in_option2 = id2605out_value;
    const HWFloat<8,24> &id582in_option3 = id579out_output;

    HWFloat<8,24> id582x_1;

    switch((id582in_sel.getValueAsLong())) {
      case 0l:
        id582x_1 = id582in_option0;
        break;
      case 1l:
        id582x_1 = id582in_option1;
        break;
      case 2l:
        id582x_1 = id582in_option2;
        break;
      case 3l:
        id582x_1 = id582in_option3;
        break;
      default:
        id582x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id582out_result = (id582x_1);
  }
  { // Node ID: 3384 (NodeConstantRawBits)
  }
  HWFloat<8,24> id592out_result;

  { // Node ID: 592 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id592in_sel = id3047out_output[getCycle()%9];
    const HWFloat<8,24> &id592in_option0 = id582out_result;
    const HWFloat<8,24> &id592in_option1 = id3384out_value;

    HWFloat<8,24> id592x_1;

    switch((id592in_sel.getValueAsLong())) {
      case 0l:
        id592x_1 = id592in_option0;
        break;
      case 1l:
        id592x_1 = id592in_option1;
        break;
      default:
        id592x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id592out_result = (id592x_1);
  }
  HWFloat<8,24> id600out_result;

  { // Node ID: 600 (NodeMul)
    const HWFloat<8,24> &id600in_a = id3392out_value;
    const HWFloat<8,24> &id600in_b = id592out_result;

    id600out_result = (mul_float(id600in_a,id600in_b));
  }
  HWFloat<8,24> id601out_result;

  { // Node ID: 601 (NodeMul)
    const HWFloat<8,24> &id601in_a = id12out_dt;
    const HWFloat<8,24> &id601in_b = id600out_result;

    id601out_result = (mul_float(id601in_a,id601in_b));
  }
  HWFloat<8,24> id1811out_result;

  { // Node ID: 1811 (NodeMul)
    const HWFloat<8,24> &id1811in_a = id601out_result;
    const HWFloat<8,24> &id1811in_b = id3037out_output[getCycle()%9];

    id1811out_result = (mul_float(id1811in_a,id1811in_b));
  }
  HWFloat<8,24> id1812out_result;

  { // Node ID: 1812 (NodeSub)
    const HWFloat<8,24> &id1812in_a = id1810out_result;
    const HWFloat<8,24> &id1812in_b = id1811out_result;

    id1812out_result = (sub_float(id1812in_a,id1812in_b));
  }
  HWFloat<8,24> id1813out_result;

  { // Node ID: 1813 (NodeAdd)
    const HWFloat<8,24> &id1813in_a = id3037out_output[getCycle()%9];
    const HWFloat<8,24> &id1813in_b = id1812out_result;

    id1813out_result = (add_float(id1813in_a,id1813in_b));
  }
  { // Node ID: 3025 (NodeFIFO)
    const HWFloat<8,24> &id3025in_input = id1813out_result;

    id3025out_output[(getCycle()+1)%2] = id3025in_input;
  }
  HWFloat<8,24> id2906out_output;

  { // Node ID: 2906 (NodeStreamOffset)
    const HWFloat<8,24> &id2906in_input = id3025out_output[getCycle()%2];

    id2906out_output = id2906in_input;
  }
  { // Node ID: 28 (NodeInputMappedReg)
  }
  { // Node ID: 29 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id29in_sel = id2692out_result[getCycle()%2];
    const HWFloat<8,24> &id29in_option0 = id2906out_output;
    const HWFloat<8,24> &id29in_option1 = id28out_m_in;

    HWFloat<8,24> id29x_1;

    switch((id29in_sel.getValueAsLong())) {
      case 0l:
        id29x_1 = id29in_option0;
        break;
      case 1l:
        id29x_1 = id29in_option1;
        break;
      default:
        id29x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id29out_result[(getCycle()+1)%2] = (id29x_1);
  }
  { // Node ID: 3037 (NodeFIFO)
    const HWFloat<8,24> &id3037in_input = id29out_result[getCycle()%2];

    id3037out_output[(getCycle()+8)%9] = id3037in_input;
  }
  { // Node ID: 3402 (NodeConstantRawBits)
  }
  { // Node ID: 3401 (NodeConstantRawBits)
  }
  HWFloat<8,24> id418out_result;

  { // Node ID: 418 (NodeAdd)
    const HWFloat<8,24> &id418in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id418in_b = id3401out_value;

    id418out_result = (add_float(id418in_a,id418in_b));
  }
  HWFloat<8,24> id420out_result;

  { // Node ID: 420 (NodeMul)
    const HWFloat<8,24> &id420in_a = id3402out_value;
    const HWFloat<8,24> &id420in_b = id418out_result;

    id420out_result = (mul_float(id420in_a,id420in_b));
  }
  HWRawBits<8> id492out_result;

  { // Node ID: 492 (NodeSlice)
    const HWFloat<8,24> &id492in_a = id420out_result;

    id492out_result = (slice<23,8>(id492in_a));
  }
  { // Node ID: 493 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2693out_result;

  { // Node ID: 2693 (NodeEqInlined)
    const HWRawBits<8> &id2693in_a = id492out_result;
    const HWRawBits<8> &id2693in_b = id493out_value;

    id2693out_result = (eq_bits(id2693in_a,id2693in_b));
  }
  HWRawBits<23> id491out_result;

  { // Node ID: 491 (NodeSlice)
    const HWFloat<8,24> &id491in_a = id420out_result;

    id491out_result = (slice<0,23>(id491in_a));
  }
  { // Node ID: 3400 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2694out_result;

  { // Node ID: 2694 (NodeNeqInlined)
    const HWRawBits<23> &id2694in_a = id491out_result;
    const HWRawBits<23> &id2694in_b = id3400out_value;

    id2694out_result = (neq_bits(id2694in_a,id2694in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id497out_result;

  { // Node ID: 497 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id497in_a = id2693out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id497in_b = id2694out_result;

    HWOffsetFix<1,0,UNSIGNED> id497x_1;

    (id497x_1) = (and_fixed(id497in_a,id497in_b));
    id497out_result = (id497x_1);
  }
  { // Node ID: 3036 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3036in_input = id497out_result;

    id3036out_output[(getCycle()+8)%9] = id3036in_input;
  }
  { // Node ID: 421 (NodeConstantRawBits)
  }
  HWFloat<8,24> id422out_output;
  HWOffsetFix<1,0,UNSIGNED> id422out_output_doubt;

  { // Node ID: 422 (NodeDoubtBitOp)
    const HWFloat<8,24> &id422in_input = id420out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id422in_doubt = id421out_value;

    id422out_output = id422in_input;
    id422out_output_doubt = id422in_doubt;
  }
  { // Node ID: 423 (NodeCast)
    const HWFloat<8,24> &id423in_i = id422out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id423in_i_doubt = id422out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id423x_1;

    id423out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id423in_i,(&(id423x_1))));
    id423out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id423x_1),(c_hw_fix_4_0_uns_bits))),id423in_i_doubt));
  }
  { // Node ID: 426 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id425out_result;
  HWOffsetFix<1,0,UNSIGNED> id425out_result_doubt;

  { // Node ID: 425 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id425in_a = id423out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id425in_a_doubt = id423out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id425in_b = id426out_value;

    HWOffsetFix<1,0,UNSIGNED> id425x_1;

    id425out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id425in_a,id425in_b,(&(id425x_1))));
    id425out_result_doubt = (or_fixed((neq_fixed((id425x_1),(c_hw_fix_1_0_uns_bits_1))),id425in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id427out_o;
  HWOffsetFix<1,0,UNSIGNED> id427out_o_doubt;

  { // Node ID: 427 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id427in_i = id425out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id427in_i_doubt = id425out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id427x_1;

    id427out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id427in_i,(&(id427x_1))));
    id427out_o_doubt = (or_fixed((neq_fixed((id427x_1),(c_hw_fix_1_0_uns_bits_1))),id427in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id436out_output;

  { // Node ID: 436 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id436in_input = id427out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id436in_input_doubt = id427out_o_doubt;

    id436out_output = id436in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id437out_o;

  { // Node ID: 437 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id437in_i = id436out_output;

    id437out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id437in_i));
  }
  { // Node ID: 3027 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3027in_input = id437out_o;

    id3027out_output[(getCycle()+2)%3] = id3027in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id439out_o;

  { // Node ID: 439 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id439in_i = id436out_output;

    id439out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id439in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id504out_output;

  { // Node ID: 504 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id504in_input = id439out_o;

    id504out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id504in_input))));
  }
  { // Node ID: 505 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id505in_addr = id504out_output;

    HWOffsetFix<22,-24,UNSIGNED> id505x_1;

    switch(((long)((id505in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id505x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id505x_1 = (id505sta_rom_store[(id505in_addr.getValueAsLong())]);
        break;
      default:
        id505x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id505out_dout[(getCycle()+2)%3] = (id505x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id438out_o;

  { // Node ID: 438 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id438in_i = id436out_output;

    id438out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id438in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id501out_output;

  { // Node ID: 501 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id501in_input = id438out_o;

    id501out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id501in_input))));
  }
  { // Node ID: 502 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id502in_addr = id501out_output;

    HWOffsetFix<24,-24,UNSIGNED> id502x_1;

    switch(((long)((id502in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id502x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id502x_1 = (id502sta_rom_store[(id502in_addr.getValueAsLong())]);
        break;
      default:
        id502x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id502out_dout[(getCycle()+2)%3] = (id502x_1);
  }
  { // Node ID: 443 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id440out_o;

  { // Node ID: 440 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id440in_i = id436out_output;

    id440out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id440in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id442out_result;

  { // Node ID: 442 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id442in_a = id443out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id442in_b = id440out_o;

    id442out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id442in_a,id442in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id444out_o;

  { // Node ID: 444 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id444in_i = id442out_result;

    id444out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id444in_i));
  }
  { // Node ID: 3028 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3028in_input = id444out_o;

    id3028out_output[(getCycle()+2)%3] = id3028in_input;
  }
  { // Node ID: 3398 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id429out_result;

  { // Node ID: 429 (NodeGt)
    const HWFloat<8,24> &id429in_a = id420out_result;
    const HWFloat<8,24> &id429in_b = id3398out_value;

    id429out_result = (gt_float(id429in_a,id429in_b));
  }
  { // Node ID: 3030 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3030in_input = id429out_result;

    id3030out_output[(getCycle()+6)%7] = id3030in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id430out_output;

  { // Node ID: 430 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id430in_input = id427out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id430in_input_doubt = id427out_o_doubt;

    id430out_output = id430in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id431out_result;

  { // Node ID: 431 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id431in_a = id3030out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id431in_b = id430out_output;

    HWOffsetFix<1,0,UNSIGNED> id431x_1;

    (id431x_1) = (and_fixed(id431in_a,id431in_b));
    id431out_result = (id431x_1);
  }
  { // Node ID: 3031 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3031in_input = id431out_result;

    id3031out_output[(getCycle()+2)%3] = id3031in_input;
  }
  { // Node ID: 3397 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id433out_result;

  { // Node ID: 433 (NodeLt)
    const HWFloat<8,24> &id433in_a = id420out_result;
    const HWFloat<8,24> &id433in_b = id3397out_value;

    id433out_result = (lt_float(id433in_a,id433in_b));
  }
  { // Node ID: 3032 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3032in_input = id433out_result;

    id3032out_output[(getCycle()+6)%7] = id3032in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id434out_output;

  { // Node ID: 434 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id434in_input = id427out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id434in_input_doubt = id427out_o_doubt;

    id434out_output = id434in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id435out_result;

  { // Node ID: 435 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id435in_a = id3032out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id435in_b = id434out_output;

    HWOffsetFix<1,0,UNSIGNED> id435x_1;

    (id435x_1) = (and_fixed(id435in_a,id435in_b));
    id435out_result = (id435x_1);
  }
  { // Node ID: 3033 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3033in_input = id435out_result;

    id3033out_output[(getCycle()+2)%3] = id3033in_input;
  }
  { // Node ID: 3391 (NodeConstantRawBits)
  }
  { // Node ID: 3390 (NodeConstantRawBits)
  }
  HWFloat<8,24> id511out_result;

  { // Node ID: 511 (NodeAdd)
    const HWFloat<8,24> &id511in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id511in_b = id3390out_value;

    id511out_result = (add_float(id511in_a,id511in_b));
  }
  HWFloat<8,24> id513out_result;

  { // Node ID: 513 (NodeMul)
    const HWFloat<8,24> &id513in_a = id3391out_value;
    const HWFloat<8,24> &id513in_b = id511out_result;

    id513out_result = (mul_float(id513in_a,id513in_b));
  }
  HWRawBits<8> id585out_result;

  { // Node ID: 585 (NodeSlice)
    const HWFloat<8,24> &id585in_a = id513out_result;

    id585out_result = (slice<23,8>(id585in_a));
  }
  { // Node ID: 586 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2700out_result;

  { // Node ID: 2700 (NodeEqInlined)
    const HWRawBits<8> &id2700in_a = id585out_result;
    const HWRawBits<8> &id2700in_b = id586out_value;

    id2700out_result = (eq_bits(id2700in_a,id2700in_b));
  }
  HWRawBits<23> id584out_result;

  { // Node ID: 584 (NodeSlice)
    const HWFloat<8,24> &id584in_a = id513out_result;

    id584out_result = (slice<0,23>(id584in_a));
  }
  { // Node ID: 3389 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2701out_result;

  { // Node ID: 2701 (NodeNeqInlined)
    const HWRawBits<23> &id2701in_a = id584out_result;
    const HWRawBits<23> &id2701in_b = id3389out_value;

    id2701out_result = (neq_bits(id2701in_a,id2701in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id590out_result;

  { // Node ID: 590 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id590in_a = id2700out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id590in_b = id2701out_result;

    HWOffsetFix<1,0,UNSIGNED> id590x_1;

    (id590x_1) = (and_fixed(id590in_a,id590in_b));
    id590out_result = (id590x_1);
  }
  { // Node ID: 3047 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3047in_input = id590out_result;

    id3047out_output[(getCycle()+8)%9] = id3047in_input;
  }
  { // Node ID: 514 (NodeConstantRawBits)
  }
  HWFloat<8,24> id515out_output;
  HWOffsetFix<1,0,UNSIGNED> id515out_output_doubt;

  { // Node ID: 515 (NodeDoubtBitOp)
    const HWFloat<8,24> &id515in_input = id513out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id515in_doubt = id514out_value;

    id515out_output = id515in_input;
    id515out_output_doubt = id515in_doubt;
  }
  { // Node ID: 516 (NodeCast)
    const HWFloat<8,24> &id516in_i = id515out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id516in_i_doubt = id515out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id516x_1;

    id516out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id516in_i,(&(id516x_1))));
    id516out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id516x_1),(c_hw_fix_4_0_uns_bits))),id516in_i_doubt));
  }
  { // Node ID: 519 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id518out_result;
  HWOffsetFix<1,0,UNSIGNED> id518out_result_doubt;

  { // Node ID: 518 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id518in_a = id516out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id518in_a_doubt = id516out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id518in_b = id519out_value;

    HWOffsetFix<1,0,UNSIGNED> id518x_1;

    id518out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id518in_a,id518in_b,(&(id518x_1))));
    id518out_result_doubt = (or_fixed((neq_fixed((id518x_1),(c_hw_fix_1_0_uns_bits_1))),id518in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id520out_o;
  HWOffsetFix<1,0,UNSIGNED> id520out_o_doubt;

  { // Node ID: 520 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id520in_i = id518out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id520in_i_doubt = id518out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id520x_1;

    id520out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id520in_i,(&(id520x_1))));
    id520out_o_doubt = (or_fixed((neq_fixed((id520x_1),(c_hw_fix_1_0_uns_bits_1))),id520in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id529out_output;

  { // Node ID: 529 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id529in_input = id520out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id529in_input_doubt = id520out_o_doubt;

    id529out_output = id529in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id530out_o;

  { // Node ID: 530 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id530in_i = id529out_output;

    id530out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id530in_i));
  }
  { // Node ID: 3038 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3038in_input = id530out_o;

    id3038out_output[(getCycle()+2)%3] = id3038in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id532out_o;

  { // Node ID: 532 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id532in_i = id529out_output;

    id532out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id532in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id597out_output;

  { // Node ID: 597 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id597in_input = id532out_o;

    id597out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id597in_input))));
  }
  { // Node ID: 598 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id598in_addr = id597out_output;

    HWOffsetFix<22,-24,UNSIGNED> id598x_1;

    switch(((long)((id598in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id598x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id598x_1 = (id598sta_rom_store[(id598in_addr.getValueAsLong())]);
        break;
      default:
        id598x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id598out_dout[(getCycle()+2)%3] = (id598x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id531out_o;

  { // Node ID: 531 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id531in_i = id529out_output;

    id531out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id531in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id594out_output;

  { // Node ID: 594 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id594in_input = id531out_o;

    id594out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id594in_input))));
  }
  { // Node ID: 595 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id595in_addr = id594out_output;

    HWOffsetFix<24,-24,UNSIGNED> id595x_1;

    switch(((long)((id595in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id595x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id595x_1 = (id595sta_rom_store[(id595in_addr.getValueAsLong())]);
        break;
      default:
        id595x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id595out_dout[(getCycle()+2)%3] = (id595x_1);
  }
  { // Node ID: 536 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id533out_o;

  { // Node ID: 533 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id533in_i = id529out_output;

    id533out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id533in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id535out_result;

  { // Node ID: 535 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id535in_a = id536out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id535in_b = id533out_o;

    id535out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id535in_a,id535in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id537out_o;

  { // Node ID: 537 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id537in_i = id535out_result;

    id537out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id537in_i));
  }
  { // Node ID: 3039 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3039in_input = id537out_o;

    id3039out_output[(getCycle()+2)%3] = id3039in_input;
  }
  { // Node ID: 3387 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id522out_result;

  { // Node ID: 522 (NodeGt)
    const HWFloat<8,24> &id522in_a = id513out_result;
    const HWFloat<8,24> &id522in_b = id3387out_value;

    id522out_result = (gt_float(id522in_a,id522in_b));
  }
  { // Node ID: 3041 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3041in_input = id522out_result;

    id3041out_output[(getCycle()+6)%7] = id3041in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id523out_output;

  { // Node ID: 523 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id523in_input = id520out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id523in_input_doubt = id520out_o_doubt;

    id523out_output = id523in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id524out_result;

  { // Node ID: 524 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id524in_a = id3041out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id524in_b = id523out_output;

    HWOffsetFix<1,0,UNSIGNED> id524x_1;

    (id524x_1) = (and_fixed(id524in_a,id524in_b));
    id524out_result = (id524x_1);
  }
  { // Node ID: 3042 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3042in_input = id524out_result;

    id3042out_output[(getCycle()+2)%3] = id3042in_input;
  }
  { // Node ID: 3386 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id526out_result;

  { // Node ID: 526 (NodeLt)
    const HWFloat<8,24> &id526in_a = id513out_result;
    const HWFloat<8,24> &id526in_b = id3386out_value;

    id526out_result = (lt_float(id526in_a,id526in_b));
  }
  { // Node ID: 3043 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3043in_input = id526out_result;

    id3043out_output[(getCycle()+6)%7] = id3043in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id527out_output;

  { // Node ID: 527 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id527in_input = id520out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id527in_input_doubt = id520out_o_doubt;

    id527out_output = id527in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id528out_result;

  { // Node ID: 528 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id528in_a = id3043out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id528in_b = id527out_output;

    HWOffsetFix<1,0,UNSIGNED> id528x_1;

    (id528x_1) = (and_fixed(id528in_a,id528in_b));
    id528out_result = (id528x_1);
  }
  { // Node ID: 3044 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3044in_input = id528out_result;

    id3044out_output[(getCycle()+2)%3] = id3044in_input;
  }
  { // Node ID: 2892 (NodePO2FPMult)
    const HWFloat<8,24> &id2892in_floatIn = id1813out_result;

    id2892out_floatOut[(getCycle()+1)%2] = (mul_float(id2892in_floatIn,(c_hw_flt_8_24_4_0val)));
  }
  { // Node ID: 3383 (NodeConstantRawBits)
  }
  { // Node ID: 2707 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2707in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2707in_b = id3383out_value;

    id2707out_result[(getCycle()+1)%2] = (eq_fixed(id2707in_a,id2707in_b));
  }
  HWFloat<8,24> id2907out_output;

  { // Node ID: 2907 (NodeStreamOffset)
    const HWFloat<8,24> &id2907in_input = id1819out_result;

    id2907out_output = id2907in_input;
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id37in_sel = id2707out_result[getCycle()%2];
    const HWFloat<8,24> &id37in_option0 = id2907out_output;
    const HWFloat<8,24> &id37in_option1 = id36out_h_in;

    HWFloat<8,24> id37x_1;

    switch((id37in_sel.getValueAsLong())) {
      case 0l:
        id37x_1 = id37in_option0;
        break;
      case 1l:
        id37x_1 = id37in_option1;
        break;
      default:
        id37x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id37out_result[(getCycle()+1)%2] = (id37x_1);
  }
  { // Node ID: 3062 (NodeFIFO)
    const HWFloat<8,24> &id3062in_input = id37out_result[getCycle()%2];

    id3062out_output[(getCycle()+9)%10] = id3062in_input;
  }
  { // Node ID: 3381 (NodeConstantRawBits)
  }
  HWFloat<8,24> id603out_result;

  { // Node ID: 603 (NodeAdd)
    const HWFloat<8,24> &id603in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id603in_b = id3381out_value;

    id603out_result = (add_float(id603in_a,id603in_b));
  }
  { // Node ID: 2893 (NodePO2FPMult)
    const HWFloat<8,24> &id2893in_floatIn = id603out_result;

    id2893out_floatOut[(getCycle()+1)%2] = (mul_float(id2893in_floatIn,(c_hw_flt_8_24_n0_25val)));
  }
  HWRawBits<8> id677out_result;

  { // Node ID: 677 (NodeSlice)
    const HWFloat<8,24> &id677in_a = id2893out_floatOut[getCycle()%2];

    id677out_result = (slice<23,8>(id677in_a));
  }
  { // Node ID: 678 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2708out_result;

  { // Node ID: 2708 (NodeEqInlined)
    const HWRawBits<8> &id2708in_a = id677out_result;
    const HWRawBits<8> &id2708in_b = id678out_value;

    id2708out_result = (eq_bits(id2708in_a,id2708in_b));
  }
  HWRawBits<23> id676out_result;

  { // Node ID: 676 (NodeSlice)
    const HWFloat<8,24> &id676in_a = id2893out_floatOut[getCycle()%2];

    id676out_result = (slice<0,23>(id676in_a));
  }
  { // Node ID: 3380 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2709out_result;

  { // Node ID: 2709 (NodeNeqInlined)
    const HWRawBits<23> &id2709in_a = id676out_result;
    const HWRawBits<23> &id2709in_b = id3380out_value;

    id2709out_result = (neq_bits(id2709in_a,id2709in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id682out_result;

  { // Node ID: 682 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id682in_a = id2708out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id682in_b = id2709out_result;

    HWOffsetFix<1,0,UNSIGNED> id682x_1;

    (id682x_1) = (and_fixed(id682in_a,id682in_b));
    id682out_result = (id682x_1);
  }
  { // Node ID: 3061 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3061in_input = id682out_result;

    id3061out_output[(getCycle()+8)%9] = id3061in_input;
  }
  { // Node ID: 606 (NodeConstantRawBits)
  }
  HWFloat<8,24> id607out_output;
  HWOffsetFix<1,0,UNSIGNED> id607out_output_doubt;

  { // Node ID: 607 (NodeDoubtBitOp)
    const HWFloat<8,24> &id607in_input = id2893out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id607in_doubt = id606out_value;

    id607out_output = id607in_input;
    id607out_output_doubt = id607in_doubt;
  }
  { // Node ID: 608 (NodeCast)
    const HWFloat<8,24> &id608in_i = id607out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id608in_i_doubt = id607out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id608x_1;

    id608out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id608in_i,(&(id608x_1))));
    id608out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id608x_1),(c_hw_fix_4_0_uns_bits))),id608in_i_doubt));
  }
  { // Node ID: 611 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id610out_result;
  HWOffsetFix<1,0,UNSIGNED> id610out_result_doubt;

  { // Node ID: 610 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id610in_a = id608out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id610in_a_doubt = id608out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id610in_b = id611out_value;

    HWOffsetFix<1,0,UNSIGNED> id610x_1;

    id610out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id610in_a,id610in_b,(&(id610x_1))));
    id610out_result_doubt = (or_fixed((neq_fixed((id610x_1),(c_hw_fix_1_0_uns_bits_1))),id610in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id612out_o;
  HWOffsetFix<1,0,UNSIGNED> id612out_o_doubt;

  { // Node ID: 612 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id612in_i = id610out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id612in_i_doubt = id610out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id612x_1;

    id612out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id612in_i,(&(id612x_1))));
    id612out_o_doubt = (or_fixed((neq_fixed((id612x_1),(c_hw_fix_1_0_uns_bits_1))),id612in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id621out_output;

  { // Node ID: 621 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id621in_input = id612out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id621in_input_doubt = id612out_o_doubt;

    id621out_output = id621in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id622out_o;

  { // Node ID: 622 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id622in_i = id621out_output;

    id622out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id622in_i));
  }
  { // Node ID: 3052 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3052in_input = id622out_o;

    id3052out_output[(getCycle()+2)%3] = id3052in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id624out_o;

  { // Node ID: 624 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id624in_i = id621out_output;

    id624out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id624in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id689out_output;

  { // Node ID: 689 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id689in_input = id624out_o;

    id689out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id689in_input))));
  }
  { // Node ID: 690 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id690in_addr = id689out_output;

    HWOffsetFix<22,-24,UNSIGNED> id690x_1;

    switch(((long)((id690in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id690x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id690x_1 = (id690sta_rom_store[(id690in_addr.getValueAsLong())]);
        break;
      default:
        id690x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id690out_dout[(getCycle()+2)%3] = (id690x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id623out_o;

  { // Node ID: 623 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id623in_i = id621out_output;

    id623out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id623in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id686out_output;

  { // Node ID: 686 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id686in_input = id623out_o;

    id686out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id686in_input))));
  }
  { // Node ID: 687 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id687in_addr = id686out_output;

    HWOffsetFix<24,-24,UNSIGNED> id687x_1;

    switch(((long)((id687in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id687x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id687x_1 = (id687sta_rom_store[(id687in_addr.getValueAsLong())]);
        break;
      default:
        id687x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id687out_dout[(getCycle()+2)%3] = (id687x_1);
  }
  { // Node ID: 628 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id625out_o;

  { // Node ID: 625 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id625in_i = id621out_output;

    id625out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id625in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id627out_result;

  { // Node ID: 627 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id627in_a = id628out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id627in_b = id625out_o;

    id627out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id627in_a,id627in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id629out_o;

  { // Node ID: 629 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id629in_i = id627out_result;

    id629out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id629in_i));
  }
  { // Node ID: 3053 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3053in_input = id629out_o;

    id3053out_output[(getCycle()+2)%3] = id3053in_input;
  }
  { // Node ID: 3378 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id614out_result;

  { // Node ID: 614 (NodeGt)
    const HWFloat<8,24> &id614in_a = id2893out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id614in_b = id3378out_value;

    id614out_result = (gt_float(id614in_a,id614in_b));
  }
  { // Node ID: 3055 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3055in_input = id614out_result;

    id3055out_output[(getCycle()+6)%7] = id3055in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id615out_output;

  { // Node ID: 615 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id615in_input = id612out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id615in_input_doubt = id612out_o_doubt;

    id615out_output = id615in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id616out_result;

  { // Node ID: 616 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id616in_a = id3055out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id616in_b = id615out_output;

    HWOffsetFix<1,0,UNSIGNED> id616x_1;

    (id616x_1) = (and_fixed(id616in_a,id616in_b));
    id616out_result = (id616x_1);
  }
  { // Node ID: 3056 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3056in_input = id616out_result;

    id3056out_output[(getCycle()+2)%3] = id3056in_input;
  }
  { // Node ID: 3377 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id618out_result;

  { // Node ID: 618 (NodeLt)
    const HWFloat<8,24> &id618in_a = id2893out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id618in_b = id3377out_value;

    id618out_result = (lt_float(id618in_a,id618in_b));
  }
  { // Node ID: 3057 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3057in_input = id618out_result;

    id3057out_output[(getCycle()+6)%7] = id3057in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id619out_output;

  { // Node ID: 619 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id619in_input = id612out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id619in_input_doubt = id612out_o_doubt;

    id619out_output = id619in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id620out_result;

  { // Node ID: 620 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id620in_a = id3057out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id620in_b = id619out_output;

    HWOffsetFix<1,0,UNSIGNED> id620x_1;

    (id620x_1) = (and_fixed(id620in_a,id620in_b));
    id620out_result = (id620x_1);
  }
  { // Node ID: 3058 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3058in_input = id620out_result;

    id3058out_output[(getCycle()+2)%3] = id3058in_input;
  }
  { // Node ID: 3372 (NodeConstantRawBits)
  }
  { // Node ID: 3371 (NodeConstantRawBits)
  }
  HWFloat<8,24> id695out_result;

  { // Node ID: 695 (NodeAdd)
    const HWFloat<8,24> &id695in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id695in_b = id3371out_value;

    id695out_result = (add_float(id695in_a,id695in_b));
  }
  HWFloat<8,24> id697out_result;

  { // Node ID: 697 (NodeMul)
    const HWFloat<8,24> &id697in_a = id3372out_value;
    const HWFloat<8,24> &id697in_b = id695out_result;

    id697out_result = (mul_float(id697in_a,id697in_b));
  }
  HWRawBits<8> id769out_result;

  { // Node ID: 769 (NodeSlice)
    const HWFloat<8,24> &id769in_a = id697out_result;

    id769out_result = (slice<23,8>(id769in_a));
  }
  { // Node ID: 770 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2715out_result;

  { // Node ID: 2715 (NodeEqInlined)
    const HWRawBits<8> &id2715in_a = id769out_result;
    const HWRawBits<8> &id2715in_b = id770out_value;

    id2715out_result = (eq_bits(id2715in_a,id2715in_b));
  }
  HWRawBits<23> id768out_result;

  { // Node ID: 768 (NodeSlice)
    const HWFloat<8,24> &id768in_a = id697out_result;

    id768out_result = (slice<0,23>(id768in_a));
  }
  { // Node ID: 3370 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2716out_result;

  { // Node ID: 2716 (NodeNeqInlined)
    const HWRawBits<23> &id2716in_a = id768out_result;
    const HWRawBits<23> &id2716in_b = id3370out_value;

    id2716out_result = (neq_bits(id2716in_a,id2716in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id774out_result;

  { // Node ID: 774 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id774in_a = id2715out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id774in_b = id2716out_result;

    HWOffsetFix<1,0,UNSIGNED> id774x_1;

    (id774x_1) = (and_fixed(id774in_a,id774in_b));
    id774out_result = (id774x_1);
  }
  { // Node ID: 3073 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3073in_input = id774out_result;

    id3073out_output[(getCycle()+8)%9] = id3073in_input;
  }
  { // Node ID: 698 (NodeConstantRawBits)
  }
  HWFloat<8,24> id699out_output;
  HWOffsetFix<1,0,UNSIGNED> id699out_output_doubt;

  { // Node ID: 699 (NodeDoubtBitOp)
    const HWFloat<8,24> &id699in_input = id697out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id699in_doubt = id698out_value;

    id699out_output = id699in_input;
    id699out_output_doubt = id699in_doubt;
  }
  { // Node ID: 700 (NodeCast)
    const HWFloat<8,24> &id700in_i = id699out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id700in_i_doubt = id699out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id700x_1;

    id700out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id700in_i,(&(id700x_1))));
    id700out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id700x_1),(c_hw_fix_4_0_uns_bits))),id700in_i_doubt));
  }
  { // Node ID: 703 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id702out_result;
  HWOffsetFix<1,0,UNSIGNED> id702out_result_doubt;

  { // Node ID: 702 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id702in_a = id700out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id702in_a_doubt = id700out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id702in_b = id703out_value;

    HWOffsetFix<1,0,UNSIGNED> id702x_1;

    id702out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id702in_a,id702in_b,(&(id702x_1))));
    id702out_result_doubt = (or_fixed((neq_fixed((id702x_1),(c_hw_fix_1_0_uns_bits_1))),id702in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id704out_o;
  HWOffsetFix<1,0,UNSIGNED> id704out_o_doubt;

  { // Node ID: 704 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id704in_i = id702out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id704in_i_doubt = id702out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id704x_1;

    id704out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id704in_i,(&(id704x_1))));
    id704out_o_doubt = (or_fixed((neq_fixed((id704x_1),(c_hw_fix_1_0_uns_bits_1))),id704in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id713out_output;

  { // Node ID: 713 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id713in_input = id704out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id713in_input_doubt = id704out_o_doubt;

    id713out_output = id713in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id714out_o;

  { // Node ID: 714 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id714in_i = id713out_output;

    id714out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id714in_i));
  }
  { // Node ID: 3064 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3064in_input = id714out_o;

    id3064out_output[(getCycle()+2)%3] = id3064in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id716out_o;

  { // Node ID: 716 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id716in_i = id713out_output;

    id716out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id716in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id781out_output;

  { // Node ID: 781 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id781in_input = id716out_o;

    id781out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id781in_input))));
  }
  { // Node ID: 782 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id782in_addr = id781out_output;

    HWOffsetFix<22,-24,UNSIGNED> id782x_1;

    switch(((long)((id782in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id782x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id782x_1 = (id782sta_rom_store[(id782in_addr.getValueAsLong())]);
        break;
      default:
        id782x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id782out_dout[(getCycle()+2)%3] = (id782x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id715out_o;

  { // Node ID: 715 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id715in_i = id713out_output;

    id715out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id715in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id778out_output;

  { // Node ID: 778 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id778in_input = id715out_o;

    id778out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id778in_input))));
  }
  { // Node ID: 779 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id779in_addr = id778out_output;

    HWOffsetFix<24,-24,UNSIGNED> id779x_1;

    switch(((long)((id779in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id779x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id779x_1 = (id779sta_rom_store[(id779in_addr.getValueAsLong())]);
        break;
      default:
        id779x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id779out_dout[(getCycle()+2)%3] = (id779x_1);
  }
  { // Node ID: 720 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id717out_o;

  { // Node ID: 717 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id717in_i = id713out_output;

    id717out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id717in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id719out_result;

  { // Node ID: 719 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id719in_a = id720out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id719in_b = id717out_o;

    id719out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id719in_a,id719in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id721out_o;

  { // Node ID: 721 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id721in_i = id719out_result;

    id721out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id721in_i));
  }
  { // Node ID: 3065 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3065in_input = id721out_o;

    id3065out_output[(getCycle()+2)%3] = id3065in_input;
  }
  { // Node ID: 3368 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id706out_result;

  { // Node ID: 706 (NodeGt)
    const HWFloat<8,24> &id706in_a = id697out_result;
    const HWFloat<8,24> &id706in_b = id3368out_value;

    id706out_result = (gt_float(id706in_a,id706in_b));
  }
  { // Node ID: 3067 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3067in_input = id706out_result;

    id3067out_output[(getCycle()+6)%7] = id3067in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id707out_output;

  { // Node ID: 707 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id707in_input = id704out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id707in_input_doubt = id704out_o_doubt;

    id707out_output = id707in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id708out_result;

  { // Node ID: 708 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id708in_a = id3067out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id708in_b = id707out_output;

    HWOffsetFix<1,0,UNSIGNED> id708x_1;

    (id708x_1) = (and_fixed(id708in_a,id708in_b));
    id708out_result = (id708x_1);
  }
  { // Node ID: 3068 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3068in_input = id708out_result;

    id3068out_output[(getCycle()+2)%3] = id3068in_input;
  }
  { // Node ID: 3367 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id710out_result;

  { // Node ID: 710 (NodeLt)
    const HWFloat<8,24> &id710in_a = id697out_result;
    const HWFloat<8,24> &id710in_b = id3367out_value;

    id710out_result = (lt_float(id710in_a,id710in_b));
  }
  { // Node ID: 3069 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3069in_input = id710out_result;

    id3069out_output[(getCycle()+6)%7] = id3069in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id711out_output;

  { // Node ID: 711 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id711in_input = id704out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id711in_input_doubt = id704out_o_doubt;

    id711out_output = id711in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id712out_result;

  { // Node ID: 712 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id712in_a = id3069out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id712in_b = id711out_output;

    HWOffsetFix<1,0,UNSIGNED> id712x_1;

    (id712x_1) = (and_fixed(id712in_a,id712in_b));
    id712out_result = (id712x_1);
  }
  { // Node ID: 3070 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3070in_input = id712out_result;

    id3070out_output[(getCycle()+2)%3] = id3070in_input;
  }
  { // Node ID: 3363 (NodeConstantRawBits)
  }
  { // Node ID: 2722 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2722in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2722in_b = id3363out_value;

    id2722out_result[(getCycle()+1)%2] = (eq_fixed(id2722in_a,id2722in_b));
  }
  HWFloat<8,24> id2908out_output;

  { // Node ID: 2908 (NodeStreamOffset)
    const HWFloat<8,24> &id2908in_input = id1825out_result;

    id2908out_output = id2908in_input;
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 45 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id45in_sel = id2722out_result[getCycle()%2];
    const HWFloat<8,24> &id45in_option0 = id2908out_output;
    const HWFloat<8,24> &id45in_option1 = id44out_j_in;

    HWFloat<8,24> id45x_1;

    switch((id45in_sel.getValueAsLong())) {
      case 0l:
        id45x_1 = id45in_option0;
        break;
      case 1l:
        id45x_1 = id45in_option1;
        break;
      default:
        id45x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id45out_result[(getCycle()+1)%2] = (id45x_1);
  }
  { // Node ID: 3097 (NodeFIFO)
    const HWFloat<8,24> &id3097in_input = id45out_result[getCycle()%2];

    id3097out_output[(getCycle()+9)%10] = id3097in_input;
  }
  { // Node ID: 3361 (NodeConstantRawBits)
  }
  HWFloat<8,24> id789out_result;

  { // Node ID: 789 (NodeAdd)
    const HWFloat<8,24> &id789in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id789in_b = id3361out_value;

    id789out_result = (add_float(id789in_a,id789in_b));
  }
  { // Node ID: 2894 (NodePO2FPMult)
    const HWFloat<8,24> &id2894in_floatIn = id789out_result;

    id2894out_floatOut[(getCycle()+1)%2] = (mul_float(id2894in_floatIn,(c_hw_flt_8_24_n0_25val)));
  }
  HWRawBits<8> id863out_result;

  { // Node ID: 863 (NodeSlice)
    const HWFloat<8,24> &id863in_a = id2894out_floatOut[getCycle()%2];

    id863out_result = (slice<23,8>(id863in_a));
  }
  { // Node ID: 864 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2723out_result;

  { // Node ID: 2723 (NodeEqInlined)
    const HWRawBits<8> &id2723in_a = id863out_result;
    const HWRawBits<8> &id2723in_b = id864out_value;

    id2723out_result = (eq_bits(id2723in_a,id2723in_b));
  }
  HWRawBits<23> id862out_result;

  { // Node ID: 862 (NodeSlice)
    const HWFloat<8,24> &id862in_a = id2894out_floatOut[getCycle()%2];

    id862out_result = (slice<0,23>(id862in_a));
  }
  { // Node ID: 3360 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2724out_result;

  { // Node ID: 2724 (NodeNeqInlined)
    const HWRawBits<23> &id2724in_a = id862out_result;
    const HWRawBits<23> &id2724in_b = id3360out_value;

    id2724out_result = (neq_bits(id2724in_a,id2724in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id868out_result;

  { // Node ID: 868 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id868in_a = id2723out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id868in_b = id2724out_result;

    HWOffsetFix<1,0,UNSIGNED> id868x_1;

    (id868x_1) = (and_fixed(id868in_a,id868in_b));
    id868out_result = (id868x_1);
  }
  { // Node ID: 3085 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3085in_input = id868out_result;

    id3085out_output[(getCycle()+8)%9] = id3085in_input;
  }
  { // Node ID: 792 (NodeConstantRawBits)
  }
  HWFloat<8,24> id793out_output;
  HWOffsetFix<1,0,UNSIGNED> id793out_output_doubt;

  { // Node ID: 793 (NodeDoubtBitOp)
    const HWFloat<8,24> &id793in_input = id2894out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id793in_doubt = id792out_value;

    id793out_output = id793in_input;
    id793out_output_doubt = id793in_doubt;
  }
  { // Node ID: 794 (NodeCast)
    const HWFloat<8,24> &id794in_i = id793out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id794in_i_doubt = id793out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id794x_1;

    id794out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id794in_i,(&(id794x_1))));
    id794out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id794x_1),(c_hw_fix_4_0_uns_bits))),id794in_i_doubt));
  }
  { // Node ID: 797 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id796out_result;
  HWOffsetFix<1,0,UNSIGNED> id796out_result_doubt;

  { // Node ID: 796 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id796in_a = id794out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id796in_a_doubt = id794out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id796in_b = id797out_value;

    HWOffsetFix<1,0,UNSIGNED> id796x_1;

    id796out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id796in_a,id796in_b,(&(id796x_1))));
    id796out_result_doubt = (or_fixed((neq_fixed((id796x_1),(c_hw_fix_1_0_uns_bits_1))),id796in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id798out_o;
  HWOffsetFix<1,0,UNSIGNED> id798out_o_doubt;

  { // Node ID: 798 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id798in_i = id796out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id798in_i_doubt = id796out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id798x_1;

    id798out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id798in_i,(&(id798x_1))));
    id798out_o_doubt = (or_fixed((neq_fixed((id798x_1),(c_hw_fix_1_0_uns_bits_1))),id798in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id807out_output;

  { // Node ID: 807 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id807in_input = id798out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id807in_input_doubt = id798out_o_doubt;

    id807out_output = id807in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id808out_o;

  { // Node ID: 808 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id808in_i = id807out_output;

    id808out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id808in_i));
  }
  { // Node ID: 3076 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3076in_input = id808out_o;

    id3076out_output[(getCycle()+2)%3] = id3076in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id810out_o;

  { // Node ID: 810 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id810in_i = id807out_output;

    id810out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id810in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id875out_output;

  { // Node ID: 875 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id875in_input = id810out_o;

    id875out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id875in_input))));
  }
  { // Node ID: 876 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id876in_addr = id875out_output;

    HWOffsetFix<22,-24,UNSIGNED> id876x_1;

    switch(((long)((id876in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id876x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id876x_1 = (id876sta_rom_store[(id876in_addr.getValueAsLong())]);
        break;
      default:
        id876x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id876out_dout[(getCycle()+2)%3] = (id876x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id809out_o;

  { // Node ID: 809 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id809in_i = id807out_output;

    id809out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id809in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id872out_output;

  { // Node ID: 872 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id872in_input = id809out_o;

    id872out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id872in_input))));
  }
  { // Node ID: 873 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id873in_addr = id872out_output;

    HWOffsetFix<24,-24,UNSIGNED> id873x_1;

    switch(((long)((id873in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id873x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id873x_1 = (id873sta_rom_store[(id873in_addr.getValueAsLong())]);
        break;
      default:
        id873x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id873out_dout[(getCycle()+2)%3] = (id873x_1);
  }
  { // Node ID: 814 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id811out_o;

  { // Node ID: 811 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id811in_i = id807out_output;

    id811out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id811in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id813out_result;

  { // Node ID: 813 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id813in_a = id814out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id813in_b = id811out_o;

    id813out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id813in_a,id813in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id815out_o;

  { // Node ID: 815 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id815in_i = id813out_result;

    id815out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id815in_i));
  }
  { // Node ID: 3077 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3077in_input = id815out_o;

    id3077out_output[(getCycle()+2)%3] = id3077in_input;
  }
  { // Node ID: 3358 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id800out_result;

  { // Node ID: 800 (NodeGt)
    const HWFloat<8,24> &id800in_a = id2894out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id800in_b = id3358out_value;

    id800out_result = (gt_float(id800in_a,id800in_b));
  }
  { // Node ID: 3079 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3079in_input = id800out_result;

    id3079out_output[(getCycle()+6)%7] = id3079in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id801out_output;

  { // Node ID: 801 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id801in_input = id798out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id801in_input_doubt = id798out_o_doubt;

    id801out_output = id801in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id802out_result;

  { // Node ID: 802 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id802in_a = id3079out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id802in_b = id801out_output;

    HWOffsetFix<1,0,UNSIGNED> id802x_1;

    (id802x_1) = (and_fixed(id802in_a,id802in_b));
    id802out_result = (id802x_1);
  }
  { // Node ID: 3080 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3080in_input = id802out_result;

    id3080out_output[(getCycle()+2)%3] = id3080in_input;
  }
  { // Node ID: 3357 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id804out_result;

  { // Node ID: 804 (NodeLt)
    const HWFloat<8,24> &id804in_a = id2894out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id804in_b = id3357out_value;

    id804out_result = (lt_float(id804in_a,id804in_b));
  }
  { // Node ID: 3081 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3081in_input = id804out_result;

    id3081out_output[(getCycle()+6)%7] = id3081in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id805out_output;

  { // Node ID: 805 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id805in_input = id798out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id805in_input_doubt = id798out_o_doubt;

    id805out_output = id805in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id806out_result;

  { // Node ID: 806 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id806in_a = id3081out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id806in_b = id805out_output;

    HWOffsetFix<1,0,UNSIGNED> id806x_1;

    (id806x_1) = (and_fixed(id806in_a,id806in_b));
    id806out_result = (id806x_1);
  }
  { // Node ID: 3082 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3082in_input = id806out_result;

    id3082out_output[(getCycle()+2)%3] = id3082in_input;
  }
  { // Node ID: 3354 (NodeConstantRawBits)
  }
  { // Node ID: 3353 (NodeConstantRawBits)
  }
  HWFloat<8,24> id880out_result;

  { // Node ID: 880 (NodeAdd)
    const HWFloat<8,24> &id880in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id880in_b = id3353out_value;

    id880out_result = (add_float(id880in_a,id880in_b));
  }
  HWFloat<8,24> id882out_result;

  { // Node ID: 882 (NodeMul)
    const HWFloat<8,24> &id882in_a = id3354out_value;
    const HWFloat<8,24> &id882in_b = id880out_result;

    id882out_result = (mul_float(id882in_a,id882in_b));
  }
  HWRawBits<8> id954out_result;

  { // Node ID: 954 (NodeSlice)
    const HWFloat<8,24> &id954in_a = id882out_result;

    id954out_result = (slice<23,8>(id954in_a));
  }
  { // Node ID: 955 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2730out_result;

  { // Node ID: 2730 (NodeEqInlined)
    const HWRawBits<8> &id2730in_a = id954out_result;
    const HWRawBits<8> &id2730in_b = id955out_value;

    id2730out_result = (eq_bits(id2730in_a,id2730in_b));
  }
  HWRawBits<23> id953out_result;

  { // Node ID: 953 (NodeSlice)
    const HWFloat<8,24> &id953in_a = id882out_result;

    id953out_result = (slice<0,23>(id953in_a));
  }
  { // Node ID: 3352 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2731out_result;

  { // Node ID: 2731 (NodeNeqInlined)
    const HWRawBits<23> &id2731in_a = id953out_result;
    const HWRawBits<23> &id2731in_b = id3352out_value;

    id2731out_result = (neq_bits(id2731in_a,id2731in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id959out_result;

  { // Node ID: 959 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id959in_a = id2730out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id959in_b = id2731out_result;

    HWOffsetFix<1,0,UNSIGNED> id959x_1;

    (id959x_1) = (and_fixed(id959in_a,id959in_b));
    id959out_result = (id959x_1);
  }
  { // Node ID: 3096 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3096in_input = id959out_result;

    id3096out_output[(getCycle()+8)%9] = id3096in_input;
  }
  { // Node ID: 883 (NodeConstantRawBits)
  }
  HWFloat<8,24> id884out_output;
  HWOffsetFix<1,0,UNSIGNED> id884out_output_doubt;

  { // Node ID: 884 (NodeDoubtBitOp)
    const HWFloat<8,24> &id884in_input = id882out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id884in_doubt = id883out_value;

    id884out_output = id884in_input;
    id884out_output_doubt = id884in_doubt;
  }
  { // Node ID: 885 (NodeCast)
    const HWFloat<8,24> &id885in_i = id884out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id885in_i_doubt = id884out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id885x_1;

    id885out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id885in_i,(&(id885x_1))));
    id885out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id885x_1),(c_hw_fix_4_0_uns_bits))),id885in_i_doubt));
  }
  { // Node ID: 888 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id887out_result;
  HWOffsetFix<1,0,UNSIGNED> id887out_result_doubt;

  { // Node ID: 887 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id887in_a = id885out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id887in_a_doubt = id885out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id887in_b = id888out_value;

    HWOffsetFix<1,0,UNSIGNED> id887x_1;

    id887out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id887in_a,id887in_b,(&(id887x_1))));
    id887out_result_doubt = (or_fixed((neq_fixed((id887x_1),(c_hw_fix_1_0_uns_bits_1))),id887in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id889out_o;
  HWOffsetFix<1,0,UNSIGNED> id889out_o_doubt;

  { // Node ID: 889 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id889in_i = id887out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id889in_i_doubt = id887out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id889x_1;

    id889out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id889in_i,(&(id889x_1))));
    id889out_o_doubt = (or_fixed((neq_fixed((id889x_1),(c_hw_fix_1_0_uns_bits_1))),id889in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id898out_output;

  { // Node ID: 898 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id898in_input = id889out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id898in_input_doubt = id889out_o_doubt;

    id898out_output = id898in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id899out_o;

  { // Node ID: 899 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id899in_i = id898out_output;

    id899out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id899in_i));
  }
  { // Node ID: 3087 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3087in_input = id899out_o;

    id3087out_output[(getCycle()+2)%3] = id3087in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id901out_o;

  { // Node ID: 901 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id901in_i = id898out_output;

    id901out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id901in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id966out_output;

  { // Node ID: 966 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id966in_input = id901out_o;

    id966out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id966in_input))));
  }
  { // Node ID: 967 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id967in_addr = id966out_output;

    HWOffsetFix<22,-24,UNSIGNED> id967x_1;

    switch(((long)((id967in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id967x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id967x_1 = (id967sta_rom_store[(id967in_addr.getValueAsLong())]);
        break;
      default:
        id967x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id967out_dout[(getCycle()+2)%3] = (id967x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id900out_o;

  { // Node ID: 900 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id900in_i = id898out_output;

    id900out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id900in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id963out_output;

  { // Node ID: 963 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id963in_input = id900out_o;

    id963out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id963in_input))));
  }
  { // Node ID: 964 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id964in_addr = id963out_output;

    HWOffsetFix<24,-24,UNSIGNED> id964x_1;

    switch(((long)((id964in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id964x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id964x_1 = (id964sta_rom_store[(id964in_addr.getValueAsLong())]);
        break;
      default:
        id964x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id964out_dout[(getCycle()+2)%3] = (id964x_1);
  }
  { // Node ID: 905 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id902out_o;

  { // Node ID: 902 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id902in_i = id898out_output;

    id902out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id902in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id904out_result;

  { // Node ID: 904 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id904in_a = id905out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id904in_b = id902out_o;

    id904out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id904in_a,id904in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id906out_o;

  { // Node ID: 906 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id906in_i = id904out_result;

    id906out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id906in_i));
  }
  { // Node ID: 3088 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3088in_input = id906out_o;

    id3088out_output[(getCycle()+2)%3] = id3088in_input;
  }
  { // Node ID: 3350 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id891out_result;

  { // Node ID: 891 (NodeGt)
    const HWFloat<8,24> &id891in_a = id882out_result;
    const HWFloat<8,24> &id891in_b = id3350out_value;

    id891out_result = (gt_float(id891in_a,id891in_b));
  }
  { // Node ID: 3090 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3090in_input = id891out_result;

    id3090out_output[(getCycle()+6)%7] = id3090in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id892out_output;

  { // Node ID: 892 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id892in_input = id889out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id892in_input_doubt = id889out_o_doubt;

    id892out_output = id892in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id893out_result;

  { // Node ID: 893 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id893in_a = id3090out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id893in_b = id892out_output;

    HWOffsetFix<1,0,UNSIGNED> id893x_1;

    (id893x_1) = (and_fixed(id893in_a,id893in_b));
    id893out_result = (id893x_1);
  }
  { // Node ID: 3091 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3091in_input = id893out_result;

    id3091out_output[(getCycle()+2)%3] = id3091in_input;
  }
  { // Node ID: 3349 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id895out_result;

  { // Node ID: 895 (NodeLt)
    const HWFloat<8,24> &id895in_a = id882out_result;
    const HWFloat<8,24> &id895in_b = id3349out_value;

    id895out_result = (lt_float(id895in_a,id895in_b));
  }
  { // Node ID: 3092 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3092in_input = id895out_result;

    id3092out_output[(getCycle()+6)%7] = id3092in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id896out_output;

  { // Node ID: 896 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id896in_input = id889out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id896in_input_doubt = id889out_o_doubt;

    id896out_output = id896in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id897out_result;

  { // Node ID: 897 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id897in_a = id3092out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id897in_b = id896out_output;

    HWOffsetFix<1,0,UNSIGNED> id897x_1;

    (id897x_1) = (and_fixed(id897in_a,id897in_b));
    id897out_result = (id897x_1);
  }
  { // Node ID: 3093 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3093in_input = id897out_result;

    id3093out_output[(getCycle()+2)%3] = id3093in_input;
  }
  { // Node ID: 3343 (NodeConstantRawBits)
  }
  { // Node ID: 3342 (NodeConstantRawBits)
  }
  HWFloat<8,24> id973out_result;

  { // Node ID: 973 (NodeAdd)
    const HWFloat<8,24> &id973in_a = id2924out_output[getCycle()%2];
    const HWFloat<8,24> &id973in_b = id3342out_value;

    id973out_result = (add_float(id973in_a,id973in_b));
  }
  HWFloat<8,24> id975out_result;

  { // Node ID: 975 (NodeMul)
    const HWFloat<8,24> &id975in_a = id3343out_value;
    const HWFloat<8,24> &id975in_b = id973out_result;

    id975out_result = (mul_float(id975in_a,id975in_b));
  }
  HWRawBits<8> id1047out_result;

  { // Node ID: 1047 (NodeSlice)
    const HWFloat<8,24> &id1047in_a = id975out_result;

    id1047out_result = (slice<23,8>(id1047in_a));
  }
  { // Node ID: 1048 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2737out_result;

  { // Node ID: 2737 (NodeEqInlined)
    const HWRawBits<8> &id2737in_a = id1047out_result;
    const HWRawBits<8> &id2737in_b = id1048out_value;

    id2737out_result = (eq_bits(id2737in_a,id2737in_b));
  }
  HWRawBits<23> id1046out_result;

  { // Node ID: 1046 (NodeSlice)
    const HWFloat<8,24> &id1046in_a = id975out_result;

    id1046out_result = (slice<0,23>(id1046in_a));
  }
  { // Node ID: 3341 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2738out_result;

  { // Node ID: 2738 (NodeNeqInlined)
    const HWRawBits<23> &id2738in_a = id1046out_result;
    const HWRawBits<23> &id2738in_b = id3341out_value;

    id2738out_result = (neq_bits(id2738in_a,id2738in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1052out_result;

  { // Node ID: 1052 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1052in_a = id2737out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1052in_b = id2738out_result;

    HWOffsetFix<1,0,UNSIGNED> id1052x_1;

    (id1052x_1) = (and_fixed(id1052in_a,id1052in_b));
    id1052out_result = (id1052x_1);
  }
  { // Node ID: 3108 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3108in_input = id1052out_result;

    id3108out_output[(getCycle()+8)%9] = id3108in_input;
  }
  { // Node ID: 976 (NodeConstantRawBits)
  }
  HWFloat<8,24> id977out_output;
  HWOffsetFix<1,0,UNSIGNED> id977out_output_doubt;

  { // Node ID: 977 (NodeDoubtBitOp)
    const HWFloat<8,24> &id977in_input = id975out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id977in_doubt = id976out_value;

    id977out_output = id977in_input;
    id977out_output_doubt = id977in_doubt;
  }
  { // Node ID: 978 (NodeCast)
    const HWFloat<8,24> &id978in_i = id977out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id978in_i_doubt = id977out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id978x_1;

    id978out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id978in_i,(&(id978x_1))));
    id978out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id978x_1),(c_hw_fix_4_0_uns_bits))),id978in_i_doubt));
  }
  { // Node ID: 981 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id980out_result;
  HWOffsetFix<1,0,UNSIGNED> id980out_result_doubt;

  { // Node ID: 980 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id980in_a = id978out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id980in_a_doubt = id978out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id980in_b = id981out_value;

    HWOffsetFix<1,0,UNSIGNED> id980x_1;

    id980out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id980in_a,id980in_b,(&(id980x_1))));
    id980out_result_doubt = (or_fixed((neq_fixed((id980x_1),(c_hw_fix_1_0_uns_bits_1))),id980in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id982out_o;
  HWOffsetFix<1,0,UNSIGNED> id982out_o_doubt;

  { // Node ID: 982 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id982in_i = id980out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id982in_i_doubt = id980out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id982x_1;

    id982out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id982in_i,(&(id982x_1))));
    id982out_o_doubt = (or_fixed((neq_fixed((id982x_1),(c_hw_fix_1_0_uns_bits_1))),id982in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id991out_output;

  { // Node ID: 991 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id991in_input = id982out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id991in_input_doubt = id982out_o_doubt;

    id991out_output = id991in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id992out_o;

  { // Node ID: 992 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id992in_i = id991out_output;

    id992out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id992in_i));
  }
  { // Node ID: 3099 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3099in_input = id992out_o;

    id3099out_output[(getCycle()+2)%3] = id3099in_input;
  }
  HWOffsetFix<10,-12,UNSIGNED> id994out_o;

  { // Node ID: 994 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id994in_i = id991out_output;

    id994out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id994in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1059out_output;

  { // Node ID: 1059 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1059in_input = id994out_o;

    id1059out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1059in_input))));
  }
  { // Node ID: 1060 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1060in_addr = id1059out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1060x_1;

    switch(((long)((id1060in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1060x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1060x_1 = (id1060sta_rom_store[(id1060in_addr.getValueAsLong())]);
        break;
      default:
        id1060x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1060out_dout[(getCycle()+2)%3] = (id1060x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id993out_o;

  { // Node ID: 993 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id993in_i = id991out_output;

    id993out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id993in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1056out_output;

  { // Node ID: 1056 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1056in_input = id993out_o;

    id1056out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1056in_input))));
  }
  { // Node ID: 1057 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1057in_addr = id1056out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1057x_1;

    switch(((long)((id1057in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1057x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1057x_1 = (id1057sta_rom_store[(id1057in_addr.getValueAsLong())]);
        break;
      default:
        id1057x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1057out_dout[(getCycle()+2)%3] = (id1057x_1);
  }
  { // Node ID: 998 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id995out_o;

  { // Node ID: 995 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id995in_i = id991out_output;

    id995out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id995in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id997out_result;

  { // Node ID: 997 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id997in_a = id998out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id997in_b = id995out_o;

    id997out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id997in_a,id997in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id999out_o;

  { // Node ID: 999 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id999in_i = id997out_result;

    id999out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id999in_i));
  }
  { // Node ID: 3100 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3100in_input = id999out_o;

    id3100out_output[(getCycle()+2)%3] = id3100in_input;
  }
  { // Node ID: 3339 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id984out_result;

  { // Node ID: 984 (NodeGt)
    const HWFloat<8,24> &id984in_a = id975out_result;
    const HWFloat<8,24> &id984in_b = id3339out_value;

    id984out_result = (gt_float(id984in_a,id984in_b));
  }
  { // Node ID: 3102 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3102in_input = id984out_result;

    id3102out_output[(getCycle()+6)%7] = id3102in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id985out_output;

  { // Node ID: 985 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id985in_input = id982out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id985in_input_doubt = id982out_o_doubt;

    id985out_output = id985in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id986out_result;

  { // Node ID: 986 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id986in_a = id3102out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id986in_b = id985out_output;

    HWOffsetFix<1,0,UNSIGNED> id986x_1;

    (id986x_1) = (and_fixed(id986in_a,id986in_b));
    id986out_result = (id986x_1);
  }
  { // Node ID: 3103 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3103in_input = id986out_result;

    id3103out_output[(getCycle()+2)%3] = id3103in_input;
  }
  { // Node ID: 3338 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id988out_result;

  { // Node ID: 988 (NodeLt)
    const HWFloat<8,24> &id988in_a = id975out_result;
    const HWFloat<8,24> &id988in_b = id3338out_value;

    id988out_result = (lt_float(id988in_a,id988in_b));
  }
  { // Node ID: 3104 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3104in_input = id988out_result;

    id3104out_output[(getCycle()+6)%7] = id3104in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id989out_output;

  { // Node ID: 989 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id989in_input = id982out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id989in_input_doubt = id982out_o_doubt;

    id989out_output = id989in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id990out_result;

  { // Node ID: 990 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id990in_a = id3104out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id990in_b = id989out_output;

    HWOffsetFix<1,0,UNSIGNED> id990x_1;

    (id990x_1) = (and_fixed(id990in_a,id990in_b));
    id990out_result = (id990x_1);
  }
  { // Node ID: 3105 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3105in_input = id990out_result;

    id3105out_output[(getCycle()+2)%3] = id3105in_input;
  }
  { // Node ID: 3333 (NodeConstantRawBits)
  }
  { // Node ID: 2744 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2744in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2744in_b = id3333out_value;

    id2744out_result[(getCycle()+1)%2] = (eq_fixed(id2744in_a,id2744in_b));
  }
  HWFloat<8,24> id2909out_output;

  { // Node ID: 2909 (NodeStreamOffset)
    const HWFloat<8,24> &id2909in_input = id3112out_output[getCycle()%2];

    id2909out_output = id2909in_input;
  }
  { // Node ID: 40 (NodeInputMappedReg)
  }
  { // Node ID: 41 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id41in_sel = id2744out_result[getCycle()%2];
    const HWFloat<8,24> &id41in_option0 = id2909out_output;
    const HWFloat<8,24> &id41in_option1 = id40out_d_in;

    HWFloat<8,24> id41x_1;

    switch((id41in_sel.getValueAsLong())) {
      case 0l:
        id41x_1 = id41in_option0;
        break;
      case 1l:
        id41x_1 = id41in_option1;
        break;
      default:
        id41x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id41out_result[(getCycle()+1)%2] = (id41x_1);
  }
  { // Node ID: 3133 (NodeFIFO)
    const HWFloat<8,24> &id3133in_input = id41out_result[getCycle()%2];

    id3133out_output[(getCycle()+8)%9] = id3133in_input;
  }
  { // Node ID: 3332 (NodeConstantRawBits)
  }
  { // Node ID: 3331 (NodeConstantRawBits)
  }
  { // Node ID: 3330 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1067out_result;

  { // Node ID: 1067 (NodeSub)
    const HWFloat<8,24> &id1067in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1067in_b = id3330out_value;

    id1067out_result = (sub_float(id1067in_a,id1067in_b));
  }
  HWFloat<8,24> id1069out_result;

  { // Node ID: 1069 (NodeMul)
    const HWFloat<8,24> &id1069in_a = id3331out_value;
    const HWFloat<8,24> &id1069in_b = id1067out_result;

    id1069out_result = (mul_float(id1069in_a,id1069in_b));
  }
  HWRawBits<8> id1141out_result;

  { // Node ID: 1141 (NodeSlice)
    const HWFloat<8,24> &id1141in_a = id1069out_result;

    id1141out_result = (slice<23,8>(id1141in_a));
  }
  { // Node ID: 1142 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2745out_result;

  { // Node ID: 2745 (NodeEqInlined)
    const HWRawBits<8> &id2745in_a = id1141out_result;
    const HWRawBits<8> &id2745in_b = id1142out_value;

    id2745out_result = (eq_bits(id2745in_a,id2745in_b));
  }
  HWRawBits<23> id1140out_result;

  { // Node ID: 1140 (NodeSlice)
    const HWFloat<8,24> &id1140in_a = id1069out_result;

    id1140out_result = (slice<0,23>(id1140in_a));
  }
  { // Node ID: 3329 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2746out_result;

  { // Node ID: 2746 (NodeNeqInlined)
    const HWRawBits<23> &id2746in_a = id1140out_result;
    const HWRawBits<23> &id2746in_b = id3329out_value;

    id2746out_result = (neq_bits(id2746in_a,id2746in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1146out_result;

  { // Node ID: 1146 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1146in_a = id2745out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1146in_b = id2746out_result;

    HWOffsetFix<1,0,UNSIGNED> id1146x_1;

    (id1146x_1) = (and_fixed(id1146in_a,id1146in_b));
    id1146out_result = (id1146x_1);
  }
  { // Node ID: 3122 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3122in_input = id1146out_result;

    id3122out_output[(getCycle()+8)%9] = id3122in_input;
  }
  { // Node ID: 1070 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1071out_output;
  HWOffsetFix<1,0,UNSIGNED> id1071out_output_doubt;

  { // Node ID: 1071 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1071in_input = id1069out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1071in_doubt = id1070out_value;

    id1071out_output = id1071in_input;
    id1071out_output_doubt = id1071in_doubt;
  }
  { // Node ID: 1072 (NodeCast)
    const HWFloat<8,24> &id1072in_i = id1071out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1072in_i_doubt = id1071out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1072x_1;

    id1072out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1072in_i,(&(id1072x_1))));
    id1072out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1072x_1),(c_hw_fix_4_0_uns_bits))),id1072in_i_doubt));
  }
  { // Node ID: 1075 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1074out_result;
  HWOffsetFix<1,0,UNSIGNED> id1074out_result_doubt;

  { // Node ID: 1074 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1074in_a = id1072out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1074in_a_doubt = id1072out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1074in_b = id1075out_value;

    HWOffsetFix<1,0,UNSIGNED> id1074x_1;

    id1074out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1074in_a,id1074in_b,(&(id1074x_1))));
    id1074out_result_doubt = (or_fixed((neq_fixed((id1074x_1),(c_hw_fix_1_0_uns_bits_1))),id1074in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1076out_o;
  HWOffsetFix<1,0,UNSIGNED> id1076out_o_doubt;

  { // Node ID: 1076 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1076in_i = id1074out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1076in_i_doubt = id1074out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1076x_1;

    id1076out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1076in_i,(&(id1076x_1))));
    id1076out_o_doubt = (or_fixed((neq_fixed((id1076x_1),(c_hw_fix_1_0_uns_bits_1))),id1076in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1085out_output;

  { // Node ID: 1085 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1085in_input = id1076out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1085in_input_doubt = id1076out_o_doubt;

    id1085out_output = id1085in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1086out_o;

  { // Node ID: 1086 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1086in_i = id1085out_output;

    id1086out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1086in_i));
  }
  { // Node ID: 3113 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3113in_input = id1086out_o;

    id3113out_output[(getCycle()+2)%3] = id3113in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1108out_o;

  { // Node ID: 1108 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1108in_i = id3113out_output[getCycle()%3];

    id1108out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1108in_i));
  }
  { // Node ID: 1111 (NodeConstantRawBits)
  }
  { // Node ID: 2848 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1088out_o;

  { // Node ID: 1088 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1088in_i = id1085out_output;

    id1088out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1088in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1153out_output;

  { // Node ID: 1153 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1153in_input = id1088out_o;

    id1153out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1153in_input))));
  }
  { // Node ID: 1154 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1154in_addr = id1153out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1154x_1;

    switch(((long)((id1154in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1154x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1154x_1 = (id1154sta_rom_store[(id1154in_addr.getValueAsLong())]);
        break;
      default:
        id1154x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1154out_dout[(getCycle()+2)%3] = (id1154x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1087out_o;

  { // Node ID: 1087 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1087in_i = id1085out_output;

    id1087out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1087in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1150out_output;

  { // Node ID: 1150 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1150in_input = id1087out_o;

    id1150out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1150in_input))));
  }
  { // Node ID: 1151 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1151in_addr = id1150out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1151x_1;

    switch(((long)((id1151in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1151x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1151x_1 = (id1151sta_rom_store[(id1151in_addr.getValueAsLong())]);
        break;
      default:
        id1151x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1151out_dout[(getCycle()+2)%3] = (id1151x_1);
  }
  { // Node ID: 1092 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1089out_o;

  { // Node ID: 1089 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1089in_i = id1085out_output;

    id1089out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1089in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1091out_result;

  { // Node ID: 1091 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1091in_a = id1092out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1091in_b = id1089out_o;

    id1091out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1091in_a,id1091in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1093out_o;

  { // Node ID: 1093 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1093in_i = id1091out_result;

    id1093out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1093in_i));
  }
  { // Node ID: 3114 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3114in_input = id1093out_o;

    id3114out_output[(getCycle()+2)%3] = id3114in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1094out_result;

  { // Node ID: 1094 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1094in_a = id1151out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1094in_b = id3114out_output[getCycle()%3];

    id1094out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1094in_a,id1094in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1095out_result;

  { // Node ID: 1095 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1095in_a = id3114out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1095in_b = id1151out_dout[getCycle()%3];

    id1095out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1095in_a,id1095in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1096out_result;

  { // Node ID: 1096 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1096in_a = id1094out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1096in_b = id1095out_result;

    id1096out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1096in_a,id1096in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1097out_o;

  { // Node ID: 1097 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1097in_i = id1096out_result;

    id1097out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1097in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1098out_result;

  { // Node ID: 1098 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1098in_a = id1154out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1098in_b = id1097out_o;

    id1098out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1098in_a,id1098in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1099out_result;

  { // Node ID: 1099 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1099in_a = id1097out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1099in_b = id1154out_dout[getCycle()%3];

    id1099out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1099in_a,id1099in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1100out_result;

  { // Node ID: 1100 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1100in_a = id1098out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1100in_b = id1099out_result;

    id1100out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1100in_a,id1100in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1101out_o;

  { // Node ID: 1101 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1101in_i = id1100out_result;

    id1101out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1101in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1102out_o;

  { // Node ID: 1102 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1102in_i = id1101out_o;

    id1102out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1102in_i));
  }
  { // Node ID: 3328 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2747out_result;

  { // Node ID: 2747 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2747in_a = id1102out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2747in_b = id3328out_value;

    id2747out_result = (gte_fixed(id2747in_a,id2747in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2873out_result;

  { // Node ID: 2873 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2873in_a = id1108out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2873in_b = id1111out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2873in_c = id2848out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2873in_condb = id2747out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2873x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2873x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2873x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2873x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2873x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2873x_1 = id2873in_a;
        break;
      default:
        id2873x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2873in_condb.getValueAsLong())) {
      case 0l:
        id2873x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2873x_2 = id2873in_b;
        break;
      default:
        id2873x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2873x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2873x_3 = id2873in_c;
        break;
      default:
        id2873x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2873x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2873x_1),(id2873x_2))),(id2873x_3)));
    id2873out_result = (id2873x_4);
  }
  HWRawBits<1> id2748out_result;

  { // Node ID: 2748 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2748in_a = id2873out_result;

    id2748out_result = (slice<10,1>(id2748in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2749out_output;

  { // Node ID: 2749 (NodeReinterpret)
    const HWRawBits<1> &id2749in_input = id2748out_result;

    id2749out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2749in_input));
  }
  { // Node ID: 3327 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1078out_result;

  { // Node ID: 1078 (NodeGt)
    const HWFloat<8,24> &id1078in_a = id1069out_result;
    const HWFloat<8,24> &id1078in_b = id3327out_value;

    id1078out_result = (gt_float(id1078in_a,id1078in_b));
  }
  { // Node ID: 3116 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3116in_input = id1078out_result;

    id3116out_output[(getCycle()+6)%7] = id3116in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1079out_output;

  { // Node ID: 1079 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1079in_input = id1076out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1079in_input_doubt = id1076out_o_doubt;

    id1079out_output = id1079in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1080out_result;

  { // Node ID: 1080 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1080in_a = id3116out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1080in_b = id1079out_output;

    HWOffsetFix<1,0,UNSIGNED> id1080x_1;

    (id1080x_1) = (and_fixed(id1080in_a,id1080in_b));
    id1080out_result = (id1080x_1);
  }
  { // Node ID: 3117 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3117in_input = id1080out_result;

    id3117out_output[(getCycle()+2)%3] = id3117in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1117out_result;

  { // Node ID: 1117 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1117in_a = id3117out_output[getCycle()%3];

    id1117out_result = (not_fixed(id1117in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1118out_result;

  { // Node ID: 1118 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_a = id2749out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_b = id1117out_result;

    HWOffsetFix<1,0,UNSIGNED> id1118x_1;

    (id1118x_1) = (and_fixed(id1118in_a,id1118in_b));
    id1118out_result = (id1118x_1);
  }
  { // Node ID: 3326 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1082out_result;

  { // Node ID: 1082 (NodeLt)
    const HWFloat<8,24> &id1082in_a = id1069out_result;
    const HWFloat<8,24> &id1082in_b = id3326out_value;

    id1082out_result = (lt_float(id1082in_a,id1082in_b));
  }
  { // Node ID: 3118 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3118in_input = id1082out_result;

    id3118out_output[(getCycle()+6)%7] = id3118in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1083out_output;

  { // Node ID: 1083 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1083in_input = id1076out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1083in_input_doubt = id1076out_o_doubt;

    id1083out_output = id1083in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1084out_result;

  { // Node ID: 1084 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1084in_a = id3118out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1084in_b = id1083out_output;

    HWOffsetFix<1,0,UNSIGNED> id1084x_1;

    (id1084x_1) = (and_fixed(id1084in_a,id1084in_b));
    id1084out_result = (id1084x_1);
  }
  { // Node ID: 3119 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3119in_input = id1084out_result;

    id3119out_output[(getCycle()+2)%3] = id3119in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1119out_result;

  { // Node ID: 1119 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_a = id1118out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_b = id3119out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1119x_1;

    (id1119x_1) = (or_fixed(id1119in_a,id1119in_b));
    id1119out_result = (id1119x_1);
  }
  { // Node ID: 3325 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2750out_result;

  { // Node ID: 2750 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2750in_a = id2873out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2750in_b = id3325out_value;

    id2750out_result = (gte_fixed(id2750in_a,id2750in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1126out_result;

  { // Node ID: 1126 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1126in_a = id3119out_output[getCycle()%3];

    id1126out_result = (not_fixed(id1126in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1127out_result;

  { // Node ID: 1127 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1127in_a = id2750out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1127in_b = id1126out_result;

    HWOffsetFix<1,0,UNSIGNED> id1127x_1;

    (id1127x_1) = (and_fixed(id1127in_a,id1127in_b));
    id1127out_result = (id1127x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1128out_result;

  { // Node ID: 1128 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1128in_a = id1127out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1128in_b = id3117out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1128x_1;

    (id1128x_1) = (or_fixed(id1128in_a,id1128in_b));
    id1128out_result = (id1128x_1);
  }
  HWRawBits<2> id1129out_result;

  { // Node ID: 1129 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1129in_in0 = id1119out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1129in_in1 = id1128out_result;

    id1129out_result = (cat(id1129in_in0,id1129in_in1));
  }
  { // Node ID: 1121 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1120out_o;

  { // Node ID: 1120 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1120in_i = id2873out_result;

    id1120out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1120in_i));
  }
  { // Node ID: 1105 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1106out_result;

  { // Node ID: 1106 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1106in_sel = id2747out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1106in_option0 = id1102out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1106in_option1 = id1105out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1106x_1;

    switch((id1106in_sel.getValueAsLong())) {
      case 0l:
        id1106x_1 = id1106in_option0;
        break;
      case 1l:
        id1106x_1 = id1106in_option1;
        break;
      default:
        id1106x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1106out_result = (id1106x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1107out_o;

  { // Node ID: 1107 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1107in_i = id1106out_result;

    id1107out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1107in_i));
  }
  HWRawBits<32> id1122out_result;

  { // Node ID: 1122 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1122in_in0 = id1121out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1122in_in1 = id1120out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1122in_in2 = id1107out_o;

    id1122out_result = (cat((cat(id1122in_in0,id1122in_in1)),id1122in_in2));
  }
  HWFloat<8,24> id1123out_output;

  { // Node ID: 1123 (NodeReinterpret)
    const HWRawBits<32> &id1123in_input = id1122out_result;

    id1123out_output = (cast_bits2float<8,24>(id1123in_input));
  }
  { // Node ID: 1130 (NodeConstantRawBits)
  }
  { // Node ID: 1131 (NodeConstantRawBits)
  }
  { // Node ID: 1133 (NodeConstantRawBits)
  }
  HWRawBits<32> id2751out_result;

  { // Node ID: 2751 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2751in_in0 = id1130out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2751in_in1 = id1131out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2751in_in2 = id1133out_value;

    id2751out_result = (cat((cat(id2751in_in0,id2751in_in1)),id2751in_in2));
  }
  HWFloat<8,24> id1135out_output;

  { // Node ID: 1135 (NodeReinterpret)
    const HWRawBits<32> &id1135in_input = id2751out_result;

    id1135out_output = (cast_bits2float<8,24>(id1135in_input));
  }
  { // Node ID: 2611 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1138out_result;

  { // Node ID: 1138 (NodeMux)
    const HWRawBits<2> &id1138in_sel = id1129out_result;
    const HWFloat<8,24> &id1138in_option0 = id1123out_output;
    const HWFloat<8,24> &id1138in_option1 = id1135out_output;
    const HWFloat<8,24> &id1138in_option2 = id2611out_value;
    const HWFloat<8,24> &id1138in_option3 = id1135out_output;

    HWFloat<8,24> id1138x_1;

    switch((id1138in_sel.getValueAsLong())) {
      case 0l:
        id1138x_1 = id1138in_option0;
        break;
      case 1l:
        id1138x_1 = id1138in_option1;
        break;
      case 2l:
        id1138x_1 = id1138in_option2;
        break;
      case 3l:
        id1138x_1 = id1138in_option3;
        break;
      default:
        id1138x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1138out_result = (id1138x_1);
  }
  { // Node ID: 3324 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1148out_result;

  { // Node ID: 1148 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1148in_sel = id3122out_output[getCycle()%9];
    const HWFloat<8,24> &id1148in_option0 = id1138out_result;
    const HWFloat<8,24> &id1148in_option1 = id3324out_value;

    HWFloat<8,24> id1148x_1;

    switch((id1148in_sel.getValueAsLong())) {
      case 0l:
        id1148x_1 = id1148in_option0;
        break;
      case 1l:
        id1148x_1 = id1148in_option1;
        break;
      default:
        id1148x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1148out_result = (id1148x_1);
  }
  HWFloat<8,24> id1156out_result;

  { // Node ID: 1156 (NodeMul)
    const HWFloat<8,24> &id1156in_a = id3332out_value;
    const HWFloat<8,24> &id1156in_b = id1148out_result;

    id1156out_result = (mul_float(id1156in_a,id1156in_b));
  }
  { // Node ID: 3323 (NodeConstantRawBits)
  }
  { // Node ID: 3322 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1158out_result;

  { // Node ID: 1158 (NodeSub)
    const HWFloat<8,24> &id1158in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1158in_b = id3322out_value;

    id1158out_result = (sub_float(id1158in_a,id1158in_b));
  }
  HWFloat<8,24> id1160out_result;

  { // Node ID: 1160 (NodeMul)
    const HWFloat<8,24> &id1160in_a = id3323out_value;
    const HWFloat<8,24> &id1160in_b = id1158out_result;

    id1160out_result = (mul_float(id1160in_a,id1160in_b));
  }
  HWRawBits<8> id1232out_result;

  { // Node ID: 1232 (NodeSlice)
    const HWFloat<8,24> &id1232in_a = id1160out_result;

    id1232out_result = (slice<23,8>(id1232in_a));
  }
  { // Node ID: 1233 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2752out_result;

  { // Node ID: 2752 (NodeEqInlined)
    const HWRawBits<8> &id2752in_a = id1232out_result;
    const HWRawBits<8> &id2752in_b = id1233out_value;

    id2752out_result = (eq_bits(id2752in_a,id2752in_b));
  }
  HWRawBits<23> id1231out_result;

  { // Node ID: 1231 (NodeSlice)
    const HWFloat<8,24> &id1231in_a = id1160out_result;

    id1231out_result = (slice<0,23>(id1231in_a));
  }
  { // Node ID: 3321 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2753out_result;

  { // Node ID: 2753 (NodeNeqInlined)
    const HWRawBits<23> &id2753in_a = id1231out_result;
    const HWRawBits<23> &id2753in_b = id3321out_value;

    id2753out_result = (neq_bits(id2753in_a,id2753in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1237out_result;

  { // Node ID: 1237 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1237in_a = id2752out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1237in_b = id2753out_result;

    HWOffsetFix<1,0,UNSIGNED> id1237x_1;

    (id1237x_1) = (and_fixed(id1237in_a,id1237in_b));
    id1237out_result = (id1237x_1);
  }
  { // Node ID: 3132 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3132in_input = id1237out_result;

    id3132out_output[(getCycle()+8)%9] = id3132in_input;
  }
  { // Node ID: 1161 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1162out_output;
  HWOffsetFix<1,0,UNSIGNED> id1162out_output_doubt;

  { // Node ID: 1162 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1162in_input = id1160out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1162in_doubt = id1161out_value;

    id1162out_output = id1162in_input;
    id1162out_output_doubt = id1162in_doubt;
  }
  { // Node ID: 1163 (NodeCast)
    const HWFloat<8,24> &id1163in_i = id1162out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1163in_i_doubt = id1162out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1163x_1;

    id1163out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1163in_i,(&(id1163x_1))));
    id1163out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1163x_1),(c_hw_fix_4_0_uns_bits))),id1163in_i_doubt));
  }
  { // Node ID: 1166 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1165out_result;
  HWOffsetFix<1,0,UNSIGNED> id1165out_result_doubt;

  { // Node ID: 1165 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1165in_a = id1163out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1165in_a_doubt = id1163out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1165in_b = id1166out_value;

    HWOffsetFix<1,0,UNSIGNED> id1165x_1;

    id1165out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1165in_a,id1165in_b,(&(id1165x_1))));
    id1165out_result_doubt = (or_fixed((neq_fixed((id1165x_1),(c_hw_fix_1_0_uns_bits_1))),id1165in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1167out_o;
  HWOffsetFix<1,0,UNSIGNED> id1167out_o_doubt;

  { // Node ID: 1167 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1167in_i = id1165out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1167in_i_doubt = id1165out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1167x_1;

    id1167out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1167in_i,(&(id1167x_1))));
    id1167out_o_doubt = (or_fixed((neq_fixed((id1167x_1),(c_hw_fix_1_0_uns_bits_1))),id1167in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1176out_output;

  { // Node ID: 1176 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1176in_input = id1167out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1176in_input_doubt = id1167out_o_doubt;

    id1176out_output = id1176in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1177out_o;

  { // Node ID: 1177 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1177in_i = id1176out_output;

    id1177out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1177in_i));
  }
  { // Node ID: 3123 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3123in_input = id1177out_o;

    id3123out_output[(getCycle()+2)%3] = id3123in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1199out_o;

  { // Node ID: 1199 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1199in_i = id3123out_output[getCycle()%3];

    id1199out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1199in_i));
  }
  { // Node ID: 1202 (NodeConstantRawBits)
  }
  { // Node ID: 2850 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1179out_o;

  { // Node ID: 1179 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1179in_i = id1176out_output;

    id1179out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1179in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1244out_output;

  { // Node ID: 1244 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1244in_input = id1179out_o;

    id1244out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1244in_input))));
  }
  { // Node ID: 1245 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1245in_addr = id1244out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1245x_1;

    switch(((long)((id1245in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1245x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1245x_1 = (id1245sta_rom_store[(id1245in_addr.getValueAsLong())]);
        break;
      default:
        id1245x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1245out_dout[(getCycle()+2)%3] = (id1245x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1178out_o;

  { // Node ID: 1178 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1178in_i = id1176out_output;

    id1178out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1178in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1241out_output;

  { // Node ID: 1241 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1241in_input = id1178out_o;

    id1241out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1241in_input))));
  }
  { // Node ID: 1242 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1242in_addr = id1241out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1242x_1;

    switch(((long)((id1242in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1242x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1242x_1 = (id1242sta_rom_store[(id1242in_addr.getValueAsLong())]);
        break;
      default:
        id1242x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1242out_dout[(getCycle()+2)%3] = (id1242x_1);
  }
  { // Node ID: 1183 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1180out_o;

  { // Node ID: 1180 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1180in_i = id1176out_output;

    id1180out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1180in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1182out_result;

  { // Node ID: 1182 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1182in_a = id1183out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1182in_b = id1180out_o;

    id1182out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1182in_a,id1182in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1184out_o;

  { // Node ID: 1184 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1184in_i = id1182out_result;

    id1184out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1184in_i));
  }
  { // Node ID: 3124 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3124in_input = id1184out_o;

    id3124out_output[(getCycle()+2)%3] = id3124in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1185out_result;

  { // Node ID: 1185 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1185in_a = id1242out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1185in_b = id3124out_output[getCycle()%3];

    id1185out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1185in_a,id1185in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1186out_result;

  { // Node ID: 1186 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1186in_a = id3124out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1186in_b = id1242out_dout[getCycle()%3];

    id1186out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1186in_a,id1186in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1187out_result;

  { // Node ID: 1187 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1187in_a = id1185out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1187in_b = id1186out_result;

    id1187out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1187in_a,id1187in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1188out_o;

  { // Node ID: 1188 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1188in_i = id1187out_result;

    id1188out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1188in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1189out_result;

  { // Node ID: 1189 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1189in_a = id1245out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1189in_b = id1188out_o;

    id1189out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1189in_a,id1189in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1190out_result;

  { // Node ID: 1190 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1190in_a = id1188out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1190in_b = id1245out_dout[getCycle()%3];

    id1190out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1190in_a,id1190in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1191out_result;

  { // Node ID: 1191 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1191in_a = id1189out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1191in_b = id1190out_result;

    id1191out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1191in_a,id1191in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1192out_o;

  { // Node ID: 1192 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1192in_i = id1191out_result;

    id1192out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1192in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1193out_o;

  { // Node ID: 1193 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1193in_i = id1192out_o;

    id1193out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1193in_i));
  }
  { // Node ID: 3320 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2754out_result;

  { // Node ID: 2754 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2754in_a = id1193out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2754in_b = id3320out_value;

    id2754out_result = (gte_fixed(id2754in_a,id2754in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2872out_result;

  { // Node ID: 2872 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2872in_a = id1199out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2872in_b = id1202out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2872in_c = id2850out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2872in_condb = id2754out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2872x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2872x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2872x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2872x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2872x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2872x_1 = id2872in_a;
        break;
      default:
        id2872x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2872in_condb.getValueAsLong())) {
      case 0l:
        id2872x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2872x_2 = id2872in_b;
        break;
      default:
        id2872x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2872x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2872x_3 = id2872in_c;
        break;
      default:
        id2872x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2872x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2872x_1),(id2872x_2))),(id2872x_3)));
    id2872out_result = (id2872x_4);
  }
  HWRawBits<1> id2755out_result;

  { // Node ID: 2755 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2755in_a = id2872out_result;

    id2755out_result = (slice<10,1>(id2755in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2756out_output;

  { // Node ID: 2756 (NodeReinterpret)
    const HWRawBits<1> &id2756in_input = id2755out_result;

    id2756out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2756in_input));
  }
  { // Node ID: 3319 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1169out_result;

  { // Node ID: 1169 (NodeGt)
    const HWFloat<8,24> &id1169in_a = id1160out_result;
    const HWFloat<8,24> &id1169in_b = id3319out_value;

    id1169out_result = (gt_float(id1169in_a,id1169in_b));
  }
  { // Node ID: 3126 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3126in_input = id1169out_result;

    id3126out_output[(getCycle()+6)%7] = id3126in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1170out_output;

  { // Node ID: 1170 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1170in_input = id1167out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1170in_input_doubt = id1167out_o_doubt;

    id1170out_output = id1170in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1171out_result;

  { // Node ID: 1171 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1171in_a = id3126out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1171in_b = id1170out_output;

    HWOffsetFix<1,0,UNSIGNED> id1171x_1;

    (id1171x_1) = (and_fixed(id1171in_a,id1171in_b));
    id1171out_result = (id1171x_1);
  }
  { // Node ID: 3127 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3127in_input = id1171out_result;

    id3127out_output[(getCycle()+2)%3] = id3127in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1208out_result;

  { // Node ID: 1208 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1208in_a = id3127out_output[getCycle()%3];

    id1208out_result = (not_fixed(id1208in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1209out_result;

  { // Node ID: 1209 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1209in_a = id2756out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1209in_b = id1208out_result;

    HWOffsetFix<1,0,UNSIGNED> id1209x_1;

    (id1209x_1) = (and_fixed(id1209in_a,id1209in_b));
    id1209out_result = (id1209x_1);
  }
  { // Node ID: 3318 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1173out_result;

  { // Node ID: 1173 (NodeLt)
    const HWFloat<8,24> &id1173in_a = id1160out_result;
    const HWFloat<8,24> &id1173in_b = id3318out_value;

    id1173out_result = (lt_float(id1173in_a,id1173in_b));
  }
  { // Node ID: 3128 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3128in_input = id1173out_result;

    id3128out_output[(getCycle()+6)%7] = id3128in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1174out_output;

  { // Node ID: 1174 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1174in_input = id1167out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1174in_input_doubt = id1167out_o_doubt;

    id1174out_output = id1174in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1175out_result;

  { // Node ID: 1175 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1175in_a = id3128out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1175in_b = id1174out_output;

    HWOffsetFix<1,0,UNSIGNED> id1175x_1;

    (id1175x_1) = (and_fixed(id1175in_a,id1175in_b));
    id1175out_result = (id1175x_1);
  }
  { // Node ID: 3129 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3129in_input = id1175out_result;

    id3129out_output[(getCycle()+2)%3] = id3129in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1210out_result;

  { // Node ID: 1210 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1210in_a = id1209out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1210in_b = id3129out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1210x_1;

    (id1210x_1) = (or_fixed(id1210in_a,id1210in_b));
    id1210out_result = (id1210x_1);
  }
  { // Node ID: 3317 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2757out_result;

  { // Node ID: 2757 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2757in_a = id2872out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2757in_b = id3317out_value;

    id2757out_result = (gte_fixed(id2757in_a,id2757in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1217out_result;

  { // Node ID: 1217 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1217in_a = id3129out_output[getCycle()%3];

    id1217out_result = (not_fixed(id1217in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1218out_result;

  { // Node ID: 1218 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1218in_a = id2757out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1218in_b = id1217out_result;

    HWOffsetFix<1,0,UNSIGNED> id1218x_1;

    (id1218x_1) = (and_fixed(id1218in_a,id1218in_b));
    id1218out_result = (id1218x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1219out_result;

  { // Node ID: 1219 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1219in_a = id1218out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1219in_b = id3127out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1219x_1;

    (id1219x_1) = (or_fixed(id1219in_a,id1219in_b));
    id1219out_result = (id1219x_1);
  }
  HWRawBits<2> id1220out_result;

  { // Node ID: 1220 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1220in_in0 = id1210out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1220in_in1 = id1219out_result;

    id1220out_result = (cat(id1220in_in0,id1220in_in1));
  }
  { // Node ID: 1212 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1211out_o;

  { // Node ID: 1211 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1211in_i = id2872out_result;

    id1211out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1211in_i));
  }
  { // Node ID: 1196 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1197out_result;

  { // Node ID: 1197 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1197in_sel = id2754out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1197in_option0 = id1193out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1197in_option1 = id1196out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1197x_1;

    switch((id1197in_sel.getValueAsLong())) {
      case 0l:
        id1197x_1 = id1197in_option0;
        break;
      case 1l:
        id1197x_1 = id1197in_option1;
        break;
      default:
        id1197x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1197out_result = (id1197x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1198out_o;

  { // Node ID: 1198 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1198in_i = id1197out_result;

    id1198out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1198in_i));
  }
  HWRawBits<32> id1213out_result;

  { // Node ID: 1213 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1213in_in0 = id1212out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1213in_in1 = id1211out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1213in_in2 = id1198out_o;

    id1213out_result = (cat((cat(id1213in_in0,id1213in_in1)),id1213in_in2));
  }
  HWFloat<8,24> id1214out_output;

  { // Node ID: 1214 (NodeReinterpret)
    const HWRawBits<32> &id1214in_input = id1213out_result;

    id1214out_output = (cast_bits2float<8,24>(id1214in_input));
  }
  { // Node ID: 1221 (NodeConstantRawBits)
  }
  { // Node ID: 1222 (NodeConstantRawBits)
  }
  { // Node ID: 1224 (NodeConstantRawBits)
  }
  HWRawBits<32> id2758out_result;

  { // Node ID: 2758 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2758in_in0 = id1221out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2758in_in1 = id1222out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2758in_in2 = id1224out_value;

    id2758out_result = (cat((cat(id2758in_in0,id2758in_in1)),id2758in_in2));
  }
  HWFloat<8,24> id1226out_output;

  { // Node ID: 1226 (NodeReinterpret)
    const HWRawBits<32> &id1226in_input = id2758out_result;

    id1226out_output = (cast_bits2float<8,24>(id1226in_input));
  }
  { // Node ID: 2612 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1229out_result;

  { // Node ID: 1229 (NodeMux)
    const HWRawBits<2> &id1229in_sel = id1220out_result;
    const HWFloat<8,24> &id1229in_option0 = id1214out_output;
    const HWFloat<8,24> &id1229in_option1 = id1226out_output;
    const HWFloat<8,24> &id1229in_option2 = id2612out_value;
    const HWFloat<8,24> &id1229in_option3 = id1226out_output;

    HWFloat<8,24> id1229x_1;

    switch((id1229in_sel.getValueAsLong())) {
      case 0l:
        id1229x_1 = id1229in_option0;
        break;
      case 1l:
        id1229x_1 = id1229in_option1;
        break;
      case 2l:
        id1229x_1 = id1229in_option2;
        break;
      case 3l:
        id1229x_1 = id1229in_option3;
        break;
      default:
        id1229x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1229out_result = (id1229x_1);
  }
  { // Node ID: 3316 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1239out_result;

  { // Node ID: 1239 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1239in_sel = id3132out_output[getCycle()%9];
    const HWFloat<8,24> &id1239in_option0 = id1229out_result;
    const HWFloat<8,24> &id1239in_option1 = id3316out_value;

    HWFloat<8,24> id1239x_1;

    switch((id1239in_sel.getValueAsLong())) {
      case 0l:
        id1239x_1 = id1239in_option0;
        break;
      case 1l:
        id1239x_1 = id1239in_option1;
        break;
      default:
        id1239x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1239out_result = (id1239x_1);
  }
  { // Node ID: 3315 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1247out_result;

  { // Node ID: 1247 (NodeAdd)
    const HWFloat<8,24> &id1247in_a = id1239out_result;
    const HWFloat<8,24> &id1247in_b = id3315out_value;

    id1247out_result = (add_float(id1247in_a,id1247in_b));
  }
  HWFloat<8,24> id1248out_result;

  { // Node ID: 1248 (NodeDiv)
    const HWFloat<8,24> &id1248in_a = id1156out_result;
    const HWFloat<8,24> &id1248in_b = id1247out_result;

    id1248out_result = (div_float(id1248in_a,id1248in_b));
  }
  HWFloat<8,24> id1249out_result;

  { // Node ID: 1249 (NodeMul)
    const HWFloat<8,24> &id1249in_a = id12out_dt;
    const HWFloat<8,24> &id1249in_b = id1248out_result;

    id1249out_result = (mul_float(id1249in_a,id1249in_b));
  }
  { // Node ID: 3314 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1827out_result;

  { // Node ID: 1827 (NodeSub)
    const HWFloat<8,24> &id1827in_a = id3314out_value;
    const HWFloat<8,24> &id1827in_b = id3133out_output[getCycle()%9];

    id1827out_result = (sub_float(id1827in_a,id1827in_b));
  }
  HWFloat<8,24> id1828out_result;

  { // Node ID: 1828 (NodeMul)
    const HWFloat<8,24> &id1828in_a = id1249out_result;
    const HWFloat<8,24> &id1828in_b = id1827out_result;

    id1828out_result = (mul_float(id1828in_a,id1828in_b));
  }
  { // Node ID: 3313 (NodeConstantRawBits)
  }
  { // Node ID: 3312 (NodeConstantRawBits)
  }
  { // Node ID: 3311 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1251out_result;

  { // Node ID: 1251 (NodeAdd)
    const HWFloat<8,24> &id1251in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1251in_b = id3311out_value;

    id1251out_result = (add_float(id1251in_a,id1251in_b));
  }
  HWFloat<8,24> id1253out_result;

  { // Node ID: 1253 (NodeMul)
    const HWFloat<8,24> &id1253in_a = id3312out_value;
    const HWFloat<8,24> &id1253in_b = id1251out_result;

    id1253out_result = (mul_float(id1253in_a,id1253in_b));
  }
  HWRawBits<8> id1325out_result;

  { // Node ID: 1325 (NodeSlice)
    const HWFloat<8,24> &id1325in_a = id1253out_result;

    id1325out_result = (slice<23,8>(id1325in_a));
  }
  { // Node ID: 1326 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2759out_result;

  { // Node ID: 2759 (NodeEqInlined)
    const HWRawBits<8> &id2759in_a = id1325out_result;
    const HWRawBits<8> &id2759in_b = id1326out_value;

    id2759out_result = (eq_bits(id2759in_a,id2759in_b));
  }
  HWRawBits<23> id1324out_result;

  { // Node ID: 1324 (NodeSlice)
    const HWFloat<8,24> &id1324in_a = id1253out_result;

    id1324out_result = (slice<0,23>(id1324in_a));
  }
  { // Node ID: 3310 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2760out_result;

  { // Node ID: 2760 (NodeNeqInlined)
    const HWRawBits<23> &id2760in_a = id1324out_result;
    const HWRawBits<23> &id2760in_b = id3310out_value;

    id2760out_result = (neq_bits(id2760in_a,id2760in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1330out_result;

  { // Node ID: 1330 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1330in_a = id2759out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1330in_b = id2760out_result;

    HWOffsetFix<1,0,UNSIGNED> id1330x_1;

    (id1330x_1) = (and_fixed(id1330in_a,id1330in_b));
    id1330out_result = (id1330x_1);
  }
  { // Node ID: 3143 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3143in_input = id1330out_result;

    id3143out_output[(getCycle()+8)%9] = id3143in_input;
  }
  { // Node ID: 1254 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1255out_output;
  HWOffsetFix<1,0,UNSIGNED> id1255out_output_doubt;

  { // Node ID: 1255 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1255in_input = id1253out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1255in_doubt = id1254out_value;

    id1255out_output = id1255in_input;
    id1255out_output_doubt = id1255in_doubt;
  }
  { // Node ID: 1256 (NodeCast)
    const HWFloat<8,24> &id1256in_i = id1255out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1256in_i_doubt = id1255out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1256x_1;

    id1256out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1256in_i,(&(id1256x_1))));
    id1256out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1256x_1),(c_hw_fix_4_0_uns_bits))),id1256in_i_doubt));
  }
  { // Node ID: 1259 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1258out_result;
  HWOffsetFix<1,0,UNSIGNED> id1258out_result_doubt;

  { // Node ID: 1258 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1258in_a = id1256out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1258in_a_doubt = id1256out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1258in_b = id1259out_value;

    HWOffsetFix<1,0,UNSIGNED> id1258x_1;

    id1258out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1258in_a,id1258in_b,(&(id1258x_1))));
    id1258out_result_doubt = (or_fixed((neq_fixed((id1258x_1),(c_hw_fix_1_0_uns_bits_1))),id1258in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1260out_o;
  HWOffsetFix<1,0,UNSIGNED> id1260out_o_doubt;

  { // Node ID: 1260 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1260in_i = id1258out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1260in_i_doubt = id1258out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1260x_1;

    id1260out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1260in_i,(&(id1260x_1))));
    id1260out_o_doubt = (or_fixed((neq_fixed((id1260x_1),(c_hw_fix_1_0_uns_bits_1))),id1260in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1269out_output;

  { // Node ID: 1269 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1269in_input = id1260out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1269in_input_doubt = id1260out_o_doubt;

    id1269out_output = id1269in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1270out_o;

  { // Node ID: 1270 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1270in_i = id1269out_output;

    id1270out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1270in_i));
  }
  { // Node ID: 3134 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3134in_input = id1270out_o;

    id3134out_output[(getCycle()+2)%3] = id3134in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1292out_o;

  { // Node ID: 1292 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1292in_i = id3134out_output[getCycle()%3];

    id1292out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1292in_i));
  }
  { // Node ID: 1295 (NodeConstantRawBits)
  }
  { // Node ID: 2852 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1272out_o;

  { // Node ID: 1272 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1272in_i = id1269out_output;

    id1272out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1272in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1337out_output;

  { // Node ID: 1337 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1337in_input = id1272out_o;

    id1337out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1337in_input))));
  }
  { // Node ID: 1338 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1338in_addr = id1337out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1338x_1;

    switch(((long)((id1338in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1338x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1338x_1 = (id1338sta_rom_store[(id1338in_addr.getValueAsLong())]);
        break;
      default:
        id1338x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1338out_dout[(getCycle()+2)%3] = (id1338x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1271out_o;

  { // Node ID: 1271 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1271in_i = id1269out_output;

    id1271out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1271in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1334out_output;

  { // Node ID: 1334 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1334in_input = id1271out_o;

    id1334out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1334in_input))));
  }
  { // Node ID: 1335 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1335in_addr = id1334out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1335x_1;

    switch(((long)((id1335in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1335x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1335x_1 = (id1335sta_rom_store[(id1335in_addr.getValueAsLong())]);
        break;
      default:
        id1335x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1335out_dout[(getCycle()+2)%3] = (id1335x_1);
  }
  { // Node ID: 1276 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1273out_o;

  { // Node ID: 1273 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1273in_i = id1269out_output;

    id1273out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1273in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1275out_result;

  { // Node ID: 1275 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1275in_a = id1276out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1275in_b = id1273out_o;

    id1275out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1275in_a,id1275in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1277out_o;

  { // Node ID: 1277 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1277in_i = id1275out_result;

    id1277out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1277in_i));
  }
  { // Node ID: 3135 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3135in_input = id1277out_o;

    id3135out_output[(getCycle()+2)%3] = id3135in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1278out_result;

  { // Node ID: 1278 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1278in_a = id1335out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1278in_b = id3135out_output[getCycle()%3];

    id1278out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1278in_a,id1278in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1279out_result;

  { // Node ID: 1279 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1279in_a = id3135out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1279in_b = id1335out_dout[getCycle()%3];

    id1279out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1279in_a,id1279in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1280out_result;

  { // Node ID: 1280 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1280in_a = id1278out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1280in_b = id1279out_result;

    id1280out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1280in_a,id1280in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1281out_o;

  { // Node ID: 1281 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1281in_i = id1280out_result;

    id1281out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1281in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1282out_result;

  { // Node ID: 1282 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1282in_a = id1338out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1282in_b = id1281out_o;

    id1282out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1282in_a,id1282in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1283out_result;

  { // Node ID: 1283 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1283in_a = id1281out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1283in_b = id1338out_dout[getCycle()%3];

    id1283out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1283in_a,id1283in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1284out_result;

  { // Node ID: 1284 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1284in_a = id1282out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1284in_b = id1283out_result;

    id1284out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1284in_a,id1284in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1285out_o;

  { // Node ID: 1285 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1285in_i = id1284out_result;

    id1285out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1285in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1286out_o;

  { // Node ID: 1286 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1286in_i = id1285out_o;

    id1286out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1286in_i));
  }
  { // Node ID: 3309 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2761out_result;

  { // Node ID: 2761 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2761in_a = id1286out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2761in_b = id3309out_value;

    id2761out_result = (gte_fixed(id2761in_a,id2761in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2871out_result;

  { // Node ID: 2871 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2871in_a = id1292out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2871in_b = id1295out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2871in_c = id2852out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2871in_condb = id2761out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2871x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2871x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2871x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2871x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2871x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2871x_1 = id2871in_a;
        break;
      default:
        id2871x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2871in_condb.getValueAsLong())) {
      case 0l:
        id2871x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2871x_2 = id2871in_b;
        break;
      default:
        id2871x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2871x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2871x_3 = id2871in_c;
        break;
      default:
        id2871x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2871x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2871x_1),(id2871x_2))),(id2871x_3)));
    id2871out_result = (id2871x_4);
  }
  HWRawBits<1> id2762out_result;

  { // Node ID: 2762 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2762in_a = id2871out_result;

    id2762out_result = (slice<10,1>(id2762in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2763out_output;

  { // Node ID: 2763 (NodeReinterpret)
    const HWRawBits<1> &id2763in_input = id2762out_result;

    id2763out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2763in_input));
  }
  { // Node ID: 3308 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1262out_result;

  { // Node ID: 1262 (NodeGt)
    const HWFloat<8,24> &id1262in_a = id1253out_result;
    const HWFloat<8,24> &id1262in_b = id3308out_value;

    id1262out_result = (gt_float(id1262in_a,id1262in_b));
  }
  { // Node ID: 3137 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3137in_input = id1262out_result;

    id3137out_output[(getCycle()+6)%7] = id3137in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1263out_output;

  { // Node ID: 1263 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1263in_input = id1260out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1263in_input_doubt = id1260out_o_doubt;

    id1263out_output = id1263in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1264out_result;

  { // Node ID: 1264 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1264in_a = id3137out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1264in_b = id1263out_output;

    HWOffsetFix<1,0,UNSIGNED> id1264x_1;

    (id1264x_1) = (and_fixed(id1264in_a,id1264in_b));
    id1264out_result = (id1264x_1);
  }
  { // Node ID: 3138 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3138in_input = id1264out_result;

    id3138out_output[(getCycle()+2)%3] = id3138in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1301out_result;

  { // Node ID: 1301 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1301in_a = id3138out_output[getCycle()%3];

    id1301out_result = (not_fixed(id1301in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1302out_result;

  { // Node ID: 1302 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1302in_a = id2763out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1302in_b = id1301out_result;

    HWOffsetFix<1,0,UNSIGNED> id1302x_1;

    (id1302x_1) = (and_fixed(id1302in_a,id1302in_b));
    id1302out_result = (id1302x_1);
  }
  { // Node ID: 3307 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1266out_result;

  { // Node ID: 1266 (NodeLt)
    const HWFloat<8,24> &id1266in_a = id1253out_result;
    const HWFloat<8,24> &id1266in_b = id3307out_value;

    id1266out_result = (lt_float(id1266in_a,id1266in_b));
  }
  { // Node ID: 3139 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3139in_input = id1266out_result;

    id3139out_output[(getCycle()+6)%7] = id3139in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1267out_output;

  { // Node ID: 1267 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1267in_input = id1260out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1267in_input_doubt = id1260out_o_doubt;

    id1267out_output = id1267in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1268out_result;

  { // Node ID: 1268 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1268in_a = id3139out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1268in_b = id1267out_output;

    HWOffsetFix<1,0,UNSIGNED> id1268x_1;

    (id1268x_1) = (and_fixed(id1268in_a,id1268in_b));
    id1268out_result = (id1268x_1);
  }
  { // Node ID: 3140 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3140in_input = id1268out_result;

    id3140out_output[(getCycle()+2)%3] = id3140in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1303out_result;

  { // Node ID: 1303 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1303in_a = id1302out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1303in_b = id3140out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1303x_1;

    (id1303x_1) = (or_fixed(id1303in_a,id1303in_b));
    id1303out_result = (id1303x_1);
  }
  { // Node ID: 3306 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2764out_result;

  { // Node ID: 2764 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2764in_a = id2871out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2764in_b = id3306out_value;

    id2764out_result = (gte_fixed(id2764in_a,id2764in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1310out_result;

  { // Node ID: 1310 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1310in_a = id3140out_output[getCycle()%3];

    id1310out_result = (not_fixed(id1310in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1311out_result;

  { // Node ID: 1311 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1311in_a = id2764out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1311in_b = id1310out_result;

    HWOffsetFix<1,0,UNSIGNED> id1311x_1;

    (id1311x_1) = (and_fixed(id1311in_a,id1311in_b));
    id1311out_result = (id1311x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1312out_result;

  { // Node ID: 1312 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1312in_a = id1311out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1312in_b = id3138out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1312x_1;

    (id1312x_1) = (or_fixed(id1312in_a,id1312in_b));
    id1312out_result = (id1312x_1);
  }
  HWRawBits<2> id1313out_result;

  { // Node ID: 1313 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1313in_in0 = id1303out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1313in_in1 = id1312out_result;

    id1313out_result = (cat(id1313in_in0,id1313in_in1));
  }
  { // Node ID: 1305 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1304out_o;

  { // Node ID: 1304 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1304in_i = id2871out_result;

    id1304out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1304in_i));
  }
  { // Node ID: 1289 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1290out_result;

  { // Node ID: 1290 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1290in_sel = id2761out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1290in_option0 = id1286out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1290in_option1 = id1289out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1290x_1;

    switch((id1290in_sel.getValueAsLong())) {
      case 0l:
        id1290x_1 = id1290in_option0;
        break;
      case 1l:
        id1290x_1 = id1290in_option1;
        break;
      default:
        id1290x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1290out_result = (id1290x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1291out_o;

  { // Node ID: 1291 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1291in_i = id1290out_result;

    id1291out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1291in_i));
  }
  HWRawBits<32> id1306out_result;

  { // Node ID: 1306 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1306in_in0 = id1305out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1306in_in1 = id1304out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1306in_in2 = id1291out_o;

    id1306out_result = (cat((cat(id1306in_in0,id1306in_in1)),id1306in_in2));
  }
  HWFloat<8,24> id1307out_output;

  { // Node ID: 1307 (NodeReinterpret)
    const HWRawBits<32> &id1307in_input = id1306out_result;

    id1307out_output = (cast_bits2float<8,24>(id1307in_input));
  }
  { // Node ID: 1314 (NodeConstantRawBits)
  }
  { // Node ID: 1315 (NodeConstantRawBits)
  }
  { // Node ID: 1317 (NodeConstantRawBits)
  }
  HWRawBits<32> id2765out_result;

  { // Node ID: 2765 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2765in_in0 = id1314out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2765in_in1 = id1315out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2765in_in2 = id1317out_value;

    id2765out_result = (cat((cat(id2765in_in0,id2765in_in1)),id2765in_in2));
  }
  HWFloat<8,24> id1319out_output;

  { // Node ID: 1319 (NodeReinterpret)
    const HWRawBits<32> &id1319in_input = id2765out_result;

    id1319out_output = (cast_bits2float<8,24>(id1319in_input));
  }
  { // Node ID: 2613 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1322out_result;

  { // Node ID: 1322 (NodeMux)
    const HWRawBits<2> &id1322in_sel = id1313out_result;
    const HWFloat<8,24> &id1322in_option0 = id1307out_output;
    const HWFloat<8,24> &id1322in_option1 = id1319out_output;
    const HWFloat<8,24> &id1322in_option2 = id2613out_value;
    const HWFloat<8,24> &id1322in_option3 = id1319out_output;

    HWFloat<8,24> id1322x_1;

    switch((id1322in_sel.getValueAsLong())) {
      case 0l:
        id1322x_1 = id1322in_option0;
        break;
      case 1l:
        id1322x_1 = id1322in_option1;
        break;
      case 2l:
        id1322x_1 = id1322in_option2;
        break;
      case 3l:
        id1322x_1 = id1322in_option3;
        break;
      default:
        id1322x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1322out_result = (id1322x_1);
  }
  { // Node ID: 3305 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1332out_result;

  { // Node ID: 1332 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1332in_sel = id3143out_output[getCycle()%9];
    const HWFloat<8,24> &id1332in_option0 = id1322out_result;
    const HWFloat<8,24> &id1332in_option1 = id3305out_value;

    HWFloat<8,24> id1332x_1;

    switch((id1332in_sel.getValueAsLong())) {
      case 0l:
        id1332x_1 = id1332in_option0;
        break;
      case 1l:
        id1332x_1 = id1332in_option1;
        break;
      default:
        id1332x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1332out_result = (id1332x_1);
  }
  HWFloat<8,24> id1340out_result;

  { // Node ID: 1340 (NodeMul)
    const HWFloat<8,24> &id1340in_a = id3313out_value;
    const HWFloat<8,24> &id1340in_b = id1332out_result;

    id1340out_result = (mul_float(id1340in_a,id1340in_b));
  }
  { // Node ID: 3304 (NodeConstantRawBits)
  }
  { // Node ID: 3303 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1342out_result;

  { // Node ID: 1342 (NodeAdd)
    const HWFloat<8,24> &id1342in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1342in_b = id3303out_value;

    id1342out_result = (add_float(id1342in_a,id1342in_b));
  }
  HWFloat<8,24> id1344out_result;

  { // Node ID: 1344 (NodeMul)
    const HWFloat<8,24> &id1344in_a = id3304out_value;
    const HWFloat<8,24> &id1344in_b = id1342out_result;

    id1344out_result = (mul_float(id1344in_a,id1344in_b));
  }
  HWRawBits<8> id1416out_result;

  { // Node ID: 1416 (NodeSlice)
    const HWFloat<8,24> &id1416in_a = id1344out_result;

    id1416out_result = (slice<23,8>(id1416in_a));
  }
  { // Node ID: 1417 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2766out_result;

  { // Node ID: 2766 (NodeEqInlined)
    const HWRawBits<8> &id2766in_a = id1416out_result;
    const HWRawBits<8> &id2766in_b = id1417out_value;

    id2766out_result = (eq_bits(id2766in_a,id2766in_b));
  }
  HWRawBits<23> id1415out_result;

  { // Node ID: 1415 (NodeSlice)
    const HWFloat<8,24> &id1415in_a = id1344out_result;

    id1415out_result = (slice<0,23>(id1415in_a));
  }
  { // Node ID: 3302 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2767out_result;

  { // Node ID: 2767 (NodeNeqInlined)
    const HWRawBits<23> &id2767in_a = id1415out_result;
    const HWRawBits<23> &id2767in_b = id3302out_value;

    id2767out_result = (neq_bits(id2767in_a,id2767in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1421out_result;

  { // Node ID: 1421 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1421in_a = id2766out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1421in_b = id2767out_result;

    HWOffsetFix<1,0,UNSIGNED> id1421x_1;

    (id1421x_1) = (and_fixed(id1421in_a,id1421in_b));
    id1421out_result = (id1421x_1);
  }
  { // Node ID: 3153 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3153in_input = id1421out_result;

    id3153out_output[(getCycle()+8)%9] = id3153in_input;
  }
  { // Node ID: 1345 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1346out_output;
  HWOffsetFix<1,0,UNSIGNED> id1346out_output_doubt;

  { // Node ID: 1346 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1346in_input = id1344out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1346in_doubt = id1345out_value;

    id1346out_output = id1346in_input;
    id1346out_output_doubt = id1346in_doubt;
  }
  { // Node ID: 1347 (NodeCast)
    const HWFloat<8,24> &id1347in_i = id1346out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1347in_i_doubt = id1346out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1347x_1;

    id1347out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1347in_i,(&(id1347x_1))));
    id1347out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1347x_1),(c_hw_fix_4_0_uns_bits))),id1347in_i_doubt));
  }
  { // Node ID: 1350 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1349out_result;
  HWOffsetFix<1,0,UNSIGNED> id1349out_result_doubt;

  { // Node ID: 1349 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1349in_a = id1347out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1349in_a_doubt = id1347out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1349in_b = id1350out_value;

    HWOffsetFix<1,0,UNSIGNED> id1349x_1;

    id1349out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1349in_a,id1349in_b,(&(id1349x_1))));
    id1349out_result_doubt = (or_fixed((neq_fixed((id1349x_1),(c_hw_fix_1_0_uns_bits_1))),id1349in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1351out_o;
  HWOffsetFix<1,0,UNSIGNED> id1351out_o_doubt;

  { // Node ID: 1351 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1351in_i = id1349out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1351in_i_doubt = id1349out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1351x_1;

    id1351out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1351in_i,(&(id1351x_1))));
    id1351out_o_doubt = (or_fixed((neq_fixed((id1351x_1),(c_hw_fix_1_0_uns_bits_1))),id1351in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1360out_output;

  { // Node ID: 1360 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1360in_input = id1351out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1360in_input_doubt = id1351out_o_doubt;

    id1360out_output = id1360in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1361out_o;

  { // Node ID: 1361 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1361in_i = id1360out_output;

    id1361out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1361in_i));
  }
  { // Node ID: 3144 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3144in_input = id1361out_o;

    id3144out_output[(getCycle()+2)%3] = id3144in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1383out_o;

  { // Node ID: 1383 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1383in_i = id3144out_output[getCycle()%3];

    id1383out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1383in_i));
  }
  { // Node ID: 1386 (NodeConstantRawBits)
  }
  { // Node ID: 2854 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1363out_o;

  { // Node ID: 1363 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1363in_i = id1360out_output;

    id1363out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1363in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1428out_output;

  { // Node ID: 1428 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1428in_input = id1363out_o;

    id1428out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1428in_input))));
  }
  { // Node ID: 1429 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1429in_addr = id1428out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1429x_1;

    switch(((long)((id1429in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1429x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1429x_1 = (id1429sta_rom_store[(id1429in_addr.getValueAsLong())]);
        break;
      default:
        id1429x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1429out_dout[(getCycle()+2)%3] = (id1429x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1362out_o;

  { // Node ID: 1362 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1362in_i = id1360out_output;

    id1362out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1362in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1425out_output;

  { // Node ID: 1425 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1425in_input = id1362out_o;

    id1425out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1425in_input))));
  }
  { // Node ID: 1426 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1426in_addr = id1425out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1426x_1;

    switch(((long)((id1426in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1426x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1426x_1 = (id1426sta_rom_store[(id1426in_addr.getValueAsLong())]);
        break;
      default:
        id1426x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1426out_dout[(getCycle()+2)%3] = (id1426x_1);
  }
  { // Node ID: 1367 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1364out_o;

  { // Node ID: 1364 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1364in_i = id1360out_output;

    id1364out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1364in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1366out_result;

  { // Node ID: 1366 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1366in_a = id1367out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1366in_b = id1364out_o;

    id1366out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1366in_a,id1366in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1368out_o;

  { // Node ID: 1368 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1368in_i = id1366out_result;

    id1368out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1368in_i));
  }
  { // Node ID: 3145 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3145in_input = id1368out_o;

    id3145out_output[(getCycle()+2)%3] = id3145in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1369out_result;

  { // Node ID: 1369 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1369in_a = id1426out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1369in_b = id3145out_output[getCycle()%3];

    id1369out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1369in_a,id1369in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1370out_result;

  { // Node ID: 1370 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1370in_a = id3145out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1370in_b = id1426out_dout[getCycle()%3];

    id1370out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1370in_a,id1370in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1371out_result;

  { // Node ID: 1371 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1371in_a = id1369out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1371in_b = id1370out_result;

    id1371out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1371in_a,id1371in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1372out_o;

  { // Node ID: 1372 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1372in_i = id1371out_result;

    id1372out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1372in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1373out_result;

  { // Node ID: 1373 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1373in_a = id1429out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1373in_b = id1372out_o;

    id1373out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1373in_a,id1373in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1374out_result;

  { // Node ID: 1374 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1374in_a = id1372out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1374in_b = id1429out_dout[getCycle()%3];

    id1374out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1374in_a,id1374in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1375out_result;

  { // Node ID: 1375 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1375in_a = id1373out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1375in_b = id1374out_result;

    id1375out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1375in_a,id1375in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1376out_o;

  { // Node ID: 1376 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1376in_i = id1375out_result;

    id1376out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1376in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1377out_o;

  { // Node ID: 1377 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1377in_i = id1376out_o;

    id1377out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1377in_i));
  }
  { // Node ID: 3301 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2768out_result;

  { // Node ID: 2768 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2768in_a = id1377out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2768in_b = id3301out_value;

    id2768out_result = (gte_fixed(id2768in_a,id2768in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2870out_result;

  { // Node ID: 2870 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2870in_a = id1383out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2870in_b = id1386out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2870in_c = id2854out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2870in_condb = id2768out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2870x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2870x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2870x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2870x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2870x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2870x_1 = id2870in_a;
        break;
      default:
        id2870x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2870in_condb.getValueAsLong())) {
      case 0l:
        id2870x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2870x_2 = id2870in_b;
        break;
      default:
        id2870x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2870x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2870x_3 = id2870in_c;
        break;
      default:
        id2870x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2870x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2870x_1),(id2870x_2))),(id2870x_3)));
    id2870out_result = (id2870x_4);
  }
  HWRawBits<1> id2769out_result;

  { // Node ID: 2769 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2769in_a = id2870out_result;

    id2769out_result = (slice<10,1>(id2769in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2770out_output;

  { // Node ID: 2770 (NodeReinterpret)
    const HWRawBits<1> &id2770in_input = id2769out_result;

    id2770out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2770in_input));
  }
  { // Node ID: 3300 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1353out_result;

  { // Node ID: 1353 (NodeGt)
    const HWFloat<8,24> &id1353in_a = id1344out_result;
    const HWFloat<8,24> &id1353in_b = id3300out_value;

    id1353out_result = (gt_float(id1353in_a,id1353in_b));
  }
  { // Node ID: 3147 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3147in_input = id1353out_result;

    id3147out_output[(getCycle()+6)%7] = id3147in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1354out_output;

  { // Node ID: 1354 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1354in_input = id1351out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1354in_input_doubt = id1351out_o_doubt;

    id1354out_output = id1354in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1355out_result;

  { // Node ID: 1355 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1355in_a = id3147out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1355in_b = id1354out_output;

    HWOffsetFix<1,0,UNSIGNED> id1355x_1;

    (id1355x_1) = (and_fixed(id1355in_a,id1355in_b));
    id1355out_result = (id1355x_1);
  }
  { // Node ID: 3148 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3148in_input = id1355out_result;

    id3148out_output[(getCycle()+2)%3] = id3148in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1392out_result;

  { // Node ID: 1392 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1392in_a = id3148out_output[getCycle()%3];

    id1392out_result = (not_fixed(id1392in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1393out_result;

  { // Node ID: 1393 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1393in_a = id2770out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1393in_b = id1392out_result;

    HWOffsetFix<1,0,UNSIGNED> id1393x_1;

    (id1393x_1) = (and_fixed(id1393in_a,id1393in_b));
    id1393out_result = (id1393x_1);
  }
  { // Node ID: 3299 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1357out_result;

  { // Node ID: 1357 (NodeLt)
    const HWFloat<8,24> &id1357in_a = id1344out_result;
    const HWFloat<8,24> &id1357in_b = id3299out_value;

    id1357out_result = (lt_float(id1357in_a,id1357in_b));
  }
  { // Node ID: 3149 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3149in_input = id1357out_result;

    id3149out_output[(getCycle()+6)%7] = id3149in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1358out_output;

  { // Node ID: 1358 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1358in_input = id1351out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1358in_input_doubt = id1351out_o_doubt;

    id1358out_output = id1358in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1359out_result;

  { // Node ID: 1359 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1359in_a = id3149out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1359in_b = id1358out_output;

    HWOffsetFix<1,0,UNSIGNED> id1359x_1;

    (id1359x_1) = (and_fixed(id1359in_a,id1359in_b));
    id1359out_result = (id1359x_1);
  }
  { // Node ID: 3150 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3150in_input = id1359out_result;

    id3150out_output[(getCycle()+2)%3] = id3150in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1394out_result;

  { // Node ID: 1394 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1394in_a = id1393out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1394in_b = id3150out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1394x_1;

    (id1394x_1) = (or_fixed(id1394in_a,id1394in_b));
    id1394out_result = (id1394x_1);
  }
  { // Node ID: 3298 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2771out_result;

  { // Node ID: 2771 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2771in_a = id2870out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2771in_b = id3298out_value;

    id2771out_result = (gte_fixed(id2771in_a,id2771in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1401out_result;

  { // Node ID: 1401 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1401in_a = id3150out_output[getCycle()%3];

    id1401out_result = (not_fixed(id1401in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1402out_result;

  { // Node ID: 1402 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1402in_a = id2771out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1402in_b = id1401out_result;

    HWOffsetFix<1,0,UNSIGNED> id1402x_1;

    (id1402x_1) = (and_fixed(id1402in_a,id1402in_b));
    id1402out_result = (id1402x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1403out_result;

  { // Node ID: 1403 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1403in_a = id1402out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1403in_b = id3148out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1403x_1;

    (id1403x_1) = (or_fixed(id1403in_a,id1403in_b));
    id1403out_result = (id1403x_1);
  }
  HWRawBits<2> id1404out_result;

  { // Node ID: 1404 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1404in_in0 = id1394out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1404in_in1 = id1403out_result;

    id1404out_result = (cat(id1404in_in0,id1404in_in1));
  }
  { // Node ID: 1396 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1395out_o;

  { // Node ID: 1395 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1395in_i = id2870out_result;

    id1395out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1395in_i));
  }
  { // Node ID: 1380 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1381out_result;

  { // Node ID: 1381 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1381in_sel = id2768out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1381in_option0 = id1377out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1381in_option1 = id1380out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1381x_1;

    switch((id1381in_sel.getValueAsLong())) {
      case 0l:
        id1381x_1 = id1381in_option0;
        break;
      case 1l:
        id1381x_1 = id1381in_option1;
        break;
      default:
        id1381x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1381out_result = (id1381x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1382out_o;

  { // Node ID: 1382 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1382in_i = id1381out_result;

    id1382out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1382in_i));
  }
  HWRawBits<32> id1397out_result;

  { // Node ID: 1397 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1397in_in0 = id1396out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1397in_in1 = id1395out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1397in_in2 = id1382out_o;

    id1397out_result = (cat((cat(id1397in_in0,id1397in_in1)),id1397in_in2));
  }
  HWFloat<8,24> id1398out_output;

  { // Node ID: 1398 (NodeReinterpret)
    const HWRawBits<32> &id1398in_input = id1397out_result;

    id1398out_output = (cast_bits2float<8,24>(id1398in_input));
  }
  { // Node ID: 1405 (NodeConstantRawBits)
  }
  { // Node ID: 1406 (NodeConstantRawBits)
  }
  { // Node ID: 1408 (NodeConstantRawBits)
  }
  HWRawBits<32> id2772out_result;

  { // Node ID: 2772 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2772in_in0 = id1405out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2772in_in1 = id1406out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2772in_in2 = id1408out_value;

    id2772out_result = (cat((cat(id2772in_in0,id2772in_in1)),id2772in_in2));
  }
  HWFloat<8,24> id1410out_output;

  { // Node ID: 1410 (NodeReinterpret)
    const HWRawBits<32> &id1410in_input = id2772out_result;

    id1410out_output = (cast_bits2float<8,24>(id1410in_input));
  }
  { // Node ID: 2614 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1413out_result;

  { // Node ID: 1413 (NodeMux)
    const HWRawBits<2> &id1413in_sel = id1404out_result;
    const HWFloat<8,24> &id1413in_option0 = id1398out_output;
    const HWFloat<8,24> &id1413in_option1 = id1410out_output;
    const HWFloat<8,24> &id1413in_option2 = id2614out_value;
    const HWFloat<8,24> &id1413in_option3 = id1410out_output;

    HWFloat<8,24> id1413x_1;

    switch((id1413in_sel.getValueAsLong())) {
      case 0l:
        id1413x_1 = id1413in_option0;
        break;
      case 1l:
        id1413x_1 = id1413in_option1;
        break;
      case 2l:
        id1413x_1 = id1413in_option2;
        break;
      case 3l:
        id1413x_1 = id1413in_option3;
        break;
      default:
        id1413x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1413out_result = (id1413x_1);
  }
  { // Node ID: 3297 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1423out_result;

  { // Node ID: 1423 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1423in_sel = id3153out_output[getCycle()%9];
    const HWFloat<8,24> &id1423in_option0 = id1413out_result;
    const HWFloat<8,24> &id1423in_option1 = id3297out_value;

    HWFloat<8,24> id1423x_1;

    switch((id1423in_sel.getValueAsLong())) {
      case 0l:
        id1423x_1 = id1423in_option0;
        break;
      case 1l:
        id1423x_1 = id1423in_option1;
        break;
      default:
        id1423x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1423out_result = (id1423x_1);
  }
  { // Node ID: 3296 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1431out_result;

  { // Node ID: 1431 (NodeAdd)
    const HWFloat<8,24> &id1431in_a = id1423out_result;
    const HWFloat<8,24> &id1431in_b = id3296out_value;

    id1431out_result = (add_float(id1431in_a,id1431in_b));
  }
  HWFloat<8,24> id1432out_result;

  { // Node ID: 1432 (NodeDiv)
    const HWFloat<8,24> &id1432in_a = id1340out_result;
    const HWFloat<8,24> &id1432in_b = id1431out_result;

    id1432out_result = (div_float(id1432in_a,id1432in_b));
  }
  HWFloat<8,24> id1433out_result;

  { // Node ID: 1433 (NodeMul)
    const HWFloat<8,24> &id1433in_a = id12out_dt;
    const HWFloat<8,24> &id1433in_b = id1432out_result;

    id1433out_result = (mul_float(id1433in_a,id1433in_b));
  }
  HWFloat<8,24> id1829out_result;

  { // Node ID: 1829 (NodeMul)
    const HWFloat<8,24> &id1829in_a = id1433out_result;
    const HWFloat<8,24> &id1829in_b = id3133out_output[getCycle()%9];

    id1829out_result = (mul_float(id1829in_a,id1829in_b));
  }
  HWFloat<8,24> id1830out_result;

  { // Node ID: 1830 (NodeSub)
    const HWFloat<8,24> &id1830in_a = id1828out_result;
    const HWFloat<8,24> &id1830in_b = id1829out_result;

    id1830out_result = (sub_float(id1830in_a,id1830in_b));
  }
  HWFloat<8,24> id1831out_result;

  { // Node ID: 1831 (NodeAdd)
    const HWFloat<8,24> &id1831in_a = id3133out_output[getCycle()%9];
    const HWFloat<8,24> &id1831in_b = id1830out_result;

    id1831out_result = (add_float(id1831in_a,id1831in_b));
  }
  { // Node ID: 3112 (NodeFIFO)
    const HWFloat<8,24> &id3112in_input = id1831out_result;

    id3112out_output[(getCycle()+1)%2] = id3112in_input;
  }
  { // Node ID: 3295 (NodeConstantRawBits)
  }
  { // Node ID: 2773 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2773in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2773in_b = id3295out_value;

    id2773out_result[(getCycle()+1)%2] = (eq_fixed(id2773in_a,id2773in_b));
  }
  HWFloat<8,24> id2910out_output;

  { // Node ID: 2910 (NodeStreamOffset)
    const HWFloat<8,24> &id2910in_input = id3157out_output[getCycle()%2];

    id2910out_output = id2910in_input;
  }
  { // Node ID: 32 (NodeInputMappedReg)
  }
  { // Node ID: 33 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id33in_sel = id2773out_result[getCycle()%2];
    const HWFloat<8,24> &id33in_option0 = id2910out_output;
    const HWFloat<8,24> &id33in_option1 = id32out_f_in;

    HWFloat<8,24> id33x_1;

    switch((id33in_sel.getValueAsLong())) {
      case 0l:
        id33x_1 = id33in_option0;
        break;
      case 1l:
        id33x_1 = id33in_option1;
        break;
      default:
        id33x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id33out_result[(getCycle()+1)%2] = (id33x_1);
  }
  { // Node ID: 3178 (NodeFIFO)
    const HWFloat<8,24> &id3178in_input = id33out_result[getCycle()%2];

    id3178out_output[(getCycle()+8)%9] = id3178in_input;
  }
  { // Node ID: 3294 (NodeConstantRawBits)
  }
  { // Node ID: 3293 (NodeConstantRawBits)
  }
  { // Node ID: 3292 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1435out_result;

  { // Node ID: 1435 (NodeAdd)
    const HWFloat<8,24> &id1435in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1435in_b = id3292out_value;

    id1435out_result = (add_float(id1435in_a,id1435in_b));
  }
  HWFloat<8,24> id1437out_result;

  { // Node ID: 1437 (NodeMul)
    const HWFloat<8,24> &id1437in_a = id3293out_value;
    const HWFloat<8,24> &id1437in_b = id1435out_result;

    id1437out_result = (mul_float(id1437in_a,id1437in_b));
  }
  HWRawBits<8> id1509out_result;

  { // Node ID: 1509 (NodeSlice)
    const HWFloat<8,24> &id1509in_a = id1437out_result;

    id1509out_result = (slice<23,8>(id1509in_a));
  }
  { // Node ID: 1510 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2774out_result;

  { // Node ID: 2774 (NodeEqInlined)
    const HWRawBits<8> &id2774in_a = id1509out_result;
    const HWRawBits<8> &id2774in_b = id1510out_value;

    id2774out_result = (eq_bits(id2774in_a,id2774in_b));
  }
  HWRawBits<23> id1508out_result;

  { // Node ID: 1508 (NodeSlice)
    const HWFloat<8,24> &id1508in_a = id1437out_result;

    id1508out_result = (slice<0,23>(id1508in_a));
  }
  { // Node ID: 3291 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2775out_result;

  { // Node ID: 2775 (NodeNeqInlined)
    const HWRawBits<23> &id2775in_a = id1508out_result;
    const HWRawBits<23> &id2775in_b = id3291out_value;

    id2775out_result = (neq_bits(id2775in_a,id2775in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1514out_result;

  { // Node ID: 1514 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1514in_a = id2774out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1514in_b = id2775out_result;

    HWOffsetFix<1,0,UNSIGNED> id1514x_1;

    (id1514x_1) = (and_fixed(id1514in_a,id1514in_b));
    id1514out_result = (id1514x_1);
  }
  { // Node ID: 3167 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3167in_input = id1514out_result;

    id3167out_output[(getCycle()+8)%9] = id3167in_input;
  }
  { // Node ID: 1438 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1439out_output;
  HWOffsetFix<1,0,UNSIGNED> id1439out_output_doubt;

  { // Node ID: 1439 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1439in_input = id1437out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1439in_doubt = id1438out_value;

    id1439out_output = id1439in_input;
    id1439out_output_doubt = id1439in_doubt;
  }
  { // Node ID: 1440 (NodeCast)
    const HWFloat<8,24> &id1440in_i = id1439out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1440in_i_doubt = id1439out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1440x_1;

    id1440out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1440in_i,(&(id1440x_1))));
    id1440out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1440x_1),(c_hw_fix_4_0_uns_bits))),id1440in_i_doubt));
  }
  { // Node ID: 1443 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1442out_result;
  HWOffsetFix<1,0,UNSIGNED> id1442out_result_doubt;

  { // Node ID: 1442 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1442in_a = id1440out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1442in_a_doubt = id1440out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1442in_b = id1443out_value;

    HWOffsetFix<1,0,UNSIGNED> id1442x_1;

    id1442out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1442in_a,id1442in_b,(&(id1442x_1))));
    id1442out_result_doubt = (or_fixed((neq_fixed((id1442x_1),(c_hw_fix_1_0_uns_bits_1))),id1442in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1444out_o;
  HWOffsetFix<1,0,UNSIGNED> id1444out_o_doubt;

  { // Node ID: 1444 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1444in_i = id1442out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1444in_i_doubt = id1442out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1444x_1;

    id1444out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1444in_i,(&(id1444x_1))));
    id1444out_o_doubt = (or_fixed((neq_fixed((id1444x_1),(c_hw_fix_1_0_uns_bits_1))),id1444in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1453out_output;

  { // Node ID: 1453 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1453in_input = id1444out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1453in_input_doubt = id1444out_o_doubt;

    id1453out_output = id1453in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1454out_o;

  { // Node ID: 1454 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1454in_i = id1453out_output;

    id1454out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1454in_i));
  }
  { // Node ID: 3158 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3158in_input = id1454out_o;

    id3158out_output[(getCycle()+2)%3] = id3158in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1476out_o;

  { // Node ID: 1476 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1476in_i = id3158out_output[getCycle()%3];

    id1476out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1476in_i));
  }
  { // Node ID: 1479 (NodeConstantRawBits)
  }
  { // Node ID: 2856 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1456out_o;

  { // Node ID: 1456 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1456in_i = id1453out_output;

    id1456out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1456in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1521out_output;

  { // Node ID: 1521 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1521in_input = id1456out_o;

    id1521out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1521in_input))));
  }
  { // Node ID: 1522 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1522in_addr = id1521out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1522x_1;

    switch(((long)((id1522in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1522x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1522x_1 = (id1522sta_rom_store[(id1522in_addr.getValueAsLong())]);
        break;
      default:
        id1522x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1522out_dout[(getCycle()+2)%3] = (id1522x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1455out_o;

  { // Node ID: 1455 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1455in_i = id1453out_output;

    id1455out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1455in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1518out_output;

  { // Node ID: 1518 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1518in_input = id1455out_o;

    id1518out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1518in_input))));
  }
  { // Node ID: 1519 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1519in_addr = id1518out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1519x_1;

    switch(((long)((id1519in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1519x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1519x_1 = (id1519sta_rom_store[(id1519in_addr.getValueAsLong())]);
        break;
      default:
        id1519x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1519out_dout[(getCycle()+2)%3] = (id1519x_1);
  }
  { // Node ID: 1460 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1457out_o;

  { // Node ID: 1457 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1457in_i = id1453out_output;

    id1457out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1457in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1459out_result;

  { // Node ID: 1459 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1459in_a = id1460out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1459in_b = id1457out_o;

    id1459out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1459in_a,id1459in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1461out_o;

  { // Node ID: 1461 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1461in_i = id1459out_result;

    id1461out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1461in_i));
  }
  { // Node ID: 3159 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3159in_input = id1461out_o;

    id3159out_output[(getCycle()+2)%3] = id3159in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1462out_result;

  { // Node ID: 1462 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1462in_a = id1519out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1462in_b = id3159out_output[getCycle()%3];

    id1462out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1462in_a,id1462in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1463out_result;

  { // Node ID: 1463 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1463in_a = id3159out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1463in_b = id1519out_dout[getCycle()%3];

    id1463out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1463in_a,id1463in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1464out_result;

  { // Node ID: 1464 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1464in_a = id1462out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1464in_b = id1463out_result;

    id1464out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1464in_a,id1464in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1465out_o;

  { // Node ID: 1465 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1465in_i = id1464out_result;

    id1465out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1465in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1466out_result;

  { // Node ID: 1466 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1466in_a = id1522out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1466in_b = id1465out_o;

    id1466out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1466in_a,id1466in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1467out_result;

  { // Node ID: 1467 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1467in_a = id1465out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1467in_b = id1522out_dout[getCycle()%3];

    id1467out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1467in_a,id1467in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1468out_result;

  { // Node ID: 1468 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1468in_a = id1466out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1468in_b = id1467out_result;

    id1468out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1468in_a,id1468in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1469out_o;

  { // Node ID: 1469 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1469in_i = id1468out_result;

    id1469out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1469in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1470out_o;

  { // Node ID: 1470 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1470in_i = id1469out_o;

    id1470out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1470in_i));
  }
  { // Node ID: 3290 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2776out_result;

  { // Node ID: 2776 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2776in_a = id1470out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2776in_b = id3290out_value;

    id2776out_result = (gte_fixed(id2776in_a,id2776in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2869out_result;

  { // Node ID: 2869 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2869in_a = id1476out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2869in_b = id1479out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2869in_c = id2856out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2869in_condb = id2776out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2869x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2869x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2869x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2869x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2869x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2869x_1 = id2869in_a;
        break;
      default:
        id2869x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2869in_condb.getValueAsLong())) {
      case 0l:
        id2869x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2869x_2 = id2869in_b;
        break;
      default:
        id2869x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2869x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2869x_3 = id2869in_c;
        break;
      default:
        id2869x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2869x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2869x_1),(id2869x_2))),(id2869x_3)));
    id2869out_result = (id2869x_4);
  }
  HWRawBits<1> id2777out_result;

  { // Node ID: 2777 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2777in_a = id2869out_result;

    id2777out_result = (slice<10,1>(id2777in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2778out_output;

  { // Node ID: 2778 (NodeReinterpret)
    const HWRawBits<1> &id2778in_input = id2777out_result;

    id2778out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2778in_input));
  }
  { // Node ID: 3289 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1446out_result;

  { // Node ID: 1446 (NodeGt)
    const HWFloat<8,24> &id1446in_a = id1437out_result;
    const HWFloat<8,24> &id1446in_b = id3289out_value;

    id1446out_result = (gt_float(id1446in_a,id1446in_b));
  }
  { // Node ID: 3161 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3161in_input = id1446out_result;

    id3161out_output[(getCycle()+6)%7] = id3161in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1447out_output;

  { // Node ID: 1447 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1447in_input = id1444out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1447in_input_doubt = id1444out_o_doubt;

    id1447out_output = id1447in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1448out_result;

  { // Node ID: 1448 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1448in_a = id3161out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1448in_b = id1447out_output;

    HWOffsetFix<1,0,UNSIGNED> id1448x_1;

    (id1448x_1) = (and_fixed(id1448in_a,id1448in_b));
    id1448out_result = (id1448x_1);
  }
  { // Node ID: 3162 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3162in_input = id1448out_result;

    id3162out_output[(getCycle()+2)%3] = id3162in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1485out_result;

  { // Node ID: 1485 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1485in_a = id3162out_output[getCycle()%3];

    id1485out_result = (not_fixed(id1485in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1486out_result;

  { // Node ID: 1486 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1486in_a = id2778out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1486in_b = id1485out_result;

    HWOffsetFix<1,0,UNSIGNED> id1486x_1;

    (id1486x_1) = (and_fixed(id1486in_a,id1486in_b));
    id1486out_result = (id1486x_1);
  }
  { // Node ID: 3288 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1450out_result;

  { // Node ID: 1450 (NodeLt)
    const HWFloat<8,24> &id1450in_a = id1437out_result;
    const HWFloat<8,24> &id1450in_b = id3288out_value;

    id1450out_result = (lt_float(id1450in_a,id1450in_b));
  }
  { // Node ID: 3163 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3163in_input = id1450out_result;

    id3163out_output[(getCycle()+6)%7] = id3163in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1451out_output;

  { // Node ID: 1451 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1451in_input = id1444out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1451in_input_doubt = id1444out_o_doubt;

    id1451out_output = id1451in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1452out_result;

  { // Node ID: 1452 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1452in_a = id3163out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1452in_b = id1451out_output;

    HWOffsetFix<1,0,UNSIGNED> id1452x_1;

    (id1452x_1) = (and_fixed(id1452in_a,id1452in_b));
    id1452out_result = (id1452x_1);
  }
  { // Node ID: 3164 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3164in_input = id1452out_result;

    id3164out_output[(getCycle()+2)%3] = id3164in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1487out_result;

  { // Node ID: 1487 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1487in_a = id1486out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1487in_b = id3164out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1487x_1;

    (id1487x_1) = (or_fixed(id1487in_a,id1487in_b));
    id1487out_result = (id1487x_1);
  }
  { // Node ID: 3287 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2779out_result;

  { // Node ID: 2779 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2779in_a = id2869out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2779in_b = id3287out_value;

    id2779out_result = (gte_fixed(id2779in_a,id2779in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1494out_result;

  { // Node ID: 1494 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1494in_a = id3164out_output[getCycle()%3];

    id1494out_result = (not_fixed(id1494in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1495out_result;

  { // Node ID: 1495 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1495in_a = id2779out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1495in_b = id1494out_result;

    HWOffsetFix<1,0,UNSIGNED> id1495x_1;

    (id1495x_1) = (and_fixed(id1495in_a,id1495in_b));
    id1495out_result = (id1495x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1496out_result;

  { // Node ID: 1496 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1496in_a = id1495out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1496in_b = id3162out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1496x_1;

    (id1496x_1) = (or_fixed(id1496in_a,id1496in_b));
    id1496out_result = (id1496x_1);
  }
  HWRawBits<2> id1497out_result;

  { // Node ID: 1497 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1497in_in0 = id1487out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1497in_in1 = id1496out_result;

    id1497out_result = (cat(id1497in_in0,id1497in_in1));
  }
  { // Node ID: 1489 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1488out_o;

  { // Node ID: 1488 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1488in_i = id2869out_result;

    id1488out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1488in_i));
  }
  { // Node ID: 1473 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1474out_result;

  { // Node ID: 1474 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1474in_sel = id2776out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1474in_option0 = id1470out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1474in_option1 = id1473out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1474x_1;

    switch((id1474in_sel.getValueAsLong())) {
      case 0l:
        id1474x_1 = id1474in_option0;
        break;
      case 1l:
        id1474x_1 = id1474in_option1;
        break;
      default:
        id1474x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1474out_result = (id1474x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1475out_o;

  { // Node ID: 1475 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1475in_i = id1474out_result;

    id1475out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1475in_i));
  }
  HWRawBits<32> id1490out_result;

  { // Node ID: 1490 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1490in_in0 = id1489out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1490in_in1 = id1488out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1490in_in2 = id1475out_o;

    id1490out_result = (cat((cat(id1490in_in0,id1490in_in1)),id1490in_in2));
  }
  HWFloat<8,24> id1491out_output;

  { // Node ID: 1491 (NodeReinterpret)
    const HWRawBits<32> &id1491in_input = id1490out_result;

    id1491out_output = (cast_bits2float<8,24>(id1491in_input));
  }
  { // Node ID: 1498 (NodeConstantRawBits)
  }
  { // Node ID: 1499 (NodeConstantRawBits)
  }
  { // Node ID: 1501 (NodeConstantRawBits)
  }
  HWRawBits<32> id2780out_result;

  { // Node ID: 2780 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2780in_in0 = id1498out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2780in_in1 = id1499out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2780in_in2 = id1501out_value;

    id2780out_result = (cat((cat(id2780in_in0,id2780in_in1)),id2780in_in2));
  }
  HWFloat<8,24> id1503out_output;

  { // Node ID: 1503 (NodeReinterpret)
    const HWRawBits<32> &id1503in_input = id2780out_result;

    id1503out_output = (cast_bits2float<8,24>(id1503in_input));
  }
  { // Node ID: 2615 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1506out_result;

  { // Node ID: 1506 (NodeMux)
    const HWRawBits<2> &id1506in_sel = id1497out_result;
    const HWFloat<8,24> &id1506in_option0 = id1491out_output;
    const HWFloat<8,24> &id1506in_option1 = id1503out_output;
    const HWFloat<8,24> &id1506in_option2 = id2615out_value;
    const HWFloat<8,24> &id1506in_option3 = id1503out_output;

    HWFloat<8,24> id1506x_1;

    switch((id1506in_sel.getValueAsLong())) {
      case 0l:
        id1506x_1 = id1506in_option0;
        break;
      case 1l:
        id1506x_1 = id1506in_option1;
        break;
      case 2l:
        id1506x_1 = id1506in_option2;
        break;
      case 3l:
        id1506x_1 = id1506in_option3;
        break;
      default:
        id1506x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1506out_result = (id1506x_1);
  }
  { // Node ID: 3286 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1516out_result;

  { // Node ID: 1516 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1516in_sel = id3167out_output[getCycle()%9];
    const HWFloat<8,24> &id1516in_option0 = id1506out_result;
    const HWFloat<8,24> &id1516in_option1 = id3286out_value;

    HWFloat<8,24> id1516x_1;

    switch((id1516in_sel.getValueAsLong())) {
      case 0l:
        id1516x_1 = id1516in_option0;
        break;
      case 1l:
        id1516x_1 = id1516in_option1;
        break;
      default:
        id1516x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1516out_result = (id1516x_1);
  }
  HWFloat<8,24> id1524out_result;

  { // Node ID: 1524 (NodeMul)
    const HWFloat<8,24> &id1524in_a = id3294out_value;
    const HWFloat<8,24> &id1524in_b = id1516out_result;

    id1524out_result = (mul_float(id1524in_a,id1524in_b));
  }
  { // Node ID: 3285 (NodeConstantRawBits)
  }
  { // Node ID: 3284 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1526out_result;

  { // Node ID: 1526 (NodeAdd)
    const HWFloat<8,24> &id1526in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1526in_b = id3284out_value;

    id1526out_result = (add_float(id1526in_a,id1526in_b));
  }
  HWFloat<8,24> id1528out_result;

  { // Node ID: 1528 (NodeMul)
    const HWFloat<8,24> &id1528in_a = id3285out_value;
    const HWFloat<8,24> &id1528in_b = id1526out_result;

    id1528out_result = (mul_float(id1528in_a,id1528in_b));
  }
  HWRawBits<8> id1600out_result;

  { // Node ID: 1600 (NodeSlice)
    const HWFloat<8,24> &id1600in_a = id1528out_result;

    id1600out_result = (slice<23,8>(id1600in_a));
  }
  { // Node ID: 1601 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2781out_result;

  { // Node ID: 2781 (NodeEqInlined)
    const HWRawBits<8> &id2781in_a = id1600out_result;
    const HWRawBits<8> &id2781in_b = id1601out_value;

    id2781out_result = (eq_bits(id2781in_a,id2781in_b));
  }
  HWRawBits<23> id1599out_result;

  { // Node ID: 1599 (NodeSlice)
    const HWFloat<8,24> &id1599in_a = id1528out_result;

    id1599out_result = (slice<0,23>(id1599in_a));
  }
  { // Node ID: 3283 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2782out_result;

  { // Node ID: 2782 (NodeNeqInlined)
    const HWRawBits<23> &id2782in_a = id1599out_result;
    const HWRawBits<23> &id2782in_b = id3283out_value;

    id2782out_result = (neq_bits(id2782in_a,id2782in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1605out_result;

  { // Node ID: 1605 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1605in_a = id2781out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1605in_b = id2782out_result;

    HWOffsetFix<1,0,UNSIGNED> id1605x_1;

    (id1605x_1) = (and_fixed(id1605in_a,id1605in_b));
    id1605out_result = (id1605x_1);
  }
  { // Node ID: 3177 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3177in_input = id1605out_result;

    id3177out_output[(getCycle()+8)%9] = id3177in_input;
  }
  { // Node ID: 1529 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1530out_output;
  HWOffsetFix<1,0,UNSIGNED> id1530out_output_doubt;

  { // Node ID: 1530 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1530in_input = id1528out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1530in_doubt = id1529out_value;

    id1530out_output = id1530in_input;
    id1530out_output_doubt = id1530in_doubt;
  }
  { // Node ID: 1531 (NodeCast)
    const HWFloat<8,24> &id1531in_i = id1530out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1531in_i_doubt = id1530out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1531x_1;

    id1531out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1531in_i,(&(id1531x_1))));
    id1531out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1531x_1),(c_hw_fix_4_0_uns_bits))),id1531in_i_doubt));
  }
  { // Node ID: 1534 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1533out_result;
  HWOffsetFix<1,0,UNSIGNED> id1533out_result_doubt;

  { // Node ID: 1533 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1533in_a = id1531out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1533in_a_doubt = id1531out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1533in_b = id1534out_value;

    HWOffsetFix<1,0,UNSIGNED> id1533x_1;

    id1533out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1533in_a,id1533in_b,(&(id1533x_1))));
    id1533out_result_doubt = (or_fixed((neq_fixed((id1533x_1),(c_hw_fix_1_0_uns_bits_1))),id1533in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1535out_o;
  HWOffsetFix<1,0,UNSIGNED> id1535out_o_doubt;

  { // Node ID: 1535 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1535in_i = id1533out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1535in_i_doubt = id1533out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1535x_1;

    id1535out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1535in_i,(&(id1535x_1))));
    id1535out_o_doubt = (or_fixed((neq_fixed((id1535x_1),(c_hw_fix_1_0_uns_bits_1))),id1535in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1544out_output;

  { // Node ID: 1544 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1544in_input = id1535out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1544in_input_doubt = id1535out_o_doubt;

    id1544out_output = id1544in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1545out_o;

  { // Node ID: 1545 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1545in_i = id1544out_output;

    id1545out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1545in_i));
  }
  { // Node ID: 3168 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3168in_input = id1545out_o;

    id3168out_output[(getCycle()+2)%3] = id3168in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1567out_o;

  { // Node ID: 1567 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1567in_i = id3168out_output[getCycle()%3];

    id1567out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1567in_i));
  }
  { // Node ID: 1570 (NodeConstantRawBits)
  }
  { // Node ID: 2858 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1547out_o;

  { // Node ID: 1547 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1547in_i = id1544out_output;

    id1547out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1547in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1612out_output;

  { // Node ID: 1612 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1612in_input = id1547out_o;

    id1612out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1612in_input))));
  }
  { // Node ID: 1613 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1613in_addr = id1612out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1613x_1;

    switch(((long)((id1613in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1613x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1613x_1 = (id1613sta_rom_store[(id1613in_addr.getValueAsLong())]);
        break;
      default:
        id1613x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1613out_dout[(getCycle()+2)%3] = (id1613x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1546out_o;

  { // Node ID: 1546 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1546in_i = id1544out_output;

    id1546out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1546in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1609out_output;

  { // Node ID: 1609 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1609in_input = id1546out_o;

    id1609out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1609in_input))));
  }
  { // Node ID: 1610 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1610in_addr = id1609out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1610x_1;

    switch(((long)((id1610in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1610x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1610x_1 = (id1610sta_rom_store[(id1610in_addr.getValueAsLong())]);
        break;
      default:
        id1610x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1610out_dout[(getCycle()+2)%3] = (id1610x_1);
  }
  { // Node ID: 1551 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1548out_o;

  { // Node ID: 1548 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1548in_i = id1544out_output;

    id1548out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1548in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1550out_result;

  { // Node ID: 1550 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1550in_a = id1551out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1550in_b = id1548out_o;

    id1550out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1550in_a,id1550in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1552out_o;

  { // Node ID: 1552 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1552in_i = id1550out_result;

    id1552out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1552in_i));
  }
  { // Node ID: 3169 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3169in_input = id1552out_o;

    id3169out_output[(getCycle()+2)%3] = id3169in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1553out_result;

  { // Node ID: 1553 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1553in_a = id1610out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1553in_b = id3169out_output[getCycle()%3];

    id1553out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1553in_a,id1553in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1554out_result;

  { // Node ID: 1554 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1554in_a = id3169out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1554in_b = id1610out_dout[getCycle()%3];

    id1554out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1554in_a,id1554in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1555out_result;

  { // Node ID: 1555 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1555in_a = id1553out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1555in_b = id1554out_result;

    id1555out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1555in_a,id1555in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1556out_o;

  { // Node ID: 1556 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1556in_i = id1555out_result;

    id1556out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1556in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1557out_result;

  { // Node ID: 1557 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1557in_a = id1613out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1557in_b = id1556out_o;

    id1557out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1557in_a,id1557in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1558out_result;

  { // Node ID: 1558 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1558in_a = id1556out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1558in_b = id1613out_dout[getCycle()%3];

    id1558out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1558in_a,id1558in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1559out_result;

  { // Node ID: 1559 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1559in_a = id1557out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1559in_b = id1558out_result;

    id1559out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1559in_a,id1559in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1560out_o;

  { // Node ID: 1560 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1560in_i = id1559out_result;

    id1560out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1560in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1561out_o;

  { // Node ID: 1561 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1561in_i = id1560out_o;

    id1561out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1561in_i));
  }
  { // Node ID: 3282 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2783out_result;

  { // Node ID: 2783 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2783in_a = id1561out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2783in_b = id3282out_value;

    id2783out_result = (gte_fixed(id2783in_a,id2783in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2868out_result;

  { // Node ID: 2868 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2868in_a = id1567out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2868in_b = id1570out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2868in_c = id2858out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2868in_condb = id2783out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2868x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2868x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2868x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2868x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2868x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2868x_1 = id2868in_a;
        break;
      default:
        id2868x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2868in_condb.getValueAsLong())) {
      case 0l:
        id2868x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2868x_2 = id2868in_b;
        break;
      default:
        id2868x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2868x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2868x_3 = id2868in_c;
        break;
      default:
        id2868x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2868x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2868x_1),(id2868x_2))),(id2868x_3)));
    id2868out_result = (id2868x_4);
  }
  HWRawBits<1> id2784out_result;

  { // Node ID: 2784 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2784in_a = id2868out_result;

    id2784out_result = (slice<10,1>(id2784in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2785out_output;

  { // Node ID: 2785 (NodeReinterpret)
    const HWRawBits<1> &id2785in_input = id2784out_result;

    id2785out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2785in_input));
  }
  { // Node ID: 3281 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1537out_result;

  { // Node ID: 1537 (NodeGt)
    const HWFloat<8,24> &id1537in_a = id1528out_result;
    const HWFloat<8,24> &id1537in_b = id3281out_value;

    id1537out_result = (gt_float(id1537in_a,id1537in_b));
  }
  { // Node ID: 3171 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3171in_input = id1537out_result;

    id3171out_output[(getCycle()+6)%7] = id3171in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1538out_output;

  { // Node ID: 1538 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1538in_input = id1535out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1538in_input_doubt = id1535out_o_doubt;

    id1538out_output = id1538in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1539out_result;

  { // Node ID: 1539 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1539in_a = id3171out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1539in_b = id1538out_output;

    HWOffsetFix<1,0,UNSIGNED> id1539x_1;

    (id1539x_1) = (and_fixed(id1539in_a,id1539in_b));
    id1539out_result = (id1539x_1);
  }
  { // Node ID: 3172 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3172in_input = id1539out_result;

    id3172out_output[(getCycle()+2)%3] = id3172in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1576out_result;

  { // Node ID: 1576 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1576in_a = id3172out_output[getCycle()%3];

    id1576out_result = (not_fixed(id1576in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1577out_result;

  { // Node ID: 1577 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1577in_a = id2785out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1577in_b = id1576out_result;

    HWOffsetFix<1,0,UNSIGNED> id1577x_1;

    (id1577x_1) = (and_fixed(id1577in_a,id1577in_b));
    id1577out_result = (id1577x_1);
  }
  { // Node ID: 3280 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1541out_result;

  { // Node ID: 1541 (NodeLt)
    const HWFloat<8,24> &id1541in_a = id1528out_result;
    const HWFloat<8,24> &id1541in_b = id3280out_value;

    id1541out_result = (lt_float(id1541in_a,id1541in_b));
  }
  { // Node ID: 3173 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3173in_input = id1541out_result;

    id3173out_output[(getCycle()+6)%7] = id3173in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1542out_output;

  { // Node ID: 1542 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1542in_input = id1535out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1542in_input_doubt = id1535out_o_doubt;

    id1542out_output = id1542in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1543out_result;

  { // Node ID: 1543 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1543in_a = id3173out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1543in_b = id1542out_output;

    HWOffsetFix<1,0,UNSIGNED> id1543x_1;

    (id1543x_1) = (and_fixed(id1543in_a,id1543in_b));
    id1543out_result = (id1543x_1);
  }
  { // Node ID: 3174 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3174in_input = id1543out_result;

    id3174out_output[(getCycle()+2)%3] = id3174in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1578out_result;

  { // Node ID: 1578 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1578in_a = id1577out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1578in_b = id3174out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1578x_1;

    (id1578x_1) = (or_fixed(id1578in_a,id1578in_b));
    id1578out_result = (id1578x_1);
  }
  { // Node ID: 3279 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2786out_result;

  { // Node ID: 2786 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2786in_a = id2868out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2786in_b = id3279out_value;

    id2786out_result = (gte_fixed(id2786in_a,id2786in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1585out_result;

  { // Node ID: 1585 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1585in_a = id3174out_output[getCycle()%3];

    id1585out_result = (not_fixed(id1585in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1586out_result;

  { // Node ID: 1586 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1586in_a = id2786out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1586in_b = id1585out_result;

    HWOffsetFix<1,0,UNSIGNED> id1586x_1;

    (id1586x_1) = (and_fixed(id1586in_a,id1586in_b));
    id1586out_result = (id1586x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1587out_result;

  { // Node ID: 1587 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1587in_a = id1586out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1587in_b = id3172out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1587x_1;

    (id1587x_1) = (or_fixed(id1587in_a,id1587in_b));
    id1587out_result = (id1587x_1);
  }
  HWRawBits<2> id1588out_result;

  { // Node ID: 1588 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1588in_in0 = id1578out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1588in_in1 = id1587out_result;

    id1588out_result = (cat(id1588in_in0,id1588in_in1));
  }
  { // Node ID: 1580 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1579out_o;

  { // Node ID: 1579 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1579in_i = id2868out_result;

    id1579out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1579in_i));
  }
  { // Node ID: 1564 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1565out_result;

  { // Node ID: 1565 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1565in_sel = id2783out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1565in_option0 = id1561out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1565in_option1 = id1564out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1565x_1;

    switch((id1565in_sel.getValueAsLong())) {
      case 0l:
        id1565x_1 = id1565in_option0;
        break;
      case 1l:
        id1565x_1 = id1565in_option1;
        break;
      default:
        id1565x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1565out_result = (id1565x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1566out_o;

  { // Node ID: 1566 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1566in_i = id1565out_result;

    id1566out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1566in_i));
  }
  HWRawBits<32> id1581out_result;

  { // Node ID: 1581 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1581in_in0 = id1580out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1581in_in1 = id1579out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1581in_in2 = id1566out_o;

    id1581out_result = (cat((cat(id1581in_in0,id1581in_in1)),id1581in_in2));
  }
  HWFloat<8,24> id1582out_output;

  { // Node ID: 1582 (NodeReinterpret)
    const HWRawBits<32> &id1582in_input = id1581out_result;

    id1582out_output = (cast_bits2float<8,24>(id1582in_input));
  }
  { // Node ID: 1589 (NodeConstantRawBits)
  }
  { // Node ID: 1590 (NodeConstantRawBits)
  }
  { // Node ID: 1592 (NodeConstantRawBits)
  }
  HWRawBits<32> id2787out_result;

  { // Node ID: 2787 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2787in_in0 = id1589out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2787in_in1 = id1590out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2787in_in2 = id1592out_value;

    id2787out_result = (cat((cat(id2787in_in0,id2787in_in1)),id2787in_in2));
  }
  HWFloat<8,24> id1594out_output;

  { // Node ID: 1594 (NodeReinterpret)
    const HWRawBits<32> &id1594in_input = id2787out_result;

    id1594out_output = (cast_bits2float<8,24>(id1594in_input));
  }
  { // Node ID: 2616 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1597out_result;

  { // Node ID: 1597 (NodeMux)
    const HWRawBits<2> &id1597in_sel = id1588out_result;
    const HWFloat<8,24> &id1597in_option0 = id1582out_output;
    const HWFloat<8,24> &id1597in_option1 = id1594out_output;
    const HWFloat<8,24> &id1597in_option2 = id2616out_value;
    const HWFloat<8,24> &id1597in_option3 = id1594out_output;

    HWFloat<8,24> id1597x_1;

    switch((id1597in_sel.getValueAsLong())) {
      case 0l:
        id1597x_1 = id1597in_option0;
        break;
      case 1l:
        id1597x_1 = id1597in_option1;
        break;
      case 2l:
        id1597x_1 = id1597in_option2;
        break;
      case 3l:
        id1597x_1 = id1597in_option3;
        break;
      default:
        id1597x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1597out_result = (id1597x_1);
  }
  { // Node ID: 3278 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1607out_result;

  { // Node ID: 1607 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1607in_sel = id3177out_output[getCycle()%9];
    const HWFloat<8,24> &id1607in_option0 = id1597out_result;
    const HWFloat<8,24> &id1607in_option1 = id3278out_value;

    HWFloat<8,24> id1607x_1;

    switch((id1607in_sel.getValueAsLong())) {
      case 0l:
        id1607x_1 = id1607in_option0;
        break;
      case 1l:
        id1607x_1 = id1607in_option1;
        break;
      default:
        id1607x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1607out_result = (id1607x_1);
  }
  { // Node ID: 3277 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1615out_result;

  { // Node ID: 1615 (NodeAdd)
    const HWFloat<8,24> &id1615in_a = id1607out_result;
    const HWFloat<8,24> &id1615in_b = id3277out_value;

    id1615out_result = (add_float(id1615in_a,id1615in_b));
  }
  HWFloat<8,24> id1616out_result;

  { // Node ID: 1616 (NodeDiv)
    const HWFloat<8,24> &id1616in_a = id1524out_result;
    const HWFloat<8,24> &id1616in_b = id1615out_result;

    id1616out_result = (div_float(id1616in_a,id1616in_b));
  }
  HWFloat<8,24> id1617out_result;

  { // Node ID: 1617 (NodeMul)
    const HWFloat<8,24> &id1617in_a = id12out_dt;
    const HWFloat<8,24> &id1617in_b = id1616out_result;

    id1617out_result = (mul_float(id1617in_a,id1617in_b));
  }
  { // Node ID: 3276 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1833out_result;

  { // Node ID: 1833 (NodeSub)
    const HWFloat<8,24> &id1833in_a = id3276out_value;
    const HWFloat<8,24> &id1833in_b = id3178out_output[getCycle()%9];

    id1833out_result = (sub_float(id1833in_a,id1833in_b));
  }
  HWFloat<8,24> id1834out_result;

  { // Node ID: 1834 (NodeMul)
    const HWFloat<8,24> &id1834in_a = id1617out_result;
    const HWFloat<8,24> &id1834in_b = id1833out_result;

    id1834out_result = (mul_float(id1834in_a,id1834in_b));
  }
  { // Node ID: 3275 (NodeConstantRawBits)
  }
  { // Node ID: 3274 (NodeConstantRawBits)
  }
  { // Node ID: 3273 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1619out_result;

  { // Node ID: 1619 (NodeAdd)
    const HWFloat<8,24> &id1619in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1619in_b = id3273out_value;

    id1619out_result = (add_float(id1619in_a,id1619in_b));
  }
  HWFloat<8,24> id1621out_result;

  { // Node ID: 1621 (NodeMul)
    const HWFloat<8,24> &id1621in_a = id3274out_value;
    const HWFloat<8,24> &id1621in_b = id1619out_result;

    id1621out_result = (mul_float(id1621in_a,id1621in_b));
  }
  HWRawBits<8> id1693out_result;

  { // Node ID: 1693 (NodeSlice)
    const HWFloat<8,24> &id1693in_a = id1621out_result;

    id1693out_result = (slice<23,8>(id1693in_a));
  }
  { // Node ID: 1694 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2788out_result;

  { // Node ID: 2788 (NodeEqInlined)
    const HWRawBits<8> &id2788in_a = id1693out_result;
    const HWRawBits<8> &id2788in_b = id1694out_value;

    id2788out_result = (eq_bits(id2788in_a,id2788in_b));
  }
  HWRawBits<23> id1692out_result;

  { // Node ID: 1692 (NodeSlice)
    const HWFloat<8,24> &id1692in_a = id1621out_result;

    id1692out_result = (slice<0,23>(id1692in_a));
  }
  { // Node ID: 3272 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2789out_result;

  { // Node ID: 2789 (NodeNeqInlined)
    const HWRawBits<23> &id2789in_a = id1692out_result;
    const HWRawBits<23> &id2789in_b = id3272out_value;

    id2789out_result = (neq_bits(id2789in_a,id2789in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1698out_result;

  { // Node ID: 1698 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1698in_a = id2788out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1698in_b = id2789out_result;

    HWOffsetFix<1,0,UNSIGNED> id1698x_1;

    (id1698x_1) = (and_fixed(id1698in_a,id1698in_b));
    id1698out_result = (id1698x_1);
  }
  { // Node ID: 3188 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3188in_input = id1698out_result;

    id3188out_output[(getCycle()+8)%9] = id3188in_input;
  }
  { // Node ID: 1622 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1623out_output;
  HWOffsetFix<1,0,UNSIGNED> id1623out_output_doubt;

  { // Node ID: 1623 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1623in_input = id1621out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1623in_doubt = id1622out_value;

    id1623out_output = id1623in_input;
    id1623out_output_doubt = id1623in_doubt;
  }
  { // Node ID: 1624 (NodeCast)
    const HWFloat<8,24> &id1624in_i = id1623out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1624in_i_doubt = id1623out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1624x_1;

    id1624out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1624in_i,(&(id1624x_1))));
    id1624out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1624x_1),(c_hw_fix_4_0_uns_bits))),id1624in_i_doubt));
  }
  { // Node ID: 1627 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1626out_result;
  HWOffsetFix<1,0,UNSIGNED> id1626out_result_doubt;

  { // Node ID: 1626 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1626in_a = id1624out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1626in_a_doubt = id1624out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1626in_b = id1627out_value;

    HWOffsetFix<1,0,UNSIGNED> id1626x_1;

    id1626out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1626in_a,id1626in_b,(&(id1626x_1))));
    id1626out_result_doubt = (or_fixed((neq_fixed((id1626x_1),(c_hw_fix_1_0_uns_bits_1))),id1626in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1628out_o;
  HWOffsetFix<1,0,UNSIGNED> id1628out_o_doubt;

  { // Node ID: 1628 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1628in_i = id1626out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1628in_i_doubt = id1626out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1628x_1;

    id1628out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1628in_i,(&(id1628x_1))));
    id1628out_o_doubt = (or_fixed((neq_fixed((id1628x_1),(c_hw_fix_1_0_uns_bits_1))),id1628in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1637out_output;

  { // Node ID: 1637 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1637in_input = id1628out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1637in_input_doubt = id1628out_o_doubt;

    id1637out_output = id1637in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1638out_o;

  { // Node ID: 1638 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1638in_i = id1637out_output;

    id1638out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1638in_i));
  }
  { // Node ID: 3179 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3179in_input = id1638out_o;

    id3179out_output[(getCycle()+2)%3] = id3179in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1660out_o;

  { // Node ID: 1660 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1660in_i = id3179out_output[getCycle()%3];

    id1660out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1660in_i));
  }
  { // Node ID: 1663 (NodeConstantRawBits)
  }
  { // Node ID: 2860 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1640out_o;

  { // Node ID: 1640 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1640in_i = id1637out_output;

    id1640out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1640in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1705out_output;

  { // Node ID: 1705 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1705in_input = id1640out_o;

    id1705out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1705in_input))));
  }
  { // Node ID: 1706 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1706in_addr = id1705out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1706x_1;

    switch(((long)((id1706in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1706x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1706x_1 = (id1706sta_rom_store[(id1706in_addr.getValueAsLong())]);
        break;
      default:
        id1706x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1706out_dout[(getCycle()+2)%3] = (id1706x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1639out_o;

  { // Node ID: 1639 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1639in_i = id1637out_output;

    id1639out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1639in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1702out_output;

  { // Node ID: 1702 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1702in_input = id1639out_o;

    id1702out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1702in_input))));
  }
  { // Node ID: 1703 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1703in_addr = id1702out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1703x_1;

    switch(((long)((id1703in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1703x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1703x_1 = (id1703sta_rom_store[(id1703in_addr.getValueAsLong())]);
        break;
      default:
        id1703x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1703out_dout[(getCycle()+2)%3] = (id1703x_1);
  }
  { // Node ID: 1644 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1641out_o;

  { // Node ID: 1641 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1641in_i = id1637out_output;

    id1641out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1641in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1643out_result;

  { // Node ID: 1643 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1643in_a = id1644out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1643in_b = id1641out_o;

    id1643out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1643in_a,id1643in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1645out_o;

  { // Node ID: 1645 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1645in_i = id1643out_result;

    id1645out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1645in_i));
  }
  { // Node ID: 3180 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3180in_input = id1645out_o;

    id3180out_output[(getCycle()+2)%3] = id3180in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1646out_result;

  { // Node ID: 1646 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1646in_a = id1703out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1646in_b = id3180out_output[getCycle()%3];

    id1646out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1646in_a,id1646in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1647out_result;

  { // Node ID: 1647 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1647in_a = id3180out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1647in_b = id1703out_dout[getCycle()%3];

    id1647out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1647in_a,id1647in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1648out_result;

  { // Node ID: 1648 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1648in_a = id1646out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1648in_b = id1647out_result;

    id1648out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1648in_a,id1648in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1649out_o;

  { // Node ID: 1649 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1649in_i = id1648out_result;

    id1649out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1649in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1650out_result;

  { // Node ID: 1650 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1650in_a = id1706out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1650in_b = id1649out_o;

    id1650out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1650in_a,id1650in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1651out_result;

  { // Node ID: 1651 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1651in_a = id1649out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1651in_b = id1706out_dout[getCycle()%3];

    id1651out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1651in_a,id1651in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1652out_result;

  { // Node ID: 1652 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1652in_a = id1650out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1652in_b = id1651out_result;

    id1652out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1652in_a,id1652in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1653out_o;

  { // Node ID: 1653 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1653in_i = id1652out_result;

    id1653out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1653in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1654out_o;

  { // Node ID: 1654 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1654in_i = id1653out_o;

    id1654out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1654in_i));
  }
  { // Node ID: 3271 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2790out_result;

  { // Node ID: 2790 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2790in_a = id1654out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2790in_b = id3271out_value;

    id2790out_result = (gte_fixed(id2790in_a,id2790in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2867out_result;

  { // Node ID: 2867 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2867in_a = id1660out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2867in_b = id1663out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2867in_c = id2860out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2867in_condb = id2790out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2867x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2867x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2867x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2867x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2867x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2867x_1 = id2867in_a;
        break;
      default:
        id2867x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2867in_condb.getValueAsLong())) {
      case 0l:
        id2867x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2867x_2 = id2867in_b;
        break;
      default:
        id2867x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2867x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2867x_3 = id2867in_c;
        break;
      default:
        id2867x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2867x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2867x_1),(id2867x_2))),(id2867x_3)));
    id2867out_result = (id2867x_4);
  }
  HWRawBits<1> id2791out_result;

  { // Node ID: 2791 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2791in_a = id2867out_result;

    id2791out_result = (slice<10,1>(id2791in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2792out_output;

  { // Node ID: 2792 (NodeReinterpret)
    const HWRawBits<1> &id2792in_input = id2791out_result;

    id2792out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2792in_input));
  }
  { // Node ID: 3270 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1630out_result;

  { // Node ID: 1630 (NodeGt)
    const HWFloat<8,24> &id1630in_a = id1621out_result;
    const HWFloat<8,24> &id1630in_b = id3270out_value;

    id1630out_result = (gt_float(id1630in_a,id1630in_b));
  }
  { // Node ID: 3182 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3182in_input = id1630out_result;

    id3182out_output[(getCycle()+6)%7] = id3182in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1631out_output;

  { // Node ID: 1631 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1631in_input = id1628out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1631in_input_doubt = id1628out_o_doubt;

    id1631out_output = id1631in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1632out_result;

  { // Node ID: 1632 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1632in_a = id3182out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1632in_b = id1631out_output;

    HWOffsetFix<1,0,UNSIGNED> id1632x_1;

    (id1632x_1) = (and_fixed(id1632in_a,id1632in_b));
    id1632out_result = (id1632x_1);
  }
  { // Node ID: 3183 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3183in_input = id1632out_result;

    id3183out_output[(getCycle()+2)%3] = id3183in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1669out_result;

  { // Node ID: 1669 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1669in_a = id3183out_output[getCycle()%3];

    id1669out_result = (not_fixed(id1669in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1670out_result;

  { // Node ID: 1670 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1670in_a = id2792out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1670in_b = id1669out_result;

    HWOffsetFix<1,0,UNSIGNED> id1670x_1;

    (id1670x_1) = (and_fixed(id1670in_a,id1670in_b));
    id1670out_result = (id1670x_1);
  }
  { // Node ID: 3269 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1634out_result;

  { // Node ID: 1634 (NodeLt)
    const HWFloat<8,24> &id1634in_a = id1621out_result;
    const HWFloat<8,24> &id1634in_b = id3269out_value;

    id1634out_result = (lt_float(id1634in_a,id1634in_b));
  }
  { // Node ID: 3184 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3184in_input = id1634out_result;

    id3184out_output[(getCycle()+6)%7] = id3184in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1635out_output;

  { // Node ID: 1635 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1635in_input = id1628out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1635in_input_doubt = id1628out_o_doubt;

    id1635out_output = id1635in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1636out_result;

  { // Node ID: 1636 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1636in_a = id3184out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1636in_b = id1635out_output;

    HWOffsetFix<1,0,UNSIGNED> id1636x_1;

    (id1636x_1) = (and_fixed(id1636in_a,id1636in_b));
    id1636out_result = (id1636x_1);
  }
  { // Node ID: 3185 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3185in_input = id1636out_result;

    id3185out_output[(getCycle()+2)%3] = id3185in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1671out_result;

  { // Node ID: 1671 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1671in_a = id1670out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1671in_b = id3185out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1671x_1;

    (id1671x_1) = (or_fixed(id1671in_a,id1671in_b));
    id1671out_result = (id1671x_1);
  }
  { // Node ID: 3268 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2793out_result;

  { // Node ID: 2793 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2793in_a = id2867out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2793in_b = id3268out_value;

    id2793out_result = (gte_fixed(id2793in_a,id2793in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1678out_result;

  { // Node ID: 1678 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1678in_a = id3185out_output[getCycle()%3];

    id1678out_result = (not_fixed(id1678in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1679out_result;

  { // Node ID: 1679 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1679in_a = id2793out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1679in_b = id1678out_result;

    HWOffsetFix<1,0,UNSIGNED> id1679x_1;

    (id1679x_1) = (and_fixed(id1679in_a,id1679in_b));
    id1679out_result = (id1679x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1680out_result;

  { // Node ID: 1680 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1680in_a = id1679out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1680in_b = id3183out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1680x_1;

    (id1680x_1) = (or_fixed(id1680in_a,id1680in_b));
    id1680out_result = (id1680x_1);
  }
  HWRawBits<2> id1681out_result;

  { // Node ID: 1681 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1681in_in0 = id1671out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1681in_in1 = id1680out_result;

    id1681out_result = (cat(id1681in_in0,id1681in_in1));
  }
  { // Node ID: 1673 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1672out_o;

  { // Node ID: 1672 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1672in_i = id2867out_result;

    id1672out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1672in_i));
  }
  { // Node ID: 1657 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1658out_result;

  { // Node ID: 1658 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1658in_sel = id2790out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1658in_option0 = id1654out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1658in_option1 = id1657out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1658x_1;

    switch((id1658in_sel.getValueAsLong())) {
      case 0l:
        id1658x_1 = id1658in_option0;
        break;
      case 1l:
        id1658x_1 = id1658in_option1;
        break;
      default:
        id1658x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1658out_result = (id1658x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1659out_o;

  { // Node ID: 1659 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1659in_i = id1658out_result;

    id1659out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1659in_i));
  }
  HWRawBits<32> id1674out_result;

  { // Node ID: 1674 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1674in_in0 = id1673out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1674in_in1 = id1672out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1674in_in2 = id1659out_o;

    id1674out_result = (cat((cat(id1674in_in0,id1674in_in1)),id1674in_in2));
  }
  HWFloat<8,24> id1675out_output;

  { // Node ID: 1675 (NodeReinterpret)
    const HWRawBits<32> &id1675in_input = id1674out_result;

    id1675out_output = (cast_bits2float<8,24>(id1675in_input));
  }
  { // Node ID: 1682 (NodeConstantRawBits)
  }
  { // Node ID: 1683 (NodeConstantRawBits)
  }
  { // Node ID: 1685 (NodeConstantRawBits)
  }
  HWRawBits<32> id2794out_result;

  { // Node ID: 2794 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2794in_in0 = id1682out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2794in_in1 = id1683out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2794in_in2 = id1685out_value;

    id2794out_result = (cat((cat(id2794in_in0,id2794in_in1)),id2794in_in2));
  }
  HWFloat<8,24> id1687out_output;

  { // Node ID: 1687 (NodeReinterpret)
    const HWRawBits<32> &id1687in_input = id2794out_result;

    id1687out_output = (cast_bits2float<8,24>(id1687in_input));
  }
  { // Node ID: 2617 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1690out_result;

  { // Node ID: 1690 (NodeMux)
    const HWRawBits<2> &id1690in_sel = id1681out_result;
    const HWFloat<8,24> &id1690in_option0 = id1675out_output;
    const HWFloat<8,24> &id1690in_option1 = id1687out_output;
    const HWFloat<8,24> &id1690in_option2 = id2617out_value;
    const HWFloat<8,24> &id1690in_option3 = id1687out_output;

    HWFloat<8,24> id1690x_1;

    switch((id1690in_sel.getValueAsLong())) {
      case 0l:
        id1690x_1 = id1690in_option0;
        break;
      case 1l:
        id1690x_1 = id1690in_option1;
        break;
      case 2l:
        id1690x_1 = id1690in_option2;
        break;
      case 3l:
        id1690x_1 = id1690in_option3;
        break;
      default:
        id1690x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1690out_result = (id1690x_1);
  }
  { // Node ID: 3267 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1700out_result;

  { // Node ID: 1700 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1700in_sel = id3188out_output[getCycle()%9];
    const HWFloat<8,24> &id1700in_option0 = id1690out_result;
    const HWFloat<8,24> &id1700in_option1 = id3267out_value;

    HWFloat<8,24> id1700x_1;

    switch((id1700in_sel.getValueAsLong())) {
      case 0l:
        id1700x_1 = id1700in_option0;
        break;
      case 1l:
        id1700x_1 = id1700in_option1;
        break;
      default:
        id1700x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1700out_result = (id1700x_1);
  }
  HWFloat<8,24> id1708out_result;

  { // Node ID: 1708 (NodeMul)
    const HWFloat<8,24> &id1708in_a = id3275out_value;
    const HWFloat<8,24> &id1708in_b = id1700out_result;

    id1708out_result = (mul_float(id1708in_a,id1708in_b));
  }
  { // Node ID: 3266 (NodeConstantRawBits)
  }
  { // Node ID: 3265 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1710out_result;

  { // Node ID: 1710 (NodeAdd)
    const HWFloat<8,24> &id1710in_a = id17out_result[getCycle()%2];
    const HWFloat<8,24> &id1710in_b = id3265out_value;

    id1710out_result = (add_float(id1710in_a,id1710in_b));
  }
  HWFloat<8,24> id1712out_result;

  { // Node ID: 1712 (NodeMul)
    const HWFloat<8,24> &id1712in_a = id3266out_value;
    const HWFloat<8,24> &id1712in_b = id1710out_result;

    id1712out_result = (mul_float(id1712in_a,id1712in_b));
  }
  HWRawBits<8> id1784out_result;

  { // Node ID: 1784 (NodeSlice)
    const HWFloat<8,24> &id1784in_a = id1712out_result;

    id1784out_result = (slice<23,8>(id1784in_a));
  }
  { // Node ID: 1785 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2795out_result;

  { // Node ID: 2795 (NodeEqInlined)
    const HWRawBits<8> &id2795in_a = id1784out_result;
    const HWRawBits<8> &id2795in_b = id1785out_value;

    id2795out_result = (eq_bits(id2795in_a,id2795in_b));
  }
  HWRawBits<23> id1783out_result;

  { // Node ID: 1783 (NodeSlice)
    const HWFloat<8,24> &id1783in_a = id1712out_result;

    id1783out_result = (slice<0,23>(id1783in_a));
  }
  { // Node ID: 3264 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2796out_result;

  { // Node ID: 2796 (NodeNeqInlined)
    const HWRawBits<23> &id2796in_a = id1783out_result;
    const HWRawBits<23> &id2796in_b = id3264out_value;

    id2796out_result = (neq_bits(id2796in_a,id2796in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1789out_result;

  { // Node ID: 1789 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1789in_a = id2795out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1789in_b = id2796out_result;

    HWOffsetFix<1,0,UNSIGNED> id1789x_1;

    (id1789x_1) = (and_fixed(id1789in_a,id1789in_b));
    id1789out_result = (id1789x_1);
  }
  { // Node ID: 3198 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3198in_input = id1789out_result;

    id3198out_output[(getCycle()+8)%9] = id3198in_input;
  }
  { // Node ID: 1713 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1714out_output;
  HWOffsetFix<1,0,UNSIGNED> id1714out_output_doubt;

  { // Node ID: 1714 (NodeDoubtBitOp)
    const HWFloat<8,24> &id1714in_input = id1712out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1714in_doubt = id1713out_value;

    id1714out_output = id1714in_input;
    id1714out_output_doubt = id1714in_doubt;
  }
  { // Node ID: 1715 (NodeCast)
    const HWFloat<8,24> &id1715in_i = id1714out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1715in_i_doubt = id1714out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1715x_1;

    id1715out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1715in_i,(&(id1715x_1))));
    id1715out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1715x_1),(c_hw_fix_4_0_uns_bits))),id1715in_i_doubt));
  }
  { // Node ID: 1718 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id1717out_result;
  HWOffsetFix<1,0,UNSIGNED> id1717out_result_doubt;

  { // Node ID: 1717 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1717in_a = id1715out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1717in_a_doubt = id1715out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id1717in_b = id1718out_value;

    HWOffsetFix<1,0,UNSIGNED> id1717x_1;

    id1717out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id1717in_a,id1717in_b,(&(id1717x_1))));
    id1717out_result_doubt = (or_fixed((neq_fixed((id1717x_1),(c_hw_fix_1_0_uns_bits_1))),id1717in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1719out_o;
  HWOffsetFix<1,0,UNSIGNED> id1719out_o_doubt;

  { // Node ID: 1719 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id1719in_i = id1717out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1719in_i_doubt = id1717out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1719x_1;

    id1719out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id1719in_i,(&(id1719x_1))));
    id1719out_o_doubt = (or_fixed((neq_fixed((id1719x_1),(c_hw_fix_1_0_uns_bits_1))),id1719in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id1728out_output;

  { // Node ID: 1728 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1728in_input = id1719out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1728in_input_doubt = id1719out_o_doubt;

    id1728out_output = id1728in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1729out_o;

  { // Node ID: 1729 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1729in_i = id1728out_output;

    id1729out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1729in_i));
  }
  { // Node ID: 3189 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id3189in_input = id1729out_o;

    id3189out_output[(getCycle()+2)%3] = id3189in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1751out_o;

  { // Node ID: 1751 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1751in_i = id3189out_output[getCycle()%3];

    id1751out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1751in_i));
  }
  { // Node ID: 1754 (NodeConstantRawBits)
  }
  { // Node ID: 2862 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id1731out_o;

  { // Node ID: 1731 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1731in_i = id1728out_output;

    id1731out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id1731in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1796out_output;

  { // Node ID: 1796 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id1796in_input = id1731out_o;

    id1796out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1796in_input))));
  }
  { // Node ID: 1797 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1797in_addr = id1796out_output;

    HWOffsetFix<22,-24,UNSIGNED> id1797x_1;

    switch(((long)((id1797in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1797x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id1797x_1 = (id1797sta_rom_store[(id1797in_addr.getValueAsLong())]);
        break;
      default:
        id1797x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id1797out_dout[(getCycle()+2)%3] = (id1797x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id1730out_o;

  { // Node ID: 1730 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1730in_i = id1728out_output;

    id1730out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id1730in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id1793out_output;

  { // Node ID: 1793 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id1793in_input = id1730out_o;

    id1793out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id1793in_input))));
  }
  { // Node ID: 1794 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id1794in_addr = id1793out_output;

    HWOffsetFix<24,-24,UNSIGNED> id1794x_1;

    switch(((long)((id1794in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id1794x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id1794x_1 = (id1794sta_rom_store[(id1794in_addr.getValueAsLong())]);
        break;
      default:
        id1794x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id1794out_dout[(getCycle()+2)%3] = (id1794x_1);
  }
  { // Node ID: 1735 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id1732out_o;

  { // Node ID: 1732 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1732in_i = id1728out_output;

    id1732out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id1732in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id1734out_result;

  { // Node ID: 1734 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id1734in_a = id1735out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id1734in_b = id1732out_o;

    id1734out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id1734in_a,id1734in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id1736out_o;

  { // Node ID: 1736 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id1736in_i = id1734out_result;

    id1736out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id1736in_i));
  }
  { // Node ID: 3190 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id3190in_input = id1736out_o;

    id3190out_output[(getCycle()+2)%3] = id3190in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id1737out_result;

  { // Node ID: 1737 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id1737in_a = id1794out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id1737in_b = id3190out_output[getCycle()%3];

    id1737out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1737in_a,id1737in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id1738out_result;

  { // Node ID: 1738 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id1738in_a = id3190out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id1738in_b = id1794out_dout[getCycle()%3];

    id1738out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id1738in_a,id1738in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id1739out_result;

  { // Node ID: 1739 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id1739in_a = id1737out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id1739in_b = id1738out_result;

    id1739out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id1739in_a,id1739in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1740out_o;

  { // Node ID: 1740 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id1740in_i = id1739out_result;

    id1740out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id1740in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1741out_result;

  { // Node ID: 1741 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id1741in_a = id1797out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id1741in_b = id1740out_o;

    id1741out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id1741in_a,id1741in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id1742out_result;

  { // Node ID: 1742 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id1742in_a = id1740out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id1742in_b = id1797out_dout[getCycle()%3];

    id1742out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id1742in_a,id1742in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id1743out_result;

  { // Node ID: 1743 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id1743in_a = id1741out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id1743in_b = id1742out_result;

    id1743out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id1743in_a,id1743in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id1744out_o;

  { // Node ID: 1744 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id1744in_i = id1743out_result;

    id1744out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id1744in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id1745out_o;

  { // Node ID: 1745 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id1745in_i = id1744out_o;

    id1745out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1745in_i));
  }
  { // Node ID: 3263 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2797out_result;

  { // Node ID: 2797 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id2797in_a = id1745out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id2797in_b = id3263out_value;

    id2797out_result = (gte_fixed(id2797in_a,id2797in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2866out_result;

  { // Node ID: 2866 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2866in_a = id1751out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2866in_b = id1754out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2866in_c = id2862out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2866in_condb = id2797out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2866x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2866x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2866x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2866x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2866x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2866x_1 = id2866in_a;
        break;
      default:
        id2866x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2866in_condb.getValueAsLong())) {
      case 0l:
        id2866x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2866x_2 = id2866in_b;
        break;
      default:
        id2866x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2866x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2866x_3 = id2866in_c;
        break;
      default:
        id2866x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2866x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2866x_1),(id2866x_2))),(id2866x_3)));
    id2866out_result = (id2866x_4);
  }
  HWRawBits<1> id2798out_result;

  { // Node ID: 2798 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2798in_a = id2866out_result;

    id2798out_result = (slice<10,1>(id2798in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2799out_output;

  { // Node ID: 2799 (NodeReinterpret)
    const HWRawBits<1> &id2799in_input = id2798out_result;

    id2799out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2799in_input));
  }
  { // Node ID: 3262 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1721out_result;

  { // Node ID: 1721 (NodeGt)
    const HWFloat<8,24> &id1721in_a = id1712out_result;
    const HWFloat<8,24> &id1721in_b = id3262out_value;

    id1721out_result = (gt_float(id1721in_a,id1721in_b));
  }
  { // Node ID: 3192 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3192in_input = id1721out_result;

    id3192out_output[(getCycle()+6)%7] = id3192in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1722out_output;

  { // Node ID: 1722 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1722in_input = id1719out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1722in_input_doubt = id1719out_o_doubt;

    id1722out_output = id1722in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1723out_result;

  { // Node ID: 1723 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1723in_a = id3192out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1723in_b = id1722out_output;

    HWOffsetFix<1,0,UNSIGNED> id1723x_1;

    (id1723x_1) = (and_fixed(id1723in_a,id1723in_b));
    id1723out_result = (id1723x_1);
  }
  { // Node ID: 3193 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3193in_input = id1723out_result;

    id3193out_output[(getCycle()+2)%3] = id3193in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1760out_result;

  { // Node ID: 1760 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1760in_a = id3193out_output[getCycle()%3];

    id1760out_result = (not_fixed(id1760in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1761out_result;

  { // Node ID: 1761 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1761in_a = id2799out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1761in_b = id1760out_result;

    HWOffsetFix<1,0,UNSIGNED> id1761x_1;

    (id1761x_1) = (and_fixed(id1761in_a,id1761in_b));
    id1761out_result = (id1761x_1);
  }
  { // Node ID: 3261 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1725out_result;

  { // Node ID: 1725 (NodeLt)
    const HWFloat<8,24> &id1725in_a = id1712out_result;
    const HWFloat<8,24> &id1725in_b = id3261out_value;

    id1725out_result = (lt_float(id1725in_a,id1725in_b));
  }
  { // Node ID: 3194 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3194in_input = id1725out_result;

    id3194out_output[(getCycle()+6)%7] = id3194in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1726out_output;

  { // Node ID: 1726 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id1726in_input = id1719out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1726in_input_doubt = id1719out_o_doubt;

    id1726out_output = id1726in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1727out_result;

  { // Node ID: 1727 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1727in_a = id3194out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1727in_b = id1726out_output;

    HWOffsetFix<1,0,UNSIGNED> id1727x_1;

    (id1727x_1) = (and_fixed(id1727in_a,id1727in_b));
    id1727out_result = (id1727x_1);
  }
  { // Node ID: 3195 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3195in_input = id1727out_result;

    id3195out_output[(getCycle()+2)%3] = id3195in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1762out_result;

  { // Node ID: 1762 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1762in_a = id1761out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1762in_b = id3195out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1762x_1;

    (id1762x_1) = (or_fixed(id1762in_a,id1762in_b));
    id1762out_result = (id1762x_1);
  }
  { // Node ID: 3260 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2800out_result;

  { // Node ID: 2800 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2800in_a = id2866out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2800in_b = id3260out_value;

    id2800out_result = (gte_fixed(id2800in_a,id2800in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1769out_result;

  { // Node ID: 1769 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1769in_a = id3195out_output[getCycle()%3];

    id1769out_result = (not_fixed(id1769in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1770out_result;

  { // Node ID: 1770 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1770in_a = id2800out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1770in_b = id1769out_result;

    HWOffsetFix<1,0,UNSIGNED> id1770x_1;

    (id1770x_1) = (and_fixed(id1770in_a,id1770in_b));
    id1770out_result = (id1770x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1771out_result;

  { // Node ID: 1771 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1771in_a = id1770out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1771in_b = id3193out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1771x_1;

    (id1771x_1) = (or_fixed(id1771in_a,id1771in_b));
    id1771out_result = (id1771x_1);
  }
  HWRawBits<2> id1772out_result;

  { // Node ID: 1772 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1772in_in0 = id1762out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1772in_in1 = id1771out_result;

    id1772out_result = (cat(id1772in_in0,id1772in_in1));
  }
  { // Node ID: 1764 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1763out_o;

  { // Node ID: 1763 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1763in_i = id2866out_result;

    id1763out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1763in_i));
  }
  { // Node ID: 1748 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id1749out_result;

  { // Node ID: 1749 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1749in_sel = id2797out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id1749in_option0 = id1745out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id1749in_option1 = id1748out_value;

    HWOffsetFix<24,-23,UNSIGNED> id1749x_1;

    switch((id1749in_sel.getValueAsLong())) {
      case 0l:
        id1749x_1 = id1749in_option0;
        break;
      case 1l:
        id1749x_1 = id1749in_option1;
        break;
      default:
        id1749x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id1749out_result = (id1749x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1750out_o;

  { // Node ID: 1750 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1750in_i = id1749out_result;

    id1750out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1750in_i));
  }
  HWRawBits<32> id1765out_result;

  { // Node ID: 1765 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1765in_in0 = id1764out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1765in_in1 = id1763out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id1765in_in2 = id1750out_o;

    id1765out_result = (cat((cat(id1765in_in0,id1765in_in1)),id1765in_in2));
  }
  HWFloat<8,24> id1766out_output;

  { // Node ID: 1766 (NodeReinterpret)
    const HWRawBits<32> &id1766in_input = id1765out_result;

    id1766out_output = (cast_bits2float<8,24>(id1766in_input));
  }
  { // Node ID: 1773 (NodeConstantRawBits)
  }
  { // Node ID: 1774 (NodeConstantRawBits)
  }
  { // Node ID: 1776 (NodeConstantRawBits)
  }
  HWRawBits<32> id2801out_result;

  { // Node ID: 2801 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2801in_in0 = id1773out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2801in_in1 = id1774out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id2801in_in2 = id1776out_value;

    id2801out_result = (cat((cat(id2801in_in0,id2801in_in1)),id2801in_in2));
  }
  HWFloat<8,24> id1778out_output;

  { // Node ID: 1778 (NodeReinterpret)
    const HWRawBits<32> &id1778in_input = id2801out_result;

    id1778out_output = (cast_bits2float<8,24>(id1778in_input));
  }
  { // Node ID: 2618 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1781out_result;

  { // Node ID: 1781 (NodeMux)
    const HWRawBits<2> &id1781in_sel = id1772out_result;
    const HWFloat<8,24> &id1781in_option0 = id1766out_output;
    const HWFloat<8,24> &id1781in_option1 = id1778out_output;
    const HWFloat<8,24> &id1781in_option2 = id2618out_value;
    const HWFloat<8,24> &id1781in_option3 = id1778out_output;

    HWFloat<8,24> id1781x_1;

    switch((id1781in_sel.getValueAsLong())) {
      case 0l:
        id1781x_1 = id1781in_option0;
        break;
      case 1l:
        id1781x_1 = id1781in_option1;
        break;
      case 2l:
        id1781x_1 = id1781in_option2;
        break;
      case 3l:
        id1781x_1 = id1781in_option3;
        break;
      default:
        id1781x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1781out_result = (id1781x_1);
  }
  { // Node ID: 3259 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1791out_result;

  { // Node ID: 1791 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1791in_sel = id3198out_output[getCycle()%9];
    const HWFloat<8,24> &id1791in_option0 = id1781out_result;
    const HWFloat<8,24> &id1791in_option1 = id3259out_value;

    HWFloat<8,24> id1791x_1;

    switch((id1791in_sel.getValueAsLong())) {
      case 0l:
        id1791x_1 = id1791in_option0;
        break;
      case 1l:
        id1791x_1 = id1791in_option1;
        break;
      default:
        id1791x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id1791out_result = (id1791x_1);
  }
  { // Node ID: 3258 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1799out_result;

  { // Node ID: 1799 (NodeAdd)
    const HWFloat<8,24> &id1799in_a = id1791out_result;
    const HWFloat<8,24> &id1799in_b = id3258out_value;

    id1799out_result = (add_float(id1799in_a,id1799in_b));
  }
  HWFloat<8,24> id1800out_result;

  { // Node ID: 1800 (NodeDiv)
    const HWFloat<8,24> &id1800in_a = id1708out_result;
    const HWFloat<8,24> &id1800in_b = id1799out_result;

    id1800out_result = (div_float(id1800in_a,id1800in_b));
  }
  HWFloat<8,24> id1801out_result;

  { // Node ID: 1801 (NodeMul)
    const HWFloat<8,24> &id1801in_a = id12out_dt;
    const HWFloat<8,24> &id1801in_b = id1800out_result;

    id1801out_result = (mul_float(id1801in_a,id1801in_b));
  }
  HWFloat<8,24> id1835out_result;

  { // Node ID: 1835 (NodeMul)
    const HWFloat<8,24> &id1835in_a = id1801out_result;
    const HWFloat<8,24> &id1835in_b = id3178out_output[getCycle()%9];

    id1835out_result = (mul_float(id1835in_a,id1835in_b));
  }
  HWFloat<8,24> id1836out_result;

  { // Node ID: 1836 (NodeSub)
    const HWFloat<8,24> &id1836in_a = id1834out_result;
    const HWFloat<8,24> &id1836in_b = id1835out_result;

    id1836out_result = (sub_float(id1836in_a,id1836in_b));
  }
  HWFloat<8,24> id1837out_result;

  { // Node ID: 1837 (NodeAdd)
    const HWFloat<8,24> &id1837in_a = id3178out_output[getCycle()%9];
    const HWFloat<8,24> &id1837in_b = id1836out_result;

    id1837out_result = (add_float(id1837in_a,id1837in_b));
  }
  { // Node ID: 3157 (NodeFIFO)
    const HWFloat<8,24> &id3157in_input = id1837out_result;

    id3157out_output[(getCycle()+1)%2] = id3157in_input;
  }
  { // Node ID: 3255 (NodeConstantRawBits)
  }
  { // Node ID: 2802 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2802in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2802in_b = id3255out_value;

    id2802out_result[(getCycle()+1)%2] = (eq_fixed(id2802in_a,id2802in_b));
  }
  { // Node ID: 3254 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1918out_result;

  { // Node ID: 1918 (NodeMul)
    const HWFloat<8,24> &id1918in_a = id3254out_value;
    const HWFloat<8,24> &id1918in_b = id1916out_result;

    id1918out_result = (mul_float(id1918in_a,id1918in_b));
  }
  { // Node ID: 3253 (NodeConstantRawBits)
  }
  { // Node ID: 3252 (NodeConstantRawBits)
  }
  HWFloat<8,24> id1920out_result;

  { // Node ID: 1920 (NodeSub)
    const HWFloat<8,24> &id1920in_a = id3252out_value;
    const HWFloat<8,24> &id1920in_b = id3233out_output[getCycle()%3];

    id1920out_result = (sub_float(id1920in_a,id1920in_b));
  }
  HWFloat<8,24> id1922out_result;

  { // Node ID: 1922 (NodeMul)
    const HWFloat<8,24> &id1922in_a = id3253out_value;
    const HWFloat<8,24> &id1922in_b = id1920out_result;

    id1922out_result = (mul_float(id1922in_a,id1922in_b));
  }
  HWFloat<8,24> id1923out_result;

  { // Node ID: 1923 (NodeAdd)
    const HWFloat<8,24> &id1923in_a = id1918out_result;
    const HWFloat<8,24> &id1923in_b = id1922out_result;

    id1923out_result = (add_float(id1923in_a,id1923in_b));
  }
  HWFloat<8,24> id1924out_result;

  { // Node ID: 1924 (NodeMul)
    const HWFloat<8,24> &id1924in_a = id12out_dt;
    const HWFloat<8,24> &id1924in_b = id1923out_result;

    id1924out_result = (mul_float(id1924in_a,id1924in_b));
  }
  HWFloat<8,24> id1925out_result;

  { // Node ID: 1925 (NodeAdd)
    const HWFloat<8,24> &id1925in_a = id3233out_output[getCycle()%3];
    const HWFloat<8,24> &id1925in_b = id1924out_result;

    id1925out_result = (add_float(id1925in_a,id1925in_b));
  }
  HWFloat<8,24> id2911out_output;

  { // Node ID: 2911 (NodeStreamOffset)
    const HWFloat<8,24> &id2911in_input = id1925out_result;

    id2911out_output = id2911in_input;
  }
  { // Node ID: 20 (NodeInputMappedReg)
  }
  { // Node ID: 21 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id21in_sel = id2802out_result[getCycle()%2];
    const HWFloat<8,24> &id21in_option0 = id2911out_output;
    const HWFloat<8,24> &id21in_option1 = id20out_ca_in;

    HWFloat<8,24> id21x_1;

    switch((id21in_sel.getValueAsLong())) {
      case 0l:
        id21x_1 = id21in_option0;
        break;
      case 1l:
        id21x_1 = id21in_option1;
        break;
      default:
        id21x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id21out_result[(getCycle()+1)%2] = (id21x_1);
  }
  { // Node ID: 3210 (NodeFIFO)
    const HWFloat<8,24> &id3210in_input = id21out_result[getCycle()%2];

    id3210out_output[(getCycle()+7)%8] = id3210in_input;
  }
  { // Node ID: 3233 (NodeFIFO)
    const HWFloat<8,24> &id3233in_input = id3210out_output[getCycle()%8];

    id3233out_output[(getCycle()+2)%3] = id3233in_input;
  }
  HWRawBits<23> id1838out_result;

  { // Node ID: 1838 (NodeSlice)
    const HWFloat<8,24> &id1838in_a = id3210out_output[getCycle()%8];

    id1838out_result = (slice<0,23>(id1838in_a));
  }
  HWOffsetFix<23,-23,UNSIGNED> id1839out_output;

  { // Node ID: 1839 (NodeReinterpret)
    const HWRawBits<23> &id1839in_input = id1838out_result;

    id1839out_output = (cast_bits2fixed<23,-23,UNSIGNED>(id1839in_input));
  }
  { // Node ID: 1840 (NodeConstantRawBits)
  }
  HWOffsetFix<23,-23,UNSIGNED> id1841out_output;
  HWOffsetFix<1,0,UNSIGNED> id1841out_output_doubt;

  { // Node ID: 1841 (NodeDoubtBitOp)
    const HWOffsetFix<23,-23,UNSIGNED> &id1841in_input = id1839out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1841in_doubt = id1840out_value;

    id1841out_output = id1841in_input;
    id1841out_output_doubt = id1841in_doubt;
  }
  HWOffsetFix<24,-23,UNSIGNED> id1842out_o;
  HWOffsetFix<1,0,UNSIGNED> id1842out_o_doubt;

  { // Node ID: 1842 (NodeCast)
    const HWOffsetFix<23,-23,UNSIGNED> &id1842in_i = id1841out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1842in_i_doubt = id1841out_output_doubt;

    id1842out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id1842in_i));
    id1842out_o_doubt = id1842in_i_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1846out_output;

  { // Node ID: 1846 (NodeDoubtBitOp)
    const HWOffsetFix<24,-23,UNSIGNED> &id1846in_input = id1842out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1846in_input_doubt = id1842out_o_doubt;

    id1846out_output = id1846in_input_doubt;
  }
  HWOffsetFix<24,-23,UNSIGNED> id1843out_output;

  { // Node ID: 1843 (NodeDoubtBitOp)
    const HWOffsetFix<24,-23,UNSIGNED> &id1843in_input = id1842out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1843in_input_doubt = id1842out_o_doubt;

    id1843out_output = id1843in_input;
  }
  { // Node ID: 3247 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1845out_result;

  { // Node ID: 1845 (NodeGte)
    const HWOffsetFix<24,-23,UNSIGNED> &id1845in_a = id1843out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1845in_b = id3247out_value;

    id1845out_result = (gte_fixed(id1845in_a,id1845in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1847out_result;

  { // Node ID: 1847 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1847in_a = id1846out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1847in_b = id1845out_result;

    HWOffsetFix<1,0,UNSIGNED> id1847x_1;

    (id1847x_1) = (or_fixed(id1847in_a,id1847in_b));
    id1847out_result = (id1847x_1);
  }
  { // Node ID: 3211 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3211in_input = id1847out_result;

    id3211out_output[(getCycle()+2)%3] = id3211in_input;
  }
  { // Node ID: 1848 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id2864out_result;

  { // Node ID: 2864 (NodeCondSub)
    const HWOffsetFix<24,-23,UNSIGNED> &id2864in_a = id1843out_output;
    const HWOffsetFix<24,-23,UNSIGNED> &id2864in_b = id1848out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2864in_condb = id1847out_result;

    HWOffsetFix<24,-23,UNSIGNED> id2864x_1;
    HWOffsetFix<24,-23,UNSIGNED> id2864x_2;

    switch((id2864in_condb.getValueAsLong())) {
      case 0l:
        id2864x_1 = (c_hw_fix_24_n23_uns_bits_1);
        break;
      case 1l:
        id2864x_1 = id2864in_b;
        break;
      default:
        id2864x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    (id2864x_2) = (sub_fixed<24,-23,UNSIGNED,TRUNCATE>(id2864in_a,(id2864x_1)));
    id2864out_result = (id2864x_2);
  }
  HWOffsetFix<23,-23,UNSIGNED> id1863out_o;

  { // Node ID: 1863 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id1863in_i = id2864out_result;

    id1863out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id1863in_i));
  }
  HWRawBits<9> id1864out_result;

  { // Node ID: 1864 (NodeSlice)
    const HWOffsetFix<23,-23,UNSIGNED> &id1864in_a = id1863out_o;

    id1864out_result = (slice<14,9>(id1864in_a));
  }
  { // Node ID: 2573 (NodeROM)
    const HWRawBits<9> &id2573in_addr = id1864out_result;

    HWRawBits<96> id2573x_1;

    switch(((long)((id2573in_addr.getValueAsLong())<(512l)))) {
      case 0l:
        id2573x_1 = (c_hw_bit_96_undef);
        break;
      case 1l:
        id2573x_1 = (id2573sta_rom_store[(id2573in_addr.getValueAsLong())]);
        break;
      default:
        id2573x_1 = (c_hw_bit_96_undef);
        break;
    }
    id2573out_dout[(getCycle()+2)%3] = (id2573x_1);
  }
  HWRawBits<14> id1865out_result;

  { // Node ID: 1865 (NodeSlice)
    const HWOffsetFix<23,-23,UNSIGNED> &id1865in_a = id1863out_o;

    id1865out_result = (slice<0,14>(id1865in_a));
  }
  HWOffsetFix<14,-14,UNSIGNED> id1866out_output;

  { // Node ID: 1866 (NodeReinterpret)
    const HWRawBits<14> &id1866in_input = id1865out_result;

    id1866out_output = (cast_bits2fixed<14,-14,UNSIGNED>(id1866in_input));
  }
  { // Node ID: 3212 (NodeFIFO)
    const HWOffsetFix<14,-14,UNSIGNED> &id3212in_input = id1866out_output;

    id3212out_output[(getCycle()+2)%3] = id3212in_input;
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2516 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2516in_output_control = id2515out_result;
    const HWFloat<8,24> &id2516in_data = id2500out_result;

    bool id2516x_1;

    (id2516x_1) = ((id2516in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2516x_1)) {
      writeOutput(m_u_out, id2516in_data);
    }
  }
  { // Node ID: 3243 (NodeConstantRawBits)
  }
  { // Node ID: 2519 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2519in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2519in_b = id3243out_value;

    id2519out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2519in_a,id2519in_b));
  }
  { // Node ID: 2806 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2806in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2806in_b = id2519out_result[getCycle()%2];

    id2806out_result[(getCycle()+1)%2] = (eq_fixed(id2806in_a,id2806in_b));
  }
  { // Node ID: 2521 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2522out_result;

  { // Node ID: 2522 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2522in_a = id2521out_io_ca_out_force_disabled;

    id2522out_result = (not_fixed(id2522in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2523out_result;

  { // Node ID: 2523 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2523in_a = id2806out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2523in_b = id2522out_result;

    HWOffsetFix<1,0,UNSIGNED> id2523x_1;

    (id2523x_1) = (and_fixed(id2523in_a,id2523in_b));
    id2523out_result = (id2523x_1);
  }
  { // Node ID: 3218 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3218in_input = id2523out_result;

    id3218out_output[(getCycle()+9)%10] = id3218in_input;
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2524 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2524in_output_control = id3218out_output[getCycle()%10];
    const HWFloat<8,24> &id2524in_data = id1925out_result;

    bool id2524x_1;

    (id2524x_1) = ((id2524in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2524x_1)) {
      writeOutput(m_ca_out, id2524in_data);
    }
  }
  { // Node ID: 3242 (NodeConstantRawBits)
  }
  { // Node ID: 2527 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2527in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2527in_b = id3242out_value;

    id2527out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2527in_a,id2527in_b));
  }
  { // Node ID: 2807 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2807in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2807in_b = id2527out_result[getCycle()%2];

    id2807out_result[(getCycle()+1)%2] = (eq_fixed(id2807in_a,id2807in_b));
  }
  { // Node ID: 2529 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2530out_result;

  { // Node ID: 2530 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2530in_a = id2529out_io_x1_out_force_disabled;

    id2530out_result = (not_fixed(id2530in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2531out_result;

  { // Node ID: 2531 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2531in_a = id2807out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2531in_b = id2530out_result;

    HWOffsetFix<1,0,UNSIGNED> id2531x_1;

    (id2531x_1) = (and_fixed(id2531in_a,id2531in_b));
    id2531out_result = (id2531x_1);
  }
  { // Node ID: 3220 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3220in_input = id2531out_result;

    id3220out_output[(getCycle()+8)%9] = id3220in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2532 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2532in_output_control = id3220out_output[getCycle()%9];
    const HWFloat<8,24> &id2532in_data = id1807out_result;

    bool id2532x_1;

    (id2532x_1) = ((id2532in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2532x_1)) {
      writeOutput(m_x1_out, id2532in_data);
    }
  }
  { // Node ID: 3241 (NodeConstantRawBits)
  }
  { // Node ID: 2535 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2535in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2535in_b = id3241out_value;

    id2535out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2535in_a,id2535in_b));
  }
  { // Node ID: 2808 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2808in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2808in_b = id2535out_result[getCycle()%2];

    id2808out_result[(getCycle()+1)%2] = (eq_fixed(id2808in_a,id2808in_b));
  }
  { // Node ID: 2537 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2538out_result;

  { // Node ID: 2538 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2538in_a = id2537out_io_m_out_force_disabled;

    id2538out_result = (not_fixed(id2538in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2539out_result;

  { // Node ID: 2539 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2539in_a = id2808out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2539in_b = id2538out_result;

    HWOffsetFix<1,0,UNSIGNED> id2539x_1;

    (id2539x_1) = (and_fixed(id2539in_a,id2539in_b));
    id2539out_result = (id2539x_1);
  }
  { // Node ID: 3222 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3222in_input = id2539out_result;

    id3222out_output[(getCycle()+8)%9] = id3222in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2540 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2540in_output_control = id3222out_output[getCycle()%9];
    const HWFloat<8,24> &id2540in_data = id1813out_result;

    bool id2540x_1;

    (id2540x_1) = ((id2540in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2540x_1)) {
      writeOutput(m_m_out, id2540in_data);
    }
  }
  { // Node ID: 3240 (NodeConstantRawBits)
  }
  { // Node ID: 2543 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2543in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2543in_b = id3240out_value;

    id2543out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2543in_a,id2543in_b));
  }
  { // Node ID: 2809 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2809in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2809in_b = id2543out_result[getCycle()%2];

    id2809out_result[(getCycle()+1)%2] = (eq_fixed(id2809in_a,id2809in_b));
  }
  { // Node ID: 2545 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2546out_result;

  { // Node ID: 2546 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2546in_a = id2545out_io_f_out_force_disabled;

    id2546out_result = (not_fixed(id2546in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2547out_result;

  { // Node ID: 2547 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2547in_a = id2809out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2547in_b = id2546out_result;

    HWOffsetFix<1,0,UNSIGNED> id2547x_1;

    (id2547x_1) = (and_fixed(id2547in_a,id2547in_b));
    id2547out_result = (id2547x_1);
  }
  { // Node ID: 3224 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3224in_input = id2547out_result;

    id3224out_output[(getCycle()+8)%9] = id3224in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2548 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2548in_output_control = id3224out_output[getCycle()%9];
    const HWFloat<8,24> &id2548in_data = id1837out_result;

    bool id2548x_1;

    (id2548x_1) = ((id2548in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2548x_1)) {
      writeOutput(m_f_out, id2548in_data);
    }
  }
  { // Node ID: 3239 (NodeConstantRawBits)
  }
  { // Node ID: 2551 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2551in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2551in_b = id3239out_value;

    id2551out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2551in_a,id2551in_b));
  }
  { // Node ID: 2810 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2810in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2810in_b = id2551out_result[getCycle()%2];

    id2810out_result[(getCycle()+1)%2] = (eq_fixed(id2810in_a,id2810in_b));
  }
  { // Node ID: 2553 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2554out_result;

  { // Node ID: 2554 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2554in_a = id2553out_io_h_out_force_disabled;

    id2554out_result = (not_fixed(id2554in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2555out_result;

  { // Node ID: 2555 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2555in_a = id2810out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2555in_b = id2554out_result;

    HWOffsetFix<1,0,UNSIGNED> id2555x_1;

    (id2555x_1) = (and_fixed(id2555in_a,id2555in_b));
    id2555out_result = (id2555x_1);
  }
  { // Node ID: 3226 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3226in_input = id2555out_result;

    id3226out_output[(getCycle()+9)%10] = id3226in_input;
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2556 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2556in_output_control = id3226out_output[getCycle()%10];
    const HWFloat<8,24> &id2556in_data = id1819out_result;

    bool id2556x_1;

    (id2556x_1) = ((id2556in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2556x_1)) {
      writeOutput(m_h_out, id2556in_data);
    }
  }
  { // Node ID: 3238 (NodeConstantRawBits)
  }
  { // Node ID: 2559 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2559in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2559in_b = id3238out_value;

    id2559out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2559in_a,id2559in_b));
  }
  { // Node ID: 2811 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2811in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2811in_b = id2559out_result[getCycle()%2];

    id2811out_result[(getCycle()+1)%2] = (eq_fixed(id2811in_a,id2811in_b));
  }
  { // Node ID: 2561 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2562out_result;

  { // Node ID: 2562 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2562in_a = id2561out_io_d_out_force_disabled;

    id2562out_result = (not_fixed(id2562in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2563out_result;

  { // Node ID: 2563 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2563in_a = id2811out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2563in_b = id2562out_result;

    HWOffsetFix<1,0,UNSIGNED> id2563x_1;

    (id2563x_1) = (and_fixed(id2563in_a,id2563in_b));
    id2563out_result = (id2563x_1);
  }
  { // Node ID: 3228 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3228in_input = id2563out_result;

    id3228out_output[(getCycle()+8)%9] = id3228in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2564 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2564in_output_control = id3228out_output[getCycle()%9];
    const HWFloat<8,24> &id2564in_data = id1831out_result;

    bool id2564x_1;

    (id2564x_1) = ((id2564in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2564x_1)) {
      writeOutput(m_d_out, id2564in_data);
    }
  }
  { // Node ID: 3237 (NodeConstantRawBits)
  }
  { // Node ID: 2567 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2567in_a = id3234out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2567in_b = id3237out_value;

    id2567out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2567in_a,id2567in_b));
  }
  { // Node ID: 2812 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2812in_a = id2912out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2812in_b = id2567out_result[getCycle()%2];

    id2812out_result[(getCycle()+1)%2] = (eq_fixed(id2812in_a,id2812in_b));
  }
  { // Node ID: 2569 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2570out_result;

  { // Node ID: 2570 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2570in_a = id2569out_io_j_out_force_disabled;

    id2570out_result = (not_fixed(id2570in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2571out_result;

  { // Node ID: 2571 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2571in_a = id2812out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2571in_b = id2570out_result;

    HWOffsetFix<1,0,UNSIGNED> id2571x_1;

    (id2571x_1) = (and_fixed(id2571in_a,id2571in_b));
    id2571out_result = (id2571x_1);
  }
  { // Node ID: 3230 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3230in_input = id2571out_result;

    id3230out_output[(getCycle()+9)%10] = id3230in_input;
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2572 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2572in_output_control = id3230out_output[getCycle()%10];
    const HWFloat<8,24> &id2572in_data = id1825out_result;

    bool id2572x_1;

    (id2572x_1) = ((id2572in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2572x_1)) {
      writeOutput(m_j_out, id2572in_data);
    }
  }
  { // Node ID: 2586 (NodeConstantRawBits)
  }
  { // Node ID: 3236 (NodeConstantRawBits)
  }
  { // Node ID: 2583 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (9l)))
  { // Node ID: 2584 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2584in_enable = id3236out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2584in_max = id2583out_value;

    HWOffsetFix<49,0,UNSIGNED> id2584x_1;
    HWOffsetFix<1,0,UNSIGNED> id2584x_2;
    HWOffsetFix<1,0,UNSIGNED> id2584x_3;
    HWOffsetFix<49,0,UNSIGNED> id2584x_4t_1e_1;

    id2584out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2584st_count)));
    (id2584x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2584st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2584x_2) = (gte_fixed((id2584x_1),id2584in_max));
    (id2584x_3) = (and_fixed((id2584x_2),id2584in_enable));
    id2584out_wrap = (id2584x_3);
    if((id2584in_enable.getValueAsBool())) {
      if(((id2584x_3).getValueAsBool())) {
        (id2584st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2584x_4t_1e_1) = (id2584x_1);
        (id2584st_count) = (id2584x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id2585out_output;

  { // Node ID: 2585 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id2585in_input = id2584out_count;

    id2585out_output = id2585in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2587 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id2587in_load = id2586out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id2587in_data = id2585out_output;

    bool id2587x_1;

    (id2587x_1) = ((id2587in_load.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2587x_1)) {
      setMappedRegValue("current_run_cycle_count", id2587in_data);
    }
  }
  { // Node ID: 3235 (NodeConstantRawBits)
  }
  { // Node ID: 2589 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 2590 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2590in_enable = id3235out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2590in_max = id2589out_value;

    HWOffsetFix<49,0,UNSIGNED> id2590x_1;
    HWOffsetFix<1,0,UNSIGNED> id2590x_2;
    HWOffsetFix<1,0,UNSIGNED> id2590x_3;
    HWOffsetFix<49,0,UNSIGNED> id2590x_4t_1e_1;

    id2590out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2590st_count)));
    (id2590x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2590st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2590x_2) = (gte_fixed((id2590x_1),id2590in_max));
    (id2590x_3) = (and_fixed((id2590x_2),id2590in_enable));
    id2590out_wrap = (id2590x_3);
    if((id2590in_enable.getValueAsBool())) {
      if(((id2590x_3).getValueAsBool())) {
        (id2590st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2590x_4t_1e_1) = (id2590x_1);
        (id2590st_count) = (id2590x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 2592 (NodeInputMappedReg)
  }
  { // Node ID: 2813 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id2813in_a = id2590out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id2813in_b = id2592out_run_cycle_count;

    id2813out_result[(getCycle()+1)%2] = (eq_fixed(id2813in_a,id2813in_b));
  }
  if ( (getFillLevel() >= (7l)))
  { // Node ID: 2591 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id2591in_start = id2813out_result[getCycle()%2];

    if((id2591in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
