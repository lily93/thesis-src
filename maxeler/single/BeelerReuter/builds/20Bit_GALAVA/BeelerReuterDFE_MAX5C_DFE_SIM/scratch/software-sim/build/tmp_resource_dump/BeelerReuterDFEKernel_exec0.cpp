#include "stdsimheader.h"

namespace maxcompilersim {

void BeelerReuterDFEKernel::execute0() {
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 3033 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 11 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id11in_enable = id8out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id11in_max = id3033out_value;

    HWOffsetFix<12,0,UNSIGNED> id11x_1;
    HWOffsetFix<1,0,UNSIGNED> id11x_2;
    HWOffsetFix<1,0,UNSIGNED> id11x_3;
    HWOffsetFix<12,0,UNSIGNED> id11x_4t_1e_1;

    id11out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id11st_count)));
    (id11x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id11st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id11x_2) = (gte_fixed((id11x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id11in_max))));
    (id11x_3) = (and_fixed((id11x_2),id11in_enable));
    id11out_wrap = (id11x_3);
    if((id11in_enable.getValueAsBool())) {
      if(((id11x_3).getValueAsBool())) {
        (id11st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id11x_4t_1e_1) = (id11x_1);
        (id11st_count) = (id11x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 2712 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id2712in_input = id11out_count;

    id2712out_output[(getCycle()+1)%2] = id2712in_input;
  }
  { // Node ID: 3298 (NodeConstantRawBits)
  }
  { // Node ID: 2311 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2311in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2311in_b = id3298out_value;

    id2311out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2311in_a,id2311in_b));
  }
  { // Node ID: 2419 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2419in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2419in_b = id2311out_result[getCycle()%2];

    id2419out_result[(getCycle()+1)%2] = (eq_fixed(id2419in_a,id2419in_b));
  }
  { // Node ID: 2313 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2314out_result;

  { // Node ID: 2314 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2314in_a = id2313out_io_u_out_force_disabled;

    id2314out_result = (not_fixed(id2314in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2315out_result;

  { // Node ID: 2315 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2315in_a = id2419out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2315in_b = id2314out_result;

    HWOffsetFix<1,0,UNSIGNED> id2315x_1;

    (id2315x_1) = (and_fixed(id2315in_a,id2315in_b));
    id2315out_result = (id2315x_1);
  }
  { // Node ID: 3015 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3015in_input = id2315out_result;

    id3015out_output[(getCycle()+9)%10] = id3015in_input;
  }
  { // Node ID: 9 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 10 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id10in_enable = id11out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id9out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id10x_1;
    HWOffsetFix<1,0,UNSIGNED> id10x_2;
    HWOffsetFix<1,0,UNSIGNED> id10x_3;
    HWOffsetFix<33,0,UNSIGNED> id10x_4t_1e_1;

    id10out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id10st_count)));
    (id10x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id10st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id10x_2) = (gte_fixed((id10x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id10in_max))));
    (id10x_3) = (and_fixed((id10x_2),id10in_enable));
    id10out_wrap = (id10x_3);
    if((id10in_enable.getValueAsBool())) {
      if(((id10x_3).getValueAsBool())) {
        (id10st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id10x_4t_1e_1) = (id10x_1);
        (id10st_count) = (id10x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 3297 (NodeConstantRawBits)
  }
  { // Node ID: 2420 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2420in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2420in_b = id3297out_value;

    id2420out_result[(getCycle()+1)%2] = (eq_fixed(id2420in_a,id2420in_b));
  }
  { // Node ID: 3296 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1811out_o;

  { // Node ID: 1811 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1811in_i = id2724out_output[getCycle()%3];

    id1811out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1811in_i));
  }
  { // Node ID: 1814 (NodeConstantRawBits)
  }
  { // Node ID: 2616 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id1801out_result;

  { // Node ID: 1801 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1801in_a = id1854out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1801in_b = id2725out_output[getCycle()%3];

    id1801out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1801in_a,id1801in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1802out_result;

  { // Node ID: 1802 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1802in_a = id2725out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1802in_b = id1854out_dout[getCycle()%3];

    id1802out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1802in_a,id1802in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1803out_result;

  { // Node ID: 1803 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1803in_a = id1801out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1803in_b = id1802out_result;

    id1803out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1803in_a,id1803in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1804out_o;

  { // Node ID: 1804 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1804in_i = id1803out_result;

    id1804out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1804in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1805out_o;

  { // Node ID: 1805 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1805in_i = id1804out_o;

    id1805out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1805in_i));
  }
  { // Node ID: 3283 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2430out_result;

  { // Node ID: 2430 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2430in_a = id1805out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2430in_b = id3283out_value;

    id2430out_result = (gte_fixed(id2430in_a,id2430in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2689out_result;

  { // Node ID: 2689 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2689in_a = id1811out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2689in_b = id1814out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2689in_c = id2616out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2689in_condb = id2430out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2689x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2689x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2689x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2689x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2689x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2689x_1 = id2689in_a;
        break;
      default:
        id2689x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2689in_condb.getValueAsLong())) {
      case 0l:
        id2689x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2689x_2 = id2689in_b;
        break;
      default:
        id2689x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2689x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2689x_3 = id2689in_c;
        break;
      default:
        id2689x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2689x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2689x_1),(id2689x_2))),(id2689x_3)));
    id2689out_result = (id2689x_4);
  }
  HWRawBits<1> id2431out_result;

  { // Node ID: 2431 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2431in_a = id2689out_result;

    id2431out_result = (slice<10,1>(id2431in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2432out_output;

  { // Node ID: 2432 (NodeReinterpret)
    const HWRawBits<1> &id2432in_input = id2431out_result;

    id2432out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2432in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1820out_result;

  { // Node ID: 1820 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1820in_a = id2728out_output[getCycle()%3];

    id1820out_result = (not_fixed(id1820in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1821out_result;

  { // Node ID: 1821 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1821in_a = id2432out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1821in_b = id1820out_result;

    HWOffsetFix<1,0,UNSIGNED> id1821x_1;

    (id1821x_1) = (and_fixed(id1821in_a,id1821in_b));
    id1821out_result = (id1821x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1822out_result;

  { // Node ID: 1822 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1822in_a = id1821out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1822in_b = id2730out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1822x_1;

    (id1822x_1) = (or_fixed(id1822in_a,id1822in_b));
    id1822out_result = (id1822x_1);
  }
  { // Node ID: 3280 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2433out_result;

  { // Node ID: 2433 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2433in_a = id2689out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2433in_b = id3280out_value;

    id2433out_result = (gte_fixed(id2433in_a,id2433in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1829out_result;

  { // Node ID: 1829 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1829in_a = id2730out_output[getCycle()%3];

    id1829out_result = (not_fixed(id1829in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1830out_result;

  { // Node ID: 1830 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1830in_a = id2433out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1830in_b = id1829out_result;

    HWOffsetFix<1,0,UNSIGNED> id1830x_1;

    (id1830x_1) = (and_fixed(id1830in_a,id1830in_b));
    id1830out_result = (id1830x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1831out_result;

  { // Node ID: 1831 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1831in_a = id1830out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1831in_b = id2728out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1831x_1;

    (id1831x_1) = (or_fixed(id1831in_a,id1831in_b));
    id1831out_result = (id1831x_1);
  }
  HWRawBits<2> id1832out_result;

  { // Node ID: 1832 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1832in_in0 = id1822out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1832in_in1 = id1831out_result;

    id1832out_result = (cat(id1832in_in0,id1832in_in1));
  }
  { // Node ID: 1824 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1823out_o;

  { // Node ID: 1823 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1823in_i = id2689out_result;

    id1823out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1823in_i));
  }
  { // Node ID: 1808 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1809out_result;

  { // Node ID: 1809 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1809in_sel = id2430out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1809in_option0 = id1805out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1809in_option1 = id1808out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1809x_1;

    switch((id1809in_sel.getValueAsLong())) {
      case 0l:
        id1809x_1 = id1809in_option0;
        break;
      case 1l:
        id1809x_1 = id1809in_option1;
        break;
      default:
        id1809x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1809out_result = (id1809x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1810out_o;

  { // Node ID: 1810 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1810in_i = id1809out_result;

    id1810out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1810in_i));
  }
  HWRawBits<20> id1825out_result;

  { // Node ID: 1825 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1825in_in0 = id1824out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1825in_in1 = id1823out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1825in_in2 = id1810out_o;

    id1825out_result = (cat((cat(id1825in_in0,id1825in_in1)),id1825in_in2));
  }
  HWFloat<8,12> id1826out_output;

  { // Node ID: 1826 (NodeReinterpret)
    const HWRawBits<20> &id1826in_input = id1825out_result;

    id1826out_output = (cast_bits2float<8,12>(id1826in_input));
  }
  { // Node ID: 1833 (NodeConstantRawBits)
  }
  { // Node ID: 1834 (NodeConstantRawBits)
  }
  { // Node ID: 1836 (NodeConstantRawBits)
  }
  HWRawBits<20> id2434out_result;

  { // Node ID: 2434 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2434in_in0 = id1833out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2434in_in1 = id1834out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2434in_in2 = id1836out_value;

    id2434out_result = (cat((cat(id2434in_in0,id2434in_in1)),id2434in_in2));
  }
  HWFloat<8,12> id1838out_output;

  { // Node ID: 1838 (NodeReinterpret)
    const HWRawBits<20> &id1838in_input = id2434out_result;

    id1838out_output = (cast_bits2float<8,12>(id1838in_input));
  }
  { // Node ID: 2395 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1841out_result;

  { // Node ID: 1841 (NodeMux)
    const HWRawBits<2> &id1841in_sel = id1832out_result;
    const HWFloat<8,12> &id1841in_option0 = id1826out_output;
    const HWFloat<8,12> &id1841in_option1 = id1838out_output;
    const HWFloat<8,12> &id1841in_option2 = id2395out_value;
    const HWFloat<8,12> &id1841in_option3 = id1838out_output;

    HWFloat<8,12> id1841x_1;

    switch((id1841in_sel.getValueAsLong())) {
      case 0l:
        id1841x_1 = id1841in_option0;
        break;
      case 1l:
        id1841x_1 = id1841in_option1;
        break;
      case 2l:
        id1841x_1 = id1841in_option2;
        break;
      case 3l:
        id1841x_1 = id1841in_option3;
        break;
      default:
        id1841x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1841out_result = (id1841x_1);
  }
  { // Node ID: 3279 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1851out_result;

  { // Node ID: 1851 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1851in_sel = id2733out_output[getCycle()%9];
    const HWFloat<8,12> &id1851in_option0 = id1841out_result;
    const HWFloat<8,12> &id1851in_option1 = id3279out_value;

    HWFloat<8,12> id1851x_1;

    switch((id1851in_sel.getValueAsLong())) {
      case 0l:
        id1851x_1 = id1851in_option0;
        break;
      case 1l:
        id1851x_1 = id1851in_option1;
        break;
      default:
        id1851x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1851out_result = (id1851x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1892out_o;

  { // Node ID: 1892 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1892in_i = id2735out_output[getCycle()%3];

    id1892out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1892in_i));
  }
  { // Node ID: 1895 (NodeConstantRawBits)
  }
  { // Node ID: 2618 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id1882out_result;

  { // Node ID: 1882 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1882in_a = id1935out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1882in_b = id2736out_output[getCycle()%3];

    id1882out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1882in_a,id1882in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1883out_result;

  { // Node ID: 1883 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1883in_a = id2736out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1883in_b = id1935out_dout[getCycle()%3];

    id1883out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1883in_a,id1883in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1884out_result;

  { // Node ID: 1884 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1884in_a = id1882out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1884in_b = id1883out_result;

    id1884out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1884in_a,id1884in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1885out_o;

  { // Node ID: 1885 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1885in_i = id1884out_result;

    id1885out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1885in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1886out_o;

  { // Node ID: 1886 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1886in_i = id1885out_o;

    id1886out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1886in_i));
  }
  { // Node ID: 3275 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2437out_result;

  { // Node ID: 2437 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2437in_a = id1886out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2437in_b = id3275out_value;

    id2437out_result = (gte_fixed(id2437in_a,id2437in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2688out_result;

  { // Node ID: 2688 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2688in_a = id1892out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2688in_b = id1895out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2688in_c = id2618out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2688in_condb = id2437out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2688x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2688x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2688x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2688x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2688x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2688x_1 = id2688in_a;
        break;
      default:
        id2688x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2688in_condb.getValueAsLong())) {
      case 0l:
        id2688x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2688x_2 = id2688in_b;
        break;
      default:
        id2688x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2688x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2688x_3 = id2688in_c;
        break;
      default:
        id2688x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2688x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2688x_1),(id2688x_2))),(id2688x_3)));
    id2688out_result = (id2688x_4);
  }
  HWRawBits<1> id2438out_result;

  { // Node ID: 2438 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2438in_a = id2688out_result;

    id2438out_result = (slice<10,1>(id2438in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2439out_output;

  { // Node ID: 2439 (NodeReinterpret)
    const HWRawBits<1> &id2439in_input = id2438out_result;

    id2439out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2439in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1901out_result;

  { // Node ID: 1901 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1901in_a = id2739out_output[getCycle()%3];

    id1901out_result = (not_fixed(id1901in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1902out_result;

  { // Node ID: 1902 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1902in_a = id2439out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1902in_b = id1901out_result;

    HWOffsetFix<1,0,UNSIGNED> id1902x_1;

    (id1902x_1) = (and_fixed(id1902in_a,id1902in_b));
    id1902out_result = (id1902x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1903out_result;

  { // Node ID: 1903 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1903in_a = id1902out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1903in_b = id2741out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1903x_1;

    (id1903x_1) = (or_fixed(id1903in_a,id1903in_b));
    id1903out_result = (id1903x_1);
  }
  { // Node ID: 3272 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2440out_result;

  { // Node ID: 2440 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2440in_a = id2688out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2440in_b = id3272out_value;

    id2440out_result = (gte_fixed(id2440in_a,id2440in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1910out_result;

  { // Node ID: 1910 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1910in_a = id2741out_output[getCycle()%3];

    id1910out_result = (not_fixed(id1910in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1911out_result;

  { // Node ID: 1911 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1911in_a = id2440out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1911in_b = id1910out_result;

    HWOffsetFix<1,0,UNSIGNED> id1911x_1;

    (id1911x_1) = (and_fixed(id1911in_a,id1911in_b));
    id1911out_result = (id1911x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1912out_result;

  { // Node ID: 1912 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1912in_a = id1911out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1912in_b = id2739out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1912x_1;

    (id1912x_1) = (or_fixed(id1912in_a,id1912in_b));
    id1912out_result = (id1912x_1);
  }
  HWRawBits<2> id1913out_result;

  { // Node ID: 1913 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1913in_in0 = id1903out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1913in_in1 = id1912out_result;

    id1913out_result = (cat(id1913in_in0,id1913in_in1));
  }
  { // Node ID: 1905 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1904out_o;

  { // Node ID: 1904 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1904in_i = id2688out_result;

    id1904out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1904in_i));
  }
  { // Node ID: 1889 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1890out_result;

  { // Node ID: 1890 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1890in_sel = id2437out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1890in_option0 = id1886out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1890in_option1 = id1889out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1890x_1;

    switch((id1890in_sel.getValueAsLong())) {
      case 0l:
        id1890x_1 = id1890in_option0;
        break;
      case 1l:
        id1890x_1 = id1890in_option1;
        break;
      default:
        id1890x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1890out_result = (id1890x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1891out_o;

  { // Node ID: 1891 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1891in_i = id1890out_result;

    id1891out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1891in_i));
  }
  HWRawBits<20> id1906out_result;

  { // Node ID: 1906 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1906in_in0 = id1905out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1906in_in1 = id1904out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1906in_in2 = id1891out_o;

    id1906out_result = (cat((cat(id1906in_in0,id1906in_in1)),id1906in_in2));
  }
  HWFloat<8,12> id1907out_output;

  { // Node ID: 1907 (NodeReinterpret)
    const HWRawBits<20> &id1907in_input = id1906out_result;

    id1907out_output = (cast_bits2float<8,12>(id1907in_input));
  }
  { // Node ID: 1914 (NodeConstantRawBits)
  }
  { // Node ID: 1915 (NodeConstantRawBits)
  }
  { // Node ID: 1917 (NodeConstantRawBits)
  }
  HWRawBits<20> id2441out_result;

  { // Node ID: 2441 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2441in_in0 = id1914out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2441in_in1 = id1915out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2441in_in2 = id1917out_value;

    id2441out_result = (cat((cat(id2441in_in0,id2441in_in1)),id2441in_in2));
  }
  HWFloat<8,12> id1919out_output;

  { // Node ID: 1919 (NodeReinterpret)
    const HWRawBits<20> &id1919in_input = id2441out_result;

    id1919out_output = (cast_bits2float<8,12>(id1919in_input));
  }
  { // Node ID: 2396 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1922out_result;

  { // Node ID: 1922 (NodeMux)
    const HWRawBits<2> &id1922in_sel = id1913out_result;
    const HWFloat<8,12> &id1922in_option0 = id1907out_output;
    const HWFloat<8,12> &id1922in_option1 = id1919out_output;
    const HWFloat<8,12> &id1922in_option2 = id2396out_value;
    const HWFloat<8,12> &id1922in_option3 = id1919out_output;

    HWFloat<8,12> id1922x_1;

    switch((id1922in_sel.getValueAsLong())) {
      case 0l:
        id1922x_1 = id1922in_option0;
        break;
      case 1l:
        id1922x_1 = id1922in_option1;
        break;
      case 2l:
        id1922x_1 = id1922in_option2;
        break;
      case 3l:
        id1922x_1 = id1922in_option3;
        break;
      default:
        id1922x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1922out_result = (id1922x_1);
  }
  { // Node ID: 3271 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1932out_result;

  { // Node ID: 1932 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1932in_sel = id2744out_output[getCycle()%9];
    const HWFloat<8,12> &id1932in_option0 = id1922out_result;
    const HWFloat<8,12> &id1932in_option1 = id3271out_value;

    HWFloat<8,12> id1932x_1;

    switch((id1932in_sel.getValueAsLong())) {
      case 0l:
        id1932x_1 = id1932in_option0;
        break;
      case 1l:
        id1932x_1 = id1932in_option1;
        break;
      default:
        id1932x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1932out_result = (id1932x_1);
  }
  HWFloat<8,12> id1936out_result;

  { // Node ID: 1936 (NodeAdd)
    const HWFloat<8,12> &id1936in_a = id1851out_result;
    const HWFloat<8,12> &id1936in_b = id1932out_result;

    id1936out_result = (add_float(id1936in_a,id1936in_b));
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2020out_result;

  { // Node ID: 2020 (NodeEq)
    const HWFloat<8,12> &id2020in_a = id1936out_result;
    const HWFloat<8,12> &id2020in_b = id5out_value;

    id2020out_result = (eq_float(id2020in_a,id2020in_b));
  }
  { // Node ID: 2021 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2022out_result;

  { // Node ID: 2022 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2022in_sel = id2020out_result;
    const HWFloat<8,12> &id2022in_option0 = id1936out_result;
    const HWFloat<8,12> &id2022in_option1 = id2021out_value;

    HWFloat<8,12> id2022x_1;

    switch((id2022in_sel.getValueAsLong())) {
      case 0l:
        id2022x_1 = id2022in_option0;
        break;
      case 1l:
        id2022x_1 = id2022in_option1;
        break;
      default:
        id2022x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2022out_result = (id2022x_1);
  }
  HWFloat<8,12> id2111out_result;

  { // Node ID: 2111 (NodeDiv)
    const HWFloat<8,12> &id2111in_a = id2691out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id2111in_b = id2022out_result;

    id2111out_result = (div_float(id2111in_a,id2111in_b));
  }
  { // Node ID: 3270 (NodeConstantRawBits)
  }
  { // Node ID: 3269 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2113out_result;

  { // Node ID: 2113 (NodeAdd)
    const HWFloat<8,12> &id2113in_a = id3031out_output[getCycle()%2];
    const HWFloat<8,12> &id2113in_b = id3269out_value;

    id2113out_result = (add_float(id2113in_a,id2113in_b));
  }
  HWFloat<8,12> id2115out_result;

  { // Node ID: 2115 (NodeMul)
    const HWFloat<8,12> &id2115in_a = id3270out_value;
    const HWFloat<8,12> &id2115in_b = id2113out_result;

    id2115out_result = (mul_float(id2115in_a,id2115in_b));
  }
  { // Node ID: 3268 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1974out_o;

  { // Node ID: 1974 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1974in_i = id2747out_output[getCycle()%3];

    id1974out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1974in_i));
  }
  { // Node ID: 1977 (NodeConstantRawBits)
  }
  { // Node ID: 2620 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id1964out_result;

  { // Node ID: 1964 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1964in_a = id2017out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1964in_b = id2748out_output[getCycle()%3];

    id1964out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1964in_a,id1964in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1965out_result;

  { // Node ID: 1965 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1965in_a = id2748out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1965in_b = id2017out_dout[getCycle()%3];

    id1965out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1965in_a,id1965in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1966out_result;

  { // Node ID: 1966 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1966in_a = id1964out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1966in_b = id1965out_result;

    id1966out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1966in_a,id1966in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1967out_o;

  { // Node ID: 1967 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1967in_i = id1966out_result;

    id1967out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1967in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1968out_o;

  { // Node ID: 1968 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1968in_i = id1967out_o;

    id1968out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1968in_i));
  }
  { // Node ID: 3264 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2444out_result;

  { // Node ID: 2444 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2444in_a = id1968out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2444in_b = id3264out_value;

    id2444out_result = (gte_fixed(id2444in_a,id2444in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2687out_result;

  { // Node ID: 2687 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2687in_a = id1974out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2687in_b = id1977out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2687in_c = id2620out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2687in_condb = id2444out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2687x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2687x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2687x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2687x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2687x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2687x_1 = id2687in_a;
        break;
      default:
        id2687x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2687in_condb.getValueAsLong())) {
      case 0l:
        id2687x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2687x_2 = id2687in_b;
        break;
      default:
        id2687x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2687x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2687x_3 = id2687in_c;
        break;
      default:
        id2687x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2687x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2687x_1),(id2687x_2))),(id2687x_3)));
    id2687out_result = (id2687x_4);
  }
  HWRawBits<1> id2445out_result;

  { // Node ID: 2445 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2445in_a = id2687out_result;

    id2445out_result = (slice<10,1>(id2445in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2446out_output;

  { // Node ID: 2446 (NodeReinterpret)
    const HWRawBits<1> &id2446in_input = id2445out_result;

    id2446out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2446in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1983out_result;

  { // Node ID: 1983 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1983in_a = id2751out_output[getCycle()%3];

    id1983out_result = (not_fixed(id1983in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1984out_result;

  { // Node ID: 1984 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1984in_a = id2446out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1984in_b = id1983out_result;

    HWOffsetFix<1,0,UNSIGNED> id1984x_1;

    (id1984x_1) = (and_fixed(id1984in_a,id1984in_b));
    id1984out_result = (id1984x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1985out_result;

  { // Node ID: 1985 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1985in_a = id1984out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1985in_b = id2753out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1985x_1;

    (id1985x_1) = (or_fixed(id1985in_a,id1985in_b));
    id1985out_result = (id1985x_1);
  }
  { // Node ID: 3261 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2447out_result;

  { // Node ID: 2447 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2447in_a = id2687out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2447in_b = id3261out_value;

    id2447out_result = (gte_fixed(id2447in_a,id2447in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1992out_result;

  { // Node ID: 1992 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1992in_a = id2753out_output[getCycle()%3];

    id1992out_result = (not_fixed(id1992in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1993out_result;

  { // Node ID: 1993 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1993in_a = id2447out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1993in_b = id1992out_result;

    HWOffsetFix<1,0,UNSIGNED> id1993x_1;

    (id1993x_1) = (and_fixed(id1993in_a,id1993in_b));
    id1993out_result = (id1993x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1994out_result;

  { // Node ID: 1994 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1994in_a = id1993out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1994in_b = id2751out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1994x_1;

    (id1994x_1) = (or_fixed(id1994in_a,id1994in_b));
    id1994out_result = (id1994x_1);
  }
  HWRawBits<2> id1995out_result;

  { // Node ID: 1995 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1995in_in0 = id1985out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1995in_in1 = id1994out_result;

    id1995out_result = (cat(id1995in_in0,id1995in_in1));
  }
  { // Node ID: 1987 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1986out_o;

  { // Node ID: 1986 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1986in_i = id2687out_result;

    id1986out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1986in_i));
  }
  { // Node ID: 1971 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1972out_result;

  { // Node ID: 1972 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1972in_sel = id2444out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1972in_option0 = id1968out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1972in_option1 = id1971out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1972x_1;

    switch((id1972in_sel.getValueAsLong())) {
      case 0l:
        id1972x_1 = id1972in_option0;
        break;
      case 1l:
        id1972x_1 = id1972in_option1;
        break;
      default:
        id1972x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1972out_result = (id1972x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1973out_o;

  { // Node ID: 1973 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1973in_i = id1972out_result;

    id1973out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1973in_i));
  }
  HWRawBits<20> id1988out_result;

  { // Node ID: 1988 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1988in_in0 = id1987out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1988in_in1 = id1986out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1988in_in2 = id1973out_o;

    id1988out_result = (cat((cat(id1988in_in0,id1988in_in1)),id1988in_in2));
  }
  HWFloat<8,12> id1989out_output;

  { // Node ID: 1989 (NodeReinterpret)
    const HWRawBits<20> &id1989in_input = id1988out_result;

    id1989out_output = (cast_bits2float<8,12>(id1989in_input));
  }
  { // Node ID: 1996 (NodeConstantRawBits)
  }
  { // Node ID: 1997 (NodeConstantRawBits)
  }
  { // Node ID: 1999 (NodeConstantRawBits)
  }
  HWRawBits<20> id2448out_result;

  { // Node ID: 2448 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2448in_in0 = id1996out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2448in_in1 = id1997out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2448in_in2 = id1999out_value;

    id2448out_result = (cat((cat(id2448in_in0,id2448in_in1)),id2448in_in2));
  }
  HWFloat<8,12> id2001out_output;

  { // Node ID: 2001 (NodeReinterpret)
    const HWRawBits<20> &id2001in_input = id2448out_result;

    id2001out_output = (cast_bits2float<8,12>(id2001in_input));
  }
  { // Node ID: 2397 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2004out_result;

  { // Node ID: 2004 (NodeMux)
    const HWRawBits<2> &id2004in_sel = id1995out_result;
    const HWFloat<8,12> &id2004in_option0 = id1989out_output;
    const HWFloat<8,12> &id2004in_option1 = id2001out_output;
    const HWFloat<8,12> &id2004in_option2 = id2397out_value;
    const HWFloat<8,12> &id2004in_option3 = id2001out_output;

    HWFloat<8,12> id2004x_1;

    switch((id2004in_sel.getValueAsLong())) {
      case 0l:
        id2004x_1 = id2004in_option0;
        break;
      case 1l:
        id2004x_1 = id2004in_option1;
        break;
      case 2l:
        id2004x_1 = id2004in_option2;
        break;
      case 3l:
        id2004x_1 = id2004in_option3;
        break;
      default:
        id2004x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2004out_result = (id2004x_1);
  }
  { // Node ID: 3260 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2014out_result;

  { // Node ID: 2014 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2014in_sel = id2756out_output[getCycle()%9];
    const HWFloat<8,12> &id2014in_option0 = id2004out_result;
    const HWFloat<8,12> &id2014in_option1 = id3260out_value;

    HWFloat<8,12> id2014x_1;

    switch((id2014in_sel.getValueAsLong())) {
      case 0l:
        id2014x_1 = id2014in_option0;
        break;
      case 1l:
        id2014x_1 = id2014in_option1;
        break;
      default:
        id2014x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2014out_result = (id2014x_1);
  }
  HWFloat<8,12> id2019out_result;

  { // Node ID: 2019 (NodeSub)
    const HWFloat<8,12> &id2019in_a = id3268out_value;
    const HWFloat<8,12> &id2019in_b = id2014out_result;

    id2019out_result = (sub_float(id2019in_a,id2019in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2023out_result;

  { // Node ID: 2023 (NodeEq)
    const HWFloat<8,12> &id2023in_a = id2019out_result;
    const HWFloat<8,12> &id2023in_b = id5out_value;

    id2023out_result = (eq_float(id2023in_a,id2023in_b));
  }
  { // Node ID: 2024 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2025out_result;

  { // Node ID: 2025 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2025in_sel = id2023out_result;
    const HWFloat<8,12> &id2025in_option0 = id2019out_result;
    const HWFloat<8,12> &id2025in_option1 = id2024out_value;

    HWFloat<8,12> id2025x_1;

    switch((id2025in_sel.getValueAsLong())) {
      case 0l:
        id2025x_1 = id2025in_option0;
        break;
      case 1l:
        id2025x_1 = id2025in_option1;
        break;
      default:
        id2025x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2025out_result = (id2025x_1);
  }
  HWFloat<8,12> id2116out_result;

  { // Node ID: 2116 (NodeDiv)
    const HWFloat<8,12> &id2116in_a = id2115out_result;
    const HWFloat<8,12> &id2116in_b = id2025out_result;

    id2116out_result = (div_float(id2116in_a,id2116in_b));
  }
  HWFloat<8,12> id2117out_result;

  { // Node ID: 2117 (NodeAdd)
    const HWFloat<8,12> &id2117in_a = id2111out_result;
    const HWFloat<8,12> &id2117in_b = id2116out_result;

    id2117out_result = (add_float(id2117in_a,id2117in_b));
  }
  HWFloat<8,12> id2119out_result;

  { // Node ID: 2119 (NodeMul)
    const HWFloat<8,12> &id2119in_a = id3296out_value;
    const HWFloat<8,12> &id2119in_b = id2117out_result;

    id2119out_result = (mul_float(id2119in_a,id2119in_b));
  }
  { // Node ID: 3259 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2157out_o;

  { // Node ID: 2157 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2157in_i = id2758out_output[getCycle()%3];

    id2157out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2157in_i));
  }
  { // Node ID: 2160 (NodeConstantRawBits)
  }
  { // Node ID: 2622 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id2147out_result;

  { // Node ID: 2147 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id2147in_a = id2200out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id2147in_b = id2759out_output[getCycle()%3];

    id2147out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id2147in_a,id2147in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id2148out_result;

  { // Node ID: 2148 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id2148in_a = id2759out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id2148in_b = id2200out_dout[getCycle()%3];

    id2148out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id2148in_a,id2148in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2149out_result;

  { // Node ID: 2149 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id2149in_a = id2147out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id2149in_b = id2148out_result;

    id2149out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2149in_a,id2149in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id2150out_o;

  { // Node ID: 2150 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id2150in_i = id2149out_result;

    id2150out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id2150in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id2151out_o;

  { // Node ID: 2151 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id2151in_i = id2150out_o;

    id2151out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id2151in_i));
  }
  { // Node ID: 3255 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2451out_result;

  { // Node ID: 2451 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2451in_a = id2151out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2451in_b = id3255out_value;

    id2451out_result = (gte_fixed(id2451in_a,id2451in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2686out_result;

  { // Node ID: 2686 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2686in_a = id2157out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2686in_b = id2160out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2686in_c = id2622out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2686in_condb = id2451out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2686x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2686x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2686x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2686x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2686x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2686x_1 = id2686in_a;
        break;
      default:
        id2686x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2686in_condb.getValueAsLong())) {
      case 0l:
        id2686x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2686x_2 = id2686in_b;
        break;
      default:
        id2686x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2686x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2686x_3 = id2686in_c;
        break;
      default:
        id2686x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2686x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2686x_1),(id2686x_2))),(id2686x_3)));
    id2686out_result = (id2686x_4);
  }
  HWRawBits<1> id2452out_result;

  { // Node ID: 2452 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2452in_a = id2686out_result;

    id2452out_result = (slice<10,1>(id2452in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2453out_output;

  { // Node ID: 2453 (NodeReinterpret)
    const HWRawBits<1> &id2453in_input = id2452out_result;

    id2453out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2453in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2166out_result;

  { // Node ID: 2166 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2166in_a = id2762out_output[getCycle()%3];

    id2166out_result = (not_fixed(id2166in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2167out_result;

  { // Node ID: 2167 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2167in_a = id2453out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2167in_b = id2166out_result;

    HWOffsetFix<1,0,UNSIGNED> id2167x_1;

    (id2167x_1) = (and_fixed(id2167in_a,id2167in_b));
    id2167out_result = (id2167x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2168out_result;

  { // Node ID: 2168 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2168in_a = id2167out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2168in_b = id2764out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2168x_1;

    (id2168x_1) = (or_fixed(id2168in_a,id2168in_b));
    id2168out_result = (id2168x_1);
  }
  { // Node ID: 3252 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2454out_result;

  { // Node ID: 2454 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2454in_a = id2686out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2454in_b = id3252out_value;

    id2454out_result = (gte_fixed(id2454in_a,id2454in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2175out_result;

  { // Node ID: 2175 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2175in_a = id2764out_output[getCycle()%3];

    id2175out_result = (not_fixed(id2175in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2176out_result;

  { // Node ID: 2176 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2176in_a = id2454out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2176in_b = id2175out_result;

    HWOffsetFix<1,0,UNSIGNED> id2176x_1;

    (id2176x_1) = (and_fixed(id2176in_a,id2176in_b));
    id2176out_result = (id2176x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2177out_result;

  { // Node ID: 2177 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2177in_a = id2176out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2177in_b = id2762out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2177x_1;

    (id2177x_1) = (or_fixed(id2177in_a,id2177in_b));
    id2177out_result = (id2177x_1);
  }
  HWRawBits<2> id2178out_result;

  { // Node ID: 2178 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2178in_in0 = id2168out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2178in_in1 = id2177out_result;

    id2178out_result = (cat(id2178in_in0,id2178in_in1));
  }
  { // Node ID: 2170 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2169out_o;

  { // Node ID: 2169 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2169in_i = id2686out_result;

    id2169out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2169in_i));
  }
  { // Node ID: 2154 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id2155out_result;

  { // Node ID: 2155 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2155in_sel = id2451out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id2155in_option0 = id2151out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2155in_option1 = id2154out_value;

    HWOffsetFix<12,-11,UNSIGNED> id2155x_1;

    switch((id2155in_sel.getValueAsLong())) {
      case 0l:
        id2155x_1 = id2155in_option0;
        break;
      case 1l:
        id2155x_1 = id2155in_option1;
        break;
      default:
        id2155x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id2155out_result = (id2155x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id2156out_o;

  { // Node ID: 2156 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id2156in_i = id2155out_result;

    id2156out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id2156in_i));
  }
  HWRawBits<20> id2171out_result;

  { // Node ID: 2171 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2171in_in0 = id2170out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2171in_in1 = id2169out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id2171in_in2 = id2156out_o;

    id2171out_result = (cat((cat(id2171in_in0,id2171in_in1)),id2171in_in2));
  }
  HWFloat<8,12> id2172out_output;

  { // Node ID: 2172 (NodeReinterpret)
    const HWRawBits<20> &id2172in_input = id2171out_result;

    id2172out_output = (cast_bits2float<8,12>(id2172in_input));
  }
  { // Node ID: 2179 (NodeConstantRawBits)
  }
  { // Node ID: 2180 (NodeConstantRawBits)
  }
  { // Node ID: 2182 (NodeConstantRawBits)
  }
  HWRawBits<20> id2455out_result;

  { // Node ID: 2455 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2455in_in0 = id2179out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2455in_in1 = id2180out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2455in_in2 = id2182out_value;

    id2455out_result = (cat((cat(id2455in_in0,id2455in_in1)),id2455in_in2));
  }
  HWFloat<8,12> id2184out_output;

  { // Node ID: 2184 (NodeReinterpret)
    const HWRawBits<20> &id2184in_input = id2455out_result;

    id2184out_output = (cast_bits2float<8,12>(id2184in_input));
  }
  { // Node ID: 2398 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2187out_result;

  { // Node ID: 2187 (NodeMux)
    const HWRawBits<2> &id2187in_sel = id2178out_result;
    const HWFloat<8,12> &id2187in_option0 = id2172out_output;
    const HWFloat<8,12> &id2187in_option1 = id2184out_output;
    const HWFloat<8,12> &id2187in_option2 = id2398out_value;
    const HWFloat<8,12> &id2187in_option3 = id2184out_output;

    HWFloat<8,12> id2187x_1;

    switch((id2187in_sel.getValueAsLong())) {
      case 0l:
        id2187x_1 = id2187in_option0;
        break;
      case 1l:
        id2187x_1 = id2187in_option1;
        break;
      case 2l:
        id2187x_1 = id2187in_option2;
        break;
      case 3l:
        id2187x_1 = id2187in_option3;
        break;
      default:
        id2187x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2187out_result = (id2187x_1);
  }
  { // Node ID: 3251 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2197out_result;

  { // Node ID: 2197 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2197in_sel = id2767out_output[getCycle()%9];
    const HWFloat<8,12> &id2197in_option0 = id2187out_result;
    const HWFloat<8,12> &id2197in_option1 = id3251out_value;

    HWFloat<8,12> id2197x_1;

    switch((id2197in_sel.getValueAsLong())) {
      case 0l:
        id2197x_1 = id2197in_option0;
        break;
      case 1l:
        id2197x_1 = id2197in_option1;
        break;
      default:
        id2197x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2197out_result = (id2197x_1);
  }
  { // Node ID: 3250 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2202out_result;

  { // Node ID: 2202 (NodeSub)
    const HWFloat<8,12> &id2202in_a = id2197out_result;
    const HWFloat<8,12> &id2202in_b = id3250out_value;

    id2202out_result = (sub_float(id2202in_a,id2202in_b));
  }
  HWFloat<8,12> id2204out_result;

  { // Node ID: 2204 (NodeMul)
    const HWFloat<8,12> &id2204in_a = id3259out_value;
    const HWFloat<8,12> &id2204in_b = id2202out_result;

    id2204out_result = (mul_float(id2204in_a,id2204in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2242out_o;

  { // Node ID: 2242 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2242in_i = id2769out_output[getCycle()%3];

    id2242out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2242in_i));
  }
  { // Node ID: 2245 (NodeConstantRawBits)
  }
  { // Node ID: 2624 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id2232out_result;

  { // Node ID: 2232 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id2232in_a = id2285out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id2232in_b = id2770out_output[getCycle()%3];

    id2232out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id2232in_a,id2232in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id2233out_result;

  { // Node ID: 2233 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id2233in_a = id2770out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id2233in_b = id2285out_dout[getCycle()%3];

    id2233out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id2233in_a,id2233in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2234out_result;

  { // Node ID: 2234 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id2234in_a = id2232out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id2234in_b = id2233out_result;

    id2234out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2234in_a,id2234in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id2235out_o;

  { // Node ID: 2235 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id2235in_i = id2234out_result;

    id2235out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id2235in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id2236out_o;

  { // Node ID: 2236 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id2236in_i = id2235out_o;

    id2236out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id2236in_i));
  }
  { // Node ID: 3246 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2458out_result;

  { // Node ID: 2458 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2458in_a = id2236out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2458in_b = id3246out_value;

    id2458out_result = (gte_fixed(id2458in_a,id2458in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2685out_result;

  { // Node ID: 2685 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2685in_a = id2242out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2685in_b = id2245out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2685in_c = id2624out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2685in_condb = id2458out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2685x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2685x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2685x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2685x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2685x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2685x_1 = id2685in_a;
        break;
      default:
        id2685x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2685in_condb.getValueAsLong())) {
      case 0l:
        id2685x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2685x_2 = id2685in_b;
        break;
      default:
        id2685x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2685x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2685x_3 = id2685in_c;
        break;
      default:
        id2685x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2685x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2685x_1),(id2685x_2))),(id2685x_3)));
    id2685out_result = (id2685x_4);
  }
  HWRawBits<1> id2459out_result;

  { // Node ID: 2459 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2459in_a = id2685out_result;

    id2459out_result = (slice<10,1>(id2459in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2460out_output;

  { // Node ID: 2460 (NodeReinterpret)
    const HWRawBits<1> &id2460in_input = id2459out_result;

    id2460out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2460in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2251out_result;

  { // Node ID: 2251 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2251in_a = id2773out_output[getCycle()%3];

    id2251out_result = (not_fixed(id2251in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2252out_result;

  { // Node ID: 2252 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2252in_a = id2460out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2252in_b = id2251out_result;

    HWOffsetFix<1,0,UNSIGNED> id2252x_1;

    (id2252x_1) = (and_fixed(id2252in_a,id2252in_b));
    id2252out_result = (id2252x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2253out_result;

  { // Node ID: 2253 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2253in_a = id2252out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2253in_b = id2775out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2253x_1;

    (id2253x_1) = (or_fixed(id2253in_a,id2253in_b));
    id2253out_result = (id2253x_1);
  }
  { // Node ID: 3243 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2461out_result;

  { // Node ID: 2461 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2461in_a = id2685out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2461in_b = id3243out_value;

    id2461out_result = (gte_fixed(id2461in_a,id2461in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2260out_result;

  { // Node ID: 2260 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2260in_a = id2775out_output[getCycle()%3];

    id2260out_result = (not_fixed(id2260in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2261out_result;

  { // Node ID: 2261 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2261in_a = id2461out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2261in_b = id2260out_result;

    HWOffsetFix<1,0,UNSIGNED> id2261x_1;

    (id2261x_1) = (and_fixed(id2261in_a,id2261in_b));
    id2261out_result = (id2261x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2262out_result;

  { // Node ID: 2262 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2262in_a = id2261out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2262in_b = id2773out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2262x_1;

    (id2262x_1) = (or_fixed(id2262in_a,id2262in_b));
    id2262out_result = (id2262x_1);
  }
  HWRawBits<2> id2263out_result;

  { // Node ID: 2263 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2263in_in0 = id2253out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2263in_in1 = id2262out_result;

    id2263out_result = (cat(id2263in_in0,id2263in_in1));
  }
  { // Node ID: 2255 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2254out_o;

  { // Node ID: 2254 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2254in_i = id2685out_result;

    id2254out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2254in_i));
  }
  { // Node ID: 2239 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id2240out_result;

  { // Node ID: 2240 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2240in_sel = id2458out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id2240in_option0 = id2236out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2240in_option1 = id2239out_value;

    HWOffsetFix<12,-11,UNSIGNED> id2240x_1;

    switch((id2240in_sel.getValueAsLong())) {
      case 0l:
        id2240x_1 = id2240in_option0;
        break;
      case 1l:
        id2240x_1 = id2240in_option1;
        break;
      default:
        id2240x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id2240out_result = (id2240x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id2241out_o;

  { // Node ID: 2241 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id2241in_i = id2240out_result;

    id2241out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id2241in_i));
  }
  HWRawBits<20> id2256out_result;

  { // Node ID: 2256 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2256in_in0 = id2255out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2256in_in1 = id2254out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id2256in_in2 = id2241out_o;

    id2256out_result = (cat((cat(id2256in_in0,id2256in_in1)),id2256in_in2));
  }
  HWFloat<8,12> id2257out_output;

  { // Node ID: 2257 (NodeReinterpret)
    const HWRawBits<20> &id2257in_input = id2256out_result;

    id2257out_output = (cast_bits2float<8,12>(id2257in_input));
  }
  { // Node ID: 2264 (NodeConstantRawBits)
  }
  { // Node ID: 2265 (NodeConstantRawBits)
  }
  { // Node ID: 2267 (NodeConstantRawBits)
  }
  HWRawBits<20> id2462out_result;

  { // Node ID: 2462 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2462in_in0 = id2264out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2462in_in1 = id2265out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2462in_in2 = id2267out_value;

    id2462out_result = (cat((cat(id2462in_in0,id2462in_in1)),id2462in_in2));
  }
  HWFloat<8,12> id2269out_output;

  { // Node ID: 2269 (NodeReinterpret)
    const HWRawBits<20> &id2269in_input = id2462out_result;

    id2269out_output = (cast_bits2float<8,12>(id2269in_input));
  }
  { // Node ID: 2399 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2272out_result;

  { // Node ID: 2272 (NodeMux)
    const HWRawBits<2> &id2272in_sel = id2263out_result;
    const HWFloat<8,12> &id2272in_option0 = id2257out_output;
    const HWFloat<8,12> &id2272in_option1 = id2269out_output;
    const HWFloat<8,12> &id2272in_option2 = id2399out_value;
    const HWFloat<8,12> &id2272in_option3 = id2269out_output;

    HWFloat<8,12> id2272x_1;

    switch((id2272in_sel.getValueAsLong())) {
      case 0l:
        id2272x_1 = id2272in_option0;
        break;
      case 1l:
        id2272x_1 = id2272in_option1;
        break;
      case 2l:
        id2272x_1 = id2272in_option2;
        break;
      case 3l:
        id2272x_1 = id2272in_option3;
        break;
      default:
        id2272x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2272out_result = (id2272x_1);
  }
  { // Node ID: 3242 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2282out_result;

  { // Node ID: 2282 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2282in_sel = id2778out_output[getCycle()%9];
    const HWFloat<8,12> &id2282in_option0 = id2272out_result;
    const HWFloat<8,12> &id2282in_option1 = id3242out_value;

    HWFloat<8,12> id2282x_1;

    switch((id2282in_sel.getValueAsLong())) {
      case 0l:
        id2282x_1 = id2282in_option0;
        break;
      case 1l:
        id2282x_1 = id2282in_option1;
        break;
      default:
        id2282x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2282out_result = (id2282x_1);
  }
  HWFloat<8,12> id2286out_result;

  { // Node ID: 2286 (NodeDiv)
    const HWFloat<8,12> &id2286in_a = id2204out_result;
    const HWFloat<8,12> &id2286in_b = id2282out_result;

    id2286out_result = (div_float(id2286in_a,id2286in_b));
  }
  HWFloat<8,12> id2287out_result;

  { // Node ID: 2287 (NodeMul)
    const HWFloat<8,12> &id2287in_a = id2286out_result;
    const HWFloat<8,12> &id2287in_b = id2779out_output[getCycle()%2];

    id2287out_result = (mul_float(id2287in_a,id2287in_b));
  }
  HWFloat<8,12> id2296out_result;

  { // Node ID: 2296 (NodeAdd)
    const HWFloat<8,12> &id2296in_a = id2119out_result;
    const HWFloat<8,12> &id2296in_b = id2287out_result;

    id2296out_result = (add_float(id2296in_a,id2296in_b));
  }
  HWFloat<8,12> id2289out_result;

  { // Node ID: 2289 (NodeMul)
    const HWFloat<8,12> &id2289in_a = id2692out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id2289in_b = id2824out_output[getCycle()%2];

    id2289out_result = (mul_float(id2289in_a,id2289in_b));
  }
  HWFloat<8,12> id2290out_result;

  { // Node ID: 2290 (NodeMul)
    const HWFloat<8,12> &id2290in_a = id2289out_result;
    const HWFloat<8,12> &id2290in_b = id2824out_output[getCycle()%2];

    id2290out_result = (mul_float(id2290in_a,id2290in_b));
  }
  { // Node ID: 3181 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id591out_o;

  { // Node ID: 591 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id591in_i = id2851out_output[getCycle()%3];

    id591out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id591in_i));
  }
  { // Node ID: 594 (NodeConstantRawBits)
  }
  { // Node ID: 2638 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id581out_result;

  { // Node ID: 581 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id581in_a = id634out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id581in_b = id2852out_output[getCycle()%3];

    id581out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id581in_a,id581in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id582out_result;

  { // Node ID: 582 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id582in_a = id2852out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id582in_b = id634out_dout[getCycle()%3];

    id582out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id582in_a,id582in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id583out_result;

  { // Node ID: 583 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id583in_a = id581out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id583in_b = id582out_result;

    id583out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id583in_a,id583in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id584out_o;

  { // Node ID: 584 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id584in_i = id583out_result;

    id584out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id584in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id585out_o;

  { // Node ID: 585 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id585in_i = id584out_o;

    id585out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id585in_i));
  }
  { // Node ID: 3178 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2510out_result;

  { // Node ID: 2510 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2510in_a = id585out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2510in_b = id3178out_value;

    id2510out_result = (gte_fixed(id2510in_a,id2510in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2678out_result;

  { // Node ID: 2678 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2678in_a = id591out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2678in_b = id594out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2678in_c = id2638out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2678in_condb = id2510out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2678x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2678x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2678x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2678x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2678x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2678x_1 = id2678in_a;
        break;
      default:
        id2678x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2678in_condb.getValueAsLong())) {
      case 0l:
        id2678x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2678x_2 = id2678in_b;
        break;
      default:
        id2678x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2678x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2678x_3 = id2678in_c;
        break;
      default:
        id2678x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2678x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2678x_1),(id2678x_2))),(id2678x_3)));
    id2678out_result = (id2678x_4);
  }
  HWRawBits<1> id2511out_result;

  { // Node ID: 2511 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2511in_a = id2678out_result;

    id2511out_result = (slice<10,1>(id2511in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2512out_output;

  { // Node ID: 2512 (NodeReinterpret)
    const HWRawBits<1> &id2512in_input = id2511out_result;

    id2512out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2512in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id600out_result;

  { // Node ID: 600 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id600in_a = id2855out_output[getCycle()%3];

    id600out_result = (not_fixed(id600in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id601out_result;

  { // Node ID: 601 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id601in_a = id2512out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id601in_b = id600out_result;

    HWOffsetFix<1,0,UNSIGNED> id601x_1;

    (id601x_1) = (and_fixed(id601in_a,id601in_b));
    id601out_result = (id601x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id602out_result;

  { // Node ID: 602 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id602in_a = id601out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id602in_b = id2857out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id602x_1;

    (id602x_1) = (or_fixed(id602in_a,id602in_b));
    id602out_result = (id602x_1);
  }
  { // Node ID: 3175 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2513out_result;

  { // Node ID: 2513 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2513in_a = id2678out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2513in_b = id3175out_value;

    id2513out_result = (gte_fixed(id2513in_a,id2513in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id609out_result;

  { // Node ID: 609 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id609in_a = id2857out_output[getCycle()%3];

    id609out_result = (not_fixed(id609in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id610out_result;

  { // Node ID: 610 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id610in_a = id2513out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id610in_b = id609out_result;

    HWOffsetFix<1,0,UNSIGNED> id610x_1;

    (id610x_1) = (and_fixed(id610in_a,id610in_b));
    id610out_result = (id610x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id611out_result;

  { // Node ID: 611 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id611in_a = id610out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id611in_b = id2855out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id611x_1;

    (id611x_1) = (or_fixed(id611in_a,id611in_b));
    id611out_result = (id611x_1);
  }
  HWRawBits<2> id612out_result;

  { // Node ID: 612 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id612in_in0 = id602out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id612in_in1 = id611out_result;

    id612out_result = (cat(id612in_in0,id612in_in1));
  }
  { // Node ID: 604 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id603out_o;

  { // Node ID: 603 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id603in_i = id2678out_result;

    id603out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id603in_i));
  }
  { // Node ID: 588 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id589out_result;

  { // Node ID: 589 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id589in_sel = id2510out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id589in_option0 = id585out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id589in_option1 = id588out_value;

    HWOffsetFix<12,-11,UNSIGNED> id589x_1;

    switch((id589in_sel.getValueAsLong())) {
      case 0l:
        id589x_1 = id589in_option0;
        break;
      case 1l:
        id589x_1 = id589in_option1;
        break;
      default:
        id589x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id589out_result = (id589x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id590out_o;

  { // Node ID: 590 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id590in_i = id589out_result;

    id590out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id590in_i));
  }
  HWRawBits<20> id605out_result;

  { // Node ID: 605 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id605in_in0 = id604out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id605in_in1 = id603out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id605in_in2 = id590out_o;

    id605out_result = (cat((cat(id605in_in0,id605in_in1)),id605in_in2));
  }
  HWFloat<8,12> id606out_output;

  { // Node ID: 606 (NodeReinterpret)
    const HWRawBits<20> &id606in_input = id605out_result;

    id606out_output = (cast_bits2float<8,12>(id606in_input));
  }
  { // Node ID: 613 (NodeConstantRawBits)
  }
  { // Node ID: 614 (NodeConstantRawBits)
  }
  { // Node ID: 616 (NodeConstantRawBits)
  }
  HWRawBits<20> id2514out_result;

  { // Node ID: 2514 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2514in_in0 = id613out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2514in_in1 = id614out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2514in_in2 = id616out_value;

    id2514out_result = (cat((cat(id2514in_in0,id2514in_in1)),id2514in_in2));
  }
  HWFloat<8,12> id618out_output;

  { // Node ID: 618 (NodeReinterpret)
    const HWRawBits<20> &id618in_input = id2514out_result;

    id618out_output = (cast_bits2float<8,12>(id618in_input));
  }
  { // Node ID: 2406 (NodeConstantRawBits)
  }
  HWFloat<8,12> id621out_result;

  { // Node ID: 621 (NodeMux)
    const HWRawBits<2> &id621in_sel = id612out_result;
    const HWFloat<8,12> &id621in_option0 = id606out_output;
    const HWFloat<8,12> &id621in_option1 = id618out_output;
    const HWFloat<8,12> &id621in_option2 = id2406out_value;
    const HWFloat<8,12> &id621in_option3 = id618out_output;

    HWFloat<8,12> id621x_1;

    switch((id621in_sel.getValueAsLong())) {
      case 0l:
        id621x_1 = id621in_option0;
        break;
      case 1l:
        id621x_1 = id621in_option1;
        break;
      case 2l:
        id621x_1 = id621in_option2;
        break;
      case 3l:
        id621x_1 = id621in_option3;
        break;
      default:
        id621x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id621out_result = (id621x_1);
  }
  { // Node ID: 3174 (NodeConstantRawBits)
  }
  HWFloat<8,12> id631out_result;

  { // Node ID: 631 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id631in_sel = id2860out_output[getCycle()%9];
    const HWFloat<8,12> &id631in_option0 = id621out_result;
    const HWFloat<8,12> &id631in_option1 = id3174out_value;

    HWFloat<8,12> id631x_1;

    switch((id631in_sel.getValueAsLong())) {
      case 0l:
        id631x_1 = id631in_option0;
        break;
      case 1l:
        id631x_1 = id631in_option1;
        break;
      default:
        id631x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id631out_result = (id631x_1);
  }
  HWFloat<8,12> id636out_result;

  { // Node ID: 636 (NodeMul)
    const HWFloat<8,12> &id636in_a = id3181out_value;
    const HWFloat<8,12> &id636in_b = id631out_result;

    id636out_result = (mul_float(id636in_a,id636in_b));
  }
  HWFloat<8,12> id637out_result;

  { // Node ID: 637 (NodeMul)
    const HWFloat<8,12> &id637in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id637in_b = id636out_result;

    id637out_result = (mul_float(id637in_a,id637in_b));
  }
  { // Node ID: 3173 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1663out_result;

  { // Node ID: 1663 (NodeSub)
    const HWFloat<8,12> &id1663in_a = id3173out_value;
    const HWFloat<8,12> &id1663in_b = id2861out_output[getCycle()%10];

    id1663out_result = (sub_float(id1663in_a,id1663in_b));
  }
  HWFloat<8,12> id1664out_result;

  { // Node ID: 1664 (NodeMul)
    const HWFloat<8,12> &id1664in_a = id637out_result;
    const HWFloat<8,12> &id1664in_b = id1663out_result;

    id1664out_result = (mul_float(id1664in_a,id1664in_b));
  }
  { // Node ID: 3172 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id675out_o;

  { // Node ID: 675 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id675in_i = id2863out_output[getCycle()%3];

    id675out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id675in_i));
  }
  { // Node ID: 678 (NodeConstantRawBits)
  }
  { // Node ID: 2640 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id665out_result;

  { // Node ID: 665 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id665in_a = id718out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id665in_b = id2864out_output[getCycle()%3];

    id665out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id665in_a,id665in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id666out_result;

  { // Node ID: 666 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id666in_a = id2864out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id666in_b = id718out_dout[getCycle()%3];

    id666out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id666in_a,id666in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id667out_result;

  { // Node ID: 667 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id667in_a = id665out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id667in_b = id666out_result;

    id667out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id667in_a,id667in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id668out_o;

  { // Node ID: 668 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id668in_i = id667out_result;

    id668out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id668in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id669out_o;

  { // Node ID: 669 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id669in_i = id668out_o;

    id669out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id669in_i));
  }
  { // Node ID: 3168 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2517out_result;

  { // Node ID: 2517 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2517in_a = id669out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2517in_b = id3168out_value;

    id2517out_result = (gte_fixed(id2517in_a,id2517in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2677out_result;

  { // Node ID: 2677 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2677in_a = id675out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2677in_b = id678out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2677in_c = id2640out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2677in_condb = id2517out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2677x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2677x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2677x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2677x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2677x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2677x_1 = id2677in_a;
        break;
      default:
        id2677x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2677in_condb.getValueAsLong())) {
      case 0l:
        id2677x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2677x_2 = id2677in_b;
        break;
      default:
        id2677x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2677x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2677x_3 = id2677in_c;
        break;
      default:
        id2677x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2677x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2677x_1),(id2677x_2))),(id2677x_3)));
    id2677out_result = (id2677x_4);
  }
  HWRawBits<1> id2518out_result;

  { // Node ID: 2518 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2518in_a = id2677out_result;

    id2518out_result = (slice<10,1>(id2518in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2519out_output;

  { // Node ID: 2519 (NodeReinterpret)
    const HWRawBits<1> &id2519in_input = id2518out_result;

    id2519out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2519in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id684out_result;

  { // Node ID: 684 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id684in_a = id2867out_output[getCycle()%3];

    id684out_result = (not_fixed(id684in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id685out_result;

  { // Node ID: 685 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id685in_a = id2519out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id685in_b = id684out_result;

    HWOffsetFix<1,0,UNSIGNED> id685x_1;

    (id685x_1) = (and_fixed(id685in_a,id685in_b));
    id685out_result = (id685x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id686out_result;

  { // Node ID: 686 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id686in_a = id685out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id686in_b = id2869out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id686x_1;

    (id686x_1) = (or_fixed(id686in_a,id686in_b));
    id686out_result = (id686x_1);
  }
  { // Node ID: 3165 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2520out_result;

  { // Node ID: 2520 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2520in_a = id2677out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2520in_b = id3165out_value;

    id2520out_result = (gte_fixed(id2520in_a,id2520in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id693out_result;

  { // Node ID: 693 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id693in_a = id2869out_output[getCycle()%3];

    id693out_result = (not_fixed(id693in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id694out_result;

  { // Node ID: 694 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id694in_a = id2520out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id694in_b = id693out_result;

    HWOffsetFix<1,0,UNSIGNED> id694x_1;

    (id694x_1) = (and_fixed(id694in_a,id694in_b));
    id694out_result = (id694x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id695out_result;

  { // Node ID: 695 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id695in_a = id694out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id695in_b = id2867out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id695x_1;

    (id695x_1) = (or_fixed(id695in_a,id695in_b));
    id695out_result = (id695x_1);
  }
  HWRawBits<2> id696out_result;

  { // Node ID: 696 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id696in_in0 = id686out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id696in_in1 = id695out_result;

    id696out_result = (cat(id696in_in0,id696in_in1));
  }
  { // Node ID: 688 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id687out_o;

  { // Node ID: 687 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id687in_i = id2677out_result;

    id687out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id687in_i));
  }
  { // Node ID: 672 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id673out_result;

  { // Node ID: 673 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id673in_sel = id2517out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id673in_option0 = id669out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id673in_option1 = id672out_value;

    HWOffsetFix<12,-11,UNSIGNED> id673x_1;

    switch((id673in_sel.getValueAsLong())) {
      case 0l:
        id673x_1 = id673in_option0;
        break;
      case 1l:
        id673x_1 = id673in_option1;
        break;
      default:
        id673x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id673out_result = (id673x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id674out_o;

  { // Node ID: 674 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id674in_i = id673out_result;

    id674out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id674in_i));
  }
  HWRawBits<20> id689out_result;

  { // Node ID: 689 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id689in_in0 = id688out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id689in_in1 = id687out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id689in_in2 = id674out_o;

    id689out_result = (cat((cat(id689in_in0,id689in_in1)),id689in_in2));
  }
  HWFloat<8,12> id690out_output;

  { // Node ID: 690 (NodeReinterpret)
    const HWRawBits<20> &id690in_input = id689out_result;

    id690out_output = (cast_bits2float<8,12>(id690in_input));
  }
  { // Node ID: 697 (NodeConstantRawBits)
  }
  { // Node ID: 698 (NodeConstantRawBits)
  }
  { // Node ID: 700 (NodeConstantRawBits)
  }
  HWRawBits<20> id2521out_result;

  { // Node ID: 2521 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2521in_in0 = id697out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2521in_in1 = id698out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2521in_in2 = id700out_value;

    id2521out_result = (cat((cat(id2521in_in0,id2521in_in1)),id2521in_in2));
  }
  HWFloat<8,12> id702out_output;

  { // Node ID: 702 (NodeReinterpret)
    const HWRawBits<20> &id702in_input = id2521out_result;

    id702out_output = (cast_bits2float<8,12>(id702in_input));
  }
  { // Node ID: 2407 (NodeConstantRawBits)
  }
  HWFloat<8,12> id705out_result;

  { // Node ID: 705 (NodeMux)
    const HWRawBits<2> &id705in_sel = id696out_result;
    const HWFloat<8,12> &id705in_option0 = id690out_output;
    const HWFloat<8,12> &id705in_option1 = id702out_output;
    const HWFloat<8,12> &id705in_option2 = id2407out_value;
    const HWFloat<8,12> &id705in_option3 = id702out_output;

    HWFloat<8,12> id705x_1;

    switch((id705in_sel.getValueAsLong())) {
      case 0l:
        id705x_1 = id705in_option0;
        break;
      case 1l:
        id705x_1 = id705in_option1;
        break;
      case 2l:
        id705x_1 = id705in_option2;
        break;
      case 3l:
        id705x_1 = id705in_option3;
        break;
      default:
        id705x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id705out_result = (id705x_1);
  }
  { // Node ID: 3164 (NodeConstantRawBits)
  }
  HWFloat<8,12> id715out_result;

  { // Node ID: 715 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id715in_sel = id2872out_output[getCycle()%9];
    const HWFloat<8,12> &id715in_option0 = id705out_result;
    const HWFloat<8,12> &id715in_option1 = id3164out_value;

    HWFloat<8,12> id715x_1;

    switch((id715in_sel.getValueAsLong())) {
      case 0l:
        id715x_1 = id715in_option0;
        break;
      case 1l:
        id715x_1 = id715in_option1;
        break;
      default:
        id715x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id715out_result = (id715x_1);
  }
  { // Node ID: 3163 (NodeConstantRawBits)
  }
  HWFloat<8,12> id720out_result;

  { // Node ID: 720 (NodeAdd)
    const HWFloat<8,12> &id720in_a = id715out_result;
    const HWFloat<8,12> &id720in_b = id3163out_value;

    id720out_result = (add_float(id720in_a,id720in_b));
  }
  HWFloat<8,12> id722out_result;

  { // Node ID: 722 (NodeDiv)
    const HWFloat<8,12> &id722in_a = id3172out_value;
    const HWFloat<8,12> &id722in_b = id720out_result;

    id722out_result = (div_float(id722in_a,id722in_b));
  }
  HWFloat<8,12> id723out_result;

  { // Node ID: 723 (NodeMul)
    const HWFloat<8,12> &id723in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id723in_b = id722out_result;

    id723out_result = (mul_float(id723in_a,id723in_b));
  }
  HWFloat<8,12> id1665out_result;

  { // Node ID: 1665 (NodeMul)
    const HWFloat<8,12> &id1665in_a = id723out_result;
    const HWFloat<8,12> &id1665in_b = id2861out_output[getCycle()%10];

    id1665out_result = (mul_float(id1665in_a,id1665in_b));
  }
  HWFloat<8,12> id1666out_result;

  { // Node ID: 1666 (NodeSub)
    const HWFloat<8,12> &id1666in_a = id1664out_result;
    const HWFloat<8,12> &id1666in_b = id1665out_result;

    id1666out_result = (sub_float(id1666in_a,id1666in_b));
  }
  HWFloat<8,12> id1667out_result;

  { // Node ID: 1667 (NodeAdd)
    const HWFloat<8,12> &id1667in_a = id2861out_output[getCycle()%10];
    const HWFloat<8,12> &id1667in_b = id1666out_result;

    id1667out_result = (add_float(id1667in_a,id1667in_b));
  }
  HWFloat<8,12> id2291out_result;

  { // Node ID: 2291 (NodeMul)
    const HWFloat<8,12> &id2291in_a = id2290out_result;
    const HWFloat<8,12> &id2291in_b = id1667out_result;

    id2291out_result = (mul_float(id2291in_a,id2291in_b));
  }
  { // Node ID: 3161 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id761out_o;

  { // Node ID: 761 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id761in_i = id2875out_output[getCycle()%3];

    id761out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id761in_i));
  }
  { // Node ID: 764 (NodeConstantRawBits)
  }
  { // Node ID: 2642 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id751out_result;

  { // Node ID: 751 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id751in_a = id804out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id751in_b = id2876out_output[getCycle()%3];

    id751out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id751in_a,id751in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id752out_result;

  { // Node ID: 752 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id752in_a = id2876out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id752in_b = id804out_dout[getCycle()%3];

    id752out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id752in_a,id752in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id753out_result;

  { // Node ID: 753 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id753in_a = id751out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id753in_b = id752out_result;

    id753out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id753in_a,id753in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id754out_o;

  { // Node ID: 754 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id754in_i = id753out_result;

    id754out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id754in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id755out_o;

  { // Node ID: 755 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id755in_i = id754out_o;

    id755out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id755in_i));
  }
  { // Node ID: 3158 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2525out_result;

  { // Node ID: 2525 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2525in_a = id755out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2525in_b = id3158out_value;

    id2525out_result = (gte_fixed(id2525in_a,id2525in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2676out_result;

  { // Node ID: 2676 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2676in_a = id761out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2676in_b = id764out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2676in_c = id2642out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2676in_condb = id2525out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2676x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2676x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2676x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2676x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2676x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2676x_1 = id2676in_a;
        break;
      default:
        id2676x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2676in_condb.getValueAsLong())) {
      case 0l:
        id2676x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2676x_2 = id2676in_b;
        break;
      default:
        id2676x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2676x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2676x_3 = id2676in_c;
        break;
      default:
        id2676x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2676x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2676x_1),(id2676x_2))),(id2676x_3)));
    id2676out_result = (id2676x_4);
  }
  HWRawBits<1> id2526out_result;

  { // Node ID: 2526 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2526in_a = id2676out_result;

    id2526out_result = (slice<10,1>(id2526in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2527out_output;

  { // Node ID: 2527 (NodeReinterpret)
    const HWRawBits<1> &id2527in_input = id2526out_result;

    id2527out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2527in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id770out_result;

  { // Node ID: 770 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id770in_a = id2879out_output[getCycle()%3];

    id770out_result = (not_fixed(id770in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id771out_result;

  { // Node ID: 771 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id771in_a = id2527out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id771in_b = id770out_result;

    HWOffsetFix<1,0,UNSIGNED> id771x_1;

    (id771x_1) = (and_fixed(id771in_a,id771in_b));
    id771out_result = (id771x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id772out_result;

  { // Node ID: 772 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id772in_a = id771out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id772in_b = id2881out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id772x_1;

    (id772x_1) = (or_fixed(id772in_a,id772in_b));
    id772out_result = (id772x_1);
  }
  { // Node ID: 3155 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2528out_result;

  { // Node ID: 2528 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2528in_a = id2676out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2528in_b = id3155out_value;

    id2528out_result = (gte_fixed(id2528in_a,id2528in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id779out_result;

  { // Node ID: 779 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id779in_a = id2881out_output[getCycle()%3];

    id779out_result = (not_fixed(id779in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id780out_result;

  { // Node ID: 780 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id780in_a = id2528out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id780in_b = id779out_result;

    HWOffsetFix<1,0,UNSIGNED> id780x_1;

    (id780x_1) = (and_fixed(id780in_a,id780in_b));
    id780out_result = (id780x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id781out_result;

  { // Node ID: 781 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id781in_a = id780out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id781in_b = id2879out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id781x_1;

    (id781x_1) = (or_fixed(id781in_a,id781in_b));
    id781out_result = (id781x_1);
  }
  HWRawBits<2> id782out_result;

  { // Node ID: 782 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id782in_in0 = id772out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id782in_in1 = id781out_result;

    id782out_result = (cat(id782in_in0,id782in_in1));
  }
  { // Node ID: 774 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id773out_o;

  { // Node ID: 773 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id773in_i = id2676out_result;

    id773out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id773in_i));
  }
  { // Node ID: 758 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id759out_result;

  { // Node ID: 759 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id759in_sel = id2525out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id759in_option0 = id755out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id759in_option1 = id758out_value;

    HWOffsetFix<12,-11,UNSIGNED> id759x_1;

    switch((id759in_sel.getValueAsLong())) {
      case 0l:
        id759x_1 = id759in_option0;
        break;
      case 1l:
        id759x_1 = id759in_option1;
        break;
      default:
        id759x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id759out_result = (id759x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id760out_o;

  { // Node ID: 760 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id760in_i = id759out_result;

    id760out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id760in_i));
  }
  HWRawBits<20> id775out_result;

  { // Node ID: 775 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id775in_in0 = id774out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id775in_in1 = id773out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id775in_in2 = id760out_o;

    id775out_result = (cat((cat(id775in_in0,id775in_in1)),id775in_in2));
  }
  HWFloat<8,12> id776out_output;

  { // Node ID: 776 (NodeReinterpret)
    const HWRawBits<20> &id776in_input = id775out_result;

    id776out_output = (cast_bits2float<8,12>(id776in_input));
  }
  { // Node ID: 783 (NodeConstantRawBits)
  }
  { // Node ID: 784 (NodeConstantRawBits)
  }
  { // Node ID: 786 (NodeConstantRawBits)
  }
  HWRawBits<20> id2529out_result;

  { // Node ID: 2529 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2529in_in0 = id783out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2529in_in1 = id784out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2529in_in2 = id786out_value;

    id2529out_result = (cat((cat(id2529in_in0,id2529in_in1)),id2529in_in2));
  }
  HWFloat<8,12> id788out_output;

  { // Node ID: 788 (NodeReinterpret)
    const HWRawBits<20> &id788in_input = id2529out_result;

    id788out_output = (cast_bits2float<8,12>(id788in_input));
  }
  { // Node ID: 2408 (NodeConstantRawBits)
  }
  HWFloat<8,12> id791out_result;

  { // Node ID: 791 (NodeMux)
    const HWRawBits<2> &id791in_sel = id782out_result;
    const HWFloat<8,12> &id791in_option0 = id776out_output;
    const HWFloat<8,12> &id791in_option1 = id788out_output;
    const HWFloat<8,12> &id791in_option2 = id2408out_value;
    const HWFloat<8,12> &id791in_option3 = id788out_output;

    HWFloat<8,12> id791x_1;

    switch((id791in_sel.getValueAsLong())) {
      case 0l:
        id791x_1 = id791in_option0;
        break;
      case 1l:
        id791x_1 = id791in_option1;
        break;
      case 2l:
        id791x_1 = id791in_option2;
        break;
      case 3l:
        id791x_1 = id791in_option3;
        break;
      default:
        id791x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id791out_result = (id791x_1);
  }
  { // Node ID: 3154 (NodeConstantRawBits)
  }
  HWFloat<8,12> id801out_result;

  { // Node ID: 801 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id801in_sel = id2884out_output[getCycle()%9];
    const HWFloat<8,12> &id801in_option0 = id791out_result;
    const HWFloat<8,12> &id801in_option1 = id3154out_value;

    HWFloat<8,12> id801x_1;

    switch((id801in_sel.getValueAsLong())) {
      case 0l:
        id801x_1 = id801in_option0;
        break;
      case 1l:
        id801x_1 = id801in_option1;
        break;
      default:
        id801x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id801out_result = (id801x_1);
  }
  HWFloat<8,12> id806out_result;

  { // Node ID: 806 (NodeMul)
    const HWFloat<8,12> &id806in_a = id3161out_value;
    const HWFloat<8,12> &id806in_b = id801out_result;

    id806out_result = (mul_float(id806in_a,id806in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id844out_o;

  { // Node ID: 844 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id844in_i = id2886out_output[getCycle()%3];

    id844out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id844in_i));
  }
  { // Node ID: 847 (NodeConstantRawBits)
  }
  { // Node ID: 2644 (NodeConstantRawBits)
  }
  { // Node ID: 832 (NodeConstantRawBits)
  }
  HWOffsetFix<16,-22,UNSIGNED> id831out_result;

  { // Node ID: 831 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id831in_a = id832out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id831in_b = id2887out_output[getCycle()%3];

    id831out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id831in_a,id831in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id833out_o;

  { // Node ID: 833 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id833in_i = id831out_result;

    id833out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id833in_i));
  }
  HWOffsetFix<15,-14,UNSIGNED> id834out_result;

  { // Node ID: 834 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id834in_a = id887out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id834in_b = id833out_o;

    id834out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id834in_a,id834in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id835out_result;

  { // Node ID: 835 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id835in_a = id833out_o;
    const HWOffsetFix<12,-12,UNSIGNED> &id835in_b = id887out_dout[getCycle()%3];

    id835out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id835in_a,id835in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id836out_result;

  { // Node ID: 836 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id836in_a = id834out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id836in_b = id835out_result;

    id836out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id836in_a,id836in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id837out_o;

  { // Node ID: 837 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id837in_i = id836out_result;

    id837out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id837in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id838out_o;

  { // Node ID: 838 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id838in_i = id837out_o;

    id838out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id838in_i));
  }
  { // Node ID: 3150 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2532out_result;

  { // Node ID: 2532 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2532in_a = id838out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2532in_b = id3150out_value;

    id2532out_result = (gte_fixed(id2532in_a,id2532in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2675out_result;

  { // Node ID: 2675 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2675in_a = id844out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2675in_b = id847out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2675in_c = id2644out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2675in_condb = id2532out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2675x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2675x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2675x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2675x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2675x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2675x_1 = id2675in_a;
        break;
      default:
        id2675x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2675in_condb.getValueAsLong())) {
      case 0l:
        id2675x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2675x_2 = id2675in_b;
        break;
      default:
        id2675x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2675x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2675x_3 = id2675in_c;
        break;
      default:
        id2675x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2675x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2675x_1),(id2675x_2))),(id2675x_3)));
    id2675out_result = (id2675x_4);
  }
  HWRawBits<1> id2533out_result;

  { // Node ID: 2533 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2533in_a = id2675out_result;

    id2533out_result = (slice<10,1>(id2533in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2534out_output;

  { // Node ID: 2534 (NodeReinterpret)
    const HWRawBits<1> &id2534in_input = id2533out_result;

    id2534out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2534in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id853out_result;

  { // Node ID: 853 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id853in_a = id2889out_output[getCycle()%3];

    id853out_result = (not_fixed(id853in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id854out_result;

  { // Node ID: 854 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id854in_a = id2534out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id854in_b = id853out_result;

    HWOffsetFix<1,0,UNSIGNED> id854x_1;

    (id854x_1) = (and_fixed(id854in_a,id854in_b));
    id854out_result = (id854x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id855out_result;

  { // Node ID: 855 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id855in_a = id854out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id855in_b = id2891out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id855x_1;

    (id855x_1) = (or_fixed(id855in_a,id855in_b));
    id855out_result = (id855x_1);
  }
  { // Node ID: 3147 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2535out_result;

  { // Node ID: 2535 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2535in_a = id2675out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2535in_b = id3147out_value;

    id2535out_result = (gte_fixed(id2535in_a,id2535in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id862out_result;

  { // Node ID: 862 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id862in_a = id2891out_output[getCycle()%3];

    id862out_result = (not_fixed(id862in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id863out_result;

  { // Node ID: 863 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id863in_a = id2535out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id863in_b = id862out_result;

    HWOffsetFix<1,0,UNSIGNED> id863x_1;

    (id863x_1) = (and_fixed(id863in_a,id863in_b));
    id863out_result = (id863x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id864out_result;

  { // Node ID: 864 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id864in_a = id863out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id864in_b = id2889out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id864x_1;

    (id864x_1) = (or_fixed(id864in_a,id864in_b));
    id864out_result = (id864x_1);
  }
  HWRawBits<2> id865out_result;

  { // Node ID: 865 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id865in_in0 = id855out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id865in_in1 = id864out_result;

    id865out_result = (cat(id865in_in0,id865in_in1));
  }
  { // Node ID: 857 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id856out_o;

  { // Node ID: 856 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id856in_i = id2675out_result;

    id856out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id856in_i));
  }
  { // Node ID: 841 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id842out_result;

  { // Node ID: 842 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id842in_sel = id2532out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id842in_option0 = id838out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id842in_option1 = id841out_value;

    HWOffsetFix<12,-11,UNSIGNED> id842x_1;

    switch((id842in_sel.getValueAsLong())) {
      case 0l:
        id842x_1 = id842in_option0;
        break;
      case 1l:
        id842x_1 = id842in_option1;
        break;
      default:
        id842x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id842out_result = (id842x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id843out_o;

  { // Node ID: 843 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id843in_i = id842out_result;

    id843out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id843in_i));
  }
  HWRawBits<20> id858out_result;

  { // Node ID: 858 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id858in_in0 = id857out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id858in_in1 = id856out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id858in_in2 = id843out_o;

    id858out_result = (cat((cat(id858in_in0,id858in_in1)),id858in_in2));
  }
  HWFloat<8,12> id859out_output;

  { // Node ID: 859 (NodeReinterpret)
    const HWRawBits<20> &id859in_input = id858out_result;

    id859out_output = (cast_bits2float<8,12>(id859in_input));
  }
  { // Node ID: 866 (NodeConstantRawBits)
  }
  { // Node ID: 867 (NodeConstantRawBits)
  }
  { // Node ID: 869 (NodeConstantRawBits)
  }
  HWRawBits<20> id2536out_result;

  { // Node ID: 2536 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2536in_in0 = id866out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2536in_in1 = id867out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2536in_in2 = id869out_value;

    id2536out_result = (cat((cat(id2536in_in0,id2536in_in1)),id2536in_in2));
  }
  HWFloat<8,12> id871out_output;

  { // Node ID: 871 (NodeReinterpret)
    const HWRawBits<20> &id871in_input = id2536out_result;

    id871out_output = (cast_bits2float<8,12>(id871in_input));
  }
  { // Node ID: 2409 (NodeConstantRawBits)
  }
  HWFloat<8,12> id874out_result;

  { // Node ID: 874 (NodeMux)
    const HWRawBits<2> &id874in_sel = id865out_result;
    const HWFloat<8,12> &id874in_option0 = id859out_output;
    const HWFloat<8,12> &id874in_option1 = id871out_output;
    const HWFloat<8,12> &id874in_option2 = id2409out_value;
    const HWFloat<8,12> &id874in_option3 = id871out_output;

    HWFloat<8,12> id874x_1;

    switch((id874in_sel.getValueAsLong())) {
      case 0l:
        id874x_1 = id874in_option0;
        break;
      case 1l:
        id874x_1 = id874in_option1;
        break;
      case 2l:
        id874x_1 = id874in_option2;
        break;
      case 3l:
        id874x_1 = id874in_option3;
        break;
      default:
        id874x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id874out_result = (id874x_1);
  }
  { // Node ID: 3146 (NodeConstantRawBits)
  }
  HWFloat<8,12> id884out_result;

  { // Node ID: 884 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id884in_sel = id2894out_output[getCycle()%9];
    const HWFloat<8,12> &id884in_option0 = id874out_result;
    const HWFloat<8,12> &id884in_option1 = id3146out_value;

    HWFloat<8,12> id884x_1;

    switch((id884in_sel.getValueAsLong())) {
      case 0l:
        id884x_1 = id884in_option0;
        break;
      case 1l:
        id884x_1 = id884in_option1;
        break;
      default:
        id884x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id884out_result = (id884x_1);
  }
  { // Node ID: 3145 (NodeConstantRawBits)
  }
  HWFloat<8,12> id889out_result;

  { // Node ID: 889 (NodeAdd)
    const HWFloat<8,12> &id889in_a = id884out_result;
    const HWFloat<8,12> &id889in_b = id3145out_value;

    id889out_result = (add_float(id889in_a,id889in_b));
  }
  HWFloat<8,12> id890out_result;

  { // Node ID: 890 (NodeDiv)
    const HWFloat<8,12> &id890in_a = id806out_result;
    const HWFloat<8,12> &id890in_b = id889out_result;

    id890out_result = (div_float(id890in_a,id890in_b));
  }
  HWFloat<8,12> id891out_result;

  { // Node ID: 891 (NodeMul)
    const HWFloat<8,12> &id891in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id891in_b = id890out_result;

    id891out_result = (mul_float(id891in_a,id891in_b));
  }
  { // Node ID: 3144 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1669out_result;

  { // Node ID: 1669 (NodeSub)
    const HWFloat<8,12> &id1669in_a = id3144out_value;
    const HWFloat<8,12> &id1669in_b = id2895out_output[getCycle()%10];

    id1669out_result = (sub_float(id1669in_a,id1669in_b));
  }
  HWFloat<8,12> id1670out_result;

  { // Node ID: 1670 (NodeMul)
    const HWFloat<8,12> &id1670in_a = id891out_result;
    const HWFloat<8,12> &id1670in_b = id1669out_result;

    id1670out_result = (mul_float(id1670in_a,id1670in_b));
  }
  { // Node ID: 3143 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id929out_o;

  { // Node ID: 929 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id929in_i = id2897out_output[getCycle()%3];

    id929out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id929in_i));
  }
  { // Node ID: 932 (NodeConstantRawBits)
  }
  { // Node ID: 2646 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id919out_result;

  { // Node ID: 919 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id919in_a = id972out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id919in_b = id2898out_output[getCycle()%3];

    id919out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id919in_a,id919in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id920out_result;

  { // Node ID: 920 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id920in_a = id2898out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id920in_b = id972out_dout[getCycle()%3];

    id920out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id920in_a,id920in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id921out_result;

  { // Node ID: 921 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id921in_a = id919out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id921in_b = id920out_result;

    id921out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id921in_a,id921in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id922out_o;

  { // Node ID: 922 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id922in_i = id921out_result;

    id922out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id922in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id923out_o;

  { // Node ID: 923 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id923in_i = id922out_o;

    id923out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id923in_i));
  }
  { // Node ID: 3139 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2539out_result;

  { // Node ID: 2539 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2539in_a = id923out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2539in_b = id3139out_value;

    id2539out_result = (gte_fixed(id2539in_a,id2539in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2674out_result;

  { // Node ID: 2674 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2674in_a = id929out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2674in_b = id932out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2674in_c = id2646out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2674in_condb = id2539out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2674x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2674x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2674x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2674x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2674x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2674x_1 = id2674in_a;
        break;
      default:
        id2674x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2674in_condb.getValueAsLong())) {
      case 0l:
        id2674x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2674x_2 = id2674in_b;
        break;
      default:
        id2674x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2674x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2674x_3 = id2674in_c;
        break;
      default:
        id2674x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2674x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2674x_1),(id2674x_2))),(id2674x_3)));
    id2674out_result = (id2674x_4);
  }
  HWRawBits<1> id2540out_result;

  { // Node ID: 2540 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2540in_a = id2674out_result;

    id2540out_result = (slice<10,1>(id2540in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2541out_output;

  { // Node ID: 2541 (NodeReinterpret)
    const HWRawBits<1> &id2541in_input = id2540out_result;

    id2541out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2541in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id938out_result;

  { // Node ID: 938 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id938in_a = id2901out_output[getCycle()%3];

    id938out_result = (not_fixed(id938in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id939out_result;

  { // Node ID: 939 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id939in_a = id2541out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id939in_b = id938out_result;

    HWOffsetFix<1,0,UNSIGNED> id939x_1;

    (id939x_1) = (and_fixed(id939in_a,id939in_b));
    id939out_result = (id939x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id940out_result;

  { // Node ID: 940 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id940in_a = id939out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id940in_b = id2903out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id940x_1;

    (id940x_1) = (or_fixed(id940in_a,id940in_b));
    id940out_result = (id940x_1);
  }
  { // Node ID: 3136 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2542out_result;

  { // Node ID: 2542 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2542in_a = id2674out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2542in_b = id3136out_value;

    id2542out_result = (gte_fixed(id2542in_a,id2542in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id947out_result;

  { // Node ID: 947 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id947in_a = id2903out_output[getCycle()%3];

    id947out_result = (not_fixed(id947in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id948out_result;

  { // Node ID: 948 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id948in_a = id2542out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id948in_b = id947out_result;

    HWOffsetFix<1,0,UNSIGNED> id948x_1;

    (id948x_1) = (and_fixed(id948in_a,id948in_b));
    id948out_result = (id948x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id949out_result;

  { // Node ID: 949 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id949in_a = id948out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id949in_b = id2901out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id949x_1;

    (id949x_1) = (or_fixed(id949in_a,id949in_b));
    id949out_result = (id949x_1);
  }
  HWRawBits<2> id950out_result;

  { // Node ID: 950 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id950in_in0 = id940out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id950in_in1 = id949out_result;

    id950out_result = (cat(id950in_in0,id950in_in1));
  }
  { // Node ID: 942 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id941out_o;

  { // Node ID: 941 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id941in_i = id2674out_result;

    id941out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id941in_i));
  }
  { // Node ID: 926 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id927out_result;

  { // Node ID: 927 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id927in_sel = id2539out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id927in_option0 = id923out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id927in_option1 = id926out_value;

    HWOffsetFix<12,-11,UNSIGNED> id927x_1;

    switch((id927in_sel.getValueAsLong())) {
      case 0l:
        id927x_1 = id927in_option0;
        break;
      case 1l:
        id927x_1 = id927in_option1;
        break;
      default:
        id927x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id927out_result = (id927x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id928out_o;

  { // Node ID: 928 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id928in_i = id927out_result;

    id928out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id928in_i));
  }
  HWRawBits<20> id943out_result;

  { // Node ID: 943 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id943in_in0 = id942out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id943in_in1 = id941out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id943in_in2 = id928out_o;

    id943out_result = (cat((cat(id943in_in0,id943in_in1)),id943in_in2));
  }
  HWFloat<8,12> id944out_output;

  { // Node ID: 944 (NodeReinterpret)
    const HWRawBits<20> &id944in_input = id943out_result;

    id944out_output = (cast_bits2float<8,12>(id944in_input));
  }
  { // Node ID: 951 (NodeConstantRawBits)
  }
  { // Node ID: 952 (NodeConstantRawBits)
  }
  { // Node ID: 954 (NodeConstantRawBits)
  }
  HWRawBits<20> id2543out_result;

  { // Node ID: 2543 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2543in_in0 = id951out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2543in_in1 = id952out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2543in_in2 = id954out_value;

    id2543out_result = (cat((cat(id2543in_in0,id2543in_in1)),id2543in_in2));
  }
  HWFloat<8,12> id956out_output;

  { // Node ID: 956 (NodeReinterpret)
    const HWRawBits<20> &id956in_input = id2543out_result;

    id956out_output = (cast_bits2float<8,12>(id956in_input));
  }
  { // Node ID: 2410 (NodeConstantRawBits)
  }
  HWFloat<8,12> id959out_result;

  { // Node ID: 959 (NodeMux)
    const HWRawBits<2> &id959in_sel = id950out_result;
    const HWFloat<8,12> &id959in_option0 = id944out_output;
    const HWFloat<8,12> &id959in_option1 = id956out_output;
    const HWFloat<8,12> &id959in_option2 = id2410out_value;
    const HWFloat<8,12> &id959in_option3 = id956out_output;

    HWFloat<8,12> id959x_1;

    switch((id959in_sel.getValueAsLong())) {
      case 0l:
        id959x_1 = id959in_option0;
        break;
      case 1l:
        id959x_1 = id959in_option1;
        break;
      case 2l:
        id959x_1 = id959in_option2;
        break;
      case 3l:
        id959x_1 = id959in_option3;
        break;
      default:
        id959x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id959out_result = (id959x_1);
  }
  { // Node ID: 3135 (NodeConstantRawBits)
  }
  HWFloat<8,12> id969out_result;

  { // Node ID: 969 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id969in_sel = id2906out_output[getCycle()%9];
    const HWFloat<8,12> &id969in_option0 = id959out_result;
    const HWFloat<8,12> &id969in_option1 = id3135out_value;

    HWFloat<8,12> id969x_1;

    switch((id969in_sel.getValueAsLong())) {
      case 0l:
        id969x_1 = id969in_option0;
        break;
      case 1l:
        id969x_1 = id969in_option1;
        break;
      default:
        id969x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id969out_result = (id969x_1);
  }
  { // Node ID: 3134 (NodeConstantRawBits)
  }
  HWFloat<8,12> id974out_result;

  { // Node ID: 974 (NodeAdd)
    const HWFloat<8,12> &id974in_a = id969out_result;
    const HWFloat<8,12> &id974in_b = id3134out_value;

    id974out_result = (add_float(id974in_a,id974in_b));
  }
  HWFloat<8,12> id976out_result;

  { // Node ID: 976 (NodeDiv)
    const HWFloat<8,12> &id976in_a = id3143out_value;
    const HWFloat<8,12> &id976in_b = id974out_result;

    id976out_result = (div_float(id976in_a,id976in_b));
  }
  HWFloat<8,12> id977out_result;

  { // Node ID: 977 (NodeMul)
    const HWFloat<8,12> &id977in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id977in_b = id976out_result;

    id977out_result = (mul_float(id977in_a,id977in_b));
  }
  HWFloat<8,12> id1671out_result;

  { // Node ID: 1671 (NodeMul)
    const HWFloat<8,12> &id1671in_a = id977out_result;
    const HWFloat<8,12> &id1671in_b = id2895out_output[getCycle()%10];

    id1671out_result = (mul_float(id1671in_a,id1671in_b));
  }
  HWFloat<8,12> id1672out_result;

  { // Node ID: 1672 (NodeSub)
    const HWFloat<8,12> &id1672in_a = id1670out_result;
    const HWFloat<8,12> &id1672in_b = id1671out_result;

    id1672out_result = (sub_float(id1672in_a,id1672in_b));
  }
  HWFloat<8,12> id1673out_result;

  { // Node ID: 1673 (NodeAdd)
    const HWFloat<8,12> &id1673in_a = id2895out_output[getCycle()%10];
    const HWFloat<8,12> &id1673in_b = id1672out_result;

    id1673out_result = (add_float(id1673in_a,id1673in_b));
  }
  HWFloat<8,12> id2292out_result;

  { // Node ID: 2292 (NodeMul)
    const HWFloat<8,12> &id2292in_a = id2291out_result;
    const HWFloat<8,12> &id2292in_b = id1673out_result;

    id2292out_result = (mul_float(id2292in_a,id2292in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2293out_result;

  { // Node ID: 2293 (NodeAdd)
    const HWFloat<8,12> &id2293in_a = id2292out_result;
    const HWFloat<8,12> &id2293in_b = id3out_value;

    id2293out_result = (add_float(id2293in_a,id2293in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2294out_result;

  { // Node ID: 2294 (NodeSub)
    const HWFloat<8,12> &id2294in_a = id3031out_output[getCycle()%2];
    const HWFloat<8,12> &id2294in_b = id2out_value;

    id2294out_result = (sub_float(id2294in_a,id2294in_b));
  }
  HWFloat<8,12> id2295out_result;

  { // Node ID: 2295 (NodeMul)
    const HWFloat<8,12> &id2295in_a = id2293out_result;
    const HWFloat<8,12> &id2295in_b = id2294out_result;

    id2295out_result = (mul_float(id2295in_a,id2295in_b));
  }
  HWFloat<8,12> id2297out_result;

  { // Node ID: 2297 (NodeAdd)
    const HWFloat<8,12> &id2297in_a = id2296out_result;
    const HWFloat<8,12> &id2297in_b = id2295out_result;

    id2297out_result = (add_float(id2297in_a,id2297in_b));
  }
  { // Node ID: 3133 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1761out_result;

  { // Node ID: 1761 (NodeMul)
    const HWFloat<8,12> &id1761in_a = id3133out_value;
    const HWFloat<8,12> &id1761in_b = id2910out_output[getCycle()%2];

    id1761out_result = (mul_float(id1761in_a,id1761in_b));
  }
  HWFloat<8,12> id1762out_result;

  { // Node ID: 1762 (NodeMul)
    const HWFloat<8,12> &id1762in_a = id1761out_result;
    const HWFloat<8,12> &id1762in_b = id2955out_output[getCycle()%2];

    id1762out_result = (mul_float(id1762in_a,id1762in_b));
  }
  { // Node ID: 3056 (NodeConstantRawBits)
  }
  { // Node ID: 3055 (NodeConstantRawBits)
  }
  { // Node ID: 3050 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1748out_result;

  { // Node ID: 1748 (NodeLt)
    const HWFloat<8,12> &id1748in_a = id3032out_output[getCycle()%3];
    const HWFloat<8,12> &id1748in_b = id3050out_value;

    id1748out_result = (lt_float(id1748in_a,id1748in_b));
  }
  HWRawBits<8> id1729out_result;

  { // Node ID: 1729 (NodeSlice)
    const HWFloat<8,12> &id1729in_a = id3032out_output[getCycle()%3];

    id1729out_result = (slice<11,8>(id1729in_a));
  }
  { // Node ID: 1730 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2603out_result;

  { // Node ID: 2603 (NodeEqInlined)
    const HWRawBits<8> &id2603in_a = id1729out_result;
    const HWRawBits<8> &id2603in_b = id1730out_value;

    id2603out_result = (eq_bits(id2603in_a,id2603in_b));
  }
  HWRawBits<11> id1728out_result;

  { // Node ID: 1728 (NodeSlice)
    const HWFloat<8,12> &id1728in_a = id3032out_output[getCycle()%3];

    id1728out_result = (slice<0,11>(id1728in_a));
  }
  { // Node ID: 3049 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2604out_result;

  { // Node ID: 2604 (NodeNeqInlined)
    const HWRawBits<11> &id2604in_a = id1728out_result;
    const HWRawBits<11> &id2604in_b = id3049out_value;

    id2604out_result = (neq_bits(id2604in_a,id2604in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1734out_result;

  { // Node ID: 1734 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1734in_a = id2603out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1734in_b = id2604out_result;

    HWOffsetFix<1,0,UNSIGNED> id1734x_1;

    (id1734x_1) = (and_fixed(id1734in_a,id1734in_b));
    id1734out_result = (id1734x_1);
  }
  { // Node ID: 3048 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1736out_result;

  { // Node ID: 1736 (NodeLt)
    const HWFloat<8,12> &id1736in_a = id3032out_output[getCycle()%3];
    const HWFloat<8,12> &id1736in_b = id3048out_value;

    id1736out_result = (lt_float(id1736in_a,id1736in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1737out_result;

  { // Node ID: 1737 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1737in_a = id1734out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1737in_b = id1736out_result;

    HWOffsetFix<1,0,UNSIGNED> id1737x_1;

    (id1737x_1) = (or_fixed(id1737in_a,id1737in_b));
    id1737out_result = (id1737x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1749out_result;

  { // Node ID: 1749 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1749in_a = id1737out_result;

    id1749out_result = (not_fixed(id1749in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1750out_result;

  { // Node ID: 1750 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1750in_a = id1748out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1750in_b = id1749out_result;

    HWOffsetFix<1,0,UNSIGNED> id1750x_1;

    (id1750x_1) = (and_fixed(id1750in_a,id1750in_b));
    id1750out_result = (id1750x_1);
  }
  HWRawBits<19> id1741out_result;

  { // Node ID: 1741 (NodeSlice)
    const HWFloat<8,12> &id1741in_a = id3032out_output[getCycle()%3];

    id1741out_result = (slice<0,19>(id1741in_a));
  }
  { // Node ID: 1740 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2605out_result;

  { // Node ID: 2605 (NodeEqInlined)
    const HWRawBits<19> &id2605in_a = id1741out_result;
    const HWRawBits<19> &id2605in_b = id1740out_value;

    id2605out_result = (eq_bits(id2605in_a,id2605in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1743out_result;

  { // Node ID: 1743 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1743in_a = id1737out_result;

    id1743out_result = (not_fixed(id1743in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1744out_result;

  { // Node ID: 1744 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1744in_a = id2605out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1744in_b = id1743out_result;

    HWOffsetFix<1,0,UNSIGNED> id1744x_1;

    (id1744x_1) = (and_fixed(id1744in_a,id1744in_b));
    id1744out_result = (id1744x_1);
  }
  { // Node ID: 1701 (NodeConstantRawBits)
  }
  HWRawBits<8> id1700out_result;

  { // Node ID: 1700 (NodeSlice)
    const HWFloat<8,12> &id1700in_a = id3032out_output[getCycle()%3];

    id1700out_result = (slice<11,8>(id1700in_a));
  }
  HWRawBits<9> id1702out_result;

  { // Node ID: 1702 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1702in_in0 = id1701out_value;
    const HWRawBits<8> &id1702in_in1 = id1700out_result;

    id1702out_result = (cat(id1702in_in0,id1702in_in1));
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1703out_output;

  { // Node ID: 1703 (NodeReinterpret)
    const HWRawBits<9> &id1703in_input = id1702out_result;

    id1703out_output = (cast_bits2fixed<9,0,TWOSCOMPLEMENT>(id1703in_input));
  }
  { // Node ID: 3047 (NodeConstantRawBits)
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1705out_result;

  { // Node ID: 1705 (NodeSub)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1705in_a = id1703out_output;
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1705in_b = id3047out_value;

    id1705out_result = (sub_fixed<9,0,TWOSCOMPLEMENT,TONEAREVEN>(id1705in_a,id1705in_b));
  }
  { // Node ID: 1706 (NodeConstantRawBits)
  }
  HWOffsetFix<9,0,TWOSCOMPLEMENT> id2665out_result;

  { // Node ID: 2665 (NodeCondAdd)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id2665in_a = id1705out_result;
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id2665in_b = id1706out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2665in_condb = id3009out_output[getCycle()%3];

    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2665x_1;
    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2665x_2;
    HWOffsetFix<9,0,TWOSCOMPLEMENT> id2665x_3;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2665x_1 = (c_hw_fix_9_0_sgn_bits_2);
        break;
      case 1l:
        id2665x_1 = id2665in_a;
        break;
      default:
        id2665x_1 = (c_hw_fix_9_0_sgn_undef);
        break;
    }
    switch((id2665in_condb.getValueAsLong())) {
      case 0l:
        id2665x_2 = (c_hw_fix_9_0_sgn_bits_2);
        break;
      case 1l:
        id2665x_2 = id2665in_b;
        break;
      default:
        id2665x_2 = (c_hw_fix_9_0_sgn_undef);
        break;
    }
    (id2665x_3) = (add_fixed<9,0,TWOSCOMPLEMENT,TRUNCATE>((id2665x_1),(id2665x_2)));
    id2665out_result = (id2665x_3);
  }
  HWRawBits<12> id2380out_result;

  { // Node ID: 2380 (NodeSlice)
    const HWRawBits<48> &id2380in_a = id2373out_dout[getCycle()%3];

    id2380out_result = (slice<36,12>(id2380in_a));
  }
  HWOffsetFix<12,-39,TWOSCOMPLEMENT> id2381out_output;

  { // Node ID: 2381 (NodeReinterpret)
    const HWRawBits<12> &id2381in_input = id2380out_result;

    id2381out_output = (cast_bits2fixed<12,-39,TWOSCOMPLEMENT>(id2381in_input));
  }
  HWOffsetFix<14,-41,TWOSCOMPLEMENT> id1715out_result;

  { // Node ID: 1715 (NodeMul)
    const HWOffsetFix<12,-39,TWOSCOMPLEMENT> &id1715in_a = id2381out_output;
    const HWOffsetFix<2,-2,UNSIGNED> &id1715in_b = id3010out_output[getCycle()%3];

    id1715out_result = (mul_fixed<14,-41,TWOSCOMPLEMENT,TONEAREVEN>(id1715in_a,id1715in_b));
  }
  HWRawBits<12> id2378out_result;

  { // Node ID: 2378 (NodeSlice)
    const HWRawBits<48> &id2378in_a = id2373out_dout[getCycle()%3];

    id2378out_result = (slice<24,12>(id2378in_a));
  }
  HWOffsetFix<12,-29,TWOSCOMPLEMENT> id2379out_output;

  { // Node ID: 2379 (NodeReinterpret)
    const HWRawBits<12> &id2379in_input = id2378out_result;

    id2379out_output = (cast_bits2fixed<12,-29,TWOSCOMPLEMENT>(id2379in_input));
  }
  HWOffsetFix<24,-41,TWOSCOMPLEMENT> id1716out_result;

  { // Node ID: 1716 (NodeAdd)
    const HWOffsetFix<14,-41,TWOSCOMPLEMENT> &id1716in_a = id1715out_result;
    const HWOffsetFix<12,-29,TWOSCOMPLEMENT> &id1716in_b = id2379out_output;

    id1716out_result = (add_fixed<24,-41,TWOSCOMPLEMENT,TONEAREVEN>(id1716in_a,id1716in_b));
  }
  HWOffsetFix<12,-29,TWOSCOMPLEMENT> id1717out_o;

  { // Node ID: 1717 (NodeCast)
    const HWOffsetFix<24,-41,TWOSCOMPLEMENT> &id1717in_i = id1716out_result;

    id1717out_o = (cast_fixed2fixed<12,-29,TWOSCOMPLEMENT,TONEAREVEN>(id1717in_i));
  }
  HWOffsetFix<14,-31,TWOSCOMPLEMENT> id1718out_result;

  { // Node ID: 1718 (NodeMul)
    const HWOffsetFix<12,-29,TWOSCOMPLEMENT> &id1718in_a = id1717out_o;
    const HWOffsetFix<2,-2,UNSIGNED> &id1718in_b = id3010out_output[getCycle()%3];

    id1718out_result = (mul_fixed<14,-31,TWOSCOMPLEMENT,TONEAREVEN>(id1718in_a,id1718in_b));
  }
  HWRawBits<12> id2376out_result;

  { // Node ID: 2376 (NodeSlice)
    const HWRawBits<48> &id2376in_a = id2373out_dout[getCycle()%3];

    id2376out_result = (slice<12,12>(id2376in_a));
  }
  HWOffsetFix<12,-19,TWOSCOMPLEMENT> id2377out_output;

  { // Node ID: 2377 (NodeReinterpret)
    const HWRawBits<12> &id2377in_input = id2376out_result;

    id2377out_output = (cast_bits2fixed<12,-19,TWOSCOMPLEMENT>(id2377in_input));
  }
  HWOffsetFix<24,-31,TWOSCOMPLEMENT> id1719out_result;

  { // Node ID: 1719 (NodeAdd)
    const HWOffsetFix<14,-31,TWOSCOMPLEMENT> &id1719in_a = id1718out_result;
    const HWOffsetFix<12,-19,TWOSCOMPLEMENT> &id1719in_b = id2377out_output;

    id1719out_result = (add_fixed<24,-31,TWOSCOMPLEMENT,TONEAREVEN>(id1719in_a,id1719in_b));
  }
  HWOffsetFix<12,-19,TWOSCOMPLEMENT> id1720out_o;

  { // Node ID: 1720 (NodeCast)
    const HWOffsetFix<24,-31,TWOSCOMPLEMENT> &id1720in_i = id1719out_result;

    id1720out_o = (cast_fixed2fixed<12,-19,TWOSCOMPLEMENT,TONEAREVEN>(id1720in_i));
  }
  HWOffsetFix<14,-21,TWOSCOMPLEMENT> id1721out_result;

  { // Node ID: 1721 (NodeMul)
    const HWOffsetFix<12,-19,TWOSCOMPLEMENT> &id1721in_a = id1720out_o;
    const HWOffsetFix<2,-2,UNSIGNED> &id1721in_b = id3010out_output[getCycle()%3];

    id1721out_result = (mul_fixed<14,-21,TWOSCOMPLEMENT,TONEAREVEN>(id1721in_a,id1721in_b));
  }
  HWRawBits<12> id2374out_result;

  { // Node ID: 2374 (NodeSlice)
    const HWRawBits<48> &id2374in_a = id2373out_dout[getCycle()%3];

    id2374out_result = (slice<0,12>(id2374in_a));
  }
  HWOffsetFix<12,-11,TWOSCOMPLEMENT> id2375out_output;

  { // Node ID: 2375 (NodeReinterpret)
    const HWRawBits<12> &id2375in_input = id2374out_result;

    id2375out_output = (cast_bits2fixed<12,-11,TWOSCOMPLEMENT>(id2375in_input));
  }
  HWOffsetFix<23,-21,TWOSCOMPLEMENT> id1722out_result;

  { // Node ID: 1722 (NodeAdd)
    const HWOffsetFix<14,-21,TWOSCOMPLEMENT> &id1722in_a = id1721out_result;
    const HWOffsetFix<12,-11,TWOSCOMPLEMENT> &id1722in_b = id2375out_output;

    id1722out_result = (add_fixed<23,-21,TWOSCOMPLEMENT,TONEAREVEN>(id1722in_a,id1722in_b));
  }
  HWOffsetFix<12,-10,TWOSCOMPLEMENT> id1723out_o;

  { // Node ID: 1723 (NodeCast)
    const HWOffsetFix<23,-21,TWOSCOMPLEMENT> &id1723in_i = id1722out_result;

    id1723out_o = (cast_fixed2fixed<12,-10,TWOSCOMPLEMENT,TONEAREVEN>(id1723in_i));
  }
  HWOffsetFix<20,-10,TWOSCOMPLEMENT> id1726out_result;

  { // Node ID: 1726 (NodeAdd)
    const HWOffsetFix<9,0,TWOSCOMPLEMENT> &id1726in_a = id2665out_result;
    const HWOffsetFix<12,-10,TWOSCOMPLEMENT> &id1726in_b = id1723out_o;

    id1726out_result = (add_fixed<20,-10,TWOSCOMPLEMENT,TONEAREVEN>(id1726in_a,id1726in_b));
  }
  HWFloat<8,12> id1727out_o;

  { // Node ID: 1727 (NodeCast)
    const HWOffsetFix<20,-10,TWOSCOMPLEMENT> &id1727in_i = id1726out_result;

    id1727out_o = (cast_fixed2float<8,12>(id1727in_i));
  }
  { // Node ID: 3045 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1739out_result;

  { // Node ID: 1739 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1739in_sel = id1737out_result;
    const HWFloat<8,12> &id1739in_option0 = id1727out_o;
    const HWFloat<8,12> &id1739in_option1 = id3045out_value;

    HWFloat<8,12> id1739x_1;

    switch((id1739in_sel.getValueAsLong())) {
      case 0l:
        id1739x_1 = id1739in_option0;
        break;
      case 1l:
        id1739x_1 = id1739in_option1;
        break;
      default:
        id1739x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1739out_result = (id1739x_1);
  }
  { // Node ID: 3044 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1746out_result;

  { // Node ID: 1746 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1746in_sel = id1744out_result;
    const HWFloat<8,12> &id1746in_option0 = id1739out_result;
    const HWFloat<8,12> &id1746in_option1 = id3044out_value;

    HWFloat<8,12> id1746x_1;

    switch((id1746in_sel.getValueAsLong())) {
      case 0l:
        id1746x_1 = id1746in_option0;
        break;
      case 1l:
        id1746x_1 = id1746in_option1;
        break;
      default:
        id1746x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1746out_result = (id1746x_1);
  }
  { // Node ID: 3043 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1752out_result;

  { // Node ID: 1752 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1752in_sel = id1750out_result;
    const HWFloat<8,12> &id1752in_option0 = id1746out_result;
    const HWFloat<8,12> &id1752in_option1 = id3043out_value;

    HWFloat<8,12> id1752x_1;

    switch((id1752in_sel.getValueAsLong())) {
      case 0l:
        id1752x_1 = id1752in_option0;
        break;
      case 1l:
        id1752x_1 = id1752in_option1;
        break;
      default:
        id1752x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1752out_result = (id1752x_1);
  }
  { // Node ID: 1753 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1754out_result;

  { // Node ID: 1754 (NodeMul)
    const HWFloat<8,12> &id1754in_a = id1752out_result;
    const HWFloat<8,12> &id1754in_b = id1753out_value;

    id1754out_result = (mul_float(id1754in_a,id1754in_b));
  }
  HWFloat<8,12> id1757out_result;

  { // Node ID: 1757 (NodeMul)
    const HWFloat<8,12> &id1757in_a = id3055out_value;
    const HWFloat<8,12> &id1757in_b = id1754out_result;

    id1757out_result = (mul_float(id1757in_a,id1757in_b));
  }
  HWFloat<8,12> id1759out_result;

  { // Node ID: 1759 (NodeSub)
    const HWFloat<8,12> &id1759in_a = id3056out_value;
    const HWFloat<8,12> &id1759in_b = id1757out_result;

    id1759out_result = (sub_float(id1759in_a,id1759in_b));
  }
  HWFloat<8,12> id1763out_result;

  { // Node ID: 1763 (NodeSub)
    const HWFloat<8,12> &id1763in_a = id3031out_output[getCycle()%2];
    const HWFloat<8,12> &id1763in_b = id1759out_result;

    id1763out_result = (sub_float(id1763in_a,id1763in_b));
  }
  HWFloat<8,12> id1764out_result;

  { // Node ID: 1764 (NodeMul)
    const HWFloat<8,12> &id1764in_a = id1762out_result;
    const HWFloat<8,12> &id1764in_b = id1763out_result;

    id1764out_result = (mul_float(id1764in_a,id1764in_b));
  }
  HWFloat<8,12> id2298out_result;

  { // Node ID: 2298 (NodeAdd)
    const HWFloat<8,12> &id2298in_a = id2297out_result;
    const HWFloat<8,12> &id2298in_b = id1764out_result;

    id2298out_result = (add_float(id2298in_a,id2298in_b));
  }
  HWFloat<8,12> id2299out_result;

  { // Node ID: 2299 (NodeMul)
    const HWFloat<8,12> &id2299in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id2299in_b = id2298out_result;

    id2299out_result = (mul_float(id2299in_a,id2299in_b));
  }
  HWFloat<8,12> id2300out_result;

  { // Node ID: 2300 (NodeSub)
    const HWFloat<8,12> &id2300in_a = id3031out_output[getCycle()%2];
    const HWFloat<8,12> &id2300in_b = id2299out_result;

    id2300out_result = (sub_float(id2300in_a,id2300in_b));
  }
  HWFloat<8,12> id2704out_output;

  { // Node ID: 2704 (NodeStreamOffset)
    const HWFloat<8,12> &id2704in_input = id2300out_result;

    id2704out_output = id2704in_input;
  }
  { // Node ID: 16 (NodeInputMappedReg)
  }
  { // Node ID: 17 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_sel = id2420out_result[getCycle()%2];
    const HWFloat<8,12> &id17in_option0 = id2704out_output;
    const HWFloat<8,12> &id17in_option1 = id16out_u_in;

    HWFloat<8,12> id17x_1;

    switch((id17in_sel.getValueAsLong())) {
      case 0l:
        id17x_1 = id17in_option0;
        break;
      case 1l:
        id17x_1 = id17in_option1;
        break;
      default:
        id17x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id17out_result[(getCycle()+1)%2] = (id17x_1);
  }
  { // Node ID: 2723 (NodeFIFO)
    const HWFloat<8,12> &id2723in_input = id17out_result[getCycle()%2];

    id2723out_output[(getCycle()+1)%2] = id2723in_input;
  }
  { // Node ID: 3030 (NodeFIFO)
    const HWFloat<8,12> &id3030in_input = id2723out_output[getCycle()%2];

    id3030out_output[(getCycle()+7)%8] = id3030in_input;
  }
  { // Node ID: 3031 (NodeFIFO)
    const HWFloat<8,12> &id3031in_input = id3030out_output[getCycle()%8];

    id3031out_output[(getCycle()+1)%2] = id3031in_input;
  }
  { // Node ID: 12 (NodeInputMappedReg)
  }
  { // Node ID: 13 (NodeCast)
    const HWFloat<11,53> &id13in_i = id12out_dt;

    id13out_o[(getCycle()+3)%4] = (cast_float2float<8,12>(id13in_i));
  }
  { // Node ID: 3295 (NodeConstantRawBits)
  }
  { // Node ID: 3294 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2027out_result;

  { // Node ID: 2027 (NodeAdd)
    const HWFloat<8,12> &id2027in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id2027in_b = id3294out_value;

    id2027out_result = (add_float(id2027in_a,id2027in_b));
  }
  HWFloat<8,12> id2029out_result;

  { // Node ID: 2029 (NodeMul)
    const HWFloat<8,12> &id2029in_a = id3295out_value;
    const HWFloat<8,12> &id2029in_b = id2027out_result;

    id2029out_result = (mul_float(id2029in_a,id2029in_b));
  }
  HWRawBits<8> id2096out_result;

  { // Node ID: 2096 (NodeSlice)
    const HWFloat<8,12> &id2096in_a = id2029out_result;

    id2096out_result = (slice<11,8>(id2096in_a));
  }
  { // Node ID: 2097 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2421out_result;

  { // Node ID: 2421 (NodeEqInlined)
    const HWRawBits<8> &id2421in_a = id2096out_result;
    const HWRawBits<8> &id2421in_b = id2097out_value;

    id2421out_result = (eq_bits(id2421in_a,id2421in_b));
  }
  HWRawBits<11> id2095out_result;

  { // Node ID: 2095 (NodeSlice)
    const HWFloat<8,12> &id2095in_a = id2029out_result;

    id2095out_result = (slice<0,11>(id2095in_a));
  }
  { // Node ID: 3293 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2422out_result;

  { // Node ID: 2422 (NodeNeqInlined)
    const HWRawBits<11> &id2422in_a = id2095out_result;
    const HWRawBits<11> &id2422in_b = id3293out_value;

    id2422out_result = (neq_bits(id2422in_a,id2422in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2101out_result;

  { // Node ID: 2101 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2101in_a = id2421out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2101in_b = id2422out_result;

    HWOffsetFix<1,0,UNSIGNED> id2101x_1;

    (id2101x_1) = (and_fixed(id2101in_a,id2101in_b));
    id2101out_result = (id2101x_1);
  }
  { // Node ID: 2722 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2722in_input = id2101out_result;

    id2722out_output[(getCycle()+8)%9] = id2722in_input;
  }
  { // Node ID: 2030 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2031out_output;
  HWOffsetFix<1,0,UNSIGNED> id2031out_output_doubt;

  { // Node ID: 2031 (NodeDoubtBitOp)
    const HWFloat<8,12> &id2031in_input = id2029out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2031in_doubt = id2030out_value;

    id2031out_output = id2031in_input;
    id2031out_output_doubt = id2031in_doubt;
  }
  { // Node ID: 2032 (NodeCast)
    const HWFloat<8,12> &id2032in_i = id2031out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2032in_i_doubt = id2031out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2032x_1;

    id2032out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2032in_i,(&(id2032x_1))));
    id2032out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2032x_1),(c_hw_fix_4_0_uns_bits))),id2032in_i_doubt));
  }
  { // Node ID: 2035 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id2034out_result;
  HWOffsetFix<1,0,UNSIGNED> id2034out_result_doubt;

  { // Node ID: 2034 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2034in_a = id2032out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2034in_a_doubt = id2032out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id2034in_b = id2035out_value;

    HWOffsetFix<1,0,UNSIGNED> id2034x_1;

    id2034out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id2034in_a,id2034in_b,(&(id2034x_1))));
    id2034out_result_doubt = (or_fixed((neq_fixed((id2034x_1),(c_hw_fix_1_0_uns_bits_1))),id2034in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2036out_o;
  HWOffsetFix<1,0,UNSIGNED> id2036out_o_doubt;

  { // Node ID: 2036 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id2036in_i = id2034out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2036in_i_doubt = id2034out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2036x_1;

    id2036out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2036in_i,(&(id2036x_1))));
    id2036out_o_doubt = (or_fixed((neq_fixed((id2036x_1),(c_hw_fix_1_0_uns_bits_1))),id2036in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2045out_output;

  { // Node ID: 2045 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2045in_input = id2036out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2045in_input_doubt = id2036out_o_doubt;

    id2045out_output = id2045in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2046out_o;

  { // Node ID: 2046 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2046in_i = id2045out_output;

    id2046out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2046in_i));
  }
  { // Node ID: 2713 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2713in_input = id2046out_o;

    id2713out_output[(getCycle()+2)%3] = id2713in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2063out_o;

  { // Node ID: 2063 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2063in_i = id2713out_output[getCycle()%3];

    id2063out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2063in_i));
  }
  { // Node ID: 2066 (NodeConstantRawBits)
  }
  { // Node ID: 2614 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id2047out_o;

  { // Node ID: 2047 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2047in_i = id2045out_output;

    id2047out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id2047in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id2105out_output;

  { // Node ID: 2105 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id2105in_input = id2047out_o;

    id2105out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id2105in_input))));
  }
  { // Node ID: 2106 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id2106in_addr = id2105out_output;

    HWOffsetFix<12,-12,UNSIGNED> id2106x_1;

    switch(((long)((id2106in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id2106x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id2106x_1 = (id2106sta_rom_store[(id2106in_addr.getValueAsLong())]);
        break;
      default:
        id2106x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id2106out_dout[(getCycle()+2)%3] = (id2106x_1);
  }
  { // Node ID: 2051 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id2048out_o;

  { // Node ID: 2048 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2048in_i = id2045out_output;

    id2048out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id2048in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id2050out_result;

  { // Node ID: 2050 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id2050in_a = id2051out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id2050in_b = id2048out_o;

    id2050out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id2050in_a,id2050in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id2052out_o;

  { // Node ID: 2052 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id2052in_i = id2050out_result;

    id2052out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id2052in_i));
  }
  { // Node ID: 2714 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2714in_input = id2052out_o;

    id2714out_output[(getCycle()+2)%3] = id2714in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id2053out_result;

  { // Node ID: 2053 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id2053in_a = id2106out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id2053in_b = id2714out_output[getCycle()%3];

    id2053out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id2053in_a,id2053in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id2054out_result;

  { // Node ID: 2054 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id2054in_a = id2714out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id2054in_b = id2106out_dout[getCycle()%3];

    id2054out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id2054in_a,id2054in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id2055out_result;

  { // Node ID: 2055 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id2055in_a = id2053out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id2055in_b = id2054out_result;

    id2055out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id2055in_a,id2055in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id2056out_o;

  { // Node ID: 2056 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id2056in_i = id2055out_result;

    id2056out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id2056in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id2057out_o;

  { // Node ID: 2057 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id2057in_i = id2056out_o;

    id2057out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id2057in_i));
  }
  { // Node ID: 3292 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2423out_result;

  { // Node ID: 2423 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2423in_a = id2057out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2423in_b = id3292out_value;

    id2423out_result = (gte_fixed(id2423in_a,id2423in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2690out_result;

  { // Node ID: 2690 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2690in_a = id2063out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2690in_b = id2066out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2690in_c = id2614out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2690in_condb = id2423out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2690x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2690x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2690x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2690x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2690x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2690x_1 = id2690in_a;
        break;
      default:
        id2690x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2690in_condb.getValueAsLong())) {
      case 0l:
        id2690x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2690x_2 = id2690in_b;
        break;
      default:
        id2690x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2690x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2690x_3 = id2690in_c;
        break;
      default:
        id2690x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2690x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2690x_1),(id2690x_2))),(id2690x_3)));
    id2690out_result = (id2690x_4);
  }
  HWRawBits<1> id2424out_result;

  { // Node ID: 2424 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2424in_a = id2690out_result;

    id2424out_result = (slice<10,1>(id2424in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2425out_output;

  { // Node ID: 2425 (NodeReinterpret)
    const HWRawBits<1> &id2425in_input = id2424out_result;

    id2425out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2425in_input));
  }
  { // Node ID: 3291 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2038out_result;

  { // Node ID: 2038 (NodeGt)
    const HWFloat<8,12> &id2038in_a = id2029out_result;
    const HWFloat<8,12> &id2038in_b = id3291out_value;

    id2038out_result = (gt_float(id2038in_a,id2038in_b));
  }
  { // Node ID: 2716 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2716in_input = id2038out_result;

    id2716out_output[(getCycle()+6)%7] = id2716in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2039out_output;

  { // Node ID: 2039 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2039in_input = id2036out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2039in_input_doubt = id2036out_o_doubt;

    id2039out_output = id2039in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2040out_result;

  { // Node ID: 2040 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2040in_a = id2716out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2040in_b = id2039out_output;

    HWOffsetFix<1,0,UNSIGNED> id2040x_1;

    (id2040x_1) = (and_fixed(id2040in_a,id2040in_b));
    id2040out_result = (id2040x_1);
  }
  { // Node ID: 2717 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2717in_input = id2040out_result;

    id2717out_output[(getCycle()+2)%3] = id2717in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2072out_result;

  { // Node ID: 2072 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2072in_a = id2717out_output[getCycle()%3];

    id2072out_result = (not_fixed(id2072in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2073out_result;

  { // Node ID: 2073 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2073in_a = id2425out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2073in_b = id2072out_result;

    HWOffsetFix<1,0,UNSIGNED> id2073x_1;

    (id2073x_1) = (and_fixed(id2073in_a,id2073in_b));
    id2073out_result = (id2073x_1);
  }
  { // Node ID: 3290 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2042out_result;

  { // Node ID: 2042 (NodeLt)
    const HWFloat<8,12> &id2042in_a = id2029out_result;
    const HWFloat<8,12> &id2042in_b = id3290out_value;

    id2042out_result = (lt_float(id2042in_a,id2042in_b));
  }
  { // Node ID: 2718 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2718in_input = id2042out_result;

    id2718out_output[(getCycle()+6)%7] = id2718in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2043out_output;

  { // Node ID: 2043 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2043in_input = id2036out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2043in_input_doubt = id2036out_o_doubt;

    id2043out_output = id2043in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2044out_result;

  { // Node ID: 2044 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2044in_a = id2718out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2044in_b = id2043out_output;

    HWOffsetFix<1,0,UNSIGNED> id2044x_1;

    (id2044x_1) = (and_fixed(id2044in_a,id2044in_b));
    id2044out_result = (id2044x_1);
  }
  { // Node ID: 2719 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2719in_input = id2044out_result;

    id2719out_output[(getCycle()+2)%3] = id2719in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2074out_result;

  { // Node ID: 2074 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2074in_a = id2073out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2074in_b = id2719out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2074x_1;

    (id2074x_1) = (or_fixed(id2074in_a,id2074in_b));
    id2074out_result = (id2074x_1);
  }
  { // Node ID: 3289 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2426out_result;

  { // Node ID: 2426 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2426in_a = id2690out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2426in_b = id3289out_value;

    id2426out_result = (gte_fixed(id2426in_a,id2426in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2081out_result;

  { // Node ID: 2081 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2081in_a = id2719out_output[getCycle()%3];

    id2081out_result = (not_fixed(id2081in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2082out_result;

  { // Node ID: 2082 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2082in_a = id2426out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2082in_b = id2081out_result;

    HWOffsetFix<1,0,UNSIGNED> id2082x_1;

    (id2082x_1) = (and_fixed(id2082in_a,id2082in_b));
    id2082out_result = (id2082x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2083out_result;

  { // Node ID: 2083 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2083in_a = id2082out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2083in_b = id2717out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2083x_1;

    (id2083x_1) = (or_fixed(id2083in_a,id2083in_b));
    id2083out_result = (id2083x_1);
  }
  HWRawBits<2> id2084out_result;

  { // Node ID: 2084 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2084in_in0 = id2074out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2084in_in1 = id2083out_result;

    id2084out_result = (cat(id2084in_in0,id2084in_in1));
  }
  { // Node ID: 2076 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id2075out_o;

  { // Node ID: 2075 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2075in_i = id2690out_result;

    id2075out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id2075in_i));
  }
  { // Node ID: 2060 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id2061out_result;

  { // Node ID: 2061 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2061in_sel = id2423out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id2061in_option0 = id2057out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2061in_option1 = id2060out_value;

    HWOffsetFix<12,-11,UNSIGNED> id2061x_1;

    switch((id2061in_sel.getValueAsLong())) {
      case 0l:
        id2061x_1 = id2061in_option0;
        break;
      case 1l:
        id2061x_1 = id2061in_option1;
        break;
      default:
        id2061x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id2061out_result = (id2061x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id2062out_o;

  { // Node ID: 2062 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id2062in_i = id2061out_result;

    id2062out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id2062in_i));
  }
  HWRawBits<20> id2077out_result;

  { // Node ID: 2077 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2077in_in0 = id2076out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id2077in_in1 = id2075out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id2077in_in2 = id2062out_o;

    id2077out_result = (cat((cat(id2077in_in0,id2077in_in1)),id2077in_in2));
  }
  HWFloat<8,12> id2078out_output;

  { // Node ID: 2078 (NodeReinterpret)
    const HWRawBits<20> &id2078in_input = id2077out_result;

    id2078out_output = (cast_bits2float<8,12>(id2078in_input));
  }
  { // Node ID: 2085 (NodeConstantRawBits)
  }
  { // Node ID: 2086 (NodeConstantRawBits)
  }
  { // Node ID: 2088 (NodeConstantRawBits)
  }
  HWRawBits<20> id2427out_result;

  { // Node ID: 2427 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2427in_in0 = id2085out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2427in_in1 = id2086out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2427in_in2 = id2088out_value;

    id2427out_result = (cat((cat(id2427in_in0,id2427in_in1)),id2427in_in2));
  }
  HWFloat<8,12> id2090out_output;

  { // Node ID: 2090 (NodeReinterpret)
    const HWRawBits<20> &id2090in_input = id2427out_result;

    id2090out_output = (cast_bits2float<8,12>(id2090in_input));
  }
  { // Node ID: 2394 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2093out_result;

  { // Node ID: 2093 (NodeMux)
    const HWRawBits<2> &id2093in_sel = id2084out_result;
    const HWFloat<8,12> &id2093in_option0 = id2078out_output;
    const HWFloat<8,12> &id2093in_option1 = id2090out_output;
    const HWFloat<8,12> &id2093in_option2 = id2394out_value;
    const HWFloat<8,12> &id2093in_option3 = id2090out_output;

    HWFloat<8,12> id2093x_1;

    switch((id2093in_sel.getValueAsLong())) {
      case 0l:
        id2093x_1 = id2093in_option0;
        break;
      case 1l:
        id2093x_1 = id2093in_option1;
        break;
      case 2l:
        id2093x_1 = id2093in_option2;
        break;
      case 3l:
        id2093x_1 = id2093in_option3;
        break;
      default:
        id2093x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2093out_result = (id2093x_1);
  }
  { // Node ID: 3288 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2103out_result;

  { // Node ID: 2103 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2103in_sel = id2722out_output[getCycle()%9];
    const HWFloat<8,12> &id2103in_option0 = id2093out_result;
    const HWFloat<8,12> &id2103in_option1 = id3288out_value;

    HWFloat<8,12> id2103x_1;

    switch((id2103in_sel.getValueAsLong())) {
      case 0l:
        id2103x_1 = id2103in_option0;
        break;
      case 1l:
        id2103x_1 = id2103in_option1;
        break;
      default:
        id2103x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id2103out_result = (id2103x_1);
  }
  { // Node ID: 3287 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2108out_result;

  { // Node ID: 2108 (NodeSub)
    const HWFloat<8,12> &id2108in_a = id2103out_result;
    const HWFloat<8,12> &id2108in_b = id3287out_value;

    id2108out_result = (sub_float(id2108in_a,id2108in_b));
  }
  { // Node ID: 2691 (NodePO2FPMult)
    const HWFloat<8,12> &id2691in_floatIn = id2108out_result;

    id2691out_floatOut[(getCycle()+1)%2] = (mul_float(id2691in_floatIn,(c_hw_flt_8_12_4_0val)));
  }
  { // Node ID: 3286 (NodeConstantRawBits)
  }
  { // Node ID: 3285 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1775out_result;

  { // Node ID: 1775 (NodeAdd)
    const HWFloat<8,12> &id1775in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id1775in_b = id3285out_value;

    id1775out_result = (add_float(id1775in_a,id1775in_b));
  }
  HWFloat<8,12> id1777out_result;

  { // Node ID: 1777 (NodeMul)
    const HWFloat<8,12> &id1777in_a = id3286out_value;
    const HWFloat<8,12> &id1777in_b = id1775out_result;

    id1777out_result = (mul_float(id1777in_a,id1777in_b));
  }
  HWRawBits<8> id1844out_result;

  { // Node ID: 1844 (NodeSlice)
    const HWFloat<8,12> &id1844in_a = id1777out_result;

    id1844out_result = (slice<11,8>(id1844in_a));
  }
  { // Node ID: 1845 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2428out_result;

  { // Node ID: 2428 (NodeEqInlined)
    const HWRawBits<8> &id2428in_a = id1844out_result;
    const HWRawBits<8> &id2428in_b = id1845out_value;

    id2428out_result = (eq_bits(id2428in_a,id2428in_b));
  }
  HWRawBits<11> id1843out_result;

  { // Node ID: 1843 (NodeSlice)
    const HWFloat<8,12> &id1843in_a = id1777out_result;

    id1843out_result = (slice<0,11>(id1843in_a));
  }
  { // Node ID: 3284 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2429out_result;

  { // Node ID: 2429 (NodeNeqInlined)
    const HWRawBits<11> &id2429in_a = id1843out_result;
    const HWRawBits<11> &id2429in_b = id3284out_value;

    id2429out_result = (neq_bits(id2429in_a,id2429in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1849out_result;

  { // Node ID: 1849 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1849in_a = id2428out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1849in_b = id2429out_result;

    HWOffsetFix<1,0,UNSIGNED> id1849x_1;

    (id1849x_1) = (and_fixed(id1849in_a,id1849in_b));
    id1849out_result = (id1849x_1);
  }
  { // Node ID: 2733 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2733in_input = id1849out_result;

    id2733out_output[(getCycle()+8)%9] = id2733in_input;
  }
  { // Node ID: 1778 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1779out_output;
  HWOffsetFix<1,0,UNSIGNED> id1779out_output_doubt;

  { // Node ID: 1779 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1779in_input = id1777out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1779in_doubt = id1778out_value;

    id1779out_output = id1779in_input;
    id1779out_output_doubt = id1779in_doubt;
  }
  { // Node ID: 1780 (NodeCast)
    const HWFloat<8,12> &id1780in_i = id1779out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1780in_i_doubt = id1779out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1780x_1;

    id1780out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1780in_i,(&(id1780x_1))));
    id1780out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1780x_1),(c_hw_fix_4_0_uns_bits))),id1780in_i_doubt));
  }
  { // Node ID: 1783 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1782out_result;
  HWOffsetFix<1,0,UNSIGNED> id1782out_result_doubt;

  { // Node ID: 1782 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1782in_a = id1780out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1782in_a_doubt = id1780out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1782in_b = id1783out_value;

    HWOffsetFix<1,0,UNSIGNED> id1782x_1;

    id1782out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1782in_a,id1782in_b,(&(id1782x_1))));
    id1782out_result_doubt = (or_fixed((neq_fixed((id1782x_1),(c_hw_fix_1_0_uns_bits_1))),id1782in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1784out_o;
  HWOffsetFix<1,0,UNSIGNED> id1784out_o_doubt;

  { // Node ID: 1784 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1784in_i = id1782out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1784in_i_doubt = id1782out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1784x_1;

    id1784out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1784in_i,(&(id1784x_1))));
    id1784out_o_doubt = (or_fixed((neq_fixed((id1784x_1),(c_hw_fix_1_0_uns_bits_1))),id1784in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1793out_output;

  { // Node ID: 1793 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1793in_input = id1784out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1793in_input_doubt = id1784out_o_doubt;

    id1793out_output = id1793in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1794out_o;

  { // Node ID: 1794 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1794in_i = id1793out_output;

    id1794out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1794in_i));
  }
  { // Node ID: 2724 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2724in_input = id1794out_o;

    id2724out_output[(getCycle()+2)%3] = id2724in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id1795out_o;

  { // Node ID: 1795 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1795in_i = id1793out_output;

    id1795out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1795in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1853out_output;

  { // Node ID: 1853 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1853in_input = id1795out_o;

    id1853out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1853in_input))));
  }
  { // Node ID: 1854 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1854in_addr = id1853out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1854x_1;

    switch(((long)((id1854in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1854x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1854x_1 = (id1854sta_rom_store[(id1854in_addr.getValueAsLong())]);
        break;
      default:
        id1854x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1854out_dout[(getCycle()+2)%3] = (id1854x_1);
  }
  { // Node ID: 1799 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1796out_o;

  { // Node ID: 1796 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1796in_i = id1793out_output;

    id1796out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1796in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1798out_result;

  { // Node ID: 1798 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1798in_a = id1799out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1798in_b = id1796out_o;

    id1798out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1798in_a,id1798in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1800out_o;

  { // Node ID: 1800 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1800in_i = id1798out_result;

    id1800out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1800in_i));
  }
  { // Node ID: 2725 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2725in_input = id1800out_o;

    id2725out_output[(getCycle()+2)%3] = id2725in_input;
  }
  { // Node ID: 3282 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1786out_result;

  { // Node ID: 1786 (NodeGt)
    const HWFloat<8,12> &id1786in_a = id1777out_result;
    const HWFloat<8,12> &id1786in_b = id3282out_value;

    id1786out_result = (gt_float(id1786in_a,id1786in_b));
  }
  { // Node ID: 2727 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2727in_input = id1786out_result;

    id2727out_output[(getCycle()+6)%7] = id2727in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1787out_output;

  { // Node ID: 1787 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1787in_input = id1784out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1787in_input_doubt = id1784out_o_doubt;

    id1787out_output = id1787in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1788out_result;

  { // Node ID: 1788 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1788in_a = id2727out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1788in_b = id1787out_output;

    HWOffsetFix<1,0,UNSIGNED> id1788x_1;

    (id1788x_1) = (and_fixed(id1788in_a,id1788in_b));
    id1788out_result = (id1788x_1);
  }
  { // Node ID: 2728 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2728in_input = id1788out_result;

    id2728out_output[(getCycle()+2)%3] = id2728in_input;
  }
  { // Node ID: 3281 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1790out_result;

  { // Node ID: 1790 (NodeLt)
    const HWFloat<8,12> &id1790in_a = id1777out_result;
    const HWFloat<8,12> &id1790in_b = id3281out_value;

    id1790out_result = (lt_float(id1790in_a,id1790in_b));
  }
  { // Node ID: 2729 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2729in_input = id1790out_result;

    id2729out_output[(getCycle()+6)%7] = id2729in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1791out_output;

  { // Node ID: 1791 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1791in_input = id1784out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1791in_input_doubt = id1784out_o_doubt;

    id1791out_output = id1791in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1792out_result;

  { // Node ID: 1792 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1792in_a = id2729out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1792in_b = id1791out_output;

    HWOffsetFix<1,0,UNSIGNED> id1792x_1;

    (id1792x_1) = (and_fixed(id1792in_a,id1792in_b));
    id1792out_result = (id1792x_1);
  }
  { // Node ID: 2730 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2730in_input = id1792out_result;

    id2730out_output[(getCycle()+2)%3] = id2730in_input;
  }
  { // Node ID: 3278 (NodeConstantRawBits)
  }
  { // Node ID: 3277 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1856out_result;

  { // Node ID: 1856 (NodeAdd)
    const HWFloat<8,12> &id1856in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id1856in_b = id3277out_value;

    id1856out_result = (add_float(id1856in_a,id1856in_b));
  }
  HWFloat<8,12> id1858out_result;

  { // Node ID: 1858 (NodeMul)
    const HWFloat<8,12> &id1858in_a = id3278out_value;
    const HWFloat<8,12> &id1858in_b = id1856out_result;

    id1858out_result = (mul_float(id1858in_a,id1858in_b));
  }
  HWRawBits<8> id1925out_result;

  { // Node ID: 1925 (NodeSlice)
    const HWFloat<8,12> &id1925in_a = id1858out_result;

    id1925out_result = (slice<11,8>(id1925in_a));
  }
  { // Node ID: 1926 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2435out_result;

  { // Node ID: 2435 (NodeEqInlined)
    const HWRawBits<8> &id2435in_a = id1925out_result;
    const HWRawBits<8> &id2435in_b = id1926out_value;

    id2435out_result = (eq_bits(id2435in_a,id2435in_b));
  }
  HWRawBits<11> id1924out_result;

  { // Node ID: 1924 (NodeSlice)
    const HWFloat<8,12> &id1924in_a = id1858out_result;

    id1924out_result = (slice<0,11>(id1924in_a));
  }
  { // Node ID: 3276 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2436out_result;

  { // Node ID: 2436 (NodeNeqInlined)
    const HWRawBits<11> &id2436in_a = id1924out_result;
    const HWRawBits<11> &id2436in_b = id3276out_value;

    id2436out_result = (neq_bits(id2436in_a,id2436in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1930out_result;

  { // Node ID: 1930 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1930in_a = id2435out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1930in_b = id2436out_result;

    HWOffsetFix<1,0,UNSIGNED> id1930x_1;

    (id1930x_1) = (and_fixed(id1930in_a,id1930in_b));
    id1930out_result = (id1930x_1);
  }
  { // Node ID: 2744 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2744in_input = id1930out_result;

    id2744out_output[(getCycle()+8)%9] = id2744in_input;
  }
  { // Node ID: 1859 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1860out_output;
  HWOffsetFix<1,0,UNSIGNED> id1860out_output_doubt;

  { // Node ID: 1860 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1860in_input = id1858out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1860in_doubt = id1859out_value;

    id1860out_output = id1860in_input;
    id1860out_output_doubt = id1860in_doubt;
  }
  { // Node ID: 1861 (NodeCast)
    const HWFloat<8,12> &id1861in_i = id1860out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1861in_i_doubt = id1860out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1861x_1;

    id1861out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1861in_i,(&(id1861x_1))));
    id1861out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1861x_1),(c_hw_fix_4_0_uns_bits))),id1861in_i_doubt));
  }
  { // Node ID: 1864 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1863out_result;
  HWOffsetFix<1,0,UNSIGNED> id1863out_result_doubt;

  { // Node ID: 1863 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1863in_a = id1861out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1863in_a_doubt = id1861out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1863in_b = id1864out_value;

    HWOffsetFix<1,0,UNSIGNED> id1863x_1;

    id1863out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1863in_a,id1863in_b,(&(id1863x_1))));
    id1863out_result_doubt = (or_fixed((neq_fixed((id1863x_1),(c_hw_fix_1_0_uns_bits_1))),id1863in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1865out_o;
  HWOffsetFix<1,0,UNSIGNED> id1865out_o_doubt;

  { // Node ID: 1865 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1865in_i = id1863out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1865in_i_doubt = id1863out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1865x_1;

    id1865out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1865in_i,(&(id1865x_1))));
    id1865out_o_doubt = (or_fixed((neq_fixed((id1865x_1),(c_hw_fix_1_0_uns_bits_1))),id1865in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1874out_output;

  { // Node ID: 1874 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1874in_input = id1865out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1874in_input_doubt = id1865out_o_doubt;

    id1874out_output = id1874in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1875out_o;

  { // Node ID: 1875 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1875in_i = id1874out_output;

    id1875out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1875in_i));
  }
  { // Node ID: 2735 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2735in_input = id1875out_o;

    id2735out_output[(getCycle()+2)%3] = id2735in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id1876out_o;

  { // Node ID: 1876 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1876in_i = id1874out_output;

    id1876out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1876in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1934out_output;

  { // Node ID: 1934 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1934in_input = id1876out_o;

    id1934out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1934in_input))));
  }
  { // Node ID: 1935 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1935in_addr = id1934out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1935x_1;

    switch(((long)((id1935in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1935x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1935x_1 = (id1935sta_rom_store[(id1935in_addr.getValueAsLong())]);
        break;
      default:
        id1935x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1935out_dout[(getCycle()+2)%3] = (id1935x_1);
  }
  { // Node ID: 1880 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1877out_o;

  { // Node ID: 1877 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1877in_i = id1874out_output;

    id1877out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1877in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1879out_result;

  { // Node ID: 1879 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1879in_a = id1880out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1879in_b = id1877out_o;

    id1879out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1879in_a,id1879in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1881out_o;

  { // Node ID: 1881 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1881in_i = id1879out_result;

    id1881out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1881in_i));
  }
  { // Node ID: 2736 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2736in_input = id1881out_o;

    id2736out_output[(getCycle()+2)%3] = id2736in_input;
  }
  { // Node ID: 3274 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1867out_result;

  { // Node ID: 1867 (NodeGt)
    const HWFloat<8,12> &id1867in_a = id1858out_result;
    const HWFloat<8,12> &id1867in_b = id3274out_value;

    id1867out_result = (gt_float(id1867in_a,id1867in_b));
  }
  { // Node ID: 2738 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2738in_input = id1867out_result;

    id2738out_output[(getCycle()+6)%7] = id2738in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1868out_output;

  { // Node ID: 1868 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1868in_input = id1865out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1868in_input_doubt = id1865out_o_doubt;

    id1868out_output = id1868in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1869out_result;

  { // Node ID: 1869 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1869in_a = id2738out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1869in_b = id1868out_output;

    HWOffsetFix<1,0,UNSIGNED> id1869x_1;

    (id1869x_1) = (and_fixed(id1869in_a,id1869in_b));
    id1869out_result = (id1869x_1);
  }
  { // Node ID: 2739 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2739in_input = id1869out_result;

    id2739out_output[(getCycle()+2)%3] = id2739in_input;
  }
  { // Node ID: 3273 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1871out_result;

  { // Node ID: 1871 (NodeLt)
    const HWFloat<8,12> &id1871in_a = id1858out_result;
    const HWFloat<8,12> &id1871in_b = id3273out_value;

    id1871out_result = (lt_float(id1871in_a,id1871in_b));
  }
  { // Node ID: 2740 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2740in_input = id1871out_result;

    id2740out_output[(getCycle()+6)%7] = id2740in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1872out_output;

  { // Node ID: 1872 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1872in_input = id1865out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1872in_input_doubt = id1865out_o_doubt;

    id1872out_output = id1872in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1873out_result;

  { // Node ID: 1873 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1873in_a = id2740out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1873in_b = id1872out_output;

    HWOffsetFix<1,0,UNSIGNED> id1873x_1;

    (id1873x_1) = (and_fixed(id1873in_a,id1873in_b));
    id1873out_result = (id1873x_1);
  }
  { // Node ID: 2741 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2741in_input = id1873out_result;

    id2741out_output[(getCycle()+2)%3] = id2741in_input;
  }
  { // Node ID: 3267 (NodeConstantRawBits)
  }
  { // Node ID: 3266 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1938out_result;

  { // Node ID: 1938 (NodeAdd)
    const HWFloat<8,12> &id1938in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id1938in_b = id3266out_value;

    id1938out_result = (add_float(id1938in_a,id1938in_b));
  }
  HWFloat<8,12> id1940out_result;

  { // Node ID: 1940 (NodeMul)
    const HWFloat<8,12> &id1940in_a = id3267out_value;
    const HWFloat<8,12> &id1940in_b = id1938out_result;

    id1940out_result = (mul_float(id1940in_a,id1940in_b));
  }
  HWRawBits<8> id2007out_result;

  { // Node ID: 2007 (NodeSlice)
    const HWFloat<8,12> &id2007in_a = id1940out_result;

    id2007out_result = (slice<11,8>(id2007in_a));
  }
  { // Node ID: 2008 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2442out_result;

  { // Node ID: 2442 (NodeEqInlined)
    const HWRawBits<8> &id2442in_a = id2007out_result;
    const HWRawBits<8> &id2442in_b = id2008out_value;

    id2442out_result = (eq_bits(id2442in_a,id2442in_b));
  }
  HWRawBits<11> id2006out_result;

  { // Node ID: 2006 (NodeSlice)
    const HWFloat<8,12> &id2006in_a = id1940out_result;

    id2006out_result = (slice<0,11>(id2006in_a));
  }
  { // Node ID: 3265 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2443out_result;

  { // Node ID: 2443 (NodeNeqInlined)
    const HWRawBits<11> &id2443in_a = id2006out_result;
    const HWRawBits<11> &id2443in_b = id3265out_value;

    id2443out_result = (neq_bits(id2443in_a,id2443in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2012out_result;

  { // Node ID: 2012 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2012in_a = id2442out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2012in_b = id2443out_result;

    HWOffsetFix<1,0,UNSIGNED> id2012x_1;

    (id2012x_1) = (and_fixed(id2012in_a,id2012in_b));
    id2012out_result = (id2012x_1);
  }
  { // Node ID: 2756 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2756in_input = id2012out_result;

    id2756out_output[(getCycle()+8)%9] = id2756in_input;
  }
  { // Node ID: 1941 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1942out_output;
  HWOffsetFix<1,0,UNSIGNED> id1942out_output_doubt;

  { // Node ID: 1942 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1942in_input = id1940out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1942in_doubt = id1941out_value;

    id1942out_output = id1942in_input;
    id1942out_output_doubt = id1942in_doubt;
  }
  { // Node ID: 1943 (NodeCast)
    const HWFloat<8,12> &id1943in_i = id1942out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1943in_i_doubt = id1942out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1943x_1;

    id1943out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1943in_i,(&(id1943x_1))));
    id1943out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1943x_1),(c_hw_fix_4_0_uns_bits))),id1943in_i_doubt));
  }
  { // Node ID: 1946 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1945out_result;
  HWOffsetFix<1,0,UNSIGNED> id1945out_result_doubt;

  { // Node ID: 1945 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1945in_a = id1943out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1945in_a_doubt = id1943out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1945in_b = id1946out_value;

    HWOffsetFix<1,0,UNSIGNED> id1945x_1;

    id1945out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1945in_a,id1945in_b,(&(id1945x_1))));
    id1945out_result_doubt = (or_fixed((neq_fixed((id1945x_1),(c_hw_fix_1_0_uns_bits_1))),id1945in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1947out_o;
  HWOffsetFix<1,0,UNSIGNED> id1947out_o_doubt;

  { // Node ID: 1947 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1947in_i = id1945out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1947in_i_doubt = id1945out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1947x_1;

    id1947out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1947in_i,(&(id1947x_1))));
    id1947out_o_doubt = (or_fixed((neq_fixed((id1947x_1),(c_hw_fix_1_0_uns_bits_1))),id1947in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1956out_output;

  { // Node ID: 1956 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1956in_input = id1947out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1956in_input_doubt = id1947out_o_doubt;

    id1956out_output = id1956in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1957out_o;

  { // Node ID: 1957 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1957in_i = id1956out_output;

    id1957out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1957in_i));
  }
  { // Node ID: 2747 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2747in_input = id1957out_o;

    id2747out_output[(getCycle()+2)%3] = id2747in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id1958out_o;

  { // Node ID: 1958 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1958in_i = id1956out_output;

    id1958out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1958in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id2016out_output;

  { // Node ID: 2016 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id2016in_input = id1958out_o;

    id2016out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id2016in_input))));
  }
  { // Node ID: 2017 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id2017in_addr = id2016out_output;

    HWOffsetFix<12,-12,UNSIGNED> id2017x_1;

    switch(((long)((id2017in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id2017x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id2017x_1 = (id2017sta_rom_store[(id2017in_addr.getValueAsLong())]);
        break;
      default:
        id2017x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id2017out_dout[(getCycle()+2)%3] = (id2017x_1);
  }
  { // Node ID: 1962 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1959out_o;

  { // Node ID: 1959 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1959in_i = id1956out_output;

    id1959out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1959in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1961out_result;

  { // Node ID: 1961 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1961in_a = id1962out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1961in_b = id1959out_o;

    id1961out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1961in_a,id1961in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1963out_o;

  { // Node ID: 1963 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1963in_i = id1961out_result;

    id1963out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1963in_i));
  }
  { // Node ID: 2748 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2748in_input = id1963out_o;

    id2748out_output[(getCycle()+2)%3] = id2748in_input;
  }
  { // Node ID: 3263 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1949out_result;

  { // Node ID: 1949 (NodeGt)
    const HWFloat<8,12> &id1949in_a = id1940out_result;
    const HWFloat<8,12> &id1949in_b = id3263out_value;

    id1949out_result = (gt_float(id1949in_a,id1949in_b));
  }
  { // Node ID: 2750 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2750in_input = id1949out_result;

    id2750out_output[(getCycle()+6)%7] = id2750in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1950out_output;

  { // Node ID: 1950 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1950in_input = id1947out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1950in_input_doubt = id1947out_o_doubt;

    id1950out_output = id1950in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1951out_result;

  { // Node ID: 1951 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1951in_a = id2750out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1951in_b = id1950out_output;

    HWOffsetFix<1,0,UNSIGNED> id1951x_1;

    (id1951x_1) = (and_fixed(id1951in_a,id1951in_b));
    id1951out_result = (id1951x_1);
  }
  { // Node ID: 2751 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2751in_input = id1951out_result;

    id2751out_output[(getCycle()+2)%3] = id2751in_input;
  }
  { // Node ID: 3262 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1953out_result;

  { // Node ID: 1953 (NodeLt)
    const HWFloat<8,12> &id1953in_a = id1940out_result;
    const HWFloat<8,12> &id1953in_b = id3262out_value;

    id1953out_result = (lt_float(id1953in_a,id1953in_b));
  }
  { // Node ID: 2752 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2752in_input = id1953out_result;

    id2752out_output[(getCycle()+6)%7] = id2752in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1954out_output;

  { // Node ID: 1954 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1954in_input = id1947out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1954in_input_doubt = id1947out_o_doubt;

    id1954out_output = id1954in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1955out_result;

  { // Node ID: 1955 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1955in_a = id2752out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1955in_b = id1954out_output;

    HWOffsetFix<1,0,UNSIGNED> id1955x_1;

    (id1955x_1) = (and_fixed(id1955in_a,id1955in_b));
    id1955out_result = (id1955x_1);
  }
  { // Node ID: 2753 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2753in_input = id1955out_result;

    id2753out_output[(getCycle()+2)%3] = id2753in_input;
  }
  { // Node ID: 3258 (NodeConstantRawBits)
  }
  { // Node ID: 3257 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2121out_result;

  { // Node ID: 2121 (NodeAdd)
    const HWFloat<8,12> &id2121in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id2121in_b = id3257out_value;

    id2121out_result = (add_float(id2121in_a,id2121in_b));
  }
  HWFloat<8,12> id2123out_result;

  { // Node ID: 2123 (NodeMul)
    const HWFloat<8,12> &id2123in_a = id3258out_value;
    const HWFloat<8,12> &id2123in_b = id2121out_result;

    id2123out_result = (mul_float(id2123in_a,id2123in_b));
  }
  HWRawBits<8> id2190out_result;

  { // Node ID: 2190 (NodeSlice)
    const HWFloat<8,12> &id2190in_a = id2123out_result;

    id2190out_result = (slice<11,8>(id2190in_a));
  }
  { // Node ID: 2191 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2449out_result;

  { // Node ID: 2449 (NodeEqInlined)
    const HWRawBits<8> &id2449in_a = id2190out_result;
    const HWRawBits<8> &id2449in_b = id2191out_value;

    id2449out_result = (eq_bits(id2449in_a,id2449in_b));
  }
  HWRawBits<11> id2189out_result;

  { // Node ID: 2189 (NodeSlice)
    const HWFloat<8,12> &id2189in_a = id2123out_result;

    id2189out_result = (slice<0,11>(id2189in_a));
  }
  { // Node ID: 3256 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2450out_result;

  { // Node ID: 2450 (NodeNeqInlined)
    const HWRawBits<11> &id2450in_a = id2189out_result;
    const HWRawBits<11> &id2450in_b = id3256out_value;

    id2450out_result = (neq_bits(id2450in_a,id2450in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2195out_result;

  { // Node ID: 2195 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2195in_a = id2449out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2195in_b = id2450out_result;

    HWOffsetFix<1,0,UNSIGNED> id2195x_1;

    (id2195x_1) = (and_fixed(id2195in_a,id2195in_b));
    id2195out_result = (id2195x_1);
  }
  { // Node ID: 2767 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2767in_input = id2195out_result;

    id2767out_output[(getCycle()+8)%9] = id2767in_input;
  }
  { // Node ID: 2124 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2125out_output;
  HWOffsetFix<1,0,UNSIGNED> id2125out_output_doubt;

  { // Node ID: 2125 (NodeDoubtBitOp)
    const HWFloat<8,12> &id2125in_input = id2123out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2125in_doubt = id2124out_value;

    id2125out_output = id2125in_input;
    id2125out_output_doubt = id2125in_doubt;
  }
  { // Node ID: 2126 (NodeCast)
    const HWFloat<8,12> &id2126in_i = id2125out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2126in_i_doubt = id2125out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2126x_1;

    id2126out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2126in_i,(&(id2126x_1))));
    id2126out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2126x_1),(c_hw_fix_4_0_uns_bits))),id2126in_i_doubt));
  }
  { // Node ID: 2129 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id2128out_result;
  HWOffsetFix<1,0,UNSIGNED> id2128out_result_doubt;

  { // Node ID: 2128 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2128in_a = id2126out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2128in_a_doubt = id2126out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id2128in_b = id2129out_value;

    HWOffsetFix<1,0,UNSIGNED> id2128x_1;

    id2128out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id2128in_a,id2128in_b,(&(id2128x_1))));
    id2128out_result_doubt = (or_fixed((neq_fixed((id2128x_1),(c_hw_fix_1_0_uns_bits_1))),id2128in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2130out_o;
  HWOffsetFix<1,0,UNSIGNED> id2130out_o_doubt;

  { // Node ID: 2130 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id2130in_i = id2128out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2130in_i_doubt = id2128out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2130x_1;

    id2130out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2130in_i,(&(id2130x_1))));
    id2130out_o_doubt = (or_fixed((neq_fixed((id2130x_1),(c_hw_fix_1_0_uns_bits_1))),id2130in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2139out_output;

  { // Node ID: 2139 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2139in_input = id2130out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2139in_input_doubt = id2130out_o_doubt;

    id2139out_output = id2139in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2140out_o;

  { // Node ID: 2140 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2140in_i = id2139out_output;

    id2140out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2140in_i));
  }
  { // Node ID: 2758 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2758in_input = id2140out_o;

    id2758out_output[(getCycle()+2)%3] = id2758in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id2141out_o;

  { // Node ID: 2141 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2141in_i = id2139out_output;

    id2141out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id2141in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id2199out_output;

  { // Node ID: 2199 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id2199in_input = id2141out_o;

    id2199out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id2199in_input))));
  }
  { // Node ID: 2200 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id2200in_addr = id2199out_output;

    HWOffsetFix<12,-12,UNSIGNED> id2200x_1;

    switch(((long)((id2200in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id2200x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id2200x_1 = (id2200sta_rom_store[(id2200in_addr.getValueAsLong())]);
        break;
      default:
        id2200x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id2200out_dout[(getCycle()+2)%3] = (id2200x_1);
  }
  { // Node ID: 2145 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id2142out_o;

  { // Node ID: 2142 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2142in_i = id2139out_output;

    id2142out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id2142in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id2144out_result;

  { // Node ID: 2144 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id2144in_a = id2145out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id2144in_b = id2142out_o;

    id2144out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id2144in_a,id2144in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id2146out_o;

  { // Node ID: 2146 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id2146in_i = id2144out_result;

    id2146out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id2146in_i));
  }
  { // Node ID: 2759 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2759in_input = id2146out_o;

    id2759out_output[(getCycle()+2)%3] = id2759in_input;
  }
  { // Node ID: 3254 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2132out_result;

  { // Node ID: 2132 (NodeGt)
    const HWFloat<8,12> &id2132in_a = id2123out_result;
    const HWFloat<8,12> &id2132in_b = id3254out_value;

    id2132out_result = (gt_float(id2132in_a,id2132in_b));
  }
  { // Node ID: 2761 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2761in_input = id2132out_result;

    id2761out_output[(getCycle()+6)%7] = id2761in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2133out_output;

  { // Node ID: 2133 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2133in_input = id2130out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2133in_input_doubt = id2130out_o_doubt;

    id2133out_output = id2133in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2134out_result;

  { // Node ID: 2134 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2134in_a = id2761out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2134in_b = id2133out_output;

    HWOffsetFix<1,0,UNSIGNED> id2134x_1;

    (id2134x_1) = (and_fixed(id2134in_a,id2134in_b));
    id2134out_result = (id2134x_1);
  }
  { // Node ID: 2762 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2762in_input = id2134out_result;

    id2762out_output[(getCycle()+2)%3] = id2762in_input;
  }
  { // Node ID: 3253 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2136out_result;

  { // Node ID: 2136 (NodeLt)
    const HWFloat<8,12> &id2136in_a = id2123out_result;
    const HWFloat<8,12> &id2136in_b = id3253out_value;

    id2136out_result = (lt_float(id2136in_a,id2136in_b));
  }
  { // Node ID: 2763 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2763in_input = id2136out_result;

    id2763out_output[(getCycle()+6)%7] = id2763in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2137out_output;

  { // Node ID: 2137 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2137in_input = id2130out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2137in_input_doubt = id2130out_o_doubt;

    id2137out_output = id2137in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2138out_result;

  { // Node ID: 2138 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2138in_a = id2763out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2138in_b = id2137out_output;

    HWOffsetFix<1,0,UNSIGNED> id2138x_1;

    (id2138x_1) = (and_fixed(id2138in_a,id2138in_b));
    id2138out_result = (id2138x_1);
  }
  { // Node ID: 2764 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2764in_input = id2138out_result;

    id2764out_output[(getCycle()+2)%3] = id2764in_input;
  }
  { // Node ID: 3249 (NodeConstantRawBits)
  }
  { // Node ID: 3248 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2206out_result;

  { // Node ID: 2206 (NodeAdd)
    const HWFloat<8,12> &id2206in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id2206in_b = id3248out_value;

    id2206out_result = (add_float(id2206in_a,id2206in_b));
  }
  HWFloat<8,12> id2208out_result;

  { // Node ID: 2208 (NodeMul)
    const HWFloat<8,12> &id2208in_a = id3249out_value;
    const HWFloat<8,12> &id2208in_b = id2206out_result;

    id2208out_result = (mul_float(id2208in_a,id2208in_b));
  }
  HWRawBits<8> id2275out_result;

  { // Node ID: 2275 (NodeSlice)
    const HWFloat<8,12> &id2275in_a = id2208out_result;

    id2275out_result = (slice<11,8>(id2275in_a));
  }
  { // Node ID: 2276 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2456out_result;

  { // Node ID: 2456 (NodeEqInlined)
    const HWRawBits<8> &id2456in_a = id2275out_result;
    const HWRawBits<8> &id2456in_b = id2276out_value;

    id2456out_result = (eq_bits(id2456in_a,id2456in_b));
  }
  HWRawBits<11> id2274out_result;

  { // Node ID: 2274 (NodeSlice)
    const HWFloat<8,12> &id2274in_a = id2208out_result;

    id2274out_result = (slice<0,11>(id2274in_a));
  }
  { // Node ID: 3247 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2457out_result;

  { // Node ID: 2457 (NodeNeqInlined)
    const HWRawBits<11> &id2457in_a = id2274out_result;
    const HWRawBits<11> &id2457in_b = id3247out_value;

    id2457out_result = (neq_bits(id2457in_a,id2457in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2280out_result;

  { // Node ID: 2280 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2280in_a = id2456out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2280in_b = id2457out_result;

    HWOffsetFix<1,0,UNSIGNED> id2280x_1;

    (id2280x_1) = (and_fixed(id2280in_a,id2280in_b));
    id2280out_result = (id2280x_1);
  }
  { // Node ID: 2778 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2778in_input = id2280out_result;

    id2778out_output[(getCycle()+8)%9] = id2778in_input;
  }
  { // Node ID: 2209 (NodeConstantRawBits)
  }
  HWFloat<8,12> id2210out_output;
  HWOffsetFix<1,0,UNSIGNED> id2210out_output_doubt;

  { // Node ID: 2210 (NodeDoubtBitOp)
    const HWFloat<8,12> &id2210in_input = id2208out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2210in_doubt = id2209out_value;

    id2210out_output = id2210in_input;
    id2210out_output_doubt = id2210in_doubt;
  }
  { // Node ID: 2211 (NodeCast)
    const HWFloat<8,12> &id2211in_i = id2210out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2211in_i_doubt = id2210out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2211x_1;

    id2211out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2211in_i,(&(id2211x_1))));
    id2211out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2211x_1),(c_hw_fix_4_0_uns_bits))),id2211in_i_doubt));
  }
  { // Node ID: 2214 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id2213out_result;
  HWOffsetFix<1,0,UNSIGNED> id2213out_result_doubt;

  { // Node ID: 2213 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2213in_a = id2211out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2213in_a_doubt = id2211out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id2213in_b = id2214out_value;

    HWOffsetFix<1,0,UNSIGNED> id2213x_1;

    id2213out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id2213in_a,id2213in_b,(&(id2213x_1))));
    id2213out_result_doubt = (or_fixed((neq_fixed((id2213x_1),(c_hw_fix_1_0_uns_bits_1))),id2213in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2215out_o;
  HWOffsetFix<1,0,UNSIGNED> id2215out_o_doubt;

  { // Node ID: 2215 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id2215in_i = id2213out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2215in_i_doubt = id2213out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2215x_1;

    id2215out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id2215in_i,(&(id2215x_1))));
    id2215out_o_doubt = (or_fixed((neq_fixed((id2215x_1),(c_hw_fix_1_0_uns_bits_1))),id2215in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2224out_output;

  { // Node ID: 2224 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2224in_input = id2215out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2224in_input_doubt = id2215out_o_doubt;

    id2224out_output = id2224in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2225out_o;

  { // Node ID: 2225 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2225in_i = id2224out_output;

    id2225out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id2225in_i));
  }
  { // Node ID: 2769 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2769in_input = id2225out_o;

    id2769out_output[(getCycle()+2)%3] = id2769in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id2226out_o;

  { // Node ID: 2226 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2226in_i = id2224out_output;

    id2226out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id2226in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id2284out_output;

  { // Node ID: 2284 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id2284in_input = id2226out_o;

    id2284out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id2284in_input))));
  }
  { // Node ID: 2285 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id2285in_addr = id2284out_output;

    HWOffsetFix<12,-12,UNSIGNED> id2285x_1;

    switch(((long)((id2285in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id2285x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id2285x_1 = (id2285sta_rom_store[(id2285in_addr.getValueAsLong())]);
        break;
      default:
        id2285x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id2285out_dout[(getCycle()+2)%3] = (id2285x_1);
  }
  { // Node ID: 2230 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id2227out_o;

  { // Node ID: 2227 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2227in_i = id2224out_output;

    id2227out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id2227in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id2229out_result;

  { // Node ID: 2229 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id2229in_a = id2230out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id2229in_b = id2227out_o;

    id2229out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id2229in_a,id2229in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id2231out_o;

  { // Node ID: 2231 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id2231in_i = id2229out_result;

    id2231out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id2231in_i));
  }
  { // Node ID: 2770 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2770in_input = id2231out_o;

    id2770out_output[(getCycle()+2)%3] = id2770in_input;
  }
  { // Node ID: 3245 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2217out_result;

  { // Node ID: 2217 (NodeGt)
    const HWFloat<8,12> &id2217in_a = id2208out_result;
    const HWFloat<8,12> &id2217in_b = id3245out_value;

    id2217out_result = (gt_float(id2217in_a,id2217in_b));
  }
  { // Node ID: 2772 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2772in_input = id2217out_result;

    id2772out_output[(getCycle()+6)%7] = id2772in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2218out_output;

  { // Node ID: 2218 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2218in_input = id2215out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2218in_input_doubt = id2215out_o_doubt;

    id2218out_output = id2218in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2219out_result;

  { // Node ID: 2219 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2219in_a = id2772out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2219in_b = id2218out_output;

    HWOffsetFix<1,0,UNSIGNED> id2219x_1;

    (id2219x_1) = (and_fixed(id2219in_a,id2219in_b));
    id2219out_result = (id2219x_1);
  }
  { // Node ID: 2773 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2773in_input = id2219out_result;

    id2773out_output[(getCycle()+2)%3] = id2773in_input;
  }
  { // Node ID: 3244 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2221out_result;

  { // Node ID: 2221 (NodeLt)
    const HWFloat<8,12> &id2221in_a = id2208out_result;
    const HWFloat<8,12> &id2221in_b = id3244out_value;

    id2221out_result = (lt_float(id2221in_a,id2221in_b));
  }
  { // Node ID: 2774 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2774in_input = id2221out_result;

    id2774out_output[(getCycle()+6)%7] = id2774in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2222out_output;

  { // Node ID: 2222 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id2222in_input = id2215out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2222in_input_doubt = id2215out_o_doubt;

    id2222out_output = id2222in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2223out_result;

  { // Node ID: 2223 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2223in_a = id2774out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2223in_b = id2222out_output;

    HWOffsetFix<1,0,UNSIGNED> id2223x_1;

    (id2223x_1) = (and_fixed(id2223in_a,id2223in_b));
    id2223out_result = (id2223x_1);
  }
  { // Node ID: 2775 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2775in_input = id2223out_result;

    id2775out_output[(getCycle()+2)%3] = id2775in_input;
  }
  { // Node ID: 3241 (NodeConstantRawBits)
  }
  { // Node ID: 2463 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2463in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2463in_b = id3241out_value;

    id2463out_result[(getCycle()+1)%2] = (eq_fixed(id2463in_a,id2463in_b));
  }
  HWFloat<8,12> id2705out_output;

  { // Node ID: 2705 (NodeStreamOffset)
    const HWFloat<8,12> &id2705in_input = id2779out_output[getCycle()%2];

    id2705out_output = id2705in_input;
  }
  { // Node ID: 24 (NodeInputMappedReg)
  }
  { // Node ID: 25 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id25in_sel = id2463out_result[getCycle()%2];
    const HWFloat<8,12> &id25in_option0 = id2705out_output;
    const HWFloat<8,12> &id25in_option1 = id24out_x1_in;

    HWFloat<8,12> id25x_1;

    switch((id25in_sel.getValueAsLong())) {
      case 0l:
        id25x_1 = id25in_option0;
        break;
      case 1l:
        id25x_1 = id25in_option1;
        break;
      default:
        id25x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id25out_result[(getCycle()+1)%2] = (id25x_1);
  }
  { // Node ID: 2800 (NodeFIFO)
    const HWFloat<8,12> &id2800in_input = id25out_result[getCycle()%2];

    id2800out_output[(getCycle()+8)%9] = id2800in_input;
  }
  { // Node ID: 3240 (NodeConstantRawBits)
  }
  { // Node ID: 3239 (NodeConstantRawBits)
  }
  { // Node ID: 3238 (NodeConstantRawBits)
  }
  HWFloat<8,12> id47out_result;

  { // Node ID: 47 (NodeAdd)
    const HWFloat<8,12> &id47in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id47in_b = id3238out_value;

    id47out_result = (add_float(id47in_a,id47in_b));
  }
  HWFloat<8,12> id49out_result;

  { // Node ID: 49 (NodeMul)
    const HWFloat<8,12> &id49in_a = id3239out_value;
    const HWFloat<8,12> &id49in_b = id47out_result;

    id49out_result = (mul_float(id49in_a,id49in_b));
  }
  HWRawBits<8> id116out_result;

  { // Node ID: 116 (NodeSlice)
    const HWFloat<8,12> &id116in_a = id49out_result;

    id116out_result = (slice<11,8>(id116in_a));
  }
  { // Node ID: 117 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2464out_result;

  { // Node ID: 2464 (NodeEqInlined)
    const HWRawBits<8> &id2464in_a = id116out_result;
    const HWRawBits<8> &id2464in_b = id117out_value;

    id2464out_result = (eq_bits(id2464in_a,id2464in_b));
  }
  HWRawBits<11> id115out_result;

  { // Node ID: 115 (NodeSlice)
    const HWFloat<8,12> &id115in_a = id49out_result;

    id115out_result = (slice<0,11>(id115in_a));
  }
  { // Node ID: 3237 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2465out_result;

  { // Node ID: 2465 (NodeNeqInlined)
    const HWRawBits<11> &id2465in_a = id115out_result;
    const HWRawBits<11> &id2465in_b = id3237out_value;

    id2465out_result = (neq_bits(id2465in_a,id2465in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id121out_result;

  { // Node ID: 121 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id121in_a = id2464out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id121in_b = id2465out_result;

    HWOffsetFix<1,0,UNSIGNED> id121x_1;

    (id121x_1) = (and_fixed(id121in_a,id121in_b));
    id121out_result = (id121x_1);
  }
  { // Node ID: 2789 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2789in_input = id121out_result;

    id2789out_output[(getCycle()+8)%9] = id2789in_input;
  }
  { // Node ID: 50 (NodeConstantRawBits)
  }
  HWFloat<8,12> id51out_output;
  HWOffsetFix<1,0,UNSIGNED> id51out_output_doubt;

  { // Node ID: 51 (NodeDoubtBitOp)
    const HWFloat<8,12> &id51in_input = id49out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id51in_doubt = id50out_value;

    id51out_output = id51in_input;
    id51out_output_doubt = id51in_doubt;
  }
  { // Node ID: 52 (NodeCast)
    const HWFloat<8,12> &id52in_i = id51out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id52in_i_doubt = id51out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id52x_1;

    id52out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id52in_i,(&(id52x_1))));
    id52out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id52x_1),(c_hw_fix_4_0_uns_bits))),id52in_i_doubt));
  }
  { // Node ID: 55 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id54out_result;
  HWOffsetFix<1,0,UNSIGNED> id54out_result_doubt;

  { // Node ID: 54 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id54in_a = id52out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id54in_a_doubt = id52out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id54in_b = id55out_value;

    HWOffsetFix<1,0,UNSIGNED> id54x_1;

    id54out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id54in_a,id54in_b,(&(id54x_1))));
    id54out_result_doubt = (or_fixed((neq_fixed((id54x_1),(c_hw_fix_1_0_uns_bits_1))),id54in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id56out_o;
  HWOffsetFix<1,0,UNSIGNED> id56out_o_doubt;

  { // Node ID: 56 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id56in_i = id54out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id56in_i_doubt = id54out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id56x_1;

    id56out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id56in_i,(&(id56x_1))));
    id56out_o_doubt = (or_fixed((neq_fixed((id56x_1),(c_hw_fix_1_0_uns_bits_1))),id56in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id65out_output;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id65in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id65in_input_doubt = id56out_o_doubt;

    id65out_output = id65in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id66out_o;

  { // Node ID: 66 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id66in_i = id65out_output;

    id66out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id66in_i));
  }
  { // Node ID: 2780 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2780in_input = id66out_o;

    id2780out_output[(getCycle()+2)%3] = id2780in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id83out_o;

  { // Node ID: 83 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id83in_i = id2780out_output[getCycle()%3];

    id83out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id83in_i));
  }
  { // Node ID: 86 (NodeConstantRawBits)
  }
  { // Node ID: 2626 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id67out_o;

  { // Node ID: 67 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id67in_i = id65out_output;

    id67out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id67in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id125out_output;

  { // Node ID: 125 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id125in_input = id67out_o;

    id125out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id125in_input))));
  }
  { // Node ID: 126 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id126in_addr = id125out_output;

    HWOffsetFix<12,-12,UNSIGNED> id126x_1;

    switch(((long)((id126in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id126x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id126x_1 = (id126sta_rom_store[(id126in_addr.getValueAsLong())]);
        break;
      default:
        id126x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id126out_dout[(getCycle()+2)%3] = (id126x_1);
  }
  { // Node ID: 71 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id68out_o;

  { // Node ID: 68 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id68in_i = id65out_output;

    id68out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id68in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id70out_result;

  { // Node ID: 70 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id70in_a = id71out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id70in_b = id68out_o;

    id70out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id70in_a,id70in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id72out_o;

  { // Node ID: 72 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id72in_i = id70out_result;

    id72out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id72in_i));
  }
  { // Node ID: 2781 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2781in_input = id72out_o;

    id2781out_output[(getCycle()+2)%3] = id2781in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id73out_result;

  { // Node ID: 73 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id73in_a = id126out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id73in_b = id2781out_output[getCycle()%3];

    id73out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id73in_a,id73in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id74out_result;

  { // Node ID: 74 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id74in_a = id2781out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id74in_b = id126out_dout[getCycle()%3];

    id74out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id74in_a,id74in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id75out_result;

  { // Node ID: 75 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id75in_a = id73out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id75in_b = id74out_result;

    id75out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id75in_a,id75in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id76out_o;

  { // Node ID: 76 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id76in_i = id75out_result;

    id76out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id76in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id77out_o;

  { // Node ID: 77 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id77in_i = id76out_o;

    id77out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id77in_i));
  }
  { // Node ID: 3236 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2466out_result;

  { // Node ID: 2466 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2466in_a = id77out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2466in_b = id3236out_value;

    id2466out_result = (gte_fixed(id2466in_a,id2466in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2684out_result;

  { // Node ID: 2684 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2684in_a = id83out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2684in_b = id86out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2684in_c = id2626out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2684in_condb = id2466out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2684x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2684x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2684x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2684x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2684x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2684x_1 = id2684in_a;
        break;
      default:
        id2684x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2684in_condb.getValueAsLong())) {
      case 0l:
        id2684x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2684x_2 = id2684in_b;
        break;
      default:
        id2684x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2684x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2684x_3 = id2684in_c;
        break;
      default:
        id2684x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2684x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2684x_1),(id2684x_2))),(id2684x_3)));
    id2684out_result = (id2684x_4);
  }
  HWRawBits<1> id2467out_result;

  { // Node ID: 2467 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2467in_a = id2684out_result;

    id2467out_result = (slice<10,1>(id2467in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2468out_output;

  { // Node ID: 2468 (NodeReinterpret)
    const HWRawBits<1> &id2468in_input = id2467out_result;

    id2468out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2468in_input));
  }
  { // Node ID: 3235 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<8,12> &id58in_a = id49out_result;
    const HWFloat<8,12> &id58in_b = id3235out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 2783 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2783in_input = id58out_result;

    id2783out_output[(getCycle()+6)%7] = id2783in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id59out_output;

  { // Node ID: 59 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id59in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id59in_input_doubt = id56out_o_doubt;

    id59out_output = id59in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id60out_result;

  { // Node ID: 60 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_a = id2783out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id60in_b = id59out_output;

    HWOffsetFix<1,0,UNSIGNED> id60x_1;

    (id60x_1) = (and_fixed(id60in_a,id60in_b));
    id60out_result = (id60x_1);
  }
  { // Node ID: 2784 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2784in_input = id60out_result;

    id2784out_output[(getCycle()+2)%3] = id2784in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id92out_result;

  { // Node ID: 92 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id92in_a = id2784out_output[getCycle()%3];

    id92out_result = (not_fixed(id92in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id93out_result;

  { // Node ID: 93 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id93in_a = id2468out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id93in_b = id92out_result;

    HWOffsetFix<1,0,UNSIGNED> id93x_1;

    (id93x_1) = (and_fixed(id93in_a,id93in_b));
    id93out_result = (id93x_1);
  }
  { // Node ID: 3234 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id62out_result;

  { // Node ID: 62 (NodeLt)
    const HWFloat<8,12> &id62in_a = id49out_result;
    const HWFloat<8,12> &id62in_b = id3234out_value;

    id62out_result = (lt_float(id62in_a,id62in_b));
  }
  { // Node ID: 2785 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2785in_input = id62out_result;

    id2785out_output[(getCycle()+6)%7] = id2785in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id63out_output;

  { // Node ID: 63 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id63in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id63in_input_doubt = id56out_o_doubt;

    id63out_output = id63in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id64out_result;

  { // Node ID: 64 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id64in_a = id2785out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id64in_b = id63out_output;

    HWOffsetFix<1,0,UNSIGNED> id64x_1;

    (id64x_1) = (and_fixed(id64in_a,id64in_b));
    id64out_result = (id64x_1);
  }
  { // Node ID: 2786 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2786in_input = id64out_result;

    id2786out_output[(getCycle()+2)%3] = id2786in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id94out_result;

  { // Node ID: 94 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id94in_a = id93out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id94in_b = id2786out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id94x_1;

    (id94x_1) = (or_fixed(id94in_a,id94in_b));
    id94out_result = (id94x_1);
  }
  { // Node ID: 3233 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2469out_result;

  { // Node ID: 2469 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2469in_a = id2684out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2469in_b = id3233out_value;

    id2469out_result = (gte_fixed(id2469in_a,id2469in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id101out_result;

  { // Node ID: 101 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id101in_a = id2786out_output[getCycle()%3];

    id101out_result = (not_fixed(id101in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id102out_result;

  { // Node ID: 102 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id102in_a = id2469out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id102in_b = id101out_result;

    HWOffsetFix<1,0,UNSIGNED> id102x_1;

    (id102x_1) = (and_fixed(id102in_a,id102in_b));
    id102out_result = (id102x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id103out_result;

  { // Node ID: 103 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id103in_a = id102out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id103in_b = id2784out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id103x_1;

    (id103x_1) = (or_fixed(id103in_a,id103in_b));
    id103out_result = (id103x_1);
  }
  HWRawBits<2> id104out_result;

  { // Node ID: 104 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id104in_in0 = id94out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id104in_in1 = id103out_result;

    id104out_result = (cat(id104in_in0,id104in_in1));
  }
  { // Node ID: 96 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id95out_o;

  { // Node ID: 95 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id95in_i = id2684out_result;

    id95out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id95in_i));
  }
  { // Node ID: 80 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id81out_result;

  { // Node ID: 81 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id81in_sel = id2466out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id81in_option0 = id77out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id81in_option1 = id80out_value;

    HWOffsetFix<12,-11,UNSIGNED> id81x_1;

    switch((id81in_sel.getValueAsLong())) {
      case 0l:
        id81x_1 = id81in_option0;
        break;
      case 1l:
        id81x_1 = id81in_option1;
        break;
      default:
        id81x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id81out_result = (id81x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id82in_i = id81out_result;

    id82out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id82in_i));
  }
  HWRawBits<20> id97out_result;

  { // Node ID: 97 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id97in_in0 = id96out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id97in_in1 = id95out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id97in_in2 = id82out_o;

    id97out_result = (cat((cat(id97in_in0,id97in_in1)),id97in_in2));
  }
  HWFloat<8,12> id98out_output;

  { // Node ID: 98 (NodeReinterpret)
    const HWRawBits<20> &id98in_input = id97out_result;

    id98out_output = (cast_bits2float<8,12>(id98in_input));
  }
  { // Node ID: 105 (NodeConstantRawBits)
  }
  { // Node ID: 106 (NodeConstantRawBits)
  }
  { // Node ID: 108 (NodeConstantRawBits)
  }
  HWRawBits<20> id2470out_result;

  { // Node ID: 2470 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2470in_in0 = id105out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2470in_in1 = id106out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2470in_in2 = id108out_value;

    id2470out_result = (cat((cat(id2470in_in0,id2470in_in1)),id2470in_in2));
  }
  HWFloat<8,12> id110out_output;

  { // Node ID: 110 (NodeReinterpret)
    const HWRawBits<20> &id110in_input = id2470out_result;

    id110out_output = (cast_bits2float<8,12>(id110in_input));
  }
  { // Node ID: 2400 (NodeConstantRawBits)
  }
  HWFloat<8,12> id113out_result;

  { // Node ID: 113 (NodeMux)
    const HWRawBits<2> &id113in_sel = id104out_result;
    const HWFloat<8,12> &id113in_option0 = id98out_output;
    const HWFloat<8,12> &id113in_option1 = id110out_output;
    const HWFloat<8,12> &id113in_option2 = id2400out_value;
    const HWFloat<8,12> &id113in_option3 = id110out_output;

    HWFloat<8,12> id113x_1;

    switch((id113in_sel.getValueAsLong())) {
      case 0l:
        id113x_1 = id113in_option0;
        break;
      case 1l:
        id113x_1 = id113in_option1;
        break;
      case 2l:
        id113x_1 = id113in_option2;
        break;
      case 3l:
        id113x_1 = id113in_option3;
        break;
      default:
        id113x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id113out_result = (id113x_1);
  }
  { // Node ID: 3232 (NodeConstantRawBits)
  }
  HWFloat<8,12> id123out_result;

  { // Node ID: 123 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id123in_sel = id2789out_output[getCycle()%9];
    const HWFloat<8,12> &id123in_option0 = id113out_result;
    const HWFloat<8,12> &id123in_option1 = id3232out_value;

    HWFloat<8,12> id123x_1;

    switch((id123in_sel.getValueAsLong())) {
      case 0l:
        id123x_1 = id123in_option0;
        break;
      case 1l:
        id123x_1 = id123in_option1;
        break;
      default:
        id123x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id123out_result = (id123x_1);
  }
  HWFloat<8,12> id128out_result;

  { // Node ID: 128 (NodeMul)
    const HWFloat<8,12> &id128in_a = id3240out_value;
    const HWFloat<8,12> &id128in_b = id123out_result;

    id128out_result = (mul_float(id128in_a,id128in_b));
  }
  { // Node ID: 3231 (NodeConstantRawBits)
  }
  { // Node ID: 3230 (NodeConstantRawBits)
  }
  HWFloat<8,12> id130out_result;

  { // Node ID: 130 (NodeAdd)
    const HWFloat<8,12> &id130in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id130in_b = id3230out_value;

    id130out_result = (add_float(id130in_a,id130in_b));
  }
  HWFloat<8,12> id132out_result;

  { // Node ID: 132 (NodeMul)
    const HWFloat<8,12> &id132in_a = id3231out_value;
    const HWFloat<8,12> &id132in_b = id130out_result;

    id132out_result = (mul_float(id132in_a,id132in_b));
  }
  HWRawBits<8> id199out_result;

  { // Node ID: 199 (NodeSlice)
    const HWFloat<8,12> &id199in_a = id132out_result;

    id199out_result = (slice<11,8>(id199in_a));
  }
  { // Node ID: 200 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2471out_result;

  { // Node ID: 2471 (NodeEqInlined)
    const HWRawBits<8> &id2471in_a = id199out_result;
    const HWRawBits<8> &id2471in_b = id200out_value;

    id2471out_result = (eq_bits(id2471in_a,id2471in_b));
  }
  HWRawBits<11> id198out_result;

  { // Node ID: 198 (NodeSlice)
    const HWFloat<8,12> &id198in_a = id132out_result;

    id198out_result = (slice<0,11>(id198in_a));
  }
  { // Node ID: 3229 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2472out_result;

  { // Node ID: 2472 (NodeNeqInlined)
    const HWRawBits<11> &id2472in_a = id198out_result;
    const HWRawBits<11> &id2472in_b = id3229out_value;

    id2472out_result = (neq_bits(id2472in_a,id2472in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id204out_result;

  { // Node ID: 204 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id204in_a = id2471out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id204in_b = id2472out_result;

    HWOffsetFix<1,0,UNSIGNED> id204x_1;

    (id204x_1) = (and_fixed(id204in_a,id204in_b));
    id204out_result = (id204x_1);
  }
  { // Node ID: 2799 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2799in_input = id204out_result;

    id2799out_output[(getCycle()+8)%9] = id2799in_input;
  }
  { // Node ID: 133 (NodeConstantRawBits)
  }
  HWFloat<8,12> id134out_output;
  HWOffsetFix<1,0,UNSIGNED> id134out_output_doubt;

  { // Node ID: 134 (NodeDoubtBitOp)
    const HWFloat<8,12> &id134in_input = id132out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id134in_doubt = id133out_value;

    id134out_output = id134in_input;
    id134out_output_doubt = id134in_doubt;
  }
  { // Node ID: 135 (NodeCast)
    const HWFloat<8,12> &id135in_i = id134out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id135in_i_doubt = id134out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id135x_1;

    id135out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id135in_i,(&(id135x_1))));
    id135out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id135x_1),(c_hw_fix_4_0_uns_bits))),id135in_i_doubt));
  }
  { // Node ID: 138 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id137out_result;
  HWOffsetFix<1,0,UNSIGNED> id137out_result_doubt;

  { // Node ID: 137 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id137in_a = id135out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id137in_a_doubt = id135out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id137in_b = id138out_value;

    HWOffsetFix<1,0,UNSIGNED> id137x_1;

    id137out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id137in_a,id137in_b,(&(id137x_1))));
    id137out_result_doubt = (or_fixed((neq_fixed((id137x_1),(c_hw_fix_1_0_uns_bits_1))),id137in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id139out_o;
  HWOffsetFix<1,0,UNSIGNED> id139out_o_doubt;

  { // Node ID: 139 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id139in_i = id137out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id139in_i_doubt = id137out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id139x_1;

    id139out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id139in_i,(&(id139x_1))));
    id139out_o_doubt = (or_fixed((neq_fixed((id139x_1),(c_hw_fix_1_0_uns_bits_1))),id139in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id148out_output;

  { // Node ID: 148 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id148in_input = id139out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id148in_input_doubt = id139out_o_doubt;

    id148out_output = id148in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id149out_o;

  { // Node ID: 149 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id149in_i = id148out_output;

    id149out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id149in_i));
  }
  { // Node ID: 2790 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2790in_input = id149out_o;

    id2790out_output[(getCycle()+2)%3] = id2790in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id166out_o;

  { // Node ID: 166 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id166in_i = id2790out_output[getCycle()%3];

    id166out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id166in_i));
  }
  { // Node ID: 169 (NodeConstantRawBits)
  }
  { // Node ID: 2628 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id150out_o;

  { // Node ID: 150 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id150in_i = id148out_output;

    id150out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id150in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id208out_output;

  { // Node ID: 208 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id208in_input = id150out_o;

    id208out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id208in_input))));
  }
  { // Node ID: 209 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id209in_addr = id208out_output;

    HWOffsetFix<12,-12,UNSIGNED> id209x_1;

    switch(((long)((id209in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id209x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id209x_1 = (id209sta_rom_store[(id209in_addr.getValueAsLong())]);
        break;
      default:
        id209x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id209out_dout[(getCycle()+2)%3] = (id209x_1);
  }
  { // Node ID: 154 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id151out_o;

  { // Node ID: 151 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id151in_i = id148out_output;

    id151out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id151in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id153out_result;

  { // Node ID: 153 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id153in_a = id154out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id153in_b = id151out_o;

    id153out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id153in_a,id153in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id155out_o;

  { // Node ID: 155 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id155in_i = id153out_result;

    id155out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id155in_i));
  }
  { // Node ID: 2791 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2791in_input = id155out_o;

    id2791out_output[(getCycle()+2)%3] = id2791in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id156out_result;

  { // Node ID: 156 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id156in_a = id209out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id156in_b = id2791out_output[getCycle()%3];

    id156out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id156in_a,id156in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id157out_result;

  { // Node ID: 157 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id157in_a = id2791out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id157in_b = id209out_dout[getCycle()%3];

    id157out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id157in_a,id157in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id158out_result;

  { // Node ID: 158 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id158in_a = id156out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id158in_b = id157out_result;

    id158out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id158in_a,id158in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id159out_o;

  { // Node ID: 159 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id159in_i = id158out_result;

    id159out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id159in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id160out_o;

  { // Node ID: 160 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id160in_i = id159out_o;

    id160out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id160in_i));
  }
  { // Node ID: 3228 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2473out_result;

  { // Node ID: 2473 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2473in_a = id160out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2473in_b = id3228out_value;

    id2473out_result = (gte_fixed(id2473in_a,id2473in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2683out_result;

  { // Node ID: 2683 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2683in_a = id166out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2683in_b = id169out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2683in_c = id2628out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2683in_condb = id2473out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2683x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2683x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2683x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2683x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2683x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2683x_1 = id2683in_a;
        break;
      default:
        id2683x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2683in_condb.getValueAsLong())) {
      case 0l:
        id2683x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2683x_2 = id2683in_b;
        break;
      default:
        id2683x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2683x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2683x_3 = id2683in_c;
        break;
      default:
        id2683x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2683x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2683x_1),(id2683x_2))),(id2683x_3)));
    id2683out_result = (id2683x_4);
  }
  HWRawBits<1> id2474out_result;

  { // Node ID: 2474 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2474in_a = id2683out_result;

    id2474out_result = (slice<10,1>(id2474in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2475out_output;

  { // Node ID: 2475 (NodeReinterpret)
    const HWRawBits<1> &id2475in_input = id2474out_result;

    id2475out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2475in_input));
  }
  { // Node ID: 3227 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id141out_result;

  { // Node ID: 141 (NodeGt)
    const HWFloat<8,12> &id141in_a = id132out_result;
    const HWFloat<8,12> &id141in_b = id3227out_value;

    id141out_result = (gt_float(id141in_a,id141in_b));
  }
  { // Node ID: 2793 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2793in_input = id141out_result;

    id2793out_output[(getCycle()+6)%7] = id2793in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id142out_output;

  { // Node ID: 142 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id142in_input = id139out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id142in_input_doubt = id139out_o_doubt;

    id142out_output = id142in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id143out_result;

  { // Node ID: 143 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id143in_a = id2793out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id143in_b = id142out_output;

    HWOffsetFix<1,0,UNSIGNED> id143x_1;

    (id143x_1) = (and_fixed(id143in_a,id143in_b));
    id143out_result = (id143x_1);
  }
  { // Node ID: 2794 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2794in_input = id143out_result;

    id2794out_output[(getCycle()+2)%3] = id2794in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id175out_result;

  { // Node ID: 175 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id175in_a = id2794out_output[getCycle()%3];

    id175out_result = (not_fixed(id175in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id176out_result;

  { // Node ID: 176 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id176in_a = id2475out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id176in_b = id175out_result;

    HWOffsetFix<1,0,UNSIGNED> id176x_1;

    (id176x_1) = (and_fixed(id176in_a,id176in_b));
    id176out_result = (id176x_1);
  }
  { // Node ID: 3226 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id145out_result;

  { // Node ID: 145 (NodeLt)
    const HWFloat<8,12> &id145in_a = id132out_result;
    const HWFloat<8,12> &id145in_b = id3226out_value;

    id145out_result = (lt_float(id145in_a,id145in_b));
  }
  { // Node ID: 2795 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2795in_input = id145out_result;

    id2795out_output[(getCycle()+6)%7] = id2795in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id146out_output;

  { // Node ID: 146 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id146in_input = id139out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id146in_input_doubt = id139out_o_doubt;

    id146out_output = id146in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id147out_result;

  { // Node ID: 147 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id147in_a = id2795out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id147in_b = id146out_output;

    HWOffsetFix<1,0,UNSIGNED> id147x_1;

    (id147x_1) = (and_fixed(id147in_a,id147in_b));
    id147out_result = (id147x_1);
  }
  { // Node ID: 2796 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2796in_input = id147out_result;

    id2796out_output[(getCycle()+2)%3] = id2796in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id177out_result;

  { // Node ID: 177 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id177in_a = id176out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id177in_b = id2796out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id177x_1;

    (id177x_1) = (or_fixed(id177in_a,id177in_b));
    id177out_result = (id177x_1);
  }
  { // Node ID: 3225 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2476out_result;

  { // Node ID: 2476 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2476in_a = id2683out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2476in_b = id3225out_value;

    id2476out_result = (gte_fixed(id2476in_a,id2476in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id184out_result;

  { // Node ID: 184 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id184in_a = id2796out_output[getCycle()%3];

    id184out_result = (not_fixed(id184in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id185out_result;

  { // Node ID: 185 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id185in_a = id2476out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id185in_b = id184out_result;

    HWOffsetFix<1,0,UNSIGNED> id185x_1;

    (id185x_1) = (and_fixed(id185in_a,id185in_b));
    id185out_result = (id185x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id186out_result;

  { // Node ID: 186 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id186in_a = id185out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id186in_b = id2794out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id186x_1;

    (id186x_1) = (or_fixed(id186in_a,id186in_b));
    id186out_result = (id186x_1);
  }
  HWRawBits<2> id187out_result;

  { // Node ID: 187 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id187in_in0 = id177out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id187in_in1 = id186out_result;

    id187out_result = (cat(id187in_in0,id187in_in1));
  }
  { // Node ID: 179 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id178out_o;

  { // Node ID: 178 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id178in_i = id2683out_result;

    id178out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id178in_i));
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id164out_result;

  { // Node ID: 164 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id164in_sel = id2473out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id164in_option0 = id160out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id164in_option1 = id163out_value;

    HWOffsetFix<12,-11,UNSIGNED> id164x_1;

    switch((id164in_sel.getValueAsLong())) {
      case 0l:
        id164x_1 = id164in_option0;
        break;
      case 1l:
        id164x_1 = id164in_option1;
        break;
      default:
        id164x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id164out_result = (id164x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id165out_o;

  { // Node ID: 165 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id165in_i = id164out_result;

    id165out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id165in_i));
  }
  HWRawBits<20> id180out_result;

  { // Node ID: 180 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id180in_in0 = id179out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id180in_in1 = id178out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id180in_in2 = id165out_o;

    id180out_result = (cat((cat(id180in_in0,id180in_in1)),id180in_in2));
  }
  HWFloat<8,12> id181out_output;

  { // Node ID: 181 (NodeReinterpret)
    const HWRawBits<20> &id181in_input = id180out_result;

    id181out_output = (cast_bits2float<8,12>(id181in_input));
  }
  { // Node ID: 188 (NodeConstantRawBits)
  }
  { // Node ID: 189 (NodeConstantRawBits)
  }
  { // Node ID: 191 (NodeConstantRawBits)
  }
  HWRawBits<20> id2477out_result;

  { // Node ID: 2477 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2477in_in0 = id188out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2477in_in1 = id189out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2477in_in2 = id191out_value;

    id2477out_result = (cat((cat(id2477in_in0,id2477in_in1)),id2477in_in2));
  }
  HWFloat<8,12> id193out_output;

  { // Node ID: 193 (NodeReinterpret)
    const HWRawBits<20> &id193in_input = id2477out_result;

    id193out_output = (cast_bits2float<8,12>(id193in_input));
  }
  { // Node ID: 2401 (NodeConstantRawBits)
  }
  HWFloat<8,12> id196out_result;

  { // Node ID: 196 (NodeMux)
    const HWRawBits<2> &id196in_sel = id187out_result;
    const HWFloat<8,12> &id196in_option0 = id181out_output;
    const HWFloat<8,12> &id196in_option1 = id193out_output;
    const HWFloat<8,12> &id196in_option2 = id2401out_value;
    const HWFloat<8,12> &id196in_option3 = id193out_output;

    HWFloat<8,12> id196x_1;

    switch((id196in_sel.getValueAsLong())) {
      case 0l:
        id196x_1 = id196in_option0;
        break;
      case 1l:
        id196x_1 = id196in_option1;
        break;
      case 2l:
        id196x_1 = id196in_option2;
        break;
      case 3l:
        id196x_1 = id196in_option3;
        break;
      default:
        id196x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id196out_result = (id196x_1);
  }
  { // Node ID: 3224 (NodeConstantRawBits)
  }
  HWFloat<8,12> id206out_result;

  { // Node ID: 206 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id206in_sel = id2799out_output[getCycle()%9];
    const HWFloat<8,12> &id206in_option0 = id196out_result;
    const HWFloat<8,12> &id206in_option1 = id3224out_value;

    HWFloat<8,12> id206x_1;

    switch((id206in_sel.getValueAsLong())) {
      case 0l:
        id206x_1 = id206in_option0;
        break;
      case 1l:
        id206x_1 = id206in_option1;
        break;
      default:
        id206x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id206out_result = (id206x_1);
  }
  { // Node ID: 3223 (NodeConstantRawBits)
  }
  HWFloat<8,12> id211out_result;

  { // Node ID: 211 (NodeAdd)
    const HWFloat<8,12> &id211in_a = id206out_result;
    const HWFloat<8,12> &id211in_b = id3223out_value;

    id211out_result = (add_float(id211in_a,id211in_b));
  }
  HWFloat<8,12> id212out_result;

  { // Node ID: 212 (NodeDiv)
    const HWFloat<8,12> &id212in_a = id128out_result;
    const HWFloat<8,12> &id212in_b = id211out_result;

    id212out_result = (div_float(id212in_a,id212in_b));
  }
  HWFloat<8,12> id213out_result;

  { // Node ID: 213 (NodeMul)
    const HWFloat<8,12> &id213in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id213in_b = id212out_result;

    id213out_result = (mul_float(id213in_a,id213in_b));
  }
  { // Node ID: 3222 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1651out_result;

  { // Node ID: 1651 (NodeSub)
    const HWFloat<8,12> &id1651in_a = id3222out_value;
    const HWFloat<8,12> &id1651in_b = id2800out_output[getCycle()%9];

    id1651out_result = (sub_float(id1651in_a,id1651in_b));
  }
  HWFloat<8,12> id1652out_result;

  { // Node ID: 1652 (NodeMul)
    const HWFloat<8,12> &id1652in_a = id213out_result;
    const HWFloat<8,12> &id1652in_b = id1651out_result;

    id1652out_result = (mul_float(id1652in_a,id1652in_b));
  }
  { // Node ID: 3221 (NodeConstantRawBits)
  }
  { // Node ID: 3220 (NodeConstantRawBits)
  }
  { // Node ID: 3219 (NodeConstantRawBits)
  }
  HWFloat<8,12> id215out_result;

  { // Node ID: 215 (NodeAdd)
    const HWFloat<8,12> &id215in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id215in_b = id3219out_value;

    id215out_result = (add_float(id215in_a,id215in_b));
  }
  HWFloat<8,12> id217out_result;

  { // Node ID: 217 (NodeMul)
    const HWFloat<8,12> &id217in_a = id3220out_value;
    const HWFloat<8,12> &id217in_b = id215out_result;

    id217out_result = (mul_float(id217in_a,id217in_b));
  }
  HWRawBits<8> id284out_result;

  { // Node ID: 284 (NodeSlice)
    const HWFloat<8,12> &id284in_a = id217out_result;

    id284out_result = (slice<11,8>(id284in_a));
  }
  { // Node ID: 285 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2478out_result;

  { // Node ID: 2478 (NodeEqInlined)
    const HWRawBits<8> &id2478in_a = id284out_result;
    const HWRawBits<8> &id2478in_b = id285out_value;

    id2478out_result = (eq_bits(id2478in_a,id2478in_b));
  }
  HWRawBits<11> id283out_result;

  { // Node ID: 283 (NodeSlice)
    const HWFloat<8,12> &id283in_a = id217out_result;

    id283out_result = (slice<0,11>(id283in_a));
  }
  { // Node ID: 3218 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2479out_result;

  { // Node ID: 2479 (NodeNeqInlined)
    const HWRawBits<11> &id2479in_a = id283out_result;
    const HWRawBits<11> &id2479in_b = id3218out_value;

    id2479out_result = (neq_bits(id2479in_a,id2479in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id289out_result;

  { // Node ID: 289 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id289in_a = id2478out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id289in_b = id2479out_result;

    HWOffsetFix<1,0,UNSIGNED> id289x_1;

    (id289x_1) = (and_fixed(id289in_a,id289in_b));
    id289out_result = (id289x_1);
  }
  { // Node ID: 2810 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2810in_input = id289out_result;

    id2810out_output[(getCycle()+8)%9] = id2810in_input;
  }
  { // Node ID: 218 (NodeConstantRawBits)
  }
  HWFloat<8,12> id219out_output;
  HWOffsetFix<1,0,UNSIGNED> id219out_output_doubt;

  { // Node ID: 219 (NodeDoubtBitOp)
    const HWFloat<8,12> &id219in_input = id217out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id219in_doubt = id218out_value;

    id219out_output = id219in_input;
    id219out_output_doubt = id219in_doubt;
  }
  { // Node ID: 220 (NodeCast)
    const HWFloat<8,12> &id220in_i = id219out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id220in_i_doubt = id219out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id220x_1;

    id220out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id220in_i,(&(id220x_1))));
    id220out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id220x_1),(c_hw_fix_4_0_uns_bits))),id220in_i_doubt));
  }
  { // Node ID: 223 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id222out_result;
  HWOffsetFix<1,0,UNSIGNED> id222out_result_doubt;

  { // Node ID: 222 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id222in_a = id220out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id222in_a_doubt = id220out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id222in_b = id223out_value;

    HWOffsetFix<1,0,UNSIGNED> id222x_1;

    id222out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id222in_a,id222in_b,(&(id222x_1))));
    id222out_result_doubt = (or_fixed((neq_fixed((id222x_1),(c_hw_fix_1_0_uns_bits_1))),id222in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id224out_o;
  HWOffsetFix<1,0,UNSIGNED> id224out_o_doubt;

  { // Node ID: 224 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id224in_i = id222out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id224in_i_doubt = id222out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id224x_1;

    id224out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id224in_i,(&(id224x_1))));
    id224out_o_doubt = (or_fixed((neq_fixed((id224x_1),(c_hw_fix_1_0_uns_bits_1))),id224in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id233out_output;

  { // Node ID: 233 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id233in_input = id224out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_input_doubt = id224out_o_doubt;

    id233out_output = id233in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id234out_o;

  { // Node ID: 234 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id234in_i = id233out_output;

    id234out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id234in_i));
  }
  { // Node ID: 2801 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2801in_input = id234out_o;

    id2801out_output[(getCycle()+2)%3] = id2801in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id251out_o;

  { // Node ID: 251 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id251in_i = id2801out_output[getCycle()%3];

    id251out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id251in_i));
  }
  { // Node ID: 254 (NodeConstantRawBits)
  }
  { // Node ID: 2630 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id235out_o;

  { // Node ID: 235 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id235in_i = id233out_output;

    id235out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id235in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id293out_output;

  { // Node ID: 293 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id293in_input = id235out_o;

    id293out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id293in_input))));
  }
  { // Node ID: 294 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id294in_addr = id293out_output;

    HWOffsetFix<12,-12,UNSIGNED> id294x_1;

    switch(((long)((id294in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id294x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id294x_1 = (id294sta_rom_store[(id294in_addr.getValueAsLong())]);
        break;
      default:
        id294x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id294out_dout[(getCycle()+2)%3] = (id294x_1);
  }
  { // Node ID: 239 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id236out_o;

  { // Node ID: 236 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id236in_i = id233out_output;

    id236out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id236in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id238out_result;

  { // Node ID: 238 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id238in_a = id239out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id238in_b = id236out_o;

    id238out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id238in_a,id238in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id240out_o;

  { // Node ID: 240 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id240in_i = id238out_result;

    id240out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id240in_i));
  }
  { // Node ID: 2802 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2802in_input = id240out_o;

    id2802out_output[(getCycle()+2)%3] = id2802in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id241out_result;

  { // Node ID: 241 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id241in_a = id294out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id241in_b = id2802out_output[getCycle()%3];

    id241out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id241in_a,id241in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id242out_result;

  { // Node ID: 242 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id242in_a = id2802out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id242in_b = id294out_dout[getCycle()%3];

    id242out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id242in_a,id242in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id243out_result;

  { // Node ID: 243 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id243in_a = id241out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id243in_b = id242out_result;

    id243out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id243in_a,id243in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id244out_o;

  { // Node ID: 244 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id244in_i = id243out_result;

    id244out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id244in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id245out_o;

  { // Node ID: 245 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id245in_i = id244out_o;

    id245out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id245in_i));
  }
  { // Node ID: 3217 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2480out_result;

  { // Node ID: 2480 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2480in_a = id245out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2480in_b = id3217out_value;

    id2480out_result = (gte_fixed(id2480in_a,id2480in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2682out_result;

  { // Node ID: 2682 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2682in_a = id251out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2682in_b = id254out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2682in_c = id2630out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2682in_condb = id2480out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2682x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2682x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2682x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2682x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2682x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2682x_1 = id2682in_a;
        break;
      default:
        id2682x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2682in_condb.getValueAsLong())) {
      case 0l:
        id2682x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2682x_2 = id2682in_b;
        break;
      default:
        id2682x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2682x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2682x_3 = id2682in_c;
        break;
      default:
        id2682x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2682x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2682x_1),(id2682x_2))),(id2682x_3)));
    id2682out_result = (id2682x_4);
  }
  HWRawBits<1> id2481out_result;

  { // Node ID: 2481 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2481in_a = id2682out_result;

    id2481out_result = (slice<10,1>(id2481in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2482out_output;

  { // Node ID: 2482 (NodeReinterpret)
    const HWRawBits<1> &id2482in_input = id2481out_result;

    id2482out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2482in_input));
  }
  { // Node ID: 3216 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id226out_result;

  { // Node ID: 226 (NodeGt)
    const HWFloat<8,12> &id226in_a = id217out_result;
    const HWFloat<8,12> &id226in_b = id3216out_value;

    id226out_result = (gt_float(id226in_a,id226in_b));
  }
  { // Node ID: 2804 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2804in_input = id226out_result;

    id2804out_output[(getCycle()+6)%7] = id2804in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id227out_output;

  { // Node ID: 227 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id227in_input = id224out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id227in_input_doubt = id224out_o_doubt;

    id227out_output = id227in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id228out_result;

  { // Node ID: 228 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id228in_a = id2804out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id228in_b = id227out_output;

    HWOffsetFix<1,0,UNSIGNED> id228x_1;

    (id228x_1) = (and_fixed(id228in_a,id228in_b));
    id228out_result = (id228x_1);
  }
  { // Node ID: 2805 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2805in_input = id228out_result;

    id2805out_output[(getCycle()+2)%3] = id2805in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id260out_result;

  { // Node ID: 260 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id260in_a = id2805out_output[getCycle()%3];

    id260out_result = (not_fixed(id260in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id261out_result;

  { // Node ID: 261 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id261in_a = id2482out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id261in_b = id260out_result;

    HWOffsetFix<1,0,UNSIGNED> id261x_1;

    (id261x_1) = (and_fixed(id261in_a,id261in_b));
    id261out_result = (id261x_1);
  }
  { // Node ID: 3215 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id230out_result;

  { // Node ID: 230 (NodeLt)
    const HWFloat<8,12> &id230in_a = id217out_result;
    const HWFloat<8,12> &id230in_b = id3215out_value;

    id230out_result = (lt_float(id230in_a,id230in_b));
  }
  { // Node ID: 2806 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2806in_input = id230out_result;

    id2806out_output[(getCycle()+6)%7] = id2806in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id231out_output;

  { // Node ID: 231 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id231in_input = id224out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id231in_input_doubt = id224out_o_doubt;

    id231out_output = id231in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id232out_result;

  { // Node ID: 232 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id232in_a = id2806out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id232in_b = id231out_output;

    HWOffsetFix<1,0,UNSIGNED> id232x_1;

    (id232x_1) = (and_fixed(id232in_a,id232in_b));
    id232out_result = (id232x_1);
  }
  { // Node ID: 2807 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2807in_input = id232out_result;

    id2807out_output[(getCycle()+2)%3] = id2807in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id262out_result;

  { // Node ID: 262 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id262in_a = id261out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id262in_b = id2807out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id262x_1;

    (id262x_1) = (or_fixed(id262in_a,id262in_b));
    id262out_result = (id262x_1);
  }
  { // Node ID: 3214 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2483out_result;

  { // Node ID: 2483 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2483in_a = id2682out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2483in_b = id3214out_value;

    id2483out_result = (gte_fixed(id2483in_a,id2483in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id269out_result;

  { // Node ID: 269 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id269in_a = id2807out_output[getCycle()%3];

    id269out_result = (not_fixed(id269in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id270out_result;

  { // Node ID: 270 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id270in_a = id2483out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id270in_b = id269out_result;

    HWOffsetFix<1,0,UNSIGNED> id270x_1;

    (id270x_1) = (and_fixed(id270in_a,id270in_b));
    id270out_result = (id270x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id271out_result;

  { // Node ID: 271 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id271in_a = id270out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id271in_b = id2805out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id271x_1;

    (id271x_1) = (or_fixed(id271in_a,id271in_b));
    id271out_result = (id271x_1);
  }
  HWRawBits<2> id272out_result;

  { // Node ID: 272 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id272in_in0 = id262out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id272in_in1 = id271out_result;

    id272out_result = (cat(id272in_in0,id272in_in1));
  }
  { // Node ID: 264 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id263out_o;

  { // Node ID: 263 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id263in_i = id2682out_result;

    id263out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id263in_i));
  }
  { // Node ID: 248 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id249out_result;

  { // Node ID: 249 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id249in_sel = id2480out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id249in_option0 = id245out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id249in_option1 = id248out_value;

    HWOffsetFix<12,-11,UNSIGNED> id249x_1;

    switch((id249in_sel.getValueAsLong())) {
      case 0l:
        id249x_1 = id249in_option0;
        break;
      case 1l:
        id249x_1 = id249in_option1;
        break;
      default:
        id249x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id249out_result = (id249x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id250out_o;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id250in_i = id249out_result;

    id250out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id250in_i));
  }
  HWRawBits<20> id265out_result;

  { // Node ID: 265 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id265in_in0 = id264out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id265in_in1 = id263out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id265in_in2 = id250out_o;

    id265out_result = (cat((cat(id265in_in0,id265in_in1)),id265in_in2));
  }
  HWFloat<8,12> id266out_output;

  { // Node ID: 266 (NodeReinterpret)
    const HWRawBits<20> &id266in_input = id265out_result;

    id266out_output = (cast_bits2float<8,12>(id266in_input));
  }
  { // Node ID: 273 (NodeConstantRawBits)
  }
  { // Node ID: 274 (NodeConstantRawBits)
  }
  { // Node ID: 276 (NodeConstantRawBits)
  }
  HWRawBits<20> id2484out_result;

  { // Node ID: 2484 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2484in_in0 = id273out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2484in_in1 = id274out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2484in_in2 = id276out_value;

    id2484out_result = (cat((cat(id2484in_in0,id2484in_in1)),id2484in_in2));
  }
  HWFloat<8,12> id278out_output;

  { // Node ID: 278 (NodeReinterpret)
    const HWRawBits<20> &id278in_input = id2484out_result;

    id278out_output = (cast_bits2float<8,12>(id278in_input));
  }
  { // Node ID: 2402 (NodeConstantRawBits)
  }
  HWFloat<8,12> id281out_result;

  { // Node ID: 281 (NodeMux)
    const HWRawBits<2> &id281in_sel = id272out_result;
    const HWFloat<8,12> &id281in_option0 = id266out_output;
    const HWFloat<8,12> &id281in_option1 = id278out_output;
    const HWFloat<8,12> &id281in_option2 = id2402out_value;
    const HWFloat<8,12> &id281in_option3 = id278out_output;

    HWFloat<8,12> id281x_1;

    switch((id281in_sel.getValueAsLong())) {
      case 0l:
        id281x_1 = id281in_option0;
        break;
      case 1l:
        id281x_1 = id281in_option1;
        break;
      case 2l:
        id281x_1 = id281in_option2;
        break;
      case 3l:
        id281x_1 = id281in_option3;
        break;
      default:
        id281x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id281out_result = (id281x_1);
  }
  { // Node ID: 3213 (NodeConstantRawBits)
  }
  HWFloat<8,12> id291out_result;

  { // Node ID: 291 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id291in_sel = id2810out_output[getCycle()%9];
    const HWFloat<8,12> &id291in_option0 = id281out_result;
    const HWFloat<8,12> &id291in_option1 = id3213out_value;

    HWFloat<8,12> id291x_1;

    switch((id291in_sel.getValueAsLong())) {
      case 0l:
        id291x_1 = id291in_option0;
        break;
      case 1l:
        id291x_1 = id291in_option1;
        break;
      default:
        id291x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id291out_result = (id291x_1);
  }
  HWFloat<8,12> id296out_result;

  { // Node ID: 296 (NodeMul)
    const HWFloat<8,12> &id296in_a = id3221out_value;
    const HWFloat<8,12> &id296in_b = id291out_result;

    id296out_result = (mul_float(id296in_a,id296in_b));
  }
  { // Node ID: 3212 (NodeConstantRawBits)
  }
  { // Node ID: 3211 (NodeConstantRawBits)
  }
  HWFloat<8,12> id298out_result;

  { // Node ID: 298 (NodeAdd)
    const HWFloat<8,12> &id298in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id298in_b = id3211out_value;

    id298out_result = (add_float(id298in_a,id298in_b));
  }
  HWFloat<8,12> id300out_result;

  { // Node ID: 300 (NodeMul)
    const HWFloat<8,12> &id300in_a = id3212out_value;
    const HWFloat<8,12> &id300in_b = id298out_result;

    id300out_result = (mul_float(id300in_a,id300in_b));
  }
  HWRawBits<8> id367out_result;

  { // Node ID: 367 (NodeSlice)
    const HWFloat<8,12> &id367in_a = id300out_result;

    id367out_result = (slice<11,8>(id367in_a));
  }
  { // Node ID: 368 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2485out_result;

  { // Node ID: 2485 (NodeEqInlined)
    const HWRawBits<8> &id2485in_a = id367out_result;
    const HWRawBits<8> &id2485in_b = id368out_value;

    id2485out_result = (eq_bits(id2485in_a,id2485in_b));
  }
  HWRawBits<11> id366out_result;

  { // Node ID: 366 (NodeSlice)
    const HWFloat<8,12> &id366in_a = id300out_result;

    id366out_result = (slice<0,11>(id366in_a));
  }
  { // Node ID: 3210 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2486out_result;

  { // Node ID: 2486 (NodeNeqInlined)
    const HWRawBits<11> &id2486in_a = id366out_result;
    const HWRawBits<11> &id2486in_b = id3210out_value;

    id2486out_result = (neq_bits(id2486in_a,id2486in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id372out_result;

  { // Node ID: 372 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id372in_a = id2485out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id372in_b = id2486out_result;

    HWOffsetFix<1,0,UNSIGNED> id372x_1;

    (id372x_1) = (and_fixed(id372in_a,id372in_b));
    id372out_result = (id372x_1);
  }
  { // Node ID: 2820 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2820in_input = id372out_result;

    id2820out_output[(getCycle()+8)%9] = id2820in_input;
  }
  { // Node ID: 301 (NodeConstantRawBits)
  }
  HWFloat<8,12> id302out_output;
  HWOffsetFix<1,0,UNSIGNED> id302out_output_doubt;

  { // Node ID: 302 (NodeDoubtBitOp)
    const HWFloat<8,12> &id302in_input = id300out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id302in_doubt = id301out_value;

    id302out_output = id302in_input;
    id302out_output_doubt = id302in_doubt;
  }
  { // Node ID: 303 (NodeCast)
    const HWFloat<8,12> &id303in_i = id302out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id303in_i_doubt = id302out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id303x_1;

    id303out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id303in_i,(&(id303x_1))));
    id303out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id303x_1),(c_hw_fix_4_0_uns_bits))),id303in_i_doubt));
  }
  { // Node ID: 306 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id305out_result;
  HWOffsetFix<1,0,UNSIGNED> id305out_result_doubt;

  { // Node ID: 305 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id305in_a = id303out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id305in_a_doubt = id303out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id305in_b = id306out_value;

    HWOffsetFix<1,0,UNSIGNED> id305x_1;

    id305out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id305in_a,id305in_b,(&(id305x_1))));
    id305out_result_doubt = (or_fixed((neq_fixed((id305x_1),(c_hw_fix_1_0_uns_bits_1))),id305in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id307out_o;
  HWOffsetFix<1,0,UNSIGNED> id307out_o_doubt;

  { // Node ID: 307 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id307in_i = id305out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id307in_i_doubt = id305out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id307x_1;

    id307out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id307in_i,(&(id307x_1))));
    id307out_o_doubt = (or_fixed((neq_fixed((id307x_1),(c_hw_fix_1_0_uns_bits_1))),id307in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id316out_output;

  { // Node ID: 316 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id316in_input = id307out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id316in_input_doubt = id307out_o_doubt;

    id316out_output = id316in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id317out_o;

  { // Node ID: 317 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id317in_i = id316out_output;

    id317out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id317in_i));
  }
  { // Node ID: 2811 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2811in_input = id317out_o;

    id2811out_output[(getCycle()+2)%3] = id2811in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id334out_o;

  { // Node ID: 334 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id334in_i = id2811out_output[getCycle()%3];

    id334out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id334in_i));
  }
  { // Node ID: 337 (NodeConstantRawBits)
  }
  { // Node ID: 2632 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id318out_o;

  { // Node ID: 318 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id318in_i = id316out_output;

    id318out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id318in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id376out_output;

  { // Node ID: 376 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id376in_input = id318out_o;

    id376out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id376in_input))));
  }
  { // Node ID: 377 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id377in_addr = id376out_output;

    HWOffsetFix<12,-12,UNSIGNED> id377x_1;

    switch(((long)((id377in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id377x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id377x_1 = (id377sta_rom_store[(id377in_addr.getValueAsLong())]);
        break;
      default:
        id377x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id377out_dout[(getCycle()+2)%3] = (id377x_1);
  }
  { // Node ID: 322 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id319out_o;

  { // Node ID: 319 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id319in_i = id316out_output;

    id319out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id319in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id321out_result;

  { // Node ID: 321 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id321in_a = id322out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id321in_b = id319out_o;

    id321out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id321in_a,id321in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id323out_o;

  { // Node ID: 323 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id323in_i = id321out_result;

    id323out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id323in_i));
  }
  { // Node ID: 2812 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2812in_input = id323out_o;

    id2812out_output[(getCycle()+2)%3] = id2812in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id324out_result;

  { // Node ID: 324 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id324in_a = id377out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id324in_b = id2812out_output[getCycle()%3];

    id324out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id324in_a,id324in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id325out_result;

  { // Node ID: 325 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id325in_a = id2812out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id325in_b = id377out_dout[getCycle()%3];

    id325out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id325in_a,id325in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id326out_result;

  { // Node ID: 326 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id326in_a = id324out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id326in_b = id325out_result;

    id326out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id326in_a,id326in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id327out_o;

  { // Node ID: 327 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id327in_i = id326out_result;

    id327out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id327in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id328out_o;

  { // Node ID: 328 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id328in_i = id327out_o;

    id328out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id328in_i));
  }
  { // Node ID: 3209 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2487out_result;

  { // Node ID: 2487 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2487in_a = id328out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2487in_b = id3209out_value;

    id2487out_result = (gte_fixed(id2487in_a,id2487in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2681out_result;

  { // Node ID: 2681 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2681in_a = id334out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2681in_b = id337out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2681in_c = id2632out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2681in_condb = id2487out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2681x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2681x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2681x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2681x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2681x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2681x_1 = id2681in_a;
        break;
      default:
        id2681x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2681in_condb.getValueAsLong())) {
      case 0l:
        id2681x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2681x_2 = id2681in_b;
        break;
      default:
        id2681x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2681x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2681x_3 = id2681in_c;
        break;
      default:
        id2681x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2681x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2681x_1),(id2681x_2))),(id2681x_3)));
    id2681out_result = (id2681x_4);
  }
  HWRawBits<1> id2488out_result;

  { // Node ID: 2488 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2488in_a = id2681out_result;

    id2488out_result = (slice<10,1>(id2488in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2489out_output;

  { // Node ID: 2489 (NodeReinterpret)
    const HWRawBits<1> &id2489in_input = id2488out_result;

    id2489out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2489in_input));
  }
  { // Node ID: 3208 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id309out_result;

  { // Node ID: 309 (NodeGt)
    const HWFloat<8,12> &id309in_a = id300out_result;
    const HWFloat<8,12> &id309in_b = id3208out_value;

    id309out_result = (gt_float(id309in_a,id309in_b));
  }
  { // Node ID: 2814 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2814in_input = id309out_result;

    id2814out_output[(getCycle()+6)%7] = id2814in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id310out_output;

  { // Node ID: 310 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id310in_input = id307out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id310in_input_doubt = id307out_o_doubt;

    id310out_output = id310in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id311out_result;

  { // Node ID: 311 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id311in_a = id2814out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id311in_b = id310out_output;

    HWOffsetFix<1,0,UNSIGNED> id311x_1;

    (id311x_1) = (and_fixed(id311in_a,id311in_b));
    id311out_result = (id311x_1);
  }
  { // Node ID: 2815 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2815in_input = id311out_result;

    id2815out_output[(getCycle()+2)%3] = id2815in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id343out_result;

  { // Node ID: 343 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id343in_a = id2815out_output[getCycle()%3];

    id343out_result = (not_fixed(id343in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_a = id2489out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id344in_b = id343out_result;

    HWOffsetFix<1,0,UNSIGNED> id344x_1;

    (id344x_1) = (and_fixed(id344in_a,id344in_b));
    id344out_result = (id344x_1);
  }
  { // Node ID: 3207 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeLt)
    const HWFloat<8,12> &id313in_a = id300out_result;
    const HWFloat<8,12> &id313in_b = id3207out_value;

    id313out_result = (lt_float(id313in_a,id313in_b));
  }
  { // Node ID: 2816 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2816in_input = id313out_result;

    id2816out_output[(getCycle()+6)%7] = id2816in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id314out_output;

  { // Node ID: 314 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id314in_input = id307out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id314in_input_doubt = id307out_o_doubt;

    id314out_output = id314in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id315out_result;

  { // Node ID: 315 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id315in_a = id2816out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id315in_b = id314out_output;

    HWOffsetFix<1,0,UNSIGNED> id315x_1;

    (id315x_1) = (and_fixed(id315in_a,id315in_b));
    id315out_result = (id315x_1);
  }
  { // Node ID: 2817 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2817in_input = id315out_result;

    id2817out_output[(getCycle()+2)%3] = id2817in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id345out_result;

  { // Node ID: 345 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id345in_a = id344out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id345in_b = id2817out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id345x_1;

    (id345x_1) = (or_fixed(id345in_a,id345in_b));
    id345out_result = (id345x_1);
  }
  { // Node ID: 3206 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2490out_result;

  { // Node ID: 2490 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2490in_a = id2681out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2490in_b = id3206out_value;

    id2490out_result = (gte_fixed(id2490in_a,id2490in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id352out_result;

  { // Node ID: 352 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id352in_a = id2817out_output[getCycle()%3];

    id352out_result = (not_fixed(id352in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id353out_result;

  { // Node ID: 353 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id353in_a = id2490out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id353in_b = id352out_result;

    HWOffsetFix<1,0,UNSIGNED> id353x_1;

    (id353x_1) = (and_fixed(id353in_a,id353in_b));
    id353out_result = (id353x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id354out_result;

  { // Node ID: 354 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id354in_a = id353out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id354in_b = id2815out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id354x_1;

    (id354x_1) = (or_fixed(id354in_a,id354in_b));
    id354out_result = (id354x_1);
  }
  HWRawBits<2> id355out_result;

  { // Node ID: 355 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id355in_in0 = id345out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id355in_in1 = id354out_result;

    id355out_result = (cat(id355in_in0,id355in_in1));
  }
  { // Node ID: 347 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id346out_o;

  { // Node ID: 346 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id346in_i = id2681out_result;

    id346out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id346in_i));
  }
  { // Node ID: 331 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id332out_result;

  { // Node ID: 332 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id332in_sel = id2487out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id332in_option0 = id328out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id332in_option1 = id331out_value;

    HWOffsetFix<12,-11,UNSIGNED> id332x_1;

    switch((id332in_sel.getValueAsLong())) {
      case 0l:
        id332x_1 = id332in_option0;
        break;
      case 1l:
        id332x_1 = id332in_option1;
        break;
      default:
        id332x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id332out_result = (id332x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id333out_o;

  { // Node ID: 333 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id333in_i = id332out_result;

    id333out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id333in_i));
  }
  HWRawBits<20> id348out_result;

  { // Node ID: 348 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id348in_in0 = id347out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id348in_in1 = id346out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id348in_in2 = id333out_o;

    id348out_result = (cat((cat(id348in_in0,id348in_in1)),id348in_in2));
  }
  HWFloat<8,12> id349out_output;

  { // Node ID: 349 (NodeReinterpret)
    const HWRawBits<20> &id349in_input = id348out_result;

    id349out_output = (cast_bits2float<8,12>(id349in_input));
  }
  { // Node ID: 356 (NodeConstantRawBits)
  }
  { // Node ID: 357 (NodeConstantRawBits)
  }
  { // Node ID: 359 (NodeConstantRawBits)
  }
  HWRawBits<20> id2491out_result;

  { // Node ID: 2491 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2491in_in0 = id356out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2491in_in1 = id357out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2491in_in2 = id359out_value;

    id2491out_result = (cat((cat(id2491in_in0,id2491in_in1)),id2491in_in2));
  }
  HWFloat<8,12> id361out_output;

  { // Node ID: 361 (NodeReinterpret)
    const HWRawBits<20> &id361in_input = id2491out_result;

    id361out_output = (cast_bits2float<8,12>(id361in_input));
  }
  { // Node ID: 2403 (NodeConstantRawBits)
  }
  HWFloat<8,12> id364out_result;

  { // Node ID: 364 (NodeMux)
    const HWRawBits<2> &id364in_sel = id355out_result;
    const HWFloat<8,12> &id364in_option0 = id349out_output;
    const HWFloat<8,12> &id364in_option1 = id361out_output;
    const HWFloat<8,12> &id364in_option2 = id2403out_value;
    const HWFloat<8,12> &id364in_option3 = id361out_output;

    HWFloat<8,12> id364x_1;

    switch((id364in_sel.getValueAsLong())) {
      case 0l:
        id364x_1 = id364in_option0;
        break;
      case 1l:
        id364x_1 = id364in_option1;
        break;
      case 2l:
        id364x_1 = id364in_option2;
        break;
      case 3l:
        id364x_1 = id364in_option3;
        break;
      default:
        id364x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id364out_result = (id364x_1);
  }
  { // Node ID: 3205 (NodeConstantRawBits)
  }
  HWFloat<8,12> id374out_result;

  { // Node ID: 374 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id374in_sel = id2820out_output[getCycle()%9];
    const HWFloat<8,12> &id374in_option0 = id364out_result;
    const HWFloat<8,12> &id374in_option1 = id3205out_value;

    HWFloat<8,12> id374x_1;

    switch((id374in_sel.getValueAsLong())) {
      case 0l:
        id374x_1 = id374in_option0;
        break;
      case 1l:
        id374x_1 = id374in_option1;
        break;
      default:
        id374x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id374out_result = (id374x_1);
  }
  { // Node ID: 3204 (NodeConstantRawBits)
  }
  HWFloat<8,12> id379out_result;

  { // Node ID: 379 (NodeAdd)
    const HWFloat<8,12> &id379in_a = id374out_result;
    const HWFloat<8,12> &id379in_b = id3204out_value;

    id379out_result = (add_float(id379in_a,id379in_b));
  }
  HWFloat<8,12> id380out_result;

  { // Node ID: 380 (NodeDiv)
    const HWFloat<8,12> &id380in_a = id296out_result;
    const HWFloat<8,12> &id380in_b = id379out_result;

    id380out_result = (div_float(id380in_a,id380in_b));
  }
  HWFloat<8,12> id381out_result;

  { // Node ID: 381 (NodeMul)
    const HWFloat<8,12> &id381in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id381in_b = id380out_result;

    id381out_result = (mul_float(id381in_a,id381in_b));
  }
  HWFloat<8,12> id1653out_result;

  { // Node ID: 1653 (NodeMul)
    const HWFloat<8,12> &id1653in_a = id381out_result;
    const HWFloat<8,12> &id1653in_b = id2800out_output[getCycle()%9];

    id1653out_result = (mul_float(id1653in_a,id1653in_b));
  }
  HWFloat<8,12> id1654out_result;

  { // Node ID: 1654 (NodeSub)
    const HWFloat<8,12> &id1654in_a = id1652out_result;
    const HWFloat<8,12> &id1654in_b = id1653out_result;

    id1654out_result = (sub_float(id1654in_a,id1654in_b));
  }
  HWFloat<8,12> id1655out_result;

  { // Node ID: 1655 (NodeAdd)
    const HWFloat<8,12> &id1655in_a = id2800out_output[getCycle()%9];
    const HWFloat<8,12> &id1655in_b = id1654out_result;

    id1655out_result = (add_float(id1655in_a,id1655in_b));
  }
  { // Node ID: 2779 (NodeFIFO)
    const HWFloat<8,12> &id2779in_input = id1655out_result;

    id2779out_output[(getCycle()+1)%2] = id2779in_input;
  }
  { // Node ID: 3203 (NodeConstantRawBits)
  }
  { // Node ID: 2492 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2492in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2492in_b = id3203out_value;

    id2492out_result[(getCycle()+1)%2] = (eq_fixed(id2492in_a,id2492in_b));
  }
  { // Node ID: 3202 (NodeConstantRawBits)
  }
  HWFloat<8,12> id383out_result;

  { // Node ID: 383 (NodeAdd)
    const HWFloat<8,12> &id383in_a = id3030out_output[getCycle()%8];
    const HWFloat<8,12> &id383in_b = id3202out_value;

    id383out_result = (add_float(id383in_a,id383in_b));
  }
  HWFloat<8,12> id384out_result;

  { // Node ID: 384 (NodeNeg)
    const HWFloat<8,12> &id384in_a = id383out_result;

    id384out_result = (neg_float(id384in_a));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id422out_o;

  { // Node ID: 422 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id422in_i = id2826out_output[getCycle()%3];

    id422out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id422in_i));
  }
  { // Node ID: 425 (NodeConstantRawBits)
  }
  { // Node ID: 2634 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id412out_result;

  { // Node ID: 412 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id412in_a = id465out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id412in_b = id2827out_output[getCycle()%3];

    id412out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id412in_a,id412in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id413out_result;

  { // Node ID: 413 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id413in_a = id2827out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id413in_b = id465out_dout[getCycle()%3];

    id413out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id413in_a,id413in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id414out_result;

  { // Node ID: 414 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id414in_a = id412out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id414in_b = id413out_result;

    id414out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id414in_a,id414in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id415out_o;

  { // Node ID: 415 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id415in_i = id414out_result;

    id415out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id415in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id416out_o;

  { // Node ID: 416 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id416in_i = id415out_o;

    id416out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id416in_i));
  }
  { // Node ID: 3198 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2495out_result;

  { // Node ID: 2495 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2495in_a = id416out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2495in_b = id3198out_value;

    id2495out_result = (gte_fixed(id2495in_a,id2495in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2680out_result;

  { // Node ID: 2680 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2680in_a = id422out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2680in_b = id425out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2680in_c = id2634out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2680in_condb = id2495out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2680x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2680x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2680x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2680x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2680x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2680x_1 = id2680in_a;
        break;
      default:
        id2680x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2680in_condb.getValueAsLong())) {
      case 0l:
        id2680x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2680x_2 = id2680in_b;
        break;
      default:
        id2680x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2680x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2680x_3 = id2680in_c;
        break;
      default:
        id2680x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2680x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2680x_1),(id2680x_2))),(id2680x_3)));
    id2680out_result = (id2680x_4);
  }
  HWRawBits<1> id2496out_result;

  { // Node ID: 2496 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2496in_a = id2680out_result;

    id2496out_result = (slice<10,1>(id2496in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2497out_output;

  { // Node ID: 2497 (NodeReinterpret)
    const HWRawBits<1> &id2497in_input = id2496out_result;

    id2497out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2497in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id431out_result;

  { // Node ID: 431 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id431in_a = id2830out_output[getCycle()%3];

    id431out_result = (not_fixed(id431in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id432out_result;

  { // Node ID: 432 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id432in_a = id2497out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_b = id431out_result;

    HWOffsetFix<1,0,UNSIGNED> id432x_1;

    (id432x_1) = (and_fixed(id432in_a,id432in_b));
    id432out_result = (id432x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id433out_result;

  { // Node ID: 433 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id433in_a = id432out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id433in_b = id2832out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id433x_1;

    (id433x_1) = (or_fixed(id433in_a,id433in_b));
    id433out_result = (id433x_1);
  }
  { // Node ID: 3195 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2498out_result;

  { // Node ID: 2498 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2498in_a = id2680out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2498in_b = id3195out_value;

    id2498out_result = (gte_fixed(id2498in_a,id2498in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id440out_result;

  { // Node ID: 440 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id440in_a = id2832out_output[getCycle()%3];

    id440out_result = (not_fixed(id440in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id441out_result;

  { // Node ID: 441 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id441in_a = id2498out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id441in_b = id440out_result;

    HWOffsetFix<1,0,UNSIGNED> id441x_1;

    (id441x_1) = (and_fixed(id441in_a,id441in_b));
    id441out_result = (id441x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id442out_result;

  { // Node ID: 442 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id442in_a = id441out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id442in_b = id2830out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id442x_1;

    (id442x_1) = (or_fixed(id442in_a,id442in_b));
    id442out_result = (id442x_1);
  }
  HWRawBits<2> id443out_result;

  { // Node ID: 443 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_in0 = id433out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id443in_in1 = id442out_result;

    id443out_result = (cat(id443in_in0,id443in_in1));
  }
  { // Node ID: 435 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id434out_o;

  { // Node ID: 434 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id434in_i = id2680out_result;

    id434out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id434in_i));
  }
  { // Node ID: 419 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id420out_result;

  { // Node ID: 420 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id420in_sel = id2495out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id420in_option0 = id416out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id420in_option1 = id419out_value;

    HWOffsetFix<12,-11,UNSIGNED> id420x_1;

    switch((id420in_sel.getValueAsLong())) {
      case 0l:
        id420x_1 = id420in_option0;
        break;
      case 1l:
        id420x_1 = id420in_option1;
        break;
      default:
        id420x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id420out_result = (id420x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id421out_o;

  { // Node ID: 421 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id421in_i = id420out_result;

    id421out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id421in_i));
  }
  HWRawBits<20> id436out_result;

  { // Node ID: 436 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id436in_in0 = id435out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id436in_in1 = id434out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id436in_in2 = id421out_o;

    id436out_result = (cat((cat(id436in_in0,id436in_in1)),id436in_in2));
  }
  HWFloat<8,12> id437out_output;

  { // Node ID: 437 (NodeReinterpret)
    const HWRawBits<20> &id437in_input = id436out_result;

    id437out_output = (cast_bits2float<8,12>(id437in_input));
  }
  { // Node ID: 444 (NodeConstantRawBits)
  }
  { // Node ID: 445 (NodeConstantRawBits)
  }
  { // Node ID: 447 (NodeConstantRawBits)
  }
  HWRawBits<20> id2499out_result;

  { // Node ID: 2499 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2499in_in0 = id444out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2499in_in1 = id445out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2499in_in2 = id447out_value;

    id2499out_result = (cat((cat(id2499in_in0,id2499in_in1)),id2499in_in2));
  }
  HWFloat<8,12> id449out_output;

  { // Node ID: 449 (NodeReinterpret)
    const HWRawBits<20> &id449in_input = id2499out_result;

    id449out_output = (cast_bits2float<8,12>(id449in_input));
  }
  { // Node ID: 2404 (NodeConstantRawBits)
  }
  HWFloat<8,12> id452out_result;

  { // Node ID: 452 (NodeMux)
    const HWRawBits<2> &id452in_sel = id443out_result;
    const HWFloat<8,12> &id452in_option0 = id437out_output;
    const HWFloat<8,12> &id452in_option1 = id449out_output;
    const HWFloat<8,12> &id452in_option2 = id2404out_value;
    const HWFloat<8,12> &id452in_option3 = id449out_output;

    HWFloat<8,12> id452x_1;

    switch((id452in_sel.getValueAsLong())) {
      case 0l:
        id452x_1 = id452in_option0;
        break;
      case 1l:
        id452x_1 = id452in_option1;
        break;
      case 2l:
        id452x_1 = id452in_option2;
        break;
      case 3l:
        id452x_1 = id452in_option3;
        break;
      default:
        id452x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id452out_result = (id452x_1);
  }
  { // Node ID: 3194 (NodeConstantRawBits)
  }
  HWFloat<8,12> id462out_result;

  { // Node ID: 462 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id462in_sel = id2835out_output[getCycle()%9];
    const HWFloat<8,12> &id462in_option0 = id452out_result;
    const HWFloat<8,12> &id462in_option1 = id3194out_value;

    HWFloat<8,12> id462x_1;

    switch((id462in_sel.getValueAsLong())) {
      case 0l:
        id462x_1 = id462in_option0;
        break;
      case 1l:
        id462x_1 = id462in_option1;
        break;
      default:
        id462x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id462out_result = (id462x_1);
  }
  { // Node ID: 3193 (NodeConstantRawBits)
  }
  HWFloat<8,12> id467out_result;

  { // Node ID: 467 (NodeSub)
    const HWFloat<8,12> &id467in_a = id462out_result;
    const HWFloat<8,12> &id467in_b = id3193out_value;

    id467out_result = (sub_float(id467in_a,id467in_b));
  }
  HWFloat<8,12> id468out_result;

  { // Node ID: 468 (NodeDiv)
    const HWFloat<8,12> &id468in_a = id384out_result;
    const HWFloat<8,12> &id468in_b = id467out_result;

    id468out_result = (div_float(id468in_a,id468in_b));
  }
  HWFloat<8,12> id469out_result;

  { // Node ID: 469 (NodeMul)
    const HWFloat<8,12> &id469in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id469in_b = id468out_result;

    id469out_result = (mul_float(id469in_a,id469in_b));
  }
  { // Node ID: 3192 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1657out_result;

  { // Node ID: 1657 (NodeSub)
    const HWFloat<8,12> &id1657in_a = id3192out_value;
    const HWFloat<8,12> &id1657in_b = id2836out_output[getCycle()%9];

    id1657out_result = (sub_float(id1657in_a,id1657in_b));
  }
  HWFloat<8,12> id1658out_result;

  { // Node ID: 1658 (NodeMul)
    const HWFloat<8,12> &id1658in_a = id469out_result;
    const HWFloat<8,12> &id1658in_b = id1657out_result;

    id1658out_result = (mul_float(id1658in_a,id1658in_b));
  }
  { // Node ID: 3191 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id507out_o;

  { // Node ID: 507 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id507in_i = id2837out_output[getCycle()%3];

    id507out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id507in_i));
  }
  { // Node ID: 510 (NodeConstantRawBits)
  }
  { // Node ID: 2636 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id497out_result;

  { // Node ID: 497 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id497in_a = id550out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id497in_b = id2838out_output[getCycle()%3];

    id497out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id497in_a,id497in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id498out_result;

  { // Node ID: 498 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id498in_a = id2838out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id498in_b = id550out_dout[getCycle()%3];

    id498out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id498in_a,id498in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id499out_result;

  { // Node ID: 499 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id499in_a = id497out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id499in_b = id498out_result;

    id499out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id499in_a,id499in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id500out_o;

  { // Node ID: 500 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id500in_i = id499out_result;

    id500out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id500in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id501out_o;

  { // Node ID: 501 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id501in_i = id500out_o;

    id501out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id501in_i));
  }
  { // Node ID: 3187 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2502out_result;

  { // Node ID: 2502 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2502in_a = id501out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2502in_b = id3187out_value;

    id2502out_result = (gte_fixed(id2502in_a,id2502in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2679out_result;

  { // Node ID: 2679 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2679in_a = id507out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2679in_b = id510out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2679in_c = id2636out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2679in_condb = id2502out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2679x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2679x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2679x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2679x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2679x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2679x_1 = id2679in_a;
        break;
      default:
        id2679x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2679in_condb.getValueAsLong())) {
      case 0l:
        id2679x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2679x_2 = id2679in_b;
        break;
      default:
        id2679x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2679x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2679x_3 = id2679in_c;
        break;
      default:
        id2679x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2679x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2679x_1),(id2679x_2))),(id2679x_3)));
    id2679out_result = (id2679x_4);
  }
  HWRawBits<1> id2503out_result;

  { // Node ID: 2503 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2503in_a = id2679out_result;

    id2503out_result = (slice<10,1>(id2503in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2504out_output;

  { // Node ID: 2504 (NodeReinterpret)
    const HWRawBits<1> &id2504in_input = id2503out_result;

    id2504out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2504in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id516out_result;

  { // Node ID: 516 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id516in_a = id2841out_output[getCycle()%3];

    id516out_result = (not_fixed(id516in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id517out_result;

  { // Node ID: 517 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id517in_a = id2504out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id517in_b = id516out_result;

    HWOffsetFix<1,0,UNSIGNED> id517x_1;

    (id517x_1) = (and_fixed(id517in_a,id517in_b));
    id517out_result = (id517x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id518out_result;

  { // Node ID: 518 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id518in_a = id517out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id518in_b = id2843out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id518x_1;

    (id518x_1) = (or_fixed(id518in_a,id518in_b));
    id518out_result = (id518x_1);
  }
  { // Node ID: 3184 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2505out_result;

  { // Node ID: 2505 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2505in_a = id2679out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2505in_b = id3184out_value;

    id2505out_result = (gte_fixed(id2505in_a,id2505in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id525out_result;

  { // Node ID: 525 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id525in_a = id2843out_output[getCycle()%3];

    id525out_result = (not_fixed(id525in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id526out_result;

  { // Node ID: 526 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id526in_a = id2505out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id526in_b = id525out_result;

    HWOffsetFix<1,0,UNSIGNED> id526x_1;

    (id526x_1) = (and_fixed(id526in_a,id526in_b));
    id526out_result = (id526x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id527out_result;

  { // Node ID: 527 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id527in_a = id526out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id527in_b = id2841out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id527x_1;

    (id527x_1) = (or_fixed(id527in_a,id527in_b));
    id527out_result = (id527x_1);
  }
  HWRawBits<2> id528out_result;

  { // Node ID: 528 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in0 = id518out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id528in_in1 = id527out_result;

    id528out_result = (cat(id528in_in0,id528in_in1));
  }
  { // Node ID: 520 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id519out_o;

  { // Node ID: 519 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id519in_i = id2679out_result;

    id519out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id519in_i));
  }
  { // Node ID: 504 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id505out_result;

  { // Node ID: 505 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id505in_sel = id2502out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id505in_option0 = id501out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id505in_option1 = id504out_value;

    HWOffsetFix<12,-11,UNSIGNED> id505x_1;

    switch((id505in_sel.getValueAsLong())) {
      case 0l:
        id505x_1 = id505in_option0;
        break;
      case 1l:
        id505x_1 = id505in_option1;
        break;
      default:
        id505x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id505out_result = (id505x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id506out_o;

  { // Node ID: 506 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id506in_i = id505out_result;

    id506out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id506in_i));
  }
  HWRawBits<20> id521out_result;

  { // Node ID: 521 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id521in_in0 = id520out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id521in_in1 = id519out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id521in_in2 = id506out_o;

    id521out_result = (cat((cat(id521in_in0,id521in_in1)),id521in_in2));
  }
  HWFloat<8,12> id522out_output;

  { // Node ID: 522 (NodeReinterpret)
    const HWRawBits<20> &id522in_input = id521out_result;

    id522out_output = (cast_bits2float<8,12>(id522in_input));
  }
  { // Node ID: 529 (NodeConstantRawBits)
  }
  { // Node ID: 530 (NodeConstantRawBits)
  }
  { // Node ID: 532 (NodeConstantRawBits)
  }
  HWRawBits<20> id2506out_result;

  { // Node ID: 2506 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2506in_in0 = id529out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2506in_in1 = id530out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2506in_in2 = id532out_value;

    id2506out_result = (cat((cat(id2506in_in0,id2506in_in1)),id2506in_in2));
  }
  HWFloat<8,12> id534out_output;

  { // Node ID: 534 (NodeReinterpret)
    const HWRawBits<20> &id534in_input = id2506out_result;

    id534out_output = (cast_bits2float<8,12>(id534in_input));
  }
  { // Node ID: 2405 (NodeConstantRawBits)
  }
  HWFloat<8,12> id537out_result;

  { // Node ID: 537 (NodeMux)
    const HWRawBits<2> &id537in_sel = id528out_result;
    const HWFloat<8,12> &id537in_option0 = id522out_output;
    const HWFloat<8,12> &id537in_option1 = id534out_output;
    const HWFloat<8,12> &id537in_option2 = id2405out_value;
    const HWFloat<8,12> &id537in_option3 = id534out_output;

    HWFloat<8,12> id537x_1;

    switch((id537in_sel.getValueAsLong())) {
      case 0l:
        id537x_1 = id537in_option0;
        break;
      case 1l:
        id537x_1 = id537in_option1;
        break;
      case 2l:
        id537x_1 = id537in_option2;
        break;
      case 3l:
        id537x_1 = id537in_option3;
        break;
      default:
        id537x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id537out_result = (id537x_1);
  }
  { // Node ID: 3183 (NodeConstantRawBits)
  }
  HWFloat<8,12> id547out_result;

  { // Node ID: 547 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id547in_sel = id2846out_output[getCycle()%9];
    const HWFloat<8,12> &id547in_option0 = id537out_result;
    const HWFloat<8,12> &id547in_option1 = id3183out_value;

    HWFloat<8,12> id547x_1;

    switch((id547in_sel.getValueAsLong())) {
      case 0l:
        id547x_1 = id547in_option0;
        break;
      case 1l:
        id547x_1 = id547in_option1;
        break;
      default:
        id547x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id547out_result = (id547x_1);
  }
  HWFloat<8,12> id552out_result;

  { // Node ID: 552 (NodeMul)
    const HWFloat<8,12> &id552in_a = id3191out_value;
    const HWFloat<8,12> &id552in_b = id547out_result;

    id552out_result = (mul_float(id552in_a,id552in_b));
  }
  HWFloat<8,12> id553out_result;

  { // Node ID: 553 (NodeMul)
    const HWFloat<8,12> &id553in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id553in_b = id552out_result;

    id553out_result = (mul_float(id553in_a,id553in_b));
  }
  HWFloat<8,12> id1659out_result;

  { // Node ID: 1659 (NodeMul)
    const HWFloat<8,12> &id1659in_a = id553out_result;
    const HWFloat<8,12> &id1659in_b = id2836out_output[getCycle()%9];

    id1659out_result = (mul_float(id1659in_a,id1659in_b));
  }
  HWFloat<8,12> id1660out_result;

  { // Node ID: 1660 (NodeSub)
    const HWFloat<8,12> &id1660in_a = id1658out_result;
    const HWFloat<8,12> &id1660in_b = id1659out_result;

    id1660out_result = (sub_float(id1660in_a,id1660in_b));
  }
  HWFloat<8,12> id1661out_result;

  { // Node ID: 1661 (NodeAdd)
    const HWFloat<8,12> &id1661in_a = id2836out_output[getCycle()%9];
    const HWFloat<8,12> &id1661in_b = id1660out_result;

    id1661out_result = (add_float(id1661in_a,id1661in_b));
  }
  { // Node ID: 2824 (NodeFIFO)
    const HWFloat<8,12> &id2824in_input = id1661out_result;

    id2824out_output[(getCycle()+1)%2] = id2824in_input;
  }
  HWFloat<8,12> id2706out_output;

  { // Node ID: 2706 (NodeStreamOffset)
    const HWFloat<8,12> &id2706in_input = id2824out_output[getCycle()%2];

    id2706out_output = id2706in_input;
  }
  { // Node ID: 28 (NodeInputMappedReg)
  }
  { // Node ID: 29 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id29in_sel = id2492out_result[getCycle()%2];
    const HWFloat<8,12> &id29in_option0 = id2706out_output;
    const HWFloat<8,12> &id29in_option1 = id28out_m_in;

    HWFloat<8,12> id29x_1;

    switch((id29in_sel.getValueAsLong())) {
      case 0l:
        id29x_1 = id29in_option0;
        break;
      case 1l:
        id29x_1 = id29in_option1;
        break;
      default:
        id29x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id29out_result[(getCycle()+1)%2] = (id29x_1);
  }
  { // Node ID: 2836 (NodeFIFO)
    const HWFloat<8,12> &id2836in_input = id29out_result[getCycle()%2];

    id2836out_output[(getCycle()+8)%9] = id2836in_input;
  }
  { // Node ID: 3201 (NodeConstantRawBits)
  }
  { // Node ID: 3200 (NodeConstantRawBits)
  }
  HWFloat<8,12> id386out_result;

  { // Node ID: 386 (NodeAdd)
    const HWFloat<8,12> &id386in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id386in_b = id3200out_value;

    id386out_result = (add_float(id386in_a,id386in_b));
  }
  HWFloat<8,12> id388out_result;

  { // Node ID: 388 (NodeMul)
    const HWFloat<8,12> &id388in_a = id3201out_value;
    const HWFloat<8,12> &id388in_b = id386out_result;

    id388out_result = (mul_float(id388in_a,id388in_b));
  }
  HWRawBits<8> id455out_result;

  { // Node ID: 455 (NodeSlice)
    const HWFloat<8,12> &id455in_a = id388out_result;

    id455out_result = (slice<11,8>(id455in_a));
  }
  { // Node ID: 456 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2493out_result;

  { // Node ID: 2493 (NodeEqInlined)
    const HWRawBits<8> &id2493in_a = id455out_result;
    const HWRawBits<8> &id2493in_b = id456out_value;

    id2493out_result = (eq_bits(id2493in_a,id2493in_b));
  }
  HWRawBits<11> id454out_result;

  { // Node ID: 454 (NodeSlice)
    const HWFloat<8,12> &id454in_a = id388out_result;

    id454out_result = (slice<0,11>(id454in_a));
  }
  { // Node ID: 3199 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2494out_result;

  { // Node ID: 2494 (NodeNeqInlined)
    const HWRawBits<11> &id2494in_a = id454out_result;
    const HWRawBits<11> &id2494in_b = id3199out_value;

    id2494out_result = (neq_bits(id2494in_a,id2494in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id460out_result;

  { // Node ID: 460 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id460in_a = id2493out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id460in_b = id2494out_result;

    HWOffsetFix<1,0,UNSIGNED> id460x_1;

    (id460x_1) = (and_fixed(id460in_a,id460in_b));
    id460out_result = (id460x_1);
  }
  { // Node ID: 2835 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2835in_input = id460out_result;

    id2835out_output[(getCycle()+8)%9] = id2835in_input;
  }
  { // Node ID: 389 (NodeConstantRawBits)
  }
  HWFloat<8,12> id390out_output;
  HWOffsetFix<1,0,UNSIGNED> id390out_output_doubt;

  { // Node ID: 390 (NodeDoubtBitOp)
    const HWFloat<8,12> &id390in_input = id388out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id390in_doubt = id389out_value;

    id390out_output = id390in_input;
    id390out_output_doubt = id390in_doubt;
  }
  { // Node ID: 391 (NodeCast)
    const HWFloat<8,12> &id391in_i = id390out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id391in_i_doubt = id390out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id391x_1;

    id391out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id391in_i,(&(id391x_1))));
    id391out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id391x_1),(c_hw_fix_4_0_uns_bits))),id391in_i_doubt));
  }
  { // Node ID: 394 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id393out_result;
  HWOffsetFix<1,0,UNSIGNED> id393out_result_doubt;

  { // Node ID: 393 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id393in_a = id391out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id393in_a_doubt = id391out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id393in_b = id394out_value;

    HWOffsetFix<1,0,UNSIGNED> id393x_1;

    id393out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id393in_a,id393in_b,(&(id393x_1))));
    id393out_result_doubt = (or_fixed((neq_fixed((id393x_1),(c_hw_fix_1_0_uns_bits_1))),id393in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id395out_o;
  HWOffsetFix<1,0,UNSIGNED> id395out_o_doubt;

  { // Node ID: 395 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id395in_i = id393out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id395in_i_doubt = id393out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id395x_1;

    id395out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id395in_i,(&(id395x_1))));
    id395out_o_doubt = (or_fixed((neq_fixed((id395x_1),(c_hw_fix_1_0_uns_bits_1))),id395in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id404out_output;

  { // Node ID: 404 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id404in_input = id395out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id404in_input_doubt = id395out_o_doubt;

    id404out_output = id404in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id405out_o;

  { // Node ID: 405 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id405in_i = id404out_output;

    id405out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id405in_i));
  }
  { // Node ID: 2826 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2826in_input = id405out_o;

    id2826out_output[(getCycle()+2)%3] = id2826in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id406out_o;

  { // Node ID: 406 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id406in_i = id404out_output;

    id406out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id406in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id464out_output;

  { // Node ID: 464 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id464in_input = id406out_o;

    id464out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id464in_input))));
  }
  { // Node ID: 465 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id465in_addr = id464out_output;

    HWOffsetFix<12,-12,UNSIGNED> id465x_1;

    switch(((long)((id465in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id465x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id465x_1 = (id465sta_rom_store[(id465in_addr.getValueAsLong())]);
        break;
      default:
        id465x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id465out_dout[(getCycle()+2)%3] = (id465x_1);
  }
  { // Node ID: 410 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id407out_o;

  { // Node ID: 407 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id407in_i = id404out_output;

    id407out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id407in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id409out_result;

  { // Node ID: 409 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id409in_a = id410out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id409in_b = id407out_o;

    id409out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id409in_a,id409in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id411out_o;

  { // Node ID: 411 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id411in_i = id409out_result;

    id411out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id411in_i));
  }
  { // Node ID: 2827 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2827in_input = id411out_o;

    id2827out_output[(getCycle()+2)%3] = id2827in_input;
  }
  { // Node ID: 3197 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id397out_result;

  { // Node ID: 397 (NodeGt)
    const HWFloat<8,12> &id397in_a = id388out_result;
    const HWFloat<8,12> &id397in_b = id3197out_value;

    id397out_result = (gt_float(id397in_a,id397in_b));
  }
  { // Node ID: 2829 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2829in_input = id397out_result;

    id2829out_output[(getCycle()+6)%7] = id2829in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id398out_output;

  { // Node ID: 398 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id398in_input = id395out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id398in_input_doubt = id395out_o_doubt;

    id398out_output = id398in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id399out_result;

  { // Node ID: 399 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_a = id2829out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id399in_b = id398out_output;

    HWOffsetFix<1,0,UNSIGNED> id399x_1;

    (id399x_1) = (and_fixed(id399in_a,id399in_b));
    id399out_result = (id399x_1);
  }
  { // Node ID: 2830 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2830in_input = id399out_result;

    id2830out_output[(getCycle()+2)%3] = id2830in_input;
  }
  { // Node ID: 3196 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeLt)
    const HWFloat<8,12> &id401in_a = id388out_result;
    const HWFloat<8,12> &id401in_b = id3196out_value;

    id401out_result = (lt_float(id401in_a,id401in_b));
  }
  { // Node ID: 2831 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2831in_input = id401out_result;

    id2831out_output[(getCycle()+6)%7] = id2831in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id402out_output;

  { // Node ID: 402 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id402in_input = id395out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id402in_input_doubt = id395out_o_doubt;

    id402out_output = id402in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id403out_result;

  { // Node ID: 403 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_a = id2831out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id403in_b = id402out_output;

    HWOffsetFix<1,0,UNSIGNED> id403x_1;

    (id403x_1) = (and_fixed(id403in_a,id403in_b));
    id403out_result = (id403x_1);
  }
  { // Node ID: 2832 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2832in_input = id403out_result;

    id2832out_output[(getCycle()+2)%3] = id2832in_input;
  }
  { // Node ID: 3190 (NodeConstantRawBits)
  }
  { // Node ID: 3189 (NodeConstantRawBits)
  }
  HWFloat<8,12> id471out_result;

  { // Node ID: 471 (NodeAdd)
    const HWFloat<8,12> &id471in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id471in_b = id3189out_value;

    id471out_result = (add_float(id471in_a,id471in_b));
  }
  HWFloat<8,12> id473out_result;

  { // Node ID: 473 (NodeMul)
    const HWFloat<8,12> &id473in_a = id3190out_value;
    const HWFloat<8,12> &id473in_b = id471out_result;

    id473out_result = (mul_float(id473in_a,id473in_b));
  }
  HWRawBits<8> id540out_result;

  { // Node ID: 540 (NodeSlice)
    const HWFloat<8,12> &id540in_a = id473out_result;

    id540out_result = (slice<11,8>(id540in_a));
  }
  { // Node ID: 541 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2500out_result;

  { // Node ID: 2500 (NodeEqInlined)
    const HWRawBits<8> &id2500in_a = id540out_result;
    const HWRawBits<8> &id2500in_b = id541out_value;

    id2500out_result = (eq_bits(id2500in_a,id2500in_b));
  }
  HWRawBits<11> id539out_result;

  { // Node ID: 539 (NodeSlice)
    const HWFloat<8,12> &id539in_a = id473out_result;

    id539out_result = (slice<0,11>(id539in_a));
  }
  { // Node ID: 3188 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2501out_result;

  { // Node ID: 2501 (NodeNeqInlined)
    const HWRawBits<11> &id2501in_a = id539out_result;
    const HWRawBits<11> &id2501in_b = id3188out_value;

    id2501out_result = (neq_bits(id2501in_a,id2501in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id545out_result;

  { // Node ID: 545 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id545in_a = id2500out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id545in_b = id2501out_result;

    HWOffsetFix<1,0,UNSIGNED> id545x_1;

    (id545x_1) = (and_fixed(id545in_a,id545in_b));
    id545out_result = (id545x_1);
  }
  { // Node ID: 2846 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2846in_input = id545out_result;

    id2846out_output[(getCycle()+8)%9] = id2846in_input;
  }
  { // Node ID: 474 (NodeConstantRawBits)
  }
  HWFloat<8,12> id475out_output;
  HWOffsetFix<1,0,UNSIGNED> id475out_output_doubt;

  { // Node ID: 475 (NodeDoubtBitOp)
    const HWFloat<8,12> &id475in_input = id473out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id475in_doubt = id474out_value;

    id475out_output = id475in_input;
    id475out_output_doubt = id475in_doubt;
  }
  { // Node ID: 476 (NodeCast)
    const HWFloat<8,12> &id476in_i = id475out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id476in_i_doubt = id475out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id476x_1;

    id476out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id476in_i,(&(id476x_1))));
    id476out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id476x_1),(c_hw_fix_4_0_uns_bits))),id476in_i_doubt));
  }
  { // Node ID: 479 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id478out_result;
  HWOffsetFix<1,0,UNSIGNED> id478out_result_doubt;

  { // Node ID: 478 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id478in_a = id476out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id478in_a_doubt = id476out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id478in_b = id479out_value;

    HWOffsetFix<1,0,UNSIGNED> id478x_1;

    id478out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id478in_a,id478in_b,(&(id478x_1))));
    id478out_result_doubt = (or_fixed((neq_fixed((id478x_1),(c_hw_fix_1_0_uns_bits_1))),id478in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id480out_o;
  HWOffsetFix<1,0,UNSIGNED> id480out_o_doubt;

  { // Node ID: 480 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id480in_i = id478out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id480in_i_doubt = id478out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id480x_1;

    id480out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id480in_i,(&(id480x_1))));
    id480out_o_doubt = (or_fixed((neq_fixed((id480x_1),(c_hw_fix_1_0_uns_bits_1))),id480in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id489out_output;

  { // Node ID: 489 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id489in_input = id480out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id489in_input_doubt = id480out_o_doubt;

    id489out_output = id489in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id490out_o;

  { // Node ID: 490 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id490in_i = id489out_output;

    id490out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id490in_i));
  }
  { // Node ID: 2837 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2837in_input = id490out_o;

    id2837out_output[(getCycle()+2)%3] = id2837in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id491out_o;

  { // Node ID: 491 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id491in_i = id489out_output;

    id491out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id491in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id549out_output;

  { // Node ID: 549 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id549in_input = id491out_o;

    id549out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id549in_input))));
  }
  { // Node ID: 550 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id550in_addr = id549out_output;

    HWOffsetFix<12,-12,UNSIGNED> id550x_1;

    switch(((long)((id550in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id550x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id550x_1 = (id550sta_rom_store[(id550in_addr.getValueAsLong())]);
        break;
      default:
        id550x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id550out_dout[(getCycle()+2)%3] = (id550x_1);
  }
  { // Node ID: 495 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id492out_o;

  { // Node ID: 492 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id492in_i = id489out_output;

    id492out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id492in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id494out_result;

  { // Node ID: 494 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id494in_a = id495out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id494in_b = id492out_o;

    id494out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id494in_a,id494in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id496out_o;

  { // Node ID: 496 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id496in_i = id494out_result;

    id496out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id496in_i));
  }
  { // Node ID: 2838 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2838in_input = id496out_o;

    id2838out_output[(getCycle()+2)%3] = id2838in_input;
  }
  { // Node ID: 3186 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id482out_result;

  { // Node ID: 482 (NodeGt)
    const HWFloat<8,12> &id482in_a = id473out_result;
    const HWFloat<8,12> &id482in_b = id3186out_value;

    id482out_result = (gt_float(id482in_a,id482in_b));
  }
  { // Node ID: 2840 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2840in_input = id482out_result;

    id2840out_output[(getCycle()+6)%7] = id2840in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id483out_output;

  { // Node ID: 483 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id483in_input = id480out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id483in_input_doubt = id480out_o_doubt;

    id483out_output = id483in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id484out_result;

  { // Node ID: 484 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id484in_a = id2840out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id484in_b = id483out_output;

    HWOffsetFix<1,0,UNSIGNED> id484x_1;

    (id484x_1) = (and_fixed(id484in_a,id484in_b));
    id484out_result = (id484x_1);
  }
  { // Node ID: 2841 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2841in_input = id484out_result;

    id2841out_output[(getCycle()+2)%3] = id2841in_input;
  }
  { // Node ID: 3185 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id486out_result;

  { // Node ID: 486 (NodeLt)
    const HWFloat<8,12> &id486in_a = id473out_result;
    const HWFloat<8,12> &id486in_b = id3185out_value;

    id486out_result = (lt_float(id486in_a,id486in_b));
  }
  { // Node ID: 2842 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2842in_input = id486out_result;

    id2842out_output[(getCycle()+6)%7] = id2842in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id487out_output;

  { // Node ID: 487 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id487in_input = id480out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id487in_input_doubt = id480out_o_doubt;

    id487out_output = id487in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id488out_result;

  { // Node ID: 488 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id488in_a = id2842out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id488in_b = id487out_output;

    HWOffsetFix<1,0,UNSIGNED> id488x_1;

    (id488x_1) = (and_fixed(id488in_a,id488in_b));
    id488out_result = (id488x_1);
  }
  { // Node ID: 2843 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2843in_input = id488out_result;

    id2843out_output[(getCycle()+2)%3] = id2843in_input;
  }
  { // Node ID: 2692 (NodePO2FPMult)
    const HWFloat<8,12> &id2692in_floatIn = id1661out_result;

    id2692out_floatOut[(getCycle()+1)%2] = (mul_float(id2692in_floatIn,(c_hw_flt_8_12_4_0val)));
  }
  { // Node ID: 3182 (NodeConstantRawBits)
  }
  { // Node ID: 2507 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2507in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2507in_b = id3182out_value;

    id2507out_result[(getCycle()+1)%2] = (eq_fixed(id2507in_a,id2507in_b));
  }
  HWFloat<8,12> id2707out_output;

  { // Node ID: 2707 (NodeStreamOffset)
    const HWFloat<8,12> &id2707in_input = id1667out_result;

    id2707out_output = id2707in_input;
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id37in_sel = id2507out_result[getCycle()%2];
    const HWFloat<8,12> &id37in_option0 = id2707out_output;
    const HWFloat<8,12> &id37in_option1 = id36out_h_in;

    HWFloat<8,12> id37x_1;

    switch((id37in_sel.getValueAsLong())) {
      case 0l:
        id37x_1 = id37in_option0;
        break;
      case 1l:
        id37x_1 = id37in_option1;
        break;
      default:
        id37x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id37out_result[(getCycle()+1)%2] = (id37x_1);
  }
  { // Node ID: 2861 (NodeFIFO)
    const HWFloat<8,12> &id2861in_input = id37out_result[getCycle()%2];

    id2861out_output[(getCycle()+9)%10] = id2861in_input;
  }
  { // Node ID: 3180 (NodeConstantRawBits)
  }
  HWFloat<8,12> id555out_result;

  { // Node ID: 555 (NodeAdd)
    const HWFloat<8,12> &id555in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id555in_b = id3180out_value;

    id555out_result = (add_float(id555in_a,id555in_b));
  }
  { // Node ID: 2693 (NodePO2FPMult)
    const HWFloat<8,12> &id2693in_floatIn = id555out_result;

    id2693out_floatOut[(getCycle()+1)%2] = (mul_float(id2693in_floatIn,(c_hw_flt_8_12_n0_25val)));
  }
  HWRawBits<8> id624out_result;

  { // Node ID: 624 (NodeSlice)
    const HWFloat<8,12> &id624in_a = id2693out_floatOut[getCycle()%2];

    id624out_result = (slice<11,8>(id624in_a));
  }
  { // Node ID: 625 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2508out_result;

  { // Node ID: 2508 (NodeEqInlined)
    const HWRawBits<8> &id2508in_a = id624out_result;
    const HWRawBits<8> &id2508in_b = id625out_value;

    id2508out_result = (eq_bits(id2508in_a,id2508in_b));
  }
  HWRawBits<11> id623out_result;

  { // Node ID: 623 (NodeSlice)
    const HWFloat<8,12> &id623in_a = id2693out_floatOut[getCycle()%2];

    id623out_result = (slice<0,11>(id623in_a));
  }
  { // Node ID: 3179 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2509out_result;

  { // Node ID: 2509 (NodeNeqInlined)
    const HWRawBits<11> &id2509in_a = id623out_result;
    const HWRawBits<11> &id2509in_b = id3179out_value;

    id2509out_result = (neq_bits(id2509in_a,id2509in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id629out_result;

  { // Node ID: 629 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id629in_a = id2508out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id629in_b = id2509out_result;

    HWOffsetFix<1,0,UNSIGNED> id629x_1;

    (id629x_1) = (and_fixed(id629in_a,id629in_b));
    id629out_result = (id629x_1);
  }
  { // Node ID: 2860 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2860in_input = id629out_result;

    id2860out_output[(getCycle()+8)%9] = id2860in_input;
  }
  { // Node ID: 558 (NodeConstantRawBits)
  }
  HWFloat<8,12> id559out_output;
  HWOffsetFix<1,0,UNSIGNED> id559out_output_doubt;

  { // Node ID: 559 (NodeDoubtBitOp)
    const HWFloat<8,12> &id559in_input = id2693out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id559in_doubt = id558out_value;

    id559out_output = id559in_input;
    id559out_output_doubt = id559in_doubt;
  }
  { // Node ID: 560 (NodeCast)
    const HWFloat<8,12> &id560in_i = id559out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id560in_i_doubt = id559out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id560x_1;

    id560out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id560in_i,(&(id560x_1))));
    id560out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id560x_1),(c_hw_fix_4_0_uns_bits))),id560in_i_doubt));
  }
  { // Node ID: 563 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id562out_result;
  HWOffsetFix<1,0,UNSIGNED> id562out_result_doubt;

  { // Node ID: 562 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id562in_a = id560out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id562in_a_doubt = id560out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id562in_b = id563out_value;

    HWOffsetFix<1,0,UNSIGNED> id562x_1;

    id562out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id562in_a,id562in_b,(&(id562x_1))));
    id562out_result_doubt = (or_fixed((neq_fixed((id562x_1),(c_hw_fix_1_0_uns_bits_1))),id562in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id564out_o;
  HWOffsetFix<1,0,UNSIGNED> id564out_o_doubt;

  { // Node ID: 564 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id564in_i = id562out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id564in_i_doubt = id562out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id564x_1;

    id564out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id564in_i,(&(id564x_1))));
    id564out_o_doubt = (or_fixed((neq_fixed((id564x_1),(c_hw_fix_1_0_uns_bits_1))),id564in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id573out_output;

  { // Node ID: 573 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id573in_input = id564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id573in_input_doubt = id564out_o_doubt;

    id573out_output = id573in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id574out_o;

  { // Node ID: 574 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id574in_i = id573out_output;

    id574out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id574in_i));
  }
  { // Node ID: 2851 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2851in_input = id574out_o;

    id2851out_output[(getCycle()+2)%3] = id2851in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id575out_o;

  { // Node ID: 575 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id575in_i = id573out_output;

    id575out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id575in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id633out_output;

  { // Node ID: 633 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id633in_input = id575out_o;

    id633out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id633in_input))));
  }
  { // Node ID: 634 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id634in_addr = id633out_output;

    HWOffsetFix<12,-12,UNSIGNED> id634x_1;

    switch(((long)((id634in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id634x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id634x_1 = (id634sta_rom_store[(id634in_addr.getValueAsLong())]);
        break;
      default:
        id634x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id634out_dout[(getCycle()+2)%3] = (id634x_1);
  }
  { // Node ID: 579 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id576out_o;

  { // Node ID: 576 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id576in_i = id573out_output;

    id576out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id576in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id578out_result;

  { // Node ID: 578 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id578in_a = id579out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id578in_b = id576out_o;

    id578out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id578in_a,id578in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id580out_o;

  { // Node ID: 580 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id580in_i = id578out_result;

    id580out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id580in_i));
  }
  { // Node ID: 2852 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2852in_input = id580out_o;

    id2852out_output[(getCycle()+2)%3] = id2852in_input;
  }
  { // Node ID: 3177 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id566out_result;

  { // Node ID: 566 (NodeGt)
    const HWFloat<8,12> &id566in_a = id2693out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id566in_b = id3177out_value;

    id566out_result = (gt_float(id566in_a,id566in_b));
  }
  { // Node ID: 2854 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2854in_input = id566out_result;

    id2854out_output[(getCycle()+6)%7] = id2854in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id567out_output;

  { // Node ID: 567 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id567in_input = id564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id567in_input_doubt = id564out_o_doubt;

    id567out_output = id567in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id568out_result;

  { // Node ID: 568 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id568in_a = id2854out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id568in_b = id567out_output;

    HWOffsetFix<1,0,UNSIGNED> id568x_1;

    (id568x_1) = (and_fixed(id568in_a,id568in_b));
    id568out_result = (id568x_1);
  }
  { // Node ID: 2855 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2855in_input = id568out_result;

    id2855out_output[(getCycle()+2)%3] = id2855in_input;
  }
  { // Node ID: 3176 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id570out_result;

  { // Node ID: 570 (NodeLt)
    const HWFloat<8,12> &id570in_a = id2693out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id570in_b = id3176out_value;

    id570out_result = (lt_float(id570in_a,id570in_b));
  }
  { // Node ID: 2856 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2856in_input = id570out_result;

    id2856out_output[(getCycle()+6)%7] = id2856in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id571out_output;

  { // Node ID: 571 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id571in_input = id564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id571in_input_doubt = id564out_o_doubt;

    id571out_output = id571in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id572out_result;

  { // Node ID: 572 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id572in_a = id2856out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id572in_b = id571out_output;

    HWOffsetFix<1,0,UNSIGNED> id572x_1;

    (id572x_1) = (and_fixed(id572in_a,id572in_b));
    id572out_result = (id572x_1);
  }
  { // Node ID: 2857 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2857in_input = id572out_result;

    id2857out_output[(getCycle()+2)%3] = id2857in_input;
  }
  { // Node ID: 3171 (NodeConstantRawBits)
  }
  { // Node ID: 3170 (NodeConstantRawBits)
  }
  HWFloat<8,12> id639out_result;

  { // Node ID: 639 (NodeAdd)
    const HWFloat<8,12> &id639in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id639in_b = id3170out_value;

    id639out_result = (add_float(id639in_a,id639in_b));
  }
  HWFloat<8,12> id641out_result;

  { // Node ID: 641 (NodeMul)
    const HWFloat<8,12> &id641in_a = id3171out_value;
    const HWFloat<8,12> &id641in_b = id639out_result;

    id641out_result = (mul_float(id641in_a,id641in_b));
  }
  HWRawBits<8> id708out_result;

  { // Node ID: 708 (NodeSlice)
    const HWFloat<8,12> &id708in_a = id641out_result;

    id708out_result = (slice<11,8>(id708in_a));
  }
  { // Node ID: 709 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2515out_result;

  { // Node ID: 2515 (NodeEqInlined)
    const HWRawBits<8> &id2515in_a = id708out_result;
    const HWRawBits<8> &id2515in_b = id709out_value;

    id2515out_result = (eq_bits(id2515in_a,id2515in_b));
  }
  HWRawBits<11> id707out_result;

  { // Node ID: 707 (NodeSlice)
    const HWFloat<8,12> &id707in_a = id641out_result;

    id707out_result = (slice<0,11>(id707in_a));
  }
  { // Node ID: 3169 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2516out_result;

  { // Node ID: 2516 (NodeNeqInlined)
    const HWRawBits<11> &id2516in_a = id707out_result;
    const HWRawBits<11> &id2516in_b = id3169out_value;

    id2516out_result = (neq_bits(id2516in_a,id2516in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id713out_result;

  { // Node ID: 713 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id713in_a = id2515out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id713in_b = id2516out_result;

    HWOffsetFix<1,0,UNSIGNED> id713x_1;

    (id713x_1) = (and_fixed(id713in_a,id713in_b));
    id713out_result = (id713x_1);
  }
  { // Node ID: 2872 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2872in_input = id713out_result;

    id2872out_output[(getCycle()+8)%9] = id2872in_input;
  }
  { // Node ID: 642 (NodeConstantRawBits)
  }
  HWFloat<8,12> id643out_output;
  HWOffsetFix<1,0,UNSIGNED> id643out_output_doubt;

  { // Node ID: 643 (NodeDoubtBitOp)
    const HWFloat<8,12> &id643in_input = id641out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id643in_doubt = id642out_value;

    id643out_output = id643in_input;
    id643out_output_doubt = id643in_doubt;
  }
  { // Node ID: 644 (NodeCast)
    const HWFloat<8,12> &id644in_i = id643out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id644in_i_doubt = id643out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id644x_1;

    id644out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id644in_i,(&(id644x_1))));
    id644out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id644x_1),(c_hw_fix_4_0_uns_bits))),id644in_i_doubt));
  }
  { // Node ID: 647 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id646out_result;
  HWOffsetFix<1,0,UNSIGNED> id646out_result_doubt;

  { // Node ID: 646 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id646in_a = id644out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id646in_a_doubt = id644out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id646in_b = id647out_value;

    HWOffsetFix<1,0,UNSIGNED> id646x_1;

    id646out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id646in_a,id646in_b,(&(id646x_1))));
    id646out_result_doubt = (or_fixed((neq_fixed((id646x_1),(c_hw_fix_1_0_uns_bits_1))),id646in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id648out_o;
  HWOffsetFix<1,0,UNSIGNED> id648out_o_doubt;

  { // Node ID: 648 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id648in_i = id646out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id648in_i_doubt = id646out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id648x_1;

    id648out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id648in_i,(&(id648x_1))));
    id648out_o_doubt = (or_fixed((neq_fixed((id648x_1),(c_hw_fix_1_0_uns_bits_1))),id648in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id657out_output;

  { // Node ID: 657 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id657in_input = id648out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id657in_input_doubt = id648out_o_doubt;

    id657out_output = id657in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id658out_o;

  { // Node ID: 658 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id658in_i = id657out_output;

    id658out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id658in_i));
  }
  { // Node ID: 2863 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2863in_input = id658out_o;

    id2863out_output[(getCycle()+2)%3] = id2863in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id659out_o;

  { // Node ID: 659 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id659in_i = id657out_output;

    id659out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id659in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id717out_output;

  { // Node ID: 717 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id717in_input = id659out_o;

    id717out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id717in_input))));
  }
  { // Node ID: 718 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id718in_addr = id717out_output;

    HWOffsetFix<12,-12,UNSIGNED> id718x_1;

    switch(((long)((id718in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id718x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id718x_1 = (id718sta_rom_store[(id718in_addr.getValueAsLong())]);
        break;
      default:
        id718x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id718out_dout[(getCycle()+2)%3] = (id718x_1);
  }
  { // Node ID: 663 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id660out_o;

  { // Node ID: 660 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id660in_i = id657out_output;

    id660out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id660in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id662out_result;

  { // Node ID: 662 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id662in_a = id663out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id662in_b = id660out_o;

    id662out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id662in_a,id662in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id664out_o;

  { // Node ID: 664 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id664in_i = id662out_result;

    id664out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id664in_i));
  }
  { // Node ID: 2864 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2864in_input = id664out_o;

    id2864out_output[(getCycle()+2)%3] = id2864in_input;
  }
  { // Node ID: 3167 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id650out_result;

  { // Node ID: 650 (NodeGt)
    const HWFloat<8,12> &id650in_a = id641out_result;
    const HWFloat<8,12> &id650in_b = id3167out_value;

    id650out_result = (gt_float(id650in_a,id650in_b));
  }
  { // Node ID: 2866 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2866in_input = id650out_result;

    id2866out_output[(getCycle()+6)%7] = id2866in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id651out_output;

  { // Node ID: 651 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id651in_input = id648out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id651in_input_doubt = id648out_o_doubt;

    id651out_output = id651in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id652out_result;

  { // Node ID: 652 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id652in_a = id2866out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id652in_b = id651out_output;

    HWOffsetFix<1,0,UNSIGNED> id652x_1;

    (id652x_1) = (and_fixed(id652in_a,id652in_b));
    id652out_result = (id652x_1);
  }
  { // Node ID: 2867 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2867in_input = id652out_result;

    id2867out_output[(getCycle()+2)%3] = id2867in_input;
  }
  { // Node ID: 3166 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id654out_result;

  { // Node ID: 654 (NodeLt)
    const HWFloat<8,12> &id654in_a = id641out_result;
    const HWFloat<8,12> &id654in_b = id3166out_value;

    id654out_result = (lt_float(id654in_a,id654in_b));
  }
  { // Node ID: 2868 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2868in_input = id654out_result;

    id2868out_output[(getCycle()+6)%7] = id2868in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id655out_output;

  { // Node ID: 655 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id655in_input = id648out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id655in_input_doubt = id648out_o_doubt;

    id655out_output = id655in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id656out_result;

  { // Node ID: 656 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id656in_a = id2868out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id656in_b = id655out_output;

    HWOffsetFix<1,0,UNSIGNED> id656x_1;

    (id656x_1) = (and_fixed(id656in_a,id656in_b));
    id656out_result = (id656x_1);
  }
  { // Node ID: 2869 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2869in_input = id656out_result;

    id2869out_output[(getCycle()+2)%3] = id2869in_input;
  }
  { // Node ID: 3162 (NodeConstantRawBits)
  }
  { // Node ID: 2522 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2522in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2522in_b = id3162out_value;

    id2522out_result[(getCycle()+1)%2] = (eq_fixed(id2522in_a,id2522in_b));
  }
  HWFloat<8,12> id2708out_output;

  { // Node ID: 2708 (NodeStreamOffset)
    const HWFloat<8,12> &id2708in_input = id1673out_result;

    id2708out_output = id2708in_input;
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 45 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id45in_sel = id2522out_result[getCycle()%2];
    const HWFloat<8,12> &id45in_option0 = id2708out_output;
    const HWFloat<8,12> &id45in_option1 = id44out_j_in;

    HWFloat<8,12> id45x_1;

    switch((id45in_sel.getValueAsLong())) {
      case 0l:
        id45x_1 = id45in_option0;
        break;
      case 1l:
        id45x_1 = id45in_option1;
        break;
      default:
        id45x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id45out_result[(getCycle()+1)%2] = (id45x_1);
  }
  { // Node ID: 2895 (NodeFIFO)
    const HWFloat<8,12> &id2895in_input = id45out_result[getCycle()%2];

    id2895out_output[(getCycle()+9)%10] = id2895in_input;
  }
  { // Node ID: 3160 (NodeConstantRawBits)
  }
  HWFloat<8,12> id725out_result;

  { // Node ID: 725 (NodeAdd)
    const HWFloat<8,12> &id725in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id725in_b = id3160out_value;

    id725out_result = (add_float(id725in_a,id725in_b));
  }
  { // Node ID: 2694 (NodePO2FPMult)
    const HWFloat<8,12> &id2694in_floatIn = id725out_result;

    id2694out_floatOut[(getCycle()+1)%2] = (mul_float(id2694in_floatIn,(c_hw_flt_8_12_n0_25val)));
  }
  HWRawBits<8> id794out_result;

  { // Node ID: 794 (NodeSlice)
    const HWFloat<8,12> &id794in_a = id2694out_floatOut[getCycle()%2];

    id794out_result = (slice<11,8>(id794in_a));
  }
  { // Node ID: 795 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2523out_result;

  { // Node ID: 2523 (NodeEqInlined)
    const HWRawBits<8> &id2523in_a = id794out_result;
    const HWRawBits<8> &id2523in_b = id795out_value;

    id2523out_result = (eq_bits(id2523in_a,id2523in_b));
  }
  HWRawBits<11> id793out_result;

  { // Node ID: 793 (NodeSlice)
    const HWFloat<8,12> &id793in_a = id2694out_floatOut[getCycle()%2];

    id793out_result = (slice<0,11>(id793in_a));
  }
  { // Node ID: 3159 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2524out_result;

  { // Node ID: 2524 (NodeNeqInlined)
    const HWRawBits<11> &id2524in_a = id793out_result;
    const HWRawBits<11> &id2524in_b = id3159out_value;

    id2524out_result = (neq_bits(id2524in_a,id2524in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id799out_result;

  { // Node ID: 799 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id799in_a = id2523out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id799in_b = id2524out_result;

    HWOffsetFix<1,0,UNSIGNED> id799x_1;

    (id799x_1) = (and_fixed(id799in_a,id799in_b));
    id799out_result = (id799x_1);
  }
  { // Node ID: 2884 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2884in_input = id799out_result;

    id2884out_output[(getCycle()+8)%9] = id2884in_input;
  }
  { // Node ID: 728 (NodeConstantRawBits)
  }
  HWFloat<8,12> id729out_output;
  HWOffsetFix<1,0,UNSIGNED> id729out_output_doubt;

  { // Node ID: 729 (NodeDoubtBitOp)
    const HWFloat<8,12> &id729in_input = id2694out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id729in_doubt = id728out_value;

    id729out_output = id729in_input;
    id729out_output_doubt = id729in_doubt;
  }
  { // Node ID: 730 (NodeCast)
    const HWFloat<8,12> &id730in_i = id729out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id730in_i_doubt = id729out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id730x_1;

    id730out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id730in_i,(&(id730x_1))));
    id730out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id730x_1),(c_hw_fix_4_0_uns_bits))),id730in_i_doubt));
  }
  { // Node ID: 733 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id732out_result;
  HWOffsetFix<1,0,UNSIGNED> id732out_result_doubt;

  { // Node ID: 732 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id732in_a = id730out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id732in_a_doubt = id730out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id732in_b = id733out_value;

    HWOffsetFix<1,0,UNSIGNED> id732x_1;

    id732out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id732in_a,id732in_b,(&(id732x_1))));
    id732out_result_doubt = (or_fixed((neq_fixed((id732x_1),(c_hw_fix_1_0_uns_bits_1))),id732in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id734out_o;
  HWOffsetFix<1,0,UNSIGNED> id734out_o_doubt;

  { // Node ID: 734 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id734in_i = id732out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id734in_i_doubt = id732out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id734x_1;

    id734out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id734in_i,(&(id734x_1))));
    id734out_o_doubt = (or_fixed((neq_fixed((id734x_1),(c_hw_fix_1_0_uns_bits_1))),id734in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id743out_output;

  { // Node ID: 743 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id743in_input = id734out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id743in_input_doubt = id734out_o_doubt;

    id743out_output = id743in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id744out_o;

  { // Node ID: 744 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id744in_i = id743out_output;

    id744out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id744in_i));
  }
  { // Node ID: 2875 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2875in_input = id744out_o;

    id2875out_output[(getCycle()+2)%3] = id2875in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id745out_o;

  { // Node ID: 745 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id745in_i = id743out_output;

    id745out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id745in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id803out_output;

  { // Node ID: 803 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id803in_input = id745out_o;

    id803out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id803in_input))));
  }
  { // Node ID: 804 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id804in_addr = id803out_output;

    HWOffsetFix<12,-12,UNSIGNED> id804x_1;

    switch(((long)((id804in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id804x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id804x_1 = (id804sta_rom_store[(id804in_addr.getValueAsLong())]);
        break;
      default:
        id804x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id804out_dout[(getCycle()+2)%3] = (id804x_1);
  }
  { // Node ID: 749 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id746out_o;

  { // Node ID: 746 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id746in_i = id743out_output;

    id746out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id746in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id748out_result;

  { // Node ID: 748 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id748in_a = id749out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id748in_b = id746out_o;

    id748out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id748in_a,id748in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id750out_o;

  { // Node ID: 750 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id750in_i = id748out_result;

    id750out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id750in_i));
  }
  { // Node ID: 2876 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2876in_input = id750out_o;

    id2876out_output[(getCycle()+2)%3] = id2876in_input;
  }
  { // Node ID: 3157 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id736out_result;

  { // Node ID: 736 (NodeGt)
    const HWFloat<8,12> &id736in_a = id2694out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id736in_b = id3157out_value;

    id736out_result = (gt_float(id736in_a,id736in_b));
  }
  { // Node ID: 2878 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2878in_input = id736out_result;

    id2878out_output[(getCycle()+6)%7] = id2878in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id737out_output;

  { // Node ID: 737 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id737in_input = id734out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id737in_input_doubt = id734out_o_doubt;

    id737out_output = id737in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id738out_result;

  { // Node ID: 738 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id738in_a = id2878out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id738in_b = id737out_output;

    HWOffsetFix<1,0,UNSIGNED> id738x_1;

    (id738x_1) = (and_fixed(id738in_a,id738in_b));
    id738out_result = (id738x_1);
  }
  { // Node ID: 2879 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2879in_input = id738out_result;

    id2879out_output[(getCycle()+2)%3] = id2879in_input;
  }
  { // Node ID: 3156 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id740out_result;

  { // Node ID: 740 (NodeLt)
    const HWFloat<8,12> &id740in_a = id2694out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id740in_b = id3156out_value;

    id740out_result = (lt_float(id740in_a,id740in_b));
  }
  { // Node ID: 2880 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2880in_input = id740out_result;

    id2880out_output[(getCycle()+6)%7] = id2880in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id741out_output;

  { // Node ID: 741 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id741in_input = id734out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id741in_input_doubt = id734out_o_doubt;

    id741out_output = id741in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id742out_result;

  { // Node ID: 742 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id742in_a = id2880out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id742in_b = id741out_output;

    HWOffsetFix<1,0,UNSIGNED> id742x_1;

    (id742x_1) = (and_fixed(id742in_a,id742in_b));
    id742out_result = (id742x_1);
  }
  { // Node ID: 2881 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2881in_input = id742out_result;

    id2881out_output[(getCycle()+2)%3] = id2881in_input;
  }
  { // Node ID: 3153 (NodeConstantRawBits)
  }
  { // Node ID: 3152 (NodeConstantRawBits)
  }
  HWFloat<8,12> id808out_result;

  { // Node ID: 808 (NodeAdd)
    const HWFloat<8,12> &id808in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id808in_b = id3152out_value;

    id808out_result = (add_float(id808in_a,id808in_b));
  }
  HWFloat<8,12> id810out_result;

  { // Node ID: 810 (NodeMul)
    const HWFloat<8,12> &id810in_a = id3153out_value;
    const HWFloat<8,12> &id810in_b = id808out_result;

    id810out_result = (mul_float(id810in_a,id810in_b));
  }
  HWRawBits<8> id877out_result;

  { // Node ID: 877 (NodeSlice)
    const HWFloat<8,12> &id877in_a = id810out_result;

    id877out_result = (slice<11,8>(id877in_a));
  }
  { // Node ID: 878 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2530out_result;

  { // Node ID: 2530 (NodeEqInlined)
    const HWRawBits<8> &id2530in_a = id877out_result;
    const HWRawBits<8> &id2530in_b = id878out_value;

    id2530out_result = (eq_bits(id2530in_a,id2530in_b));
  }
  HWRawBits<11> id876out_result;

  { // Node ID: 876 (NodeSlice)
    const HWFloat<8,12> &id876in_a = id810out_result;

    id876out_result = (slice<0,11>(id876in_a));
  }
  { // Node ID: 3151 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2531out_result;

  { // Node ID: 2531 (NodeNeqInlined)
    const HWRawBits<11> &id2531in_a = id876out_result;
    const HWRawBits<11> &id2531in_b = id3151out_value;

    id2531out_result = (neq_bits(id2531in_a,id2531in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id882out_result;

  { // Node ID: 882 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id882in_a = id2530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id882in_b = id2531out_result;

    HWOffsetFix<1,0,UNSIGNED> id882x_1;

    (id882x_1) = (and_fixed(id882in_a,id882in_b));
    id882out_result = (id882x_1);
  }
  { // Node ID: 2894 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2894in_input = id882out_result;

    id2894out_output[(getCycle()+8)%9] = id2894in_input;
  }
  { // Node ID: 811 (NodeConstantRawBits)
  }
  HWFloat<8,12> id812out_output;
  HWOffsetFix<1,0,UNSIGNED> id812out_output_doubt;

  { // Node ID: 812 (NodeDoubtBitOp)
    const HWFloat<8,12> &id812in_input = id810out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id812in_doubt = id811out_value;

    id812out_output = id812in_input;
    id812out_output_doubt = id812in_doubt;
  }
  { // Node ID: 813 (NodeCast)
    const HWFloat<8,12> &id813in_i = id812out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id813in_i_doubt = id812out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id813x_1;

    id813out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id813in_i,(&(id813x_1))));
    id813out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id813x_1),(c_hw_fix_4_0_uns_bits))),id813in_i_doubt));
  }
  { // Node ID: 816 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id815out_result;
  HWOffsetFix<1,0,UNSIGNED> id815out_result_doubt;

  { // Node ID: 815 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id815in_a = id813out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id815in_a_doubt = id813out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id815in_b = id816out_value;

    HWOffsetFix<1,0,UNSIGNED> id815x_1;

    id815out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id815in_a,id815in_b,(&(id815x_1))));
    id815out_result_doubt = (or_fixed((neq_fixed((id815x_1),(c_hw_fix_1_0_uns_bits_1))),id815in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id817out_o;
  HWOffsetFix<1,0,UNSIGNED> id817out_o_doubt;

  { // Node ID: 817 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id817in_i = id815out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id817in_i_doubt = id815out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id817x_1;

    id817out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id817in_i,(&(id817x_1))));
    id817out_o_doubt = (or_fixed((neq_fixed((id817x_1),(c_hw_fix_1_0_uns_bits_1))),id817in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id826out_output;

  { // Node ID: 826 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id826in_input = id817out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id826in_input_doubt = id817out_o_doubt;

    id826out_output = id826in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id827out_o;

  { // Node ID: 827 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id827in_i = id826out_output;

    id827out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id827in_i));
  }
  { // Node ID: 2886 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2886in_input = id827out_o;

    id2886out_output[(getCycle()+2)%3] = id2886in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id828out_o;

  { // Node ID: 828 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id828in_i = id826out_output;

    id828out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id828in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id886out_output;

  { // Node ID: 886 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id886in_input = id828out_o;

    id886out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id886in_input))));
  }
  { // Node ID: 887 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id887in_addr = id886out_output;

    HWOffsetFix<12,-12,UNSIGNED> id887x_1;

    switch(((long)((id887in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id887x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id887x_1 = (id887sta_rom_store[(id887in_addr.getValueAsLong())]);
        break;
      default:
        id887x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id887out_dout[(getCycle()+2)%3] = (id887x_1);
  }
  HWOffsetFix<8,-14,UNSIGNED> id829out_o;

  { // Node ID: 829 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id829in_i = id826out_output;

    id829out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id829in_i));
  }
  { // Node ID: 2887 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2887in_input = id829out_o;

    id2887out_output[(getCycle()+2)%3] = id2887in_input;
  }
  { // Node ID: 3149 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id819out_result;

  { // Node ID: 819 (NodeGt)
    const HWFloat<8,12> &id819in_a = id810out_result;
    const HWFloat<8,12> &id819in_b = id3149out_value;

    id819out_result = (gt_float(id819in_a,id819in_b));
  }
  { // Node ID: 2888 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2888in_input = id819out_result;

    id2888out_output[(getCycle()+6)%7] = id2888in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id820out_output;

  { // Node ID: 820 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id820in_input = id817out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id820in_input_doubt = id817out_o_doubt;

    id820out_output = id820in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id821out_result;

  { // Node ID: 821 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id821in_a = id2888out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id821in_b = id820out_output;

    HWOffsetFix<1,0,UNSIGNED> id821x_1;

    (id821x_1) = (and_fixed(id821in_a,id821in_b));
    id821out_result = (id821x_1);
  }
  { // Node ID: 2889 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2889in_input = id821out_result;

    id2889out_output[(getCycle()+2)%3] = id2889in_input;
  }
  { // Node ID: 3148 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id823out_result;

  { // Node ID: 823 (NodeLt)
    const HWFloat<8,12> &id823in_a = id810out_result;
    const HWFloat<8,12> &id823in_b = id3148out_value;

    id823out_result = (lt_float(id823in_a,id823in_b));
  }
  { // Node ID: 2890 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2890in_input = id823out_result;

    id2890out_output[(getCycle()+6)%7] = id2890in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id824out_output;

  { // Node ID: 824 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id824in_input = id817out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id824in_input_doubt = id817out_o_doubt;

    id824out_output = id824in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id825out_result;

  { // Node ID: 825 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id825in_a = id2890out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id825in_b = id824out_output;

    HWOffsetFix<1,0,UNSIGNED> id825x_1;

    (id825x_1) = (and_fixed(id825in_a,id825in_b));
    id825out_result = (id825x_1);
  }
  { // Node ID: 2891 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2891in_input = id825out_result;

    id2891out_output[(getCycle()+2)%3] = id2891in_input;
  }
  { // Node ID: 3142 (NodeConstantRawBits)
  }
  { // Node ID: 3141 (NodeConstantRawBits)
  }
  HWFloat<8,12> id893out_result;

  { // Node ID: 893 (NodeAdd)
    const HWFloat<8,12> &id893in_a = id2723out_output[getCycle()%2];
    const HWFloat<8,12> &id893in_b = id3141out_value;

    id893out_result = (add_float(id893in_a,id893in_b));
  }
  HWFloat<8,12> id895out_result;

  { // Node ID: 895 (NodeMul)
    const HWFloat<8,12> &id895in_a = id3142out_value;
    const HWFloat<8,12> &id895in_b = id893out_result;

    id895out_result = (mul_float(id895in_a,id895in_b));
  }
  HWRawBits<8> id962out_result;

  { // Node ID: 962 (NodeSlice)
    const HWFloat<8,12> &id962in_a = id895out_result;

    id962out_result = (slice<11,8>(id962in_a));
  }
  { // Node ID: 963 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2537out_result;

  { // Node ID: 2537 (NodeEqInlined)
    const HWRawBits<8> &id2537in_a = id962out_result;
    const HWRawBits<8> &id2537in_b = id963out_value;

    id2537out_result = (eq_bits(id2537in_a,id2537in_b));
  }
  HWRawBits<11> id961out_result;

  { // Node ID: 961 (NodeSlice)
    const HWFloat<8,12> &id961in_a = id895out_result;

    id961out_result = (slice<0,11>(id961in_a));
  }
  { // Node ID: 3140 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2538out_result;

  { // Node ID: 2538 (NodeNeqInlined)
    const HWRawBits<11> &id2538in_a = id961out_result;
    const HWRawBits<11> &id2538in_b = id3140out_value;

    id2538out_result = (neq_bits(id2538in_a,id2538in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id967out_result;

  { // Node ID: 967 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id967in_a = id2537out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id967in_b = id2538out_result;

    HWOffsetFix<1,0,UNSIGNED> id967x_1;

    (id967x_1) = (and_fixed(id967in_a,id967in_b));
    id967out_result = (id967x_1);
  }
  { // Node ID: 2906 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2906in_input = id967out_result;

    id2906out_output[(getCycle()+8)%9] = id2906in_input;
  }
  { // Node ID: 896 (NodeConstantRawBits)
  }
  HWFloat<8,12> id897out_output;
  HWOffsetFix<1,0,UNSIGNED> id897out_output_doubt;

  { // Node ID: 897 (NodeDoubtBitOp)
    const HWFloat<8,12> &id897in_input = id895out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id897in_doubt = id896out_value;

    id897out_output = id897in_input;
    id897out_output_doubt = id897in_doubt;
  }
  { // Node ID: 898 (NodeCast)
    const HWFloat<8,12> &id898in_i = id897out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id898in_i_doubt = id897out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id898x_1;

    id898out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id898in_i,(&(id898x_1))));
    id898out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id898x_1),(c_hw_fix_4_0_uns_bits))),id898in_i_doubt));
  }
  { // Node ID: 901 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id900out_result;
  HWOffsetFix<1,0,UNSIGNED> id900out_result_doubt;

  { // Node ID: 900 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id900in_a = id898out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id900in_a_doubt = id898out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id900in_b = id901out_value;

    HWOffsetFix<1,0,UNSIGNED> id900x_1;

    id900out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id900in_a,id900in_b,(&(id900x_1))));
    id900out_result_doubt = (or_fixed((neq_fixed((id900x_1),(c_hw_fix_1_0_uns_bits_1))),id900in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id902out_o;
  HWOffsetFix<1,0,UNSIGNED> id902out_o_doubt;

  { // Node ID: 902 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id902in_i = id900out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id902in_i_doubt = id900out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id902x_1;

    id902out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id902in_i,(&(id902x_1))));
    id902out_o_doubt = (or_fixed((neq_fixed((id902x_1),(c_hw_fix_1_0_uns_bits_1))),id902in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id911out_output;

  { // Node ID: 911 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id911in_input = id902out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id911in_input_doubt = id902out_o_doubt;

    id911out_output = id911in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id912out_o;

  { // Node ID: 912 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id912in_i = id911out_output;

    id912out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id912in_i));
  }
  { // Node ID: 2897 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2897in_input = id912out_o;

    id2897out_output[(getCycle()+2)%3] = id2897in_input;
  }
  HWOffsetFix<6,-6,UNSIGNED> id913out_o;

  { // Node ID: 913 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id913in_i = id911out_output;

    id913out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id913in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id971out_output;

  { // Node ID: 971 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id971in_input = id913out_o;

    id971out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id971in_input))));
  }
  { // Node ID: 972 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id972in_addr = id971out_output;

    HWOffsetFix<12,-12,UNSIGNED> id972x_1;

    switch(((long)((id972in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id972x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id972x_1 = (id972sta_rom_store[(id972in_addr.getValueAsLong())]);
        break;
      default:
        id972x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id972out_dout[(getCycle()+2)%3] = (id972x_1);
  }
  { // Node ID: 917 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id914out_o;

  { // Node ID: 914 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id914in_i = id911out_output;

    id914out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id914in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id916out_result;

  { // Node ID: 916 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id916in_a = id917out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id916in_b = id914out_o;

    id916out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id916in_a,id916in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id918out_o;

  { // Node ID: 918 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id918in_i = id916out_result;

    id918out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id918in_i));
  }
  { // Node ID: 2898 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2898in_input = id918out_o;

    id2898out_output[(getCycle()+2)%3] = id2898in_input;
  }
  { // Node ID: 3138 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id904out_result;

  { // Node ID: 904 (NodeGt)
    const HWFloat<8,12> &id904in_a = id895out_result;
    const HWFloat<8,12> &id904in_b = id3138out_value;

    id904out_result = (gt_float(id904in_a,id904in_b));
  }
  { // Node ID: 2900 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2900in_input = id904out_result;

    id2900out_output[(getCycle()+6)%7] = id2900in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id905out_output;

  { // Node ID: 905 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id905in_input = id902out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id905in_input_doubt = id902out_o_doubt;

    id905out_output = id905in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id906out_result;

  { // Node ID: 906 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id906in_a = id2900out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id906in_b = id905out_output;

    HWOffsetFix<1,0,UNSIGNED> id906x_1;

    (id906x_1) = (and_fixed(id906in_a,id906in_b));
    id906out_result = (id906x_1);
  }
  { // Node ID: 2901 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2901in_input = id906out_result;

    id2901out_output[(getCycle()+2)%3] = id2901in_input;
  }
  { // Node ID: 3137 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id908out_result;

  { // Node ID: 908 (NodeLt)
    const HWFloat<8,12> &id908in_a = id895out_result;
    const HWFloat<8,12> &id908in_b = id3137out_value;

    id908out_result = (lt_float(id908in_a,id908in_b));
  }
  { // Node ID: 2902 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2902in_input = id908out_result;

    id2902out_output[(getCycle()+6)%7] = id2902in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id909out_output;

  { // Node ID: 909 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id909in_input = id902out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id909in_input_doubt = id902out_o_doubt;

    id909out_output = id909in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id910out_result;

  { // Node ID: 910 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id910in_a = id2902out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id910in_b = id909out_output;

    HWOffsetFix<1,0,UNSIGNED> id910x_1;

    (id910x_1) = (and_fixed(id910in_a,id910in_b));
    id910out_result = (id910x_1);
  }
  { // Node ID: 2903 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2903in_input = id910out_result;

    id2903out_output[(getCycle()+2)%3] = id2903in_input;
  }
  { // Node ID: 3132 (NodeConstantRawBits)
  }
  { // Node ID: 2544 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2544in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2544in_b = id3132out_value;

    id2544out_result[(getCycle()+1)%2] = (eq_fixed(id2544in_a,id2544in_b));
  }
  HWFloat<8,12> id2709out_output;

  { // Node ID: 2709 (NodeStreamOffset)
    const HWFloat<8,12> &id2709in_input = id2910out_output[getCycle()%2];

    id2709out_output = id2709in_input;
  }
  { // Node ID: 40 (NodeInputMappedReg)
  }
  { // Node ID: 41 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id41in_sel = id2544out_result[getCycle()%2];
    const HWFloat<8,12> &id41in_option0 = id2709out_output;
    const HWFloat<8,12> &id41in_option1 = id40out_d_in;

    HWFloat<8,12> id41x_1;

    switch((id41in_sel.getValueAsLong())) {
      case 0l:
        id41x_1 = id41in_option0;
        break;
      case 1l:
        id41x_1 = id41in_option1;
        break;
      default:
        id41x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id41out_result[(getCycle()+1)%2] = (id41x_1);
  }
  { // Node ID: 2931 (NodeFIFO)
    const HWFloat<8,12> &id2931in_input = id41out_result[getCycle()%2];

    id2931out_output[(getCycle()+8)%9] = id2931in_input;
  }
  { // Node ID: 3131 (NodeConstantRawBits)
  }
  { // Node ID: 3130 (NodeConstantRawBits)
  }
  { // Node ID: 3129 (NodeConstantRawBits)
  }
  HWFloat<8,12> id979out_result;

  { // Node ID: 979 (NodeSub)
    const HWFloat<8,12> &id979in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id979in_b = id3129out_value;

    id979out_result = (sub_float(id979in_a,id979in_b));
  }
  HWFloat<8,12> id981out_result;

  { // Node ID: 981 (NodeMul)
    const HWFloat<8,12> &id981in_a = id3130out_value;
    const HWFloat<8,12> &id981in_b = id979out_result;

    id981out_result = (mul_float(id981in_a,id981in_b));
  }
  HWRawBits<8> id1048out_result;

  { // Node ID: 1048 (NodeSlice)
    const HWFloat<8,12> &id1048in_a = id981out_result;

    id1048out_result = (slice<11,8>(id1048in_a));
  }
  { // Node ID: 1049 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2545out_result;

  { // Node ID: 2545 (NodeEqInlined)
    const HWRawBits<8> &id2545in_a = id1048out_result;
    const HWRawBits<8> &id2545in_b = id1049out_value;

    id2545out_result = (eq_bits(id2545in_a,id2545in_b));
  }
  HWRawBits<11> id1047out_result;

  { // Node ID: 1047 (NodeSlice)
    const HWFloat<8,12> &id1047in_a = id981out_result;

    id1047out_result = (slice<0,11>(id1047in_a));
  }
  { // Node ID: 3128 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2546out_result;

  { // Node ID: 2546 (NodeNeqInlined)
    const HWRawBits<11> &id2546in_a = id1047out_result;
    const HWRawBits<11> &id2546in_b = id3128out_value;

    id2546out_result = (neq_bits(id2546in_a,id2546in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1053out_result;

  { // Node ID: 1053 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1053in_a = id2545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1053in_b = id2546out_result;

    HWOffsetFix<1,0,UNSIGNED> id1053x_1;

    (id1053x_1) = (and_fixed(id1053in_a,id1053in_b));
    id1053out_result = (id1053x_1);
  }
  { // Node ID: 2920 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2920in_input = id1053out_result;

    id2920out_output[(getCycle()+8)%9] = id2920in_input;
  }
  { // Node ID: 982 (NodeConstantRawBits)
  }
  HWFloat<8,12> id983out_output;
  HWOffsetFix<1,0,UNSIGNED> id983out_output_doubt;

  { // Node ID: 983 (NodeDoubtBitOp)
    const HWFloat<8,12> &id983in_input = id981out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id983in_doubt = id982out_value;

    id983out_output = id983in_input;
    id983out_output_doubt = id983in_doubt;
  }
  { // Node ID: 984 (NodeCast)
    const HWFloat<8,12> &id984in_i = id983out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id984in_i_doubt = id983out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id984x_1;

    id984out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id984in_i,(&(id984x_1))));
    id984out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id984x_1),(c_hw_fix_4_0_uns_bits))),id984in_i_doubt));
  }
  { // Node ID: 987 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id986out_result;
  HWOffsetFix<1,0,UNSIGNED> id986out_result_doubt;

  { // Node ID: 986 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id986in_a = id984out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id986in_a_doubt = id984out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id986in_b = id987out_value;

    HWOffsetFix<1,0,UNSIGNED> id986x_1;

    id986out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id986in_a,id986in_b,(&(id986x_1))));
    id986out_result_doubt = (or_fixed((neq_fixed((id986x_1),(c_hw_fix_1_0_uns_bits_1))),id986in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id988out_o;
  HWOffsetFix<1,0,UNSIGNED> id988out_o_doubt;

  { // Node ID: 988 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id988in_i = id986out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id988in_i_doubt = id986out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id988x_1;

    id988out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id988in_i,(&(id988x_1))));
    id988out_o_doubt = (or_fixed((neq_fixed((id988x_1),(c_hw_fix_1_0_uns_bits_1))),id988in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id997out_output;

  { // Node ID: 997 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id997in_input = id988out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id997in_input_doubt = id988out_o_doubt;

    id997out_output = id997in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id998out_o;

  { // Node ID: 998 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id998in_i = id997out_output;

    id998out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id998in_i));
  }
  { // Node ID: 2911 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2911in_input = id998out_o;

    id2911out_output[(getCycle()+2)%3] = id2911in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1015out_o;

  { // Node ID: 1015 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1015in_i = id2911out_output[getCycle()%3];

    id1015out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1015in_i));
  }
  { // Node ID: 1018 (NodeConstantRawBits)
  }
  { // Node ID: 2648 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id999out_o;

  { // Node ID: 999 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id999in_i = id997out_output;

    id999out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id999in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1057out_output;

  { // Node ID: 1057 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1057in_input = id999out_o;

    id1057out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1057in_input))));
  }
  { // Node ID: 1058 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1058in_addr = id1057out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1058x_1;

    switch(((long)((id1058in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1058x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1058x_1 = (id1058sta_rom_store[(id1058in_addr.getValueAsLong())]);
        break;
      default:
        id1058x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1058out_dout[(getCycle()+2)%3] = (id1058x_1);
  }
  { // Node ID: 1003 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1000out_o;

  { // Node ID: 1000 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1000in_i = id997out_output;

    id1000out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1000in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1002out_result;

  { // Node ID: 1002 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1002in_a = id1003out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1002in_b = id1000out_o;

    id1002out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1002in_a,id1002in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1004out_o;

  { // Node ID: 1004 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1004in_i = id1002out_result;

    id1004out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1004in_i));
  }
  { // Node ID: 2912 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2912in_input = id1004out_o;

    id2912out_output[(getCycle()+2)%3] = id2912in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1005out_result;

  { // Node ID: 1005 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1005in_a = id1058out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1005in_b = id2912out_output[getCycle()%3];

    id1005out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1005in_a,id1005in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1006out_result;

  { // Node ID: 1006 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1006in_a = id2912out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1006in_b = id1058out_dout[getCycle()%3];

    id1006out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1006in_a,id1006in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1007out_result;

  { // Node ID: 1007 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1007in_a = id1005out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1007in_b = id1006out_result;

    id1007out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1007in_a,id1007in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1008out_o;

  { // Node ID: 1008 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1008in_i = id1007out_result;

    id1008out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1008in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1009out_o;

  { // Node ID: 1009 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1009in_i = id1008out_o;

    id1009out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1009in_i));
  }
  { // Node ID: 3127 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2547out_result;

  { // Node ID: 2547 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2547in_a = id1009out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2547in_b = id3127out_value;

    id2547out_result = (gte_fixed(id2547in_a,id2547in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2673out_result;

  { // Node ID: 2673 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2673in_a = id1015out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2673in_b = id1018out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2673in_c = id2648out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2673in_condb = id2547out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2673x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2673x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2673x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2673x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2673x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2673x_1 = id2673in_a;
        break;
      default:
        id2673x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2673in_condb.getValueAsLong())) {
      case 0l:
        id2673x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2673x_2 = id2673in_b;
        break;
      default:
        id2673x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2673x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2673x_3 = id2673in_c;
        break;
      default:
        id2673x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2673x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2673x_1),(id2673x_2))),(id2673x_3)));
    id2673out_result = (id2673x_4);
  }
  HWRawBits<1> id2548out_result;

  { // Node ID: 2548 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2548in_a = id2673out_result;

    id2548out_result = (slice<10,1>(id2548in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2549out_output;

  { // Node ID: 2549 (NodeReinterpret)
    const HWRawBits<1> &id2549in_input = id2548out_result;

    id2549out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2549in_input));
  }
  { // Node ID: 3126 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id990out_result;

  { // Node ID: 990 (NodeGt)
    const HWFloat<8,12> &id990in_a = id981out_result;
    const HWFloat<8,12> &id990in_b = id3126out_value;

    id990out_result = (gt_float(id990in_a,id990in_b));
  }
  { // Node ID: 2914 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2914in_input = id990out_result;

    id2914out_output[(getCycle()+6)%7] = id2914in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id991out_output;

  { // Node ID: 991 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id991in_input = id988out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id991in_input_doubt = id988out_o_doubt;

    id991out_output = id991in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id992out_result;

  { // Node ID: 992 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id992in_a = id2914out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id992in_b = id991out_output;

    HWOffsetFix<1,0,UNSIGNED> id992x_1;

    (id992x_1) = (and_fixed(id992in_a,id992in_b));
    id992out_result = (id992x_1);
  }
  { // Node ID: 2915 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2915in_input = id992out_result;

    id2915out_output[(getCycle()+2)%3] = id2915in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1024out_result;

  { // Node ID: 1024 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1024in_a = id2915out_output[getCycle()%3];

    id1024out_result = (not_fixed(id1024in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1025out_result;

  { // Node ID: 1025 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1025in_a = id2549out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1025in_b = id1024out_result;

    HWOffsetFix<1,0,UNSIGNED> id1025x_1;

    (id1025x_1) = (and_fixed(id1025in_a,id1025in_b));
    id1025out_result = (id1025x_1);
  }
  { // Node ID: 3125 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id994out_result;

  { // Node ID: 994 (NodeLt)
    const HWFloat<8,12> &id994in_a = id981out_result;
    const HWFloat<8,12> &id994in_b = id3125out_value;

    id994out_result = (lt_float(id994in_a,id994in_b));
  }
  { // Node ID: 2916 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2916in_input = id994out_result;

    id2916out_output[(getCycle()+6)%7] = id2916in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id995out_output;

  { // Node ID: 995 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id995in_input = id988out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id995in_input_doubt = id988out_o_doubt;

    id995out_output = id995in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id996out_result;

  { // Node ID: 996 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id996in_a = id2916out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id996in_b = id995out_output;

    HWOffsetFix<1,0,UNSIGNED> id996x_1;

    (id996x_1) = (and_fixed(id996in_a,id996in_b));
    id996out_result = (id996x_1);
  }
  { // Node ID: 2917 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2917in_input = id996out_result;

    id2917out_output[(getCycle()+2)%3] = id2917in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1026out_result;

  { // Node ID: 1026 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1026in_a = id1025out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1026in_b = id2917out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1026x_1;

    (id1026x_1) = (or_fixed(id1026in_a,id1026in_b));
    id1026out_result = (id1026x_1);
  }
  { // Node ID: 3124 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2550out_result;

  { // Node ID: 2550 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2550in_a = id2673out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2550in_b = id3124out_value;

    id2550out_result = (gte_fixed(id2550in_a,id2550in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1033out_result;

  { // Node ID: 1033 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1033in_a = id2917out_output[getCycle()%3];

    id1033out_result = (not_fixed(id1033in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1034out_result;

  { // Node ID: 1034 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1034in_a = id2550out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1034in_b = id1033out_result;

    HWOffsetFix<1,0,UNSIGNED> id1034x_1;

    (id1034x_1) = (and_fixed(id1034in_a,id1034in_b));
    id1034out_result = (id1034x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1035out_result;

  { // Node ID: 1035 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1035in_a = id1034out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1035in_b = id2915out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1035x_1;

    (id1035x_1) = (or_fixed(id1035in_a,id1035in_b));
    id1035out_result = (id1035x_1);
  }
  HWRawBits<2> id1036out_result;

  { // Node ID: 1036 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1036in_in0 = id1026out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1036in_in1 = id1035out_result;

    id1036out_result = (cat(id1036in_in0,id1036in_in1));
  }
  { // Node ID: 1028 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1027out_o;

  { // Node ID: 1027 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1027in_i = id2673out_result;

    id1027out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1027in_i));
  }
  { // Node ID: 1012 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1013out_result;

  { // Node ID: 1013 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1013in_sel = id2547out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1013in_option0 = id1009out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1013in_option1 = id1012out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1013x_1;

    switch((id1013in_sel.getValueAsLong())) {
      case 0l:
        id1013x_1 = id1013in_option0;
        break;
      case 1l:
        id1013x_1 = id1013in_option1;
        break;
      default:
        id1013x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1013out_result = (id1013x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1014out_o;

  { // Node ID: 1014 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1014in_i = id1013out_result;

    id1014out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1014in_i));
  }
  HWRawBits<20> id1029out_result;

  { // Node ID: 1029 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1029in_in0 = id1028out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1029in_in1 = id1027out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1029in_in2 = id1014out_o;

    id1029out_result = (cat((cat(id1029in_in0,id1029in_in1)),id1029in_in2));
  }
  HWFloat<8,12> id1030out_output;

  { // Node ID: 1030 (NodeReinterpret)
    const HWRawBits<20> &id1030in_input = id1029out_result;

    id1030out_output = (cast_bits2float<8,12>(id1030in_input));
  }
  { // Node ID: 1037 (NodeConstantRawBits)
  }
  { // Node ID: 1038 (NodeConstantRawBits)
  }
  { // Node ID: 1040 (NodeConstantRawBits)
  }
  HWRawBits<20> id2551out_result;

  { // Node ID: 2551 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2551in_in0 = id1037out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2551in_in1 = id1038out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2551in_in2 = id1040out_value;

    id2551out_result = (cat((cat(id2551in_in0,id2551in_in1)),id2551in_in2));
  }
  HWFloat<8,12> id1042out_output;

  { // Node ID: 1042 (NodeReinterpret)
    const HWRawBits<20> &id1042in_input = id2551out_result;

    id1042out_output = (cast_bits2float<8,12>(id1042in_input));
  }
  { // Node ID: 2411 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1045out_result;

  { // Node ID: 1045 (NodeMux)
    const HWRawBits<2> &id1045in_sel = id1036out_result;
    const HWFloat<8,12> &id1045in_option0 = id1030out_output;
    const HWFloat<8,12> &id1045in_option1 = id1042out_output;
    const HWFloat<8,12> &id1045in_option2 = id2411out_value;
    const HWFloat<8,12> &id1045in_option3 = id1042out_output;

    HWFloat<8,12> id1045x_1;

    switch((id1045in_sel.getValueAsLong())) {
      case 0l:
        id1045x_1 = id1045in_option0;
        break;
      case 1l:
        id1045x_1 = id1045in_option1;
        break;
      case 2l:
        id1045x_1 = id1045in_option2;
        break;
      case 3l:
        id1045x_1 = id1045in_option3;
        break;
      default:
        id1045x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1045out_result = (id1045x_1);
  }
  { // Node ID: 3123 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1055out_result;

  { // Node ID: 1055 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1055in_sel = id2920out_output[getCycle()%9];
    const HWFloat<8,12> &id1055in_option0 = id1045out_result;
    const HWFloat<8,12> &id1055in_option1 = id3123out_value;

    HWFloat<8,12> id1055x_1;

    switch((id1055in_sel.getValueAsLong())) {
      case 0l:
        id1055x_1 = id1055in_option0;
        break;
      case 1l:
        id1055x_1 = id1055in_option1;
        break;
      default:
        id1055x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1055out_result = (id1055x_1);
  }
  HWFloat<8,12> id1060out_result;

  { // Node ID: 1060 (NodeMul)
    const HWFloat<8,12> &id1060in_a = id3131out_value;
    const HWFloat<8,12> &id1060in_b = id1055out_result;

    id1060out_result = (mul_float(id1060in_a,id1060in_b));
  }
  { // Node ID: 3122 (NodeConstantRawBits)
  }
  { // Node ID: 3121 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1062out_result;

  { // Node ID: 1062 (NodeSub)
    const HWFloat<8,12> &id1062in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1062in_b = id3121out_value;

    id1062out_result = (sub_float(id1062in_a,id1062in_b));
  }
  HWFloat<8,12> id1064out_result;

  { // Node ID: 1064 (NodeMul)
    const HWFloat<8,12> &id1064in_a = id3122out_value;
    const HWFloat<8,12> &id1064in_b = id1062out_result;

    id1064out_result = (mul_float(id1064in_a,id1064in_b));
  }
  HWRawBits<8> id1131out_result;

  { // Node ID: 1131 (NodeSlice)
    const HWFloat<8,12> &id1131in_a = id1064out_result;

    id1131out_result = (slice<11,8>(id1131in_a));
  }
  { // Node ID: 1132 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2552out_result;

  { // Node ID: 2552 (NodeEqInlined)
    const HWRawBits<8> &id2552in_a = id1131out_result;
    const HWRawBits<8> &id2552in_b = id1132out_value;

    id2552out_result = (eq_bits(id2552in_a,id2552in_b));
  }
  HWRawBits<11> id1130out_result;

  { // Node ID: 1130 (NodeSlice)
    const HWFloat<8,12> &id1130in_a = id1064out_result;

    id1130out_result = (slice<0,11>(id1130in_a));
  }
  { // Node ID: 3120 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2553out_result;

  { // Node ID: 2553 (NodeNeqInlined)
    const HWRawBits<11> &id2553in_a = id1130out_result;
    const HWRawBits<11> &id2553in_b = id3120out_value;

    id2553out_result = (neq_bits(id2553in_a,id2553in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1136out_result;

  { // Node ID: 1136 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1136in_a = id2552out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1136in_b = id2553out_result;

    HWOffsetFix<1,0,UNSIGNED> id1136x_1;

    (id1136x_1) = (and_fixed(id1136in_a,id1136in_b));
    id1136out_result = (id1136x_1);
  }
  { // Node ID: 2930 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2930in_input = id1136out_result;

    id2930out_output[(getCycle()+8)%9] = id2930in_input;
  }
  { // Node ID: 1065 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1066out_output;
  HWOffsetFix<1,0,UNSIGNED> id1066out_output_doubt;

  { // Node ID: 1066 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1066in_input = id1064out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1066in_doubt = id1065out_value;

    id1066out_output = id1066in_input;
    id1066out_output_doubt = id1066in_doubt;
  }
  { // Node ID: 1067 (NodeCast)
    const HWFloat<8,12> &id1067in_i = id1066out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1067in_i_doubt = id1066out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1067x_1;

    id1067out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1067in_i,(&(id1067x_1))));
    id1067out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1067x_1),(c_hw_fix_4_0_uns_bits))),id1067in_i_doubt));
  }
  { // Node ID: 1070 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1069out_result;
  HWOffsetFix<1,0,UNSIGNED> id1069out_result_doubt;

  { // Node ID: 1069 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1069in_a = id1067out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1069in_a_doubt = id1067out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1069in_b = id1070out_value;

    HWOffsetFix<1,0,UNSIGNED> id1069x_1;

    id1069out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1069in_a,id1069in_b,(&(id1069x_1))));
    id1069out_result_doubt = (or_fixed((neq_fixed((id1069x_1),(c_hw_fix_1_0_uns_bits_1))),id1069in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1071out_o;
  HWOffsetFix<1,0,UNSIGNED> id1071out_o_doubt;

  { // Node ID: 1071 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1071in_i = id1069out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1071in_i_doubt = id1069out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1071x_1;

    id1071out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1071in_i,(&(id1071x_1))));
    id1071out_o_doubt = (or_fixed((neq_fixed((id1071x_1),(c_hw_fix_1_0_uns_bits_1))),id1071in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1080out_output;

  { // Node ID: 1080 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1080in_input = id1071out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1080in_input_doubt = id1071out_o_doubt;

    id1080out_output = id1080in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1081out_o;

  { // Node ID: 1081 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1081in_i = id1080out_output;

    id1081out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1081in_i));
  }
  { // Node ID: 2921 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2921in_input = id1081out_o;

    id2921out_output[(getCycle()+2)%3] = id2921in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1098out_o;

  { // Node ID: 1098 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1098in_i = id2921out_output[getCycle()%3];

    id1098out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1098in_i));
  }
  { // Node ID: 1101 (NodeConstantRawBits)
  }
  { // Node ID: 2650 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1082out_o;

  { // Node ID: 1082 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1082in_i = id1080out_output;

    id1082out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1082in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1140out_output;

  { // Node ID: 1140 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1140in_input = id1082out_o;

    id1140out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1140in_input))));
  }
  { // Node ID: 1141 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1141in_addr = id1140out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1141x_1;

    switch(((long)((id1141in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1141x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1141x_1 = (id1141sta_rom_store[(id1141in_addr.getValueAsLong())]);
        break;
      default:
        id1141x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1141out_dout[(getCycle()+2)%3] = (id1141x_1);
  }
  { // Node ID: 1086 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1083out_o;

  { // Node ID: 1083 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1083in_i = id1080out_output;

    id1083out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1083in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1085out_result;

  { // Node ID: 1085 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1085in_a = id1086out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1085in_b = id1083out_o;

    id1085out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1085in_a,id1085in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1087out_o;

  { // Node ID: 1087 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1087in_i = id1085out_result;

    id1087out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1087in_i));
  }
  { // Node ID: 2922 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2922in_input = id1087out_o;

    id2922out_output[(getCycle()+2)%3] = id2922in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1088out_result;

  { // Node ID: 1088 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1088in_a = id1141out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1088in_b = id2922out_output[getCycle()%3];

    id1088out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1088in_a,id1088in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1089out_result;

  { // Node ID: 1089 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1089in_a = id2922out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1089in_b = id1141out_dout[getCycle()%3];

    id1089out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1089in_a,id1089in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1090out_result;

  { // Node ID: 1090 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1090in_a = id1088out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1090in_b = id1089out_result;

    id1090out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1090in_a,id1090in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1091out_o;

  { // Node ID: 1091 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1091in_i = id1090out_result;

    id1091out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1091in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1092out_o;

  { // Node ID: 1092 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1092in_i = id1091out_o;

    id1092out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1092in_i));
  }
  { // Node ID: 3119 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2554out_result;

  { // Node ID: 2554 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2554in_a = id1092out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2554in_b = id3119out_value;

    id2554out_result = (gte_fixed(id2554in_a,id2554in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2672out_result;

  { // Node ID: 2672 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2672in_a = id1098out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2672in_b = id1101out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2672in_c = id2650out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2672in_condb = id2554out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2672x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2672x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2672x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2672x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2672x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2672x_1 = id2672in_a;
        break;
      default:
        id2672x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2672in_condb.getValueAsLong())) {
      case 0l:
        id2672x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2672x_2 = id2672in_b;
        break;
      default:
        id2672x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2672x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2672x_3 = id2672in_c;
        break;
      default:
        id2672x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2672x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2672x_1),(id2672x_2))),(id2672x_3)));
    id2672out_result = (id2672x_4);
  }
  HWRawBits<1> id2555out_result;

  { // Node ID: 2555 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2555in_a = id2672out_result;

    id2555out_result = (slice<10,1>(id2555in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2556out_output;

  { // Node ID: 2556 (NodeReinterpret)
    const HWRawBits<1> &id2556in_input = id2555out_result;

    id2556out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2556in_input));
  }
  { // Node ID: 3118 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1073out_result;

  { // Node ID: 1073 (NodeGt)
    const HWFloat<8,12> &id1073in_a = id1064out_result;
    const HWFloat<8,12> &id1073in_b = id3118out_value;

    id1073out_result = (gt_float(id1073in_a,id1073in_b));
  }
  { // Node ID: 2924 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2924in_input = id1073out_result;

    id2924out_output[(getCycle()+6)%7] = id2924in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1074out_output;

  { // Node ID: 1074 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1074in_input = id1071out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1074in_input_doubt = id1071out_o_doubt;

    id1074out_output = id1074in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1075out_result;

  { // Node ID: 1075 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1075in_a = id2924out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1075in_b = id1074out_output;

    HWOffsetFix<1,0,UNSIGNED> id1075x_1;

    (id1075x_1) = (and_fixed(id1075in_a,id1075in_b));
    id1075out_result = (id1075x_1);
  }
  { // Node ID: 2925 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2925in_input = id1075out_result;

    id2925out_output[(getCycle()+2)%3] = id2925in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1107out_result;

  { // Node ID: 1107 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1107in_a = id2925out_output[getCycle()%3];

    id1107out_result = (not_fixed(id1107in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1108out_result;

  { // Node ID: 1108 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1108in_a = id2556out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1108in_b = id1107out_result;

    HWOffsetFix<1,0,UNSIGNED> id1108x_1;

    (id1108x_1) = (and_fixed(id1108in_a,id1108in_b));
    id1108out_result = (id1108x_1);
  }
  { // Node ID: 3117 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1077out_result;

  { // Node ID: 1077 (NodeLt)
    const HWFloat<8,12> &id1077in_a = id1064out_result;
    const HWFloat<8,12> &id1077in_b = id3117out_value;

    id1077out_result = (lt_float(id1077in_a,id1077in_b));
  }
  { // Node ID: 2926 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2926in_input = id1077out_result;

    id2926out_output[(getCycle()+6)%7] = id2926in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1078out_output;

  { // Node ID: 1078 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1078in_input = id1071out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1078in_input_doubt = id1071out_o_doubt;

    id1078out_output = id1078in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1079out_result;

  { // Node ID: 1079 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1079in_a = id2926out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1079in_b = id1078out_output;

    HWOffsetFix<1,0,UNSIGNED> id1079x_1;

    (id1079x_1) = (and_fixed(id1079in_a,id1079in_b));
    id1079out_result = (id1079x_1);
  }
  { // Node ID: 2927 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2927in_input = id1079out_result;

    id2927out_output[(getCycle()+2)%3] = id2927in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1109out_result;

  { // Node ID: 1109 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1109in_a = id1108out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1109in_b = id2927out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1109x_1;

    (id1109x_1) = (or_fixed(id1109in_a,id1109in_b));
    id1109out_result = (id1109x_1);
  }
  { // Node ID: 3116 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2557out_result;

  { // Node ID: 2557 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2557in_a = id2672out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2557in_b = id3116out_value;

    id2557out_result = (gte_fixed(id2557in_a,id2557in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1116out_result;

  { // Node ID: 1116 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1116in_a = id2927out_output[getCycle()%3];

    id1116out_result = (not_fixed(id1116in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1117out_result;

  { // Node ID: 1117 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1117in_a = id2557out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1117in_b = id1116out_result;

    HWOffsetFix<1,0,UNSIGNED> id1117x_1;

    (id1117x_1) = (and_fixed(id1117in_a,id1117in_b));
    id1117out_result = (id1117x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1118out_result;

  { // Node ID: 1118 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_a = id1117out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_b = id2925out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1118x_1;

    (id1118x_1) = (or_fixed(id1118in_a,id1118in_b));
    id1118out_result = (id1118x_1);
  }
  HWRawBits<2> id1119out_result;

  { // Node ID: 1119 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_in0 = id1109out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_in1 = id1118out_result;

    id1119out_result = (cat(id1119in_in0,id1119in_in1));
  }
  { // Node ID: 1111 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1110out_o;

  { // Node ID: 1110 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1110in_i = id2672out_result;

    id1110out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1110in_i));
  }
  { // Node ID: 1095 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1096out_result;

  { // Node ID: 1096 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1096in_sel = id2554out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1096in_option0 = id1092out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1096in_option1 = id1095out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1096x_1;

    switch((id1096in_sel.getValueAsLong())) {
      case 0l:
        id1096x_1 = id1096in_option0;
        break;
      case 1l:
        id1096x_1 = id1096in_option1;
        break;
      default:
        id1096x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1096out_result = (id1096x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1097out_o;

  { // Node ID: 1097 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1097in_i = id1096out_result;

    id1097out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1097in_i));
  }
  HWRawBits<20> id1112out_result;

  { // Node ID: 1112 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1112in_in0 = id1111out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1112in_in1 = id1110out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1112in_in2 = id1097out_o;

    id1112out_result = (cat((cat(id1112in_in0,id1112in_in1)),id1112in_in2));
  }
  HWFloat<8,12> id1113out_output;

  { // Node ID: 1113 (NodeReinterpret)
    const HWRawBits<20> &id1113in_input = id1112out_result;

    id1113out_output = (cast_bits2float<8,12>(id1113in_input));
  }
  { // Node ID: 1120 (NodeConstantRawBits)
  }
  { // Node ID: 1121 (NodeConstantRawBits)
  }
  { // Node ID: 1123 (NodeConstantRawBits)
  }
  HWRawBits<20> id2558out_result;

  { // Node ID: 2558 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2558in_in0 = id1120out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2558in_in1 = id1121out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2558in_in2 = id1123out_value;

    id2558out_result = (cat((cat(id2558in_in0,id2558in_in1)),id2558in_in2));
  }
  HWFloat<8,12> id1125out_output;

  { // Node ID: 1125 (NodeReinterpret)
    const HWRawBits<20> &id1125in_input = id2558out_result;

    id1125out_output = (cast_bits2float<8,12>(id1125in_input));
  }
  { // Node ID: 2412 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1128out_result;

  { // Node ID: 1128 (NodeMux)
    const HWRawBits<2> &id1128in_sel = id1119out_result;
    const HWFloat<8,12> &id1128in_option0 = id1113out_output;
    const HWFloat<8,12> &id1128in_option1 = id1125out_output;
    const HWFloat<8,12> &id1128in_option2 = id2412out_value;
    const HWFloat<8,12> &id1128in_option3 = id1125out_output;

    HWFloat<8,12> id1128x_1;

    switch((id1128in_sel.getValueAsLong())) {
      case 0l:
        id1128x_1 = id1128in_option0;
        break;
      case 1l:
        id1128x_1 = id1128in_option1;
        break;
      case 2l:
        id1128x_1 = id1128in_option2;
        break;
      case 3l:
        id1128x_1 = id1128in_option3;
        break;
      default:
        id1128x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1128out_result = (id1128x_1);
  }
  { // Node ID: 3115 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1138out_result;

  { // Node ID: 1138 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1138in_sel = id2930out_output[getCycle()%9];
    const HWFloat<8,12> &id1138in_option0 = id1128out_result;
    const HWFloat<8,12> &id1138in_option1 = id3115out_value;

    HWFloat<8,12> id1138x_1;

    switch((id1138in_sel.getValueAsLong())) {
      case 0l:
        id1138x_1 = id1138in_option0;
        break;
      case 1l:
        id1138x_1 = id1138in_option1;
        break;
      default:
        id1138x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1138out_result = (id1138x_1);
  }
  { // Node ID: 3114 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1143out_result;

  { // Node ID: 1143 (NodeAdd)
    const HWFloat<8,12> &id1143in_a = id1138out_result;
    const HWFloat<8,12> &id1143in_b = id3114out_value;

    id1143out_result = (add_float(id1143in_a,id1143in_b));
  }
  HWFloat<8,12> id1144out_result;

  { // Node ID: 1144 (NodeDiv)
    const HWFloat<8,12> &id1144in_a = id1060out_result;
    const HWFloat<8,12> &id1144in_b = id1143out_result;

    id1144out_result = (div_float(id1144in_a,id1144in_b));
  }
  HWFloat<8,12> id1145out_result;

  { // Node ID: 1145 (NodeMul)
    const HWFloat<8,12> &id1145in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id1145in_b = id1144out_result;

    id1145out_result = (mul_float(id1145in_a,id1145in_b));
  }
  { // Node ID: 3113 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1675out_result;

  { // Node ID: 1675 (NodeSub)
    const HWFloat<8,12> &id1675in_a = id3113out_value;
    const HWFloat<8,12> &id1675in_b = id2931out_output[getCycle()%9];

    id1675out_result = (sub_float(id1675in_a,id1675in_b));
  }
  HWFloat<8,12> id1676out_result;

  { // Node ID: 1676 (NodeMul)
    const HWFloat<8,12> &id1676in_a = id1145out_result;
    const HWFloat<8,12> &id1676in_b = id1675out_result;

    id1676out_result = (mul_float(id1676in_a,id1676in_b));
  }
  { // Node ID: 3112 (NodeConstantRawBits)
  }
  { // Node ID: 3111 (NodeConstantRawBits)
  }
  { // Node ID: 3110 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1147out_result;

  { // Node ID: 1147 (NodeAdd)
    const HWFloat<8,12> &id1147in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1147in_b = id3110out_value;

    id1147out_result = (add_float(id1147in_a,id1147in_b));
  }
  HWFloat<8,12> id1149out_result;

  { // Node ID: 1149 (NodeMul)
    const HWFloat<8,12> &id1149in_a = id3111out_value;
    const HWFloat<8,12> &id1149in_b = id1147out_result;

    id1149out_result = (mul_float(id1149in_a,id1149in_b));
  }
  HWRawBits<8> id1216out_result;

  { // Node ID: 1216 (NodeSlice)
    const HWFloat<8,12> &id1216in_a = id1149out_result;

    id1216out_result = (slice<11,8>(id1216in_a));
  }
  { // Node ID: 1217 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2559out_result;

  { // Node ID: 2559 (NodeEqInlined)
    const HWRawBits<8> &id2559in_a = id1216out_result;
    const HWRawBits<8> &id2559in_b = id1217out_value;

    id2559out_result = (eq_bits(id2559in_a,id2559in_b));
  }
  HWRawBits<11> id1215out_result;

  { // Node ID: 1215 (NodeSlice)
    const HWFloat<8,12> &id1215in_a = id1149out_result;

    id1215out_result = (slice<0,11>(id1215in_a));
  }
  { // Node ID: 3109 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2560out_result;

  { // Node ID: 2560 (NodeNeqInlined)
    const HWRawBits<11> &id2560in_a = id1215out_result;
    const HWRawBits<11> &id2560in_b = id3109out_value;

    id2560out_result = (neq_bits(id2560in_a,id2560in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1221out_result;

  { // Node ID: 1221 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1221in_a = id2559out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1221in_b = id2560out_result;

    HWOffsetFix<1,0,UNSIGNED> id1221x_1;

    (id1221x_1) = (and_fixed(id1221in_a,id1221in_b));
    id1221out_result = (id1221x_1);
  }
  { // Node ID: 2941 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2941in_input = id1221out_result;

    id2941out_output[(getCycle()+8)%9] = id2941in_input;
  }
  { // Node ID: 1150 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1151out_output;
  HWOffsetFix<1,0,UNSIGNED> id1151out_output_doubt;

  { // Node ID: 1151 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1151in_input = id1149out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1151in_doubt = id1150out_value;

    id1151out_output = id1151in_input;
    id1151out_output_doubt = id1151in_doubt;
  }
  { // Node ID: 1152 (NodeCast)
    const HWFloat<8,12> &id1152in_i = id1151out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1152in_i_doubt = id1151out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1152x_1;

    id1152out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1152in_i,(&(id1152x_1))));
    id1152out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1152x_1),(c_hw_fix_4_0_uns_bits))),id1152in_i_doubt));
  }
  { // Node ID: 1155 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1154out_result;
  HWOffsetFix<1,0,UNSIGNED> id1154out_result_doubt;

  { // Node ID: 1154 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1154in_a = id1152out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1154in_a_doubt = id1152out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1154in_b = id1155out_value;

    HWOffsetFix<1,0,UNSIGNED> id1154x_1;

    id1154out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1154in_a,id1154in_b,(&(id1154x_1))));
    id1154out_result_doubt = (or_fixed((neq_fixed((id1154x_1),(c_hw_fix_1_0_uns_bits_1))),id1154in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1156out_o;
  HWOffsetFix<1,0,UNSIGNED> id1156out_o_doubt;

  { // Node ID: 1156 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1156in_i = id1154out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1156in_i_doubt = id1154out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1156x_1;

    id1156out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1156in_i,(&(id1156x_1))));
    id1156out_o_doubt = (or_fixed((neq_fixed((id1156x_1),(c_hw_fix_1_0_uns_bits_1))),id1156in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1165out_output;

  { // Node ID: 1165 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1165in_input = id1156out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1165in_input_doubt = id1156out_o_doubt;

    id1165out_output = id1165in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1166out_o;

  { // Node ID: 1166 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1166in_i = id1165out_output;

    id1166out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1166in_i));
  }
  { // Node ID: 2932 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2932in_input = id1166out_o;

    id2932out_output[(getCycle()+2)%3] = id2932in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1183out_o;

  { // Node ID: 1183 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1183in_i = id2932out_output[getCycle()%3];

    id1183out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1183in_i));
  }
  { // Node ID: 1186 (NodeConstantRawBits)
  }
  { // Node ID: 2652 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1167out_o;

  { // Node ID: 1167 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1167in_i = id1165out_output;

    id1167out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1167in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1225out_output;

  { // Node ID: 1225 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1225in_input = id1167out_o;

    id1225out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1225in_input))));
  }
  { // Node ID: 1226 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1226in_addr = id1225out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1226x_1;

    switch(((long)((id1226in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1226x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1226x_1 = (id1226sta_rom_store[(id1226in_addr.getValueAsLong())]);
        break;
      default:
        id1226x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1226out_dout[(getCycle()+2)%3] = (id1226x_1);
  }
  { // Node ID: 1171 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1168out_o;

  { // Node ID: 1168 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1168in_i = id1165out_output;

    id1168out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1168in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1170out_result;

  { // Node ID: 1170 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1170in_a = id1171out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1170in_b = id1168out_o;

    id1170out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1170in_a,id1170in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1172out_o;

  { // Node ID: 1172 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1172in_i = id1170out_result;

    id1172out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1172in_i));
  }
  { // Node ID: 2933 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2933in_input = id1172out_o;

    id2933out_output[(getCycle()+2)%3] = id2933in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1173out_result;

  { // Node ID: 1173 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1173in_a = id1226out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1173in_b = id2933out_output[getCycle()%3];

    id1173out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1173in_a,id1173in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1174out_result;

  { // Node ID: 1174 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1174in_a = id2933out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1174in_b = id1226out_dout[getCycle()%3];

    id1174out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1174in_a,id1174in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1175out_result;

  { // Node ID: 1175 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1175in_a = id1173out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1175in_b = id1174out_result;

    id1175out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1175in_a,id1175in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1176out_o;

  { // Node ID: 1176 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1176in_i = id1175out_result;

    id1176out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1176in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1177out_o;

  { // Node ID: 1177 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1177in_i = id1176out_o;

    id1177out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1177in_i));
  }
  { // Node ID: 3108 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2561out_result;

  { // Node ID: 2561 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2561in_a = id1177out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2561in_b = id3108out_value;

    id2561out_result = (gte_fixed(id2561in_a,id2561in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2671out_result;

  { // Node ID: 2671 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2671in_a = id1183out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2671in_b = id1186out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2671in_c = id2652out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2671in_condb = id2561out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2671x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2671x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2671x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2671x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2671x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2671x_1 = id2671in_a;
        break;
      default:
        id2671x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2671in_condb.getValueAsLong())) {
      case 0l:
        id2671x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2671x_2 = id2671in_b;
        break;
      default:
        id2671x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2671x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2671x_3 = id2671in_c;
        break;
      default:
        id2671x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2671x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2671x_1),(id2671x_2))),(id2671x_3)));
    id2671out_result = (id2671x_4);
  }
  HWRawBits<1> id2562out_result;

  { // Node ID: 2562 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2562in_a = id2671out_result;

    id2562out_result = (slice<10,1>(id2562in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2563out_output;

  { // Node ID: 2563 (NodeReinterpret)
    const HWRawBits<1> &id2563in_input = id2562out_result;

    id2563out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2563in_input));
  }
  { // Node ID: 3107 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1158out_result;

  { // Node ID: 1158 (NodeGt)
    const HWFloat<8,12> &id1158in_a = id1149out_result;
    const HWFloat<8,12> &id1158in_b = id3107out_value;

    id1158out_result = (gt_float(id1158in_a,id1158in_b));
  }
  { // Node ID: 2935 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2935in_input = id1158out_result;

    id2935out_output[(getCycle()+6)%7] = id2935in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1159out_output;

  { // Node ID: 1159 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1159in_input = id1156out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1159in_input_doubt = id1156out_o_doubt;

    id1159out_output = id1159in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1160out_result;

  { // Node ID: 1160 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1160in_a = id2935out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1160in_b = id1159out_output;

    HWOffsetFix<1,0,UNSIGNED> id1160x_1;

    (id1160x_1) = (and_fixed(id1160in_a,id1160in_b));
    id1160out_result = (id1160x_1);
  }
  { // Node ID: 2936 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2936in_input = id1160out_result;

    id2936out_output[(getCycle()+2)%3] = id2936in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1192out_result;

  { // Node ID: 1192 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1192in_a = id2936out_output[getCycle()%3];

    id1192out_result = (not_fixed(id1192in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1193out_result;

  { // Node ID: 1193 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1193in_a = id2563out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1193in_b = id1192out_result;

    HWOffsetFix<1,0,UNSIGNED> id1193x_1;

    (id1193x_1) = (and_fixed(id1193in_a,id1193in_b));
    id1193out_result = (id1193x_1);
  }
  { // Node ID: 3106 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1162out_result;

  { // Node ID: 1162 (NodeLt)
    const HWFloat<8,12> &id1162in_a = id1149out_result;
    const HWFloat<8,12> &id1162in_b = id3106out_value;

    id1162out_result = (lt_float(id1162in_a,id1162in_b));
  }
  { // Node ID: 2937 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2937in_input = id1162out_result;

    id2937out_output[(getCycle()+6)%7] = id2937in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1163out_output;

  { // Node ID: 1163 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1163in_input = id1156out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1163in_input_doubt = id1156out_o_doubt;

    id1163out_output = id1163in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1164out_result;

  { // Node ID: 1164 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1164in_a = id2937out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1164in_b = id1163out_output;

    HWOffsetFix<1,0,UNSIGNED> id1164x_1;

    (id1164x_1) = (and_fixed(id1164in_a,id1164in_b));
    id1164out_result = (id1164x_1);
  }
  { // Node ID: 2938 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2938in_input = id1164out_result;

    id2938out_output[(getCycle()+2)%3] = id2938in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1194out_result;

  { // Node ID: 1194 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1194in_a = id1193out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1194in_b = id2938out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1194x_1;

    (id1194x_1) = (or_fixed(id1194in_a,id1194in_b));
    id1194out_result = (id1194x_1);
  }
  { // Node ID: 3105 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2564out_result;

  { // Node ID: 2564 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2564in_a = id2671out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2564in_b = id3105out_value;

    id2564out_result = (gte_fixed(id2564in_a,id2564in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1201out_result;

  { // Node ID: 1201 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1201in_a = id2938out_output[getCycle()%3];

    id1201out_result = (not_fixed(id1201in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1202out_result;

  { // Node ID: 1202 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1202in_a = id2564out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1202in_b = id1201out_result;

    HWOffsetFix<1,0,UNSIGNED> id1202x_1;

    (id1202x_1) = (and_fixed(id1202in_a,id1202in_b));
    id1202out_result = (id1202x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1203out_result;

  { // Node ID: 1203 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1203in_a = id1202out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1203in_b = id2936out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1203x_1;

    (id1203x_1) = (or_fixed(id1203in_a,id1203in_b));
    id1203out_result = (id1203x_1);
  }
  HWRawBits<2> id1204out_result;

  { // Node ID: 1204 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1204in_in0 = id1194out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1204in_in1 = id1203out_result;

    id1204out_result = (cat(id1204in_in0,id1204in_in1));
  }
  { // Node ID: 1196 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1195out_o;

  { // Node ID: 1195 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1195in_i = id2671out_result;

    id1195out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1195in_i));
  }
  { // Node ID: 1180 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1181out_result;

  { // Node ID: 1181 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1181in_sel = id2561out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1181in_option0 = id1177out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1181in_option1 = id1180out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1181x_1;

    switch((id1181in_sel.getValueAsLong())) {
      case 0l:
        id1181x_1 = id1181in_option0;
        break;
      case 1l:
        id1181x_1 = id1181in_option1;
        break;
      default:
        id1181x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1181out_result = (id1181x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1182out_o;

  { // Node ID: 1182 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1182in_i = id1181out_result;

    id1182out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1182in_i));
  }
  HWRawBits<20> id1197out_result;

  { // Node ID: 1197 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1197in_in0 = id1196out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1197in_in1 = id1195out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1197in_in2 = id1182out_o;

    id1197out_result = (cat((cat(id1197in_in0,id1197in_in1)),id1197in_in2));
  }
  HWFloat<8,12> id1198out_output;

  { // Node ID: 1198 (NodeReinterpret)
    const HWRawBits<20> &id1198in_input = id1197out_result;

    id1198out_output = (cast_bits2float<8,12>(id1198in_input));
  }
  { // Node ID: 1205 (NodeConstantRawBits)
  }
  { // Node ID: 1206 (NodeConstantRawBits)
  }
  { // Node ID: 1208 (NodeConstantRawBits)
  }
  HWRawBits<20> id2565out_result;

  { // Node ID: 2565 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2565in_in0 = id1205out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2565in_in1 = id1206out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2565in_in2 = id1208out_value;

    id2565out_result = (cat((cat(id2565in_in0,id2565in_in1)),id2565in_in2));
  }
  HWFloat<8,12> id1210out_output;

  { // Node ID: 1210 (NodeReinterpret)
    const HWRawBits<20> &id1210in_input = id2565out_result;

    id1210out_output = (cast_bits2float<8,12>(id1210in_input));
  }
  { // Node ID: 2413 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1213out_result;

  { // Node ID: 1213 (NodeMux)
    const HWRawBits<2> &id1213in_sel = id1204out_result;
    const HWFloat<8,12> &id1213in_option0 = id1198out_output;
    const HWFloat<8,12> &id1213in_option1 = id1210out_output;
    const HWFloat<8,12> &id1213in_option2 = id2413out_value;
    const HWFloat<8,12> &id1213in_option3 = id1210out_output;

    HWFloat<8,12> id1213x_1;

    switch((id1213in_sel.getValueAsLong())) {
      case 0l:
        id1213x_1 = id1213in_option0;
        break;
      case 1l:
        id1213x_1 = id1213in_option1;
        break;
      case 2l:
        id1213x_1 = id1213in_option2;
        break;
      case 3l:
        id1213x_1 = id1213in_option3;
        break;
      default:
        id1213x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1213out_result = (id1213x_1);
  }
  { // Node ID: 3104 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1223out_result;

  { // Node ID: 1223 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1223in_sel = id2941out_output[getCycle()%9];
    const HWFloat<8,12> &id1223in_option0 = id1213out_result;
    const HWFloat<8,12> &id1223in_option1 = id3104out_value;

    HWFloat<8,12> id1223x_1;

    switch((id1223in_sel.getValueAsLong())) {
      case 0l:
        id1223x_1 = id1223in_option0;
        break;
      case 1l:
        id1223x_1 = id1223in_option1;
        break;
      default:
        id1223x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1223out_result = (id1223x_1);
  }
  HWFloat<8,12> id1228out_result;

  { // Node ID: 1228 (NodeMul)
    const HWFloat<8,12> &id1228in_a = id3112out_value;
    const HWFloat<8,12> &id1228in_b = id1223out_result;

    id1228out_result = (mul_float(id1228in_a,id1228in_b));
  }
  { // Node ID: 3103 (NodeConstantRawBits)
  }
  { // Node ID: 3102 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1230out_result;

  { // Node ID: 1230 (NodeAdd)
    const HWFloat<8,12> &id1230in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1230in_b = id3102out_value;

    id1230out_result = (add_float(id1230in_a,id1230in_b));
  }
  HWFloat<8,12> id1232out_result;

  { // Node ID: 1232 (NodeMul)
    const HWFloat<8,12> &id1232in_a = id3103out_value;
    const HWFloat<8,12> &id1232in_b = id1230out_result;

    id1232out_result = (mul_float(id1232in_a,id1232in_b));
  }
  HWRawBits<8> id1299out_result;

  { // Node ID: 1299 (NodeSlice)
    const HWFloat<8,12> &id1299in_a = id1232out_result;

    id1299out_result = (slice<11,8>(id1299in_a));
  }
  { // Node ID: 1300 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2566out_result;

  { // Node ID: 2566 (NodeEqInlined)
    const HWRawBits<8> &id2566in_a = id1299out_result;
    const HWRawBits<8> &id2566in_b = id1300out_value;

    id2566out_result = (eq_bits(id2566in_a,id2566in_b));
  }
  HWRawBits<11> id1298out_result;

  { // Node ID: 1298 (NodeSlice)
    const HWFloat<8,12> &id1298in_a = id1232out_result;

    id1298out_result = (slice<0,11>(id1298in_a));
  }
  { // Node ID: 3101 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2567out_result;

  { // Node ID: 2567 (NodeNeqInlined)
    const HWRawBits<11> &id2567in_a = id1298out_result;
    const HWRawBits<11> &id2567in_b = id3101out_value;

    id2567out_result = (neq_bits(id2567in_a,id2567in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1304out_result;

  { // Node ID: 1304 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1304in_a = id2566out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1304in_b = id2567out_result;

    HWOffsetFix<1,0,UNSIGNED> id1304x_1;

    (id1304x_1) = (and_fixed(id1304in_a,id1304in_b));
    id1304out_result = (id1304x_1);
  }
  { // Node ID: 2951 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2951in_input = id1304out_result;

    id2951out_output[(getCycle()+8)%9] = id2951in_input;
  }
  { // Node ID: 1233 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1234out_output;
  HWOffsetFix<1,0,UNSIGNED> id1234out_output_doubt;

  { // Node ID: 1234 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1234in_input = id1232out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1234in_doubt = id1233out_value;

    id1234out_output = id1234in_input;
    id1234out_output_doubt = id1234in_doubt;
  }
  { // Node ID: 1235 (NodeCast)
    const HWFloat<8,12> &id1235in_i = id1234out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1235in_i_doubt = id1234out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1235x_1;

    id1235out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1235in_i,(&(id1235x_1))));
    id1235out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1235x_1),(c_hw_fix_4_0_uns_bits))),id1235in_i_doubt));
  }
  { // Node ID: 1238 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1237out_result;
  HWOffsetFix<1,0,UNSIGNED> id1237out_result_doubt;

  { // Node ID: 1237 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1237in_a = id1235out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1237in_a_doubt = id1235out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1237in_b = id1238out_value;

    HWOffsetFix<1,0,UNSIGNED> id1237x_1;

    id1237out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1237in_a,id1237in_b,(&(id1237x_1))));
    id1237out_result_doubt = (or_fixed((neq_fixed((id1237x_1),(c_hw_fix_1_0_uns_bits_1))),id1237in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1239out_o;
  HWOffsetFix<1,0,UNSIGNED> id1239out_o_doubt;

  { // Node ID: 1239 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1239in_i = id1237out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1239in_i_doubt = id1237out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1239x_1;

    id1239out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1239in_i,(&(id1239x_1))));
    id1239out_o_doubt = (or_fixed((neq_fixed((id1239x_1),(c_hw_fix_1_0_uns_bits_1))),id1239in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1248out_output;

  { // Node ID: 1248 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1248in_input = id1239out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1248in_input_doubt = id1239out_o_doubt;

    id1248out_output = id1248in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1249out_o;

  { // Node ID: 1249 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1249in_i = id1248out_output;

    id1249out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1249in_i));
  }
  { // Node ID: 2942 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2942in_input = id1249out_o;

    id2942out_output[(getCycle()+2)%3] = id2942in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1266out_o;

  { // Node ID: 1266 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1266in_i = id2942out_output[getCycle()%3];

    id1266out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1266in_i));
  }
  { // Node ID: 1269 (NodeConstantRawBits)
  }
  { // Node ID: 2654 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1250out_o;

  { // Node ID: 1250 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1250in_i = id1248out_output;

    id1250out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1250in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1308out_output;

  { // Node ID: 1308 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1308in_input = id1250out_o;

    id1308out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1308in_input))));
  }
  { // Node ID: 1309 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1309in_addr = id1308out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1309x_1;

    switch(((long)((id1309in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1309x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1309x_1 = (id1309sta_rom_store[(id1309in_addr.getValueAsLong())]);
        break;
      default:
        id1309x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1309out_dout[(getCycle()+2)%3] = (id1309x_1);
  }
  { // Node ID: 1254 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1251out_o;

  { // Node ID: 1251 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1251in_i = id1248out_output;

    id1251out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1251in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1253out_result;

  { // Node ID: 1253 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1253in_a = id1254out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1253in_b = id1251out_o;

    id1253out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1253in_a,id1253in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1255out_o;

  { // Node ID: 1255 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1255in_i = id1253out_result;

    id1255out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1255in_i));
  }
  { // Node ID: 2943 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2943in_input = id1255out_o;

    id2943out_output[(getCycle()+2)%3] = id2943in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1256out_result;

  { // Node ID: 1256 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1256in_a = id1309out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1256in_b = id2943out_output[getCycle()%3];

    id1256out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1256in_a,id1256in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1257out_result;

  { // Node ID: 1257 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1257in_a = id2943out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1257in_b = id1309out_dout[getCycle()%3];

    id1257out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1257in_a,id1257in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1258out_result;

  { // Node ID: 1258 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1258in_a = id1256out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1258in_b = id1257out_result;

    id1258out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1258in_a,id1258in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1259out_o;

  { // Node ID: 1259 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1259in_i = id1258out_result;

    id1259out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1259in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1260out_o;

  { // Node ID: 1260 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1260in_i = id1259out_o;

    id1260out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1260in_i));
  }
  { // Node ID: 3100 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2568out_result;

  { // Node ID: 2568 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2568in_a = id1260out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2568in_b = id3100out_value;

    id2568out_result = (gte_fixed(id2568in_a,id2568in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2670out_result;

  { // Node ID: 2670 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2670in_a = id1266out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2670in_b = id1269out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2670in_c = id2654out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2670in_condb = id2568out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2670x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2670x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2670x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2670x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2670x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2670x_1 = id2670in_a;
        break;
      default:
        id2670x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2670in_condb.getValueAsLong())) {
      case 0l:
        id2670x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2670x_2 = id2670in_b;
        break;
      default:
        id2670x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2670x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2670x_3 = id2670in_c;
        break;
      default:
        id2670x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2670x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2670x_1),(id2670x_2))),(id2670x_3)));
    id2670out_result = (id2670x_4);
  }
  HWRawBits<1> id2569out_result;

  { // Node ID: 2569 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2569in_a = id2670out_result;

    id2569out_result = (slice<10,1>(id2569in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2570out_output;

  { // Node ID: 2570 (NodeReinterpret)
    const HWRawBits<1> &id2570in_input = id2569out_result;

    id2570out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2570in_input));
  }
  { // Node ID: 3099 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1241out_result;

  { // Node ID: 1241 (NodeGt)
    const HWFloat<8,12> &id1241in_a = id1232out_result;
    const HWFloat<8,12> &id1241in_b = id3099out_value;

    id1241out_result = (gt_float(id1241in_a,id1241in_b));
  }
  { // Node ID: 2945 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2945in_input = id1241out_result;

    id2945out_output[(getCycle()+6)%7] = id2945in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1242out_output;

  { // Node ID: 1242 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1242in_input = id1239out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1242in_input_doubt = id1239out_o_doubt;

    id1242out_output = id1242in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1243out_result;

  { // Node ID: 1243 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1243in_a = id2945out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1243in_b = id1242out_output;

    HWOffsetFix<1,0,UNSIGNED> id1243x_1;

    (id1243x_1) = (and_fixed(id1243in_a,id1243in_b));
    id1243out_result = (id1243x_1);
  }
  { // Node ID: 2946 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2946in_input = id1243out_result;

    id2946out_output[(getCycle()+2)%3] = id2946in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1275out_result;

  { // Node ID: 1275 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1275in_a = id2946out_output[getCycle()%3];

    id1275out_result = (not_fixed(id1275in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1276out_result;

  { // Node ID: 1276 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1276in_a = id2570out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1276in_b = id1275out_result;

    HWOffsetFix<1,0,UNSIGNED> id1276x_1;

    (id1276x_1) = (and_fixed(id1276in_a,id1276in_b));
    id1276out_result = (id1276x_1);
  }
  { // Node ID: 3098 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1245out_result;

  { // Node ID: 1245 (NodeLt)
    const HWFloat<8,12> &id1245in_a = id1232out_result;
    const HWFloat<8,12> &id1245in_b = id3098out_value;

    id1245out_result = (lt_float(id1245in_a,id1245in_b));
  }
  { // Node ID: 2947 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2947in_input = id1245out_result;

    id2947out_output[(getCycle()+6)%7] = id2947in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1246out_output;

  { // Node ID: 1246 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1246in_input = id1239out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1246in_input_doubt = id1239out_o_doubt;

    id1246out_output = id1246in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1247out_result;

  { // Node ID: 1247 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1247in_a = id2947out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1247in_b = id1246out_output;

    HWOffsetFix<1,0,UNSIGNED> id1247x_1;

    (id1247x_1) = (and_fixed(id1247in_a,id1247in_b));
    id1247out_result = (id1247x_1);
  }
  { // Node ID: 2948 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2948in_input = id1247out_result;

    id2948out_output[(getCycle()+2)%3] = id2948in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1277out_result;

  { // Node ID: 1277 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1277in_a = id1276out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1277in_b = id2948out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1277x_1;

    (id1277x_1) = (or_fixed(id1277in_a,id1277in_b));
    id1277out_result = (id1277x_1);
  }
  { // Node ID: 3097 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2571out_result;

  { // Node ID: 2571 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2571in_a = id2670out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2571in_b = id3097out_value;

    id2571out_result = (gte_fixed(id2571in_a,id2571in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1284out_result;

  { // Node ID: 1284 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1284in_a = id2948out_output[getCycle()%3];

    id1284out_result = (not_fixed(id1284in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1285out_result;

  { // Node ID: 1285 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1285in_a = id2571out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1285in_b = id1284out_result;

    HWOffsetFix<1,0,UNSIGNED> id1285x_1;

    (id1285x_1) = (and_fixed(id1285in_a,id1285in_b));
    id1285out_result = (id1285x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1286out_result;

  { // Node ID: 1286 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1286in_a = id1285out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1286in_b = id2946out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1286x_1;

    (id1286x_1) = (or_fixed(id1286in_a,id1286in_b));
    id1286out_result = (id1286x_1);
  }
  HWRawBits<2> id1287out_result;

  { // Node ID: 1287 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1287in_in0 = id1277out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1287in_in1 = id1286out_result;

    id1287out_result = (cat(id1287in_in0,id1287in_in1));
  }
  { // Node ID: 1279 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1278out_o;

  { // Node ID: 1278 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1278in_i = id2670out_result;

    id1278out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1278in_i));
  }
  { // Node ID: 1263 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1264out_result;

  { // Node ID: 1264 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1264in_sel = id2568out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1264in_option0 = id1260out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1264in_option1 = id1263out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1264x_1;

    switch((id1264in_sel.getValueAsLong())) {
      case 0l:
        id1264x_1 = id1264in_option0;
        break;
      case 1l:
        id1264x_1 = id1264in_option1;
        break;
      default:
        id1264x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1264out_result = (id1264x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1265out_o;

  { // Node ID: 1265 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1265in_i = id1264out_result;

    id1265out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1265in_i));
  }
  HWRawBits<20> id1280out_result;

  { // Node ID: 1280 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1280in_in0 = id1279out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1280in_in1 = id1278out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1280in_in2 = id1265out_o;

    id1280out_result = (cat((cat(id1280in_in0,id1280in_in1)),id1280in_in2));
  }
  HWFloat<8,12> id1281out_output;

  { // Node ID: 1281 (NodeReinterpret)
    const HWRawBits<20> &id1281in_input = id1280out_result;

    id1281out_output = (cast_bits2float<8,12>(id1281in_input));
  }
  { // Node ID: 1288 (NodeConstantRawBits)
  }
  { // Node ID: 1289 (NodeConstantRawBits)
  }
  { // Node ID: 1291 (NodeConstantRawBits)
  }
  HWRawBits<20> id2572out_result;

  { // Node ID: 2572 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2572in_in0 = id1288out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2572in_in1 = id1289out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2572in_in2 = id1291out_value;

    id2572out_result = (cat((cat(id2572in_in0,id2572in_in1)),id2572in_in2));
  }
  HWFloat<8,12> id1293out_output;

  { // Node ID: 1293 (NodeReinterpret)
    const HWRawBits<20> &id1293in_input = id2572out_result;

    id1293out_output = (cast_bits2float<8,12>(id1293in_input));
  }
  { // Node ID: 2414 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1296out_result;

  { // Node ID: 1296 (NodeMux)
    const HWRawBits<2> &id1296in_sel = id1287out_result;
    const HWFloat<8,12> &id1296in_option0 = id1281out_output;
    const HWFloat<8,12> &id1296in_option1 = id1293out_output;
    const HWFloat<8,12> &id1296in_option2 = id2414out_value;
    const HWFloat<8,12> &id1296in_option3 = id1293out_output;

    HWFloat<8,12> id1296x_1;

    switch((id1296in_sel.getValueAsLong())) {
      case 0l:
        id1296x_1 = id1296in_option0;
        break;
      case 1l:
        id1296x_1 = id1296in_option1;
        break;
      case 2l:
        id1296x_1 = id1296in_option2;
        break;
      case 3l:
        id1296x_1 = id1296in_option3;
        break;
      default:
        id1296x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1296out_result = (id1296x_1);
  }
  { // Node ID: 3096 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1306out_result;

  { // Node ID: 1306 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1306in_sel = id2951out_output[getCycle()%9];
    const HWFloat<8,12> &id1306in_option0 = id1296out_result;
    const HWFloat<8,12> &id1306in_option1 = id3096out_value;

    HWFloat<8,12> id1306x_1;

    switch((id1306in_sel.getValueAsLong())) {
      case 0l:
        id1306x_1 = id1306in_option0;
        break;
      case 1l:
        id1306x_1 = id1306in_option1;
        break;
      default:
        id1306x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1306out_result = (id1306x_1);
  }
  { // Node ID: 3095 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1311out_result;

  { // Node ID: 1311 (NodeAdd)
    const HWFloat<8,12> &id1311in_a = id1306out_result;
    const HWFloat<8,12> &id1311in_b = id3095out_value;

    id1311out_result = (add_float(id1311in_a,id1311in_b));
  }
  HWFloat<8,12> id1312out_result;

  { // Node ID: 1312 (NodeDiv)
    const HWFloat<8,12> &id1312in_a = id1228out_result;
    const HWFloat<8,12> &id1312in_b = id1311out_result;

    id1312out_result = (div_float(id1312in_a,id1312in_b));
  }
  HWFloat<8,12> id1313out_result;

  { // Node ID: 1313 (NodeMul)
    const HWFloat<8,12> &id1313in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id1313in_b = id1312out_result;

    id1313out_result = (mul_float(id1313in_a,id1313in_b));
  }
  HWFloat<8,12> id1677out_result;

  { // Node ID: 1677 (NodeMul)
    const HWFloat<8,12> &id1677in_a = id1313out_result;
    const HWFloat<8,12> &id1677in_b = id2931out_output[getCycle()%9];

    id1677out_result = (mul_float(id1677in_a,id1677in_b));
  }
  HWFloat<8,12> id1678out_result;

  { // Node ID: 1678 (NodeSub)
    const HWFloat<8,12> &id1678in_a = id1676out_result;
    const HWFloat<8,12> &id1678in_b = id1677out_result;

    id1678out_result = (sub_float(id1678in_a,id1678in_b));
  }
  HWFloat<8,12> id1679out_result;

  { // Node ID: 1679 (NodeAdd)
    const HWFloat<8,12> &id1679in_a = id2931out_output[getCycle()%9];
    const HWFloat<8,12> &id1679in_b = id1678out_result;

    id1679out_result = (add_float(id1679in_a,id1679in_b));
  }
  { // Node ID: 2910 (NodeFIFO)
    const HWFloat<8,12> &id2910in_input = id1679out_result;

    id2910out_output[(getCycle()+1)%2] = id2910in_input;
  }
  { // Node ID: 3094 (NodeConstantRawBits)
  }
  { // Node ID: 2573 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2573in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2573in_b = id3094out_value;

    id2573out_result[(getCycle()+1)%2] = (eq_fixed(id2573in_a,id2573in_b));
  }
  HWFloat<8,12> id2710out_output;

  { // Node ID: 2710 (NodeStreamOffset)
    const HWFloat<8,12> &id2710in_input = id2955out_output[getCycle()%2];

    id2710out_output = id2710in_input;
  }
  { // Node ID: 32 (NodeInputMappedReg)
  }
  { // Node ID: 33 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id33in_sel = id2573out_result[getCycle()%2];
    const HWFloat<8,12> &id33in_option0 = id2710out_output;
    const HWFloat<8,12> &id33in_option1 = id32out_f_in;

    HWFloat<8,12> id33x_1;

    switch((id33in_sel.getValueAsLong())) {
      case 0l:
        id33x_1 = id33in_option0;
        break;
      case 1l:
        id33x_1 = id33in_option1;
        break;
      default:
        id33x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id33out_result[(getCycle()+1)%2] = (id33x_1);
  }
  { // Node ID: 2976 (NodeFIFO)
    const HWFloat<8,12> &id2976in_input = id33out_result[getCycle()%2];

    id2976out_output[(getCycle()+8)%9] = id2976in_input;
  }
  { // Node ID: 3093 (NodeConstantRawBits)
  }
  { // Node ID: 3092 (NodeConstantRawBits)
  }
  { // Node ID: 3091 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1315out_result;

  { // Node ID: 1315 (NodeAdd)
    const HWFloat<8,12> &id1315in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1315in_b = id3091out_value;

    id1315out_result = (add_float(id1315in_a,id1315in_b));
  }
  HWFloat<8,12> id1317out_result;

  { // Node ID: 1317 (NodeMul)
    const HWFloat<8,12> &id1317in_a = id3092out_value;
    const HWFloat<8,12> &id1317in_b = id1315out_result;

    id1317out_result = (mul_float(id1317in_a,id1317in_b));
  }
  HWRawBits<8> id1384out_result;

  { // Node ID: 1384 (NodeSlice)
    const HWFloat<8,12> &id1384in_a = id1317out_result;

    id1384out_result = (slice<11,8>(id1384in_a));
  }
  { // Node ID: 1385 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2574out_result;

  { // Node ID: 2574 (NodeEqInlined)
    const HWRawBits<8> &id2574in_a = id1384out_result;
    const HWRawBits<8> &id2574in_b = id1385out_value;

    id2574out_result = (eq_bits(id2574in_a,id2574in_b));
  }
  HWRawBits<11> id1383out_result;

  { // Node ID: 1383 (NodeSlice)
    const HWFloat<8,12> &id1383in_a = id1317out_result;

    id1383out_result = (slice<0,11>(id1383in_a));
  }
  { // Node ID: 3090 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2575out_result;

  { // Node ID: 2575 (NodeNeqInlined)
    const HWRawBits<11> &id2575in_a = id1383out_result;
    const HWRawBits<11> &id2575in_b = id3090out_value;

    id2575out_result = (neq_bits(id2575in_a,id2575in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1389out_result;

  { // Node ID: 1389 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1389in_a = id2574out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1389in_b = id2575out_result;

    HWOffsetFix<1,0,UNSIGNED> id1389x_1;

    (id1389x_1) = (and_fixed(id1389in_a,id1389in_b));
    id1389out_result = (id1389x_1);
  }
  { // Node ID: 2965 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2965in_input = id1389out_result;

    id2965out_output[(getCycle()+8)%9] = id2965in_input;
  }
  { // Node ID: 1318 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1319out_output;
  HWOffsetFix<1,0,UNSIGNED> id1319out_output_doubt;

  { // Node ID: 1319 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1319in_input = id1317out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1319in_doubt = id1318out_value;

    id1319out_output = id1319in_input;
    id1319out_output_doubt = id1319in_doubt;
  }
  { // Node ID: 1320 (NodeCast)
    const HWFloat<8,12> &id1320in_i = id1319out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1320in_i_doubt = id1319out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1320x_1;

    id1320out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1320in_i,(&(id1320x_1))));
    id1320out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1320x_1),(c_hw_fix_4_0_uns_bits))),id1320in_i_doubt));
  }
  { // Node ID: 1323 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1322out_result;
  HWOffsetFix<1,0,UNSIGNED> id1322out_result_doubt;

  { // Node ID: 1322 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1322in_a = id1320out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1322in_a_doubt = id1320out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1322in_b = id1323out_value;

    HWOffsetFix<1,0,UNSIGNED> id1322x_1;

    id1322out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1322in_a,id1322in_b,(&(id1322x_1))));
    id1322out_result_doubt = (or_fixed((neq_fixed((id1322x_1),(c_hw_fix_1_0_uns_bits_1))),id1322in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1324out_o;
  HWOffsetFix<1,0,UNSIGNED> id1324out_o_doubt;

  { // Node ID: 1324 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1324in_i = id1322out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1324in_i_doubt = id1322out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1324x_1;

    id1324out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1324in_i,(&(id1324x_1))));
    id1324out_o_doubt = (or_fixed((neq_fixed((id1324x_1),(c_hw_fix_1_0_uns_bits_1))),id1324in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1333out_output;

  { // Node ID: 1333 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1333in_input = id1324out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1333in_input_doubt = id1324out_o_doubt;

    id1333out_output = id1333in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1334out_o;

  { // Node ID: 1334 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1334in_i = id1333out_output;

    id1334out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1334in_i));
  }
  { // Node ID: 2956 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2956in_input = id1334out_o;

    id2956out_output[(getCycle()+2)%3] = id2956in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1351out_o;

  { // Node ID: 1351 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1351in_i = id2956out_output[getCycle()%3];

    id1351out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1351in_i));
  }
  { // Node ID: 1354 (NodeConstantRawBits)
  }
  { // Node ID: 2656 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1335out_o;

  { // Node ID: 1335 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1335in_i = id1333out_output;

    id1335out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1335in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1393out_output;

  { // Node ID: 1393 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1393in_input = id1335out_o;

    id1393out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1393in_input))));
  }
  { // Node ID: 1394 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1394in_addr = id1393out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1394x_1;

    switch(((long)((id1394in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1394x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1394x_1 = (id1394sta_rom_store[(id1394in_addr.getValueAsLong())]);
        break;
      default:
        id1394x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1394out_dout[(getCycle()+2)%3] = (id1394x_1);
  }
  { // Node ID: 1339 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1336out_o;

  { // Node ID: 1336 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1336in_i = id1333out_output;

    id1336out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1336in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1338out_result;

  { // Node ID: 1338 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1338in_a = id1339out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1338in_b = id1336out_o;

    id1338out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1338in_a,id1338in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1340out_o;

  { // Node ID: 1340 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1340in_i = id1338out_result;

    id1340out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1340in_i));
  }
  { // Node ID: 2957 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2957in_input = id1340out_o;

    id2957out_output[(getCycle()+2)%3] = id2957in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1341out_result;

  { // Node ID: 1341 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1341in_a = id1394out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1341in_b = id2957out_output[getCycle()%3];

    id1341out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1341in_a,id1341in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1342out_result;

  { // Node ID: 1342 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1342in_a = id2957out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1342in_b = id1394out_dout[getCycle()%3];

    id1342out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1342in_a,id1342in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1343out_result;

  { // Node ID: 1343 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1343in_a = id1341out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1343in_b = id1342out_result;

    id1343out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1343in_a,id1343in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1344out_o;

  { // Node ID: 1344 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1344in_i = id1343out_result;

    id1344out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1344in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1345out_o;

  { // Node ID: 1345 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1345in_i = id1344out_o;

    id1345out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1345in_i));
  }
  { // Node ID: 3089 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2576out_result;

  { // Node ID: 2576 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2576in_a = id1345out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2576in_b = id3089out_value;

    id2576out_result = (gte_fixed(id2576in_a,id2576in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2669out_result;

  { // Node ID: 2669 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2669in_a = id1351out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2669in_b = id1354out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2669in_c = id2656out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2669in_condb = id2576out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2669x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2669x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2669x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2669x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2669x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2669x_1 = id2669in_a;
        break;
      default:
        id2669x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2669in_condb.getValueAsLong())) {
      case 0l:
        id2669x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2669x_2 = id2669in_b;
        break;
      default:
        id2669x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2669x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2669x_3 = id2669in_c;
        break;
      default:
        id2669x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2669x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2669x_1),(id2669x_2))),(id2669x_3)));
    id2669out_result = (id2669x_4);
  }
  HWRawBits<1> id2577out_result;

  { // Node ID: 2577 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2577in_a = id2669out_result;

    id2577out_result = (slice<10,1>(id2577in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2578out_output;

  { // Node ID: 2578 (NodeReinterpret)
    const HWRawBits<1> &id2578in_input = id2577out_result;

    id2578out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2578in_input));
  }
  { // Node ID: 3088 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1326out_result;

  { // Node ID: 1326 (NodeGt)
    const HWFloat<8,12> &id1326in_a = id1317out_result;
    const HWFloat<8,12> &id1326in_b = id3088out_value;

    id1326out_result = (gt_float(id1326in_a,id1326in_b));
  }
  { // Node ID: 2959 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2959in_input = id1326out_result;

    id2959out_output[(getCycle()+6)%7] = id2959in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1327out_output;

  { // Node ID: 1327 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1327in_input = id1324out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1327in_input_doubt = id1324out_o_doubt;

    id1327out_output = id1327in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1328out_result;

  { // Node ID: 1328 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1328in_a = id2959out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1328in_b = id1327out_output;

    HWOffsetFix<1,0,UNSIGNED> id1328x_1;

    (id1328x_1) = (and_fixed(id1328in_a,id1328in_b));
    id1328out_result = (id1328x_1);
  }
  { // Node ID: 2960 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2960in_input = id1328out_result;

    id2960out_output[(getCycle()+2)%3] = id2960in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1360out_result;

  { // Node ID: 1360 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1360in_a = id2960out_output[getCycle()%3];

    id1360out_result = (not_fixed(id1360in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1361out_result;

  { // Node ID: 1361 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1361in_a = id2578out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1361in_b = id1360out_result;

    HWOffsetFix<1,0,UNSIGNED> id1361x_1;

    (id1361x_1) = (and_fixed(id1361in_a,id1361in_b));
    id1361out_result = (id1361x_1);
  }
  { // Node ID: 3087 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1330out_result;

  { // Node ID: 1330 (NodeLt)
    const HWFloat<8,12> &id1330in_a = id1317out_result;
    const HWFloat<8,12> &id1330in_b = id3087out_value;

    id1330out_result = (lt_float(id1330in_a,id1330in_b));
  }
  { // Node ID: 2961 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2961in_input = id1330out_result;

    id2961out_output[(getCycle()+6)%7] = id2961in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1331out_output;

  { // Node ID: 1331 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1331in_input = id1324out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1331in_input_doubt = id1324out_o_doubt;

    id1331out_output = id1331in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1332out_result;

  { // Node ID: 1332 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1332in_a = id2961out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1332in_b = id1331out_output;

    HWOffsetFix<1,0,UNSIGNED> id1332x_1;

    (id1332x_1) = (and_fixed(id1332in_a,id1332in_b));
    id1332out_result = (id1332x_1);
  }
  { // Node ID: 2962 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2962in_input = id1332out_result;

    id2962out_output[(getCycle()+2)%3] = id2962in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1362out_result;

  { // Node ID: 1362 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1362in_a = id1361out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1362in_b = id2962out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1362x_1;

    (id1362x_1) = (or_fixed(id1362in_a,id1362in_b));
    id1362out_result = (id1362x_1);
  }
  { // Node ID: 3086 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2579out_result;

  { // Node ID: 2579 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2579in_a = id2669out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2579in_b = id3086out_value;

    id2579out_result = (gte_fixed(id2579in_a,id2579in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1369out_result;

  { // Node ID: 1369 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1369in_a = id2962out_output[getCycle()%3];

    id1369out_result = (not_fixed(id1369in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1370out_result;

  { // Node ID: 1370 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1370in_a = id2579out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1370in_b = id1369out_result;

    HWOffsetFix<1,0,UNSIGNED> id1370x_1;

    (id1370x_1) = (and_fixed(id1370in_a,id1370in_b));
    id1370out_result = (id1370x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1371out_result;

  { // Node ID: 1371 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1371in_a = id1370out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1371in_b = id2960out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1371x_1;

    (id1371x_1) = (or_fixed(id1371in_a,id1371in_b));
    id1371out_result = (id1371x_1);
  }
  HWRawBits<2> id1372out_result;

  { // Node ID: 1372 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1372in_in0 = id1362out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1372in_in1 = id1371out_result;

    id1372out_result = (cat(id1372in_in0,id1372in_in1));
  }
  { // Node ID: 1364 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1363out_o;

  { // Node ID: 1363 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1363in_i = id2669out_result;

    id1363out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1363in_i));
  }
  { // Node ID: 1348 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1349out_result;

  { // Node ID: 1349 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1349in_sel = id2576out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1349in_option0 = id1345out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1349in_option1 = id1348out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1349x_1;

    switch((id1349in_sel.getValueAsLong())) {
      case 0l:
        id1349x_1 = id1349in_option0;
        break;
      case 1l:
        id1349x_1 = id1349in_option1;
        break;
      default:
        id1349x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1349out_result = (id1349x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1350out_o;

  { // Node ID: 1350 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1350in_i = id1349out_result;

    id1350out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1350in_i));
  }
  HWRawBits<20> id1365out_result;

  { // Node ID: 1365 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1365in_in0 = id1364out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1365in_in1 = id1363out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1365in_in2 = id1350out_o;

    id1365out_result = (cat((cat(id1365in_in0,id1365in_in1)),id1365in_in2));
  }
  HWFloat<8,12> id1366out_output;

  { // Node ID: 1366 (NodeReinterpret)
    const HWRawBits<20> &id1366in_input = id1365out_result;

    id1366out_output = (cast_bits2float<8,12>(id1366in_input));
  }
  { // Node ID: 1373 (NodeConstantRawBits)
  }
  { // Node ID: 1374 (NodeConstantRawBits)
  }
  { // Node ID: 1376 (NodeConstantRawBits)
  }
  HWRawBits<20> id2580out_result;

  { // Node ID: 2580 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2580in_in0 = id1373out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2580in_in1 = id1374out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2580in_in2 = id1376out_value;

    id2580out_result = (cat((cat(id2580in_in0,id2580in_in1)),id2580in_in2));
  }
  HWFloat<8,12> id1378out_output;

  { // Node ID: 1378 (NodeReinterpret)
    const HWRawBits<20> &id1378in_input = id2580out_result;

    id1378out_output = (cast_bits2float<8,12>(id1378in_input));
  }
  { // Node ID: 2415 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1381out_result;

  { // Node ID: 1381 (NodeMux)
    const HWRawBits<2> &id1381in_sel = id1372out_result;
    const HWFloat<8,12> &id1381in_option0 = id1366out_output;
    const HWFloat<8,12> &id1381in_option1 = id1378out_output;
    const HWFloat<8,12> &id1381in_option2 = id2415out_value;
    const HWFloat<8,12> &id1381in_option3 = id1378out_output;

    HWFloat<8,12> id1381x_1;

    switch((id1381in_sel.getValueAsLong())) {
      case 0l:
        id1381x_1 = id1381in_option0;
        break;
      case 1l:
        id1381x_1 = id1381in_option1;
        break;
      case 2l:
        id1381x_1 = id1381in_option2;
        break;
      case 3l:
        id1381x_1 = id1381in_option3;
        break;
      default:
        id1381x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1381out_result = (id1381x_1);
  }
  { // Node ID: 3085 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1391out_result;

  { // Node ID: 1391 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1391in_sel = id2965out_output[getCycle()%9];
    const HWFloat<8,12> &id1391in_option0 = id1381out_result;
    const HWFloat<8,12> &id1391in_option1 = id3085out_value;

    HWFloat<8,12> id1391x_1;

    switch((id1391in_sel.getValueAsLong())) {
      case 0l:
        id1391x_1 = id1391in_option0;
        break;
      case 1l:
        id1391x_1 = id1391in_option1;
        break;
      default:
        id1391x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1391out_result = (id1391x_1);
  }
  HWFloat<8,12> id1396out_result;

  { // Node ID: 1396 (NodeMul)
    const HWFloat<8,12> &id1396in_a = id3093out_value;
    const HWFloat<8,12> &id1396in_b = id1391out_result;

    id1396out_result = (mul_float(id1396in_a,id1396in_b));
  }
  { // Node ID: 3084 (NodeConstantRawBits)
  }
  { // Node ID: 3083 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1398out_result;

  { // Node ID: 1398 (NodeAdd)
    const HWFloat<8,12> &id1398in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1398in_b = id3083out_value;

    id1398out_result = (add_float(id1398in_a,id1398in_b));
  }
  HWFloat<8,12> id1400out_result;

  { // Node ID: 1400 (NodeMul)
    const HWFloat<8,12> &id1400in_a = id3084out_value;
    const HWFloat<8,12> &id1400in_b = id1398out_result;

    id1400out_result = (mul_float(id1400in_a,id1400in_b));
  }
  HWRawBits<8> id1467out_result;

  { // Node ID: 1467 (NodeSlice)
    const HWFloat<8,12> &id1467in_a = id1400out_result;

    id1467out_result = (slice<11,8>(id1467in_a));
  }
  { // Node ID: 1468 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2581out_result;

  { // Node ID: 2581 (NodeEqInlined)
    const HWRawBits<8> &id2581in_a = id1467out_result;
    const HWRawBits<8> &id2581in_b = id1468out_value;

    id2581out_result = (eq_bits(id2581in_a,id2581in_b));
  }
  HWRawBits<11> id1466out_result;

  { // Node ID: 1466 (NodeSlice)
    const HWFloat<8,12> &id1466in_a = id1400out_result;

    id1466out_result = (slice<0,11>(id1466in_a));
  }
  { // Node ID: 3082 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2582out_result;

  { // Node ID: 2582 (NodeNeqInlined)
    const HWRawBits<11> &id2582in_a = id1466out_result;
    const HWRawBits<11> &id2582in_b = id3082out_value;

    id2582out_result = (neq_bits(id2582in_a,id2582in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1472out_result;

  { // Node ID: 1472 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1472in_a = id2581out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1472in_b = id2582out_result;

    HWOffsetFix<1,0,UNSIGNED> id1472x_1;

    (id1472x_1) = (and_fixed(id1472in_a,id1472in_b));
    id1472out_result = (id1472x_1);
  }
  { // Node ID: 2975 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2975in_input = id1472out_result;

    id2975out_output[(getCycle()+8)%9] = id2975in_input;
  }
  { // Node ID: 1401 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1402out_output;
  HWOffsetFix<1,0,UNSIGNED> id1402out_output_doubt;

  { // Node ID: 1402 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1402in_input = id1400out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1402in_doubt = id1401out_value;

    id1402out_output = id1402in_input;
    id1402out_output_doubt = id1402in_doubt;
  }
  { // Node ID: 1403 (NodeCast)
    const HWFloat<8,12> &id1403in_i = id1402out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1403in_i_doubt = id1402out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1403x_1;

    id1403out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1403in_i,(&(id1403x_1))));
    id1403out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1403x_1),(c_hw_fix_4_0_uns_bits))),id1403in_i_doubt));
  }
  { // Node ID: 1406 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1405out_result;
  HWOffsetFix<1,0,UNSIGNED> id1405out_result_doubt;

  { // Node ID: 1405 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1405in_a = id1403out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1405in_a_doubt = id1403out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1405in_b = id1406out_value;

    HWOffsetFix<1,0,UNSIGNED> id1405x_1;

    id1405out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1405in_a,id1405in_b,(&(id1405x_1))));
    id1405out_result_doubt = (or_fixed((neq_fixed((id1405x_1),(c_hw_fix_1_0_uns_bits_1))),id1405in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1407out_o;
  HWOffsetFix<1,0,UNSIGNED> id1407out_o_doubt;

  { // Node ID: 1407 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1407in_i = id1405out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1407in_i_doubt = id1405out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1407x_1;

    id1407out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1407in_i,(&(id1407x_1))));
    id1407out_o_doubt = (or_fixed((neq_fixed((id1407x_1),(c_hw_fix_1_0_uns_bits_1))),id1407in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1416out_output;

  { // Node ID: 1416 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1416in_input = id1407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1416in_input_doubt = id1407out_o_doubt;

    id1416out_output = id1416in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1417out_o;

  { // Node ID: 1417 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1417in_i = id1416out_output;

    id1417out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1417in_i));
  }
  { // Node ID: 2966 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2966in_input = id1417out_o;

    id2966out_output[(getCycle()+2)%3] = id2966in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1434out_o;

  { // Node ID: 1434 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1434in_i = id2966out_output[getCycle()%3];

    id1434out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1434in_i));
  }
  { // Node ID: 1437 (NodeConstantRawBits)
  }
  { // Node ID: 2658 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1418out_o;

  { // Node ID: 1418 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1418in_i = id1416out_output;

    id1418out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1418in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1476out_output;

  { // Node ID: 1476 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1476in_input = id1418out_o;

    id1476out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1476in_input))));
  }
  { // Node ID: 1477 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1477in_addr = id1476out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1477x_1;

    switch(((long)((id1477in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1477x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1477x_1 = (id1477sta_rom_store[(id1477in_addr.getValueAsLong())]);
        break;
      default:
        id1477x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1477out_dout[(getCycle()+2)%3] = (id1477x_1);
  }
  { // Node ID: 1422 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1419out_o;

  { // Node ID: 1419 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1419in_i = id1416out_output;

    id1419out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1419in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1421out_result;

  { // Node ID: 1421 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1421in_a = id1422out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1421in_b = id1419out_o;

    id1421out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1421in_a,id1421in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1423out_o;

  { // Node ID: 1423 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1423in_i = id1421out_result;

    id1423out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1423in_i));
  }
  { // Node ID: 2967 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2967in_input = id1423out_o;

    id2967out_output[(getCycle()+2)%3] = id2967in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1424out_result;

  { // Node ID: 1424 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1424in_a = id1477out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1424in_b = id2967out_output[getCycle()%3];

    id1424out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1424in_a,id1424in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1425out_result;

  { // Node ID: 1425 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1425in_a = id2967out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1425in_b = id1477out_dout[getCycle()%3];

    id1425out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1425in_a,id1425in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1426out_result;

  { // Node ID: 1426 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1426in_a = id1424out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1426in_b = id1425out_result;

    id1426out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1426in_a,id1426in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1427out_o;

  { // Node ID: 1427 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1427in_i = id1426out_result;

    id1427out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1427in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1428out_o;

  { // Node ID: 1428 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1428in_i = id1427out_o;

    id1428out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1428in_i));
  }
  { // Node ID: 3081 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2583out_result;

  { // Node ID: 2583 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2583in_a = id1428out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2583in_b = id3081out_value;

    id2583out_result = (gte_fixed(id2583in_a,id2583in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2668out_result;

  { // Node ID: 2668 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2668in_a = id1434out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2668in_b = id1437out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2668in_c = id2658out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2668in_condb = id2583out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2668x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2668x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2668x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2668x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2668x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2668x_1 = id2668in_a;
        break;
      default:
        id2668x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2668in_condb.getValueAsLong())) {
      case 0l:
        id2668x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2668x_2 = id2668in_b;
        break;
      default:
        id2668x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2668x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2668x_3 = id2668in_c;
        break;
      default:
        id2668x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2668x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2668x_1),(id2668x_2))),(id2668x_3)));
    id2668out_result = (id2668x_4);
  }
  HWRawBits<1> id2584out_result;

  { // Node ID: 2584 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2584in_a = id2668out_result;

    id2584out_result = (slice<10,1>(id2584in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2585out_output;

  { // Node ID: 2585 (NodeReinterpret)
    const HWRawBits<1> &id2585in_input = id2584out_result;

    id2585out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2585in_input));
  }
  { // Node ID: 3080 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1409out_result;

  { // Node ID: 1409 (NodeGt)
    const HWFloat<8,12> &id1409in_a = id1400out_result;
    const HWFloat<8,12> &id1409in_b = id3080out_value;

    id1409out_result = (gt_float(id1409in_a,id1409in_b));
  }
  { // Node ID: 2969 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2969in_input = id1409out_result;

    id2969out_output[(getCycle()+6)%7] = id2969in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1410out_output;

  { // Node ID: 1410 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1410in_input = id1407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1410in_input_doubt = id1407out_o_doubt;

    id1410out_output = id1410in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1411out_result;

  { // Node ID: 1411 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1411in_a = id2969out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1411in_b = id1410out_output;

    HWOffsetFix<1,0,UNSIGNED> id1411x_1;

    (id1411x_1) = (and_fixed(id1411in_a,id1411in_b));
    id1411out_result = (id1411x_1);
  }
  { // Node ID: 2970 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2970in_input = id1411out_result;

    id2970out_output[(getCycle()+2)%3] = id2970in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1443out_result;

  { // Node ID: 1443 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1443in_a = id2970out_output[getCycle()%3];

    id1443out_result = (not_fixed(id1443in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1444out_result;

  { // Node ID: 1444 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1444in_a = id2585out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1444in_b = id1443out_result;

    HWOffsetFix<1,0,UNSIGNED> id1444x_1;

    (id1444x_1) = (and_fixed(id1444in_a,id1444in_b));
    id1444out_result = (id1444x_1);
  }
  { // Node ID: 3079 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1413out_result;

  { // Node ID: 1413 (NodeLt)
    const HWFloat<8,12> &id1413in_a = id1400out_result;
    const HWFloat<8,12> &id1413in_b = id3079out_value;

    id1413out_result = (lt_float(id1413in_a,id1413in_b));
  }
  { // Node ID: 2971 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2971in_input = id1413out_result;

    id2971out_output[(getCycle()+6)%7] = id2971in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1414out_output;

  { // Node ID: 1414 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1414in_input = id1407out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1414in_input_doubt = id1407out_o_doubt;

    id1414out_output = id1414in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1415out_result;

  { // Node ID: 1415 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1415in_a = id2971out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1415in_b = id1414out_output;

    HWOffsetFix<1,0,UNSIGNED> id1415x_1;

    (id1415x_1) = (and_fixed(id1415in_a,id1415in_b));
    id1415out_result = (id1415x_1);
  }
  { // Node ID: 2972 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2972in_input = id1415out_result;

    id2972out_output[(getCycle()+2)%3] = id2972in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1445out_result;

  { // Node ID: 1445 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1445in_a = id1444out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1445in_b = id2972out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1445x_1;

    (id1445x_1) = (or_fixed(id1445in_a,id1445in_b));
    id1445out_result = (id1445x_1);
  }
  { // Node ID: 3078 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2586out_result;

  { // Node ID: 2586 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2586in_a = id2668out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2586in_b = id3078out_value;

    id2586out_result = (gte_fixed(id2586in_a,id2586in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1452out_result;

  { // Node ID: 1452 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1452in_a = id2972out_output[getCycle()%3];

    id1452out_result = (not_fixed(id1452in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1453out_result;

  { // Node ID: 1453 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1453in_a = id2586out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1453in_b = id1452out_result;

    HWOffsetFix<1,0,UNSIGNED> id1453x_1;

    (id1453x_1) = (and_fixed(id1453in_a,id1453in_b));
    id1453out_result = (id1453x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1454out_result;

  { // Node ID: 1454 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1454in_a = id1453out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1454in_b = id2970out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1454x_1;

    (id1454x_1) = (or_fixed(id1454in_a,id1454in_b));
    id1454out_result = (id1454x_1);
  }
  HWRawBits<2> id1455out_result;

  { // Node ID: 1455 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1455in_in0 = id1445out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1455in_in1 = id1454out_result;

    id1455out_result = (cat(id1455in_in0,id1455in_in1));
  }
  { // Node ID: 1447 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1446out_o;

  { // Node ID: 1446 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1446in_i = id2668out_result;

    id1446out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1446in_i));
  }
  { // Node ID: 1431 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1432out_result;

  { // Node ID: 1432 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1432in_sel = id2583out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1432in_option0 = id1428out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1432in_option1 = id1431out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1432x_1;

    switch((id1432in_sel.getValueAsLong())) {
      case 0l:
        id1432x_1 = id1432in_option0;
        break;
      case 1l:
        id1432x_1 = id1432in_option1;
        break;
      default:
        id1432x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1432out_result = (id1432x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1433out_o;

  { // Node ID: 1433 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1433in_i = id1432out_result;

    id1433out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1433in_i));
  }
  HWRawBits<20> id1448out_result;

  { // Node ID: 1448 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1448in_in0 = id1447out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1448in_in1 = id1446out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1448in_in2 = id1433out_o;

    id1448out_result = (cat((cat(id1448in_in0,id1448in_in1)),id1448in_in2));
  }
  HWFloat<8,12> id1449out_output;

  { // Node ID: 1449 (NodeReinterpret)
    const HWRawBits<20> &id1449in_input = id1448out_result;

    id1449out_output = (cast_bits2float<8,12>(id1449in_input));
  }
  { // Node ID: 1456 (NodeConstantRawBits)
  }
  { // Node ID: 1457 (NodeConstantRawBits)
  }
  { // Node ID: 1459 (NodeConstantRawBits)
  }
  HWRawBits<20> id2587out_result;

  { // Node ID: 2587 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2587in_in0 = id1456out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2587in_in1 = id1457out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2587in_in2 = id1459out_value;

    id2587out_result = (cat((cat(id2587in_in0,id2587in_in1)),id2587in_in2));
  }
  HWFloat<8,12> id1461out_output;

  { // Node ID: 1461 (NodeReinterpret)
    const HWRawBits<20> &id1461in_input = id2587out_result;

    id1461out_output = (cast_bits2float<8,12>(id1461in_input));
  }
  { // Node ID: 2416 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1464out_result;

  { // Node ID: 1464 (NodeMux)
    const HWRawBits<2> &id1464in_sel = id1455out_result;
    const HWFloat<8,12> &id1464in_option0 = id1449out_output;
    const HWFloat<8,12> &id1464in_option1 = id1461out_output;
    const HWFloat<8,12> &id1464in_option2 = id2416out_value;
    const HWFloat<8,12> &id1464in_option3 = id1461out_output;

    HWFloat<8,12> id1464x_1;

    switch((id1464in_sel.getValueAsLong())) {
      case 0l:
        id1464x_1 = id1464in_option0;
        break;
      case 1l:
        id1464x_1 = id1464in_option1;
        break;
      case 2l:
        id1464x_1 = id1464in_option2;
        break;
      case 3l:
        id1464x_1 = id1464in_option3;
        break;
      default:
        id1464x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1464out_result = (id1464x_1);
  }
  { // Node ID: 3077 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1474out_result;

  { // Node ID: 1474 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1474in_sel = id2975out_output[getCycle()%9];
    const HWFloat<8,12> &id1474in_option0 = id1464out_result;
    const HWFloat<8,12> &id1474in_option1 = id3077out_value;

    HWFloat<8,12> id1474x_1;

    switch((id1474in_sel.getValueAsLong())) {
      case 0l:
        id1474x_1 = id1474in_option0;
        break;
      case 1l:
        id1474x_1 = id1474in_option1;
        break;
      default:
        id1474x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1474out_result = (id1474x_1);
  }
  { // Node ID: 3076 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1479out_result;

  { // Node ID: 1479 (NodeAdd)
    const HWFloat<8,12> &id1479in_a = id1474out_result;
    const HWFloat<8,12> &id1479in_b = id3076out_value;

    id1479out_result = (add_float(id1479in_a,id1479in_b));
  }
  HWFloat<8,12> id1480out_result;

  { // Node ID: 1480 (NodeDiv)
    const HWFloat<8,12> &id1480in_a = id1396out_result;
    const HWFloat<8,12> &id1480in_b = id1479out_result;

    id1480out_result = (div_float(id1480in_a,id1480in_b));
  }
  HWFloat<8,12> id1481out_result;

  { // Node ID: 1481 (NodeMul)
    const HWFloat<8,12> &id1481in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id1481in_b = id1480out_result;

    id1481out_result = (mul_float(id1481in_a,id1481in_b));
  }
  { // Node ID: 3075 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1681out_result;

  { // Node ID: 1681 (NodeSub)
    const HWFloat<8,12> &id1681in_a = id3075out_value;
    const HWFloat<8,12> &id1681in_b = id2976out_output[getCycle()%9];

    id1681out_result = (sub_float(id1681in_a,id1681in_b));
  }
  HWFloat<8,12> id1682out_result;

  { // Node ID: 1682 (NodeMul)
    const HWFloat<8,12> &id1682in_a = id1481out_result;
    const HWFloat<8,12> &id1682in_b = id1681out_result;

    id1682out_result = (mul_float(id1682in_a,id1682in_b));
  }
  { // Node ID: 3074 (NodeConstantRawBits)
  }
  { // Node ID: 3073 (NodeConstantRawBits)
  }
  { // Node ID: 3072 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1483out_result;

  { // Node ID: 1483 (NodeAdd)
    const HWFloat<8,12> &id1483in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1483in_b = id3072out_value;

    id1483out_result = (add_float(id1483in_a,id1483in_b));
  }
  HWFloat<8,12> id1485out_result;

  { // Node ID: 1485 (NodeMul)
    const HWFloat<8,12> &id1485in_a = id3073out_value;
    const HWFloat<8,12> &id1485in_b = id1483out_result;

    id1485out_result = (mul_float(id1485in_a,id1485in_b));
  }
  HWRawBits<8> id1552out_result;

  { // Node ID: 1552 (NodeSlice)
    const HWFloat<8,12> &id1552in_a = id1485out_result;

    id1552out_result = (slice<11,8>(id1552in_a));
  }
  { // Node ID: 1553 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2588out_result;

  { // Node ID: 2588 (NodeEqInlined)
    const HWRawBits<8> &id2588in_a = id1552out_result;
    const HWRawBits<8> &id2588in_b = id1553out_value;

    id2588out_result = (eq_bits(id2588in_a,id2588in_b));
  }
  HWRawBits<11> id1551out_result;

  { // Node ID: 1551 (NodeSlice)
    const HWFloat<8,12> &id1551in_a = id1485out_result;

    id1551out_result = (slice<0,11>(id1551in_a));
  }
  { // Node ID: 3071 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2589out_result;

  { // Node ID: 2589 (NodeNeqInlined)
    const HWRawBits<11> &id2589in_a = id1551out_result;
    const HWRawBits<11> &id2589in_b = id3071out_value;

    id2589out_result = (neq_bits(id2589in_a,id2589in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1557out_result;

  { // Node ID: 1557 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1557in_a = id2588out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1557in_b = id2589out_result;

    HWOffsetFix<1,0,UNSIGNED> id1557x_1;

    (id1557x_1) = (and_fixed(id1557in_a,id1557in_b));
    id1557out_result = (id1557x_1);
  }
  { // Node ID: 2986 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2986in_input = id1557out_result;

    id2986out_output[(getCycle()+8)%9] = id2986in_input;
  }
  { // Node ID: 1486 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1487out_output;
  HWOffsetFix<1,0,UNSIGNED> id1487out_output_doubt;

  { // Node ID: 1487 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1487in_input = id1485out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1487in_doubt = id1486out_value;

    id1487out_output = id1487in_input;
    id1487out_output_doubt = id1487in_doubt;
  }
  { // Node ID: 1488 (NodeCast)
    const HWFloat<8,12> &id1488in_i = id1487out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1488in_i_doubt = id1487out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1488x_1;

    id1488out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1488in_i,(&(id1488x_1))));
    id1488out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1488x_1),(c_hw_fix_4_0_uns_bits))),id1488in_i_doubt));
  }
  { // Node ID: 1491 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1490out_result;
  HWOffsetFix<1,0,UNSIGNED> id1490out_result_doubt;

  { // Node ID: 1490 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1490in_a = id1488out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1490in_a_doubt = id1488out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1490in_b = id1491out_value;

    HWOffsetFix<1,0,UNSIGNED> id1490x_1;

    id1490out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1490in_a,id1490in_b,(&(id1490x_1))));
    id1490out_result_doubt = (or_fixed((neq_fixed((id1490x_1),(c_hw_fix_1_0_uns_bits_1))),id1490in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1492out_o;
  HWOffsetFix<1,0,UNSIGNED> id1492out_o_doubt;

  { // Node ID: 1492 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1492in_i = id1490out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1492in_i_doubt = id1490out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1492x_1;

    id1492out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1492in_i,(&(id1492x_1))));
    id1492out_o_doubt = (or_fixed((neq_fixed((id1492x_1),(c_hw_fix_1_0_uns_bits_1))),id1492in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1501out_output;

  { // Node ID: 1501 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1501in_input = id1492out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1501in_input_doubt = id1492out_o_doubt;

    id1501out_output = id1501in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1502out_o;

  { // Node ID: 1502 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1502in_i = id1501out_output;

    id1502out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1502in_i));
  }
  { // Node ID: 2977 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2977in_input = id1502out_o;

    id2977out_output[(getCycle()+2)%3] = id2977in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1519out_o;

  { // Node ID: 1519 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1519in_i = id2977out_output[getCycle()%3];

    id1519out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1519in_i));
  }
  { // Node ID: 1522 (NodeConstantRawBits)
  }
  { // Node ID: 2660 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1503out_o;

  { // Node ID: 1503 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1503in_i = id1501out_output;

    id1503out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1503in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1561out_output;

  { // Node ID: 1561 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1561in_input = id1503out_o;

    id1561out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1561in_input))));
  }
  { // Node ID: 1562 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1562in_addr = id1561out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1562x_1;

    switch(((long)((id1562in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1562x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1562x_1 = (id1562sta_rom_store[(id1562in_addr.getValueAsLong())]);
        break;
      default:
        id1562x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1562out_dout[(getCycle()+2)%3] = (id1562x_1);
  }
  { // Node ID: 1507 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1504out_o;

  { // Node ID: 1504 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1504in_i = id1501out_output;

    id1504out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1504in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1506out_result;

  { // Node ID: 1506 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1506in_a = id1507out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1506in_b = id1504out_o;

    id1506out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1506in_a,id1506in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1508out_o;

  { // Node ID: 1508 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1508in_i = id1506out_result;

    id1508out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1508in_i));
  }
  { // Node ID: 2978 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2978in_input = id1508out_o;

    id2978out_output[(getCycle()+2)%3] = id2978in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1509out_result;

  { // Node ID: 1509 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1509in_a = id1562out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1509in_b = id2978out_output[getCycle()%3];

    id1509out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1509in_a,id1509in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1510out_result;

  { // Node ID: 1510 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1510in_a = id2978out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1510in_b = id1562out_dout[getCycle()%3];

    id1510out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1510in_a,id1510in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1511out_result;

  { // Node ID: 1511 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1511in_a = id1509out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1511in_b = id1510out_result;

    id1511out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1511in_a,id1511in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1512out_o;

  { // Node ID: 1512 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1512in_i = id1511out_result;

    id1512out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1512in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1513out_o;

  { // Node ID: 1513 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1513in_i = id1512out_o;

    id1513out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1513in_i));
  }
  { // Node ID: 3070 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2590out_result;

  { // Node ID: 2590 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2590in_a = id1513out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2590in_b = id3070out_value;

    id2590out_result = (gte_fixed(id2590in_a,id2590in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2667out_result;

  { // Node ID: 2667 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2667in_a = id1519out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2667in_b = id1522out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2667in_c = id2660out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2667in_condb = id2590out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2667x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2667x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2667x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2667x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2667x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2667x_1 = id2667in_a;
        break;
      default:
        id2667x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2667in_condb.getValueAsLong())) {
      case 0l:
        id2667x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2667x_2 = id2667in_b;
        break;
      default:
        id2667x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2667x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2667x_3 = id2667in_c;
        break;
      default:
        id2667x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2667x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2667x_1),(id2667x_2))),(id2667x_3)));
    id2667out_result = (id2667x_4);
  }
  HWRawBits<1> id2591out_result;

  { // Node ID: 2591 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2591in_a = id2667out_result;

    id2591out_result = (slice<10,1>(id2591in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2592out_output;

  { // Node ID: 2592 (NodeReinterpret)
    const HWRawBits<1> &id2592in_input = id2591out_result;

    id2592out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2592in_input));
  }
  { // Node ID: 3069 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1494out_result;

  { // Node ID: 1494 (NodeGt)
    const HWFloat<8,12> &id1494in_a = id1485out_result;
    const HWFloat<8,12> &id1494in_b = id3069out_value;

    id1494out_result = (gt_float(id1494in_a,id1494in_b));
  }
  { // Node ID: 2980 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2980in_input = id1494out_result;

    id2980out_output[(getCycle()+6)%7] = id2980in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1495out_output;

  { // Node ID: 1495 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1495in_input = id1492out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1495in_input_doubt = id1492out_o_doubt;

    id1495out_output = id1495in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1496out_result;

  { // Node ID: 1496 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1496in_a = id2980out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1496in_b = id1495out_output;

    HWOffsetFix<1,0,UNSIGNED> id1496x_1;

    (id1496x_1) = (and_fixed(id1496in_a,id1496in_b));
    id1496out_result = (id1496x_1);
  }
  { // Node ID: 2981 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2981in_input = id1496out_result;

    id2981out_output[(getCycle()+2)%3] = id2981in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1528out_result;

  { // Node ID: 1528 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1528in_a = id2981out_output[getCycle()%3];

    id1528out_result = (not_fixed(id1528in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1529out_result;

  { // Node ID: 1529 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1529in_a = id2592out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1529in_b = id1528out_result;

    HWOffsetFix<1,0,UNSIGNED> id1529x_1;

    (id1529x_1) = (and_fixed(id1529in_a,id1529in_b));
    id1529out_result = (id1529x_1);
  }
  { // Node ID: 3068 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1498out_result;

  { // Node ID: 1498 (NodeLt)
    const HWFloat<8,12> &id1498in_a = id1485out_result;
    const HWFloat<8,12> &id1498in_b = id3068out_value;

    id1498out_result = (lt_float(id1498in_a,id1498in_b));
  }
  { // Node ID: 2982 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2982in_input = id1498out_result;

    id2982out_output[(getCycle()+6)%7] = id2982in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1499out_output;

  { // Node ID: 1499 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1499in_input = id1492out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1499in_input_doubt = id1492out_o_doubt;

    id1499out_output = id1499in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1500out_result;

  { // Node ID: 1500 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1500in_a = id2982out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1500in_b = id1499out_output;

    HWOffsetFix<1,0,UNSIGNED> id1500x_1;

    (id1500x_1) = (and_fixed(id1500in_a,id1500in_b));
    id1500out_result = (id1500x_1);
  }
  { // Node ID: 2983 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2983in_input = id1500out_result;

    id2983out_output[(getCycle()+2)%3] = id2983in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1530out_result;

  { // Node ID: 1530 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1530in_a = id1529out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1530in_b = id2983out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1530x_1;

    (id1530x_1) = (or_fixed(id1530in_a,id1530in_b));
    id1530out_result = (id1530x_1);
  }
  { // Node ID: 3067 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2593out_result;

  { // Node ID: 2593 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2593in_a = id2667out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2593in_b = id3067out_value;

    id2593out_result = (gte_fixed(id2593in_a,id2593in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1537out_result;

  { // Node ID: 1537 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1537in_a = id2983out_output[getCycle()%3];

    id1537out_result = (not_fixed(id1537in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1538out_result;

  { // Node ID: 1538 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1538in_a = id2593out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1538in_b = id1537out_result;

    HWOffsetFix<1,0,UNSIGNED> id1538x_1;

    (id1538x_1) = (and_fixed(id1538in_a,id1538in_b));
    id1538out_result = (id1538x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1539out_result;

  { // Node ID: 1539 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1539in_a = id1538out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1539in_b = id2981out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1539x_1;

    (id1539x_1) = (or_fixed(id1539in_a,id1539in_b));
    id1539out_result = (id1539x_1);
  }
  HWRawBits<2> id1540out_result;

  { // Node ID: 1540 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1540in_in0 = id1530out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1540in_in1 = id1539out_result;

    id1540out_result = (cat(id1540in_in0,id1540in_in1));
  }
  { // Node ID: 1532 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1531out_o;

  { // Node ID: 1531 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1531in_i = id2667out_result;

    id1531out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1531in_i));
  }
  { // Node ID: 1516 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1517out_result;

  { // Node ID: 1517 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1517in_sel = id2590out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1517in_option0 = id1513out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1517in_option1 = id1516out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1517x_1;

    switch((id1517in_sel.getValueAsLong())) {
      case 0l:
        id1517x_1 = id1517in_option0;
        break;
      case 1l:
        id1517x_1 = id1517in_option1;
        break;
      default:
        id1517x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1517out_result = (id1517x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1518out_o;

  { // Node ID: 1518 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1518in_i = id1517out_result;

    id1518out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1518in_i));
  }
  HWRawBits<20> id1533out_result;

  { // Node ID: 1533 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1533in_in0 = id1532out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1533in_in1 = id1531out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1533in_in2 = id1518out_o;

    id1533out_result = (cat((cat(id1533in_in0,id1533in_in1)),id1533in_in2));
  }
  HWFloat<8,12> id1534out_output;

  { // Node ID: 1534 (NodeReinterpret)
    const HWRawBits<20> &id1534in_input = id1533out_result;

    id1534out_output = (cast_bits2float<8,12>(id1534in_input));
  }
  { // Node ID: 1541 (NodeConstantRawBits)
  }
  { // Node ID: 1542 (NodeConstantRawBits)
  }
  { // Node ID: 1544 (NodeConstantRawBits)
  }
  HWRawBits<20> id2594out_result;

  { // Node ID: 2594 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2594in_in0 = id1541out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2594in_in1 = id1542out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2594in_in2 = id1544out_value;

    id2594out_result = (cat((cat(id2594in_in0,id2594in_in1)),id2594in_in2));
  }
  HWFloat<8,12> id1546out_output;

  { // Node ID: 1546 (NodeReinterpret)
    const HWRawBits<20> &id1546in_input = id2594out_result;

    id1546out_output = (cast_bits2float<8,12>(id1546in_input));
  }
  { // Node ID: 2417 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1549out_result;

  { // Node ID: 1549 (NodeMux)
    const HWRawBits<2> &id1549in_sel = id1540out_result;
    const HWFloat<8,12> &id1549in_option0 = id1534out_output;
    const HWFloat<8,12> &id1549in_option1 = id1546out_output;
    const HWFloat<8,12> &id1549in_option2 = id2417out_value;
    const HWFloat<8,12> &id1549in_option3 = id1546out_output;

    HWFloat<8,12> id1549x_1;

    switch((id1549in_sel.getValueAsLong())) {
      case 0l:
        id1549x_1 = id1549in_option0;
        break;
      case 1l:
        id1549x_1 = id1549in_option1;
        break;
      case 2l:
        id1549x_1 = id1549in_option2;
        break;
      case 3l:
        id1549x_1 = id1549in_option3;
        break;
      default:
        id1549x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1549out_result = (id1549x_1);
  }
  { // Node ID: 3066 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1559out_result;

  { // Node ID: 1559 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1559in_sel = id2986out_output[getCycle()%9];
    const HWFloat<8,12> &id1559in_option0 = id1549out_result;
    const HWFloat<8,12> &id1559in_option1 = id3066out_value;

    HWFloat<8,12> id1559x_1;

    switch((id1559in_sel.getValueAsLong())) {
      case 0l:
        id1559x_1 = id1559in_option0;
        break;
      case 1l:
        id1559x_1 = id1559in_option1;
        break;
      default:
        id1559x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1559out_result = (id1559x_1);
  }
  HWFloat<8,12> id1564out_result;

  { // Node ID: 1564 (NodeMul)
    const HWFloat<8,12> &id1564in_a = id3074out_value;
    const HWFloat<8,12> &id1564in_b = id1559out_result;

    id1564out_result = (mul_float(id1564in_a,id1564in_b));
  }
  { // Node ID: 3065 (NodeConstantRawBits)
  }
  { // Node ID: 3064 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1566out_result;

  { // Node ID: 1566 (NodeAdd)
    const HWFloat<8,12> &id1566in_a = id17out_result[getCycle()%2];
    const HWFloat<8,12> &id1566in_b = id3064out_value;

    id1566out_result = (add_float(id1566in_a,id1566in_b));
  }
  HWFloat<8,12> id1568out_result;

  { // Node ID: 1568 (NodeMul)
    const HWFloat<8,12> &id1568in_a = id3065out_value;
    const HWFloat<8,12> &id1568in_b = id1566out_result;

    id1568out_result = (mul_float(id1568in_a,id1568in_b));
  }
  HWRawBits<8> id1635out_result;

  { // Node ID: 1635 (NodeSlice)
    const HWFloat<8,12> &id1635in_a = id1568out_result;

    id1635out_result = (slice<11,8>(id1635in_a));
  }
  { // Node ID: 1636 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2595out_result;

  { // Node ID: 2595 (NodeEqInlined)
    const HWRawBits<8> &id2595in_a = id1635out_result;
    const HWRawBits<8> &id2595in_b = id1636out_value;

    id2595out_result = (eq_bits(id2595in_a,id2595in_b));
  }
  HWRawBits<11> id1634out_result;

  { // Node ID: 1634 (NodeSlice)
    const HWFloat<8,12> &id1634in_a = id1568out_result;

    id1634out_result = (slice<0,11>(id1634in_a));
  }
  { // Node ID: 3063 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2596out_result;

  { // Node ID: 2596 (NodeNeqInlined)
    const HWRawBits<11> &id2596in_a = id1634out_result;
    const HWRawBits<11> &id2596in_b = id3063out_value;

    id2596out_result = (neq_bits(id2596in_a,id2596in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1640out_result;

  { // Node ID: 1640 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1640in_a = id2595out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1640in_b = id2596out_result;

    HWOffsetFix<1,0,UNSIGNED> id1640x_1;

    (id1640x_1) = (and_fixed(id1640in_a,id1640in_b));
    id1640out_result = (id1640x_1);
  }
  { // Node ID: 2996 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2996in_input = id1640out_result;

    id2996out_output[(getCycle()+8)%9] = id2996in_input;
  }
  { // Node ID: 1569 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1570out_output;
  HWOffsetFix<1,0,UNSIGNED> id1570out_output_doubt;

  { // Node ID: 1570 (NodeDoubtBitOp)
    const HWFloat<8,12> &id1570in_input = id1568out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1570in_doubt = id1569out_value;

    id1570out_output = id1570in_input;
    id1570out_output_doubt = id1570in_doubt;
  }
  { // Node ID: 1571 (NodeCast)
    const HWFloat<8,12> &id1571in_i = id1570out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1571in_i_doubt = id1570out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1571x_1;

    id1571out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1571in_i,(&(id1571x_1))));
    id1571out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1571x_1),(c_hw_fix_4_0_uns_bits))),id1571in_i_doubt));
  }
  { // Node ID: 1574 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id1573out_result;
  HWOffsetFix<1,0,UNSIGNED> id1573out_result_doubt;

  { // Node ID: 1573 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1573in_a = id1571out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1573in_a_doubt = id1571out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id1573in_b = id1574out_value;

    HWOffsetFix<1,0,UNSIGNED> id1573x_1;

    id1573out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id1573in_a,id1573in_b,(&(id1573x_1))));
    id1573out_result_doubt = (or_fixed((neq_fixed((id1573x_1),(c_hw_fix_1_0_uns_bits_1))),id1573in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1575out_o;
  HWOffsetFix<1,0,UNSIGNED> id1575out_o_doubt;

  { // Node ID: 1575 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id1575in_i = id1573out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1575in_i_doubt = id1573out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1575x_1;

    id1575out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id1575in_i,(&(id1575x_1))));
    id1575out_o_doubt = (or_fixed((neq_fixed((id1575x_1),(c_hw_fix_1_0_uns_bits_1))),id1575in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1584out_output;

  { // Node ID: 1584 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1584in_input = id1575out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1584in_input_doubt = id1575out_o_doubt;

    id1584out_output = id1584in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id1585out_o;

  { // Node ID: 1585 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1585in_i = id1584out_output;

    id1585out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id1585in_i));
  }
  { // Node ID: 2987 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id2987in_input = id1585out_o;

    id2987out_output[(getCycle()+2)%3] = id2987in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1602out_o;

  { // Node ID: 1602 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id1602in_i = id2987out_output[getCycle()%3];

    id1602out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1602in_i));
  }
  { // Node ID: 1605 (NodeConstantRawBits)
  }
  { // Node ID: 2662 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id1586out_o;

  { // Node ID: 1586 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1586in_i = id1584out_output;

    id1586out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id1586in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id1644out_output;

  { // Node ID: 1644 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id1644in_input = id1586out_o;

    id1644out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id1644in_input))));
  }
  { // Node ID: 1645 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id1645in_addr = id1644out_output;

    HWOffsetFix<12,-12,UNSIGNED> id1645x_1;

    switch(((long)((id1645in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id1645x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id1645x_1 = (id1645sta_rom_store[(id1645in_addr.getValueAsLong())]);
        break;
      default:
        id1645x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id1645out_dout[(getCycle()+2)%3] = (id1645x_1);
  }
  { // Node ID: 1590 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id1587out_o;

  { // Node ID: 1587 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1587in_i = id1584out_output;

    id1587out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id1587in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id1589out_result;

  { // Node ID: 1589 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id1589in_a = id1590out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id1589in_b = id1587out_o;

    id1589out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id1589in_a,id1589in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id1591out_o;

  { // Node ID: 1591 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id1591in_i = id1589out_result;

    id1591out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id1591in_i));
  }
  { // Node ID: 2988 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id2988in_input = id1591out_o;

    id2988out_output[(getCycle()+2)%3] = id2988in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id1592out_result;

  { // Node ID: 1592 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id1592in_a = id1645out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id1592in_b = id2988out_output[getCycle()%3];

    id1592out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id1592in_a,id1592in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id1593out_result;

  { // Node ID: 1593 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id1593in_a = id2988out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id1593in_b = id1645out_dout[getCycle()%3];

    id1593out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id1593in_a,id1593in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id1594out_result;

  { // Node ID: 1594 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id1594in_a = id1592out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id1594in_b = id1593out_result;

    id1594out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id1594in_a,id1594in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id1595out_o;

  { // Node ID: 1595 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id1595in_i = id1594out_result;

    id1595out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id1595in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id1596out_o;

  { // Node ID: 1596 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id1596in_i = id1595out_o;

    id1596out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1596in_i));
  }
  { // Node ID: 3062 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2597out_result;

  { // Node ID: 2597 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id2597in_a = id1596out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id2597in_b = id3062out_value;

    id2597out_result = (gte_fixed(id2597in_a,id2597in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2666out_result;

  { // Node ID: 2666 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2666in_a = id1602out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2666in_b = id1605out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2666in_c = id2662out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2666in_condb = id2597out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2666x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2666x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2666x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id2666x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2666x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2666x_1 = id2666in_a;
        break;
      default:
        id2666x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id2666in_condb.getValueAsLong())) {
      case 0l:
        id2666x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2666x_2 = id2666in_b;
        break;
      default:
        id2666x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id2666x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id2666x_3 = id2666in_c;
        break;
      default:
        id2666x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id2666x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id2666x_1),(id2666x_2))),(id2666x_3)));
    id2666out_result = (id2666x_4);
  }
  HWRawBits<1> id2598out_result;

  { // Node ID: 2598 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2598in_a = id2666out_result;

    id2598out_result = (slice<10,1>(id2598in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2599out_output;

  { // Node ID: 2599 (NodeReinterpret)
    const HWRawBits<1> &id2599in_input = id2598out_result;

    id2599out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2599in_input));
  }
  { // Node ID: 3061 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1577out_result;

  { // Node ID: 1577 (NodeGt)
    const HWFloat<8,12> &id1577in_a = id1568out_result;
    const HWFloat<8,12> &id1577in_b = id3061out_value;

    id1577out_result = (gt_float(id1577in_a,id1577in_b));
  }
  { // Node ID: 2990 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2990in_input = id1577out_result;

    id2990out_output[(getCycle()+6)%7] = id2990in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1578out_output;

  { // Node ID: 1578 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1578in_input = id1575out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1578in_input_doubt = id1575out_o_doubt;

    id1578out_output = id1578in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1579out_result;

  { // Node ID: 1579 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1579in_a = id2990out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1579in_b = id1578out_output;

    HWOffsetFix<1,0,UNSIGNED> id1579x_1;

    (id1579x_1) = (and_fixed(id1579in_a,id1579in_b));
    id1579out_result = (id1579x_1);
  }
  { // Node ID: 2991 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2991in_input = id1579out_result;

    id2991out_output[(getCycle()+2)%3] = id2991in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1611out_result;

  { // Node ID: 1611 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1611in_a = id2991out_output[getCycle()%3];

    id1611out_result = (not_fixed(id1611in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1612out_result;

  { // Node ID: 1612 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1612in_a = id2599out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1612in_b = id1611out_result;

    HWOffsetFix<1,0,UNSIGNED> id1612x_1;

    (id1612x_1) = (and_fixed(id1612in_a,id1612in_b));
    id1612out_result = (id1612x_1);
  }
  { // Node ID: 3060 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1581out_result;

  { // Node ID: 1581 (NodeLt)
    const HWFloat<8,12> &id1581in_a = id1568out_result;
    const HWFloat<8,12> &id1581in_b = id3060out_value;

    id1581out_result = (lt_float(id1581in_a,id1581in_b));
  }
  { // Node ID: 2992 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2992in_input = id1581out_result;

    id2992out_output[(getCycle()+6)%7] = id2992in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1582out_output;

  { // Node ID: 1582 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id1582in_input = id1575out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1582in_input_doubt = id1575out_o_doubt;

    id1582out_output = id1582in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1583out_result;

  { // Node ID: 1583 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1583in_a = id2992out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1583in_b = id1582out_output;

    HWOffsetFix<1,0,UNSIGNED> id1583x_1;

    (id1583x_1) = (and_fixed(id1583in_a,id1583in_b));
    id1583out_result = (id1583x_1);
  }
  { // Node ID: 2993 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id2993in_input = id1583out_result;

    id2993out_output[(getCycle()+2)%3] = id2993in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1613out_result;

  { // Node ID: 1613 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1613in_a = id1612out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1613in_b = id2993out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1613x_1;

    (id1613x_1) = (or_fixed(id1613in_a,id1613in_b));
    id1613out_result = (id1613x_1);
  }
  { // Node ID: 3059 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2600out_result;

  { // Node ID: 2600 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2600in_a = id2666out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2600in_b = id3059out_value;

    id2600out_result = (gte_fixed(id2600in_a,id2600in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1620out_result;

  { // Node ID: 1620 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1620in_a = id2993out_output[getCycle()%3];

    id1620out_result = (not_fixed(id1620in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1621out_result;

  { // Node ID: 1621 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1621in_a = id2600out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1621in_b = id1620out_result;

    HWOffsetFix<1,0,UNSIGNED> id1621x_1;

    (id1621x_1) = (and_fixed(id1621in_a,id1621in_b));
    id1621out_result = (id1621x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1622out_result;

  { // Node ID: 1622 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1622in_a = id1621out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1622in_b = id2991out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1622x_1;

    (id1622x_1) = (or_fixed(id1622in_a,id1622in_b));
    id1622out_result = (id1622x_1);
  }
  HWRawBits<2> id1623out_result;

  { // Node ID: 1623 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1623in_in0 = id1613out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1623in_in1 = id1622out_result;

    id1623out_result = (cat(id1623in_in0,id1623in_in1));
  }
  { // Node ID: 1615 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id1614out_o;

  { // Node ID: 1614 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1614in_i = id2666out_result;

    id1614out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id1614in_i));
  }
  { // Node ID: 1599 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id1600out_result;

  { // Node ID: 1600 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1600in_sel = id2597out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id1600in_option0 = id1596out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id1600in_option1 = id1599out_value;

    HWOffsetFix<12,-11,UNSIGNED> id1600x_1;

    switch((id1600in_sel.getValueAsLong())) {
      case 0l:
        id1600x_1 = id1600in_option0;
        break;
      case 1l:
        id1600x_1 = id1600in_option1;
        break;
      default:
        id1600x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id1600out_result = (id1600x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1601out_o;

  { // Node ID: 1601 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1601in_i = id1600out_result;

    id1601out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1601in_i));
  }
  HWRawBits<20> id1616out_result;

  { // Node ID: 1616 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1616in_in0 = id1615out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id1616in_in1 = id1614out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id1616in_in2 = id1601out_o;

    id1616out_result = (cat((cat(id1616in_in0,id1616in_in1)),id1616in_in2));
  }
  HWFloat<8,12> id1617out_output;

  { // Node ID: 1617 (NodeReinterpret)
    const HWRawBits<20> &id1617in_input = id1616out_result;

    id1617out_output = (cast_bits2float<8,12>(id1617in_input));
  }
  { // Node ID: 1624 (NodeConstantRawBits)
  }
  { // Node ID: 1625 (NodeConstantRawBits)
  }
  { // Node ID: 1627 (NodeConstantRawBits)
  }
  HWRawBits<20> id2601out_result;

  { // Node ID: 2601 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2601in_in0 = id1624out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id2601in_in1 = id1625out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2601in_in2 = id1627out_value;

    id2601out_result = (cat((cat(id2601in_in0,id2601in_in1)),id2601in_in2));
  }
  HWFloat<8,12> id1629out_output;

  { // Node ID: 1629 (NodeReinterpret)
    const HWRawBits<20> &id1629in_input = id2601out_result;

    id1629out_output = (cast_bits2float<8,12>(id1629in_input));
  }
  { // Node ID: 2418 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1632out_result;

  { // Node ID: 1632 (NodeMux)
    const HWRawBits<2> &id1632in_sel = id1623out_result;
    const HWFloat<8,12> &id1632in_option0 = id1617out_output;
    const HWFloat<8,12> &id1632in_option1 = id1629out_output;
    const HWFloat<8,12> &id1632in_option2 = id2418out_value;
    const HWFloat<8,12> &id1632in_option3 = id1629out_output;

    HWFloat<8,12> id1632x_1;

    switch((id1632in_sel.getValueAsLong())) {
      case 0l:
        id1632x_1 = id1632in_option0;
        break;
      case 1l:
        id1632x_1 = id1632in_option1;
        break;
      case 2l:
        id1632x_1 = id1632in_option2;
        break;
      case 3l:
        id1632x_1 = id1632in_option3;
        break;
      default:
        id1632x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1632out_result = (id1632x_1);
  }
  { // Node ID: 3058 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1642out_result;

  { // Node ID: 1642 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1642in_sel = id2996out_output[getCycle()%9];
    const HWFloat<8,12> &id1642in_option0 = id1632out_result;
    const HWFloat<8,12> &id1642in_option1 = id3058out_value;

    HWFloat<8,12> id1642x_1;

    switch((id1642in_sel.getValueAsLong())) {
      case 0l:
        id1642x_1 = id1642in_option0;
        break;
      case 1l:
        id1642x_1 = id1642in_option1;
        break;
      default:
        id1642x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id1642out_result = (id1642x_1);
  }
  { // Node ID: 3057 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1647out_result;

  { // Node ID: 1647 (NodeAdd)
    const HWFloat<8,12> &id1647in_a = id1642out_result;
    const HWFloat<8,12> &id1647in_b = id3057out_value;

    id1647out_result = (add_float(id1647in_a,id1647in_b));
  }
  HWFloat<8,12> id1648out_result;

  { // Node ID: 1648 (NodeDiv)
    const HWFloat<8,12> &id1648in_a = id1564out_result;
    const HWFloat<8,12> &id1648in_b = id1647out_result;

    id1648out_result = (div_float(id1648in_a,id1648in_b));
  }
  HWFloat<8,12> id1649out_result;

  { // Node ID: 1649 (NodeMul)
    const HWFloat<8,12> &id1649in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id1649in_b = id1648out_result;

    id1649out_result = (mul_float(id1649in_a,id1649in_b));
  }
  HWFloat<8,12> id1683out_result;

  { // Node ID: 1683 (NodeMul)
    const HWFloat<8,12> &id1683in_a = id1649out_result;
    const HWFloat<8,12> &id1683in_b = id2976out_output[getCycle()%9];

    id1683out_result = (mul_float(id1683in_a,id1683in_b));
  }
  HWFloat<8,12> id1684out_result;

  { // Node ID: 1684 (NodeSub)
    const HWFloat<8,12> &id1684in_a = id1682out_result;
    const HWFloat<8,12> &id1684in_b = id1683out_result;

    id1684out_result = (sub_float(id1684in_a,id1684in_b));
  }
  HWFloat<8,12> id1685out_result;

  { // Node ID: 1685 (NodeAdd)
    const HWFloat<8,12> &id1685in_a = id2976out_output[getCycle()%9];
    const HWFloat<8,12> &id1685in_b = id1684out_result;

    id1685out_result = (add_float(id1685in_a,id1685in_b));
  }
  { // Node ID: 2955 (NodeFIFO)
    const HWFloat<8,12> &id2955in_input = id1685out_result;

    id2955out_output[(getCycle()+1)%2] = id2955in_input;
  }
  { // Node ID: 3054 (NodeConstantRawBits)
  }
  { // Node ID: 2602 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2602in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2602in_b = id3054out_value;

    id2602out_result[(getCycle()+1)%2] = (eq_fixed(id2602in_a,id2602in_b));
  }
  { // Node ID: 3053 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1766out_result;

  { // Node ID: 1766 (NodeMul)
    const HWFloat<8,12> &id1766in_a = id3053out_value;
    const HWFloat<8,12> &id1766in_b = id1764out_result;

    id1766out_result = (mul_float(id1766in_a,id1766in_b));
  }
  { // Node ID: 3052 (NodeConstantRawBits)
  }
  { // Node ID: 3051 (NodeConstantRawBits)
  }
  HWFloat<8,12> id1768out_result;

  { // Node ID: 1768 (NodeSub)
    const HWFloat<8,12> &id1768in_a = id3051out_value;
    const HWFloat<8,12> &id1768in_b = id3032out_output[getCycle()%3];

    id1768out_result = (sub_float(id1768in_a,id1768in_b));
  }
  HWFloat<8,12> id1770out_result;

  { // Node ID: 1770 (NodeMul)
    const HWFloat<8,12> &id1770in_a = id3052out_value;
    const HWFloat<8,12> &id1770in_b = id1768out_result;

    id1770out_result = (mul_float(id1770in_a,id1770in_b));
  }
  HWFloat<8,12> id1771out_result;

  { // Node ID: 1771 (NodeAdd)
    const HWFloat<8,12> &id1771in_a = id1766out_result;
    const HWFloat<8,12> &id1771in_b = id1770out_result;

    id1771out_result = (add_float(id1771in_a,id1771in_b));
  }
  HWFloat<8,12> id1772out_result;

  { // Node ID: 1772 (NodeMul)
    const HWFloat<8,12> &id1772in_a = id13out_o[getCycle()%4];
    const HWFloat<8,12> &id1772in_b = id1771out_result;

    id1772out_result = (mul_float(id1772in_a,id1772in_b));
  }
  HWFloat<8,12> id1773out_result;

  { // Node ID: 1773 (NodeAdd)
    const HWFloat<8,12> &id1773in_a = id3032out_output[getCycle()%3];
    const HWFloat<8,12> &id1773in_b = id1772out_result;

    id1773out_result = (add_float(id1773in_a,id1773in_b));
  }
  HWFloat<8,12> id2711out_output;

  { // Node ID: 2711 (NodeStreamOffset)
    const HWFloat<8,12> &id2711in_input = id1773out_result;

    id2711out_output = id2711in_input;
  }
  { // Node ID: 20 (NodeInputMappedReg)
  }
  { // Node ID: 21 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id21in_sel = id2602out_result[getCycle()%2];
    const HWFloat<8,12> &id21in_option0 = id2711out_output;
    const HWFloat<8,12> &id21in_option1 = id20out_ca_in;

    HWFloat<8,12> id21x_1;

    switch((id21in_sel.getValueAsLong())) {
      case 0l:
        id21x_1 = id21in_option0;
        break;
      case 1l:
        id21x_1 = id21in_option1;
        break;
      default:
        id21x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id21out_result[(getCycle()+1)%2] = (id21x_1);
  }
  { // Node ID: 3008 (NodeFIFO)
    const HWFloat<8,12> &id3008in_input = id21out_result[getCycle()%2];

    id3008out_output[(getCycle()+7)%8] = id3008in_input;
  }
  { // Node ID: 3032 (NodeFIFO)
    const HWFloat<8,12> &id3032in_input = id3008out_output[getCycle()%8];

    id3032out_output[(getCycle()+2)%3] = id3032in_input;
  }
  HWRawBits<11> id1686out_result;

  { // Node ID: 1686 (NodeSlice)
    const HWFloat<8,12> &id1686in_a = id3008out_output[getCycle()%8];

    id1686out_result = (slice<0,11>(id1686in_a));
  }
  HWOffsetFix<11,-11,UNSIGNED> id1687out_output;

  { // Node ID: 1687 (NodeReinterpret)
    const HWRawBits<11> &id1687in_input = id1686out_result;

    id1687out_output = (cast_bits2fixed<11,-11,UNSIGNED>(id1687in_input));
  }
  { // Node ID: 1688 (NodeConstantRawBits)
  }
  HWOffsetFix<11,-11,UNSIGNED> id1689out_output;
  HWOffsetFix<1,0,UNSIGNED> id1689out_output_doubt;

  { // Node ID: 1689 (NodeDoubtBitOp)
    const HWOffsetFix<11,-11,UNSIGNED> &id1689in_input = id1687out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1689in_doubt = id1688out_value;

    id1689out_output = id1689in_input;
    id1689out_output_doubt = id1689in_doubt;
  }
  HWOffsetFix<12,-11,UNSIGNED> id1690out_o;
  HWOffsetFix<1,0,UNSIGNED> id1690out_o_doubt;

  { // Node ID: 1690 (NodeCast)
    const HWOffsetFix<11,-11,UNSIGNED> &id1690in_i = id1689out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1690in_i_doubt = id1689out_output_doubt;

    id1690out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id1690in_i));
    id1690out_o_doubt = id1690in_i_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1694out_output;

  { // Node ID: 1694 (NodeDoubtBitOp)
    const HWOffsetFix<12,-11,UNSIGNED> &id1694in_input = id1690out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1694in_input_doubt = id1690out_o_doubt;

    id1694out_output = id1694in_input_doubt;
  }
  HWOffsetFix<12,-11,UNSIGNED> id1691out_output;

  { // Node ID: 1691 (NodeDoubtBitOp)
    const HWOffsetFix<12,-11,UNSIGNED> &id1691in_input = id1690out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1691in_input_doubt = id1690out_o_doubt;

    id1691out_output = id1691in_input;
  }
  { // Node ID: 3046 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1693out_result;

  { // Node ID: 1693 (NodeGte)
    const HWOffsetFix<12,-11,UNSIGNED> &id1693in_a = id1691out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1693in_b = id3046out_value;

    id1693out_result = (gte_fixed(id1693in_a,id1693in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1695out_result;

  { // Node ID: 1695 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1695in_a = id1694out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1695in_b = id1693out_result;

    HWOffsetFix<1,0,UNSIGNED> id1695x_1;

    (id1695x_1) = (or_fixed(id1695in_a,id1695in_b));
    id1695out_result = (id1695x_1);
  }
  { // Node ID: 3009 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3009in_input = id1695out_result;

    id3009out_output[(getCycle()+2)%3] = id3009in_input;
  }
  { // Node ID: 1696 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id2664out_result;

  { // Node ID: 2664 (NodeCondSub)
    const HWOffsetFix<12,-11,UNSIGNED> &id2664in_a = id1691out_output;
    const HWOffsetFix<12,-11,UNSIGNED> &id2664in_b = id1696out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id2664in_condb = id1695out_result;

    HWOffsetFix<12,-11,UNSIGNED> id2664x_1;
    HWOffsetFix<12,-11,UNSIGNED> id2664x_2;

    switch((id2664in_condb.getValueAsLong())) {
      case 0l:
        id2664x_1 = (c_hw_fix_12_n11_uns_bits_1);
        break;
      case 1l:
        id2664x_1 = id2664in_b;
        break;
      default:
        id2664x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    (id2664x_2) = (sub_fixed<12,-11,UNSIGNED,TRUNCATE>(id2664in_a,(id2664x_1)));
    id2664out_result = (id2664x_2);
  }
  HWOffsetFix<11,-11,UNSIGNED> id1711out_o;

  { // Node ID: 1711 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id1711in_i = id2664out_result;

    id1711out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id1711in_i));
  }
  HWRawBits<9> id1712out_result;

  { // Node ID: 1712 (NodeSlice)
    const HWOffsetFix<11,-11,UNSIGNED> &id1712in_a = id1711out_o;

    id1712out_result = (slice<2,9>(id1712in_a));
  }
  { // Node ID: 2373 (NodeROM)
    const HWRawBits<9> &id2373in_addr = id1712out_result;

    HWRawBits<48> id2373x_1;

    switch(((long)((id2373in_addr.getValueAsLong())<(512l)))) {
      case 0l:
        id2373x_1 = (c_hw_bit_48_undef);
        break;
      case 1l:
        id2373x_1 = (id2373sta_rom_store[(id2373in_addr.getValueAsLong())]);
        break;
      default:
        id2373x_1 = (c_hw_bit_48_undef);
        break;
    }
    id2373out_dout[(getCycle()+2)%3] = (id2373x_1);
  }
  HWRawBits<2> id1713out_result;

  { // Node ID: 1713 (NodeSlice)
    const HWOffsetFix<11,-11,UNSIGNED> &id1713in_a = id1711out_o;

    id1713out_result = (slice<0,2>(id1713in_a));
  }
  HWOffsetFix<2,-2,UNSIGNED> id1714out_output;

  { // Node ID: 1714 (NodeReinterpret)
    const HWRawBits<2> &id1714in_input = id1713out_result;

    id1714out_output = (cast_bits2fixed<2,-2,UNSIGNED>(id1714in_input));
  }
  { // Node ID: 3010 (NodeFIFO)
    const HWOffsetFix<2,-2,UNSIGNED> &id3010in_input = id1714out_output;

    id3010out_output[(getCycle()+2)%3] = id3010in_input;
  }
  HWFloat<8,24> id2309out_o;

  { // Node ID: 2309 (NodeCast)
    const HWFloat<8,12> &id2309in_i = id2300out_result;

    id2309out_o = (cast_float2float<8,24>(id2309in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2316 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2316in_output_control = id3015out_output[getCycle()%10];
    const HWFloat<8,24> &id2316in_data = id2309out_o;

    bool id2316x_1;

    (id2316x_1) = ((id2316in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2316x_1)) {
      writeOutput(m_u_out, id2316in_data);
    }
  }
  { // Node ID: 3042 (NodeConstantRawBits)
  }
  { // Node ID: 2319 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2319in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2319in_b = id3042out_value;

    id2319out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2319in_a,id2319in_b));
  }
  { // Node ID: 2606 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2606in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2606in_b = id2319out_result[getCycle()%2];

    id2606out_result[(getCycle()+1)%2] = (eq_fixed(id2606in_a,id2606in_b));
  }
  { // Node ID: 2321 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2322out_result;

  { // Node ID: 2322 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2322in_a = id2321out_io_ca_out_force_disabled;

    id2322out_result = (not_fixed(id2322in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2323out_result;

  { // Node ID: 2323 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2323in_a = id2606out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2323in_b = id2322out_result;

    HWOffsetFix<1,0,UNSIGNED> id2323x_1;

    (id2323x_1) = (and_fixed(id2323in_a,id2323in_b));
    id2323out_result = (id2323x_1);
  }
  { // Node ID: 3017 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3017in_input = id2323out_result;

    id3017out_output[(getCycle()+9)%10] = id3017in_input;
  }
  HWFloat<8,24> id2317out_o;

  { // Node ID: 2317 (NodeCast)
    const HWFloat<8,12> &id2317in_i = id1773out_result;

    id2317out_o = (cast_float2float<8,24>(id2317in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2324 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2324in_output_control = id3017out_output[getCycle()%10];
    const HWFloat<8,24> &id2324in_data = id2317out_o;

    bool id2324x_1;

    (id2324x_1) = ((id2324in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2324x_1)) {
      writeOutput(m_ca_out, id2324in_data);
    }
  }
  { // Node ID: 3041 (NodeConstantRawBits)
  }
  { // Node ID: 2327 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2327in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2327in_b = id3041out_value;

    id2327out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2327in_a,id2327in_b));
  }
  { // Node ID: 2607 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2607in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2607in_b = id2327out_result[getCycle()%2];

    id2607out_result[(getCycle()+1)%2] = (eq_fixed(id2607in_a,id2607in_b));
  }
  { // Node ID: 2329 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2330out_result;

  { // Node ID: 2330 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2330in_a = id2329out_io_x1_out_force_disabled;

    id2330out_result = (not_fixed(id2330in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2331out_result;

  { // Node ID: 2331 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2331in_a = id2607out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2331in_b = id2330out_result;

    HWOffsetFix<1,0,UNSIGNED> id2331x_1;

    (id2331x_1) = (and_fixed(id2331in_a,id2331in_b));
    id2331out_result = (id2331x_1);
  }
  { // Node ID: 3019 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3019in_input = id2331out_result;

    id3019out_output[(getCycle()+8)%9] = id3019in_input;
  }
  HWFloat<8,24> id2325out_o;

  { // Node ID: 2325 (NodeCast)
    const HWFloat<8,12> &id2325in_i = id1655out_result;

    id2325out_o = (cast_float2float<8,24>(id2325in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2332 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2332in_output_control = id3019out_output[getCycle()%9];
    const HWFloat<8,24> &id2332in_data = id2325out_o;

    bool id2332x_1;

    (id2332x_1) = ((id2332in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2332x_1)) {
      writeOutput(m_x1_out, id2332in_data);
    }
  }
  { // Node ID: 3040 (NodeConstantRawBits)
  }
  { // Node ID: 2335 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2335in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2335in_b = id3040out_value;

    id2335out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2335in_a,id2335in_b));
  }
  { // Node ID: 2608 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2608in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2608in_b = id2335out_result[getCycle()%2];

    id2608out_result[(getCycle()+1)%2] = (eq_fixed(id2608in_a,id2608in_b));
  }
  { // Node ID: 2337 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2338out_result;

  { // Node ID: 2338 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2338in_a = id2337out_io_m_out_force_disabled;

    id2338out_result = (not_fixed(id2338in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2339out_result;

  { // Node ID: 2339 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2339in_a = id2608out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2339in_b = id2338out_result;

    HWOffsetFix<1,0,UNSIGNED> id2339x_1;

    (id2339x_1) = (and_fixed(id2339in_a,id2339in_b));
    id2339out_result = (id2339x_1);
  }
  { // Node ID: 3021 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3021in_input = id2339out_result;

    id3021out_output[(getCycle()+8)%9] = id3021in_input;
  }
  HWFloat<8,24> id2333out_o;

  { // Node ID: 2333 (NodeCast)
    const HWFloat<8,12> &id2333in_i = id1661out_result;

    id2333out_o = (cast_float2float<8,24>(id2333in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2340 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2340in_output_control = id3021out_output[getCycle()%9];
    const HWFloat<8,24> &id2340in_data = id2333out_o;

    bool id2340x_1;

    (id2340x_1) = ((id2340in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2340x_1)) {
      writeOutput(m_m_out, id2340in_data);
    }
  }
  { // Node ID: 3039 (NodeConstantRawBits)
  }
  { // Node ID: 2343 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2343in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2343in_b = id3039out_value;

    id2343out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2343in_a,id2343in_b));
  }
  { // Node ID: 2609 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2609in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2609in_b = id2343out_result[getCycle()%2];

    id2609out_result[(getCycle()+1)%2] = (eq_fixed(id2609in_a,id2609in_b));
  }
  { // Node ID: 2345 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2346out_result;

  { // Node ID: 2346 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2346in_a = id2345out_io_f_out_force_disabled;

    id2346out_result = (not_fixed(id2346in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2347out_result;

  { // Node ID: 2347 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2347in_a = id2609out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2347in_b = id2346out_result;

    HWOffsetFix<1,0,UNSIGNED> id2347x_1;

    (id2347x_1) = (and_fixed(id2347in_a,id2347in_b));
    id2347out_result = (id2347x_1);
  }
  { // Node ID: 3023 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3023in_input = id2347out_result;

    id3023out_output[(getCycle()+8)%9] = id3023in_input;
  }
  HWFloat<8,24> id2341out_o;

  { // Node ID: 2341 (NodeCast)
    const HWFloat<8,12> &id2341in_i = id1685out_result;

    id2341out_o = (cast_float2float<8,24>(id2341in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2348 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2348in_output_control = id3023out_output[getCycle()%9];
    const HWFloat<8,24> &id2348in_data = id2341out_o;

    bool id2348x_1;

    (id2348x_1) = ((id2348in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2348x_1)) {
      writeOutput(m_f_out, id2348in_data);
    }
  }
  { // Node ID: 3038 (NodeConstantRawBits)
  }
  { // Node ID: 2351 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2351in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2351in_b = id3038out_value;

    id2351out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2351in_a,id2351in_b));
  }
  { // Node ID: 2610 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2610in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2610in_b = id2351out_result[getCycle()%2];

    id2610out_result[(getCycle()+1)%2] = (eq_fixed(id2610in_a,id2610in_b));
  }
  { // Node ID: 2353 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2354out_result;

  { // Node ID: 2354 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2354in_a = id2353out_io_h_out_force_disabled;

    id2354out_result = (not_fixed(id2354in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2355out_result;

  { // Node ID: 2355 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2355in_a = id2610out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2355in_b = id2354out_result;

    HWOffsetFix<1,0,UNSIGNED> id2355x_1;

    (id2355x_1) = (and_fixed(id2355in_a,id2355in_b));
    id2355out_result = (id2355x_1);
  }
  { // Node ID: 3025 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3025in_input = id2355out_result;

    id3025out_output[(getCycle()+9)%10] = id3025in_input;
  }
  HWFloat<8,24> id2349out_o;

  { // Node ID: 2349 (NodeCast)
    const HWFloat<8,12> &id2349in_i = id1667out_result;

    id2349out_o = (cast_float2float<8,24>(id2349in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2356 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2356in_output_control = id3025out_output[getCycle()%10];
    const HWFloat<8,24> &id2356in_data = id2349out_o;

    bool id2356x_1;

    (id2356x_1) = ((id2356in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2356x_1)) {
      writeOutput(m_h_out, id2356in_data);
    }
  }
  { // Node ID: 3037 (NodeConstantRawBits)
  }
  { // Node ID: 2359 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2359in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2359in_b = id3037out_value;

    id2359out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2359in_a,id2359in_b));
  }
  { // Node ID: 2611 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2611in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2611in_b = id2359out_result[getCycle()%2];

    id2611out_result[(getCycle()+1)%2] = (eq_fixed(id2611in_a,id2611in_b));
  }
  { // Node ID: 2361 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2362out_result;

  { // Node ID: 2362 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2362in_a = id2361out_io_d_out_force_disabled;

    id2362out_result = (not_fixed(id2362in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2363out_result;

  { // Node ID: 2363 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2363in_a = id2611out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2363in_b = id2362out_result;

    HWOffsetFix<1,0,UNSIGNED> id2363x_1;

    (id2363x_1) = (and_fixed(id2363in_a,id2363in_b));
    id2363out_result = (id2363x_1);
  }
  { // Node ID: 3027 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3027in_input = id2363out_result;

    id3027out_output[(getCycle()+8)%9] = id3027in_input;
  }
  HWFloat<8,24> id2357out_o;

  { // Node ID: 2357 (NodeCast)
    const HWFloat<8,12> &id2357in_i = id1679out_result;

    id2357out_o = (cast_float2float<8,24>(id2357in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2364 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2364in_output_control = id3027out_output[getCycle()%9];
    const HWFloat<8,24> &id2364in_data = id2357out_o;

    bool id2364x_1;

    (id2364x_1) = ((id2364in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2364x_1)) {
      writeOutput(m_d_out, id2364in_data);
    }
  }
  { // Node ID: 3036 (NodeConstantRawBits)
  }
  { // Node ID: 2367 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2367in_a = id3033out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2367in_b = id3036out_value;

    id2367out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2367in_a,id2367in_b));
  }
  { // Node ID: 2612 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2612in_a = id2712out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2612in_b = id2367out_result[getCycle()%2];

    id2612out_result[(getCycle()+1)%2] = (eq_fixed(id2612in_a,id2612in_b));
  }
  { // Node ID: 2369 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2370out_result;

  { // Node ID: 2370 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2370in_a = id2369out_io_j_out_force_disabled;

    id2370out_result = (not_fixed(id2370in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2371out_result;

  { // Node ID: 2371 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2371in_a = id2612out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2371in_b = id2370out_result;

    HWOffsetFix<1,0,UNSIGNED> id2371x_1;

    (id2371x_1) = (and_fixed(id2371in_a,id2371in_b));
    id2371out_result = (id2371x_1);
  }
  { // Node ID: 3029 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3029in_input = id2371out_result;

    id3029out_output[(getCycle()+9)%10] = id3029in_input;
  }
  HWFloat<8,24> id2365out_o;

  { // Node ID: 2365 (NodeCast)
    const HWFloat<8,12> &id2365in_i = id1673out_result;

    id2365out_o = (cast_float2float<8,24>(id2365in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2372 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2372in_output_control = id3029out_output[getCycle()%10];
    const HWFloat<8,24> &id2372in_data = id2365out_o;

    bool id2372x_1;

    (id2372x_1) = ((id2372in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2372x_1)) {
      writeOutput(m_j_out, id2372in_data);
    }
  }
  { // Node ID: 2386 (NodeConstantRawBits)
  }
  { // Node ID: 3035 (NodeConstantRawBits)
  }
  { // Node ID: 2383 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (9l)))
  { // Node ID: 2384 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2384in_enable = id3035out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2384in_max = id2383out_value;

    HWOffsetFix<49,0,UNSIGNED> id2384x_1;
    HWOffsetFix<1,0,UNSIGNED> id2384x_2;
    HWOffsetFix<1,0,UNSIGNED> id2384x_3;
    HWOffsetFix<49,0,UNSIGNED> id2384x_4t_1e_1;

    id2384out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2384st_count)));
    (id2384x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2384st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2384x_2) = (gte_fixed((id2384x_1),id2384in_max));
    (id2384x_3) = (and_fixed((id2384x_2),id2384in_enable));
    id2384out_wrap = (id2384x_3);
    if((id2384in_enable.getValueAsBool())) {
      if(((id2384x_3).getValueAsBool())) {
        (id2384st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2384x_4t_1e_1) = (id2384x_1);
        (id2384st_count) = (id2384x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id2385out_output;

  { // Node ID: 2385 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id2385in_input = id2384out_count;

    id2385out_output = id2385in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2387 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id2387in_load = id2386out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id2387in_data = id2385out_output;

    bool id2387x_1;

    (id2387x_1) = ((id2387in_load.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2387x_1)) {
      setMappedRegValue("current_run_cycle_count", id2387in_data);
    }
  }
  { // Node ID: 3034 (NodeConstantRawBits)
  }
  { // Node ID: 2389 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 2390 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2390in_enable = id3034out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2390in_max = id2389out_value;

    HWOffsetFix<49,0,UNSIGNED> id2390x_1;
    HWOffsetFix<1,0,UNSIGNED> id2390x_2;
    HWOffsetFix<1,0,UNSIGNED> id2390x_3;
    HWOffsetFix<49,0,UNSIGNED> id2390x_4t_1e_1;

    id2390out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2390st_count)));
    (id2390x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2390st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2390x_2) = (gte_fixed((id2390x_1),id2390in_max));
    (id2390x_3) = (and_fixed((id2390x_2),id2390in_enable));
    id2390out_wrap = (id2390x_3);
    if((id2390in_enable.getValueAsBool())) {
      if(((id2390x_3).getValueAsBool())) {
        (id2390st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2390x_4t_1e_1) = (id2390x_1);
        (id2390st_count) = (id2390x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 2392 (NodeInputMappedReg)
  }
  { // Node ID: 2613 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id2613in_a = id2390out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id2613in_b = id2392out_run_cycle_count;

    id2613out_result[(getCycle()+1)%2] = (eq_fixed(id2613in_a,id2613in_b));
  }
  if ( (getFillLevel() >= (7l)))
  { // Node ID: 2391 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id2391in_start = id2613out_result[getCycle()%2];

    if((id2391in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
