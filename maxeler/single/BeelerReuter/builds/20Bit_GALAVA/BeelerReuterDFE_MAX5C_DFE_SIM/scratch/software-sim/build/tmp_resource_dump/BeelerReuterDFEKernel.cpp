#include "stdsimheader.h"
#include "BeelerReuterDFEKernel.h"

namespace maxcompilersim {

BeelerReuterDFEKernel::BeelerReuterDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 11, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x00al))))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_12_bits((HWFloat<8,12>(varint_u<20>(0x3eb33l))))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x001l))))
, c_hw_fix_11_0_sgn_bits_1((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x07fl))))
, c_hw_fix_12_n11_uns_bits((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x800l))))
, c_hw_fix_11_0_sgn_bits_2((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x000l))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_3((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x0ffl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_12_n11_uns_bits_1((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_n11_uns_undef((HWOffsetFix<12,-11,UNSIGNED>()))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_11_0_uns_bits_2((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x000l))))
, c_hw_flt_8_12_bits_1((HWFloat<8,12>(varint_u<20>(0x00000l))))
, c_hw_flt_8_12_undef((HWFloat<8,12>()))
, c_hw_flt_8_12_bits_2((HWFloat<8,12>(varint_u<20>(0x7fc00l))))
, c_hw_flt_8_12_bits_3((HWFloat<8,12>(varint_u<20>(0x3a831l))))
, c_hw_flt_8_12_bits_4((HWFloat<8,12>(varint_u<20>(0x3e4cdl))))
, c_hw_flt_8_12_bits_5((HWFloat<8,12>(varint_u<20>(0x41b80l))))
, c_hw_flt_8_12_bits_6((HWFloat<8,12>(varint_u<20>(0x3f800l))))
, c_hw_flt_8_12_bits_7((HWFloat<8,12>(varint_u<20>(0x3f4cdl))))
, c_hw_flt_8_12_bits_8((HWFloat<8,12>(varint_u<20>(0x3e010l))))
, c_hw_flt_8_12_bits_9((HWFloat<8,12>(varint_u<20>(0x3fd9al))))
, c_hw_flt_8_12_bits_10((HWFloat<8,12>(varint_u<20>(0x3d614l))))
, c_hw_fix_8_n8_uns_bits((HWOffsetFix<8,-8,UNSIGNED>(varint_u<8>(0xb1l))))
, c_hw_flt_8_12_bits_11((HWFloat<8,12>(varint_u<20>(0x3e99al))))
, c_hw_flt_8_12_bits_12((HWFloat<8,12>(varint_u<20>(0x3b44al))))
, c_hw_flt_8_12_bits_13((HWFloat<8,12>(varint_u<20>(0x42480l))))
, c_hw_flt_8_12_bits_14((HWFloat<8,12>(varint_u<20>(0x3db85l))))
, c_hw_flt_8_12_bits_15((HWFloat<8,12>(varint_u<20>(0xc2a4al))))
, c_hw_flt_8_12_bits_16((HWFloat<8,12>(varint_u<20>(0x41507l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0xffl))))
, c_hw_bit_11_bits((HWRawBits<11>(varint_u<11>(0x000l))))
, c_hw_bit_19_bits((HWRawBits<19>(varint_u<19>(0x7f800l))))
, c_hw_fix_9_0_sgn_bits((HWOffsetFix<9,0,TWOSCOMPLEMENT>(varint_u<9>(0x07fl))))
, c_hw_fix_9_0_sgn_bits_1((HWOffsetFix<9,0,TWOSCOMPLEMENT>(varint_u<9>(0x001l))))
, c_hw_fix_9_0_sgn_bits_2((HWOffsetFix<9,0,TWOSCOMPLEMENT>(varint_u<9>(0x000l))))
, c_hw_fix_9_0_sgn_undef((HWOffsetFix<9,0,TWOSCOMPLEMENT>()))
, c_hw_flt_8_12_bits_17((HWFloat<8,12>(varint_u<20>(0x7f800l))))
, c_hw_flt_8_12_bits_18((HWFloat<8,12>(varint_u<20>(0xff800l))))
, c_hw_flt_8_12_bits_19((HWFloat<8,12>(varint_u<20>(0x3f317l))))
, c_hw_flt_8_12_bits_20((HWFloat<8,12>(varint_u<20>(0x3d23dl))))
, c_hw_flt_8_12_bits_21((HWFloat<8,12>(varint_u<20>(0x42aa0l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_24_n23_uns_bits((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0xb8aa3bl))))
, c_hw_fix_10_0_sgn_undef((HWOffsetFix<10,0,TWOSCOMPLEMENT>()))
, c_hw_fix_12_n12_uns_undef((HWOffsetFix<12,-12,UNSIGNED>()))
, c_hw_fix_8_n14_uns_undef((HWOffsetFix<8,-14,UNSIGNED>()))
, c_hw_flt_8_12_4_0val((HWFloat<8,12>(varint_u<20>(0x40800l))))
, c_hw_flt_8_12_bits_22((HWFloat<8,12>(varint_u<20>(0x3da3dl))))
, c_hw_flt_8_12_bits_23((HWFloat<8,12>(varint_u<20>(0x42540l))))
, c_hw_flt_8_12_bits_24((HWFloat<8,12>(varint_u<20>(0xbd23dl))))
, c_hw_flt_8_12_bits_25((HWFloat<8,12>(varint_u<20>(0x429a0l))))
, c_hw_flt_8_12_bits_26((HWFloat<8,12>(varint_u<20>(0x420c0l))))
, c_hw_flt_8_12_bits_27((HWFloat<8,12>(varint_u<20>(0x3a031l))))
, c_hw_flt_8_12_bits_28((HWFloat<8,12>(varint_u<20>(0x3daa0l))))
, c_hw_flt_8_12_bits_29((HWFloat<8,12>(varint_u<20>(0x3d698l))))
, c_hw_flt_8_12_bits_30((HWFloat<8,12>(varint_u<20>(0x3aaa6l))))
, c_hw_flt_8_12_bits_31((HWFloat<8,12>(varint_u<20>(0xbd75cl))))
, c_hw_flt_8_12_bits_32((HWFloat<8,12>(varint_u<20>(0x41a00l))))
, c_hw_flt_8_12_bits_33((HWFloat<8,12>(varint_u<20>(0x423c0l))))
, c_hw_flt_8_12_bits_34((HWFloat<8,12>(varint_u<20>(0x42200l))))
, c_hw_flt_8_12_bits_35((HWFloat<8,12>(varint_u<20>(0xbdccdl))))
, c_hw_flt_8_12_bits_36((HWFloat<8,12>(varint_u<20>(0xbd656l))))
, c_hw_flt_8_12_bits_37((HWFloat<8,12>(varint_u<20>(0x42900l))))
, c_hw_flt_8_12_n0_25val((HWFloat<8,12>(varint_u<20>(0xbe800l))))
, c_hw_flt_8_12_bits_38((HWFloat<8,12>(varint_u<20>(0xbda7fl))))
, c_hw_flt_8_12_bits_39((HWFloat<8,12>(varint_u<20>(0x41b40l))))
, c_hw_flt_8_12_bits_40((HWFloat<8,12>(varint_u<20>(0x429c0l))))
, c_hw_flt_8_12_bits_41((HWFloat<8,12>(varint_u<20>(0xbe4cdl))))
, c_hw_flt_8_12_bits_42((HWFloat<8,12>(varint_u<20>(0x42000l))))
, c_hw_flt_8_12_bits_43((HWFloat<8,12>(varint_u<20>(0x3dc29l))))
, c_hw_flt_8_12_bits_44((HWFloat<8,12>(varint_u<20>(0xbc23dl))))
, c_hw_flt_8_12_bits_45((HWFloat<8,12>(varint_u<20>(0x40a00l))))
, c_hw_flt_8_12_bits_46((HWFloat<8,12>(varint_u<20>(0xbd937l))))
, c_hw_flt_8_12_bits_47((HWFloat<8,12>(varint_u<20>(0x3d8f6l))))
, c_hw_flt_8_12_bits_48((HWFloat<8,12>(varint_u<20>(0xbc8b4l))))
, c_hw_flt_8_12_bits_49((HWFloat<8,12>(varint_u<20>(0x42300l))))
, c_hw_flt_8_12_bits_50((HWFloat<8,12>(varint_u<20>(0x3d4cdl))))
, c_hw_flt_8_12_bits_51((HWFloat<8,12>(varint_u<20>(0x3c44al))))
, c_hw_flt_8_12_bits_52((HWFloat<8,12>(varint_u<20>(0xbc031l))))
, c_hw_flt_8_12_bits_53((HWFloat<8,12>(varint_u<20>(0x41e00l))))
, c_hw_flt_8_12_bits_54((HWFloat<8,12>(varint_u<20>(0x3e19al))))
, c_hw_flt_8_12_bits_55((HWFloat<8,12>(varint_u<20>(0x3bd50l))))
, c_hw_flt_8_12_bits_56((HWFloat<8,12>(varint_u<20>(0xbca3dl))))
, c_hw_flt_8_12_bits_57((HWFloat<8,12>(varint_u<20>(0x41f00l))))
, c_hw_flt_8_12_bits_58((HWFloat<8,12>(varint_u<20>(0xb3d6cl))))
, c_hw_flt_8_12_bits_59((HWFloat<8,12>(varint_u<20>(0x33d6cl))))
, c_hw_bit_48_undef((HWRawBits<48>()))
, c_hw_fix_2_n2_uns_undef((HWOffsetFix<2,-2,UNSIGNED>()))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 8 (NodeConstantRawBits)
    id8out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 3033 (NodeConstantRawBits)
    id3033out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 3298 (NodeConstantRawBits)
    id3298out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2313 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 9 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 3297 (NodeConstantRawBits)
    id3297out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 3296 (NodeConstantRawBits)
    id3296out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 1814 (NodeConstantRawBits)
    id1814out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2616 (NodeConstantRawBits)
    id2616out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3283 (NodeConstantRawBits)
    id3283out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3280 (NodeConstantRawBits)
    id3280out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1824 (NodeConstantRawBits)
    id1824out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1808 (NodeConstantRawBits)
    id1808out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1833 (NodeConstantRawBits)
    id1833out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1834 (NodeConstantRawBits)
    id1834out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1836 (NodeConstantRawBits)
    id1836out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2395 (NodeConstantRawBits)
    id2395out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3279 (NodeConstantRawBits)
    id3279out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 1895 (NodeConstantRawBits)
    id1895out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2618 (NodeConstantRawBits)
    id2618out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3275 (NodeConstantRawBits)
    id3275out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3272 (NodeConstantRawBits)
    id3272out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1905 (NodeConstantRawBits)
    id1905out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1889 (NodeConstantRawBits)
    id1889out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1914 (NodeConstantRawBits)
    id1914out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1915 (NodeConstantRawBits)
    id1915out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1917 (NodeConstantRawBits)
    id1917out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2396 (NodeConstantRawBits)
    id2396out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3271 (NodeConstantRawBits)
    id3271out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 5 (NodeConstantRawBits)
    id5out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 2021 (NodeConstantRawBits)
    id2021out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 3270 (NodeConstantRawBits)
    id3270out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 3269 (NodeConstantRawBits)
    id3269out_value = (c_hw_flt_8_12_bits_5);
  }
  { // Node ID: 3268 (NodeConstantRawBits)
    id3268out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 1977 (NodeConstantRawBits)
    id1977out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2620 (NodeConstantRawBits)
    id2620out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3264 (NodeConstantRawBits)
    id3264out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3261 (NodeConstantRawBits)
    id3261out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1987 (NodeConstantRawBits)
    id1987out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1971 (NodeConstantRawBits)
    id1971out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1996 (NodeConstantRawBits)
    id1996out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1997 (NodeConstantRawBits)
    id1997out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1999 (NodeConstantRawBits)
    id1999out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2397 (NodeConstantRawBits)
    id2397out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3260 (NodeConstantRawBits)
    id3260out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 2024 (NodeConstantRawBits)
    id2024out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 3259 (NodeConstantRawBits)
    id3259out_value = (c_hw_flt_8_12_bits_7);
  }
  { // Node ID: 2160 (NodeConstantRawBits)
    id2160out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2622 (NodeConstantRawBits)
    id2622out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3255 (NodeConstantRawBits)
    id3255out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3252 (NodeConstantRawBits)
    id3252out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 2170 (NodeConstantRawBits)
    id2170out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2154 (NodeConstantRawBits)
    id2154out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 2179 (NodeConstantRawBits)
    id2179out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2180 (NodeConstantRawBits)
    id2180out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 2182 (NodeConstantRawBits)
    id2182out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2398 (NodeConstantRawBits)
    id2398out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3251 (NodeConstantRawBits)
    id3251out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3250 (NodeConstantRawBits)
    id3250out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 2245 (NodeConstantRawBits)
    id2245out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2624 (NodeConstantRawBits)
    id2624out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3246 (NodeConstantRawBits)
    id3246out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3243 (NodeConstantRawBits)
    id3243out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 2255 (NodeConstantRawBits)
    id2255out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2239 (NodeConstantRawBits)
    id2239out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 2264 (NodeConstantRawBits)
    id2264out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2265 (NodeConstantRawBits)
    id2265out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 2267 (NodeConstantRawBits)
    id2267out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2399 (NodeConstantRawBits)
    id2399out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3242 (NodeConstantRawBits)
    id3242out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3181 (NodeConstantRawBits)
    id3181out_value = (c_hw_flt_8_12_bits_8);
  }
  { // Node ID: 594 (NodeConstantRawBits)
    id594out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2638 (NodeConstantRawBits)
    id2638out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3178 (NodeConstantRawBits)
    id3178out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3175 (NodeConstantRawBits)
    id3175out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 604 (NodeConstantRawBits)
    id604out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 588 (NodeConstantRawBits)
    id588out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 613 (NodeConstantRawBits)
    id613out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 614 (NodeConstantRawBits)
    id614out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 616 (NodeConstantRawBits)
    id616out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2406 (NodeConstantRawBits)
    id2406out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3174 (NodeConstantRawBits)
    id3174out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3173 (NodeConstantRawBits)
    id3173out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3172 (NodeConstantRawBits)
    id3172out_value = (c_hw_flt_8_12_bits_9);
  }
  { // Node ID: 678 (NodeConstantRawBits)
    id678out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2640 (NodeConstantRawBits)
    id2640out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3168 (NodeConstantRawBits)
    id3168out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3165 (NodeConstantRawBits)
    id3165out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 688 (NodeConstantRawBits)
    id688out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 672 (NodeConstantRawBits)
    id672out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 697 (NodeConstantRawBits)
    id697out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 698 (NodeConstantRawBits)
    id698out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 700 (NodeConstantRawBits)
    id700out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2407 (NodeConstantRawBits)
    id2407out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3164 (NodeConstantRawBits)
    id3164out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3163 (NodeConstantRawBits)
    id3163out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3161 (NodeConstantRawBits)
    id3161out_value = (c_hw_flt_8_12_bits_10);
  }
  { // Node ID: 764 (NodeConstantRawBits)
    id764out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2642 (NodeConstantRawBits)
    id2642out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3158 (NodeConstantRawBits)
    id3158out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3155 (NodeConstantRawBits)
    id3155out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 774 (NodeConstantRawBits)
    id774out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 758 (NodeConstantRawBits)
    id758out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 783 (NodeConstantRawBits)
    id783out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 784 (NodeConstantRawBits)
    id784out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 786 (NodeConstantRawBits)
    id786out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2408 (NodeConstantRawBits)
    id2408out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3154 (NodeConstantRawBits)
    id3154out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 847 (NodeConstantRawBits)
    id847out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2644 (NodeConstantRawBits)
    id2644out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 832 (NodeConstantRawBits)
    id832out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3150 (NodeConstantRawBits)
    id3150out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3147 (NodeConstantRawBits)
    id3147out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 857 (NodeConstantRawBits)
    id857out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 841 (NodeConstantRawBits)
    id841out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 866 (NodeConstantRawBits)
    id866out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 867 (NodeConstantRawBits)
    id867out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 869 (NodeConstantRawBits)
    id869out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2409 (NodeConstantRawBits)
    id2409out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3146 (NodeConstantRawBits)
    id3146out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3145 (NodeConstantRawBits)
    id3145out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3144 (NodeConstantRawBits)
    id3144out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3143 (NodeConstantRawBits)
    id3143out_value = (c_hw_flt_8_12_bits_11);
  }
  { // Node ID: 932 (NodeConstantRawBits)
    id932out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2646 (NodeConstantRawBits)
    id2646out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3139 (NodeConstantRawBits)
    id3139out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3136 (NodeConstantRawBits)
    id3136out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 942 (NodeConstantRawBits)
    id942out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 926 (NodeConstantRawBits)
    id926out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 951 (NodeConstantRawBits)
    id951out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 952 (NodeConstantRawBits)
    id952out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 954 (NodeConstantRawBits)
    id954out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2410 (NodeConstantRawBits)
    id2410out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3135 (NodeConstantRawBits)
    id3135out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3134 (NodeConstantRawBits)
    id3134out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_8_12_bits_12);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_8_12_bits_13);
  }
  { // Node ID: 3133 (NodeConstantRawBits)
    id3133out_value = (c_hw_flt_8_12_bits_14);
  }
  { // Node ID: 3056 (NodeConstantRawBits)
    id3056out_value = (c_hw_flt_8_12_bits_15);
  }
  { // Node ID: 3055 (NodeConstantRawBits)
    id3055out_value = (c_hw_flt_8_12_bits_16);
  }
  { // Node ID: 3050 (NodeConstantRawBits)
    id3050out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 1730 (NodeConstantRawBits)
    id1730out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3049 (NodeConstantRawBits)
    id3049out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 3048 (NodeConstantRawBits)
    id3048out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 1740 (NodeConstantRawBits)
    id1740out_value = (c_hw_bit_19_bits);
  }
  { // Node ID: 1701 (NodeConstantRawBits)
    id1701out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 3047 (NodeConstantRawBits)
    id3047out_value = (c_hw_fix_9_0_sgn_bits);
  }
  { // Node ID: 1706 (NodeConstantRawBits)
    id1706out_value = (c_hw_fix_9_0_sgn_bits_1);
  }
  { // Node ID: 3045 (NodeConstantRawBits)
    id3045out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3044 (NodeConstantRawBits)
    id3044out_value = (c_hw_flt_8_12_bits_17);
  }
  { // Node ID: 3043 (NodeConstantRawBits)
    id3043out_value = (c_hw_flt_8_12_bits_18);
  }
  { // Node ID: 1753 (NodeConstantRawBits)
    id1753out_value = (c_hw_flt_8_12_bits_19);
  }
  { // Node ID: 16 (NodeInputMappedReg)
    registerMappedRegister("u_in", Data(20));
  }
  { // Node ID: 12 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 3295 (NodeConstantRawBits)
    id3295out_value = (c_hw_flt_8_12_bits_20);
  }
  { // Node ID: 3294 (NodeConstantRawBits)
    id3294out_value = (c_hw_flt_8_12_bits_21);
  }
  { // Node ID: 2097 (NodeConstantRawBits)
    id2097out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3293 (NodeConstantRawBits)
    id3293out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 2030 (NodeConstantRawBits)
    id2030out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2035 (NodeConstantRawBits)
    id2035out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 2066 (NodeConstantRawBits)
    id2066out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2614 (NodeConstantRawBits)
    id2614out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 2106 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id2106sta_rom_store, 12, 64); 
  }
  { // Node ID: 2051 (NodeConstantRawBits)
    id2051out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3292 (NodeConstantRawBits)
    id3292out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3291 (NodeConstantRawBits)
    id3291out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3290 (NodeConstantRawBits)
    id3290out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3289 (NodeConstantRawBits)
    id3289out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 2076 (NodeConstantRawBits)
    id2076out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2060 (NodeConstantRawBits)
    id2060out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 2085 (NodeConstantRawBits)
    id2085out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2086 (NodeConstantRawBits)
    id2086out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 2088 (NodeConstantRawBits)
    id2088out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2394 (NodeConstantRawBits)
    id2394out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3288 (NodeConstantRawBits)
    id3288out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3287 (NodeConstantRawBits)
    id3287out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3286 (NodeConstantRawBits)
    id3286out_value = (c_hw_flt_8_12_bits_22);
  }
  { // Node ID: 3285 (NodeConstantRawBits)
    id3285out_value = (c_hw_flt_8_12_bits_23);
  }
  { // Node ID: 1845 (NodeConstantRawBits)
    id1845out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3284 (NodeConstantRawBits)
    id3284out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1778 (NodeConstantRawBits)
    id1778out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1783 (NodeConstantRawBits)
    id1783out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1854 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1854sta_rom_store, 12, 64); 
  }
  { // Node ID: 1799 (NodeConstantRawBits)
    id1799out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3282 (NodeConstantRawBits)
    id3282out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3281 (NodeConstantRawBits)
    id3281out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3278 (NodeConstantRawBits)
    id3278out_value = (c_hw_flt_8_12_bits_20);
  }
  { // Node ID: 3277 (NodeConstantRawBits)
    id3277out_value = (c_hw_flt_8_12_bits_23);
  }
  { // Node ID: 1926 (NodeConstantRawBits)
    id1926out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3276 (NodeConstantRawBits)
    id3276out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1859 (NodeConstantRawBits)
    id1859out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1864 (NodeConstantRawBits)
    id1864out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1935 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1935sta_rom_store, 12, 64); 
  }
  { // Node ID: 1880 (NodeConstantRawBits)
    id1880out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3274 (NodeConstantRawBits)
    id3274out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3273 (NodeConstantRawBits)
    id3273out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3267 (NodeConstantRawBits)
    id3267out_value = (c_hw_flt_8_12_bits_24);
  }
  { // Node ID: 3266 (NodeConstantRawBits)
    id3266out_value = (c_hw_flt_8_12_bits_5);
  }
  { // Node ID: 2008 (NodeConstantRawBits)
    id2008out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3265 (NodeConstantRawBits)
    id3265out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1941 (NodeConstantRawBits)
    id1941out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1946 (NodeConstantRawBits)
    id1946out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 2017 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id2017sta_rom_store, 12, 64); 
  }
  { // Node ID: 1962 (NodeConstantRawBits)
    id1962out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3263 (NodeConstantRawBits)
    id3263out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3262 (NodeConstantRawBits)
    id3262out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3258 (NodeConstantRawBits)
    id3258out_value = (c_hw_flt_8_12_bits_20);
  }
  { // Node ID: 3257 (NodeConstantRawBits)
    id3257out_value = (c_hw_flt_8_12_bits_25);
  }
  { // Node ID: 2191 (NodeConstantRawBits)
    id2191out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3256 (NodeConstantRawBits)
    id3256out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 2124 (NodeConstantRawBits)
    id2124out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2129 (NodeConstantRawBits)
    id2129out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 2200 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id2200sta_rom_store, 12, 64); 
  }
  { // Node ID: 2145 (NodeConstantRawBits)
    id2145out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3254 (NodeConstantRawBits)
    id3254out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3253 (NodeConstantRawBits)
    id3253out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3249 (NodeConstantRawBits)
    id3249out_value = (c_hw_flt_8_12_bits_20);
  }
  { // Node ID: 3248 (NodeConstantRawBits)
    id3248out_value = (c_hw_flt_8_12_bits_26);
  }
  { // Node ID: 2276 (NodeConstantRawBits)
    id2276out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3247 (NodeConstantRawBits)
    id3247out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 2209 (NodeConstantRawBits)
    id2209out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 2214 (NodeConstantRawBits)
    id2214out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 2285 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id2285sta_rom_store, 12, 64); 
  }
  { // Node ID: 2230 (NodeConstantRawBits)
    id2230out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3245 (NodeConstantRawBits)
    id3245out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3244 (NodeConstantRawBits)
    id3244out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3241 (NodeConstantRawBits)
    id3241out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 24 (NodeInputMappedReg)
    registerMappedRegister("x1_in", Data(20));
  }
  { // Node ID: 3240 (NodeConstantRawBits)
    id3240out_value = (c_hw_flt_8_12_bits_27);
  }
  { // Node ID: 3239 (NodeConstantRawBits)
    id3239out_value = (c_hw_flt_8_12_bits_28);
  }
  { // Node ID: 3238 (NodeConstantRawBits)
    id3238out_value = (c_hw_flt_8_12_bits_13);
  }
  { // Node ID: 117 (NodeConstantRawBits)
    id117out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3237 (NodeConstantRawBits)
    id3237out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 50 (NodeConstantRawBits)
    id50out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 55 (NodeConstantRawBits)
    id55out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 86 (NodeConstantRawBits)
    id86out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2626 (NodeConstantRawBits)
    id2626out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 126 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id126sta_rom_store, 12, 64); 
  }
  { // Node ID: 71 (NodeConstantRawBits)
    id71out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3236 (NodeConstantRawBits)
    id3236out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3235 (NodeConstantRawBits)
    id3235out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3234 (NodeConstantRawBits)
    id3234out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3233 (NodeConstantRawBits)
    id3233out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 96 (NodeConstantRawBits)
    id96out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 80 (NodeConstantRawBits)
    id80out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 105 (NodeConstantRawBits)
    id105out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 106 (NodeConstantRawBits)
    id106out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 108 (NodeConstantRawBits)
    id108out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2400 (NodeConstantRawBits)
    id2400out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3232 (NodeConstantRawBits)
    id3232out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3231 (NodeConstantRawBits)
    id3231out_value = (c_hw_flt_8_12_bits_29);
  }
  { // Node ID: 3230 (NodeConstantRawBits)
    id3230out_value = (c_hw_flt_8_12_bits_13);
  }
  { // Node ID: 200 (NodeConstantRawBits)
    id200out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3229 (NodeConstantRawBits)
    id3229out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 133 (NodeConstantRawBits)
    id133out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 138 (NodeConstantRawBits)
    id138out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 169 (NodeConstantRawBits)
    id169out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2628 (NodeConstantRawBits)
    id2628out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 209 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id209sta_rom_store, 12, 64); 
  }
  { // Node ID: 154 (NodeConstantRawBits)
    id154out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3228 (NodeConstantRawBits)
    id3228out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3227 (NodeConstantRawBits)
    id3227out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3226 (NodeConstantRawBits)
    id3226out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3225 (NodeConstantRawBits)
    id3225out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 179 (NodeConstantRawBits)
    id179out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 163 (NodeConstantRawBits)
    id163out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 188 (NodeConstantRawBits)
    id188out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 189 (NodeConstantRawBits)
    id189out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 191 (NodeConstantRawBits)
    id191out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2401 (NodeConstantRawBits)
    id2401out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3224 (NodeConstantRawBits)
    id3224out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3223 (NodeConstantRawBits)
    id3223out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3222 (NodeConstantRawBits)
    id3222out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3221 (NodeConstantRawBits)
    id3221out_value = (c_hw_flt_8_12_bits_30);
  }
  { // Node ID: 3220 (NodeConstantRawBits)
    id3220out_value = (c_hw_flt_8_12_bits_31);
  }
  { // Node ID: 3219 (NodeConstantRawBits)
    id3219out_value = (c_hw_flt_8_12_bits_32);
  }
  { // Node ID: 285 (NodeConstantRawBits)
    id285out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3218 (NodeConstantRawBits)
    id3218out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 218 (NodeConstantRawBits)
    id218out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 223 (NodeConstantRawBits)
    id223out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 254 (NodeConstantRawBits)
    id254out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2630 (NodeConstantRawBits)
    id2630out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 294 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id294sta_rom_store, 12, 64); 
  }
  { // Node ID: 239 (NodeConstantRawBits)
    id239out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3217 (NodeConstantRawBits)
    id3217out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3216 (NodeConstantRawBits)
    id3216out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3215 (NodeConstantRawBits)
    id3215out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3214 (NodeConstantRawBits)
    id3214out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 264 (NodeConstantRawBits)
    id264out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 248 (NodeConstantRawBits)
    id248out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 273 (NodeConstantRawBits)
    id273out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 274 (NodeConstantRawBits)
    id274out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 276 (NodeConstantRawBits)
    id276out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2402 (NodeConstantRawBits)
    id2402out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3213 (NodeConstantRawBits)
    id3213out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3212 (NodeConstantRawBits)
    id3212out_value = (c_hw_flt_8_12_bits_24);
  }
  { // Node ID: 3211 (NodeConstantRawBits)
    id3211out_value = (c_hw_flt_8_12_bits_32);
  }
  { // Node ID: 368 (NodeConstantRawBits)
    id368out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3210 (NodeConstantRawBits)
    id3210out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 301 (NodeConstantRawBits)
    id301out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 306 (NodeConstantRawBits)
    id306out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 337 (NodeConstantRawBits)
    id337out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2632 (NodeConstantRawBits)
    id2632out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 377 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id377sta_rom_store, 12, 64); 
  }
  { // Node ID: 322 (NodeConstantRawBits)
    id322out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3209 (NodeConstantRawBits)
    id3209out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3208 (NodeConstantRawBits)
    id3208out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3207 (NodeConstantRawBits)
    id3207out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3206 (NodeConstantRawBits)
    id3206out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 347 (NodeConstantRawBits)
    id347out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 331 (NodeConstantRawBits)
    id331out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 356 (NodeConstantRawBits)
    id356out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 357 (NodeConstantRawBits)
    id357out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 359 (NodeConstantRawBits)
    id359out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2403 (NodeConstantRawBits)
    id2403out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3205 (NodeConstantRawBits)
    id3205out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3204 (NodeConstantRawBits)
    id3204out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3203 (NodeConstantRawBits)
    id3203out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 3202 (NodeConstantRawBits)
    id3202out_value = (c_hw_flt_8_12_bits_33);
  }
  { // Node ID: 425 (NodeConstantRawBits)
    id425out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2634 (NodeConstantRawBits)
    id2634out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3198 (NodeConstantRawBits)
    id3198out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3195 (NodeConstantRawBits)
    id3195out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 435 (NodeConstantRawBits)
    id435out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 419 (NodeConstantRawBits)
    id419out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 444 (NodeConstantRawBits)
    id444out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 445 (NodeConstantRawBits)
    id445out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 447 (NodeConstantRawBits)
    id447out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2404 (NodeConstantRawBits)
    id2404out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3194 (NodeConstantRawBits)
    id3194out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3193 (NodeConstantRawBits)
    id3193out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3192 (NodeConstantRawBits)
    id3192out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3191 (NodeConstantRawBits)
    id3191out_value = (c_hw_flt_8_12_bits_34);
  }
  { // Node ID: 510 (NodeConstantRawBits)
    id510out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2636 (NodeConstantRawBits)
    id2636out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 3187 (NodeConstantRawBits)
    id3187out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3184 (NodeConstantRawBits)
    id3184out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 520 (NodeConstantRawBits)
    id520out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 504 (NodeConstantRawBits)
    id504out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 529 (NodeConstantRawBits)
    id529out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 530 (NodeConstantRawBits)
    id530out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 532 (NodeConstantRawBits)
    id532out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2405 (NodeConstantRawBits)
    id2405out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3183 (NodeConstantRawBits)
    id3183out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 28 (NodeInputMappedReg)
    registerMappedRegister("m_in", Data(20));
  }
  { // Node ID: 3201 (NodeConstantRawBits)
    id3201out_value = (c_hw_flt_8_12_bits_35);
  }
  { // Node ID: 3200 (NodeConstantRawBits)
    id3200out_value = (c_hw_flt_8_12_bits_33);
  }
  { // Node ID: 456 (NodeConstantRawBits)
    id456out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3199 (NodeConstantRawBits)
    id3199out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 389 (NodeConstantRawBits)
    id389out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 394 (NodeConstantRawBits)
    id394out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 465 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id465sta_rom_store, 12, 64); 
  }
  { // Node ID: 410 (NodeConstantRawBits)
    id410out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3197 (NodeConstantRawBits)
    id3197out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3196 (NodeConstantRawBits)
    id3196out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3190 (NodeConstantRawBits)
    id3190out_value = (c_hw_flt_8_12_bits_36);
  }
  { // Node ID: 3189 (NodeConstantRawBits)
    id3189out_value = (c_hw_flt_8_12_bits_37);
  }
  { // Node ID: 541 (NodeConstantRawBits)
    id541out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3188 (NodeConstantRawBits)
    id3188out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 474 (NodeConstantRawBits)
    id474out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 479 (NodeConstantRawBits)
    id479out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 550 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id550sta_rom_store, 12, 64); 
  }
  { // Node ID: 495 (NodeConstantRawBits)
    id495out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3186 (NodeConstantRawBits)
    id3186out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3185 (NodeConstantRawBits)
    id3185out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3182 (NodeConstantRawBits)
    id3182out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 36 (NodeInputMappedReg)
    registerMappedRegister("h_in", Data(20));
  }
  { // Node ID: 3180 (NodeConstantRawBits)
    id3180out_value = (c_hw_flt_8_12_bits_25);
  }
  { // Node ID: 625 (NodeConstantRawBits)
    id625out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3179 (NodeConstantRawBits)
    id3179out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 558 (NodeConstantRawBits)
    id558out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 563 (NodeConstantRawBits)
    id563out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 634 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id634sta_rom_store, 12, 64); 
  }
  { // Node ID: 579 (NodeConstantRawBits)
    id579out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3177 (NodeConstantRawBits)
    id3177out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3176 (NodeConstantRawBits)
    id3176out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3171 (NodeConstantRawBits)
    id3171out_value = (c_hw_flt_8_12_bits_38);
  }
  { // Node ID: 3170 (NodeConstantRawBits)
    id3170out_value = (c_hw_flt_8_12_bits_39);
  }
  { // Node ID: 709 (NodeConstantRawBits)
    id709out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3169 (NodeConstantRawBits)
    id3169out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 642 (NodeConstantRawBits)
    id642out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 647 (NodeConstantRawBits)
    id647out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 718 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id718sta_rom_store, 12, 64); 
  }
  { // Node ID: 663 (NodeConstantRawBits)
    id663out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3167 (NodeConstantRawBits)
    id3167out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3166 (NodeConstantRawBits)
    id3166out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3162 (NodeConstantRawBits)
    id3162out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 44 (NodeInputMappedReg)
    registerMappedRegister("j_in", Data(20));
  }
  { // Node ID: 3160 (NodeConstantRawBits)
    id3160out_value = (c_hw_flt_8_12_bits_40);
  }
  { // Node ID: 795 (NodeConstantRawBits)
    id795out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3159 (NodeConstantRawBits)
    id3159out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 728 (NodeConstantRawBits)
    id728out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 733 (NodeConstantRawBits)
    id733out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 804 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id804sta_rom_store, 12, 64); 
  }
  { // Node ID: 749 (NodeConstantRawBits)
    id749out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3157 (NodeConstantRawBits)
    id3157out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3156 (NodeConstantRawBits)
    id3156out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3153 (NodeConstantRawBits)
    id3153out_value = (c_hw_flt_8_12_bits_41);
  }
  { // Node ID: 3152 (NodeConstantRawBits)
    id3152out_value = (c_hw_flt_8_12_bits_40);
  }
  { // Node ID: 878 (NodeConstantRawBits)
    id878out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3151 (NodeConstantRawBits)
    id3151out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 811 (NodeConstantRawBits)
    id811out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 816 (NodeConstantRawBits)
    id816out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 887 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id887sta_rom_store, 12, 64); 
  }
  { // Node ID: 3149 (NodeConstantRawBits)
    id3149out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3148 (NodeConstantRawBits)
    id3148out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3142 (NodeConstantRawBits)
    id3142out_value = (c_hw_flt_8_12_bits_35);
  }
  { // Node ID: 3141 (NodeConstantRawBits)
    id3141out_value = (c_hw_flt_8_12_bits_42);
  }
  { // Node ID: 963 (NodeConstantRawBits)
    id963out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3140 (NodeConstantRawBits)
    id3140out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 896 (NodeConstantRawBits)
    id896out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 901 (NodeConstantRawBits)
    id901out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 972 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id972sta_rom_store, 12, 64); 
  }
  { // Node ID: 917 (NodeConstantRawBits)
    id917out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3138 (NodeConstantRawBits)
    id3138out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3137 (NodeConstantRawBits)
    id3137out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3132 (NodeConstantRawBits)
    id3132out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 40 (NodeInputMappedReg)
    registerMappedRegister("d_in", Data(20));
  }
  { // Node ID: 3131 (NodeConstantRawBits)
    id3131out_value = (c_hw_flt_8_12_bits_43);
  }
  { // Node ID: 3130 (NodeConstantRawBits)
    id3130out_value = (c_hw_flt_8_12_bits_44);
  }
  { // Node ID: 3129 (NodeConstantRawBits)
    id3129out_value = (c_hw_flt_8_12_bits_45);
  }
  { // Node ID: 1049 (NodeConstantRawBits)
    id1049out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3128 (NodeConstantRawBits)
    id3128out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 982 (NodeConstantRawBits)
    id982out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 987 (NodeConstantRawBits)
    id987out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1018 (NodeConstantRawBits)
    id1018out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2648 (NodeConstantRawBits)
    id2648out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1058 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1058sta_rom_store, 12, 64); 
  }
  { // Node ID: 1003 (NodeConstantRawBits)
    id1003out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3127 (NodeConstantRawBits)
    id3127out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3126 (NodeConstantRawBits)
    id3126out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3125 (NodeConstantRawBits)
    id3125out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3124 (NodeConstantRawBits)
    id3124out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1028 (NodeConstantRawBits)
    id1028out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1012 (NodeConstantRawBits)
    id1012out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1037 (NodeConstantRawBits)
    id1037out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1038 (NodeConstantRawBits)
    id1038out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1040 (NodeConstantRawBits)
    id1040out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2411 (NodeConstantRawBits)
    id2411out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3123 (NodeConstantRawBits)
    id3123out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3122 (NodeConstantRawBits)
    id3122out_value = (c_hw_flt_8_12_bits_46);
  }
  { // Node ID: 3121 (NodeConstantRawBits)
    id3121out_value = (c_hw_flt_8_12_bits_45);
  }
  { // Node ID: 1132 (NodeConstantRawBits)
    id1132out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3120 (NodeConstantRawBits)
    id3120out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1065 (NodeConstantRawBits)
    id1065out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1070 (NodeConstantRawBits)
    id1070out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1101 (NodeConstantRawBits)
    id1101out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2650 (NodeConstantRawBits)
    id2650out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1141 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1141sta_rom_store, 12, 64); 
  }
  { // Node ID: 1086 (NodeConstantRawBits)
    id1086out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3119 (NodeConstantRawBits)
    id3119out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3118 (NodeConstantRawBits)
    id3118out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3117 (NodeConstantRawBits)
    id3117out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3116 (NodeConstantRawBits)
    id3116out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1111 (NodeConstantRawBits)
    id1111out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1095 (NodeConstantRawBits)
    id1095out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1120 (NodeConstantRawBits)
    id1120out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1121 (NodeConstantRawBits)
    id1121out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1123 (NodeConstantRawBits)
    id1123out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2412 (NodeConstantRawBits)
    id2412out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3115 (NodeConstantRawBits)
    id3115out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3114 (NodeConstantRawBits)
    id3114out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3113 (NodeConstantRawBits)
    id3113out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3112 (NodeConstantRawBits)
    id3112out_value = (c_hw_flt_8_12_bits_47);
  }
  { // Node ID: 3111 (NodeConstantRawBits)
    id3111out_value = (c_hw_flt_8_12_bits_48);
  }
  { // Node ID: 3110 (NodeConstantRawBits)
    id3110out_value = (c_hw_flt_8_12_bits_49);
  }
  { // Node ID: 1217 (NodeConstantRawBits)
    id1217out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3109 (NodeConstantRawBits)
    id3109out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1150 (NodeConstantRawBits)
    id1150out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1155 (NodeConstantRawBits)
    id1155out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1186 (NodeConstantRawBits)
    id1186out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2652 (NodeConstantRawBits)
    id2652out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1226 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1226sta_rom_store, 12, 64); 
  }
  { // Node ID: 1171 (NodeConstantRawBits)
    id1171out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3108 (NodeConstantRawBits)
    id3108out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3107 (NodeConstantRawBits)
    id3107out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3106 (NodeConstantRawBits)
    id3106out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3105 (NodeConstantRawBits)
    id3105out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1196 (NodeConstantRawBits)
    id1196out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1180 (NodeConstantRawBits)
    id1180out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1205 (NodeConstantRawBits)
    id1205out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1206 (NodeConstantRawBits)
    id1206out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1208 (NodeConstantRawBits)
    id1208out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2413 (NodeConstantRawBits)
    id2413out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3104 (NodeConstantRawBits)
    id3104out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3103 (NodeConstantRawBits)
    id3103out_value = (c_hw_flt_8_12_bits_50);
  }
  { // Node ID: 3102 (NodeConstantRawBits)
    id3102out_value = (c_hw_flt_8_12_bits_49);
  }
  { // Node ID: 1300 (NodeConstantRawBits)
    id1300out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3101 (NodeConstantRawBits)
    id3101out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1233 (NodeConstantRawBits)
    id1233out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1238 (NodeConstantRawBits)
    id1238out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1269 (NodeConstantRawBits)
    id1269out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2654 (NodeConstantRawBits)
    id2654out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1309 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1309sta_rom_store, 12, 64); 
  }
  { // Node ID: 1254 (NodeConstantRawBits)
    id1254out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3100 (NodeConstantRawBits)
    id3100out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3099 (NodeConstantRawBits)
    id3099out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3098 (NodeConstantRawBits)
    id3098out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3097 (NodeConstantRawBits)
    id3097out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1279 (NodeConstantRawBits)
    id1279out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1263 (NodeConstantRawBits)
    id1263out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1288 (NodeConstantRawBits)
    id1288out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1289 (NodeConstantRawBits)
    id1289out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1291 (NodeConstantRawBits)
    id1291out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2414 (NodeConstantRawBits)
    id2414out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3096 (NodeConstantRawBits)
    id3096out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3095 (NodeConstantRawBits)
    id3095out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3094 (NodeConstantRawBits)
    id3094out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 32 (NodeInputMappedReg)
    registerMappedRegister("f_in", Data(20));
  }
  { // Node ID: 3093 (NodeConstantRawBits)
    id3093out_value = (c_hw_flt_8_12_bits_51);
  }
  { // Node ID: 3092 (NodeConstantRawBits)
    id3092out_value = (c_hw_flt_8_12_bits_52);
  }
  { // Node ID: 3091 (NodeConstantRawBits)
    id3091out_value = (c_hw_flt_8_12_bits_53);
  }
  { // Node ID: 1385 (NodeConstantRawBits)
    id1385out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3090 (NodeConstantRawBits)
    id3090out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1318 (NodeConstantRawBits)
    id1318out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1323 (NodeConstantRawBits)
    id1323out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1354 (NodeConstantRawBits)
    id1354out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2656 (NodeConstantRawBits)
    id2656out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1394 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1394sta_rom_store, 12, 64); 
  }
  { // Node ID: 1339 (NodeConstantRawBits)
    id1339out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3089 (NodeConstantRawBits)
    id3089out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3088 (NodeConstantRawBits)
    id3088out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3087 (NodeConstantRawBits)
    id3087out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3086 (NodeConstantRawBits)
    id3086out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1364 (NodeConstantRawBits)
    id1364out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1348 (NodeConstantRawBits)
    id1348out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1373 (NodeConstantRawBits)
    id1373out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1374 (NodeConstantRawBits)
    id1374out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1376 (NodeConstantRawBits)
    id1376out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2415 (NodeConstantRawBits)
    id2415out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3085 (NodeConstantRawBits)
    id3085out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3084 (NodeConstantRawBits)
    id3084out_value = (c_hw_flt_8_12_bits_54);
  }
  { // Node ID: 3083 (NodeConstantRawBits)
    id3083out_value = (c_hw_flt_8_12_bits_53);
  }
  { // Node ID: 1468 (NodeConstantRawBits)
    id1468out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3082 (NodeConstantRawBits)
    id3082out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1401 (NodeConstantRawBits)
    id1401out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1406 (NodeConstantRawBits)
    id1406out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1437 (NodeConstantRawBits)
    id1437out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2658 (NodeConstantRawBits)
    id2658out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1477 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1477sta_rom_store, 12, 64); 
  }
  { // Node ID: 1422 (NodeConstantRawBits)
    id1422out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3081 (NodeConstantRawBits)
    id3081out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3080 (NodeConstantRawBits)
    id3080out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3079 (NodeConstantRawBits)
    id3079out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3078 (NodeConstantRawBits)
    id3078out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1447 (NodeConstantRawBits)
    id1447out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1431 (NodeConstantRawBits)
    id1431out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1456 (NodeConstantRawBits)
    id1456out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1457 (NodeConstantRawBits)
    id1457out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1459 (NodeConstantRawBits)
    id1459out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2416 (NodeConstantRawBits)
    id2416out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3077 (NodeConstantRawBits)
    id3077out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3076 (NodeConstantRawBits)
    id3076out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3075 (NodeConstantRawBits)
    id3075out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3074 (NodeConstantRawBits)
    id3074out_value = (c_hw_flt_8_12_bits_55);
  }
  { // Node ID: 3073 (NodeConstantRawBits)
    id3073out_value = (c_hw_flt_8_12_bits_56);
  }
  { // Node ID: 3072 (NodeConstantRawBits)
    id3072out_value = (c_hw_flt_8_12_bits_57);
  }
  { // Node ID: 1553 (NodeConstantRawBits)
    id1553out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3071 (NodeConstantRawBits)
    id3071out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1486 (NodeConstantRawBits)
    id1486out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1491 (NodeConstantRawBits)
    id1491out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1522 (NodeConstantRawBits)
    id1522out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2660 (NodeConstantRawBits)
    id2660out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1562 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1562sta_rom_store, 12, 64); 
  }
  { // Node ID: 1507 (NodeConstantRawBits)
    id1507out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3070 (NodeConstantRawBits)
    id3070out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3069 (NodeConstantRawBits)
    id3069out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3068 (NodeConstantRawBits)
    id3068out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3067 (NodeConstantRawBits)
    id3067out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1532 (NodeConstantRawBits)
    id1532out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1516 (NodeConstantRawBits)
    id1516out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1541 (NodeConstantRawBits)
    id1541out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1542 (NodeConstantRawBits)
    id1542out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1544 (NodeConstantRawBits)
    id1544out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2417 (NodeConstantRawBits)
    id2417out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3066 (NodeConstantRawBits)
    id3066out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3065 (NodeConstantRawBits)
    id3065out_value = (c_hw_flt_8_12_bits_41);
  }
  { // Node ID: 3064 (NodeConstantRawBits)
    id3064out_value = (c_hw_flt_8_12_bits_57);
  }
  { // Node ID: 1636 (NodeConstantRawBits)
    id1636out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 3063 (NodeConstantRawBits)
    id3063out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 1569 (NodeConstantRawBits)
    id1569out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1574 (NodeConstantRawBits)
    id1574out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 1605 (NodeConstantRawBits)
    id1605out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 2662 (NodeConstantRawBits)
    id2662out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 1645 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id1645sta_rom_store, 12, 64); 
  }
  { // Node ID: 1590 (NodeConstantRawBits)
    id1590out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 3062 (NodeConstantRawBits)
    id3062out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 3061 (NodeConstantRawBits)
    id3061out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3060 (NodeConstantRawBits)
    id3060out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3059 (NodeConstantRawBits)
    id3059out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 1615 (NodeConstantRawBits)
    id1615out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1599 (NodeConstantRawBits)
    id1599out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 1624 (NodeConstantRawBits)
    id1624out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 1625 (NodeConstantRawBits)
    id1625out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 1627 (NodeConstantRawBits)
    id1627out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 2418 (NodeConstantRawBits)
    id2418out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 3058 (NodeConstantRawBits)
    id3058out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 3057 (NodeConstantRawBits)
    id3057out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 3054 (NodeConstantRawBits)
    id3054out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 3053 (NodeConstantRawBits)
    id3053out_value = (c_hw_flt_8_12_bits_58);
  }
  { // Node ID: 3052 (NodeConstantRawBits)
    id3052out_value = (c_hw_flt_8_12_bits_47);
  }
  { // Node ID: 3051 (NodeConstantRawBits)
    id3051out_value = (c_hw_flt_8_12_bits_59);
  }
  { // Node ID: 20 (NodeInputMappedReg)
    registerMappedRegister("ca_in", Data(20));
  }
  { // Node ID: 1688 (NodeConstantRawBits)
    id1688out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 3046 (NodeConstantRawBits)
    id3046out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 1696 (NodeConstantRawBits)
    id1696out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 2373 (NodeROM)
    uint64_t data[] = {
      0x7aca3b5c5000,
      0x7a1a405c2006,
      0x795a465c000c,
      0x78aa4c5bd011,
      0x77fa515ba017,
      0x774a575b701d,
      0x769a5d5b4022,
      0x75ea625b1028,
      0x753a685af02e,
      0x748a6d5ac033,
      0x73da735a9039,
      0x733a785a603f,
      0x728a7e5a3044,
      0x71ea835a104a,
      0x713a8859e050,
      0x709a8e59b055,
      0x6ffa9359905b,
      0x6f5a98596061,
      0x6eba9d593066,
      0x6e1aa359006c,
      0x6d7aa858e071,
      0x6cdaad58b077,
      0x6c3ab258807c,
      0x6baab7586082,
      0x6b0abc583087,
      0x6a7ac158108d,
      0x69dac657e092,
      0x694acb57b098,
      0x68aad057909d,
      0x681ad55760a3,
      0x678ada5740a8,
      0x66fadf5710ae,
      0x666ae356e0b3,
      0x65dae856c0b9,
      0x654aed5690be,
      0x64baf25670c3,
      0x642af65640c9,
      0x639afb5620ce,
      0x631b0055f0d4,
      0x628b0455d0d9,
      0x620b0955a0de,
      0x617b0e5580e4,
      0x60fb125550e9,
      0x606b175530ee,
      0x5feb1b5500f4,
      0x5f6b2054e0f9,
      0x5eeb2454c0fe,
      0x5e5b29549103,
      0x5ddb2d547109,
      0x5d5b3154410e,
      0x5cdb36542113,
      0x5c6b3a53f119,
      0x5beb3f53d11e,
      0x5b6b4353b123,
      0x5aeb47538128,
      0x5a7b4b53612d,
      0x59fb50534133,
      0x597b54531138,
      0x590b5852f13d,
      0x588b5c52d142,
      0x581b6052a147,
      0x57ab6452814d,
      0x572b69526152,
      0x56bb6d523157,
      0x564b7152115c,
      0x55db7551f161,
      0x556b7951d166,
      0x54fb7d51a16b,
      0x548b81518170,
      0x541b85516176,
      0x53ab8951417b,
      0x533b8d511180,
      0x52cb9050f185,
      0x525b9450d18a,
      0x51eb9850b18f,
      0x518b9c509194,
      0x511ba0506199,
      0x50bba450419e,
      0x504ba75021a3,
      0x4fdbab5001a8,
      0x4f7baf4fe1ad,
      0x4f1bb34fc1b2,
      0x4eabb64f91b7,
      0x4e4bba4f71bc,
      0x4debbe4f51c1,
      0x4d7bc14f31c6,
      0x4d1bc54f11cb,
      0x4cbbc94ef1d0,
      0x4c5bcc4ed1d5,
      0x4bfbd04eb1da,
      0x4b9bd34e81de,
      0x4b3bd74e61e3,
      0x4adbda4e41e8,
      0x4a7bde4e21ed,
      0x4a1be14e01f2,
      0x49bbe54de1f7,
      0x495be84dc1fc,
      0x490bec4da201,
      0x48abef4d8205,
      0x484bf34d620a,
      0x47fbf64d420f,
      0x479bf94d2214,
      0x473bfd4d0219,
      0x46ec004ce21e,
      0x468c034cc222,
      0x463c074ca227,
      0x45dc0a4c822c,
      0x458c0d4c6231,
      0x453c114c4236,
      0x44dc144c223a,
      0x448c174c023f,
      0x443c1a4be244,
      0x43dc1d4bc249,
      0x438c214ba24d,
      0x433c244b8252,
      0x42ec274b6257,
      0x429c2a4b425b,
      0x424c2d4b3260,
      0x41fc304b1265,
      0x41ac334af269,
      0x415c364ad26e,
      0x410c394ab273,
      0x40bc3d4a9277,
      0x406c404a727c,
      0x401c434a5281,
      0x3fcc464a3285,
      0x3f8c494a228a,
      0x3f3c4c4a028f,
      0x3eec4f49e293,
      0x3e9c5149c298,
      0x3e5c5449a29d,
      0x3e0c574982a1,
      0x3dcc5a4972a6,
      0x3d7c5d4952aa,
      0x3d2c604932af,
      0x3cec634912b3,
      0x3c9c6648f2b8,
      0x3c5c6948d2bd,
      0x3c0c6b48c2c1,
      0x3bcc6e48a2c6,
      0x3b8c714882ca,
      0x3b3c744862cf,
      0x3afc774852d3,
      0x3abc794832d8,
      0x3a6c7c4812dc,
      0x3a2c7f47f2e1,
      0x39ec8247e2e5,
      0x39ac8447c2ea,
      0x395c8747a2ee,
      0x391c8a4782f3,
      0x38dc8c4772f7,
      0x389c8f4752fc,
      0x385c92473300,
      0x381c94471305,
      0x37dc97470309,
      0x379c9a46e30d,
      0x375c9c46c312,
      0x371c9f46b316,
      0x36dca146931b,
      0x369ca446731f,
      0x365ca6466323,
      0x361ca9464328,
      0x35ecac46232c,
      0x35acae461331,
      0x356cb145f335,
      0x352cb345d339,
      0x34ecb645c33e,
      0x34bcb845a342,
      0x347cba458346,
      0x343cbd45734b,
      0x340cbf45534f,
      0x33ccc2453353,
      0x338cc4452358,
      0x335cc745035c,
      0x331cc944f360,
      0x32eccb44d365,
      0x32acce44b369,
      0x327cd044a36d,
      0x323cd3448372,
      0x320cd5447376,
      0x31ccd744537a,
      0x319cda44337e,
      0x315cdc442383,
      0x312cde440387,
      0x30ece143f38b,
      0x30bce343d38f,
      0x308ce543c394,
      0x304ce743a398,
      0x301cea43939c,
      0x2fecec4373a0,
      0x2fbcee4353a5,
      0x2f7cf04343a9,
      0x2f4cf34323ad,
      0x2f1cf54313b1,
      0x2eecf742f3b5,
      0x2ebcf942e3b9,
      0x2e7cfb42c3be,
      0x2e4cfe42b3c2,
      0x2e1d004293c6,
      0x2ded024283ca,
      0x2dbd044263ce,
      0x2d8d064253d2,
      0x2d5d084233d7,
      0x2d2d0a4223db,
      0x2cfd0d4203df,
      0x2ccd0f41f3e3,
      0x2c9d1141d3e7,
      0x2c6d1341c3eb,
      0x2c3d1541b3ef,
      0x2c0d174193f3,
      0x2bdd194183f8,
      0x2bad1b4163fc,
      0x2b7d1d415400,
      0x2b4d1f413404,
      0x2b1d21412408,
      0x2afd2341040c,
      0x2acd2540f410,
      0x2a9d2740e414,
      0x2a6d2940c418,
      0x2a3d2b40b41c,
      0x2a1d2d409420,
      0x29ed2f408424,
      0x29bd31407428,
      0x298d3340542c,
      0x296d35404430,
      0x293d37402434,
      0x290d39401438,
      0x28ed3b40043c,
      0x28bd3d3fe440,
      0x288d3f3fd444,
      0x286d413fb448,
      0x283d423fa44c,
      0x281d443f9450,
      0x27ed463f7454,
      0x27cd483f6458,
      0x279d4a3f545c,
      0x276d4c3f3460,
      0x274d4e3f2464,
      0x271d503f1468,
      0x26fd513ef46c,
      0x26cd533ee470,
      0x26ad553ec474,
      0x268d573eb478,
      0x265d593ea47c,
      0x263d5a3e947f,
      0x260d5c3e7483,
      0x25ed5e3e6487,
      0x25bd603e548b,
      0x259d623e348f,
      0x257d633e2493,
      0x254d653e1497,
      0x252d673df49b,
      0x250d693de49f,
      0x24dd6a3dd4a2,
      0x24bd6c3db4a6,
      0x249d6e3da4aa,
      0x246d6f3d94ae,
      0x244d713d84b2,
      0x242d733d64b6,
      0x240d753d54ba,
      0x23dd763d44bd,
      0x23bd783d34c1,
      0x239d7a3d14c5,
      0x237d7b3d04c9,
      0x235d7d3cf4cd,
      0x232d7f3cd4d0,
      0x230d803cc4d4,
      0x22ed823cb4d8,
      0x22cd833ca4dc,
      0x22ad853c84e0,
      0x228d873c74e3,
      0x226d883c64e7,
      0x224d8a3c54eb,
      0x221d8c3c44ef,
      0x21fd8d3c24f2,
      0x21dd8f3c14f6,
      0x21bd903c04fa,
      0x219d923bf4fe,
      0x217d933bd501,
      0x215d953bc505,
      0x213d973bb509,
      0x211d983ba50d,
      0x20fd9a3b9510,
      0x20dd9b3b7514,
      0x20bd9d3b6518,
      0x209d9e3b551c,
      0x207da03b451f,
      0x205da13b3523,
      0x203da33b1527,
      0x201da43b052a,
      0x200da63af52e,
      0x1feda73ae532,
      0x1fcda93ad535,
      0x1fadaa3ac539,
      0x1f8dac3aa53d,
      0x1f6dad3a9540,
      0x1f4daf3a8544,
      0x1f2db03a7548,
      0x1f1db23a654b,
      0x1efdb33a554f,
      0x1eddb53a4553,
      0x1ebdb63a2556,
      0x1e9db83a155a,
      0x1e7db93a055e,
      0x1e6dba39f561,
      0x1e4dbc39e565,
      0x1e2dbd39d568,
      0x1e0dbf39c56c,
      0x1dfdc039a570,
      0x1dddc1399573,
      0x1dbdc3398577,
      0x1d9dc439757a,
      0x1d8dc639657e,
      0x1d6dc7395582,
      0x1d4dc8394585,
      0x1d3dca393589,
      0x1d1dcb39258c,
      0x1cfdcc390590,
      0x1cedce38f593,
      0x1ccdcf38e597,
      0x1cadd138d59b,
      0x1c9dd238c59e,
      0x1c7dd338b5a2,
      0x1c5dd538a5a5,
      0x1c4dd63895a9,
      0x1c2dd73885ac,
      0x1c0dd93875b0,
      0x1bfdda3865b3,
      0x1bdddb3845b7,
      0x1bcddc3835ba,
      0x1badde3825be,
      0x1b9ddf3815c1,
      0x1b7de03805c5,
      0x1b5de237f5c8,
      0x1b4de337e5cc,
      0x1b2de437d5cf,
      0x1b1de537c5d3,
      0x1afde737b5d6,
      0x1aede837a5da,
      0x1acde93795dd,
      0x1abdea3785e1,
      0x1a9dec3775e4,
      0x1a8ded3765e8,
      0x1a6dee3755eb,
      0x1a5def3745ef,
      0x1a3df13735f2,
      0x1a2df23725f5,
      0x1a0df33715f9,
      0x19fdf43705fc,
      0x19ddf636e600,
      0x19cdf736d603,
      0x19bdf836c607,
      0x199df936b60a,
      0x198dfa36a60d,
      0x196dfc369611,
      0x195dfd368614,
      0x194dfe367618,
      0x192dff36661b,
      0x191e0036561e,
      0x18fe02364622,
      0x18ee03363625,
      0x18de04362629,
      0x18be0536162c,
      0x18ae0636062f,
      0x189e0735f633,
      0x187e0835e636,
      0x186e0a35d63a,
      0x185e0b35d63d,
      0x183e0c35c640,
      0x182e0d35b644,
      0x181e0e35a647,
      0x17fe0f35964a,
      0x17ee1035864e,
      0x17de12357651,
      0x17be13356654,
      0x17ae14355658,
      0x179e1535465b,
      0x178e1635365e,
      0x176e17352662,
      0x175e18351665,
      0x174e19350668,
      0x173e1a34f66c,
      0x171e1b34e66f,
      0x170e1d34d672,
      0x16fe1e34c675,
      0x16ee1f34b679,
      0x16ce2034a67c,
      0x16be2134967f,
      0x16ae22348683,
      0x169e23347686,
      0x168e24347689,
      0x166e2534668c,
      0x165e26345690,
      0x164e27344693,
      0x163e28343696,
      0x162e2934269a,
      0x161e2a34169d,
      0x15fe2b3406a0,
      0x15ee2c33f6a3,
      0x15de2d33e6a7,
      0x15ce2e33d6aa,
      0x15be2f33c6ad,
      0x15ae3033c6b0,
      0x159e3133b6b3,
      0x157e3233a6b7,
      0x156e333396ba,
      0x155e343386bd,
      0x154e353376c0,
      0x153e363366c4,
      0x152e373356c7,
      0x151e383346ca,
      0x150e393336cd,
      0x14fe3a3336d0,
      0x14de3b3326d4,
      0x14ce3c3316d7,
      0x14be3d3306da,
      0x14ae3e32f6dd,
      0x149e3f32e6e0,
      0x148e4032d6e4,
      0x147e4132c6e7,
      0x146e4232c6ea,
      0x145e4332b6ed,
      0x144e4432a6f0,
      0x143e453296f3,
      0x142e463286f7,
      0x141e473276fa,
      0x140e483266fd,
      0x13fe49326700,
      0x13ee4a325703,
      0x13de4b324706,
      0x13ce4c323709,
      0x13be4c32270d,
      0x13ae4d321710,
      0x139e4e320713,
      0x138e4f320716,
      0x137e5031f719,
      0x136e5131e71c,
      0x135e5231d71f,
      0x134e5331c722,
      0x133e5431b725,
      0x132e5531b729,
      0x131e5631a72c,
      0x130e5631972f,
      0x12fe57318732,
      0x12ee58317735,
      0x12de59316738,
      0x12ce5a31673b,
      0x12be5b31573e,
      0x12ae5c314741,
      0x129e5d313744,
      0x128e5e312747,
      0x128e5e31174b,
      0x127e5f31174e,
      0x126e60310751,
      0x125e6130f754,
      0x124e6230e757,
      0x123e6330d75a,
      0x122e6430d75d,
      0x121e6430c760,
      0x120e6530b763,
      0x11fe6630a766,
      0x11fe67309769,
      0x11ee6830976c,
      0x11de6930876f,
      0x11ce69307772,
      0x11be6a306775,
      0x11ae6b305778,
      0x119e6c30577b,
      0x118e6d30477e,
      0x118e6e303781,
      0x117e6e302784,
      0x116e6f301787,
      0x115e7030178a,
      0x114e7130078d,
      0x113e722ff790,
      0x113e722fe793,
      0x112e732fe796,
      0x111e742fd799,
      0x110e752fc79c,
      0x10fe762fb79f,
      0x10ee762fa7a2,
      0x10ee772fa7a5,
      0x10de782f97a8,
      0x10ce792f87ab,
      0x10be7a2f77ae,
      0x10ae7a2f77b1,
      0x10ae7b2f67b4,
      0x109e7c2f57b7,
      0x108e7d2f47ba,
      0x107e7e2f47bd,
      0x106e7e2f37c0,
      0x106e7f2f27c3,
      0x105e802f17c6,
      0x104e812f17c9,
      0x103e812f07cc,
      0x103e822ef7cf,
      0x102e832ee7d1,
      0x101e842ee7d4,
      0x100e842ed7d7,
      0xffe852ec7da,
      0xffe862eb7dd,
      0xfee872eb7e0,
      0xfde872ea7e3,
      0xfce882e97e6,
      0xfce892e87e9,
      0xfbe8a2e87ec,
      0xfae8a2e77ef,
      0xf9e8b2e67f2,
      0xf9e8c2e67f4,
      0xf8e8c2e57f7,
      0xf7e8d2e47fa,
      0xf7e8e2e37fd,
    };
    setRom< HWRawBits<48> > (data, id2373sta_rom_store, 48, 512); 
  }
  { // Node ID: 2316 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 3042 (NodeConstantRawBits)
    id3042out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2321 (NodeInputMappedReg)
    registerMappedRegister("io_ca_out_force_disabled", Data(1));
  }
  { // Node ID: 2324 (NodeOutput)
    m_ca_out = registerOutput("ca_out",1 );
  }
  { // Node ID: 3041 (NodeConstantRawBits)
    id3041out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2329 (NodeInputMappedReg)
    registerMappedRegister("io_x1_out_force_disabled", Data(1));
  }
  { // Node ID: 2332 (NodeOutput)
    m_x1_out = registerOutput("x1_out",2 );
  }
  { // Node ID: 3040 (NodeConstantRawBits)
    id3040out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2337 (NodeInputMappedReg)
    registerMappedRegister("io_m_out_force_disabled", Data(1));
  }
  { // Node ID: 2340 (NodeOutput)
    m_m_out = registerOutput("m_out",3 );
  }
  { // Node ID: 3039 (NodeConstantRawBits)
    id3039out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2345 (NodeInputMappedReg)
    registerMappedRegister("io_f_out_force_disabled", Data(1));
  }
  { // Node ID: 2348 (NodeOutput)
    m_f_out = registerOutput("f_out",4 );
  }
  { // Node ID: 3038 (NodeConstantRawBits)
    id3038out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2353 (NodeInputMappedReg)
    registerMappedRegister("io_h_out_force_disabled", Data(1));
  }
  { // Node ID: 2356 (NodeOutput)
    m_h_out = registerOutput("h_out",5 );
  }
  { // Node ID: 3037 (NodeConstantRawBits)
    id3037out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2361 (NodeInputMappedReg)
    registerMappedRegister("io_d_out_force_disabled", Data(1));
  }
  { // Node ID: 2364 (NodeOutput)
    m_d_out = registerOutput("d_out",6 );
  }
  { // Node ID: 3036 (NodeConstantRawBits)
    id3036out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 2369 (NodeInputMappedReg)
    registerMappedRegister("io_j_out_force_disabled", Data(1));
  }
  { // Node ID: 2372 (NodeOutput)
    m_j_out = registerOutput("j_out",7 );
  }
  { // Node ID: 2386 (NodeConstantRawBits)
    id2386out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 3035 (NodeConstantRawBits)
    id3035out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 2383 (NodeConstantRawBits)
    id2383out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 2387 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 3034 (NodeConstantRawBits)
    id3034out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 2389 (NodeConstantRawBits)
    id2389out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 2392 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void BeelerReuterDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void BeelerReuterDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 11 (NodeCounter)

    (id11st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 2712 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2712out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 2313 (NodeInputMappedReg)
    id2313out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 3015 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id3015out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 9 (NodeInputMappedReg)
    id9out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 10 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id9out_num_iteration;

    (id10st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 16 (NodeInputMappedReg)
    id16out_u_in = getMappedRegValue<HWFloat<8,12> >("u_in");
  }
  { // Node ID: 2723 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2723out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 3030 (NodeFIFO)

    for(int i=0; i<8; i++)
    {
      id3030out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 3031 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id3031out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 12 (NodeInputMappedReg)
    id12out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 2722 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2722out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2713 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2713out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2714 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2714out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2716 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2716out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2717 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2717out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2718 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2718out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2719 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2719out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2733 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2733out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2724 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2724out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2725 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2725out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2727 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2727out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2728 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2728out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2729 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2729out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2730 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2730out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2744 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2744out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2735 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2735out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2736 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2736out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2738 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2738out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2739 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2739out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2740 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2740out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2741 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2741out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2756 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2756out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2747 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2747out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2748 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2748out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2750 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2750out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2751 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2751out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2752 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2752out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2753 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2753out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2767 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2767out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2758 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2758out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2759 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2759out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2761 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2761out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2762 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2762out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2763 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2763out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2764 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2764out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2778 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2778out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2769 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2769out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2770 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2770out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2772 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2772out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2773 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2773out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2774 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2774out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2775 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2775out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 24 (NodeInputMappedReg)
    id24out_x1_in = getMappedRegValue<HWFloat<8,12> >("x1_in");
  }
  { // Node ID: 2800 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2800out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2789 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2789out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2780 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2780out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2781 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2781out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2783 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2783out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2784 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2784out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2785 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2785out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2786 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2786out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2799 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2799out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2790 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2790out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2791 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2791out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2793 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2793out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2794 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2794out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2795 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2795out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2796 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2796out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2810 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2810out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2801 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2801out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2802 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2802out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2804 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2804out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2805 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2805out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2806 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2806out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2807 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2807out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2820 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2820out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2811 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2811out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2812 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2812out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2814 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2814out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2815 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2815out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2816 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2816out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2817 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2817out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2779 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2779out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2824 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2824out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 28 (NodeInputMappedReg)
    id28out_m_in = getMappedRegValue<HWFloat<8,12> >("m_in");
  }
  { // Node ID: 2836 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2836out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2835 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2835out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2826 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2826out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2827 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2827out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2829 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2829out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2830 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2830out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2831 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2831out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2832 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2832out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2846 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2846out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2837 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2837out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2838 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2838out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2840 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2840out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2841 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2841out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2842 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2842out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2843 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2843out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_h_in = getMappedRegValue<HWFloat<8,12> >("h_in");
  }
  { // Node ID: 2861 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id2861out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2860 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2860out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2851 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2851out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2852 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2852out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2854 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2854out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2855 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2855out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2856 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2856out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2857 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2857out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2872 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2872out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2863 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2863out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2864 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2864out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2866 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2866out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2867 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2867out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2868 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2868out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2869 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2869out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_j_in = getMappedRegValue<HWFloat<8,12> >("j_in");
  }
  { // Node ID: 2895 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id2895out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2884 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2884out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2875 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2875out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2876 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2876out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2878 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2878out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2879 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2879out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2880 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2880out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2881 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2881out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2894 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2894out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2886 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2886out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2887 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2887out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2888 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2888out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2889 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2889out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2890 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2890out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2891 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2891out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2906 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2906out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2897 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2897out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2898 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2898out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2900 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2900out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2901 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2901out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2902 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2902out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2903 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2903out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 40 (NodeInputMappedReg)
    id40out_d_in = getMappedRegValue<HWFloat<8,12> >("d_in");
  }
  { // Node ID: 2931 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2931out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2920 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2920out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2911 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2911out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2912 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2912out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2914 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2914out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2915 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2915out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2916 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2916out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2917 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2917out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2930 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2930out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2921 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2921out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2922 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2922out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2924 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2924out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2925 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2925out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2926 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2926out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2927 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2927out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2941 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2941out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2932 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2932out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2933 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2933out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2935 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2935out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2936 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2936out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2937 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2937out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2938 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2938out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2951 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2951out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2942 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2942out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2943 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2943out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2945 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2945out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2946 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2946out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2947 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2947out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2948 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2948out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2910 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2910out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 32 (NodeInputMappedReg)
    id32out_f_in = getMappedRegValue<HWFloat<8,12> >("f_in");
  }
  { // Node ID: 2976 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2976out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 2965 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2965out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2956 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2956out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2957 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2957out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2959 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2959out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2960 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2960out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2961 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2961out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2962 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2962out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2975 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2975out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2966 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2966out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2967 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2967out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2969 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2969out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2970 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2970out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2971 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2971out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2972 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2972out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2986 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2986out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2977 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2977out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2978 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2978out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2980 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2980out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2981 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2981out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2982 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2982out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2983 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2983out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2996 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id2996out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2987 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2987out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 2988 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2988out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 2990 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2990out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2991 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2991out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2992 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id2992out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2993 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id2993out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2955 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id2955out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 20 (NodeInputMappedReg)
    id20out_ca_in = getMappedRegValue<HWFloat<8,12> >("ca_in");
  }
  { // Node ID: 3008 (NodeFIFO)

    for(int i=0; i<8; i++)
    {
      id3008out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 3032 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id3032out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 3009 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id3009out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 3010 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id3010out_output[i] = (c_hw_fix_2_n2_uns_undef);
    }
  }
  { // Node ID: 2321 (NodeInputMappedReg)
    id2321out_io_ca_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_ca_out_force_disabled");
  }
  { // Node ID: 3017 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id3017out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2329 (NodeInputMappedReg)
    id2329out_io_x1_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x1_out_force_disabled");
  }
  { // Node ID: 3019 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id3019out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2337 (NodeInputMappedReg)
    id2337out_io_m_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_m_out_force_disabled");
  }
  { // Node ID: 3021 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id3021out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2345 (NodeInputMappedReg)
    id2345out_io_f_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_f_out_force_disabled");
  }
  { // Node ID: 3023 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id3023out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2353 (NodeInputMappedReg)
    id2353out_io_h_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_h_out_force_disabled");
  }
  { // Node ID: 3025 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id3025out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2361 (NodeInputMappedReg)
    id2361out_io_d_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_d_out_force_disabled");
  }
  { // Node ID: 3027 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id3027out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2369 (NodeInputMappedReg)
    id2369out_io_j_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_j_out_force_disabled");
  }
  { // Node ID: 3029 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id3029out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 2384 (NodeCounter)

    (id2384st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 2390 (NodeCounter)

    (id2390st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 2392 (NodeInputMappedReg)
    id2392out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void BeelerReuterDFEKernel::updateState() {
  { // Node ID: 2313 (NodeInputMappedReg)
    id2313out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 9 (NodeInputMappedReg)
    id9out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 16 (NodeInputMappedReg)
    id16out_u_in = getMappedRegValue<HWFloat<8,12> >("u_in");
  }
  { // Node ID: 12 (NodeInputMappedReg)
    id12out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 24 (NodeInputMappedReg)
    id24out_x1_in = getMappedRegValue<HWFloat<8,12> >("x1_in");
  }
  { // Node ID: 28 (NodeInputMappedReg)
    id28out_m_in = getMappedRegValue<HWFloat<8,12> >("m_in");
  }
  { // Node ID: 36 (NodeInputMappedReg)
    id36out_h_in = getMappedRegValue<HWFloat<8,12> >("h_in");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_j_in = getMappedRegValue<HWFloat<8,12> >("j_in");
  }
  { // Node ID: 40 (NodeInputMappedReg)
    id40out_d_in = getMappedRegValue<HWFloat<8,12> >("d_in");
  }
  { // Node ID: 32 (NodeInputMappedReg)
    id32out_f_in = getMappedRegValue<HWFloat<8,12> >("f_in");
  }
  { // Node ID: 20 (NodeInputMappedReg)
    id20out_ca_in = getMappedRegValue<HWFloat<8,12> >("ca_in");
  }
  { // Node ID: 2321 (NodeInputMappedReg)
    id2321out_io_ca_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_ca_out_force_disabled");
  }
  { // Node ID: 2329 (NodeInputMappedReg)
    id2329out_io_x1_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x1_out_force_disabled");
  }
  { // Node ID: 2337 (NodeInputMappedReg)
    id2337out_io_m_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_m_out_force_disabled");
  }
  { // Node ID: 2345 (NodeInputMappedReg)
    id2345out_io_f_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_f_out_force_disabled");
  }
  { // Node ID: 2353 (NodeInputMappedReg)
    id2353out_io_h_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_h_out_force_disabled");
  }
  { // Node ID: 2361 (NodeInputMappedReg)
    id2361out_io_d_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_d_out_force_disabled");
  }
  { // Node ID: 2369 (NodeInputMappedReg)
    id2369out_io_j_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_j_out_force_disabled");
  }
  { // Node ID: 2392 (NodeInputMappedReg)
    id2392out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void BeelerReuterDFEKernel::preExecute() {
}

void BeelerReuterDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "BeelerReuterDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int BeelerReuterDFEKernel::getFlushLevelStart() {
  return ((7l)+(3l));
}

}
