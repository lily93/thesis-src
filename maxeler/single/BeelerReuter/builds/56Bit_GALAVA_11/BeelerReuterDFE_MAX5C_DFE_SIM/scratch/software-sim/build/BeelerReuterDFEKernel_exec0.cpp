#include "stdsimheader.h"

namespace maxcompilersim {

void BeelerReuterDFEKernel::execute0() {
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 3434 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 11 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id11in_enable = id8out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id11in_max = id3434out_value;

    HWOffsetFix<12,0,UNSIGNED> id11x_1;
    HWOffsetFix<1,0,UNSIGNED> id11x_2;
    HWOffsetFix<1,0,UNSIGNED> id11x_3;
    HWOffsetFix<12,0,UNSIGNED> id11x_4t_1e_1;

    id11out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id11st_count)));
    (id11x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id11st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id11x_2) = (gte_fixed((id11x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id11in_max))));
    (id11x_3) = (and_fixed((id11x_2),id11in_enable));
    id11out_wrap = (id11x_3);
    if((id11in_enable.getValueAsBool())) {
      if(((id11x_3).getValueAsBool())) {
        (id11st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id11x_4t_1e_1) = (id11x_1);
        (id11st_count) = (id11x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 3112 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id3112in_input = id11out_count;

    id3112out_output[(getCycle()+1)%2] = id3112in_input;
  }
  { // Node ID: 3699 (NodeConstantRawBits)
  }
  { // Node ID: 2711 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2711in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2711in_b = id3699out_value;

    id2711out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2711in_a,id2711in_b));
  }
  { // Node ID: 2819 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id2819in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id2819in_b = id2711out_result[getCycle()%2];

    id2819out_result[(getCycle()+1)%2] = (eq_fixed(id2819in_a,id2819in_b));
  }
  { // Node ID: 2713 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2714out_result;

  { // Node ID: 2714 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2714in_a = id2713out_io_u_out_force_disabled;

    id2714out_result = (not_fixed(id2714in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2715out_result;

  { // Node ID: 2715 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2715in_a = id2819out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2715in_b = id2714out_result;

    HWOffsetFix<1,0,UNSIGNED> id2715x_1;

    (id2715x_1) = (and_fixed(id2715in_a,id2715in_b));
    id2715out_result = (id2715x_1);
  }
  { // Node ID: 3416 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3416in_input = id2715out_result;

    id3416out_output[(getCycle()+9)%10] = id3416in_input;
  }
  { // Node ID: 9 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 10 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id10in_enable = id11out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id9out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id10x_1;
    HWOffsetFix<1,0,UNSIGNED> id10x_2;
    HWOffsetFix<1,0,UNSIGNED> id10x_3;
    HWOffsetFix<33,0,UNSIGNED> id10x_4t_1e_1;

    id10out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id10st_count)));
    (id10x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id10st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id10x_2) = (gte_fixed((id10x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id10in_max))));
    (id10x_3) = (and_fixed((id10x_2),id10in_enable));
    id10out_wrap = (id10x_3);
    if((id10in_enable.getValueAsBool())) {
      if(((id10x_3).getValueAsBool())) {
        (id10st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id10x_4t_1e_1) = (id10x_1);
        (id10st_count) = (id10x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 3698 (NodeConstantRawBits)
  }
  { // Node ID: 2820 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2820in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2820in_b = id3698out_value;

    id2820out_result[(getCycle()+1)%2] = (eq_fixed(id2820in_a,id2820in_b));
  }
  { // Node ID: 3697 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2125out_o;

  { // Node ID: 2125 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2125in_i = id3124out_output[getCycle()%3];

    id2125out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2125in_i));
  }
  { // Node ID: 2128 (NodeConstantRawBits)
  }
  { // Node ID: 3016 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id2107out_result;

  { // Node ID: 2107 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2107in_a = id2168out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2107in_b = id3125out_output[getCycle()%3];

    id2107out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2107in_a,id2107in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2108out_result;

  { // Node ID: 2108 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2108in_a = id3125out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2108in_b = id2168out_dout[getCycle()%3];

    id2108out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2108in_a,id2108in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2109out_result;

  { // Node ID: 2109 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2109in_a = id2107out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2109in_b = id2108out_result;

    id2109out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2109in_a,id2109in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2110out_o;

  { // Node ID: 2110 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2110in_i = id2109out_result;

    id2110out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2110in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2111out_result;

  { // Node ID: 2111 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2111in_a = id2171out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2111in_b = id2110out_o;

    id2111out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2111in_a,id2111in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2112out_result;

  { // Node ID: 2112 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2112in_a = id2110out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2112in_b = id2171out_dout[getCycle()%3];

    id2112out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2112in_a,id2112in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2113out_result;

  { // Node ID: 2113 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2113in_a = id2111out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2113in_b = id2112out_result;

    id2113out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2113in_a,id2113in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2114out_o;

  { // Node ID: 2114 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2114in_i = id2113out_result;

    id2114out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2114in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2115out_result;

  { // Node ID: 2115 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2115in_a = id2174out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2115in_b = id2114out_o;

    id2115out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2115in_a,id2115in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2116out_result;

  { // Node ID: 2116 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2116in_a = id2114out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2116in_b = id2174out_dout[getCycle()%3];

    id2116out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2116in_a,id2116in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2117out_result;

  { // Node ID: 2117 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2117in_a = id2115out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2117in_b = id2116out_result;

    id2117out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2117in_a,id2117in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2118out_o;

  { // Node ID: 2118 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2118in_i = id2117out_result;

    id2118out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2118in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2119out_o;

  { // Node ID: 2119 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2119in_i = id2118out_o;

    id2119out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2119in_i));
  }
  { // Node ID: 3684 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2830out_result;

  { // Node ID: 2830 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2830in_a = id2119out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2830in_b = id3684out_value;

    id2830out_result = (gte_fixed(id2830in_a,id2830in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3089out_result;

  { // Node ID: 3089 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3089in_a = id2125out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3089in_b = id2128out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3089in_c = id3016out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3089in_condb = id2830out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3089x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3089x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3089x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3089x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3089x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3089x_1 = id3089in_a;
        break;
      default:
        id3089x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3089in_condb.getValueAsLong())) {
      case 0l:
        id3089x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3089x_2 = id3089in_b;
        break;
      default:
        id3089x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3089x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3089x_3 = id3089in_c;
        break;
      default:
        id3089x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3089x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3089x_1),(id3089x_2))),(id3089x_3)));
    id3089out_result = (id3089x_4);
  }
  HWRawBits<1> id2831out_result;

  { // Node ID: 2831 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2831in_a = id3089out_result;

    id2831out_result = (slice<13,1>(id2831in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2832out_output;

  { // Node ID: 2832 (NodeReinterpret)
    const HWRawBits<1> &id2832in_input = id2831out_result;

    id2832out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2832in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2134out_result;

  { // Node ID: 2134 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2134in_a = id3128out_output[getCycle()%3];

    id2134out_result = (not_fixed(id2134in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2135out_result;

  { // Node ID: 2135 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2135in_a = id2832out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2135in_b = id2134out_result;

    HWOffsetFix<1,0,UNSIGNED> id2135x_1;

    (id2135x_1) = (and_fixed(id2135in_a,id2135in_b));
    id2135out_result = (id2135x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2136out_result;

  { // Node ID: 2136 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2136in_a = id2135out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2136in_b = id3130out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2136x_1;

    (id2136x_1) = (or_fixed(id2136in_a,id2136in_b));
    id2136out_result = (id2136x_1);
  }
  { // Node ID: 3681 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2833out_result;

  { // Node ID: 2833 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2833in_a = id3089out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2833in_b = id3681out_value;

    id2833out_result = (gte_fixed(id2833in_a,id2833in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2143out_result;

  { // Node ID: 2143 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2143in_a = id3130out_output[getCycle()%3];

    id2143out_result = (not_fixed(id2143in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2144out_result;

  { // Node ID: 2144 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2144in_a = id2833out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2144in_b = id2143out_result;

    HWOffsetFix<1,0,UNSIGNED> id2144x_1;

    (id2144x_1) = (and_fixed(id2144in_a,id2144in_b));
    id2144out_result = (id2144x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2145out_result;

  { // Node ID: 2145 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2145in_a = id2144out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2145in_b = id3128out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2145x_1;

    (id2145x_1) = (or_fixed(id2145in_a,id2145in_b));
    id2145out_result = (id2145x_1);
  }
  HWRawBits<2> id2146out_result;

  { // Node ID: 2146 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2146in_in0 = id2136out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2146in_in1 = id2145out_result;

    id2146out_result = (cat(id2146in_in0,id2146in_in1));
  }
  { // Node ID: 2138 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2137out_o;

  { // Node ID: 2137 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2137in_i = id3089out_result;

    id2137out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2137in_i));
  }
  { // Node ID: 2122 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2123out_result;

  { // Node ID: 2123 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2123in_sel = id2830out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2123in_option0 = id2119out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2123in_option1 = id2122out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2123x_1;

    switch((id2123in_sel.getValueAsLong())) {
      case 0l:
        id2123x_1 = id2123in_option0;
        break;
      case 1l:
        id2123x_1 = id2123in_option1;
        break;
      default:
        id2123x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2123out_result = (id2123x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2124out_o;

  { // Node ID: 2124 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2124in_i = id2123out_result;

    id2124out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2124in_i));
  }
  HWRawBits<56> id2139out_result;

  { // Node ID: 2139 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2139in_in0 = id2138out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2139in_in1 = id2137out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2139in_in2 = id2124out_o;

    id2139out_result = (cat((cat(id2139in_in0,id2139in_in1)),id2139in_in2));
  }
  HWFloat<11,45> id2140out_output;

  { // Node ID: 2140 (NodeReinterpret)
    const HWRawBits<56> &id2140in_input = id2139out_result;

    id2140out_output = (cast_bits2float<11,45>(id2140in_input));
  }
  { // Node ID: 2147 (NodeConstantRawBits)
  }
  { // Node ID: 2148 (NodeConstantRawBits)
  }
  { // Node ID: 2150 (NodeConstantRawBits)
  }
  HWRawBits<56> id2834out_result;

  { // Node ID: 2834 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2834in_in0 = id2147out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2834in_in1 = id2148out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2834in_in2 = id2150out_value;

    id2834out_result = (cat((cat(id2834in_in0,id2834in_in1)),id2834in_in2));
  }
  HWFloat<11,45> id2152out_output;

  { // Node ID: 2152 (NodeReinterpret)
    const HWRawBits<56> &id2152in_input = id2834out_result;

    id2152out_output = (cast_bits2float<11,45>(id2152in_input));
  }
  { // Node ID: 2795 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2155out_result;

  { // Node ID: 2155 (NodeMux)
    const HWRawBits<2> &id2155in_sel = id2146out_result;
    const HWFloat<11,45> &id2155in_option0 = id2140out_output;
    const HWFloat<11,45> &id2155in_option1 = id2152out_output;
    const HWFloat<11,45> &id2155in_option2 = id2795out_value;
    const HWFloat<11,45> &id2155in_option3 = id2152out_output;

    HWFloat<11,45> id2155x_1;

    switch((id2155in_sel.getValueAsLong())) {
      case 0l:
        id2155x_1 = id2155in_option0;
        break;
      case 1l:
        id2155x_1 = id2155in_option1;
        break;
      case 2l:
        id2155x_1 = id2155in_option2;
        break;
      case 3l:
        id2155x_1 = id2155in_option3;
        break;
      default:
        id2155x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2155out_result = (id2155x_1);
  }
  { // Node ID: 3680 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2165out_result;

  { // Node ID: 2165 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2165in_sel = id3133out_output[getCycle()%9];
    const HWFloat<11,45> &id2165in_option0 = id2155out_result;
    const HWFloat<11,45> &id2165in_option1 = id3680out_value;

    HWFloat<11,45> id2165x_1;

    switch((id2165in_sel.getValueAsLong())) {
      case 0l:
        id2165x_1 = id2165in_option0;
        break;
      case 1l:
        id2165x_1 = id2165in_option1;
        break;
      default:
        id2165x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2165out_result = (id2165x_1);
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2222out_o;

  { // Node ID: 2222 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2222in_i = id3135out_output[getCycle()%3];

    id2222out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2222in_i));
  }
  { // Node ID: 2225 (NodeConstantRawBits)
  }
  { // Node ID: 3018 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id2204out_result;

  { // Node ID: 2204 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2204in_a = id2265out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2204in_b = id3136out_output[getCycle()%3];

    id2204out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2204in_a,id2204in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2205out_result;

  { // Node ID: 2205 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2205in_a = id3136out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2205in_b = id2265out_dout[getCycle()%3];

    id2205out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2205in_a,id2205in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2206out_result;

  { // Node ID: 2206 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2206in_a = id2204out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2206in_b = id2205out_result;

    id2206out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2206in_a,id2206in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2207out_o;

  { // Node ID: 2207 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2207in_i = id2206out_result;

    id2207out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2207in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2208out_result;

  { // Node ID: 2208 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2208in_a = id2268out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2208in_b = id2207out_o;

    id2208out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2208in_a,id2208in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2209out_result;

  { // Node ID: 2209 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2209in_a = id2207out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2209in_b = id2268out_dout[getCycle()%3];

    id2209out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2209in_a,id2209in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2210out_result;

  { // Node ID: 2210 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2210in_a = id2208out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2210in_b = id2209out_result;

    id2210out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2210in_a,id2210in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2211out_o;

  { // Node ID: 2211 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2211in_i = id2210out_result;

    id2211out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2211in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2212out_result;

  { // Node ID: 2212 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2212in_a = id2271out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2212in_b = id2211out_o;

    id2212out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2212in_a,id2212in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2213out_result;

  { // Node ID: 2213 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2213in_a = id2211out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2213in_b = id2271out_dout[getCycle()%3];

    id2213out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2213in_a,id2213in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2214out_result;

  { // Node ID: 2214 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2214in_a = id2212out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2214in_b = id2213out_result;

    id2214out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2214in_a,id2214in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2215out_o;

  { // Node ID: 2215 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2215in_i = id2214out_result;

    id2215out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2215in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2216out_o;

  { // Node ID: 2216 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2216in_i = id2215out_o;

    id2216out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2216in_i));
  }
  { // Node ID: 3676 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2837out_result;

  { // Node ID: 2837 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2837in_a = id2216out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2837in_b = id3676out_value;

    id2837out_result = (gte_fixed(id2837in_a,id2837in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3088out_result;

  { // Node ID: 3088 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3088in_a = id2222out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3088in_b = id2225out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3088in_c = id3018out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3088in_condb = id2837out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3088x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3088x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3088x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3088x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3088x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3088x_1 = id3088in_a;
        break;
      default:
        id3088x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3088in_condb.getValueAsLong())) {
      case 0l:
        id3088x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3088x_2 = id3088in_b;
        break;
      default:
        id3088x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3088x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3088x_3 = id3088in_c;
        break;
      default:
        id3088x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3088x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3088x_1),(id3088x_2))),(id3088x_3)));
    id3088out_result = (id3088x_4);
  }
  HWRawBits<1> id2838out_result;

  { // Node ID: 2838 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2838in_a = id3088out_result;

    id2838out_result = (slice<13,1>(id2838in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2839out_output;

  { // Node ID: 2839 (NodeReinterpret)
    const HWRawBits<1> &id2839in_input = id2838out_result;

    id2839out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2839in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2231out_result;

  { // Node ID: 2231 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2231in_a = id3139out_output[getCycle()%3];

    id2231out_result = (not_fixed(id2231in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2232out_result;

  { // Node ID: 2232 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2232in_a = id2839out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2232in_b = id2231out_result;

    HWOffsetFix<1,0,UNSIGNED> id2232x_1;

    (id2232x_1) = (and_fixed(id2232in_a,id2232in_b));
    id2232out_result = (id2232x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2233out_result;

  { // Node ID: 2233 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2233in_a = id2232out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2233in_b = id3141out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2233x_1;

    (id2233x_1) = (or_fixed(id2233in_a,id2233in_b));
    id2233out_result = (id2233x_1);
  }
  { // Node ID: 3673 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2840out_result;

  { // Node ID: 2840 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2840in_a = id3088out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2840in_b = id3673out_value;

    id2840out_result = (gte_fixed(id2840in_a,id2840in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2240out_result;

  { // Node ID: 2240 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2240in_a = id3141out_output[getCycle()%3];

    id2240out_result = (not_fixed(id2240in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2241out_result;

  { // Node ID: 2241 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2241in_a = id2840out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2241in_b = id2240out_result;

    HWOffsetFix<1,0,UNSIGNED> id2241x_1;

    (id2241x_1) = (and_fixed(id2241in_a,id2241in_b));
    id2241out_result = (id2241x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2242out_result;

  { // Node ID: 2242 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2242in_a = id2241out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2242in_b = id3139out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2242x_1;

    (id2242x_1) = (or_fixed(id2242in_a,id2242in_b));
    id2242out_result = (id2242x_1);
  }
  HWRawBits<2> id2243out_result;

  { // Node ID: 2243 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2243in_in0 = id2233out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2243in_in1 = id2242out_result;

    id2243out_result = (cat(id2243in_in0,id2243in_in1));
  }
  { // Node ID: 2235 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2234out_o;

  { // Node ID: 2234 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2234in_i = id3088out_result;

    id2234out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2234in_i));
  }
  { // Node ID: 2219 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2220out_result;

  { // Node ID: 2220 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2220in_sel = id2837out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2220in_option0 = id2216out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2220in_option1 = id2219out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2220x_1;

    switch((id2220in_sel.getValueAsLong())) {
      case 0l:
        id2220x_1 = id2220in_option0;
        break;
      case 1l:
        id2220x_1 = id2220in_option1;
        break;
      default:
        id2220x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2220out_result = (id2220x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2221out_o;

  { // Node ID: 2221 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2221in_i = id2220out_result;

    id2221out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2221in_i));
  }
  HWRawBits<56> id2236out_result;

  { // Node ID: 2236 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2236in_in0 = id2235out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2236in_in1 = id2234out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2236in_in2 = id2221out_o;

    id2236out_result = (cat((cat(id2236in_in0,id2236in_in1)),id2236in_in2));
  }
  HWFloat<11,45> id2237out_output;

  { // Node ID: 2237 (NodeReinterpret)
    const HWRawBits<56> &id2237in_input = id2236out_result;

    id2237out_output = (cast_bits2float<11,45>(id2237in_input));
  }
  { // Node ID: 2244 (NodeConstantRawBits)
  }
  { // Node ID: 2245 (NodeConstantRawBits)
  }
  { // Node ID: 2247 (NodeConstantRawBits)
  }
  HWRawBits<56> id2841out_result;

  { // Node ID: 2841 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2841in_in0 = id2244out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2841in_in1 = id2245out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2841in_in2 = id2247out_value;

    id2841out_result = (cat((cat(id2841in_in0,id2841in_in1)),id2841in_in2));
  }
  HWFloat<11,45> id2249out_output;

  { // Node ID: 2249 (NodeReinterpret)
    const HWRawBits<56> &id2249in_input = id2841out_result;

    id2249out_output = (cast_bits2float<11,45>(id2249in_input));
  }
  { // Node ID: 2796 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2252out_result;

  { // Node ID: 2252 (NodeMux)
    const HWRawBits<2> &id2252in_sel = id2243out_result;
    const HWFloat<11,45> &id2252in_option0 = id2237out_output;
    const HWFloat<11,45> &id2252in_option1 = id2249out_output;
    const HWFloat<11,45> &id2252in_option2 = id2796out_value;
    const HWFloat<11,45> &id2252in_option3 = id2249out_output;

    HWFloat<11,45> id2252x_1;

    switch((id2252in_sel.getValueAsLong())) {
      case 0l:
        id2252x_1 = id2252in_option0;
        break;
      case 1l:
        id2252x_1 = id2252in_option1;
        break;
      case 2l:
        id2252x_1 = id2252in_option2;
        break;
      case 3l:
        id2252x_1 = id2252in_option3;
        break;
      default:
        id2252x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2252out_result = (id2252x_1);
  }
  { // Node ID: 3672 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2262out_result;

  { // Node ID: 2262 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2262in_sel = id3144out_output[getCycle()%9];
    const HWFloat<11,45> &id2262in_option0 = id2252out_result;
    const HWFloat<11,45> &id2262in_option1 = id3672out_value;

    HWFloat<11,45> id2262x_1;

    switch((id2262in_sel.getValueAsLong())) {
      case 0l:
        id2262x_1 = id2262in_option0;
        break;
      case 1l:
        id2262x_1 = id2262in_option1;
        break;
      default:
        id2262x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2262out_result = (id2262x_1);
  }
  HWFloat<11,45> id2272out_result;

  { // Node ID: 2272 (NodeAdd)
    const HWFloat<11,45> &id2272in_a = id2165out_result;
    const HWFloat<11,45> &id2272in_b = id2262out_result;

    id2272out_result = (add_float(id2272in_a,id2272in_b));
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2372out_result;

  { // Node ID: 2372 (NodeEq)
    const HWFloat<11,45> &id2372in_a = id2272out_result;
    const HWFloat<11,45> &id2372in_b = id5out_value;

    id2372out_result = (eq_float(id2372in_a,id2372in_b));
  }
  { // Node ID: 2373 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2374out_result;

  { // Node ID: 2374 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2374in_sel = id2372out_result;
    const HWFloat<11,45> &id2374in_option0 = id2272out_result;
    const HWFloat<11,45> &id2374in_option1 = id2373out_value;

    HWFloat<11,45> id2374x_1;

    switch((id2374in_sel.getValueAsLong())) {
      case 0l:
        id2374x_1 = id2374in_option0;
        break;
      case 1l:
        id2374x_1 = id2374in_option1;
        break;
      default:
        id2374x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2374out_result = (id2374x_1);
  }
  HWFloat<11,45> id2479out_result;

  { // Node ID: 2479 (NodeDiv)
    const HWFloat<11,45> &id2479in_a = id3091out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id2479in_b = id2374out_result;

    id2479out_result = (div_float(id2479in_a,id2479in_b));
  }
  { // Node ID: 3671 (NodeConstantRawBits)
  }
  { // Node ID: 3670 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2481out_result;

  { // Node ID: 2481 (NodeAdd)
    const HWFloat<11,45> &id2481in_a = id3432out_output[getCycle()%2];
    const HWFloat<11,45> &id2481in_b = id3670out_value;

    id2481out_result = (add_float(id2481in_a,id2481in_b));
  }
  HWFloat<11,45> id2483out_result;

  { // Node ID: 2483 (NodeMul)
    const HWFloat<11,45> &id2483in_a = id3671out_value;
    const HWFloat<11,45> &id2483in_b = id2481out_result;

    id2483out_result = (mul_float(id2483in_a,id2483in_b));
  }
  { // Node ID: 3669 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2320out_o;

  { // Node ID: 2320 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2320in_i = id3147out_output[getCycle()%3];

    id2320out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2320in_i));
  }
  { // Node ID: 2323 (NodeConstantRawBits)
  }
  { // Node ID: 3020 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id2302out_result;

  { // Node ID: 2302 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2302in_a = id2363out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2302in_b = id3148out_output[getCycle()%3];

    id2302out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2302in_a,id2302in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2303out_result;

  { // Node ID: 2303 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2303in_a = id3148out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2303in_b = id2363out_dout[getCycle()%3];

    id2303out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2303in_a,id2303in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2304out_result;

  { // Node ID: 2304 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2304in_a = id2302out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2304in_b = id2303out_result;

    id2304out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2304in_a,id2304in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2305out_o;

  { // Node ID: 2305 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2305in_i = id2304out_result;

    id2305out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2305in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2306out_result;

  { // Node ID: 2306 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2306in_a = id2366out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2306in_b = id2305out_o;

    id2306out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2306in_a,id2306in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2307out_result;

  { // Node ID: 2307 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2307in_a = id2305out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2307in_b = id2366out_dout[getCycle()%3];

    id2307out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2307in_a,id2307in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2308out_result;

  { // Node ID: 2308 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2308in_a = id2306out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2308in_b = id2307out_result;

    id2308out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2308in_a,id2308in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2309out_o;

  { // Node ID: 2309 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2309in_i = id2308out_result;

    id2309out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2309in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2310out_result;

  { // Node ID: 2310 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2310in_a = id2369out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2310in_b = id2309out_o;

    id2310out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2310in_a,id2310in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2311out_result;

  { // Node ID: 2311 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2311in_a = id2309out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2311in_b = id2369out_dout[getCycle()%3];

    id2311out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2311in_a,id2311in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2312out_result;

  { // Node ID: 2312 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2312in_a = id2310out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2312in_b = id2311out_result;

    id2312out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2312in_a,id2312in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2313out_o;

  { // Node ID: 2313 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2313in_i = id2312out_result;

    id2313out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2313in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2314out_o;

  { // Node ID: 2314 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2314in_i = id2313out_o;

    id2314out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2314in_i));
  }
  { // Node ID: 3665 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2844out_result;

  { // Node ID: 2844 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2844in_a = id2314out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2844in_b = id3665out_value;

    id2844out_result = (gte_fixed(id2844in_a,id2844in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3087out_result;

  { // Node ID: 3087 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3087in_a = id2320out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3087in_b = id2323out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3087in_c = id3020out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3087in_condb = id2844out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3087x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3087x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3087x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3087x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3087x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3087x_1 = id3087in_a;
        break;
      default:
        id3087x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3087in_condb.getValueAsLong())) {
      case 0l:
        id3087x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3087x_2 = id3087in_b;
        break;
      default:
        id3087x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3087x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3087x_3 = id3087in_c;
        break;
      default:
        id3087x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3087x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3087x_1),(id3087x_2))),(id3087x_3)));
    id3087out_result = (id3087x_4);
  }
  HWRawBits<1> id2845out_result;

  { // Node ID: 2845 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2845in_a = id3087out_result;

    id2845out_result = (slice<13,1>(id2845in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2846out_output;

  { // Node ID: 2846 (NodeReinterpret)
    const HWRawBits<1> &id2846in_input = id2845out_result;

    id2846out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2846in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2329out_result;

  { // Node ID: 2329 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2329in_a = id3151out_output[getCycle()%3];

    id2329out_result = (not_fixed(id2329in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2330out_result;

  { // Node ID: 2330 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2330in_a = id2846out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2330in_b = id2329out_result;

    HWOffsetFix<1,0,UNSIGNED> id2330x_1;

    (id2330x_1) = (and_fixed(id2330in_a,id2330in_b));
    id2330out_result = (id2330x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2331out_result;

  { // Node ID: 2331 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2331in_a = id2330out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2331in_b = id3153out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2331x_1;

    (id2331x_1) = (or_fixed(id2331in_a,id2331in_b));
    id2331out_result = (id2331x_1);
  }
  { // Node ID: 3662 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2847out_result;

  { // Node ID: 2847 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2847in_a = id3087out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2847in_b = id3662out_value;

    id2847out_result = (gte_fixed(id2847in_a,id2847in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2338out_result;

  { // Node ID: 2338 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2338in_a = id3153out_output[getCycle()%3];

    id2338out_result = (not_fixed(id2338in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2339out_result;

  { // Node ID: 2339 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2339in_a = id2847out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2339in_b = id2338out_result;

    HWOffsetFix<1,0,UNSIGNED> id2339x_1;

    (id2339x_1) = (and_fixed(id2339in_a,id2339in_b));
    id2339out_result = (id2339x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2340out_result;

  { // Node ID: 2340 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2340in_a = id2339out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2340in_b = id3151out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2340x_1;

    (id2340x_1) = (or_fixed(id2340in_a,id2340in_b));
    id2340out_result = (id2340x_1);
  }
  HWRawBits<2> id2341out_result;

  { // Node ID: 2341 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2341in_in0 = id2331out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2341in_in1 = id2340out_result;

    id2341out_result = (cat(id2341in_in0,id2341in_in1));
  }
  { // Node ID: 2333 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2332out_o;

  { // Node ID: 2332 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2332in_i = id3087out_result;

    id2332out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2332in_i));
  }
  { // Node ID: 2317 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2318out_result;

  { // Node ID: 2318 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2318in_sel = id2844out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2318in_option0 = id2314out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2318in_option1 = id2317out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2318x_1;

    switch((id2318in_sel.getValueAsLong())) {
      case 0l:
        id2318x_1 = id2318in_option0;
        break;
      case 1l:
        id2318x_1 = id2318in_option1;
        break;
      default:
        id2318x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2318out_result = (id2318x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2319out_o;

  { // Node ID: 2319 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2319in_i = id2318out_result;

    id2319out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2319in_i));
  }
  HWRawBits<56> id2334out_result;

  { // Node ID: 2334 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2334in_in0 = id2333out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2334in_in1 = id2332out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2334in_in2 = id2319out_o;

    id2334out_result = (cat((cat(id2334in_in0,id2334in_in1)),id2334in_in2));
  }
  HWFloat<11,45> id2335out_output;

  { // Node ID: 2335 (NodeReinterpret)
    const HWRawBits<56> &id2335in_input = id2334out_result;

    id2335out_output = (cast_bits2float<11,45>(id2335in_input));
  }
  { // Node ID: 2342 (NodeConstantRawBits)
  }
  { // Node ID: 2343 (NodeConstantRawBits)
  }
  { // Node ID: 2345 (NodeConstantRawBits)
  }
  HWRawBits<56> id2848out_result;

  { // Node ID: 2848 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2848in_in0 = id2342out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2848in_in1 = id2343out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2848in_in2 = id2345out_value;

    id2848out_result = (cat((cat(id2848in_in0,id2848in_in1)),id2848in_in2));
  }
  HWFloat<11,45> id2347out_output;

  { // Node ID: 2347 (NodeReinterpret)
    const HWRawBits<56> &id2347in_input = id2848out_result;

    id2347out_output = (cast_bits2float<11,45>(id2347in_input));
  }
  { // Node ID: 2797 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2350out_result;

  { // Node ID: 2350 (NodeMux)
    const HWRawBits<2> &id2350in_sel = id2341out_result;
    const HWFloat<11,45> &id2350in_option0 = id2335out_output;
    const HWFloat<11,45> &id2350in_option1 = id2347out_output;
    const HWFloat<11,45> &id2350in_option2 = id2797out_value;
    const HWFloat<11,45> &id2350in_option3 = id2347out_output;

    HWFloat<11,45> id2350x_1;

    switch((id2350in_sel.getValueAsLong())) {
      case 0l:
        id2350x_1 = id2350in_option0;
        break;
      case 1l:
        id2350x_1 = id2350in_option1;
        break;
      case 2l:
        id2350x_1 = id2350in_option2;
        break;
      case 3l:
        id2350x_1 = id2350in_option3;
        break;
      default:
        id2350x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2350out_result = (id2350x_1);
  }
  { // Node ID: 3661 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2360out_result;

  { // Node ID: 2360 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2360in_sel = id3156out_output[getCycle()%9];
    const HWFloat<11,45> &id2360in_option0 = id2350out_result;
    const HWFloat<11,45> &id2360in_option1 = id3661out_value;

    HWFloat<11,45> id2360x_1;

    switch((id2360in_sel.getValueAsLong())) {
      case 0l:
        id2360x_1 = id2360in_option0;
        break;
      case 1l:
        id2360x_1 = id2360in_option1;
        break;
      default:
        id2360x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2360out_result = (id2360x_1);
  }
  HWFloat<11,45> id2371out_result;

  { // Node ID: 2371 (NodeSub)
    const HWFloat<11,45> &id2371in_a = id3669out_value;
    const HWFloat<11,45> &id2371in_b = id2360out_result;

    id2371out_result = (sub_float(id2371in_a,id2371in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2375out_result;

  { // Node ID: 2375 (NodeEq)
    const HWFloat<11,45> &id2375in_a = id2371out_result;
    const HWFloat<11,45> &id2375in_b = id5out_value;

    id2375out_result = (eq_float(id2375in_a,id2375in_b));
  }
  { // Node ID: 2376 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2377out_result;

  { // Node ID: 2377 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2377in_sel = id2375out_result;
    const HWFloat<11,45> &id2377in_option0 = id2371out_result;
    const HWFloat<11,45> &id2377in_option1 = id2376out_value;

    HWFloat<11,45> id2377x_1;

    switch((id2377in_sel.getValueAsLong())) {
      case 0l:
        id2377x_1 = id2377in_option0;
        break;
      case 1l:
        id2377x_1 = id2377in_option1;
        break;
      default:
        id2377x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2377out_result = (id2377x_1);
  }
  HWFloat<11,45> id2484out_result;

  { // Node ID: 2484 (NodeDiv)
    const HWFloat<11,45> &id2484in_a = id2483out_result;
    const HWFloat<11,45> &id2484in_b = id2377out_result;

    id2484out_result = (div_float(id2484in_a,id2484in_b));
  }
  HWFloat<11,45> id2485out_result;

  { // Node ID: 2485 (NodeAdd)
    const HWFloat<11,45> &id2485in_a = id2479out_result;
    const HWFloat<11,45> &id2485in_b = id2484out_result;

    id2485out_result = (add_float(id2485in_a,id2485in_b));
  }
  HWFloat<11,45> id2487out_result;

  { // Node ID: 2487 (NodeMul)
    const HWFloat<11,45> &id2487in_a = id3697out_value;
    const HWFloat<11,45> &id2487in_b = id2485out_result;

    id2487out_result = (mul_float(id2487in_a,id2487in_b));
  }
  { // Node ID: 3660 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2535out_o;

  { // Node ID: 2535 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2535in_i = id3158out_output[getCycle()%3];

    id2535out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2535in_i));
  }
  { // Node ID: 2538 (NodeConstantRawBits)
  }
  { // Node ID: 3022 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id2517out_result;

  { // Node ID: 2517 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2517in_a = id2578out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2517in_b = id3159out_output[getCycle()%3];

    id2517out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2517in_a,id2517in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2518out_result;

  { // Node ID: 2518 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2518in_a = id3159out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2518in_b = id2578out_dout[getCycle()%3];

    id2518out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2518in_a,id2518in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2519out_result;

  { // Node ID: 2519 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2519in_a = id2517out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2519in_b = id2518out_result;

    id2519out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2519in_a,id2519in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2520out_o;

  { // Node ID: 2520 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2520in_i = id2519out_result;

    id2520out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2520in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2521out_result;

  { // Node ID: 2521 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2521in_a = id2581out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2521in_b = id2520out_o;

    id2521out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2521in_a,id2521in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2522out_result;

  { // Node ID: 2522 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2522in_a = id2520out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2522in_b = id2581out_dout[getCycle()%3];

    id2522out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2522in_a,id2522in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2523out_result;

  { // Node ID: 2523 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2523in_a = id2521out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2523in_b = id2522out_result;

    id2523out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2523in_a,id2523in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2524out_o;

  { // Node ID: 2524 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2524in_i = id2523out_result;

    id2524out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2524in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2525out_result;

  { // Node ID: 2525 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2525in_a = id2584out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2525in_b = id2524out_o;

    id2525out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2525in_a,id2525in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2526out_result;

  { // Node ID: 2526 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2526in_a = id2524out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2526in_b = id2584out_dout[getCycle()%3];

    id2526out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2526in_a,id2526in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2527out_result;

  { // Node ID: 2527 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2527in_a = id2525out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2527in_b = id2526out_result;

    id2527out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2527in_a,id2527in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2528out_o;

  { // Node ID: 2528 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2528in_i = id2527out_result;

    id2528out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2528in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2529out_o;

  { // Node ID: 2529 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2529in_i = id2528out_o;

    id2529out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2529in_i));
  }
  { // Node ID: 3656 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2851out_result;

  { // Node ID: 2851 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2851in_a = id2529out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2851in_b = id3656out_value;

    id2851out_result = (gte_fixed(id2851in_a,id2851in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3086out_result;

  { // Node ID: 3086 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3086in_a = id2535out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3086in_b = id2538out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3086in_c = id3022out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3086in_condb = id2851out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3086x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3086x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3086x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3086x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3086x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3086x_1 = id3086in_a;
        break;
      default:
        id3086x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3086in_condb.getValueAsLong())) {
      case 0l:
        id3086x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3086x_2 = id3086in_b;
        break;
      default:
        id3086x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3086x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3086x_3 = id3086in_c;
        break;
      default:
        id3086x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3086x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3086x_1),(id3086x_2))),(id3086x_3)));
    id3086out_result = (id3086x_4);
  }
  HWRawBits<1> id2852out_result;

  { // Node ID: 2852 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2852in_a = id3086out_result;

    id2852out_result = (slice<13,1>(id2852in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2853out_output;

  { // Node ID: 2853 (NodeReinterpret)
    const HWRawBits<1> &id2853in_input = id2852out_result;

    id2853out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2853in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2544out_result;

  { // Node ID: 2544 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2544in_a = id3162out_output[getCycle()%3];

    id2544out_result = (not_fixed(id2544in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2545out_result;

  { // Node ID: 2545 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2545in_a = id2853out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2545in_b = id2544out_result;

    HWOffsetFix<1,0,UNSIGNED> id2545x_1;

    (id2545x_1) = (and_fixed(id2545in_a,id2545in_b));
    id2545out_result = (id2545x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2546out_result;

  { // Node ID: 2546 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2546in_a = id2545out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2546in_b = id3164out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2546x_1;

    (id2546x_1) = (or_fixed(id2546in_a,id2546in_b));
    id2546out_result = (id2546x_1);
  }
  { // Node ID: 3653 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2854out_result;

  { // Node ID: 2854 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2854in_a = id3086out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2854in_b = id3653out_value;

    id2854out_result = (gte_fixed(id2854in_a,id2854in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2553out_result;

  { // Node ID: 2553 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2553in_a = id3164out_output[getCycle()%3];

    id2553out_result = (not_fixed(id2553in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2554out_result;

  { // Node ID: 2554 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2554in_a = id2854out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2554in_b = id2553out_result;

    HWOffsetFix<1,0,UNSIGNED> id2554x_1;

    (id2554x_1) = (and_fixed(id2554in_a,id2554in_b));
    id2554out_result = (id2554x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2555out_result;

  { // Node ID: 2555 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2555in_a = id2554out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2555in_b = id3162out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2555x_1;

    (id2555x_1) = (or_fixed(id2555in_a,id2555in_b));
    id2555out_result = (id2555x_1);
  }
  HWRawBits<2> id2556out_result;

  { // Node ID: 2556 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2556in_in0 = id2546out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2556in_in1 = id2555out_result;

    id2556out_result = (cat(id2556in_in0,id2556in_in1));
  }
  { // Node ID: 2548 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2547out_o;

  { // Node ID: 2547 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2547in_i = id3086out_result;

    id2547out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2547in_i));
  }
  { // Node ID: 2532 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2533out_result;

  { // Node ID: 2533 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2533in_sel = id2851out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2533in_option0 = id2529out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2533in_option1 = id2532out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2533x_1;

    switch((id2533in_sel.getValueAsLong())) {
      case 0l:
        id2533x_1 = id2533in_option0;
        break;
      case 1l:
        id2533x_1 = id2533in_option1;
        break;
      default:
        id2533x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2533out_result = (id2533x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2534out_o;

  { // Node ID: 2534 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2534in_i = id2533out_result;

    id2534out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2534in_i));
  }
  HWRawBits<56> id2549out_result;

  { // Node ID: 2549 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2549in_in0 = id2548out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2549in_in1 = id2547out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2549in_in2 = id2534out_o;

    id2549out_result = (cat((cat(id2549in_in0,id2549in_in1)),id2549in_in2));
  }
  HWFloat<11,45> id2550out_output;

  { // Node ID: 2550 (NodeReinterpret)
    const HWRawBits<56> &id2550in_input = id2549out_result;

    id2550out_output = (cast_bits2float<11,45>(id2550in_input));
  }
  { // Node ID: 2557 (NodeConstantRawBits)
  }
  { // Node ID: 2558 (NodeConstantRawBits)
  }
  { // Node ID: 2560 (NodeConstantRawBits)
  }
  HWRawBits<56> id2855out_result;

  { // Node ID: 2855 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2855in_in0 = id2557out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2855in_in1 = id2558out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2855in_in2 = id2560out_value;

    id2855out_result = (cat((cat(id2855in_in0,id2855in_in1)),id2855in_in2));
  }
  HWFloat<11,45> id2562out_output;

  { // Node ID: 2562 (NodeReinterpret)
    const HWRawBits<56> &id2562in_input = id2855out_result;

    id2562out_output = (cast_bits2float<11,45>(id2562in_input));
  }
  { // Node ID: 2798 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2565out_result;

  { // Node ID: 2565 (NodeMux)
    const HWRawBits<2> &id2565in_sel = id2556out_result;
    const HWFloat<11,45> &id2565in_option0 = id2550out_output;
    const HWFloat<11,45> &id2565in_option1 = id2562out_output;
    const HWFloat<11,45> &id2565in_option2 = id2798out_value;
    const HWFloat<11,45> &id2565in_option3 = id2562out_output;

    HWFloat<11,45> id2565x_1;

    switch((id2565in_sel.getValueAsLong())) {
      case 0l:
        id2565x_1 = id2565in_option0;
        break;
      case 1l:
        id2565x_1 = id2565in_option1;
        break;
      case 2l:
        id2565x_1 = id2565in_option2;
        break;
      case 3l:
        id2565x_1 = id2565in_option3;
        break;
      default:
        id2565x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2565out_result = (id2565x_1);
  }
  { // Node ID: 3652 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2575out_result;

  { // Node ID: 2575 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2575in_sel = id3167out_output[getCycle()%9];
    const HWFloat<11,45> &id2575in_option0 = id2565out_result;
    const HWFloat<11,45> &id2575in_option1 = id3652out_value;

    HWFloat<11,45> id2575x_1;

    switch((id2575in_sel.getValueAsLong())) {
      case 0l:
        id2575x_1 = id2575in_option0;
        break;
      case 1l:
        id2575x_1 = id2575in_option1;
        break;
      default:
        id2575x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2575out_result = (id2575x_1);
  }
  { // Node ID: 3651 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2586out_result;

  { // Node ID: 2586 (NodeSub)
    const HWFloat<11,45> &id2586in_a = id2575out_result;
    const HWFloat<11,45> &id2586in_b = id3651out_value;

    id2586out_result = (sub_float(id2586in_a,id2586in_b));
  }
  HWFloat<11,45> id2588out_result;

  { // Node ID: 2588 (NodeMul)
    const HWFloat<11,45> &id2588in_a = id3660out_value;
    const HWFloat<11,45> &id2588in_b = id2586out_result;

    id2588out_result = (mul_float(id2588in_a,id2588in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2636out_o;

  { // Node ID: 2636 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2636in_i = id3169out_output[getCycle()%3];

    id2636out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2636in_i));
  }
  { // Node ID: 2639 (NodeConstantRawBits)
  }
  { // Node ID: 3024 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id2618out_result;

  { // Node ID: 2618 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2618in_a = id2679out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2618in_b = id3170out_output[getCycle()%3];

    id2618out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2618in_a,id2618in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2619out_result;

  { // Node ID: 2619 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2619in_a = id3170out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2619in_b = id2679out_dout[getCycle()%3];

    id2619out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2619in_a,id2619in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2620out_result;

  { // Node ID: 2620 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2620in_a = id2618out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2620in_b = id2619out_result;

    id2620out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2620in_a,id2620in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2621out_o;

  { // Node ID: 2621 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2621in_i = id2620out_result;

    id2621out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2621in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2622out_result;

  { // Node ID: 2622 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2622in_a = id2682out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2622in_b = id2621out_o;

    id2622out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2622in_a,id2622in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2623out_result;

  { // Node ID: 2623 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2623in_a = id2621out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2623in_b = id2682out_dout[getCycle()%3];

    id2623out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2623in_a,id2623in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2624out_result;

  { // Node ID: 2624 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2624in_a = id2622out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2624in_b = id2623out_result;

    id2624out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2624in_a,id2624in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2625out_o;

  { // Node ID: 2625 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2625in_i = id2624out_result;

    id2625out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2625in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2626out_result;

  { // Node ID: 2626 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2626in_a = id2685out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2626in_b = id2625out_o;

    id2626out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2626in_a,id2626in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2627out_result;

  { // Node ID: 2627 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2627in_a = id2625out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2627in_b = id2685out_dout[getCycle()%3];

    id2627out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2627in_a,id2627in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2628out_result;

  { // Node ID: 2628 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2628in_a = id2626out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2628in_b = id2627out_result;

    id2628out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2628in_a,id2628in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2629out_o;

  { // Node ID: 2629 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2629in_i = id2628out_result;

    id2629out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2629in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2630out_o;

  { // Node ID: 2630 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2630in_i = id2629out_o;

    id2630out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2630in_i));
  }
  { // Node ID: 3647 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2858out_result;

  { // Node ID: 2858 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2858in_a = id2630out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2858in_b = id3647out_value;

    id2858out_result = (gte_fixed(id2858in_a,id2858in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3085out_result;

  { // Node ID: 3085 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3085in_a = id2636out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3085in_b = id2639out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3085in_c = id3024out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3085in_condb = id2858out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3085x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3085x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3085x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3085x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3085x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3085x_1 = id3085in_a;
        break;
      default:
        id3085x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3085in_condb.getValueAsLong())) {
      case 0l:
        id3085x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3085x_2 = id3085in_b;
        break;
      default:
        id3085x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3085x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3085x_3 = id3085in_c;
        break;
      default:
        id3085x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3085x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3085x_1),(id3085x_2))),(id3085x_3)));
    id3085out_result = (id3085x_4);
  }
  HWRawBits<1> id2859out_result;

  { // Node ID: 2859 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2859in_a = id3085out_result;

    id2859out_result = (slice<13,1>(id2859in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2860out_output;

  { // Node ID: 2860 (NodeReinterpret)
    const HWRawBits<1> &id2860in_input = id2859out_result;

    id2860out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2860in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id2645out_result;

  { // Node ID: 2645 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2645in_a = id3173out_output[getCycle()%3];

    id2645out_result = (not_fixed(id2645in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2646out_result;

  { // Node ID: 2646 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2646in_a = id2860out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2646in_b = id2645out_result;

    HWOffsetFix<1,0,UNSIGNED> id2646x_1;

    (id2646x_1) = (and_fixed(id2646in_a,id2646in_b));
    id2646out_result = (id2646x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2647out_result;

  { // Node ID: 2647 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2647in_a = id2646out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2647in_b = id3175out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2647x_1;

    (id2647x_1) = (or_fixed(id2647in_a,id2647in_b));
    id2647out_result = (id2647x_1);
  }
  { // Node ID: 3644 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2861out_result;

  { // Node ID: 2861 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2861in_a = id3085out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2861in_b = id3644out_value;

    id2861out_result = (gte_fixed(id2861in_a,id2861in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2654out_result;

  { // Node ID: 2654 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2654in_a = id3175out_output[getCycle()%3];

    id2654out_result = (not_fixed(id2654in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2655out_result;

  { // Node ID: 2655 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2655in_a = id2861out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2655in_b = id2654out_result;

    HWOffsetFix<1,0,UNSIGNED> id2655x_1;

    (id2655x_1) = (and_fixed(id2655in_a,id2655in_b));
    id2655out_result = (id2655x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2656out_result;

  { // Node ID: 2656 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2656in_a = id2655out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2656in_b = id3173out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2656x_1;

    (id2656x_1) = (or_fixed(id2656in_a,id2656in_b));
    id2656out_result = (id2656x_1);
  }
  HWRawBits<2> id2657out_result;

  { // Node ID: 2657 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2657in_in0 = id2647out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2657in_in1 = id2656out_result;

    id2657out_result = (cat(id2657in_in0,id2657in_in1));
  }
  { // Node ID: 2649 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2648out_o;

  { // Node ID: 2648 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2648in_i = id3085out_result;

    id2648out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2648in_i));
  }
  { // Node ID: 2633 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2634out_result;

  { // Node ID: 2634 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2634in_sel = id2858out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2634in_option0 = id2630out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2634in_option1 = id2633out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2634x_1;

    switch((id2634in_sel.getValueAsLong())) {
      case 0l:
        id2634x_1 = id2634in_option0;
        break;
      case 1l:
        id2634x_1 = id2634in_option1;
        break;
      default:
        id2634x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2634out_result = (id2634x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2635out_o;

  { // Node ID: 2635 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2635in_i = id2634out_result;

    id2635out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2635in_i));
  }
  HWRawBits<56> id2650out_result;

  { // Node ID: 2650 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2650in_in0 = id2649out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2650in_in1 = id2648out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2650in_in2 = id2635out_o;

    id2650out_result = (cat((cat(id2650in_in0,id2650in_in1)),id2650in_in2));
  }
  HWFloat<11,45> id2651out_output;

  { // Node ID: 2651 (NodeReinterpret)
    const HWRawBits<56> &id2651in_input = id2650out_result;

    id2651out_output = (cast_bits2float<11,45>(id2651in_input));
  }
  { // Node ID: 2658 (NodeConstantRawBits)
  }
  { // Node ID: 2659 (NodeConstantRawBits)
  }
  { // Node ID: 2661 (NodeConstantRawBits)
  }
  HWRawBits<56> id2862out_result;

  { // Node ID: 2862 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2862in_in0 = id2658out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2862in_in1 = id2659out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2862in_in2 = id2661out_value;

    id2862out_result = (cat((cat(id2862in_in0,id2862in_in1)),id2862in_in2));
  }
  HWFloat<11,45> id2663out_output;

  { // Node ID: 2663 (NodeReinterpret)
    const HWRawBits<56> &id2663in_input = id2862out_result;

    id2663out_output = (cast_bits2float<11,45>(id2663in_input));
  }
  { // Node ID: 2799 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2666out_result;

  { // Node ID: 2666 (NodeMux)
    const HWRawBits<2> &id2666in_sel = id2657out_result;
    const HWFloat<11,45> &id2666in_option0 = id2651out_output;
    const HWFloat<11,45> &id2666in_option1 = id2663out_output;
    const HWFloat<11,45> &id2666in_option2 = id2799out_value;
    const HWFloat<11,45> &id2666in_option3 = id2663out_output;

    HWFloat<11,45> id2666x_1;

    switch((id2666in_sel.getValueAsLong())) {
      case 0l:
        id2666x_1 = id2666in_option0;
        break;
      case 1l:
        id2666x_1 = id2666in_option1;
        break;
      case 2l:
        id2666x_1 = id2666in_option2;
        break;
      case 3l:
        id2666x_1 = id2666in_option3;
        break;
      default:
        id2666x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2666out_result = (id2666x_1);
  }
  { // Node ID: 3643 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2676out_result;

  { // Node ID: 2676 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2676in_sel = id3178out_output[getCycle()%9];
    const HWFloat<11,45> &id2676in_option0 = id2666out_result;
    const HWFloat<11,45> &id2676in_option1 = id3643out_value;

    HWFloat<11,45> id2676x_1;

    switch((id2676in_sel.getValueAsLong())) {
      case 0l:
        id2676x_1 = id2676in_option0;
        break;
      case 1l:
        id2676x_1 = id2676in_option1;
        break;
      default:
        id2676x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2676out_result = (id2676x_1);
  }
  HWFloat<11,45> id2686out_result;

  { // Node ID: 2686 (NodeDiv)
    const HWFloat<11,45> &id2686in_a = id2588out_result;
    const HWFloat<11,45> &id2686in_b = id2676out_result;

    id2686out_result = (div_float(id2686in_a,id2686in_b));
  }
  HWFloat<11,45> id2687out_result;

  { // Node ID: 2687 (NodeMul)
    const HWFloat<11,45> &id2687in_a = id2686out_result;
    const HWFloat<11,45> &id2687in_b = id3179out_output[getCycle()%2];

    id2687out_result = (mul_float(id2687in_a,id2687in_b));
  }
  HWFloat<11,45> id2696out_result;

  { // Node ID: 2696 (NodeAdd)
    const HWFloat<11,45> &id2696in_a = id2487out_result;
    const HWFloat<11,45> &id2696in_b = id2687out_result;

    id2696out_result = (add_float(id2696in_a,id2696in_b));
  }
  HWFloat<11,45> id2689out_result;

  { // Node ID: 2689 (NodeMul)
    const HWFloat<11,45> &id2689in_a = id3092out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id2689in_b = id3224out_output[getCycle()%2];

    id2689out_result = (mul_float(id2689in_a,id2689in_b));
  }
  HWFloat<11,45> id2690out_result;

  { // Node ID: 2690 (NodeMul)
    const HWFloat<11,45> &id2690in_a = id2689out_result;
    const HWFloat<11,45> &id2690in_b = id3224out_output[getCycle()%2];

    id2690out_result = (mul_float(id2690in_a,id2690in_b));
  }
  { // Node ID: 3582 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id697out_o;

  { // Node ID: 697 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id697in_i = id3251out_output[getCycle()%3];

    id697out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id697in_i));
  }
  { // Node ID: 700 (NodeConstantRawBits)
  }
  { // Node ID: 3038 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id679out_result;

  { // Node ID: 679 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id679in_a = id740out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id679in_b = id3252out_output[getCycle()%3];

    id679out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id679in_a,id679in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id680out_result;

  { // Node ID: 680 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id680in_a = id3252out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id680in_b = id740out_dout[getCycle()%3];

    id680out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id680in_a,id680in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id681out_result;

  { // Node ID: 681 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id681in_a = id679out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id681in_b = id680out_result;

    id681out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id681in_a,id681in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id682out_o;

  { // Node ID: 682 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id682in_i = id681out_result;

    id682out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id682in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id683out_result;

  { // Node ID: 683 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id683in_a = id743out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id683in_b = id682out_o;

    id683out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id683in_a,id683in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id684out_result;

  { // Node ID: 684 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id684in_a = id682out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id684in_b = id743out_dout[getCycle()%3];

    id684out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id684in_a,id684in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id685out_result;

  { // Node ID: 685 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id685in_a = id683out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id685in_b = id684out_result;

    id685out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id685in_a,id685in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id686out_o;

  { // Node ID: 686 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id686in_i = id685out_result;

    id686out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id686in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id687out_result;

  { // Node ID: 687 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id687in_a = id746out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id687in_b = id686out_o;

    id687out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id687in_a,id687in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id688out_result;

  { // Node ID: 688 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id688in_a = id686out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id688in_b = id746out_dout[getCycle()%3];

    id688out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id688in_a,id688in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id689out_result;

  { // Node ID: 689 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id689in_a = id687out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id689in_b = id688out_result;

    id689out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id689in_a,id689in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id690out_o;

  { // Node ID: 690 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id690in_i = id689out_result;

    id690out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id690in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id691out_o;

  { // Node ID: 691 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id691in_i = id690out_o;

    id691out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id691in_i));
  }
  { // Node ID: 3579 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2910out_result;

  { // Node ID: 2910 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2910in_a = id691out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2910in_b = id3579out_value;

    id2910out_result = (gte_fixed(id2910in_a,id2910in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3078out_result;

  { // Node ID: 3078 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3078in_a = id697out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3078in_b = id700out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3078in_c = id3038out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3078in_condb = id2910out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3078x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3078x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3078x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3078x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3078x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3078x_1 = id3078in_a;
        break;
      default:
        id3078x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3078in_condb.getValueAsLong())) {
      case 0l:
        id3078x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3078x_2 = id3078in_b;
        break;
      default:
        id3078x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3078x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3078x_3 = id3078in_c;
        break;
      default:
        id3078x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3078x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3078x_1),(id3078x_2))),(id3078x_3)));
    id3078out_result = (id3078x_4);
  }
  HWRawBits<1> id2911out_result;

  { // Node ID: 2911 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2911in_a = id3078out_result;

    id2911out_result = (slice<13,1>(id2911in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2912out_output;

  { // Node ID: 2912 (NodeReinterpret)
    const HWRawBits<1> &id2912in_input = id2911out_result;

    id2912out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2912in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id706out_result;

  { // Node ID: 706 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id706in_a = id3255out_output[getCycle()%3];

    id706out_result = (not_fixed(id706in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id707out_result;

  { // Node ID: 707 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id707in_a = id2912out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id707in_b = id706out_result;

    HWOffsetFix<1,0,UNSIGNED> id707x_1;

    (id707x_1) = (and_fixed(id707in_a,id707in_b));
    id707out_result = (id707x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id708out_result;

  { // Node ID: 708 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id708in_a = id707out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id708in_b = id3257out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id708x_1;

    (id708x_1) = (or_fixed(id708in_a,id708in_b));
    id708out_result = (id708x_1);
  }
  { // Node ID: 3576 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2913out_result;

  { // Node ID: 2913 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2913in_a = id3078out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2913in_b = id3576out_value;

    id2913out_result = (gte_fixed(id2913in_a,id2913in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id715out_result;

  { // Node ID: 715 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id715in_a = id3257out_output[getCycle()%3];

    id715out_result = (not_fixed(id715in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id716out_result;

  { // Node ID: 716 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id716in_a = id2913out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id716in_b = id715out_result;

    HWOffsetFix<1,0,UNSIGNED> id716x_1;

    (id716x_1) = (and_fixed(id716in_a,id716in_b));
    id716out_result = (id716x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id717out_result;

  { // Node ID: 717 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id717in_a = id716out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id717in_b = id3255out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id717x_1;

    (id717x_1) = (or_fixed(id717in_a,id717in_b));
    id717out_result = (id717x_1);
  }
  HWRawBits<2> id718out_result;

  { // Node ID: 718 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id718in_in0 = id708out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id718in_in1 = id717out_result;

    id718out_result = (cat(id718in_in0,id718in_in1));
  }
  { // Node ID: 710 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id709out_o;

  { // Node ID: 709 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id709in_i = id3078out_result;

    id709out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id709in_i));
  }
  { // Node ID: 694 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id695out_result;

  { // Node ID: 695 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id695in_sel = id2910out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id695in_option0 = id691out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id695in_option1 = id694out_value;

    HWOffsetFix<45,-44,UNSIGNED> id695x_1;

    switch((id695in_sel.getValueAsLong())) {
      case 0l:
        id695x_1 = id695in_option0;
        break;
      case 1l:
        id695x_1 = id695in_option1;
        break;
      default:
        id695x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id695out_result = (id695x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id696out_o;

  { // Node ID: 696 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id696in_i = id695out_result;

    id696out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id696in_i));
  }
  HWRawBits<56> id711out_result;

  { // Node ID: 711 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id711in_in0 = id710out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id711in_in1 = id709out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id711in_in2 = id696out_o;

    id711out_result = (cat((cat(id711in_in0,id711in_in1)),id711in_in2));
  }
  HWFloat<11,45> id712out_output;

  { // Node ID: 712 (NodeReinterpret)
    const HWRawBits<56> &id712in_input = id711out_result;

    id712out_output = (cast_bits2float<11,45>(id712in_input));
  }
  { // Node ID: 719 (NodeConstantRawBits)
  }
  { // Node ID: 720 (NodeConstantRawBits)
  }
  { // Node ID: 722 (NodeConstantRawBits)
  }
  HWRawBits<56> id2914out_result;

  { // Node ID: 2914 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2914in_in0 = id719out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2914in_in1 = id720out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2914in_in2 = id722out_value;

    id2914out_result = (cat((cat(id2914in_in0,id2914in_in1)),id2914in_in2));
  }
  HWFloat<11,45> id724out_output;

  { // Node ID: 724 (NodeReinterpret)
    const HWRawBits<56> &id724in_input = id2914out_result;

    id724out_output = (cast_bits2float<11,45>(id724in_input));
  }
  { // Node ID: 2806 (NodeConstantRawBits)
  }
  HWFloat<11,45> id727out_result;

  { // Node ID: 727 (NodeMux)
    const HWRawBits<2> &id727in_sel = id718out_result;
    const HWFloat<11,45> &id727in_option0 = id712out_output;
    const HWFloat<11,45> &id727in_option1 = id724out_output;
    const HWFloat<11,45> &id727in_option2 = id2806out_value;
    const HWFloat<11,45> &id727in_option3 = id724out_output;

    HWFloat<11,45> id727x_1;

    switch((id727in_sel.getValueAsLong())) {
      case 0l:
        id727x_1 = id727in_option0;
        break;
      case 1l:
        id727x_1 = id727in_option1;
        break;
      case 2l:
        id727x_1 = id727in_option2;
        break;
      case 3l:
        id727x_1 = id727in_option3;
        break;
      default:
        id727x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id727out_result = (id727x_1);
  }
  { // Node ID: 3575 (NodeConstantRawBits)
  }
  HWFloat<11,45> id737out_result;

  { // Node ID: 737 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id737in_sel = id3260out_output[getCycle()%9];
    const HWFloat<11,45> &id737in_option0 = id727out_result;
    const HWFloat<11,45> &id737in_option1 = id3575out_value;

    HWFloat<11,45> id737x_1;

    switch((id737in_sel.getValueAsLong())) {
      case 0l:
        id737x_1 = id737in_option0;
        break;
      case 1l:
        id737x_1 = id737in_option1;
        break;
      default:
        id737x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id737out_result = (id737x_1);
  }
  HWFloat<11,45> id748out_result;

  { // Node ID: 748 (NodeMul)
    const HWFloat<11,45> &id748in_a = id3582out_value;
    const HWFloat<11,45> &id748in_b = id737out_result;

    id748out_result = (mul_float(id748in_a,id748in_b));
  }
  HWFloat<11,45> id749out_result;

  { // Node ID: 749 (NodeMul)
    const HWFloat<11,45> &id749in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id749in_b = id748out_result;

    id749out_result = (mul_float(id749in_a,id749in_b));
  }
  { // Node ID: 3574 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1967out_result;

  { // Node ID: 1967 (NodeSub)
    const HWFloat<11,45> &id1967in_a = id3574out_value;
    const HWFloat<11,45> &id1967in_b = id3261out_output[getCycle()%10];

    id1967out_result = (sub_float(id1967in_a,id1967in_b));
  }
  HWFloat<11,45> id1968out_result;

  { // Node ID: 1968 (NodeMul)
    const HWFloat<11,45> &id1968in_a = id749out_result;
    const HWFloat<11,45> &id1968in_b = id1967out_result;

    id1968out_result = (mul_float(id1968in_a,id1968in_b));
  }
  { // Node ID: 3573 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id797out_o;

  { // Node ID: 797 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id797in_i = id3263out_output[getCycle()%3];

    id797out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id797in_i));
  }
  { // Node ID: 800 (NodeConstantRawBits)
  }
  { // Node ID: 3040 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id779out_result;

  { // Node ID: 779 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id779in_a = id840out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id779in_b = id3264out_output[getCycle()%3];

    id779out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id779in_a,id779in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id780out_result;

  { // Node ID: 780 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id780in_a = id3264out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id780in_b = id840out_dout[getCycle()%3];

    id780out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id780in_a,id780in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id781out_result;

  { // Node ID: 781 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id781in_a = id779out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id781in_b = id780out_result;

    id781out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id781in_a,id781in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id782out_o;

  { // Node ID: 782 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id782in_i = id781out_result;

    id782out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id782in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id783out_result;

  { // Node ID: 783 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id783in_a = id843out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id783in_b = id782out_o;

    id783out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id783in_a,id783in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id784out_result;

  { // Node ID: 784 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id784in_a = id782out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id784in_b = id843out_dout[getCycle()%3];

    id784out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id784in_a,id784in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id785out_result;

  { // Node ID: 785 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id785in_a = id783out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id785in_b = id784out_result;

    id785out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id785in_a,id785in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id786out_o;

  { // Node ID: 786 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id786in_i = id785out_result;

    id786out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id786in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id787out_result;

  { // Node ID: 787 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id787in_a = id846out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id787in_b = id786out_o;

    id787out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id787in_a,id787in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id788out_result;

  { // Node ID: 788 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id788in_a = id786out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id788in_b = id846out_dout[getCycle()%3];

    id788out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id788in_a,id788in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id789out_result;

  { // Node ID: 789 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id789in_a = id787out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id789in_b = id788out_result;

    id789out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id789in_a,id789in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id790out_o;

  { // Node ID: 790 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id790in_i = id789out_result;

    id790out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id790in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id791out_o;

  { // Node ID: 791 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id791in_i = id790out_o;

    id791out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id791in_i));
  }
  { // Node ID: 3569 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2917out_result;

  { // Node ID: 2917 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2917in_a = id791out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2917in_b = id3569out_value;

    id2917out_result = (gte_fixed(id2917in_a,id2917in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3077out_result;

  { // Node ID: 3077 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3077in_a = id797out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3077in_b = id800out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3077in_c = id3040out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3077in_condb = id2917out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3077x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3077x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3077x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3077x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3077x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3077x_1 = id3077in_a;
        break;
      default:
        id3077x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3077in_condb.getValueAsLong())) {
      case 0l:
        id3077x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3077x_2 = id3077in_b;
        break;
      default:
        id3077x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3077x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3077x_3 = id3077in_c;
        break;
      default:
        id3077x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3077x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3077x_1),(id3077x_2))),(id3077x_3)));
    id3077out_result = (id3077x_4);
  }
  HWRawBits<1> id2918out_result;

  { // Node ID: 2918 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2918in_a = id3077out_result;

    id2918out_result = (slice<13,1>(id2918in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2919out_output;

  { // Node ID: 2919 (NodeReinterpret)
    const HWRawBits<1> &id2919in_input = id2918out_result;

    id2919out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2919in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id806out_result;

  { // Node ID: 806 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id806in_a = id3267out_output[getCycle()%3];

    id806out_result = (not_fixed(id806in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id807out_result;

  { // Node ID: 807 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id807in_a = id2919out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id807in_b = id806out_result;

    HWOffsetFix<1,0,UNSIGNED> id807x_1;

    (id807x_1) = (and_fixed(id807in_a,id807in_b));
    id807out_result = (id807x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id808out_result;

  { // Node ID: 808 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id808in_a = id807out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id808in_b = id3269out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id808x_1;

    (id808x_1) = (or_fixed(id808in_a,id808in_b));
    id808out_result = (id808x_1);
  }
  { // Node ID: 3566 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2920out_result;

  { // Node ID: 2920 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2920in_a = id3077out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2920in_b = id3566out_value;

    id2920out_result = (gte_fixed(id2920in_a,id2920in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id815out_result;

  { // Node ID: 815 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id815in_a = id3269out_output[getCycle()%3];

    id815out_result = (not_fixed(id815in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id816out_result;

  { // Node ID: 816 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id816in_a = id2920out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id816in_b = id815out_result;

    HWOffsetFix<1,0,UNSIGNED> id816x_1;

    (id816x_1) = (and_fixed(id816in_a,id816in_b));
    id816out_result = (id816x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id817out_result;

  { // Node ID: 817 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id817in_a = id816out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id817in_b = id3267out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id817x_1;

    (id817x_1) = (or_fixed(id817in_a,id817in_b));
    id817out_result = (id817x_1);
  }
  HWRawBits<2> id818out_result;

  { // Node ID: 818 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id818in_in0 = id808out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id818in_in1 = id817out_result;

    id818out_result = (cat(id818in_in0,id818in_in1));
  }
  { // Node ID: 810 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id809out_o;

  { // Node ID: 809 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id809in_i = id3077out_result;

    id809out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id809in_i));
  }
  { // Node ID: 794 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id795out_result;

  { // Node ID: 795 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id795in_sel = id2917out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id795in_option0 = id791out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id795in_option1 = id794out_value;

    HWOffsetFix<45,-44,UNSIGNED> id795x_1;

    switch((id795in_sel.getValueAsLong())) {
      case 0l:
        id795x_1 = id795in_option0;
        break;
      case 1l:
        id795x_1 = id795in_option1;
        break;
      default:
        id795x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id795out_result = (id795x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id796out_o;

  { // Node ID: 796 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id796in_i = id795out_result;

    id796out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id796in_i));
  }
  HWRawBits<56> id811out_result;

  { // Node ID: 811 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id811in_in0 = id810out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id811in_in1 = id809out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id811in_in2 = id796out_o;

    id811out_result = (cat((cat(id811in_in0,id811in_in1)),id811in_in2));
  }
  HWFloat<11,45> id812out_output;

  { // Node ID: 812 (NodeReinterpret)
    const HWRawBits<56> &id812in_input = id811out_result;

    id812out_output = (cast_bits2float<11,45>(id812in_input));
  }
  { // Node ID: 819 (NodeConstantRawBits)
  }
  { // Node ID: 820 (NodeConstantRawBits)
  }
  { // Node ID: 822 (NodeConstantRawBits)
  }
  HWRawBits<56> id2921out_result;

  { // Node ID: 2921 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2921in_in0 = id819out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2921in_in1 = id820out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2921in_in2 = id822out_value;

    id2921out_result = (cat((cat(id2921in_in0,id2921in_in1)),id2921in_in2));
  }
  HWFloat<11,45> id824out_output;

  { // Node ID: 824 (NodeReinterpret)
    const HWRawBits<56> &id824in_input = id2921out_result;

    id824out_output = (cast_bits2float<11,45>(id824in_input));
  }
  { // Node ID: 2807 (NodeConstantRawBits)
  }
  HWFloat<11,45> id827out_result;

  { // Node ID: 827 (NodeMux)
    const HWRawBits<2> &id827in_sel = id818out_result;
    const HWFloat<11,45> &id827in_option0 = id812out_output;
    const HWFloat<11,45> &id827in_option1 = id824out_output;
    const HWFloat<11,45> &id827in_option2 = id2807out_value;
    const HWFloat<11,45> &id827in_option3 = id824out_output;

    HWFloat<11,45> id827x_1;

    switch((id827in_sel.getValueAsLong())) {
      case 0l:
        id827x_1 = id827in_option0;
        break;
      case 1l:
        id827x_1 = id827in_option1;
        break;
      case 2l:
        id827x_1 = id827in_option2;
        break;
      case 3l:
        id827x_1 = id827in_option3;
        break;
      default:
        id827x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id827out_result = (id827x_1);
  }
  { // Node ID: 3565 (NodeConstantRawBits)
  }
  HWFloat<11,45> id837out_result;

  { // Node ID: 837 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id837in_sel = id3272out_output[getCycle()%9];
    const HWFloat<11,45> &id837in_option0 = id827out_result;
    const HWFloat<11,45> &id837in_option1 = id3565out_value;

    HWFloat<11,45> id837x_1;

    switch((id837in_sel.getValueAsLong())) {
      case 0l:
        id837x_1 = id837in_option0;
        break;
      case 1l:
        id837x_1 = id837in_option1;
        break;
      default:
        id837x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id837out_result = (id837x_1);
  }
  { // Node ID: 3564 (NodeConstantRawBits)
  }
  HWFloat<11,45> id848out_result;

  { // Node ID: 848 (NodeAdd)
    const HWFloat<11,45> &id848in_a = id837out_result;
    const HWFloat<11,45> &id848in_b = id3564out_value;

    id848out_result = (add_float(id848in_a,id848in_b));
  }
  HWFloat<11,45> id850out_result;

  { // Node ID: 850 (NodeDiv)
    const HWFloat<11,45> &id850in_a = id3573out_value;
    const HWFloat<11,45> &id850in_b = id848out_result;

    id850out_result = (div_float(id850in_a,id850in_b));
  }
  HWFloat<11,45> id851out_result;

  { // Node ID: 851 (NodeMul)
    const HWFloat<11,45> &id851in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id851in_b = id850out_result;

    id851out_result = (mul_float(id851in_a,id851in_b));
  }
  HWFloat<11,45> id1969out_result;

  { // Node ID: 1969 (NodeMul)
    const HWFloat<11,45> &id1969in_a = id851out_result;
    const HWFloat<11,45> &id1969in_b = id3261out_output[getCycle()%10];

    id1969out_result = (mul_float(id1969in_a,id1969in_b));
  }
  HWFloat<11,45> id1970out_result;

  { // Node ID: 1970 (NodeSub)
    const HWFloat<11,45> &id1970in_a = id1968out_result;
    const HWFloat<11,45> &id1970in_b = id1969out_result;

    id1970out_result = (sub_float(id1970in_a,id1970in_b));
  }
  HWFloat<11,45> id1971out_result;

  { // Node ID: 1971 (NodeAdd)
    const HWFloat<11,45> &id1971in_a = id3261out_output[getCycle()%10];
    const HWFloat<11,45> &id1971in_b = id1970out_result;

    id1971out_result = (add_float(id1971in_a,id1971in_b));
  }
  HWFloat<11,45> id2691out_result;

  { // Node ID: 2691 (NodeMul)
    const HWFloat<11,45> &id2691in_a = id2690out_result;
    const HWFloat<11,45> &id2691in_b = id1971out_result;

    id2691out_result = (mul_float(id2691in_a,id2691in_b));
  }
  { // Node ID: 3562 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id899out_o;

  { // Node ID: 899 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id899in_i = id3275out_output[getCycle()%3];

    id899out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id899in_i));
  }
  { // Node ID: 902 (NodeConstantRawBits)
  }
  { // Node ID: 3042 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id881out_result;

  { // Node ID: 881 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id881in_a = id942out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id881in_b = id3276out_output[getCycle()%3];

    id881out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id881in_a,id881in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id882out_result;

  { // Node ID: 882 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id882in_a = id3276out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id882in_b = id942out_dout[getCycle()%3];

    id882out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id882in_a,id882in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id883out_result;

  { // Node ID: 883 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id883in_a = id881out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id883in_b = id882out_result;

    id883out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id883in_a,id883in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id884out_o;

  { // Node ID: 884 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id884in_i = id883out_result;

    id884out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id884in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id885out_result;

  { // Node ID: 885 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id885in_a = id945out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id885in_b = id884out_o;

    id885out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id885in_a,id885in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id886out_result;

  { // Node ID: 886 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id886in_a = id884out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id886in_b = id945out_dout[getCycle()%3];

    id886out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id886in_a,id886in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id887out_result;

  { // Node ID: 887 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id887in_a = id885out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id887in_b = id886out_result;

    id887out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id887in_a,id887in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id888out_o;

  { // Node ID: 888 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id888in_i = id887out_result;

    id888out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id888in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id889out_result;

  { // Node ID: 889 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id889in_a = id948out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id889in_b = id888out_o;

    id889out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id889in_a,id889in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id890out_result;

  { // Node ID: 890 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id890in_a = id888out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id890in_b = id948out_dout[getCycle()%3];

    id890out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id890in_a,id890in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id891out_result;

  { // Node ID: 891 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id891in_a = id889out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id891in_b = id890out_result;

    id891out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id891in_a,id891in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id892out_o;

  { // Node ID: 892 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id892in_i = id891out_result;

    id892out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id892in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id893out_o;

  { // Node ID: 893 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id893in_i = id892out_o;

    id893out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id893in_i));
  }
  { // Node ID: 3559 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2925out_result;

  { // Node ID: 2925 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2925in_a = id893out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2925in_b = id3559out_value;

    id2925out_result = (gte_fixed(id2925in_a,id2925in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3076out_result;

  { // Node ID: 3076 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3076in_a = id899out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3076in_b = id902out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3076in_c = id3042out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3076in_condb = id2925out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3076x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3076x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3076x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3076x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3076x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3076x_1 = id3076in_a;
        break;
      default:
        id3076x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3076in_condb.getValueAsLong())) {
      case 0l:
        id3076x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3076x_2 = id3076in_b;
        break;
      default:
        id3076x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3076x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3076x_3 = id3076in_c;
        break;
      default:
        id3076x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3076x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3076x_1),(id3076x_2))),(id3076x_3)));
    id3076out_result = (id3076x_4);
  }
  HWRawBits<1> id2926out_result;

  { // Node ID: 2926 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2926in_a = id3076out_result;

    id2926out_result = (slice<13,1>(id2926in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2927out_output;

  { // Node ID: 2927 (NodeReinterpret)
    const HWRawBits<1> &id2927in_input = id2926out_result;

    id2927out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2927in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id908out_result;

  { // Node ID: 908 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id908in_a = id3279out_output[getCycle()%3];

    id908out_result = (not_fixed(id908in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id909out_result;

  { // Node ID: 909 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id909in_a = id2927out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id909in_b = id908out_result;

    HWOffsetFix<1,0,UNSIGNED> id909x_1;

    (id909x_1) = (and_fixed(id909in_a,id909in_b));
    id909out_result = (id909x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id910out_result;

  { // Node ID: 910 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id910in_a = id909out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id910in_b = id3281out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id910x_1;

    (id910x_1) = (or_fixed(id910in_a,id910in_b));
    id910out_result = (id910x_1);
  }
  { // Node ID: 3556 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2928out_result;

  { // Node ID: 2928 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2928in_a = id3076out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2928in_b = id3556out_value;

    id2928out_result = (gte_fixed(id2928in_a,id2928in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id917out_result;

  { // Node ID: 917 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id917in_a = id3281out_output[getCycle()%3];

    id917out_result = (not_fixed(id917in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id918out_result;

  { // Node ID: 918 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id918in_a = id2928out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id918in_b = id917out_result;

    HWOffsetFix<1,0,UNSIGNED> id918x_1;

    (id918x_1) = (and_fixed(id918in_a,id918in_b));
    id918out_result = (id918x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id919out_result;

  { // Node ID: 919 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id919in_a = id918out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id919in_b = id3279out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id919x_1;

    (id919x_1) = (or_fixed(id919in_a,id919in_b));
    id919out_result = (id919x_1);
  }
  HWRawBits<2> id920out_result;

  { // Node ID: 920 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id920in_in0 = id910out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id920in_in1 = id919out_result;

    id920out_result = (cat(id920in_in0,id920in_in1));
  }
  { // Node ID: 912 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id911out_o;

  { // Node ID: 911 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id911in_i = id3076out_result;

    id911out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id911in_i));
  }
  { // Node ID: 896 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id897out_result;

  { // Node ID: 897 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id897in_sel = id2925out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id897in_option0 = id893out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id897in_option1 = id896out_value;

    HWOffsetFix<45,-44,UNSIGNED> id897x_1;

    switch((id897in_sel.getValueAsLong())) {
      case 0l:
        id897x_1 = id897in_option0;
        break;
      case 1l:
        id897x_1 = id897in_option1;
        break;
      default:
        id897x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id897out_result = (id897x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id898out_o;

  { // Node ID: 898 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id898in_i = id897out_result;

    id898out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id898in_i));
  }
  HWRawBits<56> id913out_result;

  { // Node ID: 913 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id913in_in0 = id912out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id913in_in1 = id911out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id913in_in2 = id898out_o;

    id913out_result = (cat((cat(id913in_in0,id913in_in1)),id913in_in2));
  }
  HWFloat<11,45> id914out_output;

  { // Node ID: 914 (NodeReinterpret)
    const HWRawBits<56> &id914in_input = id913out_result;

    id914out_output = (cast_bits2float<11,45>(id914in_input));
  }
  { // Node ID: 921 (NodeConstantRawBits)
  }
  { // Node ID: 922 (NodeConstantRawBits)
  }
  { // Node ID: 924 (NodeConstantRawBits)
  }
  HWRawBits<56> id2929out_result;

  { // Node ID: 2929 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2929in_in0 = id921out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2929in_in1 = id922out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2929in_in2 = id924out_value;

    id2929out_result = (cat((cat(id2929in_in0,id2929in_in1)),id2929in_in2));
  }
  HWFloat<11,45> id926out_output;

  { // Node ID: 926 (NodeReinterpret)
    const HWRawBits<56> &id926in_input = id2929out_result;

    id926out_output = (cast_bits2float<11,45>(id926in_input));
  }
  { // Node ID: 2808 (NodeConstantRawBits)
  }
  HWFloat<11,45> id929out_result;

  { // Node ID: 929 (NodeMux)
    const HWRawBits<2> &id929in_sel = id920out_result;
    const HWFloat<11,45> &id929in_option0 = id914out_output;
    const HWFloat<11,45> &id929in_option1 = id926out_output;
    const HWFloat<11,45> &id929in_option2 = id2808out_value;
    const HWFloat<11,45> &id929in_option3 = id926out_output;

    HWFloat<11,45> id929x_1;

    switch((id929in_sel.getValueAsLong())) {
      case 0l:
        id929x_1 = id929in_option0;
        break;
      case 1l:
        id929x_1 = id929in_option1;
        break;
      case 2l:
        id929x_1 = id929in_option2;
        break;
      case 3l:
        id929x_1 = id929in_option3;
        break;
      default:
        id929x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id929out_result = (id929x_1);
  }
  { // Node ID: 3555 (NodeConstantRawBits)
  }
  HWFloat<11,45> id939out_result;

  { // Node ID: 939 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id939in_sel = id3284out_output[getCycle()%9];
    const HWFloat<11,45> &id939in_option0 = id929out_result;
    const HWFloat<11,45> &id939in_option1 = id3555out_value;

    HWFloat<11,45> id939x_1;

    switch((id939in_sel.getValueAsLong())) {
      case 0l:
        id939x_1 = id939in_option0;
        break;
      case 1l:
        id939x_1 = id939in_option1;
        break;
      default:
        id939x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id939out_result = (id939x_1);
  }
  HWFloat<11,45> id950out_result;

  { // Node ID: 950 (NodeMul)
    const HWFloat<11,45> &id950in_a = id3562out_value;
    const HWFloat<11,45> &id950in_b = id939out_result;

    id950out_result = (mul_float(id950in_a,id950in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id998out_o;

  { // Node ID: 998 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id998in_i = id3286out_output[getCycle()%3];

    id998out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id998in_i));
  }
  { // Node ID: 1001 (NodeConstantRawBits)
  }
  { // Node ID: 3044 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id980out_result;

  { // Node ID: 980 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id980in_a = id1041out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id980in_b = id3287out_output[getCycle()%3];

    id980out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id980in_a,id980in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id981out_result;

  { // Node ID: 981 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id981in_a = id3287out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id981in_b = id1041out_dout[getCycle()%3];

    id981out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id981in_a,id981in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id982out_result;

  { // Node ID: 982 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id982in_a = id980out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id982in_b = id981out_result;

    id982out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id982in_a,id982in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id983out_o;

  { // Node ID: 983 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id983in_i = id982out_result;

    id983out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id983in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id984out_result;

  { // Node ID: 984 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id984in_a = id1044out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id984in_b = id983out_o;

    id984out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id984in_a,id984in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id985out_result;

  { // Node ID: 985 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id985in_a = id983out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id985in_b = id1044out_dout[getCycle()%3];

    id985out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id985in_a,id985in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id986out_result;

  { // Node ID: 986 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id986in_a = id984out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id986in_b = id985out_result;

    id986out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id986in_a,id986in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id987out_o;

  { // Node ID: 987 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id987in_i = id986out_result;

    id987out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id987in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id988out_result;

  { // Node ID: 988 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id988in_a = id1047out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id988in_b = id987out_o;

    id988out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id988in_a,id988in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id989out_result;

  { // Node ID: 989 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id989in_a = id987out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id989in_b = id1047out_dout[getCycle()%3];

    id989out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id989in_a,id989in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id990out_result;

  { // Node ID: 990 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id990in_a = id988out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id990in_b = id989out_result;

    id990out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id990in_a,id990in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id991out_o;

  { // Node ID: 991 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id991in_i = id990out_result;

    id991out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id991in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id992out_o;

  { // Node ID: 992 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id992in_i = id991out_o;

    id992out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id992in_i));
  }
  { // Node ID: 3551 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2932out_result;

  { // Node ID: 2932 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2932in_a = id992out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2932in_b = id3551out_value;

    id2932out_result = (gte_fixed(id2932in_a,id2932in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3075out_result;

  { // Node ID: 3075 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3075in_a = id998out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3075in_b = id1001out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3075in_c = id3044out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3075in_condb = id2932out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3075x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3075x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3075x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3075x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3075x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3075x_1 = id3075in_a;
        break;
      default:
        id3075x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3075in_condb.getValueAsLong())) {
      case 0l:
        id3075x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3075x_2 = id3075in_b;
        break;
      default:
        id3075x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3075x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3075x_3 = id3075in_c;
        break;
      default:
        id3075x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3075x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3075x_1),(id3075x_2))),(id3075x_3)));
    id3075out_result = (id3075x_4);
  }
  HWRawBits<1> id2933out_result;

  { // Node ID: 2933 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2933in_a = id3075out_result;

    id2933out_result = (slice<13,1>(id2933in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2934out_output;

  { // Node ID: 2934 (NodeReinterpret)
    const HWRawBits<1> &id2934in_input = id2933out_result;

    id2934out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2934in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1007out_result;

  { // Node ID: 1007 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1007in_a = id3290out_output[getCycle()%3];

    id1007out_result = (not_fixed(id1007in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1008out_result;

  { // Node ID: 1008 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1008in_a = id2934out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1008in_b = id1007out_result;

    HWOffsetFix<1,0,UNSIGNED> id1008x_1;

    (id1008x_1) = (and_fixed(id1008in_a,id1008in_b));
    id1008out_result = (id1008x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1009out_result;

  { // Node ID: 1009 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1009in_a = id1008out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1009in_b = id3292out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1009x_1;

    (id1009x_1) = (or_fixed(id1009in_a,id1009in_b));
    id1009out_result = (id1009x_1);
  }
  { // Node ID: 3548 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2935out_result;

  { // Node ID: 2935 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2935in_a = id3075out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2935in_b = id3548out_value;

    id2935out_result = (gte_fixed(id2935in_a,id2935in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1016out_result;

  { // Node ID: 1016 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1016in_a = id3292out_output[getCycle()%3];

    id1016out_result = (not_fixed(id1016in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1017out_result;

  { // Node ID: 1017 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1017in_a = id2935out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1017in_b = id1016out_result;

    HWOffsetFix<1,0,UNSIGNED> id1017x_1;

    (id1017x_1) = (and_fixed(id1017in_a,id1017in_b));
    id1017out_result = (id1017x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1018out_result;

  { // Node ID: 1018 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1018in_a = id1017out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1018in_b = id3290out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1018x_1;

    (id1018x_1) = (or_fixed(id1018in_a,id1018in_b));
    id1018out_result = (id1018x_1);
  }
  HWRawBits<2> id1019out_result;

  { // Node ID: 1019 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1019in_in0 = id1009out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1019in_in1 = id1018out_result;

    id1019out_result = (cat(id1019in_in0,id1019in_in1));
  }
  { // Node ID: 1011 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1010out_o;

  { // Node ID: 1010 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1010in_i = id3075out_result;

    id1010out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1010in_i));
  }
  { // Node ID: 995 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id996out_result;

  { // Node ID: 996 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id996in_sel = id2932out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id996in_option0 = id992out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id996in_option1 = id995out_value;

    HWOffsetFix<45,-44,UNSIGNED> id996x_1;

    switch((id996in_sel.getValueAsLong())) {
      case 0l:
        id996x_1 = id996in_option0;
        break;
      case 1l:
        id996x_1 = id996in_option1;
        break;
      default:
        id996x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id996out_result = (id996x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id997out_o;

  { // Node ID: 997 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id997in_i = id996out_result;

    id997out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id997in_i));
  }
  HWRawBits<56> id1012out_result;

  { // Node ID: 1012 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1012in_in0 = id1011out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1012in_in1 = id1010out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1012in_in2 = id997out_o;

    id1012out_result = (cat((cat(id1012in_in0,id1012in_in1)),id1012in_in2));
  }
  HWFloat<11,45> id1013out_output;

  { // Node ID: 1013 (NodeReinterpret)
    const HWRawBits<56> &id1013in_input = id1012out_result;

    id1013out_output = (cast_bits2float<11,45>(id1013in_input));
  }
  { // Node ID: 1020 (NodeConstantRawBits)
  }
  { // Node ID: 1021 (NodeConstantRawBits)
  }
  { // Node ID: 1023 (NodeConstantRawBits)
  }
  HWRawBits<56> id2936out_result;

  { // Node ID: 2936 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2936in_in0 = id1020out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2936in_in1 = id1021out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2936in_in2 = id1023out_value;

    id2936out_result = (cat((cat(id2936in_in0,id2936in_in1)),id2936in_in2));
  }
  HWFloat<11,45> id1025out_output;

  { // Node ID: 1025 (NodeReinterpret)
    const HWRawBits<56> &id1025in_input = id2936out_result;

    id1025out_output = (cast_bits2float<11,45>(id1025in_input));
  }
  { // Node ID: 2809 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1028out_result;

  { // Node ID: 1028 (NodeMux)
    const HWRawBits<2> &id1028in_sel = id1019out_result;
    const HWFloat<11,45> &id1028in_option0 = id1013out_output;
    const HWFloat<11,45> &id1028in_option1 = id1025out_output;
    const HWFloat<11,45> &id1028in_option2 = id2809out_value;
    const HWFloat<11,45> &id1028in_option3 = id1025out_output;

    HWFloat<11,45> id1028x_1;

    switch((id1028in_sel.getValueAsLong())) {
      case 0l:
        id1028x_1 = id1028in_option0;
        break;
      case 1l:
        id1028x_1 = id1028in_option1;
        break;
      case 2l:
        id1028x_1 = id1028in_option2;
        break;
      case 3l:
        id1028x_1 = id1028in_option3;
        break;
      default:
        id1028x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1028out_result = (id1028x_1);
  }
  { // Node ID: 3547 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1038out_result;

  { // Node ID: 1038 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1038in_sel = id3295out_output[getCycle()%9];
    const HWFloat<11,45> &id1038in_option0 = id1028out_result;
    const HWFloat<11,45> &id1038in_option1 = id3547out_value;

    HWFloat<11,45> id1038x_1;

    switch((id1038in_sel.getValueAsLong())) {
      case 0l:
        id1038x_1 = id1038in_option0;
        break;
      case 1l:
        id1038x_1 = id1038in_option1;
        break;
      default:
        id1038x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1038out_result = (id1038x_1);
  }
  { // Node ID: 3546 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1049out_result;

  { // Node ID: 1049 (NodeAdd)
    const HWFloat<11,45> &id1049in_a = id1038out_result;
    const HWFloat<11,45> &id1049in_b = id3546out_value;

    id1049out_result = (add_float(id1049in_a,id1049in_b));
  }
  HWFloat<11,45> id1050out_result;

  { // Node ID: 1050 (NodeDiv)
    const HWFloat<11,45> &id1050in_a = id950out_result;
    const HWFloat<11,45> &id1050in_b = id1049out_result;

    id1050out_result = (div_float(id1050in_a,id1050in_b));
  }
  HWFloat<11,45> id1051out_result;

  { // Node ID: 1051 (NodeMul)
    const HWFloat<11,45> &id1051in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1051in_b = id1050out_result;

    id1051out_result = (mul_float(id1051in_a,id1051in_b));
  }
  { // Node ID: 3545 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1973out_result;

  { // Node ID: 1973 (NodeSub)
    const HWFloat<11,45> &id1973in_a = id3545out_value;
    const HWFloat<11,45> &id1973in_b = id3296out_output[getCycle()%10];

    id1973out_result = (sub_float(id1973in_a,id1973in_b));
  }
  HWFloat<11,45> id1974out_result;

  { // Node ID: 1974 (NodeMul)
    const HWFloat<11,45> &id1974in_a = id1051out_result;
    const HWFloat<11,45> &id1974in_b = id1973out_result;

    id1974out_result = (mul_float(id1974in_a,id1974in_b));
  }
  { // Node ID: 3544 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1099out_o;

  { // Node ID: 1099 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1099in_i = id3298out_output[getCycle()%3];

    id1099out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1099in_i));
  }
  { // Node ID: 1102 (NodeConstantRawBits)
  }
  { // Node ID: 3046 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id1081out_result;

  { // Node ID: 1081 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1081in_a = id1142out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1081in_b = id3299out_output[getCycle()%3];

    id1081out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1081in_a,id1081in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1082out_result;

  { // Node ID: 1082 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1082in_a = id3299out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1082in_b = id1142out_dout[getCycle()%3];

    id1082out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1082in_a,id1082in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1083out_result;

  { // Node ID: 1083 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1083in_a = id1081out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1083in_b = id1082out_result;

    id1083out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1083in_a,id1083in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1084out_o;

  { // Node ID: 1084 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1084in_i = id1083out_result;

    id1084out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1084in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1085out_result;

  { // Node ID: 1085 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1085in_a = id1145out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1085in_b = id1084out_o;

    id1085out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1085in_a,id1085in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1086out_result;

  { // Node ID: 1086 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1086in_a = id1084out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1086in_b = id1145out_dout[getCycle()%3];

    id1086out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1086in_a,id1086in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1087out_result;

  { // Node ID: 1087 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1087in_a = id1085out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1087in_b = id1086out_result;

    id1087out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1087in_a,id1087in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1088out_o;

  { // Node ID: 1088 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1088in_i = id1087out_result;

    id1088out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1088in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1089out_result;

  { // Node ID: 1089 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1089in_a = id1148out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1089in_b = id1088out_o;

    id1089out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1089in_a,id1089in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1090out_result;

  { // Node ID: 1090 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1090in_a = id1088out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1090in_b = id1148out_dout[getCycle()%3];

    id1090out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1090in_a,id1090in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1091out_result;

  { // Node ID: 1091 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1091in_a = id1089out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1091in_b = id1090out_result;

    id1091out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1091in_a,id1091in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1092out_o;

  { // Node ID: 1092 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1092in_i = id1091out_result;

    id1092out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1092in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1093out_o;

  { // Node ID: 1093 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1093in_i = id1092out_o;

    id1093out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1093in_i));
  }
  { // Node ID: 3540 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2939out_result;

  { // Node ID: 2939 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2939in_a = id1093out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2939in_b = id3540out_value;

    id2939out_result = (gte_fixed(id2939in_a,id2939in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3074out_result;

  { // Node ID: 3074 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3074in_a = id1099out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3074in_b = id1102out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3074in_c = id3046out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3074in_condb = id2939out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3074x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3074x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3074x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3074x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3074x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3074x_1 = id3074in_a;
        break;
      default:
        id3074x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3074in_condb.getValueAsLong())) {
      case 0l:
        id3074x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3074x_2 = id3074in_b;
        break;
      default:
        id3074x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3074x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3074x_3 = id3074in_c;
        break;
      default:
        id3074x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3074x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3074x_1),(id3074x_2))),(id3074x_3)));
    id3074out_result = (id3074x_4);
  }
  HWRawBits<1> id2940out_result;

  { // Node ID: 2940 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2940in_a = id3074out_result;

    id2940out_result = (slice<13,1>(id2940in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2941out_output;

  { // Node ID: 2941 (NodeReinterpret)
    const HWRawBits<1> &id2941in_input = id2940out_result;

    id2941out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2941in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id1108out_result;

  { // Node ID: 1108 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1108in_a = id3302out_output[getCycle()%3];

    id1108out_result = (not_fixed(id1108in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1109out_result;

  { // Node ID: 1109 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1109in_a = id2941out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1109in_b = id1108out_result;

    HWOffsetFix<1,0,UNSIGNED> id1109x_1;

    (id1109x_1) = (and_fixed(id1109in_a,id1109in_b));
    id1109out_result = (id1109x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1110out_result;

  { // Node ID: 1110 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1110in_a = id1109out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1110in_b = id3304out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1110x_1;

    (id1110x_1) = (or_fixed(id1110in_a,id1110in_b));
    id1110out_result = (id1110x_1);
  }
  { // Node ID: 3537 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2942out_result;

  { // Node ID: 2942 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2942in_a = id3074out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2942in_b = id3537out_value;

    id2942out_result = (gte_fixed(id2942in_a,id2942in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1117out_result;

  { // Node ID: 1117 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1117in_a = id3304out_output[getCycle()%3];

    id1117out_result = (not_fixed(id1117in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1118out_result;

  { // Node ID: 1118 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_a = id2942out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1118in_b = id1117out_result;

    HWOffsetFix<1,0,UNSIGNED> id1118x_1;

    (id1118x_1) = (and_fixed(id1118in_a,id1118in_b));
    id1118out_result = (id1118x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1119out_result;

  { // Node ID: 1119 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_a = id1118out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1119in_b = id3302out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1119x_1;

    (id1119x_1) = (or_fixed(id1119in_a,id1119in_b));
    id1119out_result = (id1119x_1);
  }
  HWRawBits<2> id1120out_result;

  { // Node ID: 1120 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1120in_in0 = id1110out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1120in_in1 = id1119out_result;

    id1120out_result = (cat(id1120in_in0,id1120in_in1));
  }
  { // Node ID: 1112 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1111out_o;

  { // Node ID: 1111 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1111in_i = id3074out_result;

    id1111out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1111in_i));
  }
  { // Node ID: 1096 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1097out_result;

  { // Node ID: 1097 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1097in_sel = id2939out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1097in_option0 = id1093out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1097in_option1 = id1096out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1097x_1;

    switch((id1097in_sel.getValueAsLong())) {
      case 0l:
        id1097x_1 = id1097in_option0;
        break;
      case 1l:
        id1097x_1 = id1097in_option1;
        break;
      default:
        id1097x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1097out_result = (id1097x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1098out_o;

  { // Node ID: 1098 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1098in_i = id1097out_result;

    id1098out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1098in_i));
  }
  HWRawBits<56> id1113out_result;

  { // Node ID: 1113 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1113in_in0 = id1112out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1113in_in1 = id1111out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1113in_in2 = id1098out_o;

    id1113out_result = (cat((cat(id1113in_in0,id1113in_in1)),id1113in_in2));
  }
  HWFloat<11,45> id1114out_output;

  { // Node ID: 1114 (NodeReinterpret)
    const HWRawBits<56> &id1114in_input = id1113out_result;

    id1114out_output = (cast_bits2float<11,45>(id1114in_input));
  }
  { // Node ID: 1121 (NodeConstantRawBits)
  }
  { // Node ID: 1122 (NodeConstantRawBits)
  }
  { // Node ID: 1124 (NodeConstantRawBits)
  }
  HWRawBits<56> id2943out_result;

  { // Node ID: 2943 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2943in_in0 = id1121out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2943in_in1 = id1122out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2943in_in2 = id1124out_value;

    id2943out_result = (cat((cat(id2943in_in0,id2943in_in1)),id2943in_in2));
  }
  HWFloat<11,45> id1126out_output;

  { // Node ID: 1126 (NodeReinterpret)
    const HWRawBits<56> &id1126in_input = id2943out_result;

    id1126out_output = (cast_bits2float<11,45>(id1126in_input));
  }
  { // Node ID: 2810 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1129out_result;

  { // Node ID: 1129 (NodeMux)
    const HWRawBits<2> &id1129in_sel = id1120out_result;
    const HWFloat<11,45> &id1129in_option0 = id1114out_output;
    const HWFloat<11,45> &id1129in_option1 = id1126out_output;
    const HWFloat<11,45> &id1129in_option2 = id2810out_value;
    const HWFloat<11,45> &id1129in_option3 = id1126out_output;

    HWFloat<11,45> id1129x_1;

    switch((id1129in_sel.getValueAsLong())) {
      case 0l:
        id1129x_1 = id1129in_option0;
        break;
      case 1l:
        id1129x_1 = id1129in_option1;
        break;
      case 2l:
        id1129x_1 = id1129in_option2;
        break;
      case 3l:
        id1129x_1 = id1129in_option3;
        break;
      default:
        id1129x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1129out_result = (id1129x_1);
  }
  { // Node ID: 3536 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1139out_result;

  { // Node ID: 1139 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1139in_sel = id3307out_output[getCycle()%9];
    const HWFloat<11,45> &id1139in_option0 = id1129out_result;
    const HWFloat<11,45> &id1139in_option1 = id3536out_value;

    HWFloat<11,45> id1139x_1;

    switch((id1139in_sel.getValueAsLong())) {
      case 0l:
        id1139x_1 = id1139in_option0;
        break;
      case 1l:
        id1139x_1 = id1139in_option1;
        break;
      default:
        id1139x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1139out_result = (id1139x_1);
  }
  { // Node ID: 3535 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1150out_result;

  { // Node ID: 1150 (NodeAdd)
    const HWFloat<11,45> &id1150in_a = id1139out_result;
    const HWFloat<11,45> &id1150in_b = id3535out_value;

    id1150out_result = (add_float(id1150in_a,id1150in_b));
  }
  HWFloat<11,45> id1152out_result;

  { // Node ID: 1152 (NodeDiv)
    const HWFloat<11,45> &id1152in_a = id3544out_value;
    const HWFloat<11,45> &id1152in_b = id1150out_result;

    id1152out_result = (div_float(id1152in_a,id1152in_b));
  }
  HWFloat<11,45> id1153out_result;

  { // Node ID: 1153 (NodeMul)
    const HWFloat<11,45> &id1153in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1153in_b = id1152out_result;

    id1153out_result = (mul_float(id1153in_a,id1153in_b));
  }
  HWFloat<11,45> id1975out_result;

  { // Node ID: 1975 (NodeMul)
    const HWFloat<11,45> &id1975in_a = id1153out_result;
    const HWFloat<11,45> &id1975in_b = id3296out_output[getCycle()%10];

    id1975out_result = (mul_float(id1975in_a,id1975in_b));
  }
  HWFloat<11,45> id1976out_result;

  { // Node ID: 1976 (NodeSub)
    const HWFloat<11,45> &id1976in_a = id1974out_result;
    const HWFloat<11,45> &id1976in_b = id1975out_result;

    id1976out_result = (sub_float(id1976in_a,id1976in_b));
  }
  HWFloat<11,45> id1977out_result;

  { // Node ID: 1977 (NodeAdd)
    const HWFloat<11,45> &id1977in_a = id3296out_output[getCycle()%10];
    const HWFloat<11,45> &id1977in_b = id1976out_result;

    id1977out_result = (add_float(id1977in_a,id1977in_b));
  }
  HWFloat<11,45> id2692out_result;

  { // Node ID: 2692 (NodeMul)
    const HWFloat<11,45> &id2692in_a = id2691out_result;
    const HWFloat<11,45> &id2692in_b = id1977out_result;

    id2692out_result = (mul_float(id2692in_a,id2692in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2693out_result;

  { // Node ID: 2693 (NodeAdd)
    const HWFloat<11,45> &id2693in_a = id2692out_result;
    const HWFloat<11,45> &id2693in_b = id3out_value;

    id2693out_result = (add_float(id2693in_a,id2693in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2694out_result;

  { // Node ID: 2694 (NodeSub)
    const HWFloat<11,45> &id2694in_a = id3432out_output[getCycle()%2];
    const HWFloat<11,45> &id2694in_b = id2out_value;

    id2694out_result = (sub_float(id2694in_a,id2694in_b));
  }
  HWFloat<11,45> id2695out_result;

  { // Node ID: 2695 (NodeMul)
    const HWFloat<11,45> &id2695in_a = id2693out_result;
    const HWFloat<11,45> &id2695in_b = id2694out_result;

    id2695out_result = (mul_float(id2695in_a,id2695in_b));
  }
  HWFloat<11,45> id2697out_result;

  { // Node ID: 2697 (NodeAdd)
    const HWFloat<11,45> &id2697in_a = id2696out_result;
    const HWFloat<11,45> &id2697in_b = id2695out_result;

    id2697out_result = (add_float(id2697in_a,id2697in_b));
  }
  { // Node ID: 3534 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2065out_result;

  { // Node ID: 2065 (NodeMul)
    const HWFloat<11,45> &id2065in_a = id3534out_value;
    const HWFloat<11,45> &id2065in_b = id3311out_output[getCycle()%2];

    id2065out_result = (mul_float(id2065in_a,id2065in_b));
  }
  HWFloat<11,45> id2066out_result;

  { // Node ID: 2066 (NodeMul)
    const HWFloat<11,45> &id2066in_a = id2065out_result;
    const HWFloat<11,45> &id2066in_b = id3356out_output[getCycle()%2];

    id2066out_result = (mul_float(id2066in_a,id2066in_b));
  }
  { // Node ID: 3457 (NodeConstantRawBits)
  }
  { // Node ID: 3456 (NodeConstantRawBits)
  }
  { // Node ID: 3451 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2052out_result;

  { // Node ID: 2052 (NodeLt)
    const HWFloat<11,45> &id2052in_a = id3433out_output[getCycle()%3];
    const HWFloat<11,45> &id2052in_b = id3451out_value;

    id2052out_result = (lt_float(id2052in_a,id2052in_b));
  }
  HWRawBits<11> id2033out_result;

  { // Node ID: 2033 (NodeSlice)
    const HWFloat<11,45> &id2033in_a = id3433out_output[getCycle()%3];

    id2033out_result = (slice<44,11>(id2033in_a));
  }
  { // Node ID: 2034 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id3003out_result;

  { // Node ID: 3003 (NodeEqInlined)
    const HWRawBits<11> &id3003in_a = id2033out_result;
    const HWRawBits<11> &id3003in_b = id2034out_value;

    id3003out_result = (eq_bits(id3003in_a,id3003in_b));
  }
  HWRawBits<44> id2032out_result;

  { // Node ID: 2032 (NodeSlice)
    const HWFloat<11,45> &id2032in_a = id3433out_output[getCycle()%3];

    id2032out_result = (slice<0,44>(id2032in_a));
  }
  { // Node ID: 3450 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id3004out_result;

  { // Node ID: 3004 (NodeNeqInlined)
    const HWRawBits<44> &id3004in_a = id2032out_result;
    const HWRawBits<44> &id3004in_b = id3450out_value;

    id3004out_result = (neq_bits(id3004in_a,id3004in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2038out_result;

  { // Node ID: 2038 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2038in_a = id3003out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2038in_b = id3004out_result;

    HWOffsetFix<1,0,UNSIGNED> id2038x_1;

    (id2038x_1) = (and_fixed(id2038in_a,id2038in_b));
    id2038out_result = (id2038x_1);
  }
  { // Node ID: 3449 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2040out_result;

  { // Node ID: 2040 (NodeLt)
    const HWFloat<11,45> &id2040in_a = id3433out_output[getCycle()%3];
    const HWFloat<11,45> &id2040in_b = id3449out_value;

    id2040out_result = (lt_float(id2040in_a,id2040in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2041out_result;

  { // Node ID: 2041 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2041in_a = id2038out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2041in_b = id2040out_result;

    HWOffsetFix<1,0,UNSIGNED> id2041x_1;

    (id2041x_1) = (or_fixed(id2041in_a,id2041in_b));
    id2041out_result = (id2041x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2053out_result;

  { // Node ID: 2053 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2053in_a = id2041out_result;

    id2053out_result = (not_fixed(id2053in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2054out_result;

  { // Node ID: 2054 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2054in_a = id2052out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2054in_b = id2053out_result;

    HWOffsetFix<1,0,UNSIGNED> id2054x_1;

    (id2054x_1) = (and_fixed(id2054in_a,id2054in_b));
    id2054out_result = (id2054x_1);
  }
  HWRawBits<55> id2045out_result;

  { // Node ID: 2045 (NodeSlice)
    const HWFloat<11,45> &id2045in_a = id3433out_output[getCycle()%3];

    id2045out_result = (slice<0,55>(id2045in_a));
  }
  { // Node ID: 2044 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id3005out_result;

  { // Node ID: 3005 (NodeEqInlined)
    const HWRawBits<55> &id3005in_a = id2045out_result;
    const HWRawBits<55> &id3005in_b = id2044out_value;

    id3005out_result = (eq_bits(id3005in_a,id3005in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2047out_result;

  { // Node ID: 2047 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2047in_a = id2041out_result;

    id2047out_result = (not_fixed(id2047in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2048out_result;

  { // Node ID: 2048 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2048in_a = id3005out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2048in_b = id2047out_result;

    HWOffsetFix<1,0,UNSIGNED> id2048x_1;

    (id2048x_1) = (and_fixed(id2048in_a,id2048in_b));
    id2048out_result = (id2048x_1);
  }
  { // Node ID: 2005 (NodeConstantRawBits)
  }
  HWRawBits<11> id2004out_result;

  { // Node ID: 2004 (NodeSlice)
    const HWFloat<11,45> &id2004in_a = id3433out_output[getCycle()%3];

    id2004out_result = (slice<44,11>(id2004in_a));
  }
  HWRawBits<12> id2006out_result;

  { // Node ID: 2006 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2006in_in0 = id2005out_value;
    const HWRawBits<11> &id2006in_in1 = id2004out_result;

    id2006out_result = (cat(id2006in_in0,id2006in_in1));
  }
  HWOffsetFix<12,0,TWOSCOMPLEMENT> id2007out_output;

  { // Node ID: 2007 (NodeReinterpret)
    const HWRawBits<12> &id2007in_input = id2006out_result;

    id2007out_output = (cast_bits2fixed<12,0,TWOSCOMPLEMENT>(id2007in_input));
  }
  { // Node ID: 3448 (NodeConstantRawBits)
  }
  HWOffsetFix<12,0,TWOSCOMPLEMENT> id2009out_result;

  { // Node ID: 2009 (NodeSub)
    const HWOffsetFix<12,0,TWOSCOMPLEMENT> &id2009in_a = id2007out_output;
    const HWOffsetFix<12,0,TWOSCOMPLEMENT> &id2009in_b = id3448out_value;

    id2009out_result = (sub_fixed<12,0,TWOSCOMPLEMENT,TONEAREVEN>(id2009in_a,id2009in_b));
  }
  { // Node ID: 2010 (NodeConstantRawBits)
  }
  HWOffsetFix<12,0,TWOSCOMPLEMENT> id3065out_result;

  { // Node ID: 3065 (NodeCondAdd)
    const HWOffsetFix<12,0,TWOSCOMPLEMENT> &id3065in_a = id2009out_result;
    const HWOffsetFix<12,0,TWOSCOMPLEMENT> &id3065in_b = id2010out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3065in_condb = id3410out_output[getCycle()%3];

    HWOffsetFix<12,0,TWOSCOMPLEMENT> id3065x_1;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id3065x_2;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id3065x_3;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3065x_1 = (c_hw_fix_12_0_sgn_bits_2);
        break;
      case 1l:
        id3065x_1 = id3065in_a;
        break;
      default:
        id3065x_1 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    switch((id3065in_condb.getValueAsLong())) {
      case 0l:
        id3065x_2 = (c_hw_fix_12_0_sgn_bits_2);
        break;
      case 1l:
        id3065x_2 = id3065in_b;
        break;
      default:
        id3065x_2 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    (id3065x_3) = (add_fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>((id3065x_1),(id3065x_2)));
    id3065out_result = (id3065x_3);
  }
  HWRawBits<45> id2780out_result;

  { // Node ID: 2780 (NodeSlice)
    const HWRawBits<180> &id2780in_a = id2773out_dout[getCycle()%3];

    id2780out_result = (slice<135,45>(id2780in_a));
  }
  HWOffsetFix<45,-78,TWOSCOMPLEMENT> id2781out_output;

  { // Node ID: 2781 (NodeReinterpret)
    const HWRawBits<45> &id2781in_input = id2780out_result;

    id2781out_output = (cast_bits2fixed<45,-78,TWOSCOMPLEMENT>(id2781in_input));
  }
  HWOffsetFix<78,-111,TWOSCOMPLEMENT> id2019out_result;

  { // Node ID: 2019 (NodeMul)
    const HWOffsetFix<45,-78,TWOSCOMPLEMENT> &id2019in_a = id2781out_output;
    const HWOffsetFix<33,-33,UNSIGNED> &id2019in_b = id3411out_output[getCycle()%3];

    id2019out_result = (mul_fixed<78,-111,TWOSCOMPLEMENT,TONEAREVEN>(id2019in_a,id2019in_b));
  }
  HWRawBits<45> id2778out_result;

  { // Node ID: 2778 (NodeSlice)
    const HWRawBits<180> &id2778in_a = id2773out_dout[getCycle()%3];

    id2778out_result = (slice<90,45>(id2778in_a));
  }
  HWOffsetFix<45,-66,TWOSCOMPLEMENT> id2779out_output;

  { // Node ID: 2779 (NodeReinterpret)
    const HWRawBits<45> &id2779in_input = id2778out_result;

    id2779out_output = (cast_bits2fixed<45,-66,TWOSCOMPLEMENT>(id2779in_input));
  }
  HWOffsetFix<90,-111,TWOSCOMPLEMENT> id2020out_result;

  { // Node ID: 2020 (NodeAdd)
    const HWOffsetFix<78,-111,TWOSCOMPLEMENT> &id2020in_a = id2019out_result;
    const HWOffsetFix<45,-66,TWOSCOMPLEMENT> &id2020in_b = id2779out_output;

    id2020out_result = (add_fixed<90,-111,TWOSCOMPLEMENT,TONEAREVEN>(id2020in_a,id2020in_b));
  }
  HWOffsetFix<45,-66,TWOSCOMPLEMENT> id2021out_o;

  { // Node ID: 2021 (NodeCast)
    const HWOffsetFix<90,-111,TWOSCOMPLEMENT> &id2021in_i = id2020out_result;

    id2021out_o = (cast_fixed2fixed<45,-66,TWOSCOMPLEMENT,TONEAREVEN>(id2021in_i));
  }
  HWOffsetFix<78,-99,TWOSCOMPLEMENT> id2022out_result;

  { // Node ID: 2022 (NodeMul)
    const HWOffsetFix<45,-66,TWOSCOMPLEMENT> &id2022in_a = id2021out_o;
    const HWOffsetFix<33,-33,UNSIGNED> &id2022in_b = id3411out_output[getCycle()%3];

    id2022out_result = (mul_fixed<78,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2022in_a,id2022in_b));
  }
  HWRawBits<45> id2776out_result;

  { // Node ID: 2776 (NodeSlice)
    const HWRawBits<180> &id2776in_a = id2773out_dout[getCycle()%3];

    id2776out_result = (slice<45,45>(id2776in_a));
  }
  HWOffsetFix<45,-54,TWOSCOMPLEMENT> id2777out_output;

  { // Node ID: 2777 (NodeReinterpret)
    const HWRawBits<45> &id2777in_input = id2776out_result;

    id2777out_output = (cast_bits2fixed<45,-54,TWOSCOMPLEMENT>(id2777in_input));
  }
  HWOffsetFix<90,-99,TWOSCOMPLEMENT> id2023out_result;

  { // Node ID: 2023 (NodeAdd)
    const HWOffsetFix<78,-99,TWOSCOMPLEMENT> &id2023in_a = id2022out_result;
    const HWOffsetFix<45,-54,TWOSCOMPLEMENT> &id2023in_b = id2777out_output;

    id2023out_result = (add_fixed<90,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2023in_a,id2023in_b));
  }
  HWOffsetFix<45,-54,TWOSCOMPLEMENT> id2024out_o;

  { // Node ID: 2024 (NodeCast)
    const HWOffsetFix<90,-99,TWOSCOMPLEMENT> &id2024in_i = id2023out_result;

    id2024out_o = (cast_fixed2fixed<45,-54,TWOSCOMPLEMENT,TONEAREVEN>(id2024in_i));
  }
  HWOffsetFix<78,-87,TWOSCOMPLEMENT> id2025out_result;

  { // Node ID: 2025 (NodeMul)
    const HWOffsetFix<45,-54,TWOSCOMPLEMENT> &id2025in_a = id2024out_o;
    const HWOffsetFix<33,-33,UNSIGNED> &id2025in_b = id3411out_output[getCycle()%3];

    id2025out_result = (mul_fixed<78,-87,TWOSCOMPLEMENT,TONEAREVEN>(id2025in_a,id2025in_b));
  }
  HWRawBits<45> id2774out_result;

  { // Node ID: 2774 (NodeSlice)
    const HWRawBits<180> &id2774in_a = id2773out_dout[getCycle()%3];

    id2774out_result = (slice<0,45>(id2774in_a));
  }
  HWOffsetFix<45,-44,TWOSCOMPLEMENT> id2775out_output;

  { // Node ID: 2775 (NodeReinterpret)
    const HWRawBits<45> &id2775in_input = id2774out_result;

    id2775out_output = (cast_bits2fixed<45,-44,TWOSCOMPLEMENT>(id2775in_input));
  }
  HWOffsetFix<89,-87,TWOSCOMPLEMENT> id2026out_result;

  { // Node ID: 2026 (NodeAdd)
    const HWOffsetFix<78,-87,TWOSCOMPLEMENT> &id2026in_a = id2025out_result;
    const HWOffsetFix<45,-44,TWOSCOMPLEMENT> &id2026in_b = id2775out_output;

    id2026out_result = (add_fixed<89,-87,TWOSCOMPLEMENT,TONEAREVEN>(id2026in_a,id2026in_b));
  }
  HWOffsetFix<45,-43,TWOSCOMPLEMENT> id2027out_o;

  { // Node ID: 2027 (NodeCast)
    const HWOffsetFix<89,-87,TWOSCOMPLEMENT> &id2027in_i = id2026out_result;

    id2027out_o = (cast_fixed2fixed<45,-43,TWOSCOMPLEMENT,TONEAREVEN>(id2027in_i));
  }
  HWOffsetFix<56,-43,TWOSCOMPLEMENT> id2030out_result;

  { // Node ID: 2030 (NodeAdd)
    const HWOffsetFix<12,0,TWOSCOMPLEMENT> &id2030in_a = id3065out_result;
    const HWOffsetFix<45,-43,TWOSCOMPLEMENT> &id2030in_b = id2027out_o;

    id2030out_result = (add_fixed<56,-43,TWOSCOMPLEMENT,TONEAREVEN>(id2030in_a,id2030in_b));
  }
  HWFloat<11,45> id2031out_o;

  { // Node ID: 2031 (NodeCast)
    const HWOffsetFix<56,-43,TWOSCOMPLEMENT> &id2031in_i = id2030out_result;

    id2031out_o = (cast_fixed2float<11,45>(id2031in_i));
  }
  { // Node ID: 3446 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2043out_result;

  { // Node ID: 2043 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2043in_sel = id2041out_result;
    const HWFloat<11,45> &id2043in_option0 = id2031out_o;
    const HWFloat<11,45> &id2043in_option1 = id3446out_value;

    HWFloat<11,45> id2043x_1;

    switch((id2043in_sel.getValueAsLong())) {
      case 0l:
        id2043x_1 = id2043in_option0;
        break;
      case 1l:
        id2043x_1 = id2043in_option1;
        break;
      default:
        id2043x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2043out_result = (id2043x_1);
  }
  { // Node ID: 3445 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2050out_result;

  { // Node ID: 2050 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2050in_sel = id2048out_result;
    const HWFloat<11,45> &id2050in_option0 = id2043out_result;
    const HWFloat<11,45> &id2050in_option1 = id3445out_value;

    HWFloat<11,45> id2050x_1;

    switch((id2050in_sel.getValueAsLong())) {
      case 0l:
        id2050x_1 = id2050in_option0;
        break;
      case 1l:
        id2050x_1 = id2050in_option1;
        break;
      default:
        id2050x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2050out_result = (id2050x_1);
  }
  { // Node ID: 3444 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2056out_result;

  { // Node ID: 2056 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2056in_sel = id2054out_result;
    const HWFloat<11,45> &id2056in_option0 = id2050out_result;
    const HWFloat<11,45> &id2056in_option1 = id3444out_value;

    HWFloat<11,45> id2056x_1;

    switch((id2056in_sel.getValueAsLong())) {
      case 0l:
        id2056x_1 = id2056in_option0;
        break;
      case 1l:
        id2056x_1 = id2056in_option1;
        break;
      default:
        id2056x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2056out_result = (id2056x_1);
  }
  { // Node ID: 2057 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2058out_result;

  { // Node ID: 2058 (NodeMul)
    const HWFloat<11,45> &id2058in_a = id2056out_result;
    const HWFloat<11,45> &id2058in_b = id2057out_value;

    id2058out_result = (mul_float(id2058in_a,id2058in_b));
  }
  HWFloat<11,45> id2061out_result;

  { // Node ID: 2061 (NodeMul)
    const HWFloat<11,45> &id2061in_a = id3456out_value;
    const HWFloat<11,45> &id2061in_b = id2058out_result;

    id2061out_result = (mul_float(id2061in_a,id2061in_b));
  }
  HWFloat<11,45> id2063out_result;

  { // Node ID: 2063 (NodeSub)
    const HWFloat<11,45> &id2063in_a = id3457out_value;
    const HWFloat<11,45> &id2063in_b = id2061out_result;

    id2063out_result = (sub_float(id2063in_a,id2063in_b));
  }
  HWFloat<11,45> id2067out_result;

  { // Node ID: 2067 (NodeSub)
    const HWFloat<11,45> &id2067in_a = id3432out_output[getCycle()%2];
    const HWFloat<11,45> &id2067in_b = id2063out_result;

    id2067out_result = (sub_float(id2067in_a,id2067in_b));
  }
  HWFloat<11,45> id2068out_result;

  { // Node ID: 2068 (NodeMul)
    const HWFloat<11,45> &id2068in_a = id2066out_result;
    const HWFloat<11,45> &id2068in_b = id2067out_result;

    id2068out_result = (mul_float(id2068in_a,id2068in_b));
  }
  HWFloat<11,45> id2698out_result;

  { // Node ID: 2698 (NodeAdd)
    const HWFloat<11,45> &id2698in_a = id2697out_result;
    const HWFloat<11,45> &id2698in_b = id2068out_result;

    id2698out_result = (add_float(id2698in_a,id2698in_b));
  }
  HWFloat<11,45> id2699out_result;

  { // Node ID: 2699 (NodeMul)
    const HWFloat<11,45> &id2699in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id2699in_b = id2698out_result;

    id2699out_result = (mul_float(id2699in_a,id2699in_b));
  }
  HWFloat<11,45> id2700out_result;

  { // Node ID: 2700 (NodeSub)
    const HWFloat<11,45> &id2700in_a = id3432out_output[getCycle()%2];
    const HWFloat<11,45> &id2700in_b = id2699out_result;

    id2700out_result = (sub_float(id2700in_a,id2700in_b));
  }
  HWFloat<11,45> id3104out_output;

  { // Node ID: 3104 (NodeStreamOffset)
    const HWFloat<11,45> &id3104in_input = id2700out_result;

    id3104out_output = id3104in_input;
  }
  { // Node ID: 16 (NodeInputMappedReg)
  }
  { // Node ID: 17 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_sel = id2820out_result[getCycle()%2];
    const HWFloat<11,45> &id17in_option0 = id3104out_output;
    const HWFloat<11,45> &id17in_option1 = id16out_u_in;

    HWFloat<11,45> id17x_1;

    switch((id17in_sel.getValueAsLong())) {
      case 0l:
        id17x_1 = id17in_option0;
        break;
      case 1l:
        id17x_1 = id17in_option1;
        break;
      default:
        id17x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id17out_result[(getCycle()+1)%2] = (id17x_1);
  }
  { // Node ID: 3123 (NodeFIFO)
    const HWFloat<11,45> &id3123in_input = id17out_result[getCycle()%2];

    id3123out_output[(getCycle()+1)%2] = id3123in_input;
  }
  { // Node ID: 3431 (NodeFIFO)
    const HWFloat<11,45> &id3431in_input = id3123out_output[getCycle()%2];

    id3431out_output[(getCycle()+7)%8] = id3431in_input;
  }
  { // Node ID: 3432 (NodeFIFO)
    const HWFloat<11,45> &id3432in_input = id3431out_output[getCycle()%8];

    id3432out_output[(getCycle()+1)%2] = id3432in_input;
  }
  { // Node ID: 12 (NodeInputMappedReg)
  }
  { // Node ID: 13 (NodeCast)
    const HWFloat<11,53> &id13in_i = id12out_dt;

    id13out_o[(getCycle()+3)%4] = (cast_float2float<11,45>(id13in_i));
  }
  { // Node ID: 3696 (NodeConstantRawBits)
  }
  { // Node ID: 3695 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2379out_result;

  { // Node ID: 2379 (NodeAdd)
    const HWFloat<11,45> &id2379in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id2379in_b = id3695out_value;

    id2379out_result = (add_float(id2379in_a,id2379in_b));
  }
  HWFloat<11,45> id2381out_result;

  { // Node ID: 2381 (NodeMul)
    const HWFloat<11,45> &id2381in_a = id3696out_value;
    const HWFloat<11,45> &id2381in_b = id2379out_result;

    id2381out_result = (mul_float(id2381in_a,id2381in_b));
  }
  HWRawBits<11> id2458out_result;

  { // Node ID: 2458 (NodeSlice)
    const HWFloat<11,45> &id2458in_a = id2381out_result;

    id2458out_result = (slice<44,11>(id2458in_a));
  }
  { // Node ID: 2459 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2821out_result;

  { // Node ID: 2821 (NodeEqInlined)
    const HWRawBits<11> &id2821in_a = id2458out_result;
    const HWRawBits<11> &id2821in_b = id2459out_value;

    id2821out_result = (eq_bits(id2821in_a,id2821in_b));
  }
  HWRawBits<44> id2457out_result;

  { // Node ID: 2457 (NodeSlice)
    const HWFloat<11,45> &id2457in_a = id2381out_result;

    id2457out_result = (slice<0,44>(id2457in_a));
  }
  { // Node ID: 3694 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2822out_result;

  { // Node ID: 2822 (NodeNeqInlined)
    const HWRawBits<44> &id2822in_a = id2457out_result;
    const HWRawBits<44> &id2822in_b = id3694out_value;

    id2822out_result = (neq_bits(id2822in_a,id2822in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2463out_result;

  { // Node ID: 2463 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2463in_a = id2821out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2463in_b = id2822out_result;

    HWOffsetFix<1,0,UNSIGNED> id2463x_1;

    (id2463x_1) = (and_fixed(id2463in_a,id2463in_b));
    id2463out_result = (id2463x_1);
  }
  { // Node ID: 3122 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3122in_input = id2463out_result;

    id3122out_output[(getCycle()+8)%9] = id3122in_input;
  }
  { // Node ID: 2382 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2383out_output;
  HWOffsetFix<1,0,UNSIGNED> id2383out_output_doubt;

  { // Node ID: 2383 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2383in_input = id2381out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2383in_doubt = id2382out_value;

    id2383out_output = id2383in_input;
    id2383out_output_doubt = id2383in_doubt;
  }
  { // Node ID: 2384 (NodeCast)
    const HWFloat<11,45> &id2384in_i = id2383out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2384in_i_doubt = id2383out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2384x_1;

    id2384out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2384in_i,(&(id2384x_1))));
    id2384out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2384x_1),(c_hw_fix_4_0_uns_bits))),id2384in_i_doubt));
  }
  { // Node ID: 2387 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2386out_result;
  HWOffsetFix<1,0,UNSIGNED> id2386out_result_doubt;

  { // Node ID: 2386 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2386in_a = id2384out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2386in_a_doubt = id2384out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2386in_b = id2387out_value;

    HWOffsetFix<1,0,UNSIGNED> id2386x_1;

    id2386out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2386in_a,id2386in_b,(&(id2386x_1))));
    id2386out_result_doubt = (or_fixed((neq_fixed((id2386x_1),(c_hw_fix_1_0_uns_bits_1))),id2386in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2388out_o;
  HWOffsetFix<1,0,UNSIGNED> id2388out_o_doubt;

  { // Node ID: 2388 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2388in_i = id2386out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2388in_i_doubt = id2386out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2388x_1;

    id2388out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2388in_i,(&(id2388x_1))));
    id2388out_o_doubt = (or_fixed((neq_fixed((id2388x_1),(c_hw_fix_1_0_uns_bits_1))),id2388in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2397out_output;

  { // Node ID: 2397 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2397in_input = id2388out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2397in_input_doubt = id2388out_o_doubt;

    id2397out_output = id2397in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2398out_o;

  { // Node ID: 2398 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2398in_i = id2397out_output;

    id2398out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2398in_i));
  }
  { // Node ID: 3113 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3113in_input = id2398out_o;

    id3113out_output[(getCycle()+2)%3] = id3113in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id2425out_o;

  { // Node ID: 2425 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id2425in_i = id3113out_output[getCycle()%3];

    id2425out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id2425in_i));
  }
  { // Node ID: 2428 (NodeConstantRawBits)
  }
  { // Node ID: 3014 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id2401out_o;

  { // Node ID: 2401 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2401in_i = id2397out_output;

    id2401out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2401in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2473out_output;

  { // Node ID: 2473 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2473in_input = id2401out_o;

    id2473out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2473in_input))));
  }
  { // Node ID: 2474 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2474in_addr = id2473out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2474x_1;

    switch(((long)((id2474in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2474x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2474x_1 = (id2474sta_rom_store[(id2474in_addr.getValueAsLong())]);
        break;
      default:
        id2474x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2474out_dout[(getCycle()+2)%3] = (id2474x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2400out_o;

  { // Node ID: 2400 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2400in_i = id2397out_output;

    id2400out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2400in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2470out_output;

  { // Node ID: 2470 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2470in_input = id2400out_o;

    id2470out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2470in_input))));
  }
  { // Node ID: 2471 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2471in_addr = id2470out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2471x_1;

    switch(((long)((id2471in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2471x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2471x_1 = (id2471sta_rom_store[(id2471in_addr.getValueAsLong())]);
        break;
      default:
        id2471x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2471out_dout[(getCycle()+2)%3] = (id2471x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2399out_o;

  { // Node ID: 2399 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2399in_i = id2397out_output;

    id2399out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2399in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2467out_output;

  { // Node ID: 2467 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2467in_input = id2399out_o;

    id2467out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2467in_input))));
  }
  { // Node ID: 2468 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2468in_addr = id2467out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2468x_1;

    switch(((long)((id2468in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2468x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2468x_1 = (id2468sta_rom_store[(id2468in_addr.getValueAsLong())]);
        break;
      default:
        id2468x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2468out_dout[(getCycle()+2)%3] = (id2468x_1);
  }
  { // Node ID: 2405 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2402out_o;

  { // Node ID: 2402 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2402in_i = id2397out_output;

    id2402out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2402in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2404out_result;

  { // Node ID: 2404 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2404in_a = id2405out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2404in_b = id2402out_o;

    id2404out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2404in_a,id2404in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2406out_o;

  { // Node ID: 2406 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2406in_i = id2404out_result;

    id2406out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2406in_i));
  }
  { // Node ID: 3114 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3114in_input = id2406out_o;

    id3114out_output[(getCycle()+2)%3] = id3114in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id2407out_result;

  { // Node ID: 2407 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id2407in_a = id2468out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id2407in_b = id3114out_output[getCycle()%3];

    id2407out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id2407in_a,id2407in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id2408out_result;

  { // Node ID: 2408 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id2408in_a = id3114out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id2408in_b = id2468out_dout[getCycle()%3];

    id2408out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id2408in_a,id2408in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id2409out_result;

  { // Node ID: 2409 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id2409in_a = id2407out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id2409in_b = id2408out_result;

    id2409out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id2409in_a,id2409in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id2410out_o;

  { // Node ID: 2410 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id2410in_i = id2409out_result;

    id2410out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id2410in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2411out_result;

  { // Node ID: 2411 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id2411in_a = id2471out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id2411in_b = id2410out_o;

    id2411out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id2411in_a,id2411in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id2412out_result;

  { // Node ID: 2412 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id2412in_a = id2410out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id2412in_b = id2471out_dout[getCycle()%3];

    id2412out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id2412in_a,id2412in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id2413out_result;

  { // Node ID: 2413 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id2413in_a = id2411out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id2413in_b = id2412out_result;

    id2413out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id2413in_a,id2413in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id2414out_o;

  { // Node ID: 2414 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id2414in_i = id2413out_result;

    id2414out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id2414in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2415out_result;

  { // Node ID: 2415 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id2415in_a = id2474out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id2415in_b = id2414out_o;

    id2415out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id2415in_a,id2415in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id2416out_result;

  { // Node ID: 2416 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id2416in_a = id2414out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id2416in_b = id2474out_dout[getCycle()%3];

    id2416out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id2416in_a,id2416in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id2417out_result;

  { // Node ID: 2417 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id2417in_a = id2415out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id2417in_b = id2416out_result;

    id2417out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id2417in_a,id2417in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id2418out_o;

  { // Node ID: 2418 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id2418in_i = id2417out_result;

    id2418out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id2418in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id2419out_o;

  { // Node ID: 2419 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id2419in_i = id2418out_o;

    id2419out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id2419in_i));
  }
  { // Node ID: 3693 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2823out_result;

  { // Node ID: 2823 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2823in_a = id2419out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2823in_b = id3693out_value;

    id2823out_result = (gte_fixed(id2823in_a,id2823in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3090out_result;

  { // Node ID: 3090 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3090in_a = id2425out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3090in_b = id2428out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3090in_c = id3014out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3090in_condb = id2823out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3090x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3090x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3090x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3090x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3090x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3090x_1 = id3090in_a;
        break;
      default:
        id3090x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3090in_condb.getValueAsLong())) {
      case 0l:
        id3090x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3090x_2 = id3090in_b;
        break;
      default:
        id3090x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3090x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3090x_3 = id3090in_c;
        break;
      default:
        id3090x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3090x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3090x_1),(id3090x_2))),(id3090x_3)));
    id3090out_result = (id3090x_4);
  }
  HWRawBits<1> id2824out_result;

  { // Node ID: 2824 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2824in_a = id3090out_result;

    id2824out_result = (slice<13,1>(id2824in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2825out_output;

  { // Node ID: 2825 (NodeReinterpret)
    const HWRawBits<1> &id2825in_input = id2824out_result;

    id2825out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2825in_input));
  }
  { // Node ID: 3692 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2390out_result;

  { // Node ID: 2390 (NodeGt)
    const HWFloat<11,45> &id2390in_a = id2381out_result;
    const HWFloat<11,45> &id2390in_b = id3692out_value;

    id2390out_result = (gt_float(id2390in_a,id2390in_b));
  }
  { // Node ID: 3116 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3116in_input = id2390out_result;

    id3116out_output[(getCycle()+6)%7] = id3116in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2391out_output;

  { // Node ID: 2391 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2391in_input = id2388out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2391in_input_doubt = id2388out_o_doubt;

    id2391out_output = id2391in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2392out_result;

  { // Node ID: 2392 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2392in_a = id3116out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2392in_b = id2391out_output;

    HWOffsetFix<1,0,UNSIGNED> id2392x_1;

    (id2392x_1) = (and_fixed(id2392in_a,id2392in_b));
    id2392out_result = (id2392x_1);
  }
  { // Node ID: 3117 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3117in_input = id2392out_result;

    id3117out_output[(getCycle()+2)%3] = id3117in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2434out_result;

  { // Node ID: 2434 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2434in_a = id3117out_output[getCycle()%3];

    id2434out_result = (not_fixed(id2434in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2435out_result;

  { // Node ID: 2435 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2435in_a = id2825out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2435in_b = id2434out_result;

    HWOffsetFix<1,0,UNSIGNED> id2435x_1;

    (id2435x_1) = (and_fixed(id2435in_a,id2435in_b));
    id2435out_result = (id2435x_1);
  }
  { // Node ID: 3691 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2394out_result;

  { // Node ID: 2394 (NodeLt)
    const HWFloat<11,45> &id2394in_a = id2381out_result;
    const HWFloat<11,45> &id2394in_b = id3691out_value;

    id2394out_result = (lt_float(id2394in_a,id2394in_b));
  }
  { // Node ID: 3118 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3118in_input = id2394out_result;

    id3118out_output[(getCycle()+6)%7] = id3118in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2395out_output;

  { // Node ID: 2395 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2395in_input = id2388out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2395in_input_doubt = id2388out_o_doubt;

    id2395out_output = id2395in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2396out_result;

  { // Node ID: 2396 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2396in_a = id3118out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2396in_b = id2395out_output;

    HWOffsetFix<1,0,UNSIGNED> id2396x_1;

    (id2396x_1) = (and_fixed(id2396in_a,id2396in_b));
    id2396out_result = (id2396x_1);
  }
  { // Node ID: 3119 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3119in_input = id2396out_result;

    id3119out_output[(getCycle()+2)%3] = id3119in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2436out_result;

  { // Node ID: 2436 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2436in_a = id2435out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2436in_b = id3119out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2436x_1;

    (id2436x_1) = (or_fixed(id2436in_a,id2436in_b));
    id2436out_result = (id2436x_1);
  }
  { // Node ID: 3690 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2826out_result;

  { // Node ID: 2826 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2826in_a = id3090out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2826in_b = id3690out_value;

    id2826out_result = (gte_fixed(id2826in_a,id2826in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2443out_result;

  { // Node ID: 2443 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2443in_a = id3119out_output[getCycle()%3];

    id2443out_result = (not_fixed(id2443in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2444out_result;

  { // Node ID: 2444 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2444in_a = id2826out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2444in_b = id2443out_result;

    HWOffsetFix<1,0,UNSIGNED> id2444x_1;

    (id2444x_1) = (and_fixed(id2444in_a,id2444in_b));
    id2444out_result = (id2444x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id2445out_result;

  { // Node ID: 2445 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id2445in_a = id2444out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2445in_b = id3117out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id2445x_1;

    (id2445x_1) = (or_fixed(id2445in_a,id2445in_b));
    id2445out_result = (id2445x_1);
  }
  HWRawBits<2> id2446out_result;

  { // Node ID: 2446 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2446in_in0 = id2436out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2446in_in1 = id2445out_result;

    id2446out_result = (cat(id2446in_in0,id2446in_in1));
  }
  { // Node ID: 2438 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2437out_o;

  { // Node ID: 2437 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2437in_i = id3090out_result;

    id2437out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id2437in_i));
  }
  { // Node ID: 2422 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id2423out_result;

  { // Node ID: 2423 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2423in_sel = id2823out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id2423in_option0 = id2419out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2423in_option1 = id2422out_value;

    HWOffsetFix<45,-44,UNSIGNED> id2423x_1;

    switch((id2423in_sel.getValueAsLong())) {
      case 0l:
        id2423x_1 = id2423in_option0;
        break;
      case 1l:
        id2423x_1 = id2423in_option1;
        break;
      default:
        id2423x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id2423out_result = (id2423x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2424out_o;

  { // Node ID: 2424 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2424in_i = id2423out_result;

    id2424out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2424in_i));
  }
  HWRawBits<56> id2439out_result;

  { // Node ID: 2439 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2439in_in0 = id2438out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id2439in_in1 = id2437out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id2439in_in2 = id2424out_o;

    id2439out_result = (cat((cat(id2439in_in0,id2439in_in1)),id2439in_in2));
  }
  HWFloat<11,45> id2440out_output;

  { // Node ID: 2440 (NodeReinterpret)
    const HWRawBits<56> &id2440in_input = id2439out_result;

    id2440out_output = (cast_bits2float<11,45>(id2440in_input));
  }
  { // Node ID: 2447 (NodeConstantRawBits)
  }
  { // Node ID: 2448 (NodeConstantRawBits)
  }
  { // Node ID: 2450 (NodeConstantRawBits)
  }
  HWRawBits<56> id2827out_result;

  { // Node ID: 2827 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2827in_in0 = id2447out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2827in_in1 = id2448out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2827in_in2 = id2450out_value;

    id2827out_result = (cat((cat(id2827in_in0,id2827in_in1)),id2827in_in2));
  }
  HWFloat<11,45> id2452out_output;

  { // Node ID: 2452 (NodeReinterpret)
    const HWRawBits<56> &id2452in_input = id2827out_result;

    id2452out_output = (cast_bits2float<11,45>(id2452in_input));
  }
  { // Node ID: 2794 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2455out_result;

  { // Node ID: 2455 (NodeMux)
    const HWRawBits<2> &id2455in_sel = id2446out_result;
    const HWFloat<11,45> &id2455in_option0 = id2440out_output;
    const HWFloat<11,45> &id2455in_option1 = id2452out_output;
    const HWFloat<11,45> &id2455in_option2 = id2794out_value;
    const HWFloat<11,45> &id2455in_option3 = id2452out_output;

    HWFloat<11,45> id2455x_1;

    switch((id2455in_sel.getValueAsLong())) {
      case 0l:
        id2455x_1 = id2455in_option0;
        break;
      case 1l:
        id2455x_1 = id2455in_option1;
        break;
      case 2l:
        id2455x_1 = id2455in_option2;
        break;
      case 3l:
        id2455x_1 = id2455in_option3;
        break;
      default:
        id2455x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2455out_result = (id2455x_1);
  }
  { // Node ID: 3689 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2465out_result;

  { // Node ID: 2465 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id2465in_sel = id3122out_output[getCycle()%9];
    const HWFloat<11,45> &id2465in_option0 = id2455out_result;
    const HWFloat<11,45> &id2465in_option1 = id3689out_value;

    HWFloat<11,45> id2465x_1;

    switch((id2465in_sel.getValueAsLong())) {
      case 0l:
        id2465x_1 = id2465in_option0;
        break;
      case 1l:
        id2465x_1 = id2465in_option1;
        break;
      default:
        id2465x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id2465out_result = (id2465x_1);
  }
  { // Node ID: 3688 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2476out_result;

  { // Node ID: 2476 (NodeSub)
    const HWFloat<11,45> &id2476in_a = id2465out_result;
    const HWFloat<11,45> &id2476in_b = id3688out_value;

    id2476out_result = (sub_float(id2476in_a,id2476in_b));
  }
  { // Node ID: 3091 (NodePO2FPMult)
    const HWFloat<11,45> &id3091in_floatIn = id2476out_result;

    id3091out_floatOut[(getCycle()+1)%2] = (mul_float(id3091in_floatIn,(c_hw_flt_11_45_4_0val)));
  }
  { // Node ID: 3687 (NodeConstantRawBits)
  }
  { // Node ID: 3686 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2079out_result;

  { // Node ID: 2079 (NodeAdd)
    const HWFloat<11,45> &id2079in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id2079in_b = id3686out_value;

    id2079out_result = (add_float(id2079in_a,id2079in_b));
  }
  HWFloat<11,45> id2081out_result;

  { // Node ID: 2081 (NodeMul)
    const HWFloat<11,45> &id2081in_a = id3687out_value;
    const HWFloat<11,45> &id2081in_b = id2079out_result;

    id2081out_result = (mul_float(id2081in_a,id2081in_b));
  }
  HWRawBits<11> id2158out_result;

  { // Node ID: 2158 (NodeSlice)
    const HWFloat<11,45> &id2158in_a = id2081out_result;

    id2158out_result = (slice<44,11>(id2158in_a));
  }
  { // Node ID: 2159 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2828out_result;

  { // Node ID: 2828 (NodeEqInlined)
    const HWRawBits<11> &id2828in_a = id2158out_result;
    const HWRawBits<11> &id2828in_b = id2159out_value;

    id2828out_result = (eq_bits(id2828in_a,id2828in_b));
  }
  HWRawBits<44> id2157out_result;

  { // Node ID: 2157 (NodeSlice)
    const HWFloat<11,45> &id2157in_a = id2081out_result;

    id2157out_result = (slice<0,44>(id2157in_a));
  }
  { // Node ID: 3685 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2829out_result;

  { // Node ID: 2829 (NodeNeqInlined)
    const HWRawBits<44> &id2829in_a = id2157out_result;
    const HWRawBits<44> &id2829in_b = id3685out_value;

    id2829out_result = (neq_bits(id2829in_a,id2829in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2163out_result;

  { // Node ID: 2163 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2163in_a = id2828out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2163in_b = id2829out_result;

    HWOffsetFix<1,0,UNSIGNED> id2163x_1;

    (id2163x_1) = (and_fixed(id2163in_a,id2163in_b));
    id2163out_result = (id2163x_1);
  }
  { // Node ID: 3133 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3133in_input = id2163out_result;

    id3133out_output[(getCycle()+8)%9] = id3133in_input;
  }
  { // Node ID: 2082 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2083out_output;
  HWOffsetFix<1,0,UNSIGNED> id2083out_output_doubt;

  { // Node ID: 2083 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2083in_input = id2081out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2083in_doubt = id2082out_value;

    id2083out_output = id2083in_input;
    id2083out_output_doubt = id2083in_doubt;
  }
  { // Node ID: 2084 (NodeCast)
    const HWFloat<11,45> &id2084in_i = id2083out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2084in_i_doubt = id2083out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2084x_1;

    id2084out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2084in_i,(&(id2084x_1))));
    id2084out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2084x_1),(c_hw_fix_4_0_uns_bits))),id2084in_i_doubt));
  }
  { // Node ID: 2087 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2086out_result;
  HWOffsetFix<1,0,UNSIGNED> id2086out_result_doubt;

  { // Node ID: 2086 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2086in_a = id2084out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2086in_a_doubt = id2084out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2086in_b = id2087out_value;

    HWOffsetFix<1,0,UNSIGNED> id2086x_1;

    id2086out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2086in_a,id2086in_b,(&(id2086x_1))));
    id2086out_result_doubt = (or_fixed((neq_fixed((id2086x_1),(c_hw_fix_1_0_uns_bits_1))),id2086in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2088out_o;
  HWOffsetFix<1,0,UNSIGNED> id2088out_o_doubt;

  { // Node ID: 2088 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2088in_i = id2086out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2088in_i_doubt = id2086out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2088x_1;

    id2088out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2088in_i,(&(id2088x_1))));
    id2088out_o_doubt = (or_fixed((neq_fixed((id2088x_1),(c_hw_fix_1_0_uns_bits_1))),id2088in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2097out_output;

  { // Node ID: 2097 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2097in_input = id2088out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2097in_input_doubt = id2088out_o_doubt;

    id2097out_output = id2097in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2098out_o;

  { // Node ID: 2098 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2098in_i = id2097out_output;

    id2098out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2098in_i));
  }
  { // Node ID: 3124 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3124in_input = id2098out_o;

    id3124out_output[(getCycle()+2)%3] = id3124in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id2101out_o;

  { // Node ID: 2101 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2101in_i = id2097out_output;

    id2101out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2101in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2173out_output;

  { // Node ID: 2173 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2173in_input = id2101out_o;

    id2173out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2173in_input))));
  }
  { // Node ID: 2174 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2174in_addr = id2173out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2174x_1;

    switch(((long)((id2174in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2174x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2174x_1 = (id2174sta_rom_store[(id2174in_addr.getValueAsLong())]);
        break;
      default:
        id2174x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2174out_dout[(getCycle()+2)%3] = (id2174x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2100out_o;

  { // Node ID: 2100 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2100in_i = id2097out_output;

    id2100out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2100in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2170out_output;

  { // Node ID: 2170 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2170in_input = id2100out_o;

    id2170out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2170in_input))));
  }
  { // Node ID: 2171 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2171in_addr = id2170out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2171x_1;

    switch(((long)((id2171in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2171x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2171x_1 = (id2171sta_rom_store[(id2171in_addr.getValueAsLong())]);
        break;
      default:
        id2171x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2171out_dout[(getCycle()+2)%3] = (id2171x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2099out_o;

  { // Node ID: 2099 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2099in_i = id2097out_output;

    id2099out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2099in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2167out_output;

  { // Node ID: 2167 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2167in_input = id2099out_o;

    id2167out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2167in_input))));
  }
  { // Node ID: 2168 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2168in_addr = id2167out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2168x_1;

    switch(((long)((id2168in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2168x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2168x_1 = (id2168sta_rom_store[(id2168in_addr.getValueAsLong())]);
        break;
      default:
        id2168x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2168out_dout[(getCycle()+2)%3] = (id2168x_1);
  }
  { // Node ID: 2105 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2102out_o;

  { // Node ID: 2102 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2102in_i = id2097out_output;

    id2102out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2102in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2104out_result;

  { // Node ID: 2104 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2104in_a = id2105out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2104in_b = id2102out_o;

    id2104out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2104in_a,id2104in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2106out_o;

  { // Node ID: 2106 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2106in_i = id2104out_result;

    id2106out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2106in_i));
  }
  { // Node ID: 3125 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3125in_input = id2106out_o;

    id3125out_output[(getCycle()+2)%3] = id3125in_input;
  }
  { // Node ID: 3683 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2090out_result;

  { // Node ID: 2090 (NodeGt)
    const HWFloat<11,45> &id2090in_a = id2081out_result;
    const HWFloat<11,45> &id2090in_b = id3683out_value;

    id2090out_result = (gt_float(id2090in_a,id2090in_b));
  }
  { // Node ID: 3127 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3127in_input = id2090out_result;

    id3127out_output[(getCycle()+6)%7] = id3127in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2091out_output;

  { // Node ID: 2091 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2091in_input = id2088out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2091in_input_doubt = id2088out_o_doubt;

    id2091out_output = id2091in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2092out_result;

  { // Node ID: 2092 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2092in_a = id3127out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2092in_b = id2091out_output;

    HWOffsetFix<1,0,UNSIGNED> id2092x_1;

    (id2092x_1) = (and_fixed(id2092in_a,id2092in_b));
    id2092out_result = (id2092x_1);
  }
  { // Node ID: 3128 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3128in_input = id2092out_result;

    id3128out_output[(getCycle()+2)%3] = id3128in_input;
  }
  { // Node ID: 3682 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2094out_result;

  { // Node ID: 2094 (NodeLt)
    const HWFloat<11,45> &id2094in_a = id2081out_result;
    const HWFloat<11,45> &id2094in_b = id3682out_value;

    id2094out_result = (lt_float(id2094in_a,id2094in_b));
  }
  { // Node ID: 3129 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3129in_input = id2094out_result;

    id3129out_output[(getCycle()+6)%7] = id3129in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2095out_output;

  { // Node ID: 2095 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2095in_input = id2088out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2095in_input_doubt = id2088out_o_doubt;

    id2095out_output = id2095in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2096out_result;

  { // Node ID: 2096 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2096in_a = id3129out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2096in_b = id2095out_output;

    HWOffsetFix<1,0,UNSIGNED> id2096x_1;

    (id2096x_1) = (and_fixed(id2096in_a,id2096in_b));
    id2096out_result = (id2096x_1);
  }
  { // Node ID: 3130 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3130in_input = id2096out_result;

    id3130out_output[(getCycle()+2)%3] = id3130in_input;
  }
  { // Node ID: 3679 (NodeConstantRawBits)
  }
  { // Node ID: 3678 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2176out_result;

  { // Node ID: 2176 (NodeAdd)
    const HWFloat<11,45> &id2176in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id2176in_b = id3678out_value;

    id2176out_result = (add_float(id2176in_a,id2176in_b));
  }
  HWFloat<11,45> id2178out_result;

  { // Node ID: 2178 (NodeMul)
    const HWFloat<11,45> &id2178in_a = id3679out_value;
    const HWFloat<11,45> &id2178in_b = id2176out_result;

    id2178out_result = (mul_float(id2178in_a,id2178in_b));
  }
  HWRawBits<11> id2255out_result;

  { // Node ID: 2255 (NodeSlice)
    const HWFloat<11,45> &id2255in_a = id2178out_result;

    id2255out_result = (slice<44,11>(id2255in_a));
  }
  { // Node ID: 2256 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2835out_result;

  { // Node ID: 2835 (NodeEqInlined)
    const HWRawBits<11> &id2835in_a = id2255out_result;
    const HWRawBits<11> &id2835in_b = id2256out_value;

    id2835out_result = (eq_bits(id2835in_a,id2835in_b));
  }
  HWRawBits<44> id2254out_result;

  { // Node ID: 2254 (NodeSlice)
    const HWFloat<11,45> &id2254in_a = id2178out_result;

    id2254out_result = (slice<0,44>(id2254in_a));
  }
  { // Node ID: 3677 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2836out_result;

  { // Node ID: 2836 (NodeNeqInlined)
    const HWRawBits<44> &id2836in_a = id2254out_result;
    const HWRawBits<44> &id2836in_b = id3677out_value;

    id2836out_result = (neq_bits(id2836in_a,id2836in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2260out_result;

  { // Node ID: 2260 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2260in_a = id2835out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2260in_b = id2836out_result;

    HWOffsetFix<1,0,UNSIGNED> id2260x_1;

    (id2260x_1) = (and_fixed(id2260in_a,id2260in_b));
    id2260out_result = (id2260x_1);
  }
  { // Node ID: 3144 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3144in_input = id2260out_result;

    id3144out_output[(getCycle()+8)%9] = id3144in_input;
  }
  { // Node ID: 2179 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2180out_output;
  HWOffsetFix<1,0,UNSIGNED> id2180out_output_doubt;

  { // Node ID: 2180 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2180in_input = id2178out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2180in_doubt = id2179out_value;

    id2180out_output = id2180in_input;
    id2180out_output_doubt = id2180in_doubt;
  }
  { // Node ID: 2181 (NodeCast)
    const HWFloat<11,45> &id2181in_i = id2180out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2181in_i_doubt = id2180out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2181x_1;

    id2181out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2181in_i,(&(id2181x_1))));
    id2181out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2181x_1),(c_hw_fix_4_0_uns_bits))),id2181in_i_doubt));
  }
  { // Node ID: 2184 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2183out_result;
  HWOffsetFix<1,0,UNSIGNED> id2183out_result_doubt;

  { // Node ID: 2183 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2183in_a = id2181out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2183in_a_doubt = id2181out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2183in_b = id2184out_value;

    HWOffsetFix<1,0,UNSIGNED> id2183x_1;

    id2183out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2183in_a,id2183in_b,(&(id2183x_1))));
    id2183out_result_doubt = (or_fixed((neq_fixed((id2183x_1),(c_hw_fix_1_0_uns_bits_1))),id2183in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2185out_o;
  HWOffsetFix<1,0,UNSIGNED> id2185out_o_doubt;

  { // Node ID: 2185 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2185in_i = id2183out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2185in_i_doubt = id2183out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2185x_1;

    id2185out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2185in_i,(&(id2185x_1))));
    id2185out_o_doubt = (or_fixed((neq_fixed((id2185x_1),(c_hw_fix_1_0_uns_bits_1))),id2185in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2194out_output;

  { // Node ID: 2194 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2194in_input = id2185out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2194in_input_doubt = id2185out_o_doubt;

    id2194out_output = id2194in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2195out_o;

  { // Node ID: 2195 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2195in_i = id2194out_output;

    id2195out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2195in_i));
  }
  { // Node ID: 3135 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3135in_input = id2195out_o;

    id3135out_output[(getCycle()+2)%3] = id3135in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id2198out_o;

  { // Node ID: 2198 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2198in_i = id2194out_output;

    id2198out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2198in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2270out_output;

  { // Node ID: 2270 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2270in_input = id2198out_o;

    id2270out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2270in_input))));
  }
  { // Node ID: 2271 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2271in_addr = id2270out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2271x_1;

    switch(((long)((id2271in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2271x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2271x_1 = (id2271sta_rom_store[(id2271in_addr.getValueAsLong())]);
        break;
      default:
        id2271x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2271out_dout[(getCycle()+2)%3] = (id2271x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2197out_o;

  { // Node ID: 2197 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2197in_i = id2194out_output;

    id2197out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2197in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2267out_output;

  { // Node ID: 2267 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2267in_input = id2197out_o;

    id2267out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2267in_input))));
  }
  { // Node ID: 2268 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2268in_addr = id2267out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2268x_1;

    switch(((long)((id2268in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2268x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2268x_1 = (id2268sta_rom_store[(id2268in_addr.getValueAsLong())]);
        break;
      default:
        id2268x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2268out_dout[(getCycle()+2)%3] = (id2268x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2196out_o;

  { // Node ID: 2196 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2196in_i = id2194out_output;

    id2196out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2196in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2264out_output;

  { // Node ID: 2264 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2264in_input = id2196out_o;

    id2264out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2264in_input))));
  }
  { // Node ID: 2265 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2265in_addr = id2264out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2265x_1;

    switch(((long)((id2265in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2265x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2265x_1 = (id2265sta_rom_store[(id2265in_addr.getValueAsLong())]);
        break;
      default:
        id2265x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2265out_dout[(getCycle()+2)%3] = (id2265x_1);
  }
  { // Node ID: 2202 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2199out_o;

  { // Node ID: 2199 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2199in_i = id2194out_output;

    id2199out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2199in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2201out_result;

  { // Node ID: 2201 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2201in_a = id2202out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2201in_b = id2199out_o;

    id2201out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2201in_a,id2201in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2203out_o;

  { // Node ID: 2203 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2203in_i = id2201out_result;

    id2203out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2203in_i));
  }
  { // Node ID: 3136 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3136in_input = id2203out_o;

    id3136out_output[(getCycle()+2)%3] = id3136in_input;
  }
  { // Node ID: 3675 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2187out_result;

  { // Node ID: 2187 (NodeGt)
    const HWFloat<11,45> &id2187in_a = id2178out_result;
    const HWFloat<11,45> &id2187in_b = id3675out_value;

    id2187out_result = (gt_float(id2187in_a,id2187in_b));
  }
  { // Node ID: 3138 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3138in_input = id2187out_result;

    id3138out_output[(getCycle()+6)%7] = id3138in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2188out_output;

  { // Node ID: 2188 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2188in_input = id2185out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2188in_input_doubt = id2185out_o_doubt;

    id2188out_output = id2188in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2189out_result;

  { // Node ID: 2189 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2189in_a = id3138out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2189in_b = id2188out_output;

    HWOffsetFix<1,0,UNSIGNED> id2189x_1;

    (id2189x_1) = (and_fixed(id2189in_a,id2189in_b));
    id2189out_result = (id2189x_1);
  }
  { // Node ID: 3139 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3139in_input = id2189out_result;

    id3139out_output[(getCycle()+2)%3] = id3139in_input;
  }
  { // Node ID: 3674 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2191out_result;

  { // Node ID: 2191 (NodeLt)
    const HWFloat<11,45> &id2191in_a = id2178out_result;
    const HWFloat<11,45> &id2191in_b = id3674out_value;

    id2191out_result = (lt_float(id2191in_a,id2191in_b));
  }
  { // Node ID: 3140 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3140in_input = id2191out_result;

    id3140out_output[(getCycle()+6)%7] = id3140in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2192out_output;

  { // Node ID: 2192 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2192in_input = id2185out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2192in_input_doubt = id2185out_o_doubt;

    id2192out_output = id2192in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2193out_result;

  { // Node ID: 2193 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2193in_a = id3140out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2193in_b = id2192out_output;

    HWOffsetFix<1,0,UNSIGNED> id2193x_1;

    (id2193x_1) = (and_fixed(id2193in_a,id2193in_b));
    id2193out_result = (id2193x_1);
  }
  { // Node ID: 3141 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3141in_input = id2193out_result;

    id3141out_output[(getCycle()+2)%3] = id3141in_input;
  }
  { // Node ID: 3668 (NodeConstantRawBits)
  }
  { // Node ID: 3667 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2274out_result;

  { // Node ID: 2274 (NodeAdd)
    const HWFloat<11,45> &id2274in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id2274in_b = id3667out_value;

    id2274out_result = (add_float(id2274in_a,id2274in_b));
  }
  HWFloat<11,45> id2276out_result;

  { // Node ID: 2276 (NodeMul)
    const HWFloat<11,45> &id2276in_a = id3668out_value;
    const HWFloat<11,45> &id2276in_b = id2274out_result;

    id2276out_result = (mul_float(id2276in_a,id2276in_b));
  }
  HWRawBits<11> id2353out_result;

  { // Node ID: 2353 (NodeSlice)
    const HWFloat<11,45> &id2353in_a = id2276out_result;

    id2353out_result = (slice<44,11>(id2353in_a));
  }
  { // Node ID: 2354 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2842out_result;

  { // Node ID: 2842 (NodeEqInlined)
    const HWRawBits<11> &id2842in_a = id2353out_result;
    const HWRawBits<11> &id2842in_b = id2354out_value;

    id2842out_result = (eq_bits(id2842in_a,id2842in_b));
  }
  HWRawBits<44> id2352out_result;

  { // Node ID: 2352 (NodeSlice)
    const HWFloat<11,45> &id2352in_a = id2276out_result;

    id2352out_result = (slice<0,44>(id2352in_a));
  }
  { // Node ID: 3666 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2843out_result;

  { // Node ID: 2843 (NodeNeqInlined)
    const HWRawBits<44> &id2843in_a = id2352out_result;
    const HWRawBits<44> &id2843in_b = id3666out_value;

    id2843out_result = (neq_bits(id2843in_a,id2843in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2358out_result;

  { // Node ID: 2358 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2358in_a = id2842out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2358in_b = id2843out_result;

    HWOffsetFix<1,0,UNSIGNED> id2358x_1;

    (id2358x_1) = (and_fixed(id2358in_a,id2358in_b));
    id2358out_result = (id2358x_1);
  }
  { // Node ID: 3156 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3156in_input = id2358out_result;

    id3156out_output[(getCycle()+8)%9] = id3156in_input;
  }
  { // Node ID: 2277 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2278out_output;
  HWOffsetFix<1,0,UNSIGNED> id2278out_output_doubt;

  { // Node ID: 2278 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2278in_input = id2276out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2278in_doubt = id2277out_value;

    id2278out_output = id2278in_input;
    id2278out_output_doubt = id2278in_doubt;
  }
  { // Node ID: 2279 (NodeCast)
    const HWFloat<11,45> &id2279in_i = id2278out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2279in_i_doubt = id2278out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2279x_1;

    id2279out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2279in_i,(&(id2279x_1))));
    id2279out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2279x_1),(c_hw_fix_4_0_uns_bits))),id2279in_i_doubt));
  }
  { // Node ID: 2282 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2281out_result;
  HWOffsetFix<1,0,UNSIGNED> id2281out_result_doubt;

  { // Node ID: 2281 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2281in_a = id2279out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2281in_a_doubt = id2279out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2281in_b = id2282out_value;

    HWOffsetFix<1,0,UNSIGNED> id2281x_1;

    id2281out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2281in_a,id2281in_b,(&(id2281x_1))));
    id2281out_result_doubt = (or_fixed((neq_fixed((id2281x_1),(c_hw_fix_1_0_uns_bits_1))),id2281in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2283out_o;
  HWOffsetFix<1,0,UNSIGNED> id2283out_o_doubt;

  { // Node ID: 2283 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2283in_i = id2281out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2283in_i_doubt = id2281out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2283x_1;

    id2283out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2283in_i,(&(id2283x_1))));
    id2283out_o_doubt = (or_fixed((neq_fixed((id2283x_1),(c_hw_fix_1_0_uns_bits_1))),id2283in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2292out_output;

  { // Node ID: 2292 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2292in_input = id2283out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2292in_input_doubt = id2283out_o_doubt;

    id2292out_output = id2292in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2293out_o;

  { // Node ID: 2293 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2293in_i = id2292out_output;

    id2293out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2293in_i));
  }
  { // Node ID: 3147 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3147in_input = id2293out_o;

    id3147out_output[(getCycle()+2)%3] = id3147in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id2296out_o;

  { // Node ID: 2296 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2296in_i = id2292out_output;

    id2296out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2296in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2368out_output;

  { // Node ID: 2368 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2368in_input = id2296out_o;

    id2368out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2368in_input))));
  }
  { // Node ID: 2369 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2369in_addr = id2368out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2369x_1;

    switch(((long)((id2369in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2369x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2369x_1 = (id2369sta_rom_store[(id2369in_addr.getValueAsLong())]);
        break;
      default:
        id2369x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2369out_dout[(getCycle()+2)%3] = (id2369x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2295out_o;

  { // Node ID: 2295 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2295in_i = id2292out_output;

    id2295out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2295in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2365out_output;

  { // Node ID: 2365 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2365in_input = id2295out_o;

    id2365out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2365in_input))));
  }
  { // Node ID: 2366 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2366in_addr = id2365out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2366x_1;

    switch(((long)((id2366in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2366x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2366x_1 = (id2366sta_rom_store[(id2366in_addr.getValueAsLong())]);
        break;
      default:
        id2366x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2366out_dout[(getCycle()+2)%3] = (id2366x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2294out_o;

  { // Node ID: 2294 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2294in_i = id2292out_output;

    id2294out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2294in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2362out_output;

  { // Node ID: 2362 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2362in_input = id2294out_o;

    id2362out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2362in_input))));
  }
  { // Node ID: 2363 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2363in_addr = id2362out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2363x_1;

    switch(((long)((id2363in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2363x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2363x_1 = (id2363sta_rom_store[(id2363in_addr.getValueAsLong())]);
        break;
      default:
        id2363x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2363out_dout[(getCycle()+2)%3] = (id2363x_1);
  }
  { // Node ID: 2300 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2297out_o;

  { // Node ID: 2297 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2297in_i = id2292out_output;

    id2297out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2297in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2299out_result;

  { // Node ID: 2299 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2299in_a = id2300out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2299in_b = id2297out_o;

    id2299out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2299in_a,id2299in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2301out_o;

  { // Node ID: 2301 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2301in_i = id2299out_result;

    id2301out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2301in_i));
  }
  { // Node ID: 3148 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3148in_input = id2301out_o;

    id3148out_output[(getCycle()+2)%3] = id3148in_input;
  }
  { // Node ID: 3664 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2285out_result;

  { // Node ID: 2285 (NodeGt)
    const HWFloat<11,45> &id2285in_a = id2276out_result;
    const HWFloat<11,45> &id2285in_b = id3664out_value;

    id2285out_result = (gt_float(id2285in_a,id2285in_b));
  }
  { // Node ID: 3150 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3150in_input = id2285out_result;

    id3150out_output[(getCycle()+6)%7] = id3150in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2286out_output;

  { // Node ID: 2286 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2286in_input = id2283out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2286in_input_doubt = id2283out_o_doubt;

    id2286out_output = id2286in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2287out_result;

  { // Node ID: 2287 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2287in_a = id3150out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2287in_b = id2286out_output;

    HWOffsetFix<1,0,UNSIGNED> id2287x_1;

    (id2287x_1) = (and_fixed(id2287in_a,id2287in_b));
    id2287out_result = (id2287x_1);
  }
  { // Node ID: 3151 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3151in_input = id2287out_result;

    id3151out_output[(getCycle()+2)%3] = id3151in_input;
  }
  { // Node ID: 3663 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2289out_result;

  { // Node ID: 2289 (NodeLt)
    const HWFloat<11,45> &id2289in_a = id2276out_result;
    const HWFloat<11,45> &id2289in_b = id3663out_value;

    id2289out_result = (lt_float(id2289in_a,id2289in_b));
  }
  { // Node ID: 3152 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3152in_input = id2289out_result;

    id3152out_output[(getCycle()+6)%7] = id3152in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2290out_output;

  { // Node ID: 2290 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2290in_input = id2283out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2290in_input_doubt = id2283out_o_doubt;

    id2290out_output = id2290in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2291out_result;

  { // Node ID: 2291 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2291in_a = id3152out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2291in_b = id2290out_output;

    HWOffsetFix<1,0,UNSIGNED> id2291x_1;

    (id2291x_1) = (and_fixed(id2291in_a,id2291in_b));
    id2291out_result = (id2291x_1);
  }
  { // Node ID: 3153 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3153in_input = id2291out_result;

    id3153out_output[(getCycle()+2)%3] = id3153in_input;
  }
  { // Node ID: 3659 (NodeConstantRawBits)
  }
  { // Node ID: 3658 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2489out_result;

  { // Node ID: 2489 (NodeAdd)
    const HWFloat<11,45> &id2489in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id2489in_b = id3658out_value;

    id2489out_result = (add_float(id2489in_a,id2489in_b));
  }
  HWFloat<11,45> id2491out_result;

  { // Node ID: 2491 (NodeMul)
    const HWFloat<11,45> &id2491in_a = id3659out_value;
    const HWFloat<11,45> &id2491in_b = id2489out_result;

    id2491out_result = (mul_float(id2491in_a,id2491in_b));
  }
  HWRawBits<11> id2568out_result;

  { // Node ID: 2568 (NodeSlice)
    const HWFloat<11,45> &id2568in_a = id2491out_result;

    id2568out_result = (slice<44,11>(id2568in_a));
  }
  { // Node ID: 2569 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2849out_result;

  { // Node ID: 2849 (NodeEqInlined)
    const HWRawBits<11> &id2849in_a = id2568out_result;
    const HWRawBits<11> &id2849in_b = id2569out_value;

    id2849out_result = (eq_bits(id2849in_a,id2849in_b));
  }
  HWRawBits<44> id2567out_result;

  { // Node ID: 2567 (NodeSlice)
    const HWFloat<11,45> &id2567in_a = id2491out_result;

    id2567out_result = (slice<0,44>(id2567in_a));
  }
  { // Node ID: 3657 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2850out_result;

  { // Node ID: 2850 (NodeNeqInlined)
    const HWRawBits<44> &id2850in_a = id2567out_result;
    const HWRawBits<44> &id2850in_b = id3657out_value;

    id2850out_result = (neq_bits(id2850in_a,id2850in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2573out_result;

  { // Node ID: 2573 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2573in_a = id2849out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2573in_b = id2850out_result;

    HWOffsetFix<1,0,UNSIGNED> id2573x_1;

    (id2573x_1) = (and_fixed(id2573in_a,id2573in_b));
    id2573out_result = (id2573x_1);
  }
  { // Node ID: 3167 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3167in_input = id2573out_result;

    id3167out_output[(getCycle()+8)%9] = id3167in_input;
  }
  { // Node ID: 2492 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2493out_output;
  HWOffsetFix<1,0,UNSIGNED> id2493out_output_doubt;

  { // Node ID: 2493 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2493in_input = id2491out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2493in_doubt = id2492out_value;

    id2493out_output = id2493in_input;
    id2493out_output_doubt = id2493in_doubt;
  }
  { // Node ID: 2494 (NodeCast)
    const HWFloat<11,45> &id2494in_i = id2493out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2494in_i_doubt = id2493out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2494x_1;

    id2494out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2494in_i,(&(id2494x_1))));
    id2494out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2494x_1),(c_hw_fix_4_0_uns_bits))),id2494in_i_doubt));
  }
  { // Node ID: 2497 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2496out_result;
  HWOffsetFix<1,0,UNSIGNED> id2496out_result_doubt;

  { // Node ID: 2496 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2496in_a = id2494out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2496in_a_doubt = id2494out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2496in_b = id2497out_value;

    HWOffsetFix<1,0,UNSIGNED> id2496x_1;

    id2496out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2496in_a,id2496in_b,(&(id2496x_1))));
    id2496out_result_doubt = (or_fixed((neq_fixed((id2496x_1),(c_hw_fix_1_0_uns_bits_1))),id2496in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2498out_o;
  HWOffsetFix<1,0,UNSIGNED> id2498out_o_doubt;

  { // Node ID: 2498 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2498in_i = id2496out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2498in_i_doubt = id2496out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2498x_1;

    id2498out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2498in_i,(&(id2498x_1))));
    id2498out_o_doubt = (or_fixed((neq_fixed((id2498x_1),(c_hw_fix_1_0_uns_bits_1))),id2498in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2507out_output;

  { // Node ID: 2507 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2507in_input = id2498out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2507in_input_doubt = id2498out_o_doubt;

    id2507out_output = id2507in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2508out_o;

  { // Node ID: 2508 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2508in_i = id2507out_output;

    id2508out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2508in_i));
  }
  { // Node ID: 3158 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3158in_input = id2508out_o;

    id3158out_output[(getCycle()+2)%3] = id3158in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id2511out_o;

  { // Node ID: 2511 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2511in_i = id2507out_output;

    id2511out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2511in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2583out_output;

  { // Node ID: 2583 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2583in_input = id2511out_o;

    id2583out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2583in_input))));
  }
  { // Node ID: 2584 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2584in_addr = id2583out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2584x_1;

    switch(((long)((id2584in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2584x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2584x_1 = (id2584sta_rom_store[(id2584in_addr.getValueAsLong())]);
        break;
      default:
        id2584x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2584out_dout[(getCycle()+2)%3] = (id2584x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2510out_o;

  { // Node ID: 2510 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2510in_i = id2507out_output;

    id2510out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2510in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2580out_output;

  { // Node ID: 2580 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2580in_input = id2510out_o;

    id2580out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2580in_input))));
  }
  { // Node ID: 2581 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2581in_addr = id2580out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2581x_1;

    switch(((long)((id2581in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2581x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2581x_1 = (id2581sta_rom_store[(id2581in_addr.getValueAsLong())]);
        break;
      default:
        id2581x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2581out_dout[(getCycle()+2)%3] = (id2581x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2509out_o;

  { // Node ID: 2509 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2509in_i = id2507out_output;

    id2509out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2509in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2577out_output;

  { // Node ID: 2577 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2577in_input = id2509out_o;

    id2577out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2577in_input))));
  }
  { // Node ID: 2578 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2578in_addr = id2577out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2578x_1;

    switch(((long)((id2578in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2578x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2578x_1 = (id2578sta_rom_store[(id2578in_addr.getValueAsLong())]);
        break;
      default:
        id2578x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2578out_dout[(getCycle()+2)%3] = (id2578x_1);
  }
  { // Node ID: 2515 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2512out_o;

  { // Node ID: 2512 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2512in_i = id2507out_output;

    id2512out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2512in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2514out_result;

  { // Node ID: 2514 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2514in_a = id2515out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2514in_b = id2512out_o;

    id2514out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2514in_a,id2514in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2516out_o;

  { // Node ID: 2516 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2516in_i = id2514out_result;

    id2516out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2516in_i));
  }
  { // Node ID: 3159 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3159in_input = id2516out_o;

    id3159out_output[(getCycle()+2)%3] = id3159in_input;
  }
  { // Node ID: 3655 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2500out_result;

  { // Node ID: 2500 (NodeGt)
    const HWFloat<11,45> &id2500in_a = id2491out_result;
    const HWFloat<11,45> &id2500in_b = id3655out_value;

    id2500out_result = (gt_float(id2500in_a,id2500in_b));
  }
  { // Node ID: 3161 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3161in_input = id2500out_result;

    id3161out_output[(getCycle()+6)%7] = id3161in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2501out_output;

  { // Node ID: 2501 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2501in_input = id2498out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2501in_input_doubt = id2498out_o_doubt;

    id2501out_output = id2501in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2502out_result;

  { // Node ID: 2502 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2502in_a = id3161out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2502in_b = id2501out_output;

    HWOffsetFix<1,0,UNSIGNED> id2502x_1;

    (id2502x_1) = (and_fixed(id2502in_a,id2502in_b));
    id2502out_result = (id2502x_1);
  }
  { // Node ID: 3162 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3162in_input = id2502out_result;

    id3162out_output[(getCycle()+2)%3] = id3162in_input;
  }
  { // Node ID: 3654 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2504out_result;

  { // Node ID: 2504 (NodeLt)
    const HWFloat<11,45> &id2504in_a = id2491out_result;
    const HWFloat<11,45> &id2504in_b = id3654out_value;

    id2504out_result = (lt_float(id2504in_a,id2504in_b));
  }
  { // Node ID: 3163 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3163in_input = id2504out_result;

    id3163out_output[(getCycle()+6)%7] = id3163in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2505out_output;

  { // Node ID: 2505 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2505in_input = id2498out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2505in_input_doubt = id2498out_o_doubt;

    id2505out_output = id2505in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2506out_result;

  { // Node ID: 2506 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2506in_a = id3163out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2506in_b = id2505out_output;

    HWOffsetFix<1,0,UNSIGNED> id2506x_1;

    (id2506x_1) = (and_fixed(id2506in_a,id2506in_b));
    id2506out_result = (id2506x_1);
  }
  { // Node ID: 3164 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3164in_input = id2506out_result;

    id3164out_output[(getCycle()+2)%3] = id3164in_input;
  }
  { // Node ID: 3650 (NodeConstantRawBits)
  }
  { // Node ID: 3649 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2590out_result;

  { // Node ID: 2590 (NodeAdd)
    const HWFloat<11,45> &id2590in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id2590in_b = id3649out_value;

    id2590out_result = (add_float(id2590in_a,id2590in_b));
  }
  HWFloat<11,45> id2592out_result;

  { // Node ID: 2592 (NodeMul)
    const HWFloat<11,45> &id2592in_a = id3650out_value;
    const HWFloat<11,45> &id2592in_b = id2590out_result;

    id2592out_result = (mul_float(id2592in_a,id2592in_b));
  }
  HWRawBits<11> id2669out_result;

  { // Node ID: 2669 (NodeSlice)
    const HWFloat<11,45> &id2669in_a = id2592out_result;

    id2669out_result = (slice<44,11>(id2669in_a));
  }
  { // Node ID: 2670 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2856out_result;

  { // Node ID: 2856 (NodeEqInlined)
    const HWRawBits<11> &id2856in_a = id2669out_result;
    const HWRawBits<11> &id2856in_b = id2670out_value;

    id2856out_result = (eq_bits(id2856in_a,id2856in_b));
  }
  HWRawBits<44> id2668out_result;

  { // Node ID: 2668 (NodeSlice)
    const HWFloat<11,45> &id2668in_a = id2592out_result;

    id2668out_result = (slice<0,44>(id2668in_a));
  }
  { // Node ID: 3648 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2857out_result;

  { // Node ID: 2857 (NodeNeqInlined)
    const HWRawBits<44> &id2857in_a = id2668out_result;
    const HWRawBits<44> &id2857in_b = id3648out_value;

    id2857out_result = (neq_bits(id2857in_a,id2857in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id2674out_result;

  { // Node ID: 2674 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2674in_a = id2856out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2674in_b = id2857out_result;

    HWOffsetFix<1,0,UNSIGNED> id2674x_1;

    (id2674x_1) = (and_fixed(id2674in_a,id2674in_b));
    id2674out_result = (id2674x_1);
  }
  { // Node ID: 3178 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3178in_input = id2674out_result;

    id3178out_output[(getCycle()+8)%9] = id3178in_input;
  }
  { // Node ID: 2593 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2594out_output;
  HWOffsetFix<1,0,UNSIGNED> id2594out_output_doubt;

  { // Node ID: 2594 (NodeDoubtBitOp)
    const HWFloat<11,45> &id2594in_input = id2592out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2594in_doubt = id2593out_value;

    id2594out_output = id2594in_input;
    id2594out_output_doubt = id2594in_doubt;
  }
  { // Node ID: 2595 (NodeCast)
    const HWFloat<11,45> &id2595in_i = id2594out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id2595in_i_doubt = id2594out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id2595x_1;

    id2595out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2595in_i,(&(id2595x_1))));
    id2595out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id2595x_1),(c_hw_fix_4_0_uns_bits))),id2595in_i_doubt));
  }
  { // Node ID: 2598 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id2597out_result;
  HWOffsetFix<1,0,UNSIGNED> id2597out_result_doubt;

  { // Node ID: 2597 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2597in_a = id2595out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2597in_a_doubt = id2595out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id2597in_b = id2598out_value;

    HWOffsetFix<1,0,UNSIGNED> id2597x_1;

    id2597out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id2597in_a,id2597in_b,(&(id2597x_1))));
    id2597out_result_doubt = (or_fixed((neq_fixed((id2597x_1),(c_hw_fix_1_0_uns_bits_1))),id2597in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2599out_o;
  HWOffsetFix<1,0,UNSIGNED> id2599out_o_doubt;

  { // Node ID: 2599 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id2599in_i = id2597out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id2599in_i_doubt = id2597out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id2599x_1;

    id2599out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id2599in_i,(&(id2599x_1))));
    id2599out_o_doubt = (or_fixed((neq_fixed((id2599x_1),(c_hw_fix_1_0_uns_bits_1))),id2599in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id2608out_output;

  { // Node ID: 2608 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2608in_input = id2599out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2608in_input_doubt = id2599out_o_doubt;

    id2608out_output = id2608in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id2609out_o;

  { // Node ID: 2609 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2609in_i = id2608out_output;

    id2609out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id2609in_i));
  }
  { // Node ID: 3169 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3169in_input = id2609out_o;

    id3169out_output[(getCycle()+2)%3] = id3169in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id2612out_o;

  { // Node ID: 2612 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2612in_i = id2608out_output;

    id2612out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id2612in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2684out_output;

  { // Node ID: 2684 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id2684in_input = id2612out_o;

    id2684out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2684in_input))));
  }
  { // Node ID: 2685 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2685in_addr = id2684out_output;

    HWOffsetFix<32,-45,UNSIGNED> id2685x_1;

    switch(((long)((id2685in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2685x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id2685x_1 = (id2685sta_rom_store[(id2685in_addr.getValueAsLong())]);
        break;
      default:
        id2685x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id2685out_dout[(getCycle()+2)%3] = (id2685x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id2611out_o;

  { // Node ID: 2611 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2611in_i = id2608out_output;

    id2611out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id2611in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id2681out_output;

  { // Node ID: 2681 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id2681in_input = id2611out_o;

    id2681out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id2681in_input))));
  }
  { // Node ID: 2682 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id2682in_addr = id2681out_output;

    HWOffsetFix<42,-45,UNSIGNED> id2682x_1;

    switch(((long)((id2682in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id2682x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id2682x_1 = (id2682sta_rom_store[(id2682in_addr.getValueAsLong())]);
        break;
      default:
        id2682x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id2682out_dout[(getCycle()+2)%3] = (id2682x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id2610out_o;

  { // Node ID: 2610 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2610in_i = id2608out_output;

    id2610out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id2610in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id2678out_output;

  { // Node ID: 2678 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id2678in_input = id2610out_o;

    id2678out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id2678in_input))));
  }
  { // Node ID: 2679 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id2679in_addr = id2678out_output;

    HWOffsetFix<45,-45,UNSIGNED> id2679x_1;

    switch(((long)((id2679in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id2679x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id2679x_1 = (id2679sta_rom_store[(id2679in_addr.getValueAsLong())]);
        break;
      default:
        id2679x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id2679out_dout[(getCycle()+2)%3] = (id2679x_1);
  }
  { // Node ID: 2616 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id2613out_o;

  { // Node ID: 2613 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2613in_i = id2608out_output;

    id2613out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id2613in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id2615out_result;

  { // Node ID: 2615 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id2615in_a = id2616out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id2615in_b = id2613out_o;

    id2615out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id2615in_a,id2615in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id2617out_o;

  { // Node ID: 2617 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id2617in_i = id2615out_result;

    id2617out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id2617in_i));
  }
  { // Node ID: 3170 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3170in_input = id2617out_o;

    id3170out_output[(getCycle()+2)%3] = id3170in_input;
  }
  { // Node ID: 3646 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2601out_result;

  { // Node ID: 2601 (NodeGt)
    const HWFloat<11,45> &id2601in_a = id2592out_result;
    const HWFloat<11,45> &id2601in_b = id3646out_value;

    id2601out_result = (gt_float(id2601in_a,id2601in_b));
  }
  { // Node ID: 3172 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3172in_input = id2601out_result;

    id3172out_output[(getCycle()+6)%7] = id3172in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2602out_output;

  { // Node ID: 2602 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2602in_input = id2599out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2602in_input_doubt = id2599out_o_doubt;

    id2602out_output = id2602in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2603out_result;

  { // Node ID: 2603 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2603in_a = id3172out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2603in_b = id2602out_output;

    HWOffsetFix<1,0,UNSIGNED> id2603x_1;

    (id2603x_1) = (and_fixed(id2603in_a,id2603in_b));
    id2603out_result = (id2603x_1);
  }
  { // Node ID: 3173 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3173in_input = id2603out_result;

    id3173out_output[(getCycle()+2)%3] = id3173in_input;
  }
  { // Node ID: 3645 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2605out_result;

  { // Node ID: 2605 (NodeLt)
    const HWFloat<11,45> &id2605in_a = id2592out_result;
    const HWFloat<11,45> &id2605in_b = id3645out_value;

    id2605out_result = (lt_float(id2605in_a,id2605in_b));
  }
  { // Node ID: 3174 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3174in_input = id2605out_result;

    id3174out_output[(getCycle()+6)%7] = id3174in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id2606out_output;

  { // Node ID: 2606 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id2606in_input = id2599out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id2606in_input_doubt = id2599out_o_doubt;

    id2606out_output = id2606in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id2607out_result;

  { // Node ID: 2607 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2607in_a = id3174out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id2607in_b = id2606out_output;

    HWOffsetFix<1,0,UNSIGNED> id2607x_1;

    (id2607x_1) = (and_fixed(id2607in_a,id2607in_b));
    id2607out_result = (id2607x_1);
  }
  { // Node ID: 3175 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3175in_input = id2607out_result;

    id3175out_output[(getCycle()+2)%3] = id3175in_input;
  }
  { // Node ID: 3642 (NodeConstantRawBits)
  }
  { // Node ID: 2863 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2863in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2863in_b = id3642out_value;

    id2863out_result[(getCycle()+1)%2] = (eq_fixed(id2863in_a,id2863in_b));
  }
  HWFloat<11,45> id3105out_output;

  { // Node ID: 3105 (NodeStreamOffset)
    const HWFloat<11,45> &id3105in_input = id3179out_output[getCycle()%2];

    id3105out_output = id3105in_input;
  }
  { // Node ID: 24 (NodeInputMappedReg)
  }
  { // Node ID: 25 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id25in_sel = id2863out_result[getCycle()%2];
    const HWFloat<11,45> &id25in_option0 = id3105out_output;
    const HWFloat<11,45> &id25in_option1 = id24out_x1_in;

    HWFloat<11,45> id25x_1;

    switch((id25in_sel.getValueAsLong())) {
      case 0l:
        id25x_1 = id25in_option0;
        break;
      case 1l:
        id25x_1 = id25in_option1;
        break;
      default:
        id25x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id25out_result[(getCycle()+1)%2] = (id25x_1);
  }
  { // Node ID: 3200 (NodeFIFO)
    const HWFloat<11,45> &id3200in_input = id25out_result[getCycle()%2];

    id3200out_output[(getCycle()+8)%9] = id3200in_input;
  }
  { // Node ID: 3641 (NodeConstantRawBits)
  }
  { // Node ID: 3640 (NodeConstantRawBits)
  }
  { // Node ID: 3639 (NodeConstantRawBits)
  }
  HWFloat<11,45> id47out_result;

  { // Node ID: 47 (NodeAdd)
    const HWFloat<11,45> &id47in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id47in_b = id3639out_value;

    id47out_result = (add_float(id47in_a,id47in_b));
  }
  HWFloat<11,45> id49out_result;

  { // Node ID: 49 (NodeMul)
    const HWFloat<11,45> &id49in_a = id3640out_value;
    const HWFloat<11,45> &id49in_b = id47out_result;

    id49out_result = (mul_float(id49in_a,id49in_b));
  }
  HWRawBits<11> id126out_result;

  { // Node ID: 126 (NodeSlice)
    const HWFloat<11,45> &id126in_a = id49out_result;

    id126out_result = (slice<44,11>(id126in_a));
  }
  { // Node ID: 127 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2864out_result;

  { // Node ID: 2864 (NodeEqInlined)
    const HWRawBits<11> &id2864in_a = id126out_result;
    const HWRawBits<11> &id2864in_b = id127out_value;

    id2864out_result = (eq_bits(id2864in_a,id2864in_b));
  }
  HWRawBits<44> id125out_result;

  { // Node ID: 125 (NodeSlice)
    const HWFloat<11,45> &id125in_a = id49out_result;

    id125out_result = (slice<0,44>(id125in_a));
  }
  { // Node ID: 3638 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2865out_result;

  { // Node ID: 2865 (NodeNeqInlined)
    const HWRawBits<44> &id2865in_a = id125out_result;
    const HWRawBits<44> &id2865in_b = id3638out_value;

    id2865out_result = (neq_bits(id2865in_a,id2865in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id131out_result;

  { // Node ID: 131 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id131in_a = id2864out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id131in_b = id2865out_result;

    HWOffsetFix<1,0,UNSIGNED> id131x_1;

    (id131x_1) = (and_fixed(id131in_a,id131in_b));
    id131out_result = (id131x_1);
  }
  { // Node ID: 3189 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3189in_input = id131out_result;

    id3189out_output[(getCycle()+8)%9] = id3189in_input;
  }
  { // Node ID: 50 (NodeConstantRawBits)
  }
  HWFloat<11,45> id51out_output;
  HWOffsetFix<1,0,UNSIGNED> id51out_output_doubt;

  { // Node ID: 51 (NodeDoubtBitOp)
    const HWFloat<11,45> &id51in_input = id49out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id51in_doubt = id50out_value;

    id51out_output = id51in_input;
    id51out_output_doubt = id51in_doubt;
  }
  { // Node ID: 52 (NodeCast)
    const HWFloat<11,45> &id52in_i = id51out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id52in_i_doubt = id51out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id52x_1;

    id52out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id52in_i,(&(id52x_1))));
    id52out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id52x_1),(c_hw_fix_4_0_uns_bits))),id52in_i_doubt));
  }
  { // Node ID: 55 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id54out_result;
  HWOffsetFix<1,0,UNSIGNED> id54out_result_doubt;

  { // Node ID: 54 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id54in_a = id52out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id54in_a_doubt = id52out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id54in_b = id55out_value;

    HWOffsetFix<1,0,UNSIGNED> id54x_1;

    id54out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id54in_a,id54in_b,(&(id54x_1))));
    id54out_result_doubt = (or_fixed((neq_fixed((id54x_1),(c_hw_fix_1_0_uns_bits_1))),id54in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id56out_o;
  HWOffsetFix<1,0,UNSIGNED> id56out_o_doubt;

  { // Node ID: 56 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id56in_i = id54out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id56in_i_doubt = id54out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id56x_1;

    id56out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id56in_i,(&(id56x_1))));
    id56out_o_doubt = (or_fixed((neq_fixed((id56x_1),(c_hw_fix_1_0_uns_bits_1))),id56in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id65out_output;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id65in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id65in_input_doubt = id56out_o_doubt;

    id65out_output = id65in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id66out_o;

  { // Node ID: 66 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id66in_i = id65out_output;

    id66out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id66in_i));
  }
  { // Node ID: 3180 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3180in_input = id66out_o;

    id3180out_output[(getCycle()+2)%3] = id3180in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id93out_o;

  { // Node ID: 93 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id93in_i = id3180out_output[getCycle()%3];

    id93out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id93in_i));
  }
  { // Node ID: 96 (NodeConstantRawBits)
  }
  { // Node ID: 3026 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id69out_o;

  { // Node ID: 69 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id69in_i = id65out_output;

    id69out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id69in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id141out_output;

  { // Node ID: 141 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id141in_input = id69out_o;

    id141out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id141in_input))));
  }
  { // Node ID: 142 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id142in_addr = id141out_output;

    HWOffsetFix<32,-45,UNSIGNED> id142x_1;

    switch(((long)((id142in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id142x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id142x_1 = (id142sta_rom_store[(id142in_addr.getValueAsLong())]);
        break;
      default:
        id142x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id142out_dout[(getCycle()+2)%3] = (id142x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id68out_o;

  { // Node ID: 68 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id68in_i = id65out_output;

    id68out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id68in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id138out_output;

  { // Node ID: 138 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id138in_input = id68out_o;

    id138out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id138in_input))));
  }
  { // Node ID: 139 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id139in_addr = id138out_output;

    HWOffsetFix<42,-45,UNSIGNED> id139x_1;

    switch(((long)((id139in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id139x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id139x_1 = (id139sta_rom_store[(id139in_addr.getValueAsLong())]);
        break;
      default:
        id139x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id139out_dout[(getCycle()+2)%3] = (id139x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id67out_o;

  { // Node ID: 67 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id67in_i = id65out_output;

    id67out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id67in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id135out_output;

  { // Node ID: 135 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id135in_input = id67out_o;

    id135out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id135in_input))));
  }
  { // Node ID: 136 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id136in_addr = id135out_output;

    HWOffsetFix<45,-45,UNSIGNED> id136x_1;

    switch(((long)((id136in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id136x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id136x_1 = (id136sta_rom_store[(id136in_addr.getValueAsLong())]);
        break;
      default:
        id136x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id136out_dout[(getCycle()+2)%3] = (id136x_1);
  }
  { // Node ID: 73 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id70out_o;

  { // Node ID: 70 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id70in_i = id65out_output;

    id70out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id70in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id72out_result;

  { // Node ID: 72 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id72in_a = id73out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id72in_b = id70out_o;

    id72out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id72in_a,id72in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id74out_o;

  { // Node ID: 74 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id74in_i = id72out_result;

    id74out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id74in_i));
  }
  { // Node ID: 3181 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3181in_input = id74out_o;

    id3181out_output[(getCycle()+2)%3] = id3181in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id75out_result;

  { // Node ID: 75 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id75in_a = id136out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id75in_b = id3181out_output[getCycle()%3];

    id75out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id75in_a,id75in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id76in_a = id3181out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id76in_b = id136out_dout[getCycle()%3];

    id76out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id76in_a,id76in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id77out_result;

  { // Node ID: 77 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id77in_a = id75out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id77in_b = id76out_result;

    id77out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id77in_a,id77in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id78out_o;

  { // Node ID: 78 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id78in_i = id77out_result;

    id78out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id78in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id79out_result;

  { // Node ID: 79 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id79in_a = id139out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id79in_b = id78out_o;

    id79out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id79in_a,id79in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id80out_result;

  { // Node ID: 80 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id80in_a = id78out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id80in_b = id139out_dout[getCycle()%3];

    id80out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id80in_a,id80in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id81out_result;

  { // Node ID: 81 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id81in_a = id79out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id81in_b = id80out_result;

    id81out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id81in_a,id81in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id82in_i = id81out_result;

    id82out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id82in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id83out_result;

  { // Node ID: 83 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id83in_a = id142out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id83in_b = id82out_o;

    id83out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id83in_a,id83in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id84out_result;

  { // Node ID: 84 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id84in_a = id82out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id84in_b = id142out_dout[getCycle()%3];

    id84out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id84in_a,id84in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id85out_result;

  { // Node ID: 85 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id85in_a = id83out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id85in_b = id84out_result;

    id85out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id85in_a,id85in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id86out_o;

  { // Node ID: 86 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id86in_i = id85out_result;

    id86out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id86in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id87out_o;

  { // Node ID: 87 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id87in_i = id86out_o;

    id87out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id87in_i));
  }
  { // Node ID: 3637 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2866out_result;

  { // Node ID: 2866 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2866in_a = id87out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2866in_b = id3637out_value;

    id2866out_result = (gte_fixed(id2866in_a,id2866in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3084out_result;

  { // Node ID: 3084 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3084in_a = id93out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3084in_b = id96out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3084in_c = id3026out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3084in_condb = id2866out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3084x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3084x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3084x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3084x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3084x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3084x_1 = id3084in_a;
        break;
      default:
        id3084x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3084in_condb.getValueAsLong())) {
      case 0l:
        id3084x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3084x_2 = id3084in_b;
        break;
      default:
        id3084x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3084x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3084x_3 = id3084in_c;
        break;
      default:
        id3084x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3084x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3084x_1),(id3084x_2))),(id3084x_3)));
    id3084out_result = (id3084x_4);
  }
  HWRawBits<1> id2867out_result;

  { // Node ID: 2867 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2867in_a = id3084out_result;

    id2867out_result = (slice<13,1>(id2867in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2868out_output;

  { // Node ID: 2868 (NodeReinterpret)
    const HWRawBits<1> &id2868in_input = id2867out_result;

    id2868out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2868in_input));
  }
  { // Node ID: 3636 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<11,45> &id58in_a = id49out_result;
    const HWFloat<11,45> &id58in_b = id3636out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 3183 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3183in_input = id58out_result;

    id3183out_output[(getCycle()+6)%7] = id3183in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id59out_output;

  { // Node ID: 59 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id59in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id59in_input_doubt = id56out_o_doubt;

    id59out_output = id59in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id60out_result;

  { // Node ID: 60 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_a = id3183out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id60in_b = id59out_output;

    HWOffsetFix<1,0,UNSIGNED> id60x_1;

    (id60x_1) = (and_fixed(id60in_a,id60in_b));
    id60out_result = (id60x_1);
  }
  { // Node ID: 3184 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3184in_input = id60out_result;

    id3184out_output[(getCycle()+2)%3] = id3184in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id102out_result;

  { // Node ID: 102 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id102in_a = id3184out_output[getCycle()%3];

    id102out_result = (not_fixed(id102in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id103out_result;

  { // Node ID: 103 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id103in_a = id2868out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id103in_b = id102out_result;

    HWOffsetFix<1,0,UNSIGNED> id103x_1;

    (id103x_1) = (and_fixed(id103in_a,id103in_b));
    id103out_result = (id103x_1);
  }
  { // Node ID: 3635 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id62out_result;

  { // Node ID: 62 (NodeLt)
    const HWFloat<11,45> &id62in_a = id49out_result;
    const HWFloat<11,45> &id62in_b = id3635out_value;

    id62out_result = (lt_float(id62in_a,id62in_b));
  }
  { // Node ID: 3185 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3185in_input = id62out_result;

    id3185out_output[(getCycle()+6)%7] = id3185in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id63out_output;

  { // Node ID: 63 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id63in_input = id56out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id63in_input_doubt = id56out_o_doubt;

    id63out_output = id63in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id64out_result;

  { // Node ID: 64 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id64in_a = id3185out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id64in_b = id63out_output;

    HWOffsetFix<1,0,UNSIGNED> id64x_1;

    (id64x_1) = (and_fixed(id64in_a,id64in_b));
    id64out_result = (id64x_1);
  }
  { // Node ID: 3186 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3186in_input = id64out_result;

    id3186out_output[(getCycle()+2)%3] = id3186in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id104out_result;

  { // Node ID: 104 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id104in_a = id103out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id104in_b = id3186out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id104x_1;

    (id104x_1) = (or_fixed(id104in_a,id104in_b));
    id104out_result = (id104x_1);
  }
  { // Node ID: 3634 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2869out_result;

  { // Node ID: 2869 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2869in_a = id3084out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2869in_b = id3634out_value;

    id2869out_result = (gte_fixed(id2869in_a,id2869in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id111out_result;

  { // Node ID: 111 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_a = id3186out_output[getCycle()%3];

    id111out_result = (not_fixed(id111in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id112out_result;

  { // Node ID: 112 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id112in_a = id2869out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id112in_b = id111out_result;

    HWOffsetFix<1,0,UNSIGNED> id112x_1;

    (id112x_1) = (and_fixed(id112in_a,id112in_b));
    id112out_result = (id112x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id113out_result;

  { // Node ID: 113 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id113in_a = id112out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id113in_b = id3184out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id113x_1;

    (id113x_1) = (or_fixed(id113in_a,id113in_b));
    id113out_result = (id113x_1);
  }
  HWRawBits<2> id114out_result;

  { // Node ID: 114 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id114in_in0 = id104out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id114in_in1 = id113out_result;

    id114out_result = (cat(id114in_in0,id114in_in1));
  }
  { // Node ID: 106 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id105out_o;

  { // Node ID: 105 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id105in_i = id3084out_result;

    id105out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id105in_i));
  }
  { // Node ID: 90 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id91out_result;

  { // Node ID: 91 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id91in_sel = id2866out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id91in_option0 = id87out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id91in_option1 = id90out_value;

    HWOffsetFix<45,-44,UNSIGNED> id91x_1;

    switch((id91in_sel.getValueAsLong())) {
      case 0l:
        id91x_1 = id91in_option0;
        break;
      case 1l:
        id91x_1 = id91in_option1;
        break;
      default:
        id91x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id91out_result = (id91x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id92out_o;

  { // Node ID: 92 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id92in_i = id91out_result;

    id92out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id92in_i));
  }
  HWRawBits<56> id107out_result;

  { // Node ID: 107 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id107in_in0 = id106out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id107in_in1 = id105out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id107in_in2 = id92out_o;

    id107out_result = (cat((cat(id107in_in0,id107in_in1)),id107in_in2));
  }
  HWFloat<11,45> id108out_output;

  { // Node ID: 108 (NodeReinterpret)
    const HWRawBits<56> &id108in_input = id107out_result;

    id108out_output = (cast_bits2float<11,45>(id108in_input));
  }
  { // Node ID: 115 (NodeConstantRawBits)
  }
  { // Node ID: 116 (NodeConstantRawBits)
  }
  { // Node ID: 118 (NodeConstantRawBits)
  }
  HWRawBits<56> id2870out_result;

  { // Node ID: 2870 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2870in_in0 = id115out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2870in_in1 = id116out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2870in_in2 = id118out_value;

    id2870out_result = (cat((cat(id2870in_in0,id2870in_in1)),id2870in_in2));
  }
  HWFloat<11,45> id120out_output;

  { // Node ID: 120 (NodeReinterpret)
    const HWRawBits<56> &id120in_input = id2870out_result;

    id120out_output = (cast_bits2float<11,45>(id120in_input));
  }
  { // Node ID: 2800 (NodeConstantRawBits)
  }
  HWFloat<11,45> id123out_result;

  { // Node ID: 123 (NodeMux)
    const HWRawBits<2> &id123in_sel = id114out_result;
    const HWFloat<11,45> &id123in_option0 = id108out_output;
    const HWFloat<11,45> &id123in_option1 = id120out_output;
    const HWFloat<11,45> &id123in_option2 = id2800out_value;
    const HWFloat<11,45> &id123in_option3 = id120out_output;

    HWFloat<11,45> id123x_1;

    switch((id123in_sel.getValueAsLong())) {
      case 0l:
        id123x_1 = id123in_option0;
        break;
      case 1l:
        id123x_1 = id123in_option1;
        break;
      case 2l:
        id123x_1 = id123in_option2;
        break;
      case 3l:
        id123x_1 = id123in_option3;
        break;
      default:
        id123x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id123out_result = (id123x_1);
  }
  { // Node ID: 3633 (NodeConstantRawBits)
  }
  HWFloat<11,45> id133out_result;

  { // Node ID: 133 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id133in_sel = id3189out_output[getCycle()%9];
    const HWFloat<11,45> &id133in_option0 = id123out_result;
    const HWFloat<11,45> &id133in_option1 = id3633out_value;

    HWFloat<11,45> id133x_1;

    switch((id133in_sel.getValueAsLong())) {
      case 0l:
        id133x_1 = id133in_option0;
        break;
      case 1l:
        id133x_1 = id133in_option1;
        break;
      default:
        id133x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id133out_result = (id133x_1);
  }
  HWFloat<11,45> id144out_result;

  { // Node ID: 144 (NodeMul)
    const HWFloat<11,45> &id144in_a = id3641out_value;
    const HWFloat<11,45> &id144in_b = id133out_result;

    id144out_result = (mul_float(id144in_a,id144in_b));
  }
  { // Node ID: 3632 (NodeConstantRawBits)
  }
  { // Node ID: 3631 (NodeConstantRawBits)
  }
  HWFloat<11,45> id146out_result;

  { // Node ID: 146 (NodeAdd)
    const HWFloat<11,45> &id146in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id146in_b = id3631out_value;

    id146out_result = (add_float(id146in_a,id146in_b));
  }
  HWFloat<11,45> id148out_result;

  { // Node ID: 148 (NodeMul)
    const HWFloat<11,45> &id148in_a = id3632out_value;
    const HWFloat<11,45> &id148in_b = id146out_result;

    id148out_result = (mul_float(id148in_a,id148in_b));
  }
  HWRawBits<11> id225out_result;

  { // Node ID: 225 (NodeSlice)
    const HWFloat<11,45> &id225in_a = id148out_result;

    id225out_result = (slice<44,11>(id225in_a));
  }
  { // Node ID: 226 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2871out_result;

  { // Node ID: 2871 (NodeEqInlined)
    const HWRawBits<11> &id2871in_a = id225out_result;
    const HWRawBits<11> &id2871in_b = id226out_value;

    id2871out_result = (eq_bits(id2871in_a,id2871in_b));
  }
  HWRawBits<44> id224out_result;

  { // Node ID: 224 (NodeSlice)
    const HWFloat<11,45> &id224in_a = id148out_result;

    id224out_result = (slice<0,44>(id224in_a));
  }
  { // Node ID: 3630 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2872out_result;

  { // Node ID: 2872 (NodeNeqInlined)
    const HWRawBits<44> &id2872in_a = id224out_result;
    const HWRawBits<44> &id2872in_b = id3630out_value;

    id2872out_result = (neq_bits(id2872in_a,id2872in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id230out_result;

  { // Node ID: 230 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id230in_a = id2871out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id230in_b = id2872out_result;

    HWOffsetFix<1,0,UNSIGNED> id230x_1;

    (id230x_1) = (and_fixed(id230in_a,id230in_b));
    id230out_result = (id230x_1);
  }
  { // Node ID: 3199 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3199in_input = id230out_result;

    id3199out_output[(getCycle()+8)%9] = id3199in_input;
  }
  { // Node ID: 149 (NodeConstantRawBits)
  }
  HWFloat<11,45> id150out_output;
  HWOffsetFix<1,0,UNSIGNED> id150out_output_doubt;

  { // Node ID: 150 (NodeDoubtBitOp)
    const HWFloat<11,45> &id150in_input = id148out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id150in_doubt = id149out_value;

    id150out_output = id150in_input;
    id150out_output_doubt = id150in_doubt;
  }
  { // Node ID: 151 (NodeCast)
    const HWFloat<11,45> &id151in_i = id150out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id151in_i_doubt = id150out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id151x_1;

    id151out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id151in_i,(&(id151x_1))));
    id151out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id151x_1),(c_hw_fix_4_0_uns_bits))),id151in_i_doubt));
  }
  { // Node ID: 154 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id153out_result;
  HWOffsetFix<1,0,UNSIGNED> id153out_result_doubt;

  { // Node ID: 153 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id153in_a = id151out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id153in_a_doubt = id151out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id153in_b = id154out_value;

    HWOffsetFix<1,0,UNSIGNED> id153x_1;

    id153out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id153in_a,id153in_b,(&(id153x_1))));
    id153out_result_doubt = (or_fixed((neq_fixed((id153x_1),(c_hw_fix_1_0_uns_bits_1))),id153in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id155out_o;
  HWOffsetFix<1,0,UNSIGNED> id155out_o_doubt;

  { // Node ID: 155 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id155in_i = id153out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id155in_i_doubt = id153out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id155x_1;

    id155out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id155in_i,(&(id155x_1))));
    id155out_o_doubt = (or_fixed((neq_fixed((id155x_1),(c_hw_fix_1_0_uns_bits_1))),id155in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id164out_output;

  { // Node ID: 164 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id164in_input = id155out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id164in_input_doubt = id155out_o_doubt;

    id164out_output = id164in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id165out_o;

  { // Node ID: 165 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id165in_i = id164out_output;

    id165out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id165in_i));
  }
  { // Node ID: 3190 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3190in_input = id165out_o;

    id3190out_output[(getCycle()+2)%3] = id3190in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id192out_o;

  { // Node ID: 192 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id192in_i = id3190out_output[getCycle()%3];

    id192out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id192in_i));
  }
  { // Node ID: 195 (NodeConstantRawBits)
  }
  { // Node ID: 3028 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id168out_o;

  { // Node ID: 168 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id168in_i = id164out_output;

    id168out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id168in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id240out_output;

  { // Node ID: 240 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id240in_input = id168out_o;

    id240out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id240in_input))));
  }
  { // Node ID: 241 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id241in_addr = id240out_output;

    HWOffsetFix<32,-45,UNSIGNED> id241x_1;

    switch(((long)((id241in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id241x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id241x_1 = (id241sta_rom_store[(id241in_addr.getValueAsLong())]);
        break;
      default:
        id241x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id241out_dout[(getCycle()+2)%3] = (id241x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id167out_o;

  { // Node ID: 167 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id167in_i = id164out_output;

    id167out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id167in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id237out_output;

  { // Node ID: 237 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id237in_input = id167out_o;

    id237out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id237in_input))));
  }
  { // Node ID: 238 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id238in_addr = id237out_output;

    HWOffsetFix<42,-45,UNSIGNED> id238x_1;

    switch(((long)((id238in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id238x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id238x_1 = (id238sta_rom_store[(id238in_addr.getValueAsLong())]);
        break;
      default:
        id238x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id238out_dout[(getCycle()+2)%3] = (id238x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id166out_o;

  { // Node ID: 166 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id166in_i = id164out_output;

    id166out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id166in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id234out_output;

  { // Node ID: 234 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id234in_input = id166out_o;

    id234out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id234in_input))));
  }
  { // Node ID: 235 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id235in_addr = id234out_output;

    HWOffsetFix<45,-45,UNSIGNED> id235x_1;

    switch(((long)((id235in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id235x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id235x_1 = (id235sta_rom_store[(id235in_addr.getValueAsLong())]);
        break;
      default:
        id235x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id235out_dout[(getCycle()+2)%3] = (id235x_1);
  }
  { // Node ID: 172 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id169out_o;

  { // Node ID: 169 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id169in_i = id164out_output;

    id169out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id169in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id171out_result;

  { // Node ID: 171 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id171in_a = id172out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id171in_b = id169out_o;

    id171out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id171in_a,id171in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id173out_o;

  { // Node ID: 173 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id173in_i = id171out_result;

    id173out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id173in_i));
  }
  { // Node ID: 3191 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3191in_input = id173out_o;

    id3191out_output[(getCycle()+2)%3] = id3191in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id174out_result;

  { // Node ID: 174 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id174in_a = id235out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id174in_b = id3191out_output[getCycle()%3];

    id174out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id174in_a,id174in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id175out_result;

  { // Node ID: 175 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id175in_a = id3191out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id175in_b = id235out_dout[getCycle()%3];

    id175out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id175in_a,id175in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id176out_result;

  { // Node ID: 176 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id176in_a = id174out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id176in_b = id175out_result;

    id176out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id176in_a,id176in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id177out_o;

  { // Node ID: 177 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id177in_i = id176out_result;

    id177out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id177in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id178out_result;

  { // Node ID: 178 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id178in_a = id238out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id178in_b = id177out_o;

    id178out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id178in_a,id178in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id179out_result;

  { // Node ID: 179 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id179in_a = id177out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id179in_b = id238out_dout[getCycle()%3];

    id179out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id179in_a,id179in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id180out_result;

  { // Node ID: 180 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id180in_a = id178out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id180in_b = id179out_result;

    id180out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id180in_a,id180in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id181out_o;

  { // Node ID: 181 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id181in_i = id180out_result;

    id181out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id181in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id182out_result;

  { // Node ID: 182 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id182in_a = id241out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id182in_b = id181out_o;

    id182out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id182in_a,id182in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id183out_result;

  { // Node ID: 183 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id183in_a = id181out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id183in_b = id241out_dout[getCycle()%3];

    id183out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id183in_a,id183in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id184out_result;

  { // Node ID: 184 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id184in_a = id182out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id184in_b = id183out_result;

    id184out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id184in_a,id184in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id185out_o;

  { // Node ID: 185 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id185in_i = id184out_result;

    id185out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id185in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id186out_o;

  { // Node ID: 186 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id186in_i = id185out_o;

    id186out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id186in_i));
  }
  { // Node ID: 3629 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2873out_result;

  { // Node ID: 2873 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2873in_a = id186out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2873in_b = id3629out_value;

    id2873out_result = (gte_fixed(id2873in_a,id2873in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3083out_result;

  { // Node ID: 3083 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3083in_a = id192out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3083in_b = id195out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3083in_c = id3028out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3083in_condb = id2873out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3083x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3083x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3083x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3083x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3083x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3083x_1 = id3083in_a;
        break;
      default:
        id3083x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3083in_condb.getValueAsLong())) {
      case 0l:
        id3083x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3083x_2 = id3083in_b;
        break;
      default:
        id3083x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3083x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3083x_3 = id3083in_c;
        break;
      default:
        id3083x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3083x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3083x_1),(id3083x_2))),(id3083x_3)));
    id3083out_result = (id3083x_4);
  }
  HWRawBits<1> id2874out_result;

  { // Node ID: 2874 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2874in_a = id3083out_result;

    id2874out_result = (slice<13,1>(id2874in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2875out_output;

  { // Node ID: 2875 (NodeReinterpret)
    const HWRawBits<1> &id2875in_input = id2874out_result;

    id2875out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2875in_input));
  }
  { // Node ID: 3628 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id157out_result;

  { // Node ID: 157 (NodeGt)
    const HWFloat<11,45> &id157in_a = id148out_result;
    const HWFloat<11,45> &id157in_b = id3628out_value;

    id157out_result = (gt_float(id157in_a,id157in_b));
  }
  { // Node ID: 3193 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3193in_input = id157out_result;

    id3193out_output[(getCycle()+6)%7] = id3193in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id158out_output;

  { // Node ID: 158 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id158in_input = id155out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id158in_input_doubt = id155out_o_doubt;

    id158out_output = id158in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id159out_result;

  { // Node ID: 159 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id159in_a = id3193out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id159in_b = id158out_output;

    HWOffsetFix<1,0,UNSIGNED> id159x_1;

    (id159x_1) = (and_fixed(id159in_a,id159in_b));
    id159out_result = (id159x_1);
  }
  { // Node ID: 3194 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3194in_input = id159out_result;

    id3194out_output[(getCycle()+2)%3] = id3194in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id201out_result;

  { // Node ID: 201 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id201in_a = id3194out_output[getCycle()%3];

    id201out_result = (not_fixed(id201in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id202out_result;

  { // Node ID: 202 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id202in_a = id2875out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id202in_b = id201out_result;

    HWOffsetFix<1,0,UNSIGNED> id202x_1;

    (id202x_1) = (and_fixed(id202in_a,id202in_b));
    id202out_result = (id202x_1);
  }
  { // Node ID: 3627 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id161out_result;

  { // Node ID: 161 (NodeLt)
    const HWFloat<11,45> &id161in_a = id148out_result;
    const HWFloat<11,45> &id161in_b = id3627out_value;

    id161out_result = (lt_float(id161in_a,id161in_b));
  }
  { // Node ID: 3195 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3195in_input = id161out_result;

    id3195out_output[(getCycle()+6)%7] = id3195in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id162out_output;

  { // Node ID: 162 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id162in_input = id155out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id162in_input_doubt = id155out_o_doubt;

    id162out_output = id162in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id163out_result;

  { // Node ID: 163 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id163in_a = id3195out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id163in_b = id162out_output;

    HWOffsetFix<1,0,UNSIGNED> id163x_1;

    (id163x_1) = (and_fixed(id163in_a,id163in_b));
    id163out_result = (id163x_1);
  }
  { // Node ID: 3196 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3196in_input = id163out_result;

    id3196out_output[(getCycle()+2)%3] = id3196in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id203out_result;

  { // Node ID: 203 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id203in_a = id202out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id203in_b = id3196out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id203x_1;

    (id203x_1) = (or_fixed(id203in_a,id203in_b));
    id203out_result = (id203x_1);
  }
  { // Node ID: 3626 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2876out_result;

  { // Node ID: 2876 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2876in_a = id3083out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2876in_b = id3626out_value;

    id2876out_result = (gte_fixed(id2876in_a,id2876in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id210out_result;

  { // Node ID: 210 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id210in_a = id3196out_output[getCycle()%3];

    id210out_result = (not_fixed(id210in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id211out_result;

  { // Node ID: 211 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id211in_a = id2876out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id211in_b = id210out_result;

    HWOffsetFix<1,0,UNSIGNED> id211x_1;

    (id211x_1) = (and_fixed(id211in_a,id211in_b));
    id211out_result = (id211x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id212out_result;

  { // Node ID: 212 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id212in_a = id211out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id212in_b = id3194out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id212x_1;

    (id212x_1) = (or_fixed(id212in_a,id212in_b));
    id212out_result = (id212x_1);
  }
  HWRawBits<2> id213out_result;

  { // Node ID: 213 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id213in_in0 = id203out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id213in_in1 = id212out_result;

    id213out_result = (cat(id213in_in0,id213in_in1));
  }
  { // Node ID: 205 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id204out_o;

  { // Node ID: 204 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id204in_i = id3083out_result;

    id204out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id204in_i));
  }
  { // Node ID: 189 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id190out_result;

  { // Node ID: 190 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id190in_sel = id2873out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id190in_option0 = id186out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id190in_option1 = id189out_value;

    HWOffsetFix<45,-44,UNSIGNED> id190x_1;

    switch((id190in_sel.getValueAsLong())) {
      case 0l:
        id190x_1 = id190in_option0;
        break;
      case 1l:
        id190x_1 = id190in_option1;
        break;
      default:
        id190x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id190out_result = (id190x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id191out_o;

  { // Node ID: 191 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id191in_i = id190out_result;

    id191out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id191in_i));
  }
  HWRawBits<56> id206out_result;

  { // Node ID: 206 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id206in_in0 = id205out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id206in_in1 = id204out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id206in_in2 = id191out_o;

    id206out_result = (cat((cat(id206in_in0,id206in_in1)),id206in_in2));
  }
  HWFloat<11,45> id207out_output;

  { // Node ID: 207 (NodeReinterpret)
    const HWRawBits<56> &id207in_input = id206out_result;

    id207out_output = (cast_bits2float<11,45>(id207in_input));
  }
  { // Node ID: 214 (NodeConstantRawBits)
  }
  { // Node ID: 215 (NodeConstantRawBits)
  }
  { // Node ID: 217 (NodeConstantRawBits)
  }
  HWRawBits<56> id2877out_result;

  { // Node ID: 2877 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2877in_in0 = id214out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2877in_in1 = id215out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2877in_in2 = id217out_value;

    id2877out_result = (cat((cat(id2877in_in0,id2877in_in1)),id2877in_in2));
  }
  HWFloat<11,45> id219out_output;

  { // Node ID: 219 (NodeReinterpret)
    const HWRawBits<56> &id219in_input = id2877out_result;

    id219out_output = (cast_bits2float<11,45>(id219in_input));
  }
  { // Node ID: 2801 (NodeConstantRawBits)
  }
  HWFloat<11,45> id222out_result;

  { // Node ID: 222 (NodeMux)
    const HWRawBits<2> &id222in_sel = id213out_result;
    const HWFloat<11,45> &id222in_option0 = id207out_output;
    const HWFloat<11,45> &id222in_option1 = id219out_output;
    const HWFloat<11,45> &id222in_option2 = id2801out_value;
    const HWFloat<11,45> &id222in_option3 = id219out_output;

    HWFloat<11,45> id222x_1;

    switch((id222in_sel.getValueAsLong())) {
      case 0l:
        id222x_1 = id222in_option0;
        break;
      case 1l:
        id222x_1 = id222in_option1;
        break;
      case 2l:
        id222x_1 = id222in_option2;
        break;
      case 3l:
        id222x_1 = id222in_option3;
        break;
      default:
        id222x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id222out_result = (id222x_1);
  }
  { // Node ID: 3625 (NodeConstantRawBits)
  }
  HWFloat<11,45> id232out_result;

  { // Node ID: 232 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id232in_sel = id3199out_output[getCycle()%9];
    const HWFloat<11,45> &id232in_option0 = id222out_result;
    const HWFloat<11,45> &id232in_option1 = id3625out_value;

    HWFloat<11,45> id232x_1;

    switch((id232in_sel.getValueAsLong())) {
      case 0l:
        id232x_1 = id232in_option0;
        break;
      case 1l:
        id232x_1 = id232in_option1;
        break;
      default:
        id232x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id232out_result = (id232x_1);
  }
  { // Node ID: 3624 (NodeConstantRawBits)
  }
  HWFloat<11,45> id243out_result;

  { // Node ID: 243 (NodeAdd)
    const HWFloat<11,45> &id243in_a = id232out_result;
    const HWFloat<11,45> &id243in_b = id3624out_value;

    id243out_result = (add_float(id243in_a,id243in_b));
  }
  HWFloat<11,45> id244out_result;

  { // Node ID: 244 (NodeDiv)
    const HWFloat<11,45> &id244in_a = id144out_result;
    const HWFloat<11,45> &id244in_b = id243out_result;

    id244out_result = (div_float(id244in_a,id244in_b));
  }
  HWFloat<11,45> id245out_result;

  { // Node ID: 245 (NodeMul)
    const HWFloat<11,45> &id245in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id245in_b = id244out_result;

    id245out_result = (mul_float(id245in_a,id245in_b));
  }
  { // Node ID: 3623 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1955out_result;

  { // Node ID: 1955 (NodeSub)
    const HWFloat<11,45> &id1955in_a = id3623out_value;
    const HWFloat<11,45> &id1955in_b = id3200out_output[getCycle()%9];

    id1955out_result = (sub_float(id1955in_a,id1955in_b));
  }
  HWFloat<11,45> id1956out_result;

  { // Node ID: 1956 (NodeMul)
    const HWFloat<11,45> &id1956in_a = id245out_result;
    const HWFloat<11,45> &id1956in_b = id1955out_result;

    id1956out_result = (mul_float(id1956in_a,id1956in_b));
  }
  { // Node ID: 3622 (NodeConstantRawBits)
  }
  { // Node ID: 3621 (NodeConstantRawBits)
  }
  { // Node ID: 3620 (NodeConstantRawBits)
  }
  HWFloat<11,45> id247out_result;

  { // Node ID: 247 (NodeAdd)
    const HWFloat<11,45> &id247in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id247in_b = id3620out_value;

    id247out_result = (add_float(id247in_a,id247in_b));
  }
  HWFloat<11,45> id249out_result;

  { // Node ID: 249 (NodeMul)
    const HWFloat<11,45> &id249in_a = id3621out_value;
    const HWFloat<11,45> &id249in_b = id247out_result;

    id249out_result = (mul_float(id249in_a,id249in_b));
  }
  HWRawBits<11> id326out_result;

  { // Node ID: 326 (NodeSlice)
    const HWFloat<11,45> &id326in_a = id249out_result;

    id326out_result = (slice<44,11>(id326in_a));
  }
  { // Node ID: 327 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2878out_result;

  { // Node ID: 2878 (NodeEqInlined)
    const HWRawBits<11> &id2878in_a = id326out_result;
    const HWRawBits<11> &id2878in_b = id327out_value;

    id2878out_result = (eq_bits(id2878in_a,id2878in_b));
  }
  HWRawBits<44> id325out_result;

  { // Node ID: 325 (NodeSlice)
    const HWFloat<11,45> &id325in_a = id249out_result;

    id325out_result = (slice<0,44>(id325in_a));
  }
  { // Node ID: 3619 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2879out_result;

  { // Node ID: 2879 (NodeNeqInlined)
    const HWRawBits<44> &id2879in_a = id325out_result;
    const HWRawBits<44> &id2879in_b = id3619out_value;

    id2879out_result = (neq_bits(id2879in_a,id2879in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id331out_result;

  { // Node ID: 331 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_a = id2878out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id331in_b = id2879out_result;

    HWOffsetFix<1,0,UNSIGNED> id331x_1;

    (id331x_1) = (and_fixed(id331in_a,id331in_b));
    id331out_result = (id331x_1);
  }
  { // Node ID: 3210 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3210in_input = id331out_result;

    id3210out_output[(getCycle()+8)%9] = id3210in_input;
  }
  { // Node ID: 250 (NodeConstantRawBits)
  }
  HWFloat<11,45> id251out_output;
  HWOffsetFix<1,0,UNSIGNED> id251out_output_doubt;

  { // Node ID: 251 (NodeDoubtBitOp)
    const HWFloat<11,45> &id251in_input = id249out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id251in_doubt = id250out_value;

    id251out_output = id251in_input;
    id251out_output_doubt = id251in_doubt;
  }
  { // Node ID: 252 (NodeCast)
    const HWFloat<11,45> &id252in_i = id251out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id252in_i_doubt = id251out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id252x_1;

    id252out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id252in_i,(&(id252x_1))));
    id252out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id252x_1),(c_hw_fix_4_0_uns_bits))),id252in_i_doubt));
  }
  { // Node ID: 255 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id254out_result;
  HWOffsetFix<1,0,UNSIGNED> id254out_result_doubt;

  { // Node ID: 254 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id254in_a = id252out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id254in_a_doubt = id252out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id254in_b = id255out_value;

    HWOffsetFix<1,0,UNSIGNED> id254x_1;

    id254out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id254in_a,id254in_b,(&(id254x_1))));
    id254out_result_doubt = (or_fixed((neq_fixed((id254x_1),(c_hw_fix_1_0_uns_bits_1))),id254in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id256out_o;
  HWOffsetFix<1,0,UNSIGNED> id256out_o_doubt;

  { // Node ID: 256 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id256in_i = id254out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id256in_i_doubt = id254out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id256x_1;

    id256out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id256in_i,(&(id256x_1))));
    id256out_o_doubt = (or_fixed((neq_fixed((id256x_1),(c_hw_fix_1_0_uns_bits_1))),id256in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id265out_output;

  { // Node ID: 265 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id265in_input = id256out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id265in_input_doubt = id256out_o_doubt;

    id265out_output = id265in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id266out_o;

  { // Node ID: 266 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id266in_i = id265out_output;

    id266out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id266in_i));
  }
  { // Node ID: 3201 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3201in_input = id266out_o;

    id3201out_output[(getCycle()+2)%3] = id3201in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id293out_o;

  { // Node ID: 293 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id293in_i = id3201out_output[getCycle()%3];

    id293out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id293in_i));
  }
  { // Node ID: 296 (NodeConstantRawBits)
  }
  { // Node ID: 3030 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id269out_o;

  { // Node ID: 269 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id269in_i = id265out_output;

    id269out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id269in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id341out_output;

  { // Node ID: 341 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id341in_input = id269out_o;

    id341out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id341in_input))));
  }
  { // Node ID: 342 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id342in_addr = id341out_output;

    HWOffsetFix<32,-45,UNSIGNED> id342x_1;

    switch(((long)((id342in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id342x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id342x_1 = (id342sta_rom_store[(id342in_addr.getValueAsLong())]);
        break;
      default:
        id342x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id342out_dout[(getCycle()+2)%3] = (id342x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id268out_o;

  { // Node ID: 268 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id268in_i = id265out_output;

    id268out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id268in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id338out_output;

  { // Node ID: 338 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id338in_input = id268out_o;

    id338out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id338in_input))));
  }
  { // Node ID: 339 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id339in_addr = id338out_output;

    HWOffsetFix<42,-45,UNSIGNED> id339x_1;

    switch(((long)((id339in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id339x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id339x_1 = (id339sta_rom_store[(id339in_addr.getValueAsLong())]);
        break;
      default:
        id339x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id339out_dout[(getCycle()+2)%3] = (id339x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id267out_o;

  { // Node ID: 267 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id267in_i = id265out_output;

    id267out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id267in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id335out_output;

  { // Node ID: 335 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id335in_input = id267out_o;

    id335out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id335in_input))));
  }
  { // Node ID: 336 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id336in_addr = id335out_output;

    HWOffsetFix<45,-45,UNSIGNED> id336x_1;

    switch(((long)((id336in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id336x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id336x_1 = (id336sta_rom_store[(id336in_addr.getValueAsLong())]);
        break;
      default:
        id336x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id336out_dout[(getCycle()+2)%3] = (id336x_1);
  }
  { // Node ID: 273 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id270out_o;

  { // Node ID: 270 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id270in_i = id265out_output;

    id270out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id270in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id272out_result;

  { // Node ID: 272 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id272in_a = id273out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id272in_b = id270out_o;

    id272out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id272in_a,id272in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id274out_o;

  { // Node ID: 274 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id274in_i = id272out_result;

    id274out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id274in_i));
  }
  { // Node ID: 3202 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3202in_input = id274out_o;

    id3202out_output[(getCycle()+2)%3] = id3202in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id275out_result;

  { // Node ID: 275 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id275in_a = id336out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id275in_b = id3202out_output[getCycle()%3];

    id275out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id275in_a,id275in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id276out_result;

  { // Node ID: 276 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id276in_a = id3202out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id276in_b = id336out_dout[getCycle()%3];

    id276out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id276in_a,id276in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id277out_result;

  { // Node ID: 277 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id277in_a = id275out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id277in_b = id276out_result;

    id277out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id277in_a,id277in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id278out_o;

  { // Node ID: 278 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id278in_i = id277out_result;

    id278out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id278in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id279out_result;

  { // Node ID: 279 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id279in_a = id339out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id279in_b = id278out_o;

    id279out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id279in_a,id279in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id280out_result;

  { // Node ID: 280 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id280in_a = id278out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id280in_b = id339out_dout[getCycle()%3];

    id280out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id280in_a,id280in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id281out_result;

  { // Node ID: 281 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id281in_a = id279out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id281in_b = id280out_result;

    id281out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id281in_a,id281in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id282out_o;

  { // Node ID: 282 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id282in_i = id281out_result;

    id282out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id282in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id283in_a = id342out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id283in_b = id282out_o;

    id283out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id283in_a,id283in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id284out_result;

  { // Node ID: 284 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id284in_a = id282out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id284in_b = id342out_dout[getCycle()%3];

    id284out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id284in_a,id284in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id285in_a = id283out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id285in_b = id284out_result;

    id285out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id285in_a,id285in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id286out_o;

  { // Node ID: 286 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id286in_i = id285out_result;

    id286out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id286in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id287out_o;

  { // Node ID: 287 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id287in_i = id286out_o;

    id287out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id287in_i));
  }
  { // Node ID: 3618 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2880out_result;

  { // Node ID: 2880 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2880in_a = id287out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2880in_b = id3618out_value;

    id2880out_result = (gte_fixed(id2880in_a,id2880in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3082out_result;

  { // Node ID: 3082 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3082in_a = id293out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3082in_b = id296out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3082in_c = id3030out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3082in_condb = id2880out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3082x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3082x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3082x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3082x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3082x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3082x_1 = id3082in_a;
        break;
      default:
        id3082x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3082in_condb.getValueAsLong())) {
      case 0l:
        id3082x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3082x_2 = id3082in_b;
        break;
      default:
        id3082x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3082x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3082x_3 = id3082in_c;
        break;
      default:
        id3082x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3082x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3082x_1),(id3082x_2))),(id3082x_3)));
    id3082out_result = (id3082x_4);
  }
  HWRawBits<1> id2881out_result;

  { // Node ID: 2881 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2881in_a = id3082out_result;

    id2881out_result = (slice<13,1>(id2881in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2882out_output;

  { // Node ID: 2882 (NodeReinterpret)
    const HWRawBits<1> &id2882in_input = id2881out_result;

    id2882out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2882in_input));
  }
  { // Node ID: 3617 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id258out_result;

  { // Node ID: 258 (NodeGt)
    const HWFloat<11,45> &id258in_a = id249out_result;
    const HWFloat<11,45> &id258in_b = id3617out_value;

    id258out_result = (gt_float(id258in_a,id258in_b));
  }
  { // Node ID: 3204 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3204in_input = id258out_result;

    id3204out_output[(getCycle()+6)%7] = id3204in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id259out_output;

  { // Node ID: 259 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id259in_input = id256out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id259in_input_doubt = id256out_o_doubt;

    id259out_output = id259in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id260out_result;

  { // Node ID: 260 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id260in_a = id3204out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id260in_b = id259out_output;

    HWOffsetFix<1,0,UNSIGNED> id260x_1;

    (id260x_1) = (and_fixed(id260in_a,id260in_b));
    id260out_result = (id260x_1);
  }
  { // Node ID: 3205 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3205in_input = id260out_result;

    id3205out_output[(getCycle()+2)%3] = id3205in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id302out_result;

  { // Node ID: 302 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id302in_a = id3205out_output[getCycle()%3];

    id302out_result = (not_fixed(id302in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id303out_result;

  { // Node ID: 303 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id303in_a = id2882out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id303in_b = id302out_result;

    HWOffsetFix<1,0,UNSIGNED> id303x_1;

    (id303x_1) = (and_fixed(id303in_a,id303in_b));
    id303out_result = (id303x_1);
  }
  { // Node ID: 3616 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id262out_result;

  { // Node ID: 262 (NodeLt)
    const HWFloat<11,45> &id262in_a = id249out_result;
    const HWFloat<11,45> &id262in_b = id3616out_value;

    id262out_result = (lt_float(id262in_a,id262in_b));
  }
  { // Node ID: 3206 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3206in_input = id262out_result;

    id3206out_output[(getCycle()+6)%7] = id3206in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id263out_output;

  { // Node ID: 263 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id263in_input = id256out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id263in_input_doubt = id256out_o_doubt;

    id263out_output = id263in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id264out_result;

  { // Node ID: 264 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id264in_a = id3206out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id264in_b = id263out_output;

    HWOffsetFix<1,0,UNSIGNED> id264x_1;

    (id264x_1) = (and_fixed(id264in_a,id264in_b));
    id264out_result = (id264x_1);
  }
  { // Node ID: 3207 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3207in_input = id264out_result;

    id3207out_output[(getCycle()+2)%3] = id3207in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id304out_result;

  { // Node ID: 304 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id304in_a = id303out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id304in_b = id3207out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id304x_1;

    (id304x_1) = (or_fixed(id304in_a,id304in_b));
    id304out_result = (id304x_1);
  }
  { // Node ID: 3615 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2883out_result;

  { // Node ID: 2883 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2883in_a = id3082out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2883in_b = id3615out_value;

    id2883out_result = (gte_fixed(id2883in_a,id2883in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id311out_result;

  { // Node ID: 311 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id311in_a = id3207out_output[getCycle()%3];

    id311out_result = (not_fixed(id311in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id312out_result;

  { // Node ID: 312 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id312in_a = id2883out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id312in_b = id311out_result;

    HWOffsetFix<1,0,UNSIGNED> id312x_1;

    (id312x_1) = (and_fixed(id312in_a,id312in_b));
    id312out_result = (id312x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_a = id312out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id313in_b = id3205out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id313x_1;

    (id313x_1) = (or_fixed(id313in_a,id313in_b));
    id313out_result = (id313x_1);
  }
  HWRawBits<2> id314out_result;

  { // Node ID: 314 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id314in_in0 = id304out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id314in_in1 = id313out_result;

    id314out_result = (cat(id314in_in0,id314in_in1));
  }
  { // Node ID: 306 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id305out_o;

  { // Node ID: 305 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id305in_i = id3082out_result;

    id305out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id305in_i));
  }
  { // Node ID: 290 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id291out_result;

  { // Node ID: 291 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id291in_sel = id2880out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id291in_option0 = id287out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id291in_option1 = id290out_value;

    HWOffsetFix<45,-44,UNSIGNED> id291x_1;

    switch((id291in_sel.getValueAsLong())) {
      case 0l:
        id291x_1 = id291in_option0;
        break;
      case 1l:
        id291x_1 = id291in_option1;
        break;
      default:
        id291x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id291out_result = (id291x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id292out_o;

  { // Node ID: 292 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id292in_i = id291out_result;

    id292out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id292in_i));
  }
  HWRawBits<56> id307out_result;

  { // Node ID: 307 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id307in_in0 = id306out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id307in_in1 = id305out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id307in_in2 = id292out_o;

    id307out_result = (cat((cat(id307in_in0,id307in_in1)),id307in_in2));
  }
  HWFloat<11,45> id308out_output;

  { // Node ID: 308 (NodeReinterpret)
    const HWRawBits<56> &id308in_input = id307out_result;

    id308out_output = (cast_bits2float<11,45>(id308in_input));
  }
  { // Node ID: 315 (NodeConstantRawBits)
  }
  { // Node ID: 316 (NodeConstantRawBits)
  }
  { // Node ID: 318 (NodeConstantRawBits)
  }
  HWRawBits<56> id2884out_result;

  { // Node ID: 2884 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2884in_in0 = id315out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2884in_in1 = id316out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2884in_in2 = id318out_value;

    id2884out_result = (cat((cat(id2884in_in0,id2884in_in1)),id2884in_in2));
  }
  HWFloat<11,45> id320out_output;

  { // Node ID: 320 (NodeReinterpret)
    const HWRawBits<56> &id320in_input = id2884out_result;

    id320out_output = (cast_bits2float<11,45>(id320in_input));
  }
  { // Node ID: 2802 (NodeConstantRawBits)
  }
  HWFloat<11,45> id323out_result;

  { // Node ID: 323 (NodeMux)
    const HWRawBits<2> &id323in_sel = id314out_result;
    const HWFloat<11,45> &id323in_option0 = id308out_output;
    const HWFloat<11,45> &id323in_option1 = id320out_output;
    const HWFloat<11,45> &id323in_option2 = id2802out_value;
    const HWFloat<11,45> &id323in_option3 = id320out_output;

    HWFloat<11,45> id323x_1;

    switch((id323in_sel.getValueAsLong())) {
      case 0l:
        id323x_1 = id323in_option0;
        break;
      case 1l:
        id323x_1 = id323in_option1;
        break;
      case 2l:
        id323x_1 = id323in_option2;
        break;
      case 3l:
        id323x_1 = id323in_option3;
        break;
      default:
        id323x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id323out_result = (id323x_1);
  }
  { // Node ID: 3614 (NodeConstantRawBits)
  }
  HWFloat<11,45> id333out_result;

  { // Node ID: 333 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id333in_sel = id3210out_output[getCycle()%9];
    const HWFloat<11,45> &id333in_option0 = id323out_result;
    const HWFloat<11,45> &id333in_option1 = id3614out_value;

    HWFloat<11,45> id333x_1;

    switch((id333in_sel.getValueAsLong())) {
      case 0l:
        id333x_1 = id333in_option0;
        break;
      case 1l:
        id333x_1 = id333in_option1;
        break;
      default:
        id333x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id333out_result = (id333x_1);
  }
  HWFloat<11,45> id344out_result;

  { // Node ID: 344 (NodeMul)
    const HWFloat<11,45> &id344in_a = id3622out_value;
    const HWFloat<11,45> &id344in_b = id333out_result;

    id344out_result = (mul_float(id344in_a,id344in_b));
  }
  { // Node ID: 3613 (NodeConstantRawBits)
  }
  { // Node ID: 3612 (NodeConstantRawBits)
  }
  HWFloat<11,45> id346out_result;

  { // Node ID: 346 (NodeAdd)
    const HWFloat<11,45> &id346in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id346in_b = id3612out_value;

    id346out_result = (add_float(id346in_a,id346in_b));
  }
  HWFloat<11,45> id348out_result;

  { // Node ID: 348 (NodeMul)
    const HWFloat<11,45> &id348in_a = id3613out_value;
    const HWFloat<11,45> &id348in_b = id346out_result;

    id348out_result = (mul_float(id348in_a,id348in_b));
  }
  HWRawBits<11> id425out_result;

  { // Node ID: 425 (NodeSlice)
    const HWFloat<11,45> &id425in_a = id348out_result;

    id425out_result = (slice<44,11>(id425in_a));
  }
  { // Node ID: 426 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2885out_result;

  { // Node ID: 2885 (NodeEqInlined)
    const HWRawBits<11> &id2885in_a = id425out_result;
    const HWRawBits<11> &id2885in_b = id426out_value;

    id2885out_result = (eq_bits(id2885in_a,id2885in_b));
  }
  HWRawBits<44> id424out_result;

  { // Node ID: 424 (NodeSlice)
    const HWFloat<11,45> &id424in_a = id348out_result;

    id424out_result = (slice<0,44>(id424in_a));
  }
  { // Node ID: 3611 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2886out_result;

  { // Node ID: 2886 (NodeNeqInlined)
    const HWRawBits<44> &id2886in_a = id424out_result;
    const HWRawBits<44> &id2886in_b = id3611out_value;

    id2886out_result = (neq_bits(id2886in_a,id2886in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id430out_result;

  { // Node ID: 430 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id430in_a = id2885out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id430in_b = id2886out_result;

    HWOffsetFix<1,0,UNSIGNED> id430x_1;

    (id430x_1) = (and_fixed(id430in_a,id430in_b));
    id430out_result = (id430x_1);
  }
  { // Node ID: 3220 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3220in_input = id430out_result;

    id3220out_output[(getCycle()+8)%9] = id3220in_input;
  }
  { // Node ID: 349 (NodeConstantRawBits)
  }
  HWFloat<11,45> id350out_output;
  HWOffsetFix<1,0,UNSIGNED> id350out_output_doubt;

  { // Node ID: 350 (NodeDoubtBitOp)
    const HWFloat<11,45> &id350in_input = id348out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id350in_doubt = id349out_value;

    id350out_output = id350in_input;
    id350out_output_doubt = id350in_doubt;
  }
  { // Node ID: 351 (NodeCast)
    const HWFloat<11,45> &id351in_i = id350out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id351in_i_doubt = id350out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id351x_1;

    id351out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id351in_i,(&(id351x_1))));
    id351out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id351x_1),(c_hw_fix_4_0_uns_bits))),id351in_i_doubt));
  }
  { // Node ID: 354 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id353out_result;
  HWOffsetFix<1,0,UNSIGNED> id353out_result_doubt;

  { // Node ID: 353 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id353in_a = id351out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id353in_a_doubt = id351out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id353in_b = id354out_value;

    HWOffsetFix<1,0,UNSIGNED> id353x_1;

    id353out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id353in_a,id353in_b,(&(id353x_1))));
    id353out_result_doubt = (or_fixed((neq_fixed((id353x_1),(c_hw_fix_1_0_uns_bits_1))),id353in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id355out_o;
  HWOffsetFix<1,0,UNSIGNED> id355out_o_doubt;

  { // Node ID: 355 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id355in_i = id353out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id355in_i_doubt = id353out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id355x_1;

    id355out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id355in_i,(&(id355x_1))));
    id355out_o_doubt = (or_fixed((neq_fixed((id355x_1),(c_hw_fix_1_0_uns_bits_1))),id355in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id364out_output;

  { // Node ID: 364 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id364in_input = id355out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id364in_input_doubt = id355out_o_doubt;

    id364out_output = id364in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id365out_o;

  { // Node ID: 365 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id365in_i = id364out_output;

    id365out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id365in_i));
  }
  { // Node ID: 3211 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3211in_input = id365out_o;

    id3211out_output[(getCycle()+2)%3] = id3211in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id392out_o;

  { // Node ID: 392 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id392in_i = id3211out_output[getCycle()%3];

    id392out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id392in_i));
  }
  { // Node ID: 395 (NodeConstantRawBits)
  }
  { // Node ID: 3032 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id368out_o;

  { // Node ID: 368 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id368in_i = id364out_output;

    id368out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id368in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id440out_output;

  { // Node ID: 440 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id440in_input = id368out_o;

    id440out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id440in_input))));
  }
  { // Node ID: 441 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id441in_addr = id440out_output;

    HWOffsetFix<32,-45,UNSIGNED> id441x_1;

    switch(((long)((id441in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id441x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id441x_1 = (id441sta_rom_store[(id441in_addr.getValueAsLong())]);
        break;
      default:
        id441x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id441out_dout[(getCycle()+2)%3] = (id441x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id367out_o;

  { // Node ID: 367 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id367in_i = id364out_output;

    id367out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id367in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id437out_output;

  { // Node ID: 437 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id437in_input = id367out_o;

    id437out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id437in_input))));
  }
  { // Node ID: 438 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id438in_addr = id437out_output;

    HWOffsetFix<42,-45,UNSIGNED> id438x_1;

    switch(((long)((id438in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id438x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id438x_1 = (id438sta_rom_store[(id438in_addr.getValueAsLong())]);
        break;
      default:
        id438x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id438out_dout[(getCycle()+2)%3] = (id438x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id366out_o;

  { // Node ID: 366 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id366in_i = id364out_output;

    id366out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id366in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id434out_output;

  { // Node ID: 434 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id434in_input = id366out_o;

    id434out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id434in_input))));
  }
  { // Node ID: 435 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id435in_addr = id434out_output;

    HWOffsetFix<45,-45,UNSIGNED> id435x_1;

    switch(((long)((id435in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id435x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id435x_1 = (id435sta_rom_store[(id435in_addr.getValueAsLong())]);
        break;
      default:
        id435x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id435out_dout[(getCycle()+2)%3] = (id435x_1);
  }
  { // Node ID: 372 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id369out_o;

  { // Node ID: 369 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id369in_i = id364out_output;

    id369out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id369in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id371out_result;

  { // Node ID: 371 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id371in_a = id372out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id371in_b = id369out_o;

    id371out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id371in_a,id371in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id373out_o;

  { // Node ID: 373 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id373in_i = id371out_result;

    id373out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id373in_i));
  }
  { // Node ID: 3212 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3212in_input = id373out_o;

    id3212out_output[(getCycle()+2)%3] = id3212in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id374out_result;

  { // Node ID: 374 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id374in_a = id435out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id374in_b = id3212out_output[getCycle()%3];

    id374out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id374in_a,id374in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id375out_result;

  { // Node ID: 375 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id375in_a = id3212out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id375in_b = id435out_dout[getCycle()%3];

    id375out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id375in_a,id375in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id376out_result;

  { // Node ID: 376 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id376in_a = id374out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id376in_b = id375out_result;

    id376out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id376in_a,id376in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id377out_o;

  { // Node ID: 377 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id377in_i = id376out_result;

    id377out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id377in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id378out_result;

  { // Node ID: 378 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id378in_a = id438out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id378in_b = id377out_o;

    id378out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id378in_a,id378in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id379out_result;

  { // Node ID: 379 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id379in_a = id377out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id379in_b = id438out_dout[getCycle()%3];

    id379out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id379in_a,id379in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id380out_result;

  { // Node ID: 380 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id380in_a = id378out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id380in_b = id379out_result;

    id380out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id380in_a,id380in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id381out_o;

  { // Node ID: 381 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id381in_i = id380out_result;

    id381out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id381in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id382out_result;

  { // Node ID: 382 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id382in_a = id441out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id382in_b = id381out_o;

    id382out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id382in_a,id382in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id383out_result;

  { // Node ID: 383 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id383in_a = id381out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id383in_b = id441out_dout[getCycle()%3];

    id383out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id383in_a,id383in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id384out_result;

  { // Node ID: 384 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id384in_a = id382out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id384in_b = id383out_result;

    id384out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id384in_a,id384in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id385out_o;

  { // Node ID: 385 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id385in_i = id384out_result;

    id385out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id385in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id386out_o;

  { // Node ID: 386 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id386in_i = id385out_o;

    id386out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id386in_i));
  }
  { // Node ID: 3610 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2887out_result;

  { // Node ID: 2887 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2887in_a = id386out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2887in_b = id3610out_value;

    id2887out_result = (gte_fixed(id2887in_a,id2887in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3081out_result;

  { // Node ID: 3081 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3081in_a = id392out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3081in_b = id395out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3081in_c = id3032out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3081in_condb = id2887out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3081x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3081x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3081x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3081x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3081x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3081x_1 = id3081in_a;
        break;
      default:
        id3081x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3081in_condb.getValueAsLong())) {
      case 0l:
        id3081x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3081x_2 = id3081in_b;
        break;
      default:
        id3081x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3081x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3081x_3 = id3081in_c;
        break;
      default:
        id3081x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3081x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3081x_1),(id3081x_2))),(id3081x_3)));
    id3081out_result = (id3081x_4);
  }
  HWRawBits<1> id2888out_result;

  { // Node ID: 2888 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2888in_a = id3081out_result;

    id2888out_result = (slice<13,1>(id2888in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2889out_output;

  { // Node ID: 2889 (NodeReinterpret)
    const HWRawBits<1> &id2889in_input = id2888out_result;

    id2889out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2889in_input));
  }
  { // Node ID: 3609 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeGt)
    const HWFloat<11,45> &id357in_a = id348out_result;
    const HWFloat<11,45> &id357in_b = id3609out_value;

    id357out_result = (gt_float(id357in_a,id357in_b));
  }
  { // Node ID: 3214 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3214in_input = id357out_result;

    id3214out_output[(getCycle()+6)%7] = id3214in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id358out_output;

  { // Node ID: 358 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id358in_input = id355out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id358in_input_doubt = id355out_o_doubt;

    id358out_output = id358in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id359out_result;

  { // Node ID: 359 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id359in_a = id3214out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id359in_b = id358out_output;

    HWOffsetFix<1,0,UNSIGNED> id359x_1;

    (id359x_1) = (and_fixed(id359in_a,id359in_b));
    id359out_result = (id359x_1);
  }
  { // Node ID: 3215 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3215in_input = id359out_result;

    id3215out_output[(getCycle()+2)%3] = id3215in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id401out_result;

  { // Node ID: 401 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_a = id3215out_output[getCycle()%3];

    id401out_result = (not_fixed(id401in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id402out_result;

  { // Node ID: 402 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_a = id2889out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id402in_b = id401out_result;

    HWOffsetFix<1,0,UNSIGNED> id402x_1;

    (id402x_1) = (and_fixed(id402in_a,id402in_b));
    id402out_result = (id402x_1);
  }
  { // Node ID: 3608 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id361out_result;

  { // Node ID: 361 (NodeLt)
    const HWFloat<11,45> &id361in_a = id348out_result;
    const HWFloat<11,45> &id361in_b = id3608out_value;

    id361out_result = (lt_float(id361in_a,id361in_b));
  }
  { // Node ID: 3216 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3216in_input = id361out_result;

    id3216out_output[(getCycle()+6)%7] = id3216in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id362out_output;

  { // Node ID: 362 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id362in_input = id355out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id362in_input_doubt = id355out_o_doubt;

    id362out_output = id362in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id363out_result;

  { // Node ID: 363 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id363in_a = id3216out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id363in_b = id362out_output;

    HWOffsetFix<1,0,UNSIGNED> id363x_1;

    (id363x_1) = (and_fixed(id363in_a,id363in_b));
    id363out_result = (id363x_1);
  }
  { // Node ID: 3217 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3217in_input = id363out_result;

    id3217out_output[(getCycle()+2)%3] = id3217in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id403out_result;

  { // Node ID: 403 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_a = id402out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id403in_b = id3217out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id403x_1;

    (id403x_1) = (or_fixed(id403in_a,id403in_b));
    id403out_result = (id403x_1);
  }
  { // Node ID: 3607 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2890out_result;

  { // Node ID: 2890 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2890in_a = id3081out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2890in_b = id3607out_value;

    id2890out_result = (gte_fixed(id2890in_a,id2890in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id410out_result;

  { // Node ID: 410 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id410in_a = id3217out_output[getCycle()%3];

    id410out_result = (not_fixed(id410in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id411out_result;

  { // Node ID: 411 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id411in_a = id2890out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id411in_b = id410out_result;

    HWOffsetFix<1,0,UNSIGNED> id411x_1;

    (id411x_1) = (and_fixed(id411in_a,id411in_b));
    id411out_result = (id411x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id412out_result;

  { // Node ID: 412 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id412in_a = id411out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id412in_b = id3215out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id412x_1;

    (id412x_1) = (or_fixed(id412in_a,id412in_b));
    id412out_result = (id412x_1);
  }
  HWRawBits<2> id413out_result;

  { // Node ID: 413 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id413in_in0 = id403out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id413in_in1 = id412out_result;

    id413out_result = (cat(id413in_in0,id413in_in1));
  }
  { // Node ID: 405 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id404out_o;

  { // Node ID: 404 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id404in_i = id3081out_result;

    id404out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id404in_i));
  }
  { // Node ID: 389 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id390out_result;

  { // Node ID: 390 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id390in_sel = id2887out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id390in_option0 = id386out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id390in_option1 = id389out_value;

    HWOffsetFix<45,-44,UNSIGNED> id390x_1;

    switch((id390in_sel.getValueAsLong())) {
      case 0l:
        id390x_1 = id390in_option0;
        break;
      case 1l:
        id390x_1 = id390in_option1;
        break;
      default:
        id390x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id390out_result = (id390x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id391out_o;

  { // Node ID: 391 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id391in_i = id390out_result;

    id391out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id391in_i));
  }
  HWRawBits<56> id406out_result;

  { // Node ID: 406 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id406in_in0 = id405out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id406in_in1 = id404out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id406in_in2 = id391out_o;

    id406out_result = (cat((cat(id406in_in0,id406in_in1)),id406in_in2));
  }
  HWFloat<11,45> id407out_output;

  { // Node ID: 407 (NodeReinterpret)
    const HWRawBits<56> &id407in_input = id406out_result;

    id407out_output = (cast_bits2float<11,45>(id407in_input));
  }
  { // Node ID: 414 (NodeConstantRawBits)
  }
  { // Node ID: 415 (NodeConstantRawBits)
  }
  { // Node ID: 417 (NodeConstantRawBits)
  }
  HWRawBits<56> id2891out_result;

  { // Node ID: 2891 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2891in_in0 = id414out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2891in_in1 = id415out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2891in_in2 = id417out_value;

    id2891out_result = (cat((cat(id2891in_in0,id2891in_in1)),id2891in_in2));
  }
  HWFloat<11,45> id419out_output;

  { // Node ID: 419 (NodeReinterpret)
    const HWRawBits<56> &id419in_input = id2891out_result;

    id419out_output = (cast_bits2float<11,45>(id419in_input));
  }
  { // Node ID: 2803 (NodeConstantRawBits)
  }
  HWFloat<11,45> id422out_result;

  { // Node ID: 422 (NodeMux)
    const HWRawBits<2> &id422in_sel = id413out_result;
    const HWFloat<11,45> &id422in_option0 = id407out_output;
    const HWFloat<11,45> &id422in_option1 = id419out_output;
    const HWFloat<11,45> &id422in_option2 = id2803out_value;
    const HWFloat<11,45> &id422in_option3 = id419out_output;

    HWFloat<11,45> id422x_1;

    switch((id422in_sel.getValueAsLong())) {
      case 0l:
        id422x_1 = id422in_option0;
        break;
      case 1l:
        id422x_1 = id422in_option1;
        break;
      case 2l:
        id422x_1 = id422in_option2;
        break;
      case 3l:
        id422x_1 = id422in_option3;
        break;
      default:
        id422x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id422out_result = (id422x_1);
  }
  { // Node ID: 3606 (NodeConstantRawBits)
  }
  HWFloat<11,45> id432out_result;

  { // Node ID: 432 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id432in_sel = id3220out_output[getCycle()%9];
    const HWFloat<11,45> &id432in_option0 = id422out_result;
    const HWFloat<11,45> &id432in_option1 = id3606out_value;

    HWFloat<11,45> id432x_1;

    switch((id432in_sel.getValueAsLong())) {
      case 0l:
        id432x_1 = id432in_option0;
        break;
      case 1l:
        id432x_1 = id432in_option1;
        break;
      default:
        id432x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id432out_result = (id432x_1);
  }
  { // Node ID: 3605 (NodeConstantRawBits)
  }
  HWFloat<11,45> id443out_result;

  { // Node ID: 443 (NodeAdd)
    const HWFloat<11,45> &id443in_a = id432out_result;
    const HWFloat<11,45> &id443in_b = id3605out_value;

    id443out_result = (add_float(id443in_a,id443in_b));
  }
  HWFloat<11,45> id444out_result;

  { // Node ID: 444 (NodeDiv)
    const HWFloat<11,45> &id444in_a = id344out_result;
    const HWFloat<11,45> &id444in_b = id443out_result;

    id444out_result = (div_float(id444in_a,id444in_b));
  }
  HWFloat<11,45> id445out_result;

  { // Node ID: 445 (NodeMul)
    const HWFloat<11,45> &id445in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id445in_b = id444out_result;

    id445out_result = (mul_float(id445in_a,id445in_b));
  }
  HWFloat<11,45> id1957out_result;

  { // Node ID: 1957 (NodeMul)
    const HWFloat<11,45> &id1957in_a = id445out_result;
    const HWFloat<11,45> &id1957in_b = id3200out_output[getCycle()%9];

    id1957out_result = (mul_float(id1957in_a,id1957in_b));
  }
  HWFloat<11,45> id1958out_result;

  { // Node ID: 1958 (NodeSub)
    const HWFloat<11,45> &id1958in_a = id1956out_result;
    const HWFloat<11,45> &id1958in_b = id1957out_result;

    id1958out_result = (sub_float(id1958in_a,id1958in_b));
  }
  HWFloat<11,45> id1959out_result;

  { // Node ID: 1959 (NodeAdd)
    const HWFloat<11,45> &id1959in_a = id3200out_output[getCycle()%9];
    const HWFloat<11,45> &id1959in_b = id1958out_result;

    id1959out_result = (add_float(id1959in_a,id1959in_b));
  }
  { // Node ID: 3179 (NodeFIFO)
    const HWFloat<11,45> &id3179in_input = id1959out_result;

    id3179out_output[(getCycle()+1)%2] = id3179in_input;
  }
  { // Node ID: 3604 (NodeConstantRawBits)
  }
  { // Node ID: 2892 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2892in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2892in_b = id3604out_value;

    id2892out_result[(getCycle()+1)%2] = (eq_fixed(id2892in_a,id2892in_b));
  }
  { // Node ID: 3603 (NodeConstantRawBits)
  }
  HWFloat<11,45> id447out_result;

  { // Node ID: 447 (NodeAdd)
    const HWFloat<11,45> &id447in_a = id3431out_output[getCycle()%8];
    const HWFloat<11,45> &id447in_b = id3603out_value;

    id447out_result = (add_float(id447in_a,id447in_b));
  }
  HWFloat<11,45> id448out_result;

  { // Node ID: 448 (NodeNeg)
    const HWFloat<11,45> &id448in_a = id447out_result;

    id448out_result = (neg_float(id448in_a));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id496out_o;

  { // Node ID: 496 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id496in_i = id3226out_output[getCycle()%3];

    id496out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id496in_i));
  }
  { // Node ID: 499 (NodeConstantRawBits)
  }
  { // Node ID: 3034 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id478out_result;

  { // Node ID: 478 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id478in_a = id539out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id478in_b = id3227out_output[getCycle()%3];

    id478out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id478in_a,id478in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id479out_result;

  { // Node ID: 479 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id479in_a = id3227out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id479in_b = id539out_dout[getCycle()%3];

    id479out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id479in_a,id479in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id480out_result;

  { // Node ID: 480 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id480in_a = id478out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id480in_b = id479out_result;

    id480out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id480in_a,id480in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id481out_o;

  { // Node ID: 481 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id481in_i = id480out_result;

    id481out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id481in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id482out_result;

  { // Node ID: 482 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id482in_a = id542out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id482in_b = id481out_o;

    id482out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id482in_a,id482in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id483out_result;

  { // Node ID: 483 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id483in_a = id481out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id483in_b = id542out_dout[getCycle()%3];

    id483out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id483in_a,id483in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id484out_result;

  { // Node ID: 484 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id484in_a = id482out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id484in_b = id483out_result;

    id484out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id484in_a,id484in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id485out_o;

  { // Node ID: 485 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id485in_i = id484out_result;

    id485out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id485in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id486out_result;

  { // Node ID: 486 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id486in_a = id545out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id486in_b = id485out_o;

    id486out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id486in_a,id486in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id487out_result;

  { // Node ID: 487 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id487in_a = id485out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id487in_b = id545out_dout[getCycle()%3];

    id487out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id487in_a,id487in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id488out_result;

  { // Node ID: 488 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id488in_a = id486out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id488in_b = id487out_result;

    id488out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id488in_a,id488in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id489out_o;

  { // Node ID: 489 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id489in_i = id488out_result;

    id489out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id489in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id490out_o;

  { // Node ID: 490 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id490in_i = id489out_o;

    id490out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id490in_i));
  }
  { // Node ID: 3599 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2895out_result;

  { // Node ID: 2895 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2895in_a = id490out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2895in_b = id3599out_value;

    id2895out_result = (gte_fixed(id2895in_a,id2895in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3080out_result;

  { // Node ID: 3080 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3080in_a = id496out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3080in_b = id499out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3080in_c = id3034out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3080in_condb = id2895out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3080x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3080x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3080x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3080x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3080x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3080x_1 = id3080in_a;
        break;
      default:
        id3080x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3080in_condb.getValueAsLong())) {
      case 0l:
        id3080x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3080x_2 = id3080in_b;
        break;
      default:
        id3080x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3080x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3080x_3 = id3080in_c;
        break;
      default:
        id3080x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3080x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3080x_1),(id3080x_2))),(id3080x_3)));
    id3080out_result = (id3080x_4);
  }
  HWRawBits<1> id2896out_result;

  { // Node ID: 2896 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2896in_a = id3080out_result;

    id2896out_result = (slice<13,1>(id2896in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2897out_output;

  { // Node ID: 2897 (NodeReinterpret)
    const HWRawBits<1> &id2897in_input = id2896out_result;

    id2897out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2897in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id505out_result;

  { // Node ID: 505 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id505in_a = id3230out_output[getCycle()%3];

    id505out_result = (not_fixed(id505in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id506out_result;

  { // Node ID: 506 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id506in_a = id2897out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id506in_b = id505out_result;

    HWOffsetFix<1,0,UNSIGNED> id506x_1;

    (id506x_1) = (and_fixed(id506in_a,id506in_b));
    id506out_result = (id506x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id507out_result;

  { // Node ID: 507 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id507in_a = id506out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id507in_b = id3232out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id507x_1;

    (id507x_1) = (or_fixed(id507in_a,id507in_b));
    id507out_result = (id507x_1);
  }
  { // Node ID: 3596 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2898out_result;

  { // Node ID: 2898 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2898in_a = id3080out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2898in_b = id3596out_value;

    id2898out_result = (gte_fixed(id2898in_a,id2898in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id514out_result;

  { // Node ID: 514 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id514in_a = id3232out_output[getCycle()%3];

    id514out_result = (not_fixed(id514in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id515out_result;

  { // Node ID: 515 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id515in_a = id2898out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id515in_b = id514out_result;

    HWOffsetFix<1,0,UNSIGNED> id515x_1;

    (id515x_1) = (and_fixed(id515in_a,id515in_b));
    id515out_result = (id515x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id516out_result;

  { // Node ID: 516 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id516in_a = id515out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id516in_b = id3230out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id516x_1;

    (id516x_1) = (or_fixed(id516in_a,id516in_b));
    id516out_result = (id516x_1);
  }
  HWRawBits<2> id517out_result;

  { // Node ID: 517 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id517in_in0 = id507out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id517in_in1 = id516out_result;

    id517out_result = (cat(id517in_in0,id517in_in1));
  }
  { // Node ID: 509 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id508out_o;

  { // Node ID: 508 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id508in_i = id3080out_result;

    id508out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id508in_i));
  }
  { // Node ID: 493 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id494out_result;

  { // Node ID: 494 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id494in_sel = id2895out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id494in_option0 = id490out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id494in_option1 = id493out_value;

    HWOffsetFix<45,-44,UNSIGNED> id494x_1;

    switch((id494in_sel.getValueAsLong())) {
      case 0l:
        id494x_1 = id494in_option0;
        break;
      case 1l:
        id494x_1 = id494in_option1;
        break;
      default:
        id494x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id494out_result = (id494x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id495out_o;

  { // Node ID: 495 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id495in_i = id494out_result;

    id495out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id495in_i));
  }
  HWRawBits<56> id510out_result;

  { // Node ID: 510 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id510in_in0 = id509out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id510in_in1 = id508out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id510in_in2 = id495out_o;

    id510out_result = (cat((cat(id510in_in0,id510in_in1)),id510in_in2));
  }
  HWFloat<11,45> id511out_output;

  { // Node ID: 511 (NodeReinterpret)
    const HWRawBits<56> &id511in_input = id510out_result;

    id511out_output = (cast_bits2float<11,45>(id511in_input));
  }
  { // Node ID: 518 (NodeConstantRawBits)
  }
  { // Node ID: 519 (NodeConstantRawBits)
  }
  { // Node ID: 521 (NodeConstantRawBits)
  }
  HWRawBits<56> id2899out_result;

  { // Node ID: 2899 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2899in_in0 = id518out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2899in_in1 = id519out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2899in_in2 = id521out_value;

    id2899out_result = (cat((cat(id2899in_in0,id2899in_in1)),id2899in_in2));
  }
  HWFloat<11,45> id523out_output;

  { // Node ID: 523 (NodeReinterpret)
    const HWRawBits<56> &id523in_input = id2899out_result;

    id523out_output = (cast_bits2float<11,45>(id523in_input));
  }
  { // Node ID: 2804 (NodeConstantRawBits)
  }
  HWFloat<11,45> id526out_result;

  { // Node ID: 526 (NodeMux)
    const HWRawBits<2> &id526in_sel = id517out_result;
    const HWFloat<11,45> &id526in_option0 = id511out_output;
    const HWFloat<11,45> &id526in_option1 = id523out_output;
    const HWFloat<11,45> &id526in_option2 = id2804out_value;
    const HWFloat<11,45> &id526in_option3 = id523out_output;

    HWFloat<11,45> id526x_1;

    switch((id526in_sel.getValueAsLong())) {
      case 0l:
        id526x_1 = id526in_option0;
        break;
      case 1l:
        id526x_1 = id526in_option1;
        break;
      case 2l:
        id526x_1 = id526in_option2;
        break;
      case 3l:
        id526x_1 = id526in_option3;
        break;
      default:
        id526x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id526out_result = (id526x_1);
  }
  { // Node ID: 3595 (NodeConstantRawBits)
  }
  HWFloat<11,45> id536out_result;

  { // Node ID: 536 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id536in_sel = id3235out_output[getCycle()%9];
    const HWFloat<11,45> &id536in_option0 = id526out_result;
    const HWFloat<11,45> &id536in_option1 = id3595out_value;

    HWFloat<11,45> id536x_1;

    switch((id536in_sel.getValueAsLong())) {
      case 0l:
        id536x_1 = id536in_option0;
        break;
      case 1l:
        id536x_1 = id536in_option1;
        break;
      default:
        id536x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id536out_result = (id536x_1);
  }
  { // Node ID: 3594 (NodeConstantRawBits)
  }
  HWFloat<11,45> id547out_result;

  { // Node ID: 547 (NodeSub)
    const HWFloat<11,45> &id547in_a = id536out_result;
    const HWFloat<11,45> &id547in_b = id3594out_value;

    id547out_result = (sub_float(id547in_a,id547in_b));
  }
  HWFloat<11,45> id548out_result;

  { // Node ID: 548 (NodeDiv)
    const HWFloat<11,45> &id548in_a = id448out_result;
    const HWFloat<11,45> &id548in_b = id547out_result;

    id548out_result = (div_float(id548in_a,id548in_b));
  }
  HWFloat<11,45> id549out_result;

  { // Node ID: 549 (NodeMul)
    const HWFloat<11,45> &id549in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id549in_b = id548out_result;

    id549out_result = (mul_float(id549in_a,id549in_b));
  }
  { // Node ID: 3593 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1961out_result;

  { // Node ID: 1961 (NodeSub)
    const HWFloat<11,45> &id1961in_a = id3593out_value;
    const HWFloat<11,45> &id1961in_b = id3236out_output[getCycle()%9];

    id1961out_result = (sub_float(id1961in_a,id1961in_b));
  }
  HWFloat<11,45> id1962out_result;

  { // Node ID: 1962 (NodeMul)
    const HWFloat<11,45> &id1962in_a = id549out_result;
    const HWFloat<11,45> &id1962in_b = id1961out_result;

    id1962out_result = (mul_float(id1962in_a,id1962in_b));
  }
  { // Node ID: 3592 (NodeConstantRawBits)
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id597out_o;

  { // Node ID: 597 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id597in_i = id3237out_output[getCycle()%3];

    id597out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id597in_i));
  }
  { // Node ID: 600 (NodeConstantRawBits)
  }
  { // Node ID: 3036 (NodeConstantRawBits)
  }
  HWOffsetFix<49,-48,UNSIGNED> id579out_result;

  { // Node ID: 579 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id579in_a = id640out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id579in_b = id3238out_output[getCycle()%3];

    id579out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id579in_a,id579in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id580out_result;

  { // Node ID: 580 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id580in_a = id3238out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id580in_b = id640out_dout[getCycle()%3];

    id580out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id580in_a,id580in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id581out_result;

  { // Node ID: 581 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id581in_a = id579out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id581in_b = id580out_result;

    id581out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id581in_a,id581in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id582out_o;

  { // Node ID: 582 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id582in_i = id581out_result;

    id582out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id582in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id583out_result;

  { // Node ID: 583 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id583in_a = id643out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id583in_b = id582out_o;

    id583out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id583in_a,id583in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id584out_result;

  { // Node ID: 584 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id584in_a = id582out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id584in_b = id643out_dout[getCycle()%3];

    id584out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id584in_a,id584in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id585out_result;

  { // Node ID: 585 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id585in_a = id583out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id585in_b = id584out_result;

    id585out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id585in_a,id585in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id586out_o;

  { // Node ID: 586 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id586in_i = id585out_result;

    id586out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id586in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id587out_result;

  { // Node ID: 587 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id587in_a = id646out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id587in_b = id586out_o;

    id587out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id587in_a,id587in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id588out_result;

  { // Node ID: 588 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id588in_a = id586out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id588in_b = id646out_dout[getCycle()%3];

    id588out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id588in_a,id588in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id589out_result;

  { // Node ID: 589 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id589in_a = id587out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id589in_b = id588out_result;

    id589out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id589in_a,id589in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id590out_o;

  { // Node ID: 590 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id590in_i = id589out_result;

    id590out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id590in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id591out_o;

  { // Node ID: 591 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id591in_i = id590out_o;

    id591out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id591in_i));
  }
  { // Node ID: 3588 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2902out_result;

  { // Node ID: 2902 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2902in_a = id591out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2902in_b = id3588out_value;

    id2902out_result = (gte_fixed(id2902in_a,id2902in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3079out_result;

  { // Node ID: 3079 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3079in_a = id597out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3079in_b = id600out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3079in_c = id3036out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3079in_condb = id2902out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3079x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3079x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3079x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3079x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3079x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3079x_1 = id3079in_a;
        break;
      default:
        id3079x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3079in_condb.getValueAsLong())) {
      case 0l:
        id3079x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3079x_2 = id3079in_b;
        break;
      default:
        id3079x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3079x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3079x_3 = id3079in_c;
        break;
      default:
        id3079x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3079x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3079x_1),(id3079x_2))),(id3079x_3)));
    id3079out_result = (id3079x_4);
  }
  HWRawBits<1> id2903out_result;

  { // Node ID: 2903 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2903in_a = id3079out_result;

    id2903out_result = (slice<13,1>(id2903in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2904out_output;

  { // Node ID: 2904 (NodeReinterpret)
    const HWRawBits<1> &id2904in_input = id2903out_result;

    id2904out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2904in_input));
  }
  HWOffsetFix<1,0,UNSIGNED> id606out_result;

  { // Node ID: 606 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id606in_a = id3241out_output[getCycle()%3];

    id606out_result = (not_fixed(id606in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id607out_result;

  { // Node ID: 607 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id607in_a = id2904out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id607in_b = id606out_result;

    HWOffsetFix<1,0,UNSIGNED> id607x_1;

    (id607x_1) = (and_fixed(id607in_a,id607in_b));
    id607out_result = (id607x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id608out_result;

  { // Node ID: 608 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id608in_a = id607out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id608in_b = id3243out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id608x_1;

    (id608x_1) = (or_fixed(id608in_a,id608in_b));
    id608out_result = (id608x_1);
  }
  { // Node ID: 3585 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2905out_result;

  { // Node ID: 2905 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2905in_a = id3079out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2905in_b = id3585out_value;

    id2905out_result = (gte_fixed(id2905in_a,id2905in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id615out_result;

  { // Node ID: 615 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id615in_a = id3243out_output[getCycle()%3];

    id615out_result = (not_fixed(id615in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id616out_result;

  { // Node ID: 616 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id616in_a = id2905out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id616in_b = id615out_result;

    HWOffsetFix<1,0,UNSIGNED> id616x_1;

    (id616x_1) = (and_fixed(id616in_a,id616in_b));
    id616out_result = (id616x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id617out_result;

  { // Node ID: 617 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id617in_a = id616out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id617in_b = id3241out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id617x_1;

    (id617x_1) = (or_fixed(id617in_a,id617in_b));
    id617out_result = (id617x_1);
  }
  HWRawBits<2> id618out_result;

  { // Node ID: 618 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id618in_in0 = id608out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id618in_in1 = id617out_result;

    id618out_result = (cat(id618in_in0,id618in_in1));
  }
  { // Node ID: 610 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id609out_o;

  { // Node ID: 609 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id609in_i = id3079out_result;

    id609out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id609in_i));
  }
  { // Node ID: 594 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id595out_result;

  { // Node ID: 595 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id595in_sel = id2902out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id595in_option0 = id591out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id595in_option1 = id594out_value;

    HWOffsetFix<45,-44,UNSIGNED> id595x_1;

    switch((id595in_sel.getValueAsLong())) {
      case 0l:
        id595x_1 = id595in_option0;
        break;
      case 1l:
        id595x_1 = id595in_option1;
        break;
      default:
        id595x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id595out_result = (id595x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id596out_o;

  { // Node ID: 596 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id596in_i = id595out_result;

    id596out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id596in_i));
  }
  HWRawBits<56> id611out_result;

  { // Node ID: 611 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id611in_in0 = id610out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id611in_in1 = id609out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id611in_in2 = id596out_o;

    id611out_result = (cat((cat(id611in_in0,id611in_in1)),id611in_in2));
  }
  HWFloat<11,45> id612out_output;

  { // Node ID: 612 (NodeReinterpret)
    const HWRawBits<56> &id612in_input = id611out_result;

    id612out_output = (cast_bits2float<11,45>(id612in_input));
  }
  { // Node ID: 619 (NodeConstantRawBits)
  }
  { // Node ID: 620 (NodeConstantRawBits)
  }
  { // Node ID: 622 (NodeConstantRawBits)
  }
  HWRawBits<56> id2906out_result;

  { // Node ID: 2906 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2906in_in0 = id619out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2906in_in1 = id620out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2906in_in2 = id622out_value;

    id2906out_result = (cat((cat(id2906in_in0,id2906in_in1)),id2906in_in2));
  }
  HWFloat<11,45> id624out_output;

  { // Node ID: 624 (NodeReinterpret)
    const HWRawBits<56> &id624in_input = id2906out_result;

    id624out_output = (cast_bits2float<11,45>(id624in_input));
  }
  { // Node ID: 2805 (NodeConstantRawBits)
  }
  HWFloat<11,45> id627out_result;

  { // Node ID: 627 (NodeMux)
    const HWRawBits<2> &id627in_sel = id618out_result;
    const HWFloat<11,45> &id627in_option0 = id612out_output;
    const HWFloat<11,45> &id627in_option1 = id624out_output;
    const HWFloat<11,45> &id627in_option2 = id2805out_value;
    const HWFloat<11,45> &id627in_option3 = id624out_output;

    HWFloat<11,45> id627x_1;

    switch((id627in_sel.getValueAsLong())) {
      case 0l:
        id627x_1 = id627in_option0;
        break;
      case 1l:
        id627x_1 = id627in_option1;
        break;
      case 2l:
        id627x_1 = id627in_option2;
        break;
      case 3l:
        id627x_1 = id627in_option3;
        break;
      default:
        id627x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id627out_result = (id627x_1);
  }
  { // Node ID: 3584 (NodeConstantRawBits)
  }
  HWFloat<11,45> id637out_result;

  { // Node ID: 637 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id637in_sel = id3246out_output[getCycle()%9];
    const HWFloat<11,45> &id637in_option0 = id627out_result;
    const HWFloat<11,45> &id637in_option1 = id3584out_value;

    HWFloat<11,45> id637x_1;

    switch((id637in_sel.getValueAsLong())) {
      case 0l:
        id637x_1 = id637in_option0;
        break;
      case 1l:
        id637x_1 = id637in_option1;
        break;
      default:
        id637x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id637out_result = (id637x_1);
  }
  HWFloat<11,45> id648out_result;

  { // Node ID: 648 (NodeMul)
    const HWFloat<11,45> &id648in_a = id3592out_value;
    const HWFloat<11,45> &id648in_b = id637out_result;

    id648out_result = (mul_float(id648in_a,id648in_b));
  }
  HWFloat<11,45> id649out_result;

  { // Node ID: 649 (NodeMul)
    const HWFloat<11,45> &id649in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id649in_b = id648out_result;

    id649out_result = (mul_float(id649in_a,id649in_b));
  }
  HWFloat<11,45> id1963out_result;

  { // Node ID: 1963 (NodeMul)
    const HWFloat<11,45> &id1963in_a = id649out_result;
    const HWFloat<11,45> &id1963in_b = id3236out_output[getCycle()%9];

    id1963out_result = (mul_float(id1963in_a,id1963in_b));
  }
  HWFloat<11,45> id1964out_result;

  { // Node ID: 1964 (NodeSub)
    const HWFloat<11,45> &id1964in_a = id1962out_result;
    const HWFloat<11,45> &id1964in_b = id1963out_result;

    id1964out_result = (sub_float(id1964in_a,id1964in_b));
  }
  HWFloat<11,45> id1965out_result;

  { // Node ID: 1965 (NodeAdd)
    const HWFloat<11,45> &id1965in_a = id3236out_output[getCycle()%9];
    const HWFloat<11,45> &id1965in_b = id1964out_result;

    id1965out_result = (add_float(id1965in_a,id1965in_b));
  }
  { // Node ID: 3224 (NodeFIFO)
    const HWFloat<11,45> &id3224in_input = id1965out_result;

    id3224out_output[(getCycle()+1)%2] = id3224in_input;
  }
  HWFloat<11,45> id3106out_output;

  { // Node ID: 3106 (NodeStreamOffset)
    const HWFloat<11,45> &id3106in_input = id3224out_output[getCycle()%2];

    id3106out_output = id3106in_input;
  }
  { // Node ID: 28 (NodeInputMappedReg)
  }
  { // Node ID: 29 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id29in_sel = id2892out_result[getCycle()%2];
    const HWFloat<11,45> &id29in_option0 = id3106out_output;
    const HWFloat<11,45> &id29in_option1 = id28out_m_in;

    HWFloat<11,45> id29x_1;

    switch((id29in_sel.getValueAsLong())) {
      case 0l:
        id29x_1 = id29in_option0;
        break;
      case 1l:
        id29x_1 = id29in_option1;
        break;
      default:
        id29x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id29out_result[(getCycle()+1)%2] = (id29x_1);
  }
  { // Node ID: 3236 (NodeFIFO)
    const HWFloat<11,45> &id3236in_input = id29out_result[getCycle()%2];

    id3236out_output[(getCycle()+8)%9] = id3236in_input;
  }
  { // Node ID: 3602 (NodeConstantRawBits)
  }
  { // Node ID: 3601 (NodeConstantRawBits)
  }
  HWFloat<11,45> id450out_result;

  { // Node ID: 450 (NodeAdd)
    const HWFloat<11,45> &id450in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id450in_b = id3601out_value;

    id450out_result = (add_float(id450in_a,id450in_b));
  }
  HWFloat<11,45> id452out_result;

  { // Node ID: 452 (NodeMul)
    const HWFloat<11,45> &id452in_a = id3602out_value;
    const HWFloat<11,45> &id452in_b = id450out_result;

    id452out_result = (mul_float(id452in_a,id452in_b));
  }
  HWRawBits<11> id529out_result;

  { // Node ID: 529 (NodeSlice)
    const HWFloat<11,45> &id529in_a = id452out_result;

    id529out_result = (slice<44,11>(id529in_a));
  }
  { // Node ID: 530 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2893out_result;

  { // Node ID: 2893 (NodeEqInlined)
    const HWRawBits<11> &id2893in_a = id529out_result;
    const HWRawBits<11> &id2893in_b = id530out_value;

    id2893out_result = (eq_bits(id2893in_a,id2893in_b));
  }
  HWRawBits<44> id528out_result;

  { // Node ID: 528 (NodeSlice)
    const HWFloat<11,45> &id528in_a = id452out_result;

    id528out_result = (slice<0,44>(id528in_a));
  }
  { // Node ID: 3600 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2894out_result;

  { // Node ID: 2894 (NodeNeqInlined)
    const HWRawBits<44> &id2894in_a = id528out_result;
    const HWRawBits<44> &id2894in_b = id3600out_value;

    id2894out_result = (neq_bits(id2894in_a,id2894in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id534out_result;

  { // Node ID: 534 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id534in_a = id2893out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id534in_b = id2894out_result;

    HWOffsetFix<1,0,UNSIGNED> id534x_1;

    (id534x_1) = (and_fixed(id534in_a,id534in_b));
    id534out_result = (id534x_1);
  }
  { // Node ID: 3235 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3235in_input = id534out_result;

    id3235out_output[(getCycle()+8)%9] = id3235in_input;
  }
  { // Node ID: 453 (NodeConstantRawBits)
  }
  HWFloat<11,45> id454out_output;
  HWOffsetFix<1,0,UNSIGNED> id454out_output_doubt;

  { // Node ID: 454 (NodeDoubtBitOp)
    const HWFloat<11,45> &id454in_input = id452out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id454in_doubt = id453out_value;

    id454out_output = id454in_input;
    id454out_output_doubt = id454in_doubt;
  }
  { // Node ID: 455 (NodeCast)
    const HWFloat<11,45> &id455in_i = id454out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id455in_i_doubt = id454out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id455x_1;

    id455out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id455in_i,(&(id455x_1))));
    id455out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id455x_1),(c_hw_fix_4_0_uns_bits))),id455in_i_doubt));
  }
  { // Node ID: 458 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id457out_result;
  HWOffsetFix<1,0,UNSIGNED> id457out_result_doubt;

  { // Node ID: 457 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id457in_a = id455out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id457in_a_doubt = id455out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id457in_b = id458out_value;

    HWOffsetFix<1,0,UNSIGNED> id457x_1;

    id457out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id457in_a,id457in_b,(&(id457x_1))));
    id457out_result_doubt = (or_fixed((neq_fixed((id457x_1),(c_hw_fix_1_0_uns_bits_1))),id457in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id459out_o;
  HWOffsetFix<1,0,UNSIGNED> id459out_o_doubt;

  { // Node ID: 459 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id459in_i = id457out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id459in_i_doubt = id457out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id459x_1;

    id459out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id459in_i,(&(id459x_1))));
    id459out_o_doubt = (or_fixed((neq_fixed((id459x_1),(c_hw_fix_1_0_uns_bits_1))),id459in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id468out_output;

  { // Node ID: 468 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id468in_input = id459out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id468in_input_doubt = id459out_o_doubt;

    id468out_output = id468in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id469out_o;

  { // Node ID: 469 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id469in_i = id468out_output;

    id469out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id469in_i));
  }
  { // Node ID: 3226 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3226in_input = id469out_o;

    id3226out_output[(getCycle()+2)%3] = id3226in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id472out_o;

  { // Node ID: 472 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id472in_i = id468out_output;

    id472out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id472in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id544out_output;

  { // Node ID: 544 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id544in_input = id472out_o;

    id544out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id544in_input))));
  }
  { // Node ID: 545 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id545in_addr = id544out_output;

    HWOffsetFix<32,-45,UNSIGNED> id545x_1;

    switch(((long)((id545in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id545x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id545x_1 = (id545sta_rom_store[(id545in_addr.getValueAsLong())]);
        break;
      default:
        id545x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id545out_dout[(getCycle()+2)%3] = (id545x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id471out_o;

  { // Node ID: 471 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id471in_i = id468out_output;

    id471out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id471in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id541out_output;

  { // Node ID: 541 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id541in_input = id471out_o;

    id541out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id541in_input))));
  }
  { // Node ID: 542 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id542in_addr = id541out_output;

    HWOffsetFix<42,-45,UNSIGNED> id542x_1;

    switch(((long)((id542in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id542x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id542x_1 = (id542sta_rom_store[(id542in_addr.getValueAsLong())]);
        break;
      default:
        id542x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id542out_dout[(getCycle()+2)%3] = (id542x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id470out_o;

  { // Node ID: 470 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id470in_i = id468out_output;

    id470out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id470in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id538out_output;

  { // Node ID: 538 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id538in_input = id470out_o;

    id538out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id538in_input))));
  }
  { // Node ID: 539 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id539in_addr = id538out_output;

    HWOffsetFix<45,-45,UNSIGNED> id539x_1;

    switch(((long)((id539in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id539x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id539x_1 = (id539sta_rom_store[(id539in_addr.getValueAsLong())]);
        break;
      default:
        id539x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id539out_dout[(getCycle()+2)%3] = (id539x_1);
  }
  { // Node ID: 476 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id473out_o;

  { // Node ID: 473 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id473in_i = id468out_output;

    id473out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id473in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id475out_result;

  { // Node ID: 475 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id475in_a = id476out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id475in_b = id473out_o;

    id475out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id475in_a,id475in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id477out_o;

  { // Node ID: 477 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id477in_i = id475out_result;

    id477out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id477in_i));
  }
  { // Node ID: 3227 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3227in_input = id477out_o;

    id3227out_output[(getCycle()+2)%3] = id3227in_input;
  }
  { // Node ID: 3598 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id461out_result;

  { // Node ID: 461 (NodeGt)
    const HWFloat<11,45> &id461in_a = id452out_result;
    const HWFloat<11,45> &id461in_b = id3598out_value;

    id461out_result = (gt_float(id461in_a,id461in_b));
  }
  { // Node ID: 3229 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3229in_input = id461out_result;

    id3229out_output[(getCycle()+6)%7] = id3229in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id462out_output;

  { // Node ID: 462 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id462in_input = id459out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id462in_input_doubt = id459out_o_doubt;

    id462out_output = id462in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id463out_result;

  { // Node ID: 463 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id463in_a = id3229out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id463in_b = id462out_output;

    HWOffsetFix<1,0,UNSIGNED> id463x_1;

    (id463x_1) = (and_fixed(id463in_a,id463in_b));
    id463out_result = (id463x_1);
  }
  { // Node ID: 3230 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3230in_input = id463out_result;

    id3230out_output[(getCycle()+2)%3] = id3230in_input;
  }
  { // Node ID: 3597 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id465out_result;

  { // Node ID: 465 (NodeLt)
    const HWFloat<11,45> &id465in_a = id452out_result;
    const HWFloat<11,45> &id465in_b = id3597out_value;

    id465out_result = (lt_float(id465in_a,id465in_b));
  }
  { // Node ID: 3231 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3231in_input = id465out_result;

    id3231out_output[(getCycle()+6)%7] = id3231in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id466out_output;

  { // Node ID: 466 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id466in_input = id459out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id466in_input_doubt = id459out_o_doubt;

    id466out_output = id466in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id467out_result;

  { // Node ID: 467 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id467in_a = id3231out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id467in_b = id466out_output;

    HWOffsetFix<1,0,UNSIGNED> id467x_1;

    (id467x_1) = (and_fixed(id467in_a,id467in_b));
    id467out_result = (id467x_1);
  }
  { // Node ID: 3232 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3232in_input = id467out_result;

    id3232out_output[(getCycle()+2)%3] = id3232in_input;
  }
  { // Node ID: 3591 (NodeConstantRawBits)
  }
  { // Node ID: 3590 (NodeConstantRawBits)
  }
  HWFloat<11,45> id551out_result;

  { // Node ID: 551 (NodeAdd)
    const HWFloat<11,45> &id551in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id551in_b = id3590out_value;

    id551out_result = (add_float(id551in_a,id551in_b));
  }
  HWFloat<11,45> id553out_result;

  { // Node ID: 553 (NodeMul)
    const HWFloat<11,45> &id553in_a = id3591out_value;
    const HWFloat<11,45> &id553in_b = id551out_result;

    id553out_result = (mul_float(id553in_a,id553in_b));
  }
  HWRawBits<11> id630out_result;

  { // Node ID: 630 (NodeSlice)
    const HWFloat<11,45> &id630in_a = id553out_result;

    id630out_result = (slice<44,11>(id630in_a));
  }
  { // Node ID: 631 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2900out_result;

  { // Node ID: 2900 (NodeEqInlined)
    const HWRawBits<11> &id2900in_a = id630out_result;
    const HWRawBits<11> &id2900in_b = id631out_value;

    id2900out_result = (eq_bits(id2900in_a,id2900in_b));
  }
  HWRawBits<44> id629out_result;

  { // Node ID: 629 (NodeSlice)
    const HWFloat<11,45> &id629in_a = id553out_result;

    id629out_result = (slice<0,44>(id629in_a));
  }
  { // Node ID: 3589 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2901out_result;

  { // Node ID: 2901 (NodeNeqInlined)
    const HWRawBits<44> &id2901in_a = id629out_result;
    const HWRawBits<44> &id2901in_b = id3589out_value;

    id2901out_result = (neq_bits(id2901in_a,id2901in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id635out_result;

  { // Node ID: 635 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id635in_a = id2900out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id635in_b = id2901out_result;

    HWOffsetFix<1,0,UNSIGNED> id635x_1;

    (id635x_1) = (and_fixed(id635in_a,id635in_b));
    id635out_result = (id635x_1);
  }
  { // Node ID: 3246 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3246in_input = id635out_result;

    id3246out_output[(getCycle()+8)%9] = id3246in_input;
  }
  { // Node ID: 554 (NodeConstantRawBits)
  }
  HWFloat<11,45> id555out_output;
  HWOffsetFix<1,0,UNSIGNED> id555out_output_doubt;

  { // Node ID: 555 (NodeDoubtBitOp)
    const HWFloat<11,45> &id555in_input = id553out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id555in_doubt = id554out_value;

    id555out_output = id555in_input;
    id555out_output_doubt = id555in_doubt;
  }
  { // Node ID: 556 (NodeCast)
    const HWFloat<11,45> &id556in_i = id555out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id556in_i_doubt = id555out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id556x_1;

    id556out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id556in_i,(&(id556x_1))));
    id556out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id556x_1),(c_hw_fix_4_0_uns_bits))),id556in_i_doubt));
  }
  { // Node ID: 559 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id558out_result;
  HWOffsetFix<1,0,UNSIGNED> id558out_result_doubt;

  { // Node ID: 558 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id558in_a = id556out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id558in_a_doubt = id556out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id558in_b = id559out_value;

    HWOffsetFix<1,0,UNSIGNED> id558x_1;

    id558out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id558in_a,id558in_b,(&(id558x_1))));
    id558out_result_doubt = (or_fixed((neq_fixed((id558x_1),(c_hw_fix_1_0_uns_bits_1))),id558in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id560out_o;
  HWOffsetFix<1,0,UNSIGNED> id560out_o_doubt;

  { // Node ID: 560 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id560in_i = id558out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id560in_i_doubt = id558out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id560x_1;

    id560out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id560in_i,(&(id560x_1))));
    id560out_o_doubt = (or_fixed((neq_fixed((id560x_1),(c_hw_fix_1_0_uns_bits_1))),id560in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id569out_output;

  { // Node ID: 569 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id569in_input = id560out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id569in_input_doubt = id560out_o_doubt;

    id569out_output = id569in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id570out_o;

  { // Node ID: 570 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id570in_i = id569out_output;

    id570out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id570in_i));
  }
  { // Node ID: 3237 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3237in_input = id570out_o;

    id3237out_output[(getCycle()+2)%3] = id3237in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id573out_o;

  { // Node ID: 573 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id573in_i = id569out_output;

    id573out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id573in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id645out_output;

  { // Node ID: 645 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id645in_input = id573out_o;

    id645out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id645in_input))));
  }
  { // Node ID: 646 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id646in_addr = id645out_output;

    HWOffsetFix<32,-45,UNSIGNED> id646x_1;

    switch(((long)((id646in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id646x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id646x_1 = (id646sta_rom_store[(id646in_addr.getValueAsLong())]);
        break;
      default:
        id646x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id646out_dout[(getCycle()+2)%3] = (id646x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id572out_o;

  { // Node ID: 572 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id572in_i = id569out_output;

    id572out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id572in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id642out_output;

  { // Node ID: 642 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id642in_input = id572out_o;

    id642out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id642in_input))));
  }
  { // Node ID: 643 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id643in_addr = id642out_output;

    HWOffsetFix<42,-45,UNSIGNED> id643x_1;

    switch(((long)((id643in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id643x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id643x_1 = (id643sta_rom_store[(id643in_addr.getValueAsLong())]);
        break;
      default:
        id643x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id643out_dout[(getCycle()+2)%3] = (id643x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id571out_o;

  { // Node ID: 571 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id571in_i = id569out_output;

    id571out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id571in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id639out_output;

  { // Node ID: 639 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id639in_input = id571out_o;

    id639out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id639in_input))));
  }
  { // Node ID: 640 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id640in_addr = id639out_output;

    HWOffsetFix<45,-45,UNSIGNED> id640x_1;

    switch(((long)((id640in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id640x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id640x_1 = (id640sta_rom_store[(id640in_addr.getValueAsLong())]);
        break;
      default:
        id640x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id640out_dout[(getCycle()+2)%3] = (id640x_1);
  }
  { // Node ID: 577 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id574out_o;

  { // Node ID: 574 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id574in_i = id569out_output;

    id574out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id574in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id576out_result;

  { // Node ID: 576 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id576in_a = id577out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id576in_b = id574out_o;

    id576out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id576in_a,id576in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id578out_o;

  { // Node ID: 578 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id578in_i = id576out_result;

    id578out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id578in_i));
  }
  { // Node ID: 3238 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3238in_input = id578out_o;

    id3238out_output[(getCycle()+2)%3] = id3238in_input;
  }
  { // Node ID: 3587 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id562out_result;

  { // Node ID: 562 (NodeGt)
    const HWFloat<11,45> &id562in_a = id553out_result;
    const HWFloat<11,45> &id562in_b = id3587out_value;

    id562out_result = (gt_float(id562in_a,id562in_b));
  }
  { // Node ID: 3240 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3240in_input = id562out_result;

    id3240out_output[(getCycle()+6)%7] = id3240in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id563out_output;

  { // Node ID: 563 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id563in_input = id560out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id563in_input_doubt = id560out_o_doubt;

    id563out_output = id563in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id564out_result;

  { // Node ID: 564 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id564in_a = id3240out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id564in_b = id563out_output;

    HWOffsetFix<1,0,UNSIGNED> id564x_1;

    (id564x_1) = (and_fixed(id564in_a,id564in_b));
    id564out_result = (id564x_1);
  }
  { // Node ID: 3241 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3241in_input = id564out_result;

    id3241out_output[(getCycle()+2)%3] = id3241in_input;
  }
  { // Node ID: 3586 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id566out_result;

  { // Node ID: 566 (NodeLt)
    const HWFloat<11,45> &id566in_a = id553out_result;
    const HWFloat<11,45> &id566in_b = id3586out_value;

    id566out_result = (lt_float(id566in_a,id566in_b));
  }
  { // Node ID: 3242 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3242in_input = id566out_result;

    id3242out_output[(getCycle()+6)%7] = id3242in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id567out_output;

  { // Node ID: 567 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id567in_input = id560out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id567in_input_doubt = id560out_o_doubt;

    id567out_output = id567in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id568out_result;

  { // Node ID: 568 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id568in_a = id3242out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id568in_b = id567out_output;

    HWOffsetFix<1,0,UNSIGNED> id568x_1;

    (id568x_1) = (and_fixed(id568in_a,id568in_b));
    id568out_result = (id568x_1);
  }
  { // Node ID: 3243 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3243in_input = id568out_result;

    id3243out_output[(getCycle()+2)%3] = id3243in_input;
  }
  { // Node ID: 3092 (NodePO2FPMult)
    const HWFloat<11,45> &id3092in_floatIn = id1965out_result;

    id3092out_floatOut[(getCycle()+1)%2] = (mul_float(id3092in_floatIn,(c_hw_flt_11_45_4_0val)));
  }
  { // Node ID: 3583 (NodeConstantRawBits)
  }
  { // Node ID: 2907 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2907in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2907in_b = id3583out_value;

    id2907out_result[(getCycle()+1)%2] = (eq_fixed(id2907in_a,id2907in_b));
  }
  HWFloat<11,45> id3107out_output;

  { // Node ID: 3107 (NodeStreamOffset)
    const HWFloat<11,45> &id3107in_input = id1971out_result;

    id3107out_output = id3107in_input;
  }
  { // Node ID: 36 (NodeInputMappedReg)
  }
  { // Node ID: 37 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id37in_sel = id2907out_result[getCycle()%2];
    const HWFloat<11,45> &id37in_option0 = id3107out_output;
    const HWFloat<11,45> &id37in_option1 = id36out_h_in;

    HWFloat<11,45> id37x_1;

    switch((id37in_sel.getValueAsLong())) {
      case 0l:
        id37x_1 = id37in_option0;
        break;
      case 1l:
        id37x_1 = id37in_option1;
        break;
      default:
        id37x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id37out_result[(getCycle()+1)%2] = (id37x_1);
  }
  { // Node ID: 3261 (NodeFIFO)
    const HWFloat<11,45> &id3261in_input = id37out_result[getCycle()%2];

    id3261out_output[(getCycle()+9)%10] = id3261in_input;
  }
  { // Node ID: 3581 (NodeConstantRawBits)
  }
  HWFloat<11,45> id651out_result;

  { // Node ID: 651 (NodeAdd)
    const HWFloat<11,45> &id651in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id651in_b = id3581out_value;

    id651out_result = (add_float(id651in_a,id651in_b));
  }
  { // Node ID: 3093 (NodePO2FPMult)
    const HWFloat<11,45> &id3093in_floatIn = id651out_result;

    id3093out_floatOut[(getCycle()+1)%2] = (mul_float(id3093in_floatIn,(c_hw_flt_11_45_n0_25val)));
  }
  HWRawBits<11> id730out_result;

  { // Node ID: 730 (NodeSlice)
    const HWFloat<11,45> &id730in_a = id3093out_floatOut[getCycle()%2];

    id730out_result = (slice<44,11>(id730in_a));
  }
  { // Node ID: 731 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2908out_result;

  { // Node ID: 2908 (NodeEqInlined)
    const HWRawBits<11> &id2908in_a = id730out_result;
    const HWRawBits<11> &id2908in_b = id731out_value;

    id2908out_result = (eq_bits(id2908in_a,id2908in_b));
  }
  HWRawBits<44> id729out_result;

  { // Node ID: 729 (NodeSlice)
    const HWFloat<11,45> &id729in_a = id3093out_floatOut[getCycle()%2];

    id729out_result = (slice<0,44>(id729in_a));
  }
  { // Node ID: 3580 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2909out_result;

  { // Node ID: 2909 (NodeNeqInlined)
    const HWRawBits<44> &id2909in_a = id729out_result;
    const HWRawBits<44> &id2909in_b = id3580out_value;

    id2909out_result = (neq_bits(id2909in_a,id2909in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id735out_result;

  { // Node ID: 735 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id735in_a = id2908out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id735in_b = id2909out_result;

    HWOffsetFix<1,0,UNSIGNED> id735x_1;

    (id735x_1) = (and_fixed(id735in_a,id735in_b));
    id735out_result = (id735x_1);
  }
  { // Node ID: 3260 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3260in_input = id735out_result;

    id3260out_output[(getCycle()+8)%9] = id3260in_input;
  }
  { // Node ID: 654 (NodeConstantRawBits)
  }
  HWFloat<11,45> id655out_output;
  HWOffsetFix<1,0,UNSIGNED> id655out_output_doubt;

  { // Node ID: 655 (NodeDoubtBitOp)
    const HWFloat<11,45> &id655in_input = id3093out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id655in_doubt = id654out_value;

    id655out_output = id655in_input;
    id655out_output_doubt = id655in_doubt;
  }
  { // Node ID: 656 (NodeCast)
    const HWFloat<11,45> &id656in_i = id655out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id656in_i_doubt = id655out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id656x_1;

    id656out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id656in_i,(&(id656x_1))));
    id656out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id656x_1),(c_hw_fix_4_0_uns_bits))),id656in_i_doubt));
  }
  { // Node ID: 659 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id658out_result;
  HWOffsetFix<1,0,UNSIGNED> id658out_result_doubt;

  { // Node ID: 658 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id658in_a = id656out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id658in_a_doubt = id656out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id658in_b = id659out_value;

    HWOffsetFix<1,0,UNSIGNED> id658x_1;

    id658out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id658in_a,id658in_b,(&(id658x_1))));
    id658out_result_doubt = (or_fixed((neq_fixed((id658x_1),(c_hw_fix_1_0_uns_bits_1))),id658in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id660out_o;
  HWOffsetFix<1,0,UNSIGNED> id660out_o_doubt;

  { // Node ID: 660 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id660in_i = id658out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id660in_i_doubt = id658out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id660x_1;

    id660out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id660in_i,(&(id660x_1))));
    id660out_o_doubt = (or_fixed((neq_fixed((id660x_1),(c_hw_fix_1_0_uns_bits_1))),id660in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id669out_output;

  { // Node ID: 669 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id669in_input = id660out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id669in_input_doubt = id660out_o_doubt;

    id669out_output = id669in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id670out_o;

  { // Node ID: 670 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id670in_i = id669out_output;

    id670out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id670in_i));
  }
  { // Node ID: 3251 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3251in_input = id670out_o;

    id3251out_output[(getCycle()+2)%3] = id3251in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id673out_o;

  { // Node ID: 673 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id673in_i = id669out_output;

    id673out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id673in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id745out_output;

  { // Node ID: 745 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id745in_input = id673out_o;

    id745out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id745in_input))));
  }
  { // Node ID: 746 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id746in_addr = id745out_output;

    HWOffsetFix<32,-45,UNSIGNED> id746x_1;

    switch(((long)((id746in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id746x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id746x_1 = (id746sta_rom_store[(id746in_addr.getValueAsLong())]);
        break;
      default:
        id746x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id746out_dout[(getCycle()+2)%3] = (id746x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id672out_o;

  { // Node ID: 672 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id672in_i = id669out_output;

    id672out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id672in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id742out_output;

  { // Node ID: 742 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id742in_input = id672out_o;

    id742out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id742in_input))));
  }
  { // Node ID: 743 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id743in_addr = id742out_output;

    HWOffsetFix<42,-45,UNSIGNED> id743x_1;

    switch(((long)((id743in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id743x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id743x_1 = (id743sta_rom_store[(id743in_addr.getValueAsLong())]);
        break;
      default:
        id743x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id743out_dout[(getCycle()+2)%3] = (id743x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id671out_o;

  { // Node ID: 671 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id671in_i = id669out_output;

    id671out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id671in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id739out_output;

  { // Node ID: 739 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id739in_input = id671out_o;

    id739out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id739in_input))));
  }
  { // Node ID: 740 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id740in_addr = id739out_output;

    HWOffsetFix<45,-45,UNSIGNED> id740x_1;

    switch(((long)((id740in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id740x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id740x_1 = (id740sta_rom_store[(id740in_addr.getValueAsLong())]);
        break;
      default:
        id740x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id740out_dout[(getCycle()+2)%3] = (id740x_1);
  }
  { // Node ID: 677 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id674out_o;

  { // Node ID: 674 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id674in_i = id669out_output;

    id674out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id674in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id676out_result;

  { // Node ID: 676 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id676in_a = id677out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id676in_b = id674out_o;

    id676out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id676in_a,id676in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id678out_o;

  { // Node ID: 678 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id678in_i = id676out_result;

    id678out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id678in_i));
  }
  { // Node ID: 3252 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3252in_input = id678out_o;

    id3252out_output[(getCycle()+2)%3] = id3252in_input;
  }
  { // Node ID: 3578 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id662out_result;

  { // Node ID: 662 (NodeGt)
    const HWFloat<11,45> &id662in_a = id3093out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id662in_b = id3578out_value;

    id662out_result = (gt_float(id662in_a,id662in_b));
  }
  { // Node ID: 3254 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3254in_input = id662out_result;

    id3254out_output[(getCycle()+6)%7] = id3254in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id663out_output;

  { // Node ID: 663 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id663in_input = id660out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id663in_input_doubt = id660out_o_doubt;

    id663out_output = id663in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id664out_result;

  { // Node ID: 664 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_a = id3254out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id664in_b = id663out_output;

    HWOffsetFix<1,0,UNSIGNED> id664x_1;

    (id664x_1) = (and_fixed(id664in_a,id664in_b));
    id664out_result = (id664x_1);
  }
  { // Node ID: 3255 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3255in_input = id664out_result;

    id3255out_output[(getCycle()+2)%3] = id3255in_input;
  }
  { // Node ID: 3577 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id666out_result;

  { // Node ID: 666 (NodeLt)
    const HWFloat<11,45> &id666in_a = id3093out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id666in_b = id3577out_value;

    id666out_result = (lt_float(id666in_a,id666in_b));
  }
  { // Node ID: 3256 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3256in_input = id666out_result;

    id3256out_output[(getCycle()+6)%7] = id3256in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id667out_output;

  { // Node ID: 667 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id667in_input = id660out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id667in_input_doubt = id660out_o_doubt;

    id667out_output = id667in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id668out_result;

  { // Node ID: 668 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id668in_a = id3256out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id668in_b = id667out_output;

    HWOffsetFix<1,0,UNSIGNED> id668x_1;

    (id668x_1) = (and_fixed(id668in_a,id668in_b));
    id668out_result = (id668x_1);
  }
  { // Node ID: 3257 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3257in_input = id668out_result;

    id3257out_output[(getCycle()+2)%3] = id3257in_input;
  }
  { // Node ID: 3572 (NodeConstantRawBits)
  }
  { // Node ID: 3571 (NodeConstantRawBits)
  }
  HWFloat<11,45> id751out_result;

  { // Node ID: 751 (NodeAdd)
    const HWFloat<11,45> &id751in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id751in_b = id3571out_value;

    id751out_result = (add_float(id751in_a,id751in_b));
  }
  HWFloat<11,45> id753out_result;

  { // Node ID: 753 (NodeMul)
    const HWFloat<11,45> &id753in_a = id3572out_value;
    const HWFloat<11,45> &id753in_b = id751out_result;

    id753out_result = (mul_float(id753in_a,id753in_b));
  }
  HWRawBits<11> id830out_result;

  { // Node ID: 830 (NodeSlice)
    const HWFloat<11,45> &id830in_a = id753out_result;

    id830out_result = (slice<44,11>(id830in_a));
  }
  { // Node ID: 831 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2915out_result;

  { // Node ID: 2915 (NodeEqInlined)
    const HWRawBits<11> &id2915in_a = id830out_result;
    const HWRawBits<11> &id2915in_b = id831out_value;

    id2915out_result = (eq_bits(id2915in_a,id2915in_b));
  }
  HWRawBits<44> id829out_result;

  { // Node ID: 829 (NodeSlice)
    const HWFloat<11,45> &id829in_a = id753out_result;

    id829out_result = (slice<0,44>(id829in_a));
  }
  { // Node ID: 3570 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2916out_result;

  { // Node ID: 2916 (NodeNeqInlined)
    const HWRawBits<44> &id2916in_a = id829out_result;
    const HWRawBits<44> &id2916in_b = id3570out_value;

    id2916out_result = (neq_bits(id2916in_a,id2916in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id835out_result;

  { // Node ID: 835 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id835in_a = id2915out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id835in_b = id2916out_result;

    HWOffsetFix<1,0,UNSIGNED> id835x_1;

    (id835x_1) = (and_fixed(id835in_a,id835in_b));
    id835out_result = (id835x_1);
  }
  { // Node ID: 3272 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3272in_input = id835out_result;

    id3272out_output[(getCycle()+8)%9] = id3272in_input;
  }
  { // Node ID: 754 (NodeConstantRawBits)
  }
  HWFloat<11,45> id755out_output;
  HWOffsetFix<1,0,UNSIGNED> id755out_output_doubt;

  { // Node ID: 755 (NodeDoubtBitOp)
    const HWFloat<11,45> &id755in_input = id753out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id755in_doubt = id754out_value;

    id755out_output = id755in_input;
    id755out_output_doubt = id755in_doubt;
  }
  { // Node ID: 756 (NodeCast)
    const HWFloat<11,45> &id756in_i = id755out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id756in_i_doubt = id755out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id756x_1;

    id756out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id756in_i,(&(id756x_1))));
    id756out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id756x_1),(c_hw_fix_4_0_uns_bits))),id756in_i_doubt));
  }
  { // Node ID: 759 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id758out_result;
  HWOffsetFix<1,0,UNSIGNED> id758out_result_doubt;

  { // Node ID: 758 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id758in_a = id756out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id758in_a_doubt = id756out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id758in_b = id759out_value;

    HWOffsetFix<1,0,UNSIGNED> id758x_1;

    id758out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id758in_a,id758in_b,(&(id758x_1))));
    id758out_result_doubt = (or_fixed((neq_fixed((id758x_1),(c_hw_fix_1_0_uns_bits_1))),id758in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id760out_o;
  HWOffsetFix<1,0,UNSIGNED> id760out_o_doubt;

  { // Node ID: 760 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id760in_i = id758out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id760in_i_doubt = id758out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id760x_1;

    id760out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id760in_i,(&(id760x_1))));
    id760out_o_doubt = (or_fixed((neq_fixed((id760x_1),(c_hw_fix_1_0_uns_bits_1))),id760in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id769out_output;

  { // Node ID: 769 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id769in_input = id760out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id769in_input_doubt = id760out_o_doubt;

    id769out_output = id769in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id770out_o;

  { // Node ID: 770 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id770in_i = id769out_output;

    id770out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id770in_i));
  }
  { // Node ID: 3263 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3263in_input = id770out_o;

    id3263out_output[(getCycle()+2)%3] = id3263in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id773out_o;

  { // Node ID: 773 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id773in_i = id769out_output;

    id773out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id773in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id845out_output;

  { // Node ID: 845 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id845in_input = id773out_o;

    id845out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id845in_input))));
  }
  { // Node ID: 846 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id846in_addr = id845out_output;

    HWOffsetFix<32,-45,UNSIGNED> id846x_1;

    switch(((long)((id846in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id846x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id846x_1 = (id846sta_rom_store[(id846in_addr.getValueAsLong())]);
        break;
      default:
        id846x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id846out_dout[(getCycle()+2)%3] = (id846x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id772out_o;

  { // Node ID: 772 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id772in_i = id769out_output;

    id772out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id772in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id842out_output;

  { // Node ID: 842 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id842in_input = id772out_o;

    id842out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id842in_input))));
  }
  { // Node ID: 843 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id843in_addr = id842out_output;

    HWOffsetFix<42,-45,UNSIGNED> id843x_1;

    switch(((long)((id843in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id843x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id843x_1 = (id843sta_rom_store[(id843in_addr.getValueAsLong())]);
        break;
      default:
        id843x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id843out_dout[(getCycle()+2)%3] = (id843x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id771out_o;

  { // Node ID: 771 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id771in_i = id769out_output;

    id771out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id771in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id839out_output;

  { // Node ID: 839 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id839in_input = id771out_o;

    id839out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id839in_input))));
  }
  { // Node ID: 840 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id840in_addr = id839out_output;

    HWOffsetFix<45,-45,UNSIGNED> id840x_1;

    switch(((long)((id840in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id840x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id840x_1 = (id840sta_rom_store[(id840in_addr.getValueAsLong())]);
        break;
      default:
        id840x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id840out_dout[(getCycle()+2)%3] = (id840x_1);
  }
  { // Node ID: 777 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id774out_o;

  { // Node ID: 774 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id774in_i = id769out_output;

    id774out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id774in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id776out_result;

  { // Node ID: 776 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id776in_a = id777out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id776in_b = id774out_o;

    id776out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id776in_a,id776in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id778out_o;

  { // Node ID: 778 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id778in_i = id776out_result;

    id778out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id778in_i));
  }
  { // Node ID: 3264 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3264in_input = id778out_o;

    id3264out_output[(getCycle()+2)%3] = id3264in_input;
  }
  { // Node ID: 3568 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id762out_result;

  { // Node ID: 762 (NodeGt)
    const HWFloat<11,45> &id762in_a = id753out_result;
    const HWFloat<11,45> &id762in_b = id3568out_value;

    id762out_result = (gt_float(id762in_a,id762in_b));
  }
  { // Node ID: 3266 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3266in_input = id762out_result;

    id3266out_output[(getCycle()+6)%7] = id3266in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id763out_output;

  { // Node ID: 763 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id763in_input = id760out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id763in_input_doubt = id760out_o_doubt;

    id763out_output = id763in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id764out_result;

  { // Node ID: 764 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id764in_a = id3266out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id764in_b = id763out_output;

    HWOffsetFix<1,0,UNSIGNED> id764x_1;

    (id764x_1) = (and_fixed(id764in_a,id764in_b));
    id764out_result = (id764x_1);
  }
  { // Node ID: 3267 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3267in_input = id764out_result;

    id3267out_output[(getCycle()+2)%3] = id3267in_input;
  }
  { // Node ID: 3567 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id766out_result;

  { // Node ID: 766 (NodeLt)
    const HWFloat<11,45> &id766in_a = id753out_result;
    const HWFloat<11,45> &id766in_b = id3567out_value;

    id766out_result = (lt_float(id766in_a,id766in_b));
  }
  { // Node ID: 3268 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3268in_input = id766out_result;

    id3268out_output[(getCycle()+6)%7] = id3268in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id767out_output;

  { // Node ID: 767 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id767in_input = id760out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id767in_input_doubt = id760out_o_doubt;

    id767out_output = id767in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id768out_result;

  { // Node ID: 768 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id768in_a = id3268out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id768in_b = id767out_output;

    HWOffsetFix<1,0,UNSIGNED> id768x_1;

    (id768x_1) = (and_fixed(id768in_a,id768in_b));
    id768out_result = (id768x_1);
  }
  { // Node ID: 3269 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3269in_input = id768out_result;

    id3269out_output[(getCycle()+2)%3] = id3269in_input;
  }
  { // Node ID: 3563 (NodeConstantRawBits)
  }
  { // Node ID: 2922 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2922in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2922in_b = id3563out_value;

    id2922out_result[(getCycle()+1)%2] = (eq_fixed(id2922in_a,id2922in_b));
  }
  HWFloat<11,45> id3108out_output;

  { // Node ID: 3108 (NodeStreamOffset)
    const HWFloat<11,45> &id3108in_input = id1977out_result;

    id3108out_output = id3108in_input;
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 45 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id45in_sel = id2922out_result[getCycle()%2];
    const HWFloat<11,45> &id45in_option0 = id3108out_output;
    const HWFloat<11,45> &id45in_option1 = id44out_j_in;

    HWFloat<11,45> id45x_1;

    switch((id45in_sel.getValueAsLong())) {
      case 0l:
        id45x_1 = id45in_option0;
        break;
      case 1l:
        id45x_1 = id45in_option1;
        break;
      default:
        id45x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id45out_result[(getCycle()+1)%2] = (id45x_1);
  }
  { // Node ID: 3296 (NodeFIFO)
    const HWFloat<11,45> &id3296in_input = id45out_result[getCycle()%2];

    id3296out_output[(getCycle()+9)%10] = id3296in_input;
  }
  { // Node ID: 3561 (NodeConstantRawBits)
  }
  HWFloat<11,45> id853out_result;

  { // Node ID: 853 (NodeAdd)
    const HWFloat<11,45> &id853in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id853in_b = id3561out_value;

    id853out_result = (add_float(id853in_a,id853in_b));
  }
  { // Node ID: 3094 (NodePO2FPMult)
    const HWFloat<11,45> &id3094in_floatIn = id853out_result;

    id3094out_floatOut[(getCycle()+1)%2] = (mul_float(id3094in_floatIn,(c_hw_flt_11_45_n0_25val)));
  }
  HWRawBits<11> id932out_result;

  { // Node ID: 932 (NodeSlice)
    const HWFloat<11,45> &id932in_a = id3094out_floatOut[getCycle()%2];

    id932out_result = (slice<44,11>(id932in_a));
  }
  { // Node ID: 933 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2923out_result;

  { // Node ID: 2923 (NodeEqInlined)
    const HWRawBits<11> &id2923in_a = id932out_result;
    const HWRawBits<11> &id2923in_b = id933out_value;

    id2923out_result = (eq_bits(id2923in_a,id2923in_b));
  }
  HWRawBits<44> id931out_result;

  { // Node ID: 931 (NodeSlice)
    const HWFloat<11,45> &id931in_a = id3094out_floatOut[getCycle()%2];

    id931out_result = (slice<0,44>(id931in_a));
  }
  { // Node ID: 3560 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2924out_result;

  { // Node ID: 2924 (NodeNeqInlined)
    const HWRawBits<44> &id2924in_a = id931out_result;
    const HWRawBits<44> &id2924in_b = id3560out_value;

    id2924out_result = (neq_bits(id2924in_a,id2924in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id937out_result;

  { // Node ID: 937 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id937in_a = id2923out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id937in_b = id2924out_result;

    HWOffsetFix<1,0,UNSIGNED> id937x_1;

    (id937x_1) = (and_fixed(id937in_a,id937in_b));
    id937out_result = (id937x_1);
  }
  { // Node ID: 3284 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3284in_input = id937out_result;

    id3284out_output[(getCycle()+8)%9] = id3284in_input;
  }
  { // Node ID: 856 (NodeConstantRawBits)
  }
  HWFloat<11,45> id857out_output;
  HWOffsetFix<1,0,UNSIGNED> id857out_output_doubt;

  { // Node ID: 857 (NodeDoubtBitOp)
    const HWFloat<11,45> &id857in_input = id3094out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id857in_doubt = id856out_value;

    id857out_output = id857in_input;
    id857out_output_doubt = id857in_doubt;
  }
  { // Node ID: 858 (NodeCast)
    const HWFloat<11,45> &id858in_i = id857out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id858in_i_doubt = id857out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id858x_1;

    id858out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id858in_i,(&(id858x_1))));
    id858out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id858x_1),(c_hw_fix_4_0_uns_bits))),id858in_i_doubt));
  }
  { // Node ID: 861 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id860out_result;
  HWOffsetFix<1,0,UNSIGNED> id860out_result_doubt;

  { // Node ID: 860 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id860in_a = id858out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id860in_a_doubt = id858out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id860in_b = id861out_value;

    HWOffsetFix<1,0,UNSIGNED> id860x_1;

    id860out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id860in_a,id860in_b,(&(id860x_1))));
    id860out_result_doubt = (or_fixed((neq_fixed((id860x_1),(c_hw_fix_1_0_uns_bits_1))),id860in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id862out_o;
  HWOffsetFix<1,0,UNSIGNED> id862out_o_doubt;

  { // Node ID: 862 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id862in_i = id860out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id862in_i_doubt = id860out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id862x_1;

    id862out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id862in_i,(&(id862x_1))));
    id862out_o_doubt = (or_fixed((neq_fixed((id862x_1),(c_hw_fix_1_0_uns_bits_1))),id862in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id871out_output;

  { // Node ID: 871 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id871in_input = id862out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id871in_input_doubt = id862out_o_doubt;

    id871out_output = id871in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id872out_o;

  { // Node ID: 872 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id872in_i = id871out_output;

    id872out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id872in_i));
  }
  { // Node ID: 3275 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3275in_input = id872out_o;

    id3275out_output[(getCycle()+2)%3] = id3275in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id875out_o;

  { // Node ID: 875 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id875in_i = id871out_output;

    id875out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id875in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id947out_output;

  { // Node ID: 947 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id947in_input = id875out_o;

    id947out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id947in_input))));
  }
  { // Node ID: 948 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id948in_addr = id947out_output;

    HWOffsetFix<32,-45,UNSIGNED> id948x_1;

    switch(((long)((id948in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id948x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id948x_1 = (id948sta_rom_store[(id948in_addr.getValueAsLong())]);
        break;
      default:
        id948x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id948out_dout[(getCycle()+2)%3] = (id948x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id874out_o;

  { // Node ID: 874 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id874in_i = id871out_output;

    id874out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id874in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id944out_output;

  { // Node ID: 944 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id944in_input = id874out_o;

    id944out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id944in_input))));
  }
  { // Node ID: 945 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id945in_addr = id944out_output;

    HWOffsetFix<42,-45,UNSIGNED> id945x_1;

    switch(((long)((id945in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id945x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id945x_1 = (id945sta_rom_store[(id945in_addr.getValueAsLong())]);
        break;
      default:
        id945x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id945out_dout[(getCycle()+2)%3] = (id945x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id873out_o;

  { // Node ID: 873 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id873in_i = id871out_output;

    id873out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id873in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id941out_output;

  { // Node ID: 941 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id941in_input = id873out_o;

    id941out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id941in_input))));
  }
  { // Node ID: 942 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id942in_addr = id941out_output;

    HWOffsetFix<45,-45,UNSIGNED> id942x_1;

    switch(((long)((id942in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id942x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id942x_1 = (id942sta_rom_store[(id942in_addr.getValueAsLong())]);
        break;
      default:
        id942x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id942out_dout[(getCycle()+2)%3] = (id942x_1);
  }
  { // Node ID: 879 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id876out_o;

  { // Node ID: 876 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id876in_i = id871out_output;

    id876out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id876in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id878out_result;

  { // Node ID: 878 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id878in_a = id879out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id878in_b = id876out_o;

    id878out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id878in_a,id878in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id880out_o;

  { // Node ID: 880 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id880in_i = id878out_result;

    id880out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id880in_i));
  }
  { // Node ID: 3276 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3276in_input = id880out_o;

    id3276out_output[(getCycle()+2)%3] = id3276in_input;
  }
  { // Node ID: 3558 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id864out_result;

  { // Node ID: 864 (NodeGt)
    const HWFloat<11,45> &id864in_a = id3094out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id864in_b = id3558out_value;

    id864out_result = (gt_float(id864in_a,id864in_b));
  }
  { // Node ID: 3278 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3278in_input = id864out_result;

    id3278out_output[(getCycle()+6)%7] = id3278in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id865out_output;

  { // Node ID: 865 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id865in_input = id862out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id865in_input_doubt = id862out_o_doubt;

    id865out_output = id865in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id866out_result;

  { // Node ID: 866 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id866in_a = id3278out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id866in_b = id865out_output;

    HWOffsetFix<1,0,UNSIGNED> id866x_1;

    (id866x_1) = (and_fixed(id866in_a,id866in_b));
    id866out_result = (id866x_1);
  }
  { // Node ID: 3279 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3279in_input = id866out_result;

    id3279out_output[(getCycle()+2)%3] = id3279in_input;
  }
  { // Node ID: 3557 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id868out_result;

  { // Node ID: 868 (NodeLt)
    const HWFloat<11,45> &id868in_a = id3094out_floatOut[getCycle()%2];
    const HWFloat<11,45> &id868in_b = id3557out_value;

    id868out_result = (lt_float(id868in_a,id868in_b));
  }
  { // Node ID: 3280 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3280in_input = id868out_result;

    id3280out_output[(getCycle()+6)%7] = id3280in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id869out_output;

  { // Node ID: 869 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id869in_input = id862out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id869in_input_doubt = id862out_o_doubt;

    id869out_output = id869in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id870out_result;

  { // Node ID: 870 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id870in_a = id3280out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id870in_b = id869out_output;

    HWOffsetFix<1,0,UNSIGNED> id870x_1;

    (id870x_1) = (and_fixed(id870in_a,id870in_b));
    id870out_result = (id870x_1);
  }
  { // Node ID: 3281 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3281in_input = id870out_result;

    id3281out_output[(getCycle()+2)%3] = id3281in_input;
  }
  { // Node ID: 3554 (NodeConstantRawBits)
  }
  { // Node ID: 3553 (NodeConstantRawBits)
  }
  HWFloat<11,45> id952out_result;

  { // Node ID: 952 (NodeAdd)
    const HWFloat<11,45> &id952in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id952in_b = id3553out_value;

    id952out_result = (add_float(id952in_a,id952in_b));
  }
  HWFloat<11,45> id954out_result;

  { // Node ID: 954 (NodeMul)
    const HWFloat<11,45> &id954in_a = id3554out_value;
    const HWFloat<11,45> &id954in_b = id952out_result;

    id954out_result = (mul_float(id954in_a,id954in_b));
  }
  HWRawBits<11> id1031out_result;

  { // Node ID: 1031 (NodeSlice)
    const HWFloat<11,45> &id1031in_a = id954out_result;

    id1031out_result = (slice<44,11>(id1031in_a));
  }
  { // Node ID: 1032 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2930out_result;

  { // Node ID: 2930 (NodeEqInlined)
    const HWRawBits<11> &id2930in_a = id1031out_result;
    const HWRawBits<11> &id2930in_b = id1032out_value;

    id2930out_result = (eq_bits(id2930in_a,id2930in_b));
  }
  HWRawBits<44> id1030out_result;

  { // Node ID: 1030 (NodeSlice)
    const HWFloat<11,45> &id1030in_a = id954out_result;

    id1030out_result = (slice<0,44>(id1030in_a));
  }
  { // Node ID: 3552 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2931out_result;

  { // Node ID: 2931 (NodeNeqInlined)
    const HWRawBits<44> &id2931in_a = id1030out_result;
    const HWRawBits<44> &id2931in_b = id3552out_value;

    id2931out_result = (neq_bits(id2931in_a,id2931in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1036out_result;

  { // Node ID: 1036 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1036in_a = id2930out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1036in_b = id2931out_result;

    HWOffsetFix<1,0,UNSIGNED> id1036x_1;

    (id1036x_1) = (and_fixed(id1036in_a,id1036in_b));
    id1036out_result = (id1036x_1);
  }
  { // Node ID: 3295 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3295in_input = id1036out_result;

    id3295out_output[(getCycle()+8)%9] = id3295in_input;
  }
  { // Node ID: 955 (NodeConstantRawBits)
  }
  HWFloat<11,45> id956out_output;
  HWOffsetFix<1,0,UNSIGNED> id956out_output_doubt;

  { // Node ID: 956 (NodeDoubtBitOp)
    const HWFloat<11,45> &id956in_input = id954out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id956in_doubt = id955out_value;

    id956out_output = id956in_input;
    id956out_output_doubt = id956in_doubt;
  }
  { // Node ID: 957 (NodeCast)
    const HWFloat<11,45> &id957in_i = id956out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id957in_i_doubt = id956out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id957x_1;

    id957out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id957in_i,(&(id957x_1))));
    id957out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id957x_1),(c_hw_fix_4_0_uns_bits))),id957in_i_doubt));
  }
  { // Node ID: 960 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id959out_result;
  HWOffsetFix<1,0,UNSIGNED> id959out_result_doubt;

  { // Node ID: 959 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id959in_a = id957out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id959in_a_doubt = id957out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id959in_b = id960out_value;

    HWOffsetFix<1,0,UNSIGNED> id959x_1;

    id959out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id959in_a,id959in_b,(&(id959x_1))));
    id959out_result_doubt = (or_fixed((neq_fixed((id959x_1),(c_hw_fix_1_0_uns_bits_1))),id959in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id961out_o;
  HWOffsetFix<1,0,UNSIGNED> id961out_o_doubt;

  { // Node ID: 961 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id961in_i = id959out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id961in_i_doubt = id959out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id961x_1;

    id961out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id961in_i,(&(id961x_1))));
    id961out_o_doubt = (or_fixed((neq_fixed((id961x_1),(c_hw_fix_1_0_uns_bits_1))),id961in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id970out_output;

  { // Node ID: 970 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id970in_input = id961out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id970in_input_doubt = id961out_o_doubt;

    id970out_output = id970in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id971out_o;

  { // Node ID: 971 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id971in_i = id970out_output;

    id971out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id971in_i));
  }
  { // Node ID: 3286 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3286in_input = id971out_o;

    id3286out_output[(getCycle()+2)%3] = id3286in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id974out_o;

  { // Node ID: 974 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id974in_i = id970out_output;

    id974out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id974in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1046out_output;

  { // Node ID: 1046 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1046in_input = id974out_o;

    id1046out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1046in_input))));
  }
  { // Node ID: 1047 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1047in_addr = id1046out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1047x_1;

    switch(((long)((id1047in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1047x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1047x_1 = (id1047sta_rom_store[(id1047in_addr.getValueAsLong())]);
        break;
      default:
        id1047x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1047out_dout[(getCycle()+2)%3] = (id1047x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id973out_o;

  { // Node ID: 973 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id973in_i = id970out_output;

    id973out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id973in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1043out_output;

  { // Node ID: 1043 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1043in_input = id973out_o;

    id1043out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1043in_input))));
  }
  { // Node ID: 1044 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1044in_addr = id1043out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1044x_1;

    switch(((long)((id1044in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1044x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1044x_1 = (id1044sta_rom_store[(id1044in_addr.getValueAsLong())]);
        break;
      default:
        id1044x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1044out_dout[(getCycle()+2)%3] = (id1044x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id972out_o;

  { // Node ID: 972 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id972in_i = id970out_output;

    id972out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id972in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1040out_output;

  { // Node ID: 1040 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1040in_input = id972out_o;

    id1040out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1040in_input))));
  }
  { // Node ID: 1041 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1041in_addr = id1040out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1041x_1;

    switch(((long)((id1041in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1041x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1041x_1 = (id1041sta_rom_store[(id1041in_addr.getValueAsLong())]);
        break;
      default:
        id1041x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1041out_dout[(getCycle()+2)%3] = (id1041x_1);
  }
  { // Node ID: 978 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id975out_o;

  { // Node ID: 975 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id975in_i = id970out_output;

    id975out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id975in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id977out_result;

  { // Node ID: 977 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id977in_a = id978out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id977in_b = id975out_o;

    id977out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id977in_a,id977in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id979out_o;

  { // Node ID: 979 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id979in_i = id977out_result;

    id979out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id979in_i));
  }
  { // Node ID: 3287 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3287in_input = id979out_o;

    id3287out_output[(getCycle()+2)%3] = id3287in_input;
  }
  { // Node ID: 3550 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id963out_result;

  { // Node ID: 963 (NodeGt)
    const HWFloat<11,45> &id963in_a = id954out_result;
    const HWFloat<11,45> &id963in_b = id3550out_value;

    id963out_result = (gt_float(id963in_a,id963in_b));
  }
  { // Node ID: 3289 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3289in_input = id963out_result;

    id3289out_output[(getCycle()+6)%7] = id3289in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id964out_output;

  { // Node ID: 964 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id964in_input = id961out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id964in_input_doubt = id961out_o_doubt;

    id964out_output = id964in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id965out_result;

  { // Node ID: 965 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id965in_a = id3289out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id965in_b = id964out_output;

    HWOffsetFix<1,0,UNSIGNED> id965x_1;

    (id965x_1) = (and_fixed(id965in_a,id965in_b));
    id965out_result = (id965x_1);
  }
  { // Node ID: 3290 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3290in_input = id965out_result;

    id3290out_output[(getCycle()+2)%3] = id3290in_input;
  }
  { // Node ID: 3549 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id967out_result;

  { // Node ID: 967 (NodeLt)
    const HWFloat<11,45> &id967in_a = id954out_result;
    const HWFloat<11,45> &id967in_b = id3549out_value;

    id967out_result = (lt_float(id967in_a,id967in_b));
  }
  { // Node ID: 3291 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3291in_input = id967out_result;

    id3291out_output[(getCycle()+6)%7] = id3291in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id968out_output;

  { // Node ID: 968 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id968in_input = id961out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id968in_input_doubt = id961out_o_doubt;

    id968out_output = id968in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id969out_result;

  { // Node ID: 969 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id969in_a = id3291out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id969in_b = id968out_output;

    HWOffsetFix<1,0,UNSIGNED> id969x_1;

    (id969x_1) = (and_fixed(id969in_a,id969in_b));
    id969out_result = (id969x_1);
  }
  { // Node ID: 3292 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3292in_input = id969out_result;

    id3292out_output[(getCycle()+2)%3] = id3292in_input;
  }
  { // Node ID: 3543 (NodeConstantRawBits)
  }
  { // Node ID: 3542 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1053out_result;

  { // Node ID: 1053 (NodeAdd)
    const HWFloat<11,45> &id1053in_a = id3123out_output[getCycle()%2];
    const HWFloat<11,45> &id1053in_b = id3542out_value;

    id1053out_result = (add_float(id1053in_a,id1053in_b));
  }
  HWFloat<11,45> id1055out_result;

  { // Node ID: 1055 (NodeMul)
    const HWFloat<11,45> &id1055in_a = id3543out_value;
    const HWFloat<11,45> &id1055in_b = id1053out_result;

    id1055out_result = (mul_float(id1055in_a,id1055in_b));
  }
  HWRawBits<11> id1132out_result;

  { // Node ID: 1132 (NodeSlice)
    const HWFloat<11,45> &id1132in_a = id1055out_result;

    id1132out_result = (slice<44,11>(id1132in_a));
  }
  { // Node ID: 1133 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2937out_result;

  { // Node ID: 2937 (NodeEqInlined)
    const HWRawBits<11> &id2937in_a = id1132out_result;
    const HWRawBits<11> &id2937in_b = id1133out_value;

    id2937out_result = (eq_bits(id2937in_a,id2937in_b));
  }
  HWRawBits<44> id1131out_result;

  { // Node ID: 1131 (NodeSlice)
    const HWFloat<11,45> &id1131in_a = id1055out_result;

    id1131out_result = (slice<0,44>(id1131in_a));
  }
  { // Node ID: 3541 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2938out_result;

  { // Node ID: 2938 (NodeNeqInlined)
    const HWRawBits<44> &id2938in_a = id1131out_result;
    const HWRawBits<44> &id2938in_b = id3541out_value;

    id2938out_result = (neq_bits(id2938in_a,id2938in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1137out_result;

  { // Node ID: 1137 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1137in_a = id2937out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1137in_b = id2938out_result;

    HWOffsetFix<1,0,UNSIGNED> id1137x_1;

    (id1137x_1) = (and_fixed(id1137in_a,id1137in_b));
    id1137out_result = (id1137x_1);
  }
  { // Node ID: 3307 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3307in_input = id1137out_result;

    id3307out_output[(getCycle()+8)%9] = id3307in_input;
  }
  { // Node ID: 1056 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1057out_output;
  HWOffsetFix<1,0,UNSIGNED> id1057out_output_doubt;

  { // Node ID: 1057 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1057in_input = id1055out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1057in_doubt = id1056out_value;

    id1057out_output = id1057in_input;
    id1057out_output_doubt = id1057in_doubt;
  }
  { // Node ID: 1058 (NodeCast)
    const HWFloat<11,45> &id1058in_i = id1057out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1058in_i_doubt = id1057out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1058x_1;

    id1058out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1058in_i,(&(id1058x_1))));
    id1058out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1058x_1),(c_hw_fix_4_0_uns_bits))),id1058in_i_doubt));
  }
  { // Node ID: 1061 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1060out_result;
  HWOffsetFix<1,0,UNSIGNED> id1060out_result_doubt;

  { // Node ID: 1060 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1060in_a = id1058out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1060in_a_doubt = id1058out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1060in_b = id1061out_value;

    HWOffsetFix<1,0,UNSIGNED> id1060x_1;

    id1060out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1060in_a,id1060in_b,(&(id1060x_1))));
    id1060out_result_doubt = (or_fixed((neq_fixed((id1060x_1),(c_hw_fix_1_0_uns_bits_1))),id1060in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1062out_o;
  HWOffsetFix<1,0,UNSIGNED> id1062out_o_doubt;

  { // Node ID: 1062 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1062in_i = id1060out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1062in_i_doubt = id1060out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1062x_1;

    id1062out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1062in_i,(&(id1062x_1))));
    id1062out_o_doubt = (or_fixed((neq_fixed((id1062x_1),(c_hw_fix_1_0_uns_bits_1))),id1062in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1071out_output;

  { // Node ID: 1071 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1071in_input = id1062out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1071in_input_doubt = id1062out_o_doubt;

    id1071out_output = id1071in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1072out_o;

  { // Node ID: 1072 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1072in_i = id1071out_output;

    id1072out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1072in_i));
  }
  { // Node ID: 3298 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3298in_input = id1072out_o;

    id3298out_output[(getCycle()+2)%3] = id3298in_input;
  }
  HWOffsetFix<10,-23,UNSIGNED> id1075out_o;

  { // Node ID: 1075 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1075in_i = id1071out_output;

    id1075out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1075in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1147out_output;

  { // Node ID: 1147 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1147in_input = id1075out_o;

    id1147out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1147in_input))));
  }
  { // Node ID: 1148 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1148in_addr = id1147out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1148x_1;

    switch(((long)((id1148in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1148x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1148x_1 = (id1148sta_rom_store[(id1148in_addr.getValueAsLong())]);
        break;
      default:
        id1148x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1148out_dout[(getCycle()+2)%3] = (id1148x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1074out_o;

  { // Node ID: 1074 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1074in_i = id1071out_output;

    id1074out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1074in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1144out_output;

  { // Node ID: 1144 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1144in_input = id1074out_o;

    id1144out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1144in_input))));
  }
  { // Node ID: 1145 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1145in_addr = id1144out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1145x_1;

    switch(((long)((id1145in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1145x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1145x_1 = (id1145sta_rom_store[(id1145in_addr.getValueAsLong())]);
        break;
      default:
        id1145x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1145out_dout[(getCycle()+2)%3] = (id1145x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1073out_o;

  { // Node ID: 1073 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1073in_i = id1071out_output;

    id1073out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1073in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1141out_output;

  { // Node ID: 1141 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1141in_input = id1073out_o;

    id1141out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1141in_input))));
  }
  { // Node ID: 1142 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1142in_addr = id1141out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1142x_1;

    switch(((long)((id1142in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1142x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1142x_1 = (id1142sta_rom_store[(id1142in_addr.getValueAsLong())]);
        break;
      default:
        id1142x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1142out_dout[(getCycle()+2)%3] = (id1142x_1);
  }
  { // Node ID: 1079 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1076out_o;

  { // Node ID: 1076 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1076in_i = id1071out_output;

    id1076out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1076in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1078out_result;

  { // Node ID: 1078 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1078in_a = id1079out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1078in_b = id1076out_o;

    id1078out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1078in_a,id1078in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1080out_o;

  { // Node ID: 1080 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1080in_i = id1078out_result;

    id1080out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1080in_i));
  }
  { // Node ID: 3299 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3299in_input = id1080out_o;

    id3299out_output[(getCycle()+2)%3] = id3299in_input;
  }
  { // Node ID: 3539 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1064out_result;

  { // Node ID: 1064 (NodeGt)
    const HWFloat<11,45> &id1064in_a = id1055out_result;
    const HWFloat<11,45> &id1064in_b = id3539out_value;

    id1064out_result = (gt_float(id1064in_a,id1064in_b));
  }
  { // Node ID: 3301 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3301in_input = id1064out_result;

    id3301out_output[(getCycle()+6)%7] = id3301in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1065out_output;

  { // Node ID: 1065 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1065in_input = id1062out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1065in_input_doubt = id1062out_o_doubt;

    id1065out_output = id1065in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1066out_result;

  { // Node ID: 1066 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1066in_a = id3301out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1066in_b = id1065out_output;

    HWOffsetFix<1,0,UNSIGNED> id1066x_1;

    (id1066x_1) = (and_fixed(id1066in_a,id1066in_b));
    id1066out_result = (id1066x_1);
  }
  { // Node ID: 3302 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3302in_input = id1066out_result;

    id3302out_output[(getCycle()+2)%3] = id3302in_input;
  }
  { // Node ID: 3538 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1068out_result;

  { // Node ID: 1068 (NodeLt)
    const HWFloat<11,45> &id1068in_a = id1055out_result;
    const HWFloat<11,45> &id1068in_b = id3538out_value;

    id1068out_result = (lt_float(id1068in_a,id1068in_b));
  }
  { // Node ID: 3303 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3303in_input = id1068out_result;

    id3303out_output[(getCycle()+6)%7] = id3303in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1069out_output;

  { // Node ID: 1069 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1069in_input = id1062out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1069in_input_doubt = id1062out_o_doubt;

    id1069out_output = id1069in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1070out_result;

  { // Node ID: 1070 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1070in_a = id3303out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1070in_b = id1069out_output;

    HWOffsetFix<1,0,UNSIGNED> id1070x_1;

    (id1070x_1) = (and_fixed(id1070in_a,id1070in_b));
    id1070out_result = (id1070x_1);
  }
  { // Node ID: 3304 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3304in_input = id1070out_result;

    id3304out_output[(getCycle()+2)%3] = id3304in_input;
  }
  { // Node ID: 3533 (NodeConstantRawBits)
  }
  { // Node ID: 2944 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2944in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2944in_b = id3533out_value;

    id2944out_result[(getCycle()+1)%2] = (eq_fixed(id2944in_a,id2944in_b));
  }
  HWFloat<11,45> id3109out_output;

  { // Node ID: 3109 (NodeStreamOffset)
    const HWFloat<11,45> &id3109in_input = id3311out_output[getCycle()%2];

    id3109out_output = id3109in_input;
  }
  { // Node ID: 40 (NodeInputMappedReg)
  }
  { // Node ID: 41 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id41in_sel = id2944out_result[getCycle()%2];
    const HWFloat<11,45> &id41in_option0 = id3109out_output;
    const HWFloat<11,45> &id41in_option1 = id40out_d_in;

    HWFloat<11,45> id41x_1;

    switch((id41in_sel.getValueAsLong())) {
      case 0l:
        id41x_1 = id41in_option0;
        break;
      case 1l:
        id41x_1 = id41in_option1;
        break;
      default:
        id41x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id41out_result[(getCycle()+1)%2] = (id41x_1);
  }
  { // Node ID: 3332 (NodeFIFO)
    const HWFloat<11,45> &id3332in_input = id41out_result[getCycle()%2];

    id3332out_output[(getCycle()+8)%9] = id3332in_input;
  }
  { // Node ID: 3532 (NodeConstantRawBits)
  }
  { // Node ID: 3531 (NodeConstantRawBits)
  }
  { // Node ID: 3530 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1155out_result;

  { // Node ID: 1155 (NodeSub)
    const HWFloat<11,45> &id1155in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1155in_b = id3530out_value;

    id1155out_result = (sub_float(id1155in_a,id1155in_b));
  }
  HWFloat<11,45> id1157out_result;

  { // Node ID: 1157 (NodeMul)
    const HWFloat<11,45> &id1157in_a = id3531out_value;
    const HWFloat<11,45> &id1157in_b = id1155out_result;

    id1157out_result = (mul_float(id1157in_a,id1157in_b));
  }
  HWRawBits<11> id1234out_result;

  { // Node ID: 1234 (NodeSlice)
    const HWFloat<11,45> &id1234in_a = id1157out_result;

    id1234out_result = (slice<44,11>(id1234in_a));
  }
  { // Node ID: 1235 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2945out_result;

  { // Node ID: 2945 (NodeEqInlined)
    const HWRawBits<11> &id2945in_a = id1234out_result;
    const HWRawBits<11> &id2945in_b = id1235out_value;

    id2945out_result = (eq_bits(id2945in_a,id2945in_b));
  }
  HWRawBits<44> id1233out_result;

  { // Node ID: 1233 (NodeSlice)
    const HWFloat<11,45> &id1233in_a = id1157out_result;

    id1233out_result = (slice<0,44>(id1233in_a));
  }
  { // Node ID: 3529 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2946out_result;

  { // Node ID: 2946 (NodeNeqInlined)
    const HWRawBits<44> &id2946in_a = id1233out_result;
    const HWRawBits<44> &id2946in_b = id3529out_value;

    id2946out_result = (neq_bits(id2946in_a,id2946in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1239out_result;

  { // Node ID: 1239 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1239in_a = id2945out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1239in_b = id2946out_result;

    HWOffsetFix<1,0,UNSIGNED> id1239x_1;

    (id1239x_1) = (and_fixed(id1239in_a,id1239in_b));
    id1239out_result = (id1239x_1);
  }
  { // Node ID: 3321 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3321in_input = id1239out_result;

    id3321out_output[(getCycle()+8)%9] = id3321in_input;
  }
  { // Node ID: 1158 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1159out_output;
  HWOffsetFix<1,0,UNSIGNED> id1159out_output_doubt;

  { // Node ID: 1159 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1159in_input = id1157out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1159in_doubt = id1158out_value;

    id1159out_output = id1159in_input;
    id1159out_output_doubt = id1159in_doubt;
  }
  { // Node ID: 1160 (NodeCast)
    const HWFloat<11,45> &id1160in_i = id1159out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1160in_i_doubt = id1159out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1160x_1;

    id1160out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1160in_i,(&(id1160x_1))));
    id1160out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1160x_1),(c_hw_fix_4_0_uns_bits))),id1160in_i_doubt));
  }
  { // Node ID: 1163 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1162out_result;
  HWOffsetFix<1,0,UNSIGNED> id1162out_result_doubt;

  { // Node ID: 1162 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1162in_a = id1160out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1162in_a_doubt = id1160out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1162in_b = id1163out_value;

    HWOffsetFix<1,0,UNSIGNED> id1162x_1;

    id1162out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1162in_a,id1162in_b,(&(id1162x_1))));
    id1162out_result_doubt = (or_fixed((neq_fixed((id1162x_1),(c_hw_fix_1_0_uns_bits_1))),id1162in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1164out_o;
  HWOffsetFix<1,0,UNSIGNED> id1164out_o_doubt;

  { // Node ID: 1164 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1164in_i = id1162out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1164in_i_doubt = id1162out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1164x_1;

    id1164out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1164in_i,(&(id1164x_1))));
    id1164out_o_doubt = (or_fixed((neq_fixed((id1164x_1),(c_hw_fix_1_0_uns_bits_1))),id1164in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1173out_output;

  { // Node ID: 1173 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1173in_input = id1164out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1173in_input_doubt = id1164out_o_doubt;

    id1173out_output = id1173in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1174out_o;

  { // Node ID: 1174 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1174in_i = id1173out_output;

    id1174out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1174in_i));
  }
  { // Node ID: 3312 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3312in_input = id1174out_o;

    id3312out_output[(getCycle()+2)%3] = id3312in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1201out_o;

  { // Node ID: 1201 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1201in_i = id3312out_output[getCycle()%3];

    id1201out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1201in_i));
  }
  { // Node ID: 1204 (NodeConstantRawBits)
  }
  { // Node ID: 3048 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1177out_o;

  { // Node ID: 1177 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1177in_i = id1173out_output;

    id1177out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1177in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1249out_output;

  { // Node ID: 1249 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1249in_input = id1177out_o;

    id1249out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1249in_input))));
  }
  { // Node ID: 1250 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1250in_addr = id1249out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1250x_1;

    switch(((long)((id1250in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1250x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1250x_1 = (id1250sta_rom_store[(id1250in_addr.getValueAsLong())]);
        break;
      default:
        id1250x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1250out_dout[(getCycle()+2)%3] = (id1250x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1176out_o;

  { // Node ID: 1176 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1176in_i = id1173out_output;

    id1176out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1176in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1246out_output;

  { // Node ID: 1246 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1246in_input = id1176out_o;

    id1246out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1246in_input))));
  }
  { // Node ID: 1247 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1247in_addr = id1246out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1247x_1;

    switch(((long)((id1247in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1247x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1247x_1 = (id1247sta_rom_store[(id1247in_addr.getValueAsLong())]);
        break;
      default:
        id1247x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1247out_dout[(getCycle()+2)%3] = (id1247x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1175out_o;

  { // Node ID: 1175 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1175in_i = id1173out_output;

    id1175out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1175in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1243out_output;

  { // Node ID: 1243 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1243in_input = id1175out_o;

    id1243out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1243in_input))));
  }
  { // Node ID: 1244 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1244in_addr = id1243out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1244x_1;

    switch(((long)((id1244in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1244x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1244x_1 = (id1244sta_rom_store[(id1244in_addr.getValueAsLong())]);
        break;
      default:
        id1244x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1244out_dout[(getCycle()+2)%3] = (id1244x_1);
  }
  { // Node ID: 1181 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1178out_o;

  { // Node ID: 1178 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1178in_i = id1173out_output;

    id1178out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1178in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1180out_result;

  { // Node ID: 1180 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1180in_a = id1181out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1180in_b = id1178out_o;

    id1180out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1180in_a,id1180in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1182out_o;

  { // Node ID: 1182 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1182in_i = id1180out_result;

    id1182out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1182in_i));
  }
  { // Node ID: 3313 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3313in_input = id1182out_o;

    id3313out_output[(getCycle()+2)%3] = id3313in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1183out_result;

  { // Node ID: 1183 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1183in_a = id1244out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1183in_b = id3313out_output[getCycle()%3];

    id1183out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1183in_a,id1183in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1184out_result;

  { // Node ID: 1184 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1184in_a = id3313out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1184in_b = id1244out_dout[getCycle()%3];

    id1184out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1184in_a,id1184in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1185out_result;

  { // Node ID: 1185 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1185in_a = id1183out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1185in_b = id1184out_result;

    id1185out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1185in_a,id1185in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1186out_o;

  { // Node ID: 1186 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1186in_i = id1185out_result;

    id1186out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1186in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1187out_result;

  { // Node ID: 1187 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1187in_a = id1247out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1187in_b = id1186out_o;

    id1187out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1187in_a,id1187in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1188out_result;

  { // Node ID: 1188 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1188in_a = id1186out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1188in_b = id1247out_dout[getCycle()%3];

    id1188out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1188in_a,id1188in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1189out_result;

  { // Node ID: 1189 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1189in_a = id1187out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1189in_b = id1188out_result;

    id1189out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1189in_a,id1189in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1190out_o;

  { // Node ID: 1190 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1190in_i = id1189out_result;

    id1190out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1190in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1191out_result;

  { // Node ID: 1191 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1191in_a = id1250out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1191in_b = id1190out_o;

    id1191out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1191in_a,id1191in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1192out_result;

  { // Node ID: 1192 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1192in_a = id1190out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1192in_b = id1250out_dout[getCycle()%3];

    id1192out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1192in_a,id1192in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1193out_result;

  { // Node ID: 1193 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1193in_a = id1191out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1193in_b = id1192out_result;

    id1193out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1193in_a,id1193in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1194out_o;

  { // Node ID: 1194 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1194in_i = id1193out_result;

    id1194out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1194in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1195out_o;

  { // Node ID: 1195 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1195in_i = id1194out_o;

    id1195out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1195in_i));
  }
  { // Node ID: 3528 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2947out_result;

  { // Node ID: 2947 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2947in_a = id1195out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2947in_b = id3528out_value;

    id2947out_result = (gte_fixed(id2947in_a,id2947in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3073out_result;

  { // Node ID: 3073 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3073in_a = id1201out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3073in_b = id1204out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3073in_c = id3048out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3073in_condb = id2947out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3073x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3073x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3073x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3073x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3073x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3073x_1 = id3073in_a;
        break;
      default:
        id3073x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3073in_condb.getValueAsLong())) {
      case 0l:
        id3073x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3073x_2 = id3073in_b;
        break;
      default:
        id3073x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3073x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3073x_3 = id3073in_c;
        break;
      default:
        id3073x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3073x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3073x_1),(id3073x_2))),(id3073x_3)));
    id3073out_result = (id3073x_4);
  }
  HWRawBits<1> id2948out_result;

  { // Node ID: 2948 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2948in_a = id3073out_result;

    id2948out_result = (slice<13,1>(id2948in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2949out_output;

  { // Node ID: 2949 (NodeReinterpret)
    const HWRawBits<1> &id2949in_input = id2948out_result;

    id2949out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2949in_input));
  }
  { // Node ID: 3527 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1166out_result;

  { // Node ID: 1166 (NodeGt)
    const HWFloat<11,45> &id1166in_a = id1157out_result;
    const HWFloat<11,45> &id1166in_b = id3527out_value;

    id1166out_result = (gt_float(id1166in_a,id1166in_b));
  }
  { // Node ID: 3315 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3315in_input = id1166out_result;

    id3315out_output[(getCycle()+6)%7] = id3315in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1167out_output;

  { // Node ID: 1167 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1167in_input = id1164out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1167in_input_doubt = id1164out_o_doubt;

    id1167out_output = id1167in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1168out_result;

  { // Node ID: 1168 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1168in_a = id3315out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1168in_b = id1167out_output;

    HWOffsetFix<1,0,UNSIGNED> id1168x_1;

    (id1168x_1) = (and_fixed(id1168in_a,id1168in_b));
    id1168out_result = (id1168x_1);
  }
  { // Node ID: 3316 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3316in_input = id1168out_result;

    id3316out_output[(getCycle()+2)%3] = id3316in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1210out_result;

  { // Node ID: 1210 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1210in_a = id3316out_output[getCycle()%3];

    id1210out_result = (not_fixed(id1210in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1211out_result;

  { // Node ID: 1211 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1211in_a = id2949out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1211in_b = id1210out_result;

    HWOffsetFix<1,0,UNSIGNED> id1211x_1;

    (id1211x_1) = (and_fixed(id1211in_a,id1211in_b));
    id1211out_result = (id1211x_1);
  }
  { // Node ID: 3526 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1170out_result;

  { // Node ID: 1170 (NodeLt)
    const HWFloat<11,45> &id1170in_a = id1157out_result;
    const HWFloat<11,45> &id1170in_b = id3526out_value;

    id1170out_result = (lt_float(id1170in_a,id1170in_b));
  }
  { // Node ID: 3317 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3317in_input = id1170out_result;

    id3317out_output[(getCycle()+6)%7] = id3317in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1171out_output;

  { // Node ID: 1171 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1171in_input = id1164out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1171in_input_doubt = id1164out_o_doubt;

    id1171out_output = id1171in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1172out_result;

  { // Node ID: 1172 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1172in_a = id3317out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1172in_b = id1171out_output;

    HWOffsetFix<1,0,UNSIGNED> id1172x_1;

    (id1172x_1) = (and_fixed(id1172in_a,id1172in_b));
    id1172out_result = (id1172x_1);
  }
  { // Node ID: 3318 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3318in_input = id1172out_result;

    id3318out_output[(getCycle()+2)%3] = id3318in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1212out_result;

  { // Node ID: 1212 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1212in_a = id1211out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1212in_b = id3318out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1212x_1;

    (id1212x_1) = (or_fixed(id1212in_a,id1212in_b));
    id1212out_result = (id1212x_1);
  }
  { // Node ID: 3525 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2950out_result;

  { // Node ID: 2950 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2950in_a = id3073out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2950in_b = id3525out_value;

    id2950out_result = (gte_fixed(id2950in_a,id2950in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1219out_result;

  { // Node ID: 1219 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1219in_a = id3318out_output[getCycle()%3];

    id1219out_result = (not_fixed(id1219in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1220out_result;

  { // Node ID: 1220 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1220in_a = id2950out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1220in_b = id1219out_result;

    HWOffsetFix<1,0,UNSIGNED> id1220x_1;

    (id1220x_1) = (and_fixed(id1220in_a,id1220in_b));
    id1220out_result = (id1220x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1221out_result;

  { // Node ID: 1221 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1221in_a = id1220out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1221in_b = id3316out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1221x_1;

    (id1221x_1) = (or_fixed(id1221in_a,id1221in_b));
    id1221out_result = (id1221x_1);
  }
  HWRawBits<2> id1222out_result;

  { // Node ID: 1222 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1222in_in0 = id1212out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1222in_in1 = id1221out_result;

    id1222out_result = (cat(id1222in_in0,id1222in_in1));
  }
  { // Node ID: 1214 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1213out_o;

  { // Node ID: 1213 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1213in_i = id3073out_result;

    id1213out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1213in_i));
  }
  { // Node ID: 1198 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1199out_result;

  { // Node ID: 1199 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1199in_sel = id2947out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1199in_option0 = id1195out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1199in_option1 = id1198out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1199x_1;

    switch((id1199in_sel.getValueAsLong())) {
      case 0l:
        id1199x_1 = id1199in_option0;
        break;
      case 1l:
        id1199x_1 = id1199in_option1;
        break;
      default:
        id1199x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1199out_result = (id1199x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1200out_o;

  { // Node ID: 1200 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1200in_i = id1199out_result;

    id1200out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1200in_i));
  }
  HWRawBits<56> id1215out_result;

  { // Node ID: 1215 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1215in_in0 = id1214out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1215in_in1 = id1213out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1215in_in2 = id1200out_o;

    id1215out_result = (cat((cat(id1215in_in0,id1215in_in1)),id1215in_in2));
  }
  HWFloat<11,45> id1216out_output;

  { // Node ID: 1216 (NodeReinterpret)
    const HWRawBits<56> &id1216in_input = id1215out_result;

    id1216out_output = (cast_bits2float<11,45>(id1216in_input));
  }
  { // Node ID: 1223 (NodeConstantRawBits)
  }
  { // Node ID: 1224 (NodeConstantRawBits)
  }
  { // Node ID: 1226 (NodeConstantRawBits)
  }
  HWRawBits<56> id2951out_result;

  { // Node ID: 2951 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2951in_in0 = id1223out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2951in_in1 = id1224out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2951in_in2 = id1226out_value;

    id2951out_result = (cat((cat(id2951in_in0,id2951in_in1)),id2951in_in2));
  }
  HWFloat<11,45> id1228out_output;

  { // Node ID: 1228 (NodeReinterpret)
    const HWRawBits<56> &id1228in_input = id2951out_result;

    id1228out_output = (cast_bits2float<11,45>(id1228in_input));
  }
  { // Node ID: 2811 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1231out_result;

  { // Node ID: 1231 (NodeMux)
    const HWRawBits<2> &id1231in_sel = id1222out_result;
    const HWFloat<11,45> &id1231in_option0 = id1216out_output;
    const HWFloat<11,45> &id1231in_option1 = id1228out_output;
    const HWFloat<11,45> &id1231in_option2 = id2811out_value;
    const HWFloat<11,45> &id1231in_option3 = id1228out_output;

    HWFloat<11,45> id1231x_1;

    switch((id1231in_sel.getValueAsLong())) {
      case 0l:
        id1231x_1 = id1231in_option0;
        break;
      case 1l:
        id1231x_1 = id1231in_option1;
        break;
      case 2l:
        id1231x_1 = id1231in_option2;
        break;
      case 3l:
        id1231x_1 = id1231in_option3;
        break;
      default:
        id1231x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1231out_result = (id1231x_1);
  }
  { // Node ID: 3524 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1241out_result;

  { // Node ID: 1241 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1241in_sel = id3321out_output[getCycle()%9];
    const HWFloat<11,45> &id1241in_option0 = id1231out_result;
    const HWFloat<11,45> &id1241in_option1 = id3524out_value;

    HWFloat<11,45> id1241x_1;

    switch((id1241in_sel.getValueAsLong())) {
      case 0l:
        id1241x_1 = id1241in_option0;
        break;
      case 1l:
        id1241x_1 = id1241in_option1;
        break;
      default:
        id1241x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1241out_result = (id1241x_1);
  }
  HWFloat<11,45> id1252out_result;

  { // Node ID: 1252 (NodeMul)
    const HWFloat<11,45> &id1252in_a = id3532out_value;
    const HWFloat<11,45> &id1252in_b = id1241out_result;

    id1252out_result = (mul_float(id1252in_a,id1252in_b));
  }
  { // Node ID: 3523 (NodeConstantRawBits)
  }
  { // Node ID: 3522 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1254out_result;

  { // Node ID: 1254 (NodeSub)
    const HWFloat<11,45> &id1254in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1254in_b = id3522out_value;

    id1254out_result = (sub_float(id1254in_a,id1254in_b));
  }
  HWFloat<11,45> id1256out_result;

  { // Node ID: 1256 (NodeMul)
    const HWFloat<11,45> &id1256in_a = id3523out_value;
    const HWFloat<11,45> &id1256in_b = id1254out_result;

    id1256out_result = (mul_float(id1256in_a,id1256in_b));
  }
  HWRawBits<11> id1333out_result;

  { // Node ID: 1333 (NodeSlice)
    const HWFloat<11,45> &id1333in_a = id1256out_result;

    id1333out_result = (slice<44,11>(id1333in_a));
  }
  { // Node ID: 1334 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2952out_result;

  { // Node ID: 2952 (NodeEqInlined)
    const HWRawBits<11> &id2952in_a = id1333out_result;
    const HWRawBits<11> &id2952in_b = id1334out_value;

    id2952out_result = (eq_bits(id2952in_a,id2952in_b));
  }
  HWRawBits<44> id1332out_result;

  { // Node ID: 1332 (NodeSlice)
    const HWFloat<11,45> &id1332in_a = id1256out_result;

    id1332out_result = (slice<0,44>(id1332in_a));
  }
  { // Node ID: 3521 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2953out_result;

  { // Node ID: 2953 (NodeNeqInlined)
    const HWRawBits<44> &id2953in_a = id1332out_result;
    const HWRawBits<44> &id2953in_b = id3521out_value;

    id2953out_result = (neq_bits(id2953in_a,id2953in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1338out_result;

  { // Node ID: 1338 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1338in_a = id2952out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1338in_b = id2953out_result;

    HWOffsetFix<1,0,UNSIGNED> id1338x_1;

    (id1338x_1) = (and_fixed(id1338in_a,id1338in_b));
    id1338out_result = (id1338x_1);
  }
  { // Node ID: 3331 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3331in_input = id1338out_result;

    id3331out_output[(getCycle()+8)%9] = id3331in_input;
  }
  { // Node ID: 1257 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1258out_output;
  HWOffsetFix<1,0,UNSIGNED> id1258out_output_doubt;

  { // Node ID: 1258 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1258in_input = id1256out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1258in_doubt = id1257out_value;

    id1258out_output = id1258in_input;
    id1258out_output_doubt = id1258in_doubt;
  }
  { // Node ID: 1259 (NodeCast)
    const HWFloat<11,45> &id1259in_i = id1258out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1259in_i_doubt = id1258out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1259x_1;

    id1259out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1259in_i,(&(id1259x_1))));
    id1259out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1259x_1),(c_hw_fix_4_0_uns_bits))),id1259in_i_doubt));
  }
  { // Node ID: 1262 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1261out_result;
  HWOffsetFix<1,0,UNSIGNED> id1261out_result_doubt;

  { // Node ID: 1261 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1261in_a = id1259out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1261in_a_doubt = id1259out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1261in_b = id1262out_value;

    HWOffsetFix<1,0,UNSIGNED> id1261x_1;

    id1261out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1261in_a,id1261in_b,(&(id1261x_1))));
    id1261out_result_doubt = (or_fixed((neq_fixed((id1261x_1),(c_hw_fix_1_0_uns_bits_1))),id1261in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1263out_o;
  HWOffsetFix<1,0,UNSIGNED> id1263out_o_doubt;

  { // Node ID: 1263 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1263in_i = id1261out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1263in_i_doubt = id1261out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1263x_1;

    id1263out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1263in_i,(&(id1263x_1))));
    id1263out_o_doubt = (or_fixed((neq_fixed((id1263x_1),(c_hw_fix_1_0_uns_bits_1))),id1263in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1272out_output;

  { // Node ID: 1272 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1272in_input = id1263out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1272in_input_doubt = id1263out_o_doubt;

    id1272out_output = id1272in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1273out_o;

  { // Node ID: 1273 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1273in_i = id1272out_output;

    id1273out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1273in_i));
  }
  { // Node ID: 3322 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3322in_input = id1273out_o;

    id3322out_output[(getCycle()+2)%3] = id3322in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1300out_o;

  { // Node ID: 1300 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1300in_i = id3322out_output[getCycle()%3];

    id1300out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1300in_i));
  }
  { // Node ID: 1303 (NodeConstantRawBits)
  }
  { // Node ID: 3050 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1276out_o;

  { // Node ID: 1276 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1276in_i = id1272out_output;

    id1276out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1276in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1348out_output;

  { // Node ID: 1348 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1348in_input = id1276out_o;

    id1348out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1348in_input))));
  }
  { // Node ID: 1349 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1349in_addr = id1348out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1349x_1;

    switch(((long)((id1349in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1349x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1349x_1 = (id1349sta_rom_store[(id1349in_addr.getValueAsLong())]);
        break;
      default:
        id1349x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1349out_dout[(getCycle()+2)%3] = (id1349x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1275out_o;

  { // Node ID: 1275 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1275in_i = id1272out_output;

    id1275out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1275in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1345out_output;

  { // Node ID: 1345 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1345in_input = id1275out_o;

    id1345out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1345in_input))));
  }
  { // Node ID: 1346 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1346in_addr = id1345out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1346x_1;

    switch(((long)((id1346in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1346x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1346x_1 = (id1346sta_rom_store[(id1346in_addr.getValueAsLong())]);
        break;
      default:
        id1346x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1346out_dout[(getCycle()+2)%3] = (id1346x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1274out_o;

  { // Node ID: 1274 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1274in_i = id1272out_output;

    id1274out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1274in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1342out_output;

  { // Node ID: 1342 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1342in_input = id1274out_o;

    id1342out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1342in_input))));
  }
  { // Node ID: 1343 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1343in_addr = id1342out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1343x_1;

    switch(((long)((id1343in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1343x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1343x_1 = (id1343sta_rom_store[(id1343in_addr.getValueAsLong())]);
        break;
      default:
        id1343x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1343out_dout[(getCycle()+2)%3] = (id1343x_1);
  }
  { // Node ID: 1280 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1277out_o;

  { // Node ID: 1277 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1277in_i = id1272out_output;

    id1277out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1277in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1279out_result;

  { // Node ID: 1279 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1279in_a = id1280out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1279in_b = id1277out_o;

    id1279out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1279in_a,id1279in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1281out_o;

  { // Node ID: 1281 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1281in_i = id1279out_result;

    id1281out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1281in_i));
  }
  { // Node ID: 3323 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3323in_input = id1281out_o;

    id3323out_output[(getCycle()+2)%3] = id3323in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1282out_result;

  { // Node ID: 1282 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1282in_a = id1343out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1282in_b = id3323out_output[getCycle()%3];

    id1282out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1282in_a,id1282in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1283out_result;

  { // Node ID: 1283 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1283in_a = id3323out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1283in_b = id1343out_dout[getCycle()%3];

    id1283out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1283in_a,id1283in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1284out_result;

  { // Node ID: 1284 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1284in_a = id1282out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1284in_b = id1283out_result;

    id1284out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1284in_a,id1284in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1285out_o;

  { // Node ID: 1285 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1285in_i = id1284out_result;

    id1285out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1285in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1286out_result;

  { // Node ID: 1286 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1286in_a = id1346out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1286in_b = id1285out_o;

    id1286out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1286in_a,id1286in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1287out_result;

  { // Node ID: 1287 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1287in_a = id1285out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1287in_b = id1346out_dout[getCycle()%3];

    id1287out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1287in_a,id1287in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1288out_result;

  { // Node ID: 1288 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1288in_a = id1286out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1288in_b = id1287out_result;

    id1288out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1288in_a,id1288in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1289out_o;

  { // Node ID: 1289 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1289in_i = id1288out_result;

    id1289out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1289in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1290out_result;

  { // Node ID: 1290 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1290in_a = id1349out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1290in_b = id1289out_o;

    id1290out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1290in_a,id1290in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1291out_result;

  { // Node ID: 1291 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1291in_a = id1289out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1291in_b = id1349out_dout[getCycle()%3];

    id1291out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1291in_a,id1291in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1292out_result;

  { // Node ID: 1292 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1292in_a = id1290out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1292in_b = id1291out_result;

    id1292out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1292in_a,id1292in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1293out_o;

  { // Node ID: 1293 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1293in_i = id1292out_result;

    id1293out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1293in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1294out_o;

  { // Node ID: 1294 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1294in_i = id1293out_o;

    id1294out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1294in_i));
  }
  { // Node ID: 3520 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2954out_result;

  { // Node ID: 2954 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2954in_a = id1294out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2954in_b = id3520out_value;

    id2954out_result = (gte_fixed(id2954in_a,id2954in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3072out_result;

  { // Node ID: 3072 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3072in_a = id1300out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3072in_b = id1303out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3072in_c = id3050out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3072in_condb = id2954out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3072x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3072x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3072x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3072x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3072x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3072x_1 = id3072in_a;
        break;
      default:
        id3072x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3072in_condb.getValueAsLong())) {
      case 0l:
        id3072x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3072x_2 = id3072in_b;
        break;
      default:
        id3072x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3072x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3072x_3 = id3072in_c;
        break;
      default:
        id3072x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3072x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3072x_1),(id3072x_2))),(id3072x_3)));
    id3072out_result = (id3072x_4);
  }
  HWRawBits<1> id2955out_result;

  { // Node ID: 2955 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2955in_a = id3072out_result;

    id2955out_result = (slice<13,1>(id2955in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2956out_output;

  { // Node ID: 2956 (NodeReinterpret)
    const HWRawBits<1> &id2956in_input = id2955out_result;

    id2956out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2956in_input));
  }
  { // Node ID: 3519 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1265out_result;

  { // Node ID: 1265 (NodeGt)
    const HWFloat<11,45> &id1265in_a = id1256out_result;
    const HWFloat<11,45> &id1265in_b = id3519out_value;

    id1265out_result = (gt_float(id1265in_a,id1265in_b));
  }
  { // Node ID: 3325 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3325in_input = id1265out_result;

    id3325out_output[(getCycle()+6)%7] = id3325in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1266out_output;

  { // Node ID: 1266 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1266in_input = id1263out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1266in_input_doubt = id1263out_o_doubt;

    id1266out_output = id1266in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1267out_result;

  { // Node ID: 1267 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1267in_a = id3325out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1267in_b = id1266out_output;

    HWOffsetFix<1,0,UNSIGNED> id1267x_1;

    (id1267x_1) = (and_fixed(id1267in_a,id1267in_b));
    id1267out_result = (id1267x_1);
  }
  { // Node ID: 3326 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3326in_input = id1267out_result;

    id3326out_output[(getCycle()+2)%3] = id3326in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1309out_result;

  { // Node ID: 1309 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1309in_a = id3326out_output[getCycle()%3];

    id1309out_result = (not_fixed(id1309in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1310out_result;

  { // Node ID: 1310 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1310in_a = id2956out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1310in_b = id1309out_result;

    HWOffsetFix<1,0,UNSIGNED> id1310x_1;

    (id1310x_1) = (and_fixed(id1310in_a,id1310in_b));
    id1310out_result = (id1310x_1);
  }
  { // Node ID: 3518 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1269out_result;

  { // Node ID: 1269 (NodeLt)
    const HWFloat<11,45> &id1269in_a = id1256out_result;
    const HWFloat<11,45> &id1269in_b = id3518out_value;

    id1269out_result = (lt_float(id1269in_a,id1269in_b));
  }
  { // Node ID: 3327 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3327in_input = id1269out_result;

    id3327out_output[(getCycle()+6)%7] = id3327in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1270out_output;

  { // Node ID: 1270 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1270in_input = id1263out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1270in_input_doubt = id1263out_o_doubt;

    id1270out_output = id1270in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1271out_result;

  { // Node ID: 1271 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1271in_a = id3327out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1271in_b = id1270out_output;

    HWOffsetFix<1,0,UNSIGNED> id1271x_1;

    (id1271x_1) = (and_fixed(id1271in_a,id1271in_b));
    id1271out_result = (id1271x_1);
  }
  { // Node ID: 3328 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3328in_input = id1271out_result;

    id3328out_output[(getCycle()+2)%3] = id3328in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1311out_result;

  { // Node ID: 1311 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1311in_a = id1310out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1311in_b = id3328out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1311x_1;

    (id1311x_1) = (or_fixed(id1311in_a,id1311in_b));
    id1311out_result = (id1311x_1);
  }
  { // Node ID: 3517 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2957out_result;

  { // Node ID: 2957 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2957in_a = id3072out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2957in_b = id3517out_value;

    id2957out_result = (gte_fixed(id2957in_a,id2957in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1318out_result;

  { // Node ID: 1318 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1318in_a = id3328out_output[getCycle()%3];

    id1318out_result = (not_fixed(id1318in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1319out_result;

  { // Node ID: 1319 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1319in_a = id2957out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1319in_b = id1318out_result;

    HWOffsetFix<1,0,UNSIGNED> id1319x_1;

    (id1319x_1) = (and_fixed(id1319in_a,id1319in_b));
    id1319out_result = (id1319x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1320out_result;

  { // Node ID: 1320 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1320in_a = id1319out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1320in_b = id3326out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1320x_1;

    (id1320x_1) = (or_fixed(id1320in_a,id1320in_b));
    id1320out_result = (id1320x_1);
  }
  HWRawBits<2> id1321out_result;

  { // Node ID: 1321 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1321in_in0 = id1311out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1321in_in1 = id1320out_result;

    id1321out_result = (cat(id1321in_in0,id1321in_in1));
  }
  { // Node ID: 1313 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1312out_o;

  { // Node ID: 1312 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1312in_i = id3072out_result;

    id1312out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1312in_i));
  }
  { // Node ID: 1297 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1298out_result;

  { // Node ID: 1298 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1298in_sel = id2954out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1298in_option0 = id1294out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1298in_option1 = id1297out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1298x_1;

    switch((id1298in_sel.getValueAsLong())) {
      case 0l:
        id1298x_1 = id1298in_option0;
        break;
      case 1l:
        id1298x_1 = id1298in_option1;
        break;
      default:
        id1298x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1298out_result = (id1298x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1299out_o;

  { // Node ID: 1299 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1299in_i = id1298out_result;

    id1299out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1299in_i));
  }
  HWRawBits<56> id1314out_result;

  { // Node ID: 1314 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1314in_in0 = id1313out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1314in_in1 = id1312out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1314in_in2 = id1299out_o;

    id1314out_result = (cat((cat(id1314in_in0,id1314in_in1)),id1314in_in2));
  }
  HWFloat<11,45> id1315out_output;

  { // Node ID: 1315 (NodeReinterpret)
    const HWRawBits<56> &id1315in_input = id1314out_result;

    id1315out_output = (cast_bits2float<11,45>(id1315in_input));
  }
  { // Node ID: 1322 (NodeConstantRawBits)
  }
  { // Node ID: 1323 (NodeConstantRawBits)
  }
  { // Node ID: 1325 (NodeConstantRawBits)
  }
  HWRawBits<56> id2958out_result;

  { // Node ID: 2958 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2958in_in0 = id1322out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2958in_in1 = id1323out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2958in_in2 = id1325out_value;

    id2958out_result = (cat((cat(id2958in_in0,id2958in_in1)),id2958in_in2));
  }
  HWFloat<11,45> id1327out_output;

  { // Node ID: 1327 (NodeReinterpret)
    const HWRawBits<56> &id1327in_input = id2958out_result;

    id1327out_output = (cast_bits2float<11,45>(id1327in_input));
  }
  { // Node ID: 2812 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1330out_result;

  { // Node ID: 1330 (NodeMux)
    const HWRawBits<2> &id1330in_sel = id1321out_result;
    const HWFloat<11,45> &id1330in_option0 = id1315out_output;
    const HWFloat<11,45> &id1330in_option1 = id1327out_output;
    const HWFloat<11,45> &id1330in_option2 = id2812out_value;
    const HWFloat<11,45> &id1330in_option3 = id1327out_output;

    HWFloat<11,45> id1330x_1;

    switch((id1330in_sel.getValueAsLong())) {
      case 0l:
        id1330x_1 = id1330in_option0;
        break;
      case 1l:
        id1330x_1 = id1330in_option1;
        break;
      case 2l:
        id1330x_1 = id1330in_option2;
        break;
      case 3l:
        id1330x_1 = id1330in_option3;
        break;
      default:
        id1330x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1330out_result = (id1330x_1);
  }
  { // Node ID: 3516 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1340out_result;

  { // Node ID: 1340 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1340in_sel = id3331out_output[getCycle()%9];
    const HWFloat<11,45> &id1340in_option0 = id1330out_result;
    const HWFloat<11,45> &id1340in_option1 = id3516out_value;

    HWFloat<11,45> id1340x_1;

    switch((id1340in_sel.getValueAsLong())) {
      case 0l:
        id1340x_1 = id1340in_option0;
        break;
      case 1l:
        id1340x_1 = id1340in_option1;
        break;
      default:
        id1340x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1340out_result = (id1340x_1);
  }
  { // Node ID: 3515 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1351out_result;

  { // Node ID: 1351 (NodeAdd)
    const HWFloat<11,45> &id1351in_a = id1340out_result;
    const HWFloat<11,45> &id1351in_b = id3515out_value;

    id1351out_result = (add_float(id1351in_a,id1351in_b));
  }
  HWFloat<11,45> id1352out_result;

  { // Node ID: 1352 (NodeDiv)
    const HWFloat<11,45> &id1352in_a = id1252out_result;
    const HWFloat<11,45> &id1352in_b = id1351out_result;

    id1352out_result = (div_float(id1352in_a,id1352in_b));
  }
  HWFloat<11,45> id1353out_result;

  { // Node ID: 1353 (NodeMul)
    const HWFloat<11,45> &id1353in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1353in_b = id1352out_result;

    id1353out_result = (mul_float(id1353in_a,id1353in_b));
  }
  { // Node ID: 3514 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1979out_result;

  { // Node ID: 1979 (NodeSub)
    const HWFloat<11,45> &id1979in_a = id3514out_value;
    const HWFloat<11,45> &id1979in_b = id3332out_output[getCycle()%9];

    id1979out_result = (sub_float(id1979in_a,id1979in_b));
  }
  HWFloat<11,45> id1980out_result;

  { // Node ID: 1980 (NodeMul)
    const HWFloat<11,45> &id1980in_a = id1353out_result;
    const HWFloat<11,45> &id1980in_b = id1979out_result;

    id1980out_result = (mul_float(id1980in_a,id1980in_b));
  }
  { // Node ID: 3513 (NodeConstantRawBits)
  }
  { // Node ID: 3512 (NodeConstantRawBits)
  }
  { // Node ID: 3511 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1355out_result;

  { // Node ID: 1355 (NodeAdd)
    const HWFloat<11,45> &id1355in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1355in_b = id3511out_value;

    id1355out_result = (add_float(id1355in_a,id1355in_b));
  }
  HWFloat<11,45> id1357out_result;

  { // Node ID: 1357 (NodeMul)
    const HWFloat<11,45> &id1357in_a = id3512out_value;
    const HWFloat<11,45> &id1357in_b = id1355out_result;

    id1357out_result = (mul_float(id1357in_a,id1357in_b));
  }
  HWRawBits<11> id1434out_result;

  { // Node ID: 1434 (NodeSlice)
    const HWFloat<11,45> &id1434in_a = id1357out_result;

    id1434out_result = (slice<44,11>(id1434in_a));
  }
  { // Node ID: 1435 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2959out_result;

  { // Node ID: 2959 (NodeEqInlined)
    const HWRawBits<11> &id2959in_a = id1434out_result;
    const HWRawBits<11> &id2959in_b = id1435out_value;

    id2959out_result = (eq_bits(id2959in_a,id2959in_b));
  }
  HWRawBits<44> id1433out_result;

  { // Node ID: 1433 (NodeSlice)
    const HWFloat<11,45> &id1433in_a = id1357out_result;

    id1433out_result = (slice<0,44>(id1433in_a));
  }
  { // Node ID: 3510 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2960out_result;

  { // Node ID: 2960 (NodeNeqInlined)
    const HWRawBits<44> &id2960in_a = id1433out_result;
    const HWRawBits<44> &id2960in_b = id3510out_value;

    id2960out_result = (neq_bits(id2960in_a,id2960in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1439out_result;

  { // Node ID: 1439 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1439in_a = id2959out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1439in_b = id2960out_result;

    HWOffsetFix<1,0,UNSIGNED> id1439x_1;

    (id1439x_1) = (and_fixed(id1439in_a,id1439in_b));
    id1439out_result = (id1439x_1);
  }
  { // Node ID: 3342 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3342in_input = id1439out_result;

    id3342out_output[(getCycle()+8)%9] = id3342in_input;
  }
  { // Node ID: 1358 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1359out_output;
  HWOffsetFix<1,0,UNSIGNED> id1359out_output_doubt;

  { // Node ID: 1359 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1359in_input = id1357out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1359in_doubt = id1358out_value;

    id1359out_output = id1359in_input;
    id1359out_output_doubt = id1359in_doubt;
  }
  { // Node ID: 1360 (NodeCast)
    const HWFloat<11,45> &id1360in_i = id1359out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1360in_i_doubt = id1359out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1360x_1;

    id1360out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1360in_i,(&(id1360x_1))));
    id1360out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1360x_1),(c_hw_fix_4_0_uns_bits))),id1360in_i_doubt));
  }
  { // Node ID: 1363 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1362out_result;
  HWOffsetFix<1,0,UNSIGNED> id1362out_result_doubt;

  { // Node ID: 1362 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1362in_a = id1360out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1362in_a_doubt = id1360out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1362in_b = id1363out_value;

    HWOffsetFix<1,0,UNSIGNED> id1362x_1;

    id1362out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1362in_a,id1362in_b,(&(id1362x_1))));
    id1362out_result_doubt = (or_fixed((neq_fixed((id1362x_1),(c_hw_fix_1_0_uns_bits_1))),id1362in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1364out_o;
  HWOffsetFix<1,0,UNSIGNED> id1364out_o_doubt;

  { // Node ID: 1364 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1364in_i = id1362out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1364in_i_doubt = id1362out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1364x_1;

    id1364out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1364in_i,(&(id1364x_1))));
    id1364out_o_doubt = (or_fixed((neq_fixed((id1364x_1),(c_hw_fix_1_0_uns_bits_1))),id1364in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1373out_output;

  { // Node ID: 1373 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1373in_input = id1364out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1373in_input_doubt = id1364out_o_doubt;

    id1373out_output = id1373in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1374out_o;

  { // Node ID: 1374 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1374in_i = id1373out_output;

    id1374out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1374in_i));
  }
  { // Node ID: 3333 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3333in_input = id1374out_o;

    id3333out_output[(getCycle()+2)%3] = id3333in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1401out_o;

  { // Node ID: 1401 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1401in_i = id3333out_output[getCycle()%3];

    id1401out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1401in_i));
  }
  { // Node ID: 1404 (NodeConstantRawBits)
  }
  { // Node ID: 3052 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1377out_o;

  { // Node ID: 1377 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1377in_i = id1373out_output;

    id1377out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1377in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1449out_output;

  { // Node ID: 1449 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1449in_input = id1377out_o;

    id1449out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1449in_input))));
  }
  { // Node ID: 1450 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1450in_addr = id1449out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1450x_1;

    switch(((long)((id1450in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1450x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1450x_1 = (id1450sta_rom_store[(id1450in_addr.getValueAsLong())]);
        break;
      default:
        id1450x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1450out_dout[(getCycle()+2)%3] = (id1450x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1376out_o;

  { // Node ID: 1376 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1376in_i = id1373out_output;

    id1376out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1376in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1446out_output;

  { // Node ID: 1446 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1446in_input = id1376out_o;

    id1446out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1446in_input))));
  }
  { // Node ID: 1447 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1447in_addr = id1446out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1447x_1;

    switch(((long)((id1447in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1447x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1447x_1 = (id1447sta_rom_store[(id1447in_addr.getValueAsLong())]);
        break;
      default:
        id1447x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1447out_dout[(getCycle()+2)%3] = (id1447x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1375out_o;

  { // Node ID: 1375 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1375in_i = id1373out_output;

    id1375out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1375in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1443out_output;

  { // Node ID: 1443 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1443in_input = id1375out_o;

    id1443out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1443in_input))));
  }
  { // Node ID: 1444 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1444in_addr = id1443out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1444x_1;

    switch(((long)((id1444in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1444x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1444x_1 = (id1444sta_rom_store[(id1444in_addr.getValueAsLong())]);
        break;
      default:
        id1444x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1444out_dout[(getCycle()+2)%3] = (id1444x_1);
  }
  { // Node ID: 1381 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1378out_o;

  { // Node ID: 1378 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1378in_i = id1373out_output;

    id1378out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1378in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1380out_result;

  { // Node ID: 1380 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1380in_a = id1381out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1380in_b = id1378out_o;

    id1380out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1380in_a,id1380in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1382out_o;

  { // Node ID: 1382 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1382in_i = id1380out_result;

    id1382out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1382in_i));
  }
  { // Node ID: 3334 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3334in_input = id1382out_o;

    id3334out_output[(getCycle()+2)%3] = id3334in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1383out_result;

  { // Node ID: 1383 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1383in_a = id1444out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1383in_b = id3334out_output[getCycle()%3];

    id1383out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1383in_a,id1383in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1384out_result;

  { // Node ID: 1384 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1384in_a = id3334out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1384in_b = id1444out_dout[getCycle()%3];

    id1384out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1384in_a,id1384in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1385out_result;

  { // Node ID: 1385 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1385in_a = id1383out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1385in_b = id1384out_result;

    id1385out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1385in_a,id1385in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1386out_o;

  { // Node ID: 1386 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1386in_i = id1385out_result;

    id1386out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1386in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1387out_result;

  { // Node ID: 1387 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1387in_a = id1447out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1387in_b = id1386out_o;

    id1387out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1387in_a,id1387in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1388out_result;

  { // Node ID: 1388 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1388in_a = id1386out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1388in_b = id1447out_dout[getCycle()%3];

    id1388out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1388in_a,id1388in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1389out_result;

  { // Node ID: 1389 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1389in_a = id1387out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1389in_b = id1388out_result;

    id1389out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1389in_a,id1389in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1390out_o;

  { // Node ID: 1390 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1390in_i = id1389out_result;

    id1390out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1390in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1391out_result;

  { // Node ID: 1391 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1391in_a = id1450out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1391in_b = id1390out_o;

    id1391out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1391in_a,id1391in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1392out_result;

  { // Node ID: 1392 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1392in_a = id1390out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1392in_b = id1450out_dout[getCycle()%3];

    id1392out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1392in_a,id1392in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1393out_result;

  { // Node ID: 1393 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1393in_a = id1391out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1393in_b = id1392out_result;

    id1393out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1393in_a,id1393in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1394out_o;

  { // Node ID: 1394 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1394in_i = id1393out_result;

    id1394out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1394in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1395out_o;

  { // Node ID: 1395 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1395in_i = id1394out_o;

    id1395out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1395in_i));
  }
  { // Node ID: 3509 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2961out_result;

  { // Node ID: 2961 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2961in_a = id1395out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2961in_b = id3509out_value;

    id2961out_result = (gte_fixed(id2961in_a,id2961in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3071out_result;

  { // Node ID: 3071 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3071in_a = id1401out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3071in_b = id1404out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3071in_c = id3052out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3071in_condb = id2961out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3071x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3071x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3071x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3071x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3071x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3071x_1 = id3071in_a;
        break;
      default:
        id3071x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3071in_condb.getValueAsLong())) {
      case 0l:
        id3071x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3071x_2 = id3071in_b;
        break;
      default:
        id3071x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3071x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3071x_3 = id3071in_c;
        break;
      default:
        id3071x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3071x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3071x_1),(id3071x_2))),(id3071x_3)));
    id3071out_result = (id3071x_4);
  }
  HWRawBits<1> id2962out_result;

  { // Node ID: 2962 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2962in_a = id3071out_result;

    id2962out_result = (slice<13,1>(id2962in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2963out_output;

  { // Node ID: 2963 (NodeReinterpret)
    const HWRawBits<1> &id2963in_input = id2962out_result;

    id2963out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2963in_input));
  }
  { // Node ID: 3508 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1366out_result;

  { // Node ID: 1366 (NodeGt)
    const HWFloat<11,45> &id1366in_a = id1357out_result;
    const HWFloat<11,45> &id1366in_b = id3508out_value;

    id1366out_result = (gt_float(id1366in_a,id1366in_b));
  }
  { // Node ID: 3336 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3336in_input = id1366out_result;

    id3336out_output[(getCycle()+6)%7] = id3336in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1367out_output;

  { // Node ID: 1367 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1367in_input = id1364out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1367in_input_doubt = id1364out_o_doubt;

    id1367out_output = id1367in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1368out_result;

  { // Node ID: 1368 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1368in_a = id3336out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1368in_b = id1367out_output;

    HWOffsetFix<1,0,UNSIGNED> id1368x_1;

    (id1368x_1) = (and_fixed(id1368in_a,id1368in_b));
    id1368out_result = (id1368x_1);
  }
  { // Node ID: 3337 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3337in_input = id1368out_result;

    id3337out_output[(getCycle()+2)%3] = id3337in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1410out_result;

  { // Node ID: 1410 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1410in_a = id3337out_output[getCycle()%3];

    id1410out_result = (not_fixed(id1410in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1411out_result;

  { // Node ID: 1411 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1411in_a = id2963out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1411in_b = id1410out_result;

    HWOffsetFix<1,0,UNSIGNED> id1411x_1;

    (id1411x_1) = (and_fixed(id1411in_a,id1411in_b));
    id1411out_result = (id1411x_1);
  }
  { // Node ID: 3507 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1370out_result;

  { // Node ID: 1370 (NodeLt)
    const HWFloat<11,45> &id1370in_a = id1357out_result;
    const HWFloat<11,45> &id1370in_b = id3507out_value;

    id1370out_result = (lt_float(id1370in_a,id1370in_b));
  }
  { // Node ID: 3338 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3338in_input = id1370out_result;

    id3338out_output[(getCycle()+6)%7] = id3338in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1371out_output;

  { // Node ID: 1371 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1371in_input = id1364out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1371in_input_doubt = id1364out_o_doubt;

    id1371out_output = id1371in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1372out_result;

  { // Node ID: 1372 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1372in_a = id3338out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1372in_b = id1371out_output;

    HWOffsetFix<1,0,UNSIGNED> id1372x_1;

    (id1372x_1) = (and_fixed(id1372in_a,id1372in_b));
    id1372out_result = (id1372x_1);
  }
  { // Node ID: 3339 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3339in_input = id1372out_result;

    id3339out_output[(getCycle()+2)%3] = id3339in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1412out_result;

  { // Node ID: 1412 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1412in_a = id1411out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1412in_b = id3339out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1412x_1;

    (id1412x_1) = (or_fixed(id1412in_a,id1412in_b));
    id1412out_result = (id1412x_1);
  }
  { // Node ID: 3506 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2964out_result;

  { // Node ID: 2964 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2964in_a = id3071out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2964in_b = id3506out_value;

    id2964out_result = (gte_fixed(id2964in_a,id2964in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1419out_result;

  { // Node ID: 1419 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1419in_a = id3339out_output[getCycle()%3];

    id1419out_result = (not_fixed(id1419in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1420out_result;

  { // Node ID: 1420 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1420in_a = id2964out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1420in_b = id1419out_result;

    HWOffsetFix<1,0,UNSIGNED> id1420x_1;

    (id1420x_1) = (and_fixed(id1420in_a,id1420in_b));
    id1420out_result = (id1420x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1421out_result;

  { // Node ID: 1421 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1421in_a = id1420out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1421in_b = id3337out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1421x_1;

    (id1421x_1) = (or_fixed(id1421in_a,id1421in_b));
    id1421out_result = (id1421x_1);
  }
  HWRawBits<2> id1422out_result;

  { // Node ID: 1422 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1422in_in0 = id1412out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1422in_in1 = id1421out_result;

    id1422out_result = (cat(id1422in_in0,id1422in_in1));
  }
  { // Node ID: 1414 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1413out_o;

  { // Node ID: 1413 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1413in_i = id3071out_result;

    id1413out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1413in_i));
  }
  { // Node ID: 1398 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1399out_result;

  { // Node ID: 1399 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1399in_sel = id2961out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1399in_option0 = id1395out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1399in_option1 = id1398out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1399x_1;

    switch((id1399in_sel.getValueAsLong())) {
      case 0l:
        id1399x_1 = id1399in_option0;
        break;
      case 1l:
        id1399x_1 = id1399in_option1;
        break;
      default:
        id1399x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1399out_result = (id1399x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1400out_o;

  { // Node ID: 1400 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1400in_i = id1399out_result;

    id1400out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1400in_i));
  }
  HWRawBits<56> id1415out_result;

  { // Node ID: 1415 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1415in_in0 = id1414out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1415in_in1 = id1413out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1415in_in2 = id1400out_o;

    id1415out_result = (cat((cat(id1415in_in0,id1415in_in1)),id1415in_in2));
  }
  HWFloat<11,45> id1416out_output;

  { // Node ID: 1416 (NodeReinterpret)
    const HWRawBits<56> &id1416in_input = id1415out_result;

    id1416out_output = (cast_bits2float<11,45>(id1416in_input));
  }
  { // Node ID: 1423 (NodeConstantRawBits)
  }
  { // Node ID: 1424 (NodeConstantRawBits)
  }
  { // Node ID: 1426 (NodeConstantRawBits)
  }
  HWRawBits<56> id2965out_result;

  { // Node ID: 2965 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2965in_in0 = id1423out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2965in_in1 = id1424out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2965in_in2 = id1426out_value;

    id2965out_result = (cat((cat(id2965in_in0,id2965in_in1)),id2965in_in2));
  }
  HWFloat<11,45> id1428out_output;

  { // Node ID: 1428 (NodeReinterpret)
    const HWRawBits<56> &id1428in_input = id2965out_result;

    id1428out_output = (cast_bits2float<11,45>(id1428in_input));
  }
  { // Node ID: 2813 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1431out_result;

  { // Node ID: 1431 (NodeMux)
    const HWRawBits<2> &id1431in_sel = id1422out_result;
    const HWFloat<11,45> &id1431in_option0 = id1416out_output;
    const HWFloat<11,45> &id1431in_option1 = id1428out_output;
    const HWFloat<11,45> &id1431in_option2 = id2813out_value;
    const HWFloat<11,45> &id1431in_option3 = id1428out_output;

    HWFloat<11,45> id1431x_1;

    switch((id1431in_sel.getValueAsLong())) {
      case 0l:
        id1431x_1 = id1431in_option0;
        break;
      case 1l:
        id1431x_1 = id1431in_option1;
        break;
      case 2l:
        id1431x_1 = id1431in_option2;
        break;
      case 3l:
        id1431x_1 = id1431in_option3;
        break;
      default:
        id1431x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1431out_result = (id1431x_1);
  }
  { // Node ID: 3505 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1441out_result;

  { // Node ID: 1441 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1441in_sel = id3342out_output[getCycle()%9];
    const HWFloat<11,45> &id1441in_option0 = id1431out_result;
    const HWFloat<11,45> &id1441in_option1 = id3505out_value;

    HWFloat<11,45> id1441x_1;

    switch((id1441in_sel.getValueAsLong())) {
      case 0l:
        id1441x_1 = id1441in_option0;
        break;
      case 1l:
        id1441x_1 = id1441in_option1;
        break;
      default:
        id1441x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1441out_result = (id1441x_1);
  }
  HWFloat<11,45> id1452out_result;

  { // Node ID: 1452 (NodeMul)
    const HWFloat<11,45> &id1452in_a = id3513out_value;
    const HWFloat<11,45> &id1452in_b = id1441out_result;

    id1452out_result = (mul_float(id1452in_a,id1452in_b));
  }
  { // Node ID: 3504 (NodeConstantRawBits)
  }
  { // Node ID: 3503 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1454out_result;

  { // Node ID: 1454 (NodeAdd)
    const HWFloat<11,45> &id1454in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1454in_b = id3503out_value;

    id1454out_result = (add_float(id1454in_a,id1454in_b));
  }
  HWFloat<11,45> id1456out_result;

  { // Node ID: 1456 (NodeMul)
    const HWFloat<11,45> &id1456in_a = id3504out_value;
    const HWFloat<11,45> &id1456in_b = id1454out_result;

    id1456out_result = (mul_float(id1456in_a,id1456in_b));
  }
  HWRawBits<11> id1533out_result;

  { // Node ID: 1533 (NodeSlice)
    const HWFloat<11,45> &id1533in_a = id1456out_result;

    id1533out_result = (slice<44,11>(id1533in_a));
  }
  { // Node ID: 1534 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2966out_result;

  { // Node ID: 2966 (NodeEqInlined)
    const HWRawBits<11> &id2966in_a = id1533out_result;
    const HWRawBits<11> &id2966in_b = id1534out_value;

    id2966out_result = (eq_bits(id2966in_a,id2966in_b));
  }
  HWRawBits<44> id1532out_result;

  { // Node ID: 1532 (NodeSlice)
    const HWFloat<11,45> &id1532in_a = id1456out_result;

    id1532out_result = (slice<0,44>(id1532in_a));
  }
  { // Node ID: 3502 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2967out_result;

  { // Node ID: 2967 (NodeNeqInlined)
    const HWRawBits<44> &id2967in_a = id1532out_result;
    const HWRawBits<44> &id2967in_b = id3502out_value;

    id2967out_result = (neq_bits(id2967in_a,id2967in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1538out_result;

  { // Node ID: 1538 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1538in_a = id2966out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1538in_b = id2967out_result;

    HWOffsetFix<1,0,UNSIGNED> id1538x_1;

    (id1538x_1) = (and_fixed(id1538in_a,id1538in_b));
    id1538out_result = (id1538x_1);
  }
  { // Node ID: 3352 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3352in_input = id1538out_result;

    id3352out_output[(getCycle()+8)%9] = id3352in_input;
  }
  { // Node ID: 1457 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1458out_output;
  HWOffsetFix<1,0,UNSIGNED> id1458out_output_doubt;

  { // Node ID: 1458 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1458in_input = id1456out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1458in_doubt = id1457out_value;

    id1458out_output = id1458in_input;
    id1458out_output_doubt = id1458in_doubt;
  }
  { // Node ID: 1459 (NodeCast)
    const HWFloat<11,45> &id1459in_i = id1458out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1459in_i_doubt = id1458out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1459x_1;

    id1459out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1459in_i,(&(id1459x_1))));
    id1459out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1459x_1),(c_hw_fix_4_0_uns_bits))),id1459in_i_doubt));
  }
  { // Node ID: 1462 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1461out_result;
  HWOffsetFix<1,0,UNSIGNED> id1461out_result_doubt;

  { // Node ID: 1461 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1461in_a = id1459out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1461in_a_doubt = id1459out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1461in_b = id1462out_value;

    HWOffsetFix<1,0,UNSIGNED> id1461x_1;

    id1461out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1461in_a,id1461in_b,(&(id1461x_1))));
    id1461out_result_doubt = (or_fixed((neq_fixed((id1461x_1),(c_hw_fix_1_0_uns_bits_1))),id1461in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1463out_o;
  HWOffsetFix<1,0,UNSIGNED> id1463out_o_doubt;

  { // Node ID: 1463 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1463in_i = id1461out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1463in_i_doubt = id1461out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1463x_1;

    id1463out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1463in_i,(&(id1463x_1))));
    id1463out_o_doubt = (or_fixed((neq_fixed((id1463x_1),(c_hw_fix_1_0_uns_bits_1))),id1463in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1472out_output;

  { // Node ID: 1472 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1472in_input = id1463out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1472in_input_doubt = id1463out_o_doubt;

    id1472out_output = id1472in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1473out_o;

  { // Node ID: 1473 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1473in_i = id1472out_output;

    id1473out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1473in_i));
  }
  { // Node ID: 3343 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3343in_input = id1473out_o;

    id3343out_output[(getCycle()+2)%3] = id3343in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1500out_o;

  { // Node ID: 1500 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1500in_i = id3343out_output[getCycle()%3];

    id1500out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1500in_i));
  }
  { // Node ID: 1503 (NodeConstantRawBits)
  }
  { // Node ID: 3054 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1476out_o;

  { // Node ID: 1476 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1476in_i = id1472out_output;

    id1476out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1476in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1548out_output;

  { // Node ID: 1548 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1548in_input = id1476out_o;

    id1548out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1548in_input))));
  }
  { // Node ID: 1549 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1549in_addr = id1548out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1549x_1;

    switch(((long)((id1549in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1549x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1549x_1 = (id1549sta_rom_store[(id1549in_addr.getValueAsLong())]);
        break;
      default:
        id1549x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1549out_dout[(getCycle()+2)%3] = (id1549x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1475out_o;

  { // Node ID: 1475 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1475in_i = id1472out_output;

    id1475out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1475in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1545out_output;

  { // Node ID: 1545 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1545in_input = id1475out_o;

    id1545out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1545in_input))));
  }
  { // Node ID: 1546 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1546in_addr = id1545out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1546x_1;

    switch(((long)((id1546in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1546x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1546x_1 = (id1546sta_rom_store[(id1546in_addr.getValueAsLong())]);
        break;
      default:
        id1546x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1546out_dout[(getCycle()+2)%3] = (id1546x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1474out_o;

  { // Node ID: 1474 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1474in_i = id1472out_output;

    id1474out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1474in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1542out_output;

  { // Node ID: 1542 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1542in_input = id1474out_o;

    id1542out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1542in_input))));
  }
  { // Node ID: 1543 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1543in_addr = id1542out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1543x_1;

    switch(((long)((id1543in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1543x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1543x_1 = (id1543sta_rom_store[(id1543in_addr.getValueAsLong())]);
        break;
      default:
        id1543x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1543out_dout[(getCycle()+2)%3] = (id1543x_1);
  }
  { // Node ID: 1480 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1477out_o;

  { // Node ID: 1477 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1477in_i = id1472out_output;

    id1477out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1477in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1479out_result;

  { // Node ID: 1479 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1479in_a = id1480out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1479in_b = id1477out_o;

    id1479out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1479in_a,id1479in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1481out_o;

  { // Node ID: 1481 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1481in_i = id1479out_result;

    id1481out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1481in_i));
  }
  { // Node ID: 3344 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3344in_input = id1481out_o;

    id3344out_output[(getCycle()+2)%3] = id3344in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1482out_result;

  { // Node ID: 1482 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1482in_a = id1543out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1482in_b = id3344out_output[getCycle()%3];

    id1482out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1482in_a,id1482in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1483out_result;

  { // Node ID: 1483 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1483in_a = id3344out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1483in_b = id1543out_dout[getCycle()%3];

    id1483out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1483in_a,id1483in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1484out_result;

  { // Node ID: 1484 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1484in_a = id1482out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1484in_b = id1483out_result;

    id1484out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1484in_a,id1484in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1485out_o;

  { // Node ID: 1485 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1485in_i = id1484out_result;

    id1485out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1485in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1486out_result;

  { // Node ID: 1486 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1486in_a = id1546out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1486in_b = id1485out_o;

    id1486out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1486in_a,id1486in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1487out_result;

  { // Node ID: 1487 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1487in_a = id1485out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1487in_b = id1546out_dout[getCycle()%3];

    id1487out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1487in_a,id1487in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1488out_result;

  { // Node ID: 1488 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1488in_a = id1486out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1488in_b = id1487out_result;

    id1488out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1488in_a,id1488in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1489out_o;

  { // Node ID: 1489 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1489in_i = id1488out_result;

    id1489out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1489in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1490out_result;

  { // Node ID: 1490 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1490in_a = id1549out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1490in_b = id1489out_o;

    id1490out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1490in_a,id1490in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1491out_result;

  { // Node ID: 1491 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1491in_a = id1489out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1491in_b = id1549out_dout[getCycle()%3];

    id1491out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1491in_a,id1491in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1492out_result;

  { // Node ID: 1492 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1492in_a = id1490out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1492in_b = id1491out_result;

    id1492out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1492in_a,id1492in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1493out_o;

  { // Node ID: 1493 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1493in_i = id1492out_result;

    id1493out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1493in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1494out_o;

  { // Node ID: 1494 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1494in_i = id1493out_o;

    id1494out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1494in_i));
  }
  { // Node ID: 3501 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2968out_result;

  { // Node ID: 2968 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2968in_a = id1494out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2968in_b = id3501out_value;

    id2968out_result = (gte_fixed(id2968in_a,id2968in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3070out_result;

  { // Node ID: 3070 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3070in_a = id1500out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3070in_b = id1503out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3070in_c = id3054out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3070in_condb = id2968out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3070x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3070x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3070x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3070x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3070x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3070x_1 = id3070in_a;
        break;
      default:
        id3070x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3070in_condb.getValueAsLong())) {
      case 0l:
        id3070x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3070x_2 = id3070in_b;
        break;
      default:
        id3070x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3070x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3070x_3 = id3070in_c;
        break;
      default:
        id3070x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3070x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3070x_1),(id3070x_2))),(id3070x_3)));
    id3070out_result = (id3070x_4);
  }
  HWRawBits<1> id2969out_result;

  { // Node ID: 2969 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2969in_a = id3070out_result;

    id2969out_result = (slice<13,1>(id2969in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2970out_output;

  { // Node ID: 2970 (NodeReinterpret)
    const HWRawBits<1> &id2970in_input = id2969out_result;

    id2970out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2970in_input));
  }
  { // Node ID: 3500 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1465out_result;

  { // Node ID: 1465 (NodeGt)
    const HWFloat<11,45> &id1465in_a = id1456out_result;
    const HWFloat<11,45> &id1465in_b = id3500out_value;

    id1465out_result = (gt_float(id1465in_a,id1465in_b));
  }
  { // Node ID: 3346 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3346in_input = id1465out_result;

    id3346out_output[(getCycle()+6)%7] = id3346in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1466out_output;

  { // Node ID: 1466 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1466in_input = id1463out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1466in_input_doubt = id1463out_o_doubt;

    id1466out_output = id1466in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1467out_result;

  { // Node ID: 1467 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1467in_a = id3346out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1467in_b = id1466out_output;

    HWOffsetFix<1,0,UNSIGNED> id1467x_1;

    (id1467x_1) = (and_fixed(id1467in_a,id1467in_b));
    id1467out_result = (id1467x_1);
  }
  { // Node ID: 3347 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3347in_input = id1467out_result;

    id3347out_output[(getCycle()+2)%3] = id3347in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1509out_result;

  { // Node ID: 1509 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1509in_a = id3347out_output[getCycle()%3];

    id1509out_result = (not_fixed(id1509in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1510out_result;

  { // Node ID: 1510 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1510in_a = id2970out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1510in_b = id1509out_result;

    HWOffsetFix<1,0,UNSIGNED> id1510x_1;

    (id1510x_1) = (and_fixed(id1510in_a,id1510in_b));
    id1510out_result = (id1510x_1);
  }
  { // Node ID: 3499 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1469out_result;

  { // Node ID: 1469 (NodeLt)
    const HWFloat<11,45> &id1469in_a = id1456out_result;
    const HWFloat<11,45> &id1469in_b = id3499out_value;

    id1469out_result = (lt_float(id1469in_a,id1469in_b));
  }
  { // Node ID: 3348 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3348in_input = id1469out_result;

    id3348out_output[(getCycle()+6)%7] = id3348in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1470out_output;

  { // Node ID: 1470 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1470in_input = id1463out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1470in_input_doubt = id1463out_o_doubt;

    id1470out_output = id1470in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1471out_result;

  { // Node ID: 1471 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1471in_a = id3348out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1471in_b = id1470out_output;

    HWOffsetFix<1,0,UNSIGNED> id1471x_1;

    (id1471x_1) = (and_fixed(id1471in_a,id1471in_b));
    id1471out_result = (id1471x_1);
  }
  { // Node ID: 3349 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3349in_input = id1471out_result;

    id3349out_output[(getCycle()+2)%3] = id3349in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1511out_result;

  { // Node ID: 1511 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1511in_a = id1510out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1511in_b = id3349out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1511x_1;

    (id1511x_1) = (or_fixed(id1511in_a,id1511in_b));
    id1511out_result = (id1511x_1);
  }
  { // Node ID: 3498 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2971out_result;

  { // Node ID: 2971 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2971in_a = id3070out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2971in_b = id3498out_value;

    id2971out_result = (gte_fixed(id2971in_a,id2971in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1518out_result;

  { // Node ID: 1518 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1518in_a = id3349out_output[getCycle()%3];

    id1518out_result = (not_fixed(id1518in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1519out_result;

  { // Node ID: 1519 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1519in_a = id2971out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1519in_b = id1518out_result;

    HWOffsetFix<1,0,UNSIGNED> id1519x_1;

    (id1519x_1) = (and_fixed(id1519in_a,id1519in_b));
    id1519out_result = (id1519x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1520out_result;

  { // Node ID: 1520 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1520in_a = id1519out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1520in_b = id3347out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1520x_1;

    (id1520x_1) = (or_fixed(id1520in_a,id1520in_b));
    id1520out_result = (id1520x_1);
  }
  HWRawBits<2> id1521out_result;

  { // Node ID: 1521 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1521in_in0 = id1511out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1521in_in1 = id1520out_result;

    id1521out_result = (cat(id1521in_in0,id1521in_in1));
  }
  { // Node ID: 1513 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1512out_o;

  { // Node ID: 1512 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1512in_i = id3070out_result;

    id1512out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1512in_i));
  }
  { // Node ID: 1497 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1498out_result;

  { // Node ID: 1498 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1498in_sel = id2968out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1498in_option0 = id1494out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1498in_option1 = id1497out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1498x_1;

    switch((id1498in_sel.getValueAsLong())) {
      case 0l:
        id1498x_1 = id1498in_option0;
        break;
      case 1l:
        id1498x_1 = id1498in_option1;
        break;
      default:
        id1498x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1498out_result = (id1498x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1499out_o;

  { // Node ID: 1499 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1499in_i = id1498out_result;

    id1499out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1499in_i));
  }
  HWRawBits<56> id1514out_result;

  { // Node ID: 1514 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1514in_in0 = id1513out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1514in_in1 = id1512out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1514in_in2 = id1499out_o;

    id1514out_result = (cat((cat(id1514in_in0,id1514in_in1)),id1514in_in2));
  }
  HWFloat<11,45> id1515out_output;

  { // Node ID: 1515 (NodeReinterpret)
    const HWRawBits<56> &id1515in_input = id1514out_result;

    id1515out_output = (cast_bits2float<11,45>(id1515in_input));
  }
  { // Node ID: 1522 (NodeConstantRawBits)
  }
  { // Node ID: 1523 (NodeConstantRawBits)
  }
  { // Node ID: 1525 (NodeConstantRawBits)
  }
  HWRawBits<56> id2972out_result;

  { // Node ID: 2972 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2972in_in0 = id1522out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2972in_in1 = id1523out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2972in_in2 = id1525out_value;

    id2972out_result = (cat((cat(id2972in_in0,id2972in_in1)),id2972in_in2));
  }
  HWFloat<11,45> id1527out_output;

  { // Node ID: 1527 (NodeReinterpret)
    const HWRawBits<56> &id1527in_input = id2972out_result;

    id1527out_output = (cast_bits2float<11,45>(id1527in_input));
  }
  { // Node ID: 2814 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1530out_result;

  { // Node ID: 1530 (NodeMux)
    const HWRawBits<2> &id1530in_sel = id1521out_result;
    const HWFloat<11,45> &id1530in_option0 = id1515out_output;
    const HWFloat<11,45> &id1530in_option1 = id1527out_output;
    const HWFloat<11,45> &id1530in_option2 = id2814out_value;
    const HWFloat<11,45> &id1530in_option3 = id1527out_output;

    HWFloat<11,45> id1530x_1;

    switch((id1530in_sel.getValueAsLong())) {
      case 0l:
        id1530x_1 = id1530in_option0;
        break;
      case 1l:
        id1530x_1 = id1530in_option1;
        break;
      case 2l:
        id1530x_1 = id1530in_option2;
        break;
      case 3l:
        id1530x_1 = id1530in_option3;
        break;
      default:
        id1530x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1530out_result = (id1530x_1);
  }
  { // Node ID: 3497 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1540out_result;

  { // Node ID: 1540 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1540in_sel = id3352out_output[getCycle()%9];
    const HWFloat<11,45> &id1540in_option0 = id1530out_result;
    const HWFloat<11,45> &id1540in_option1 = id3497out_value;

    HWFloat<11,45> id1540x_1;

    switch((id1540in_sel.getValueAsLong())) {
      case 0l:
        id1540x_1 = id1540in_option0;
        break;
      case 1l:
        id1540x_1 = id1540in_option1;
        break;
      default:
        id1540x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1540out_result = (id1540x_1);
  }
  { // Node ID: 3496 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1551out_result;

  { // Node ID: 1551 (NodeAdd)
    const HWFloat<11,45> &id1551in_a = id1540out_result;
    const HWFloat<11,45> &id1551in_b = id3496out_value;

    id1551out_result = (add_float(id1551in_a,id1551in_b));
  }
  HWFloat<11,45> id1552out_result;

  { // Node ID: 1552 (NodeDiv)
    const HWFloat<11,45> &id1552in_a = id1452out_result;
    const HWFloat<11,45> &id1552in_b = id1551out_result;

    id1552out_result = (div_float(id1552in_a,id1552in_b));
  }
  HWFloat<11,45> id1553out_result;

  { // Node ID: 1553 (NodeMul)
    const HWFloat<11,45> &id1553in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1553in_b = id1552out_result;

    id1553out_result = (mul_float(id1553in_a,id1553in_b));
  }
  HWFloat<11,45> id1981out_result;

  { // Node ID: 1981 (NodeMul)
    const HWFloat<11,45> &id1981in_a = id1553out_result;
    const HWFloat<11,45> &id1981in_b = id3332out_output[getCycle()%9];

    id1981out_result = (mul_float(id1981in_a,id1981in_b));
  }
  HWFloat<11,45> id1982out_result;

  { // Node ID: 1982 (NodeSub)
    const HWFloat<11,45> &id1982in_a = id1980out_result;
    const HWFloat<11,45> &id1982in_b = id1981out_result;

    id1982out_result = (sub_float(id1982in_a,id1982in_b));
  }
  HWFloat<11,45> id1983out_result;

  { // Node ID: 1983 (NodeAdd)
    const HWFloat<11,45> &id1983in_a = id3332out_output[getCycle()%9];
    const HWFloat<11,45> &id1983in_b = id1982out_result;

    id1983out_result = (add_float(id1983in_a,id1983in_b));
  }
  { // Node ID: 3311 (NodeFIFO)
    const HWFloat<11,45> &id3311in_input = id1983out_result;

    id3311out_output[(getCycle()+1)%2] = id3311in_input;
  }
  { // Node ID: 3495 (NodeConstantRawBits)
  }
  { // Node ID: 2973 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id2973in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id2973in_b = id3495out_value;

    id2973out_result[(getCycle()+1)%2] = (eq_fixed(id2973in_a,id2973in_b));
  }
  HWFloat<11,45> id3110out_output;

  { // Node ID: 3110 (NodeStreamOffset)
    const HWFloat<11,45> &id3110in_input = id3356out_output[getCycle()%2];

    id3110out_output = id3110in_input;
  }
  { // Node ID: 32 (NodeInputMappedReg)
  }
  { // Node ID: 33 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id33in_sel = id2973out_result[getCycle()%2];
    const HWFloat<11,45> &id33in_option0 = id3110out_output;
    const HWFloat<11,45> &id33in_option1 = id32out_f_in;

    HWFloat<11,45> id33x_1;

    switch((id33in_sel.getValueAsLong())) {
      case 0l:
        id33x_1 = id33in_option0;
        break;
      case 1l:
        id33x_1 = id33in_option1;
        break;
      default:
        id33x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id33out_result[(getCycle()+1)%2] = (id33x_1);
  }
  { // Node ID: 3377 (NodeFIFO)
    const HWFloat<11,45> &id3377in_input = id33out_result[getCycle()%2];

    id3377out_output[(getCycle()+8)%9] = id3377in_input;
  }
  { // Node ID: 3494 (NodeConstantRawBits)
  }
  { // Node ID: 3493 (NodeConstantRawBits)
  }
  { // Node ID: 3492 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1555out_result;

  { // Node ID: 1555 (NodeAdd)
    const HWFloat<11,45> &id1555in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1555in_b = id3492out_value;

    id1555out_result = (add_float(id1555in_a,id1555in_b));
  }
  HWFloat<11,45> id1557out_result;

  { // Node ID: 1557 (NodeMul)
    const HWFloat<11,45> &id1557in_a = id3493out_value;
    const HWFloat<11,45> &id1557in_b = id1555out_result;

    id1557out_result = (mul_float(id1557in_a,id1557in_b));
  }
  HWRawBits<11> id1634out_result;

  { // Node ID: 1634 (NodeSlice)
    const HWFloat<11,45> &id1634in_a = id1557out_result;

    id1634out_result = (slice<44,11>(id1634in_a));
  }
  { // Node ID: 1635 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2974out_result;

  { // Node ID: 2974 (NodeEqInlined)
    const HWRawBits<11> &id2974in_a = id1634out_result;
    const HWRawBits<11> &id2974in_b = id1635out_value;

    id2974out_result = (eq_bits(id2974in_a,id2974in_b));
  }
  HWRawBits<44> id1633out_result;

  { // Node ID: 1633 (NodeSlice)
    const HWFloat<11,45> &id1633in_a = id1557out_result;

    id1633out_result = (slice<0,44>(id1633in_a));
  }
  { // Node ID: 3491 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2975out_result;

  { // Node ID: 2975 (NodeNeqInlined)
    const HWRawBits<44> &id2975in_a = id1633out_result;
    const HWRawBits<44> &id2975in_b = id3491out_value;

    id2975out_result = (neq_bits(id2975in_a,id2975in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1639out_result;

  { // Node ID: 1639 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1639in_a = id2974out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1639in_b = id2975out_result;

    HWOffsetFix<1,0,UNSIGNED> id1639x_1;

    (id1639x_1) = (and_fixed(id1639in_a,id1639in_b));
    id1639out_result = (id1639x_1);
  }
  { // Node ID: 3366 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3366in_input = id1639out_result;

    id3366out_output[(getCycle()+8)%9] = id3366in_input;
  }
  { // Node ID: 1558 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1559out_output;
  HWOffsetFix<1,0,UNSIGNED> id1559out_output_doubt;

  { // Node ID: 1559 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1559in_input = id1557out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1559in_doubt = id1558out_value;

    id1559out_output = id1559in_input;
    id1559out_output_doubt = id1559in_doubt;
  }
  { // Node ID: 1560 (NodeCast)
    const HWFloat<11,45> &id1560in_i = id1559out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1560in_i_doubt = id1559out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1560x_1;

    id1560out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1560in_i,(&(id1560x_1))));
    id1560out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1560x_1),(c_hw_fix_4_0_uns_bits))),id1560in_i_doubt));
  }
  { // Node ID: 1563 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1562out_result;
  HWOffsetFix<1,0,UNSIGNED> id1562out_result_doubt;

  { // Node ID: 1562 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1562in_a = id1560out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1562in_a_doubt = id1560out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1562in_b = id1563out_value;

    HWOffsetFix<1,0,UNSIGNED> id1562x_1;

    id1562out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1562in_a,id1562in_b,(&(id1562x_1))));
    id1562out_result_doubt = (or_fixed((neq_fixed((id1562x_1),(c_hw_fix_1_0_uns_bits_1))),id1562in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1564out_o;
  HWOffsetFix<1,0,UNSIGNED> id1564out_o_doubt;

  { // Node ID: 1564 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1564in_i = id1562out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1564in_i_doubt = id1562out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1564x_1;

    id1564out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1564in_i,(&(id1564x_1))));
    id1564out_o_doubt = (or_fixed((neq_fixed((id1564x_1),(c_hw_fix_1_0_uns_bits_1))),id1564in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1573out_output;

  { // Node ID: 1573 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1573in_input = id1564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1573in_input_doubt = id1564out_o_doubt;

    id1573out_output = id1573in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1574out_o;

  { // Node ID: 1574 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1574in_i = id1573out_output;

    id1574out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1574in_i));
  }
  { // Node ID: 3357 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3357in_input = id1574out_o;

    id3357out_output[(getCycle()+2)%3] = id3357in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1601out_o;

  { // Node ID: 1601 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1601in_i = id3357out_output[getCycle()%3];

    id1601out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1601in_i));
  }
  { // Node ID: 1604 (NodeConstantRawBits)
  }
  { // Node ID: 3056 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1577out_o;

  { // Node ID: 1577 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1577in_i = id1573out_output;

    id1577out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1577in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1649out_output;

  { // Node ID: 1649 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1649in_input = id1577out_o;

    id1649out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1649in_input))));
  }
  { // Node ID: 1650 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1650in_addr = id1649out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1650x_1;

    switch(((long)((id1650in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1650x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1650x_1 = (id1650sta_rom_store[(id1650in_addr.getValueAsLong())]);
        break;
      default:
        id1650x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1650out_dout[(getCycle()+2)%3] = (id1650x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1576out_o;

  { // Node ID: 1576 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1576in_i = id1573out_output;

    id1576out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1576in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1646out_output;

  { // Node ID: 1646 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1646in_input = id1576out_o;

    id1646out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1646in_input))));
  }
  { // Node ID: 1647 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1647in_addr = id1646out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1647x_1;

    switch(((long)((id1647in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1647x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1647x_1 = (id1647sta_rom_store[(id1647in_addr.getValueAsLong())]);
        break;
      default:
        id1647x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1647out_dout[(getCycle()+2)%3] = (id1647x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1575out_o;

  { // Node ID: 1575 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1575in_i = id1573out_output;

    id1575out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1575in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1643out_output;

  { // Node ID: 1643 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1643in_input = id1575out_o;

    id1643out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1643in_input))));
  }
  { // Node ID: 1644 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1644in_addr = id1643out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1644x_1;

    switch(((long)((id1644in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1644x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1644x_1 = (id1644sta_rom_store[(id1644in_addr.getValueAsLong())]);
        break;
      default:
        id1644x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1644out_dout[(getCycle()+2)%3] = (id1644x_1);
  }
  { // Node ID: 1581 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1578out_o;

  { // Node ID: 1578 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1578in_i = id1573out_output;

    id1578out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1578in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1580out_result;

  { // Node ID: 1580 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1580in_a = id1581out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1580in_b = id1578out_o;

    id1580out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1580in_a,id1580in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1582out_o;

  { // Node ID: 1582 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1582in_i = id1580out_result;

    id1582out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1582in_i));
  }
  { // Node ID: 3358 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3358in_input = id1582out_o;

    id3358out_output[(getCycle()+2)%3] = id3358in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1583out_result;

  { // Node ID: 1583 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1583in_a = id1644out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1583in_b = id3358out_output[getCycle()%3];

    id1583out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1583in_a,id1583in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1584out_result;

  { // Node ID: 1584 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1584in_a = id3358out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1584in_b = id1644out_dout[getCycle()%3];

    id1584out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1584in_a,id1584in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1585out_result;

  { // Node ID: 1585 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1585in_a = id1583out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1585in_b = id1584out_result;

    id1585out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1585in_a,id1585in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1586out_o;

  { // Node ID: 1586 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1586in_i = id1585out_result;

    id1586out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1586in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1587out_result;

  { // Node ID: 1587 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1587in_a = id1647out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1587in_b = id1586out_o;

    id1587out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1587in_a,id1587in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1588out_result;

  { // Node ID: 1588 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1588in_a = id1586out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1588in_b = id1647out_dout[getCycle()%3];

    id1588out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1588in_a,id1588in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1589out_result;

  { // Node ID: 1589 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1589in_a = id1587out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1589in_b = id1588out_result;

    id1589out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1589in_a,id1589in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1590out_o;

  { // Node ID: 1590 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1590in_i = id1589out_result;

    id1590out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1590in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1591out_result;

  { // Node ID: 1591 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1591in_a = id1650out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1591in_b = id1590out_o;

    id1591out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1591in_a,id1591in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1592out_result;

  { // Node ID: 1592 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1592in_a = id1590out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1592in_b = id1650out_dout[getCycle()%3];

    id1592out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1592in_a,id1592in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1593out_result;

  { // Node ID: 1593 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1593in_a = id1591out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1593in_b = id1592out_result;

    id1593out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1593in_a,id1593in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1594out_o;

  { // Node ID: 1594 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1594in_i = id1593out_result;

    id1594out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1594in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1595out_o;

  { // Node ID: 1595 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1595in_i = id1594out_o;

    id1595out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1595in_i));
  }
  { // Node ID: 3490 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2976out_result;

  { // Node ID: 2976 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2976in_a = id1595out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2976in_b = id3490out_value;

    id2976out_result = (gte_fixed(id2976in_a,id2976in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3069out_result;

  { // Node ID: 3069 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3069in_a = id1601out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3069in_b = id1604out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3069in_c = id3056out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3069in_condb = id2976out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3069x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3069x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3069x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3069x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3069x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3069x_1 = id3069in_a;
        break;
      default:
        id3069x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3069in_condb.getValueAsLong())) {
      case 0l:
        id3069x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3069x_2 = id3069in_b;
        break;
      default:
        id3069x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3069x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3069x_3 = id3069in_c;
        break;
      default:
        id3069x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3069x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3069x_1),(id3069x_2))),(id3069x_3)));
    id3069out_result = (id3069x_4);
  }
  HWRawBits<1> id2977out_result;

  { // Node ID: 2977 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2977in_a = id3069out_result;

    id2977out_result = (slice<13,1>(id2977in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2978out_output;

  { // Node ID: 2978 (NodeReinterpret)
    const HWRawBits<1> &id2978in_input = id2977out_result;

    id2978out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2978in_input));
  }
  { // Node ID: 3489 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1566out_result;

  { // Node ID: 1566 (NodeGt)
    const HWFloat<11,45> &id1566in_a = id1557out_result;
    const HWFloat<11,45> &id1566in_b = id3489out_value;

    id1566out_result = (gt_float(id1566in_a,id1566in_b));
  }
  { // Node ID: 3360 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3360in_input = id1566out_result;

    id3360out_output[(getCycle()+6)%7] = id3360in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1567out_output;

  { // Node ID: 1567 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1567in_input = id1564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1567in_input_doubt = id1564out_o_doubt;

    id1567out_output = id1567in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1568out_result;

  { // Node ID: 1568 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1568in_a = id3360out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1568in_b = id1567out_output;

    HWOffsetFix<1,0,UNSIGNED> id1568x_1;

    (id1568x_1) = (and_fixed(id1568in_a,id1568in_b));
    id1568out_result = (id1568x_1);
  }
  { // Node ID: 3361 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3361in_input = id1568out_result;

    id3361out_output[(getCycle()+2)%3] = id3361in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1610out_result;

  { // Node ID: 1610 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1610in_a = id3361out_output[getCycle()%3];

    id1610out_result = (not_fixed(id1610in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1611out_result;

  { // Node ID: 1611 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1611in_a = id2978out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1611in_b = id1610out_result;

    HWOffsetFix<1,0,UNSIGNED> id1611x_1;

    (id1611x_1) = (and_fixed(id1611in_a,id1611in_b));
    id1611out_result = (id1611x_1);
  }
  { // Node ID: 3488 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1570out_result;

  { // Node ID: 1570 (NodeLt)
    const HWFloat<11,45> &id1570in_a = id1557out_result;
    const HWFloat<11,45> &id1570in_b = id3488out_value;

    id1570out_result = (lt_float(id1570in_a,id1570in_b));
  }
  { // Node ID: 3362 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3362in_input = id1570out_result;

    id3362out_output[(getCycle()+6)%7] = id3362in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1571out_output;

  { // Node ID: 1571 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1571in_input = id1564out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1571in_input_doubt = id1564out_o_doubt;

    id1571out_output = id1571in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1572out_result;

  { // Node ID: 1572 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1572in_a = id3362out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1572in_b = id1571out_output;

    HWOffsetFix<1,0,UNSIGNED> id1572x_1;

    (id1572x_1) = (and_fixed(id1572in_a,id1572in_b));
    id1572out_result = (id1572x_1);
  }
  { // Node ID: 3363 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3363in_input = id1572out_result;

    id3363out_output[(getCycle()+2)%3] = id3363in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1612out_result;

  { // Node ID: 1612 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1612in_a = id1611out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1612in_b = id3363out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1612x_1;

    (id1612x_1) = (or_fixed(id1612in_a,id1612in_b));
    id1612out_result = (id1612x_1);
  }
  { // Node ID: 3487 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2979out_result;

  { // Node ID: 2979 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2979in_a = id3069out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2979in_b = id3487out_value;

    id2979out_result = (gte_fixed(id2979in_a,id2979in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1619out_result;

  { // Node ID: 1619 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1619in_a = id3363out_output[getCycle()%3];

    id1619out_result = (not_fixed(id1619in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1620out_result;

  { // Node ID: 1620 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1620in_a = id2979out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1620in_b = id1619out_result;

    HWOffsetFix<1,0,UNSIGNED> id1620x_1;

    (id1620x_1) = (and_fixed(id1620in_a,id1620in_b));
    id1620out_result = (id1620x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1621out_result;

  { // Node ID: 1621 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1621in_a = id1620out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1621in_b = id3361out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1621x_1;

    (id1621x_1) = (or_fixed(id1621in_a,id1621in_b));
    id1621out_result = (id1621x_1);
  }
  HWRawBits<2> id1622out_result;

  { // Node ID: 1622 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1622in_in0 = id1612out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1622in_in1 = id1621out_result;

    id1622out_result = (cat(id1622in_in0,id1622in_in1));
  }
  { // Node ID: 1614 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1613out_o;

  { // Node ID: 1613 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1613in_i = id3069out_result;

    id1613out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1613in_i));
  }
  { // Node ID: 1598 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1599out_result;

  { // Node ID: 1599 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1599in_sel = id2976out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1599in_option0 = id1595out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1599in_option1 = id1598out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1599x_1;

    switch((id1599in_sel.getValueAsLong())) {
      case 0l:
        id1599x_1 = id1599in_option0;
        break;
      case 1l:
        id1599x_1 = id1599in_option1;
        break;
      default:
        id1599x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1599out_result = (id1599x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1600out_o;

  { // Node ID: 1600 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1600in_i = id1599out_result;

    id1600out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1600in_i));
  }
  HWRawBits<56> id1615out_result;

  { // Node ID: 1615 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1615in_in0 = id1614out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1615in_in1 = id1613out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1615in_in2 = id1600out_o;

    id1615out_result = (cat((cat(id1615in_in0,id1615in_in1)),id1615in_in2));
  }
  HWFloat<11,45> id1616out_output;

  { // Node ID: 1616 (NodeReinterpret)
    const HWRawBits<56> &id1616in_input = id1615out_result;

    id1616out_output = (cast_bits2float<11,45>(id1616in_input));
  }
  { // Node ID: 1623 (NodeConstantRawBits)
  }
  { // Node ID: 1624 (NodeConstantRawBits)
  }
  { // Node ID: 1626 (NodeConstantRawBits)
  }
  HWRawBits<56> id2980out_result;

  { // Node ID: 2980 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2980in_in0 = id1623out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2980in_in1 = id1624out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2980in_in2 = id1626out_value;

    id2980out_result = (cat((cat(id2980in_in0,id2980in_in1)),id2980in_in2));
  }
  HWFloat<11,45> id1628out_output;

  { // Node ID: 1628 (NodeReinterpret)
    const HWRawBits<56> &id1628in_input = id2980out_result;

    id1628out_output = (cast_bits2float<11,45>(id1628in_input));
  }
  { // Node ID: 2815 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1631out_result;

  { // Node ID: 1631 (NodeMux)
    const HWRawBits<2> &id1631in_sel = id1622out_result;
    const HWFloat<11,45> &id1631in_option0 = id1616out_output;
    const HWFloat<11,45> &id1631in_option1 = id1628out_output;
    const HWFloat<11,45> &id1631in_option2 = id2815out_value;
    const HWFloat<11,45> &id1631in_option3 = id1628out_output;

    HWFloat<11,45> id1631x_1;

    switch((id1631in_sel.getValueAsLong())) {
      case 0l:
        id1631x_1 = id1631in_option0;
        break;
      case 1l:
        id1631x_1 = id1631in_option1;
        break;
      case 2l:
        id1631x_1 = id1631in_option2;
        break;
      case 3l:
        id1631x_1 = id1631in_option3;
        break;
      default:
        id1631x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1631out_result = (id1631x_1);
  }
  { // Node ID: 3486 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1641out_result;

  { // Node ID: 1641 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1641in_sel = id3366out_output[getCycle()%9];
    const HWFloat<11,45> &id1641in_option0 = id1631out_result;
    const HWFloat<11,45> &id1641in_option1 = id3486out_value;

    HWFloat<11,45> id1641x_1;

    switch((id1641in_sel.getValueAsLong())) {
      case 0l:
        id1641x_1 = id1641in_option0;
        break;
      case 1l:
        id1641x_1 = id1641in_option1;
        break;
      default:
        id1641x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1641out_result = (id1641x_1);
  }
  HWFloat<11,45> id1652out_result;

  { // Node ID: 1652 (NodeMul)
    const HWFloat<11,45> &id1652in_a = id3494out_value;
    const HWFloat<11,45> &id1652in_b = id1641out_result;

    id1652out_result = (mul_float(id1652in_a,id1652in_b));
  }
  { // Node ID: 3485 (NodeConstantRawBits)
  }
  { // Node ID: 3484 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1654out_result;

  { // Node ID: 1654 (NodeAdd)
    const HWFloat<11,45> &id1654in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1654in_b = id3484out_value;

    id1654out_result = (add_float(id1654in_a,id1654in_b));
  }
  HWFloat<11,45> id1656out_result;

  { // Node ID: 1656 (NodeMul)
    const HWFloat<11,45> &id1656in_a = id3485out_value;
    const HWFloat<11,45> &id1656in_b = id1654out_result;

    id1656out_result = (mul_float(id1656in_a,id1656in_b));
  }
  HWRawBits<11> id1733out_result;

  { // Node ID: 1733 (NodeSlice)
    const HWFloat<11,45> &id1733in_a = id1656out_result;

    id1733out_result = (slice<44,11>(id1733in_a));
  }
  { // Node ID: 1734 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2981out_result;

  { // Node ID: 2981 (NodeEqInlined)
    const HWRawBits<11> &id2981in_a = id1733out_result;
    const HWRawBits<11> &id2981in_b = id1734out_value;

    id2981out_result = (eq_bits(id2981in_a,id2981in_b));
  }
  HWRawBits<44> id1732out_result;

  { // Node ID: 1732 (NodeSlice)
    const HWFloat<11,45> &id1732in_a = id1656out_result;

    id1732out_result = (slice<0,44>(id1732in_a));
  }
  { // Node ID: 3483 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2982out_result;

  { // Node ID: 2982 (NodeNeqInlined)
    const HWRawBits<44> &id2982in_a = id1732out_result;
    const HWRawBits<44> &id2982in_b = id3483out_value;

    id2982out_result = (neq_bits(id2982in_a,id2982in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1738out_result;

  { // Node ID: 1738 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1738in_a = id2981out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1738in_b = id2982out_result;

    HWOffsetFix<1,0,UNSIGNED> id1738x_1;

    (id1738x_1) = (and_fixed(id1738in_a,id1738in_b));
    id1738out_result = (id1738x_1);
  }
  { // Node ID: 3376 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3376in_input = id1738out_result;

    id3376out_output[(getCycle()+8)%9] = id3376in_input;
  }
  { // Node ID: 1657 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1658out_output;
  HWOffsetFix<1,0,UNSIGNED> id1658out_output_doubt;

  { // Node ID: 1658 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1658in_input = id1656out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1658in_doubt = id1657out_value;

    id1658out_output = id1658in_input;
    id1658out_output_doubt = id1658in_doubt;
  }
  { // Node ID: 1659 (NodeCast)
    const HWFloat<11,45> &id1659in_i = id1658out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1659in_i_doubt = id1658out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1659x_1;

    id1659out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1659in_i,(&(id1659x_1))));
    id1659out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1659x_1),(c_hw_fix_4_0_uns_bits))),id1659in_i_doubt));
  }
  { // Node ID: 1662 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1661out_result;
  HWOffsetFix<1,0,UNSIGNED> id1661out_result_doubt;

  { // Node ID: 1661 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1661in_a = id1659out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1661in_a_doubt = id1659out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1661in_b = id1662out_value;

    HWOffsetFix<1,0,UNSIGNED> id1661x_1;

    id1661out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1661in_a,id1661in_b,(&(id1661x_1))));
    id1661out_result_doubt = (or_fixed((neq_fixed((id1661x_1),(c_hw_fix_1_0_uns_bits_1))),id1661in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1663out_o;
  HWOffsetFix<1,0,UNSIGNED> id1663out_o_doubt;

  { // Node ID: 1663 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1663in_i = id1661out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1663in_i_doubt = id1661out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1663x_1;

    id1663out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1663in_i,(&(id1663x_1))));
    id1663out_o_doubt = (or_fixed((neq_fixed((id1663x_1),(c_hw_fix_1_0_uns_bits_1))),id1663in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1672out_output;

  { // Node ID: 1672 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1672in_input = id1663out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1672in_input_doubt = id1663out_o_doubt;

    id1672out_output = id1672in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1673out_o;

  { // Node ID: 1673 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1673in_i = id1672out_output;

    id1673out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1673in_i));
  }
  { // Node ID: 3367 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3367in_input = id1673out_o;

    id3367out_output[(getCycle()+2)%3] = id3367in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1700out_o;

  { // Node ID: 1700 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1700in_i = id3367out_output[getCycle()%3];

    id1700out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1700in_i));
  }
  { // Node ID: 1703 (NodeConstantRawBits)
  }
  { // Node ID: 3058 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1676out_o;

  { // Node ID: 1676 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1676in_i = id1672out_output;

    id1676out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1676in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1748out_output;

  { // Node ID: 1748 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1748in_input = id1676out_o;

    id1748out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1748in_input))));
  }
  { // Node ID: 1749 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1749in_addr = id1748out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1749x_1;

    switch(((long)((id1749in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1749x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1749x_1 = (id1749sta_rom_store[(id1749in_addr.getValueAsLong())]);
        break;
      default:
        id1749x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1749out_dout[(getCycle()+2)%3] = (id1749x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1675out_o;

  { // Node ID: 1675 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1675in_i = id1672out_output;

    id1675out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1675in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1745out_output;

  { // Node ID: 1745 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1745in_input = id1675out_o;

    id1745out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1745in_input))));
  }
  { // Node ID: 1746 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1746in_addr = id1745out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1746x_1;

    switch(((long)((id1746in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1746x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1746x_1 = (id1746sta_rom_store[(id1746in_addr.getValueAsLong())]);
        break;
      default:
        id1746x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1746out_dout[(getCycle()+2)%3] = (id1746x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1674out_o;

  { // Node ID: 1674 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1674in_i = id1672out_output;

    id1674out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1674in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1742out_output;

  { // Node ID: 1742 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1742in_input = id1674out_o;

    id1742out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1742in_input))));
  }
  { // Node ID: 1743 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1743in_addr = id1742out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1743x_1;

    switch(((long)((id1743in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1743x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1743x_1 = (id1743sta_rom_store[(id1743in_addr.getValueAsLong())]);
        break;
      default:
        id1743x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1743out_dout[(getCycle()+2)%3] = (id1743x_1);
  }
  { // Node ID: 1680 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1677out_o;

  { // Node ID: 1677 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1677in_i = id1672out_output;

    id1677out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1677in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1679out_result;

  { // Node ID: 1679 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1679in_a = id1680out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1679in_b = id1677out_o;

    id1679out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1679in_a,id1679in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1681out_o;

  { // Node ID: 1681 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1681in_i = id1679out_result;

    id1681out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1681in_i));
  }
  { // Node ID: 3368 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3368in_input = id1681out_o;

    id3368out_output[(getCycle()+2)%3] = id3368in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1682out_result;

  { // Node ID: 1682 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1682in_a = id1743out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1682in_b = id3368out_output[getCycle()%3];

    id1682out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1682in_a,id1682in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1683out_result;

  { // Node ID: 1683 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1683in_a = id3368out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1683in_b = id1743out_dout[getCycle()%3];

    id1683out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1683in_a,id1683in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1684out_result;

  { // Node ID: 1684 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1684in_a = id1682out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1684in_b = id1683out_result;

    id1684out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1684in_a,id1684in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1685out_o;

  { // Node ID: 1685 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1685in_i = id1684out_result;

    id1685out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1685in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1686out_result;

  { // Node ID: 1686 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1686in_a = id1746out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1686in_b = id1685out_o;

    id1686out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1686in_a,id1686in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1687out_result;

  { // Node ID: 1687 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1687in_a = id1685out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1687in_b = id1746out_dout[getCycle()%3];

    id1687out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1687in_a,id1687in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1688out_result;

  { // Node ID: 1688 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1688in_a = id1686out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1688in_b = id1687out_result;

    id1688out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1688in_a,id1688in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1689out_o;

  { // Node ID: 1689 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1689in_i = id1688out_result;

    id1689out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1689in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1690out_result;

  { // Node ID: 1690 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1690in_a = id1749out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1690in_b = id1689out_o;

    id1690out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1690in_a,id1690in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1691out_result;

  { // Node ID: 1691 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1691in_a = id1689out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1691in_b = id1749out_dout[getCycle()%3];

    id1691out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1691in_a,id1691in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1692out_result;

  { // Node ID: 1692 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1692in_a = id1690out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1692in_b = id1691out_result;

    id1692out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1692in_a,id1692in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1693out_o;

  { // Node ID: 1693 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1693in_i = id1692out_result;

    id1693out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1693in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1694out_o;

  { // Node ID: 1694 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1694in_i = id1693out_o;

    id1694out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1694in_i));
  }
  { // Node ID: 3482 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2983out_result;

  { // Node ID: 2983 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2983in_a = id1694out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2983in_b = id3482out_value;

    id2983out_result = (gte_fixed(id2983in_a,id2983in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3068out_result;

  { // Node ID: 3068 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3068in_a = id1700out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3068in_b = id1703out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3068in_c = id3058out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3068in_condb = id2983out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3068x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3068x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3068x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3068x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3068x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3068x_1 = id3068in_a;
        break;
      default:
        id3068x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3068in_condb.getValueAsLong())) {
      case 0l:
        id3068x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3068x_2 = id3068in_b;
        break;
      default:
        id3068x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3068x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3068x_3 = id3068in_c;
        break;
      default:
        id3068x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3068x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3068x_1),(id3068x_2))),(id3068x_3)));
    id3068out_result = (id3068x_4);
  }
  HWRawBits<1> id2984out_result;

  { // Node ID: 2984 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2984in_a = id3068out_result;

    id2984out_result = (slice<13,1>(id2984in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2985out_output;

  { // Node ID: 2985 (NodeReinterpret)
    const HWRawBits<1> &id2985in_input = id2984out_result;

    id2985out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2985in_input));
  }
  { // Node ID: 3481 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1665out_result;

  { // Node ID: 1665 (NodeGt)
    const HWFloat<11,45> &id1665in_a = id1656out_result;
    const HWFloat<11,45> &id1665in_b = id3481out_value;

    id1665out_result = (gt_float(id1665in_a,id1665in_b));
  }
  { // Node ID: 3370 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3370in_input = id1665out_result;

    id3370out_output[(getCycle()+6)%7] = id3370in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1666out_output;

  { // Node ID: 1666 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1666in_input = id1663out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1666in_input_doubt = id1663out_o_doubt;

    id1666out_output = id1666in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1667out_result;

  { // Node ID: 1667 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1667in_a = id3370out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1667in_b = id1666out_output;

    HWOffsetFix<1,0,UNSIGNED> id1667x_1;

    (id1667x_1) = (and_fixed(id1667in_a,id1667in_b));
    id1667out_result = (id1667x_1);
  }
  { // Node ID: 3371 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3371in_input = id1667out_result;

    id3371out_output[(getCycle()+2)%3] = id3371in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1709out_result;

  { // Node ID: 1709 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1709in_a = id3371out_output[getCycle()%3];

    id1709out_result = (not_fixed(id1709in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1710out_result;

  { // Node ID: 1710 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1710in_a = id2985out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1710in_b = id1709out_result;

    HWOffsetFix<1,0,UNSIGNED> id1710x_1;

    (id1710x_1) = (and_fixed(id1710in_a,id1710in_b));
    id1710out_result = (id1710x_1);
  }
  { // Node ID: 3480 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1669out_result;

  { // Node ID: 1669 (NodeLt)
    const HWFloat<11,45> &id1669in_a = id1656out_result;
    const HWFloat<11,45> &id1669in_b = id3480out_value;

    id1669out_result = (lt_float(id1669in_a,id1669in_b));
  }
  { // Node ID: 3372 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3372in_input = id1669out_result;

    id3372out_output[(getCycle()+6)%7] = id3372in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1670out_output;

  { // Node ID: 1670 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1670in_input = id1663out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1670in_input_doubt = id1663out_o_doubt;

    id1670out_output = id1670in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1671out_result;

  { // Node ID: 1671 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1671in_a = id3372out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1671in_b = id1670out_output;

    HWOffsetFix<1,0,UNSIGNED> id1671x_1;

    (id1671x_1) = (and_fixed(id1671in_a,id1671in_b));
    id1671out_result = (id1671x_1);
  }
  { // Node ID: 3373 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3373in_input = id1671out_result;

    id3373out_output[(getCycle()+2)%3] = id3373in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1711out_result;

  { // Node ID: 1711 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1711in_a = id1710out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1711in_b = id3373out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1711x_1;

    (id1711x_1) = (or_fixed(id1711in_a,id1711in_b));
    id1711out_result = (id1711x_1);
  }
  { // Node ID: 3479 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2986out_result;

  { // Node ID: 2986 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2986in_a = id3068out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2986in_b = id3479out_value;

    id2986out_result = (gte_fixed(id2986in_a,id2986in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1718out_result;

  { // Node ID: 1718 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1718in_a = id3373out_output[getCycle()%3];

    id1718out_result = (not_fixed(id1718in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1719out_result;

  { // Node ID: 1719 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1719in_a = id2986out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1719in_b = id1718out_result;

    HWOffsetFix<1,0,UNSIGNED> id1719x_1;

    (id1719x_1) = (and_fixed(id1719in_a,id1719in_b));
    id1719out_result = (id1719x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1720out_result;

  { // Node ID: 1720 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1720in_a = id1719out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1720in_b = id3371out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1720x_1;

    (id1720x_1) = (or_fixed(id1720in_a,id1720in_b));
    id1720out_result = (id1720x_1);
  }
  HWRawBits<2> id1721out_result;

  { // Node ID: 1721 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1721in_in0 = id1711out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1721in_in1 = id1720out_result;

    id1721out_result = (cat(id1721in_in0,id1721in_in1));
  }
  { // Node ID: 1713 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1712out_o;

  { // Node ID: 1712 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1712in_i = id3068out_result;

    id1712out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1712in_i));
  }
  { // Node ID: 1697 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1698out_result;

  { // Node ID: 1698 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1698in_sel = id2983out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1698in_option0 = id1694out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1698in_option1 = id1697out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1698x_1;

    switch((id1698in_sel.getValueAsLong())) {
      case 0l:
        id1698x_1 = id1698in_option0;
        break;
      case 1l:
        id1698x_1 = id1698in_option1;
        break;
      default:
        id1698x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1698out_result = (id1698x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1699out_o;

  { // Node ID: 1699 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1699in_i = id1698out_result;

    id1699out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1699in_i));
  }
  HWRawBits<56> id1714out_result;

  { // Node ID: 1714 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1714in_in0 = id1713out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1714in_in1 = id1712out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1714in_in2 = id1699out_o;

    id1714out_result = (cat((cat(id1714in_in0,id1714in_in1)),id1714in_in2));
  }
  HWFloat<11,45> id1715out_output;

  { // Node ID: 1715 (NodeReinterpret)
    const HWRawBits<56> &id1715in_input = id1714out_result;

    id1715out_output = (cast_bits2float<11,45>(id1715in_input));
  }
  { // Node ID: 1722 (NodeConstantRawBits)
  }
  { // Node ID: 1723 (NodeConstantRawBits)
  }
  { // Node ID: 1725 (NodeConstantRawBits)
  }
  HWRawBits<56> id2987out_result;

  { // Node ID: 2987 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2987in_in0 = id1722out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2987in_in1 = id1723out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2987in_in2 = id1725out_value;

    id2987out_result = (cat((cat(id2987in_in0,id2987in_in1)),id2987in_in2));
  }
  HWFloat<11,45> id1727out_output;

  { // Node ID: 1727 (NodeReinterpret)
    const HWRawBits<56> &id1727in_input = id2987out_result;

    id1727out_output = (cast_bits2float<11,45>(id1727in_input));
  }
  { // Node ID: 2816 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1730out_result;

  { // Node ID: 1730 (NodeMux)
    const HWRawBits<2> &id1730in_sel = id1721out_result;
    const HWFloat<11,45> &id1730in_option0 = id1715out_output;
    const HWFloat<11,45> &id1730in_option1 = id1727out_output;
    const HWFloat<11,45> &id1730in_option2 = id2816out_value;
    const HWFloat<11,45> &id1730in_option3 = id1727out_output;

    HWFloat<11,45> id1730x_1;

    switch((id1730in_sel.getValueAsLong())) {
      case 0l:
        id1730x_1 = id1730in_option0;
        break;
      case 1l:
        id1730x_1 = id1730in_option1;
        break;
      case 2l:
        id1730x_1 = id1730in_option2;
        break;
      case 3l:
        id1730x_1 = id1730in_option3;
        break;
      default:
        id1730x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1730out_result = (id1730x_1);
  }
  { // Node ID: 3478 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1740out_result;

  { // Node ID: 1740 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1740in_sel = id3376out_output[getCycle()%9];
    const HWFloat<11,45> &id1740in_option0 = id1730out_result;
    const HWFloat<11,45> &id1740in_option1 = id3478out_value;

    HWFloat<11,45> id1740x_1;

    switch((id1740in_sel.getValueAsLong())) {
      case 0l:
        id1740x_1 = id1740in_option0;
        break;
      case 1l:
        id1740x_1 = id1740in_option1;
        break;
      default:
        id1740x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1740out_result = (id1740x_1);
  }
  { // Node ID: 3477 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1751out_result;

  { // Node ID: 1751 (NodeAdd)
    const HWFloat<11,45> &id1751in_a = id1740out_result;
    const HWFloat<11,45> &id1751in_b = id3477out_value;

    id1751out_result = (add_float(id1751in_a,id1751in_b));
  }
  HWFloat<11,45> id1752out_result;

  { // Node ID: 1752 (NodeDiv)
    const HWFloat<11,45> &id1752in_a = id1652out_result;
    const HWFloat<11,45> &id1752in_b = id1751out_result;

    id1752out_result = (div_float(id1752in_a,id1752in_b));
  }
  HWFloat<11,45> id1753out_result;

  { // Node ID: 1753 (NodeMul)
    const HWFloat<11,45> &id1753in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1753in_b = id1752out_result;

    id1753out_result = (mul_float(id1753in_a,id1753in_b));
  }
  { // Node ID: 3476 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1985out_result;

  { // Node ID: 1985 (NodeSub)
    const HWFloat<11,45> &id1985in_a = id3476out_value;
    const HWFloat<11,45> &id1985in_b = id3377out_output[getCycle()%9];

    id1985out_result = (sub_float(id1985in_a,id1985in_b));
  }
  HWFloat<11,45> id1986out_result;

  { // Node ID: 1986 (NodeMul)
    const HWFloat<11,45> &id1986in_a = id1753out_result;
    const HWFloat<11,45> &id1986in_b = id1985out_result;

    id1986out_result = (mul_float(id1986in_a,id1986in_b));
  }
  { // Node ID: 3475 (NodeConstantRawBits)
  }
  { // Node ID: 3474 (NodeConstantRawBits)
  }
  { // Node ID: 3473 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1755out_result;

  { // Node ID: 1755 (NodeAdd)
    const HWFloat<11,45> &id1755in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1755in_b = id3473out_value;

    id1755out_result = (add_float(id1755in_a,id1755in_b));
  }
  HWFloat<11,45> id1757out_result;

  { // Node ID: 1757 (NodeMul)
    const HWFloat<11,45> &id1757in_a = id3474out_value;
    const HWFloat<11,45> &id1757in_b = id1755out_result;

    id1757out_result = (mul_float(id1757in_a,id1757in_b));
  }
  HWRawBits<11> id1834out_result;

  { // Node ID: 1834 (NodeSlice)
    const HWFloat<11,45> &id1834in_a = id1757out_result;

    id1834out_result = (slice<44,11>(id1834in_a));
  }
  { // Node ID: 1835 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2988out_result;

  { // Node ID: 2988 (NodeEqInlined)
    const HWRawBits<11> &id2988in_a = id1834out_result;
    const HWRawBits<11> &id2988in_b = id1835out_value;

    id2988out_result = (eq_bits(id2988in_a,id2988in_b));
  }
  HWRawBits<44> id1833out_result;

  { // Node ID: 1833 (NodeSlice)
    const HWFloat<11,45> &id1833in_a = id1757out_result;

    id1833out_result = (slice<0,44>(id1833in_a));
  }
  { // Node ID: 3472 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2989out_result;

  { // Node ID: 2989 (NodeNeqInlined)
    const HWRawBits<44> &id2989in_a = id1833out_result;
    const HWRawBits<44> &id2989in_b = id3472out_value;

    id2989out_result = (neq_bits(id2989in_a,id2989in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1839out_result;

  { // Node ID: 1839 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1839in_a = id2988out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1839in_b = id2989out_result;

    HWOffsetFix<1,0,UNSIGNED> id1839x_1;

    (id1839x_1) = (and_fixed(id1839in_a,id1839in_b));
    id1839out_result = (id1839x_1);
  }
  { // Node ID: 3387 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3387in_input = id1839out_result;

    id3387out_output[(getCycle()+8)%9] = id3387in_input;
  }
  { // Node ID: 1758 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1759out_output;
  HWOffsetFix<1,0,UNSIGNED> id1759out_output_doubt;

  { // Node ID: 1759 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1759in_input = id1757out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1759in_doubt = id1758out_value;

    id1759out_output = id1759in_input;
    id1759out_output_doubt = id1759in_doubt;
  }
  { // Node ID: 1760 (NodeCast)
    const HWFloat<11,45> &id1760in_i = id1759out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1760in_i_doubt = id1759out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1760x_1;

    id1760out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1760in_i,(&(id1760x_1))));
    id1760out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1760x_1),(c_hw_fix_4_0_uns_bits))),id1760in_i_doubt));
  }
  { // Node ID: 1763 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1762out_result;
  HWOffsetFix<1,0,UNSIGNED> id1762out_result_doubt;

  { // Node ID: 1762 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1762in_a = id1760out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1762in_a_doubt = id1760out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1762in_b = id1763out_value;

    HWOffsetFix<1,0,UNSIGNED> id1762x_1;

    id1762out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1762in_a,id1762in_b,(&(id1762x_1))));
    id1762out_result_doubt = (or_fixed((neq_fixed((id1762x_1),(c_hw_fix_1_0_uns_bits_1))),id1762in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1764out_o;
  HWOffsetFix<1,0,UNSIGNED> id1764out_o_doubt;

  { // Node ID: 1764 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1764in_i = id1762out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1764in_i_doubt = id1762out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1764x_1;

    id1764out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1764in_i,(&(id1764x_1))));
    id1764out_o_doubt = (or_fixed((neq_fixed((id1764x_1),(c_hw_fix_1_0_uns_bits_1))),id1764in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1773out_output;

  { // Node ID: 1773 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1773in_input = id1764out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1773in_input_doubt = id1764out_o_doubt;

    id1773out_output = id1773in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1774out_o;

  { // Node ID: 1774 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1774in_i = id1773out_output;

    id1774out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1774in_i));
  }
  { // Node ID: 3378 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3378in_input = id1774out_o;

    id3378out_output[(getCycle()+2)%3] = id3378in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1801out_o;

  { // Node ID: 1801 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1801in_i = id3378out_output[getCycle()%3];

    id1801out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1801in_i));
  }
  { // Node ID: 1804 (NodeConstantRawBits)
  }
  { // Node ID: 3060 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1777out_o;

  { // Node ID: 1777 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1777in_i = id1773out_output;

    id1777out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1777in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1849out_output;

  { // Node ID: 1849 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1849in_input = id1777out_o;

    id1849out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1849in_input))));
  }
  { // Node ID: 1850 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1850in_addr = id1849out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1850x_1;

    switch(((long)((id1850in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1850x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1850x_1 = (id1850sta_rom_store[(id1850in_addr.getValueAsLong())]);
        break;
      default:
        id1850x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1850out_dout[(getCycle()+2)%3] = (id1850x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1776out_o;

  { // Node ID: 1776 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1776in_i = id1773out_output;

    id1776out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1776in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1846out_output;

  { // Node ID: 1846 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1846in_input = id1776out_o;

    id1846out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1846in_input))));
  }
  { // Node ID: 1847 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1847in_addr = id1846out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1847x_1;

    switch(((long)((id1847in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1847x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1847x_1 = (id1847sta_rom_store[(id1847in_addr.getValueAsLong())]);
        break;
      default:
        id1847x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1847out_dout[(getCycle()+2)%3] = (id1847x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1775out_o;

  { // Node ID: 1775 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1775in_i = id1773out_output;

    id1775out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1775in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1843out_output;

  { // Node ID: 1843 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1843in_input = id1775out_o;

    id1843out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1843in_input))));
  }
  { // Node ID: 1844 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1844in_addr = id1843out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1844x_1;

    switch(((long)((id1844in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1844x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1844x_1 = (id1844sta_rom_store[(id1844in_addr.getValueAsLong())]);
        break;
      default:
        id1844x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1844out_dout[(getCycle()+2)%3] = (id1844x_1);
  }
  { // Node ID: 1781 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1778out_o;

  { // Node ID: 1778 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1778in_i = id1773out_output;

    id1778out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1778in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1780out_result;

  { // Node ID: 1780 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1780in_a = id1781out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1780in_b = id1778out_o;

    id1780out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1780in_a,id1780in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1782out_o;

  { // Node ID: 1782 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1782in_i = id1780out_result;

    id1782out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1782in_i));
  }
  { // Node ID: 3379 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3379in_input = id1782out_o;

    id3379out_output[(getCycle()+2)%3] = id3379in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1783out_result;

  { // Node ID: 1783 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1783in_a = id1844out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1783in_b = id3379out_output[getCycle()%3];

    id1783out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1783in_a,id1783in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1784out_result;

  { // Node ID: 1784 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1784in_a = id3379out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1784in_b = id1844out_dout[getCycle()%3];

    id1784out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1784in_a,id1784in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1785out_result;

  { // Node ID: 1785 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1785in_a = id1783out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1785in_b = id1784out_result;

    id1785out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1785in_a,id1785in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1786out_o;

  { // Node ID: 1786 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1786in_i = id1785out_result;

    id1786out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1786in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1787out_result;

  { // Node ID: 1787 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1787in_a = id1847out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1787in_b = id1786out_o;

    id1787out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1787in_a,id1787in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1788out_result;

  { // Node ID: 1788 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1788in_a = id1786out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1788in_b = id1847out_dout[getCycle()%3];

    id1788out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1788in_a,id1788in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1789out_result;

  { // Node ID: 1789 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1789in_a = id1787out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1789in_b = id1788out_result;

    id1789out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1789in_a,id1789in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1790out_o;

  { // Node ID: 1790 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1790in_i = id1789out_result;

    id1790out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1790in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1791out_result;

  { // Node ID: 1791 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1791in_a = id1850out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1791in_b = id1790out_o;

    id1791out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1791in_a,id1791in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1792out_result;

  { // Node ID: 1792 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1792in_a = id1790out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1792in_b = id1850out_dout[getCycle()%3];

    id1792out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1792in_a,id1792in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1793out_result;

  { // Node ID: 1793 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1793in_a = id1791out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1793in_b = id1792out_result;

    id1793out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1793in_a,id1793in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1794out_o;

  { // Node ID: 1794 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1794in_i = id1793out_result;

    id1794out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1794in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1795out_o;

  { // Node ID: 1795 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1795in_i = id1794out_o;

    id1795out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1795in_i));
  }
  { // Node ID: 3471 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2990out_result;

  { // Node ID: 2990 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2990in_a = id1795out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2990in_b = id3471out_value;

    id2990out_result = (gte_fixed(id2990in_a,id2990in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3067out_result;

  { // Node ID: 3067 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3067in_a = id1801out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3067in_b = id1804out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3067in_c = id3060out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3067in_condb = id2990out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3067x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3067x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3067x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3067x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3067x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3067x_1 = id3067in_a;
        break;
      default:
        id3067x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3067in_condb.getValueAsLong())) {
      case 0l:
        id3067x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3067x_2 = id3067in_b;
        break;
      default:
        id3067x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3067x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3067x_3 = id3067in_c;
        break;
      default:
        id3067x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3067x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3067x_1),(id3067x_2))),(id3067x_3)));
    id3067out_result = (id3067x_4);
  }
  HWRawBits<1> id2991out_result;

  { // Node ID: 2991 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2991in_a = id3067out_result;

    id2991out_result = (slice<13,1>(id2991in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2992out_output;

  { // Node ID: 2992 (NodeReinterpret)
    const HWRawBits<1> &id2992in_input = id2991out_result;

    id2992out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2992in_input));
  }
  { // Node ID: 3470 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1766out_result;

  { // Node ID: 1766 (NodeGt)
    const HWFloat<11,45> &id1766in_a = id1757out_result;
    const HWFloat<11,45> &id1766in_b = id3470out_value;

    id1766out_result = (gt_float(id1766in_a,id1766in_b));
  }
  { // Node ID: 3381 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3381in_input = id1766out_result;

    id3381out_output[(getCycle()+6)%7] = id3381in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1767out_output;

  { // Node ID: 1767 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1767in_input = id1764out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1767in_input_doubt = id1764out_o_doubt;

    id1767out_output = id1767in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1768out_result;

  { // Node ID: 1768 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1768in_a = id3381out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1768in_b = id1767out_output;

    HWOffsetFix<1,0,UNSIGNED> id1768x_1;

    (id1768x_1) = (and_fixed(id1768in_a,id1768in_b));
    id1768out_result = (id1768x_1);
  }
  { // Node ID: 3382 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3382in_input = id1768out_result;

    id3382out_output[(getCycle()+2)%3] = id3382in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1810out_result;

  { // Node ID: 1810 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1810in_a = id3382out_output[getCycle()%3];

    id1810out_result = (not_fixed(id1810in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1811out_result;

  { // Node ID: 1811 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1811in_a = id2992out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1811in_b = id1810out_result;

    HWOffsetFix<1,0,UNSIGNED> id1811x_1;

    (id1811x_1) = (and_fixed(id1811in_a,id1811in_b));
    id1811out_result = (id1811x_1);
  }
  { // Node ID: 3469 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1770out_result;

  { // Node ID: 1770 (NodeLt)
    const HWFloat<11,45> &id1770in_a = id1757out_result;
    const HWFloat<11,45> &id1770in_b = id3469out_value;

    id1770out_result = (lt_float(id1770in_a,id1770in_b));
  }
  { // Node ID: 3383 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3383in_input = id1770out_result;

    id3383out_output[(getCycle()+6)%7] = id3383in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1771out_output;

  { // Node ID: 1771 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1771in_input = id1764out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1771in_input_doubt = id1764out_o_doubt;

    id1771out_output = id1771in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1772out_result;

  { // Node ID: 1772 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1772in_a = id3383out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1772in_b = id1771out_output;

    HWOffsetFix<1,0,UNSIGNED> id1772x_1;

    (id1772x_1) = (and_fixed(id1772in_a,id1772in_b));
    id1772out_result = (id1772x_1);
  }
  { // Node ID: 3384 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3384in_input = id1772out_result;

    id3384out_output[(getCycle()+2)%3] = id3384in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1812out_result;

  { // Node ID: 1812 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1812in_a = id1811out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1812in_b = id3384out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1812x_1;

    (id1812x_1) = (or_fixed(id1812in_a,id1812in_b));
    id1812out_result = (id1812x_1);
  }
  { // Node ID: 3468 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2993out_result;

  { // Node ID: 2993 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2993in_a = id3067out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2993in_b = id3468out_value;

    id2993out_result = (gte_fixed(id2993in_a,id2993in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1819out_result;

  { // Node ID: 1819 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1819in_a = id3384out_output[getCycle()%3];

    id1819out_result = (not_fixed(id1819in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1820out_result;

  { // Node ID: 1820 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1820in_a = id2993out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1820in_b = id1819out_result;

    HWOffsetFix<1,0,UNSIGNED> id1820x_1;

    (id1820x_1) = (and_fixed(id1820in_a,id1820in_b));
    id1820out_result = (id1820x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1821out_result;

  { // Node ID: 1821 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1821in_a = id1820out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1821in_b = id3382out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1821x_1;

    (id1821x_1) = (or_fixed(id1821in_a,id1821in_b));
    id1821out_result = (id1821x_1);
  }
  HWRawBits<2> id1822out_result;

  { // Node ID: 1822 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1822in_in0 = id1812out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1822in_in1 = id1821out_result;

    id1822out_result = (cat(id1822in_in0,id1822in_in1));
  }
  { // Node ID: 1814 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1813out_o;

  { // Node ID: 1813 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1813in_i = id3067out_result;

    id1813out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1813in_i));
  }
  { // Node ID: 1798 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1799out_result;

  { // Node ID: 1799 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1799in_sel = id2990out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1799in_option0 = id1795out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1799in_option1 = id1798out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1799x_1;

    switch((id1799in_sel.getValueAsLong())) {
      case 0l:
        id1799x_1 = id1799in_option0;
        break;
      case 1l:
        id1799x_1 = id1799in_option1;
        break;
      default:
        id1799x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1799out_result = (id1799x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1800out_o;

  { // Node ID: 1800 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1800in_i = id1799out_result;

    id1800out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1800in_i));
  }
  HWRawBits<56> id1815out_result;

  { // Node ID: 1815 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1815in_in0 = id1814out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1815in_in1 = id1813out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1815in_in2 = id1800out_o;

    id1815out_result = (cat((cat(id1815in_in0,id1815in_in1)),id1815in_in2));
  }
  HWFloat<11,45> id1816out_output;

  { // Node ID: 1816 (NodeReinterpret)
    const HWRawBits<56> &id1816in_input = id1815out_result;

    id1816out_output = (cast_bits2float<11,45>(id1816in_input));
  }
  { // Node ID: 1823 (NodeConstantRawBits)
  }
  { // Node ID: 1824 (NodeConstantRawBits)
  }
  { // Node ID: 1826 (NodeConstantRawBits)
  }
  HWRawBits<56> id2994out_result;

  { // Node ID: 2994 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id2994in_in0 = id1823out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2994in_in1 = id1824out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id2994in_in2 = id1826out_value;

    id2994out_result = (cat((cat(id2994in_in0,id2994in_in1)),id2994in_in2));
  }
  HWFloat<11,45> id1828out_output;

  { // Node ID: 1828 (NodeReinterpret)
    const HWRawBits<56> &id1828in_input = id2994out_result;

    id1828out_output = (cast_bits2float<11,45>(id1828in_input));
  }
  { // Node ID: 2817 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1831out_result;

  { // Node ID: 1831 (NodeMux)
    const HWRawBits<2> &id1831in_sel = id1822out_result;
    const HWFloat<11,45> &id1831in_option0 = id1816out_output;
    const HWFloat<11,45> &id1831in_option1 = id1828out_output;
    const HWFloat<11,45> &id1831in_option2 = id2817out_value;
    const HWFloat<11,45> &id1831in_option3 = id1828out_output;

    HWFloat<11,45> id1831x_1;

    switch((id1831in_sel.getValueAsLong())) {
      case 0l:
        id1831x_1 = id1831in_option0;
        break;
      case 1l:
        id1831x_1 = id1831in_option1;
        break;
      case 2l:
        id1831x_1 = id1831in_option2;
        break;
      case 3l:
        id1831x_1 = id1831in_option3;
        break;
      default:
        id1831x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1831out_result = (id1831x_1);
  }
  { // Node ID: 3467 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1841out_result;

  { // Node ID: 1841 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1841in_sel = id3387out_output[getCycle()%9];
    const HWFloat<11,45> &id1841in_option0 = id1831out_result;
    const HWFloat<11,45> &id1841in_option1 = id3467out_value;

    HWFloat<11,45> id1841x_1;

    switch((id1841in_sel.getValueAsLong())) {
      case 0l:
        id1841x_1 = id1841in_option0;
        break;
      case 1l:
        id1841x_1 = id1841in_option1;
        break;
      default:
        id1841x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1841out_result = (id1841x_1);
  }
  HWFloat<11,45> id1852out_result;

  { // Node ID: 1852 (NodeMul)
    const HWFloat<11,45> &id1852in_a = id3475out_value;
    const HWFloat<11,45> &id1852in_b = id1841out_result;

    id1852out_result = (mul_float(id1852in_a,id1852in_b));
  }
  { // Node ID: 3466 (NodeConstantRawBits)
  }
  { // Node ID: 3465 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1854out_result;

  { // Node ID: 1854 (NodeAdd)
    const HWFloat<11,45> &id1854in_a = id17out_result[getCycle()%2];
    const HWFloat<11,45> &id1854in_b = id3465out_value;

    id1854out_result = (add_float(id1854in_a,id1854in_b));
  }
  HWFloat<11,45> id1856out_result;

  { // Node ID: 1856 (NodeMul)
    const HWFloat<11,45> &id1856in_a = id3466out_value;
    const HWFloat<11,45> &id1856in_b = id1854out_result;

    id1856out_result = (mul_float(id1856in_a,id1856in_b));
  }
  HWRawBits<11> id1933out_result;

  { // Node ID: 1933 (NodeSlice)
    const HWFloat<11,45> &id1933in_a = id1856out_result;

    id1933out_result = (slice<44,11>(id1933in_a));
  }
  { // Node ID: 1934 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2995out_result;

  { // Node ID: 2995 (NodeEqInlined)
    const HWRawBits<11> &id2995in_a = id1933out_result;
    const HWRawBits<11> &id2995in_b = id1934out_value;

    id2995out_result = (eq_bits(id2995in_a,id2995in_b));
  }
  HWRawBits<44> id1932out_result;

  { // Node ID: 1932 (NodeSlice)
    const HWFloat<11,45> &id1932in_a = id1856out_result;

    id1932out_result = (slice<0,44>(id1932in_a));
  }
  { // Node ID: 3464 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2996out_result;

  { // Node ID: 2996 (NodeNeqInlined)
    const HWRawBits<44> &id2996in_a = id1932out_result;
    const HWRawBits<44> &id2996in_b = id3464out_value;

    id2996out_result = (neq_bits(id2996in_a,id2996in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1938out_result;

  { // Node ID: 1938 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1938in_a = id2995out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1938in_b = id2996out_result;

    HWOffsetFix<1,0,UNSIGNED> id1938x_1;

    (id1938x_1) = (and_fixed(id1938in_a,id1938in_b));
    id1938out_result = (id1938x_1);
  }
  { // Node ID: 3397 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3397in_input = id1938out_result;

    id3397out_output[(getCycle()+8)%9] = id3397in_input;
  }
  { // Node ID: 1857 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1858out_output;
  HWOffsetFix<1,0,UNSIGNED> id1858out_output_doubt;

  { // Node ID: 1858 (NodeDoubtBitOp)
    const HWFloat<11,45> &id1858in_input = id1856out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1858in_doubt = id1857out_value;

    id1858out_output = id1858in_input;
    id1858out_output_doubt = id1858in_doubt;
  }
  { // Node ID: 1859 (NodeCast)
    const HWFloat<11,45> &id1859in_i = id1858out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1859in_i_doubt = id1858out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id1859x_1;

    id1859out_o[(getCycle()+6)%7] = (cast_float2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1859in_i,(&(id1859x_1))));
    id1859out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id1859x_1),(c_hw_fix_4_0_uns_bits))),id1859in_i_doubt));
  }
  { // Node ID: 1862 (NodeConstantRawBits)
  }
  HWOffsetFix<113,-99,TWOSCOMPLEMENT> id1861out_result;
  HWOffsetFix<1,0,UNSIGNED> id1861out_result_doubt;

  { // Node ID: 1861 (NodeMul)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1861in_a = id1859out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1861in_a_doubt = id1859out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id1861in_b = id1862out_value;

    HWOffsetFix<1,0,UNSIGNED> id1861x_1;

    id1861out_result = (mul_fixed<113,-99,TWOSCOMPLEMENT,TONEAREVEN>(id1861in_a,id1861in_b,(&(id1861x_1))));
    id1861out_result_doubt = (or_fixed((neq_fixed((id1861x_1),(c_hw_fix_1_0_uns_bits_1))),id1861in_a_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1863out_o;
  HWOffsetFix<1,0,UNSIGNED> id1863out_o_doubt;

  { // Node ID: 1863 (NodeCast)
    const HWOffsetFix<113,-99,TWOSCOMPLEMENT> &id1863in_i = id1861out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1863in_i_doubt = id1861out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id1863x_1;

    id1863out_o = (cast_fixed2fixed<61,-48,TWOSCOMPLEMENT,TONEAREVEN>(id1863in_i,(&(id1863x_1))));
    id1863out_o_doubt = (or_fixed((neq_fixed((id1863x_1),(c_hw_fix_1_0_uns_bits_1))),id1863in_i_doubt));
  }
  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id1872out_output;

  { // Node ID: 1872 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1872in_input = id1863out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1872in_input_doubt = id1863out_o_doubt;

    id1872out_output = id1872in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id1873out_o;

  { // Node ID: 1873 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1873in_i = id1872out_output;

    id1873out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id1873in_i));
  }
  { // Node ID: 3388 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id3388in_input = id1873out_o;

    id3388out_output[(getCycle()+2)%3] = id3388in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id1900out_o;

  { // Node ID: 1900 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id1900in_i = id3388out_output[getCycle()%3];

    id1900out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id1900in_i));
  }
  { // Node ID: 1903 (NodeConstantRawBits)
  }
  { // Node ID: 3062 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-23,UNSIGNED> id1876out_o;

  { // Node ID: 1876 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1876in_i = id1872out_output;

    id1876out_o = (cast_fixed2fixed<10,-23,UNSIGNED,TRUNCATE>(id1876in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1948out_output;

  { // Node ID: 1948 (NodeReinterpret)
    const HWOffsetFix<10,-23,UNSIGNED> &id1948in_input = id1876out_o;

    id1948out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1948in_input))));
  }
  { // Node ID: 1949 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1949in_addr = id1948out_output;

    HWOffsetFix<32,-45,UNSIGNED> id1949x_1;

    switch(((long)((id1949in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1949x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
      case 1l:
        id1949x_1 = (id1949sta_rom_store[(id1949in_addr.getValueAsLong())]);
        break;
      default:
        id1949x_1 = (c_hw_fix_32_n45_uns_undef);
        break;
    }
    id1949out_dout[(getCycle()+2)%3] = (id1949x_1);
  }
  HWOffsetFix<10,-13,UNSIGNED> id1875out_o;

  { // Node ID: 1875 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1875in_i = id1872out_output;

    id1875out_o = (cast_fixed2fixed<10,-13,UNSIGNED,TRUNCATE>(id1875in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id1945out_output;

  { // Node ID: 1945 (NodeReinterpret)
    const HWOffsetFix<10,-13,UNSIGNED> &id1945in_input = id1875out_o;

    id1945out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id1945in_input))));
  }
  { // Node ID: 1946 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id1946in_addr = id1945out_output;

    HWOffsetFix<42,-45,UNSIGNED> id1946x_1;

    switch(((long)((id1946in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id1946x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
      case 1l:
        id1946x_1 = (id1946sta_rom_store[(id1946in_addr.getValueAsLong())]);
        break;
      default:
        id1946x_1 = (c_hw_fix_42_n45_uns_undef);
        break;
    }
    id1946out_dout[(getCycle()+2)%3] = (id1946x_1);
  }
  HWOffsetFix<3,-3,UNSIGNED> id1874out_o;

  { // Node ID: 1874 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1874in_i = id1872out_output;

    id1874out_o = (cast_fixed2fixed<3,-3,UNSIGNED,TRUNCATE>(id1874in_i));
  }
  HWOffsetFix<3,0,UNSIGNED> id1942out_output;

  { // Node ID: 1942 (NodeReinterpret)
    const HWOffsetFix<3,-3,UNSIGNED> &id1942in_input = id1874out_o;

    id1942out_output = (cast_bits2fixed<3,0,UNSIGNED>((cast_fixed2bits(id1942in_input))));
  }
  { // Node ID: 1943 (NodeROM)
    const HWOffsetFix<3,0,UNSIGNED> &id1943in_addr = id1942out_output;

    HWOffsetFix<45,-45,UNSIGNED> id1943x_1;

    switch(((long)((id1943in_addr.getValueAsLong())<(8l)))) {
      case 0l:
        id1943x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
      case 1l:
        id1943x_1 = (id1943sta_rom_store[(id1943in_addr.getValueAsLong())]);
        break;
      default:
        id1943x_1 = (c_hw_fix_45_n45_uns_undef);
        break;
    }
    id1943out_dout[(getCycle()+2)%3] = (id1943x_1);
  }
  { // Node ID: 1880 (NodeConstantRawBits)
  }
  HWOffsetFix<25,-48,UNSIGNED> id1877out_o;

  { // Node ID: 1877 (NodeCast)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1877in_i = id1872out_output;

    id1877out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TRUNCATE>(id1877in_i));
  }
  HWOffsetFix<46,-69,UNSIGNED> id1879out_result;

  { // Node ID: 1879 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id1879in_a = id1880out_value;
    const HWOffsetFix<25,-48,UNSIGNED> &id1879in_b = id1877out_o;

    id1879out_result = (mul_fixed<46,-69,UNSIGNED,TONEAREVEN>(id1879in_a,id1879in_b));
  }
  HWOffsetFix<25,-48,UNSIGNED> id1881out_o;

  { // Node ID: 1881 (NodeCast)
    const HWOffsetFix<46,-69,UNSIGNED> &id1881in_i = id1879out_result;

    id1881out_o = (cast_fixed2fixed<25,-48,UNSIGNED,TONEAREVEN>(id1881in_i));
  }
  { // Node ID: 3389 (NodeFIFO)
    const HWOffsetFix<25,-48,UNSIGNED> &id3389in_input = id1881out_o;

    id3389out_output[(getCycle()+2)%3] = id3389in_input;
  }
  HWOffsetFix<49,-48,UNSIGNED> id1882out_result;

  { // Node ID: 1882 (NodeAdd)
    const HWOffsetFix<45,-45,UNSIGNED> &id1882in_a = id1943out_dout[getCycle()%3];
    const HWOffsetFix<25,-48,UNSIGNED> &id1882in_b = id3389out_output[getCycle()%3];

    id1882out_result = (add_fixed<49,-48,UNSIGNED,TONEAREVEN>(id1882in_a,id1882in_b));
  }
  HWOffsetFix<64,-87,UNSIGNED> id1883out_result;

  { // Node ID: 1883 (NodeMul)
    const HWOffsetFix<25,-48,UNSIGNED> &id1883in_a = id3389out_output[getCycle()%3];
    const HWOffsetFix<45,-45,UNSIGNED> &id1883in_b = id1943out_dout[getCycle()%3];

    id1883out_result = (mul_fixed<64,-87,UNSIGNED,TONEAREVEN>(id1883in_a,id1883in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id1884out_result;

  { // Node ID: 1884 (NodeAdd)
    const HWOffsetFix<49,-48,UNSIGNED> &id1884in_a = id1882out_result;
    const HWOffsetFix<64,-87,UNSIGNED> &id1884in_b = id1883out_result;

    id1884out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id1884in_a,id1884in_b));
  }
  HWOffsetFix<49,-48,UNSIGNED> id1885out_o;

  { // Node ID: 1885 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id1885in_i = id1884out_result;

    id1885out_o = (cast_fixed2fixed<49,-48,UNSIGNED,TONEAREVEN>(id1885in_i));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1886out_result;

  { // Node ID: 1886 (NodeAdd)
    const HWOffsetFix<42,-45,UNSIGNED> &id1886in_a = id1946out_dout[getCycle()%3];
    const HWOffsetFix<49,-48,UNSIGNED> &id1886in_b = id1885out_o;

    id1886out_result = (add_fixed<50,-48,UNSIGNED,TONEAREVEN>(id1886in_a,id1886in_b));
  }
  HWOffsetFix<64,-66,UNSIGNED> id1887out_result;

  { // Node ID: 1887 (NodeMul)
    const HWOffsetFix<49,-48,UNSIGNED> &id1887in_a = id1885out_o;
    const HWOffsetFix<42,-45,UNSIGNED> &id1887in_b = id1946out_dout[getCycle()%3];

    id1887out_result = (mul_fixed<64,-66,UNSIGNED,TONEAREVEN>(id1887in_a,id1887in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id1888out_result;

  { // Node ID: 1888 (NodeAdd)
    const HWOffsetFix<50,-48,UNSIGNED> &id1888in_a = id1886out_result;
    const HWOffsetFix<64,-66,UNSIGNED> &id1888in_b = id1887out_result;

    id1888out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id1888in_a,id1888in_b));
  }
  HWOffsetFix<50,-48,UNSIGNED> id1889out_o;

  { // Node ID: 1889 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id1889in_i = id1888out_result;

    id1889out_o = (cast_fixed2fixed<50,-48,UNSIGNED,TONEAREVEN>(id1889in_i));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1890out_result;

  { // Node ID: 1890 (NodeAdd)
    const HWOffsetFix<32,-45,UNSIGNED> &id1890in_a = id1949out_dout[getCycle()%3];
    const HWOffsetFix<50,-48,UNSIGNED> &id1890in_b = id1889out_o;

    id1890out_result = (add_fixed<51,-48,UNSIGNED,TONEAREVEN>(id1890in_a,id1890in_b));
  }
  HWOffsetFix<64,-75,UNSIGNED> id1891out_result;

  { // Node ID: 1891 (NodeMul)
    const HWOffsetFix<50,-48,UNSIGNED> &id1891in_a = id1889out_o;
    const HWOffsetFix<32,-45,UNSIGNED> &id1891in_b = id1949out_dout[getCycle()%3];

    id1891out_result = (mul_fixed<64,-75,UNSIGNED,TONEAREVEN>(id1891in_a,id1891in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id1892out_result;

  { // Node ID: 1892 (NodeAdd)
    const HWOffsetFix<51,-48,UNSIGNED> &id1892in_a = id1890out_result;
    const HWOffsetFix<64,-75,UNSIGNED> &id1892in_b = id1891out_result;

    id1892out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id1892in_a,id1892in_b));
  }
  HWOffsetFix<51,-48,UNSIGNED> id1893out_o;

  { // Node ID: 1893 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id1893in_i = id1892out_result;

    id1893out_o = (cast_fixed2fixed<51,-48,UNSIGNED,TONEAREVEN>(id1893in_i));
  }
  HWOffsetFix<45,-44,UNSIGNED> id1894out_o;

  { // Node ID: 1894 (NodeCast)
    const HWOffsetFix<51,-48,UNSIGNED> &id1894in_i = id1893out_o;

    id1894out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1894in_i));
  }
  { // Node ID: 3463 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id2997out_result;

  { // Node ID: 2997 (NodeGteInlined)
    const HWOffsetFix<45,-44,UNSIGNED> &id2997in_a = id1894out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id2997in_b = id3463out_value;

    id2997out_result = (gte_fixed(id2997in_a,id2997in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id3066out_result;

  { // Node ID: 3066 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3066in_a = id1900out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3066in_b = id1903out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3066in_c = id3062out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3066in_condb = id2997out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3066x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3066x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3066x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id3066x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3066x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3066x_1 = id3066in_a;
        break;
      default:
        id3066x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id3066in_condb.getValueAsLong())) {
      case 0l:
        id3066x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3066x_2 = id3066in_b;
        break;
      default:
        id3066x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id3066x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id3066x_3 = id3066in_c;
        break;
      default:
        id3066x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id3066x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id3066x_1),(id3066x_2))),(id3066x_3)));
    id3066out_result = (id3066x_4);
  }
  HWRawBits<1> id2998out_result;

  { // Node ID: 2998 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id2998in_a = id3066out_result;

    id2998out_result = (slice<13,1>(id2998in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2999out_output;

  { // Node ID: 2999 (NodeReinterpret)
    const HWRawBits<1> &id2999in_input = id2998out_result;

    id2999out_output = (cast_bits2fixed<1,0,UNSIGNED>(id2999in_input));
  }
  { // Node ID: 3462 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1865out_result;

  { // Node ID: 1865 (NodeGt)
    const HWFloat<11,45> &id1865in_a = id1856out_result;
    const HWFloat<11,45> &id1865in_b = id3462out_value;

    id1865out_result = (gt_float(id1865in_a,id1865in_b));
  }
  { // Node ID: 3391 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3391in_input = id1865out_result;

    id3391out_output[(getCycle()+6)%7] = id3391in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1866out_output;

  { // Node ID: 1866 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1866in_input = id1863out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1866in_input_doubt = id1863out_o_doubt;

    id1866out_output = id1866in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1867out_result;

  { // Node ID: 1867 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1867in_a = id3391out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1867in_b = id1866out_output;

    HWOffsetFix<1,0,UNSIGNED> id1867x_1;

    (id1867x_1) = (and_fixed(id1867in_a,id1867in_b));
    id1867out_result = (id1867x_1);
  }
  { // Node ID: 3392 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3392in_input = id1867out_result;

    id3392out_output[(getCycle()+2)%3] = id3392in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1909out_result;

  { // Node ID: 1909 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1909in_a = id3392out_output[getCycle()%3];

    id1909out_result = (not_fixed(id1909in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1910out_result;

  { // Node ID: 1910 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1910in_a = id2999out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1910in_b = id1909out_result;

    HWOffsetFix<1,0,UNSIGNED> id1910x_1;

    (id1910x_1) = (and_fixed(id1910in_a,id1910in_b));
    id1910out_result = (id1910x_1);
  }
  { // Node ID: 3461 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1869out_result;

  { // Node ID: 1869 (NodeLt)
    const HWFloat<11,45> &id1869in_a = id1856out_result;
    const HWFloat<11,45> &id1869in_b = id3461out_value;

    id1869out_result = (lt_float(id1869in_a,id1869in_b));
  }
  { // Node ID: 3393 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3393in_input = id1869out_result;

    id3393out_output[(getCycle()+6)%7] = id3393in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1870out_output;

  { // Node ID: 1870 (NodeDoubtBitOp)
    const HWOffsetFix<61,-48,TWOSCOMPLEMENT> &id1870in_input = id1863out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1870in_input_doubt = id1863out_o_doubt;

    id1870out_output = id1870in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1871out_result;

  { // Node ID: 1871 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1871in_a = id3393out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id1871in_b = id1870out_output;

    HWOffsetFix<1,0,UNSIGNED> id1871x_1;

    (id1871x_1) = (and_fixed(id1871in_a,id1871in_b));
    id1871out_result = (id1871x_1);
  }
  { // Node ID: 3394 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3394in_input = id1871out_result;

    id3394out_output[(getCycle()+2)%3] = id3394in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id1911out_result;

  { // Node ID: 1911 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1911in_a = id1910out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1911in_b = id3394out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1911x_1;

    (id1911x_1) = (or_fixed(id1911in_a,id1911in_b));
    id1911out_result = (id1911x_1);
  }
  { // Node ID: 3460 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id3000out_result;

  { // Node ID: 3000 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3000in_a = id3066out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id3000in_b = id3460out_value;

    id3000out_result = (gte_fixed(id3000in_a,id3000in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1918out_result;

  { // Node ID: 1918 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1918in_a = id3394out_output[getCycle()%3];

    id1918out_result = (not_fixed(id1918in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id1919out_result;

  { // Node ID: 1919 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id1919in_a = id3000out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1919in_b = id1918out_result;

    HWOffsetFix<1,0,UNSIGNED> id1919x_1;

    (id1919x_1) = (and_fixed(id1919in_a,id1919in_b));
    id1919out_result = (id1919x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id1920out_result;

  { // Node ID: 1920 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1920in_a = id1919out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1920in_b = id3392out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id1920x_1;

    (id1920x_1) = (or_fixed(id1920in_a,id1920in_b));
    id1920out_result = (id1920x_1);
  }
  HWRawBits<2> id1921out_result;

  { // Node ID: 1921 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1921in_in0 = id1911out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id1921in_in1 = id1920out_result;

    id1921out_result = (cat(id1921in_in0,id1921in_in1));
  }
  { // Node ID: 1913 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1912out_o;

  { // Node ID: 1912 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id1912in_i = id3066out_result;

    id1912out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id1912in_i));
  }
  { // Node ID: 1897 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id1898out_result;

  { // Node ID: 1898 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1898in_sel = id2997out_result;
    const HWOffsetFix<45,-44,UNSIGNED> &id1898in_option0 = id1894out_o;
    const HWOffsetFix<45,-44,UNSIGNED> &id1898in_option1 = id1897out_value;

    HWOffsetFix<45,-44,UNSIGNED> id1898x_1;

    switch((id1898in_sel.getValueAsLong())) {
      case 0l:
        id1898x_1 = id1898in_option0;
        break;
      case 1l:
        id1898x_1 = id1898in_option1;
        break;
      default:
        id1898x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    id1898out_result = (id1898x_1);
  }
  HWOffsetFix<44,-44,UNSIGNED> id1899out_o;

  { // Node ID: 1899 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id1899in_i = id1898out_result;

    id1899out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id1899in_i));
  }
  HWRawBits<56> id1914out_result;

  { // Node ID: 1914 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id1914in_in0 = id1913out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id1914in_in1 = id1912out_o;
    const HWOffsetFix<44,-44,UNSIGNED> &id1914in_in2 = id1899out_o;

    id1914out_result = (cat((cat(id1914in_in0,id1914in_in1)),id1914in_in2));
  }
  HWFloat<11,45> id1915out_output;

  { // Node ID: 1915 (NodeReinterpret)
    const HWRawBits<56> &id1915in_input = id1914out_result;

    id1915out_output = (cast_bits2float<11,45>(id1915in_input));
  }
  { // Node ID: 1922 (NodeConstantRawBits)
  }
  { // Node ID: 1923 (NodeConstantRawBits)
  }
  { // Node ID: 1925 (NodeConstantRawBits)
  }
  HWRawBits<56> id3001out_result;

  { // Node ID: 3001 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id3001in_in0 = id1922out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id3001in_in1 = id1923out_value;
    const HWOffsetFix<44,0,UNSIGNED> &id3001in_in2 = id1925out_value;

    id3001out_result = (cat((cat(id3001in_in0,id3001in_in1)),id3001in_in2));
  }
  HWFloat<11,45> id1927out_output;

  { // Node ID: 1927 (NodeReinterpret)
    const HWRawBits<56> &id1927in_input = id3001out_result;

    id1927out_output = (cast_bits2float<11,45>(id1927in_input));
  }
  { // Node ID: 2818 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1930out_result;

  { // Node ID: 1930 (NodeMux)
    const HWRawBits<2> &id1930in_sel = id1921out_result;
    const HWFloat<11,45> &id1930in_option0 = id1915out_output;
    const HWFloat<11,45> &id1930in_option1 = id1927out_output;
    const HWFloat<11,45> &id1930in_option2 = id2818out_value;
    const HWFloat<11,45> &id1930in_option3 = id1927out_output;

    HWFloat<11,45> id1930x_1;

    switch((id1930in_sel.getValueAsLong())) {
      case 0l:
        id1930x_1 = id1930in_option0;
        break;
      case 1l:
        id1930x_1 = id1930in_option1;
        break;
      case 2l:
        id1930x_1 = id1930in_option2;
        break;
      case 3l:
        id1930x_1 = id1930in_option3;
        break;
      default:
        id1930x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1930out_result = (id1930x_1);
  }
  { // Node ID: 3459 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1940out_result;

  { // Node ID: 1940 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id1940in_sel = id3397out_output[getCycle()%9];
    const HWFloat<11,45> &id1940in_option0 = id1930out_result;
    const HWFloat<11,45> &id1940in_option1 = id3459out_value;

    HWFloat<11,45> id1940x_1;

    switch((id1940in_sel.getValueAsLong())) {
      case 0l:
        id1940x_1 = id1940in_option0;
        break;
      case 1l:
        id1940x_1 = id1940in_option1;
        break;
      default:
        id1940x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id1940out_result = (id1940x_1);
  }
  { // Node ID: 3458 (NodeConstantRawBits)
  }
  HWFloat<11,45> id1951out_result;

  { // Node ID: 1951 (NodeAdd)
    const HWFloat<11,45> &id1951in_a = id1940out_result;
    const HWFloat<11,45> &id1951in_b = id3458out_value;

    id1951out_result = (add_float(id1951in_a,id1951in_b));
  }
  HWFloat<11,45> id1952out_result;

  { // Node ID: 1952 (NodeDiv)
    const HWFloat<11,45> &id1952in_a = id1852out_result;
    const HWFloat<11,45> &id1952in_b = id1951out_result;

    id1952out_result = (div_float(id1952in_a,id1952in_b));
  }
  HWFloat<11,45> id1953out_result;

  { // Node ID: 1953 (NodeMul)
    const HWFloat<11,45> &id1953in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id1953in_b = id1952out_result;

    id1953out_result = (mul_float(id1953in_a,id1953in_b));
  }
  HWFloat<11,45> id1987out_result;

  { // Node ID: 1987 (NodeMul)
    const HWFloat<11,45> &id1987in_a = id1953out_result;
    const HWFloat<11,45> &id1987in_b = id3377out_output[getCycle()%9];

    id1987out_result = (mul_float(id1987in_a,id1987in_b));
  }
  HWFloat<11,45> id1988out_result;

  { // Node ID: 1988 (NodeSub)
    const HWFloat<11,45> &id1988in_a = id1986out_result;
    const HWFloat<11,45> &id1988in_b = id1987out_result;

    id1988out_result = (sub_float(id1988in_a,id1988in_b));
  }
  HWFloat<11,45> id1989out_result;

  { // Node ID: 1989 (NodeAdd)
    const HWFloat<11,45> &id1989in_a = id3377out_output[getCycle()%9];
    const HWFloat<11,45> &id1989in_b = id1988out_result;

    id1989out_result = (add_float(id1989in_a,id1989in_b));
  }
  { // Node ID: 3356 (NodeFIFO)
    const HWFloat<11,45> &id3356in_input = id1989out_result;

    id3356out_output[(getCycle()+1)%2] = id3356in_input;
  }
  { // Node ID: 3455 (NodeConstantRawBits)
  }
  { // Node ID: 3002 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id3002in_a = id10out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id3002in_b = id3455out_value;

    id3002out_result[(getCycle()+1)%2] = (eq_fixed(id3002in_a,id3002in_b));
  }
  { // Node ID: 3454 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2070out_result;

  { // Node ID: 2070 (NodeMul)
    const HWFloat<11,45> &id2070in_a = id3454out_value;
    const HWFloat<11,45> &id2070in_b = id2068out_result;

    id2070out_result = (mul_float(id2070in_a,id2070in_b));
  }
  { // Node ID: 3453 (NodeConstantRawBits)
  }
  { // Node ID: 3452 (NodeConstantRawBits)
  }
  HWFloat<11,45> id2072out_result;

  { // Node ID: 2072 (NodeSub)
    const HWFloat<11,45> &id2072in_a = id3452out_value;
    const HWFloat<11,45> &id2072in_b = id3433out_output[getCycle()%3];

    id2072out_result = (sub_float(id2072in_a,id2072in_b));
  }
  HWFloat<11,45> id2074out_result;

  { // Node ID: 2074 (NodeMul)
    const HWFloat<11,45> &id2074in_a = id3453out_value;
    const HWFloat<11,45> &id2074in_b = id2072out_result;

    id2074out_result = (mul_float(id2074in_a,id2074in_b));
  }
  HWFloat<11,45> id2075out_result;

  { // Node ID: 2075 (NodeAdd)
    const HWFloat<11,45> &id2075in_a = id2070out_result;
    const HWFloat<11,45> &id2075in_b = id2074out_result;

    id2075out_result = (add_float(id2075in_a,id2075in_b));
  }
  HWFloat<11,45> id2076out_result;

  { // Node ID: 2076 (NodeMul)
    const HWFloat<11,45> &id2076in_a = id13out_o[getCycle()%4];
    const HWFloat<11,45> &id2076in_b = id2075out_result;

    id2076out_result = (mul_float(id2076in_a,id2076in_b));
  }
  HWFloat<11,45> id2077out_result;

  { // Node ID: 2077 (NodeAdd)
    const HWFloat<11,45> &id2077in_a = id3433out_output[getCycle()%3];
    const HWFloat<11,45> &id2077in_b = id2076out_result;

    id2077out_result = (add_float(id2077in_a,id2077in_b));
  }
  HWFloat<11,45> id3111out_output;

  { // Node ID: 3111 (NodeStreamOffset)
    const HWFloat<11,45> &id3111in_input = id2077out_result;

    id3111out_output = id3111in_input;
  }
  { // Node ID: 20 (NodeInputMappedReg)
  }
  { // Node ID: 21 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id21in_sel = id3002out_result[getCycle()%2];
    const HWFloat<11,45> &id21in_option0 = id3111out_output;
    const HWFloat<11,45> &id21in_option1 = id20out_ca_in;

    HWFloat<11,45> id21x_1;

    switch((id21in_sel.getValueAsLong())) {
      case 0l:
        id21x_1 = id21in_option0;
        break;
      case 1l:
        id21x_1 = id21in_option1;
        break;
      default:
        id21x_1 = (c_hw_flt_11_45_undef);
        break;
    }
    id21out_result[(getCycle()+1)%2] = (id21x_1);
  }
  { // Node ID: 3409 (NodeFIFO)
    const HWFloat<11,45> &id3409in_input = id21out_result[getCycle()%2];

    id3409out_output[(getCycle()+7)%8] = id3409in_input;
  }
  { // Node ID: 3433 (NodeFIFO)
    const HWFloat<11,45> &id3433in_input = id3409out_output[getCycle()%8];

    id3433out_output[(getCycle()+2)%3] = id3433in_input;
  }
  HWRawBits<44> id1990out_result;

  { // Node ID: 1990 (NodeSlice)
    const HWFloat<11,45> &id1990in_a = id3409out_output[getCycle()%8];

    id1990out_result = (slice<0,44>(id1990in_a));
  }
  HWOffsetFix<44,-44,UNSIGNED> id1991out_output;

  { // Node ID: 1991 (NodeReinterpret)
    const HWRawBits<44> &id1991in_input = id1990out_result;

    id1991out_output = (cast_bits2fixed<44,-44,UNSIGNED>(id1991in_input));
  }
  { // Node ID: 1992 (NodeConstantRawBits)
  }
  HWOffsetFix<44,-44,UNSIGNED> id1993out_output;
  HWOffsetFix<1,0,UNSIGNED> id1993out_output_doubt;

  { // Node ID: 1993 (NodeDoubtBitOp)
    const HWOffsetFix<44,-44,UNSIGNED> &id1993in_input = id1991out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1993in_doubt = id1992out_value;

    id1993out_output = id1993in_input;
    id1993out_output_doubt = id1993in_doubt;
  }
  HWOffsetFix<45,-44,UNSIGNED> id1994out_o;
  HWOffsetFix<1,0,UNSIGNED> id1994out_o_doubt;

  { // Node ID: 1994 (NodeCast)
    const HWOffsetFix<44,-44,UNSIGNED> &id1994in_i = id1993out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1994in_i_doubt = id1993out_output_doubt;

    id1994out_o = (cast_fixed2fixed<45,-44,UNSIGNED,TONEAREVEN>(id1994in_i));
    id1994out_o_doubt = id1994in_i_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id1998out_output;

  { // Node ID: 1998 (NodeDoubtBitOp)
    const HWOffsetFix<45,-44,UNSIGNED> &id1998in_input = id1994out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1998in_input_doubt = id1994out_o_doubt;

    id1998out_output = id1998in_input_doubt;
  }
  HWOffsetFix<45,-44,UNSIGNED> id1995out_output;

  { // Node ID: 1995 (NodeDoubtBitOp)
    const HWOffsetFix<45,-44,UNSIGNED> &id1995in_input = id1994out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id1995in_input_doubt = id1994out_o_doubt;

    id1995out_output = id1995in_input;
  }
  { // Node ID: 3447 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id1997out_result;

  { // Node ID: 1997 (NodeGte)
    const HWOffsetFix<45,-44,UNSIGNED> &id1997in_a = id1995out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1997in_b = id3447out_value;

    id1997out_result = (gte_fixed(id1997in_a,id1997in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id1999out_result;

  { // Node ID: 1999 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id1999in_a = id1998out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id1999in_b = id1997out_result;

    HWOffsetFix<1,0,UNSIGNED> id1999x_1;

    (id1999x_1) = (or_fixed(id1999in_a,id1999in_b));
    id1999out_result = (id1999x_1);
  }
  { // Node ID: 3410 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3410in_input = id1999out_result;

    id3410out_output[(getCycle()+2)%3] = id3410in_input;
  }
  { // Node ID: 2000 (NodeConstantRawBits)
  }
  HWOffsetFix<45,-44,UNSIGNED> id3064out_result;

  { // Node ID: 3064 (NodeCondSub)
    const HWOffsetFix<45,-44,UNSIGNED> &id3064in_a = id1995out_output;
    const HWOffsetFix<45,-44,UNSIGNED> &id3064in_b = id2000out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id3064in_condb = id1999out_result;

    HWOffsetFix<45,-44,UNSIGNED> id3064x_1;
    HWOffsetFix<45,-44,UNSIGNED> id3064x_2;

    switch((id3064in_condb.getValueAsLong())) {
      case 0l:
        id3064x_1 = (c_hw_fix_45_n44_uns_bits_1);
        break;
      case 1l:
        id3064x_1 = id3064in_b;
        break;
      default:
        id3064x_1 = (c_hw_fix_45_n44_uns_undef);
        break;
    }
    (id3064x_2) = (sub_fixed<45,-44,UNSIGNED,TRUNCATE>(id3064in_a,(id3064x_1)));
    id3064out_result = (id3064x_2);
  }
  HWOffsetFix<44,-44,UNSIGNED> id2015out_o;

  { // Node ID: 2015 (NodeCast)
    const HWOffsetFix<45,-44,UNSIGNED> &id2015in_i = id3064out_result;

    id2015out_o = (cast_fixed2fixed<44,-44,UNSIGNED,TONEAREVEN>(id2015in_i));
  }
  HWRawBits<11> id2016out_result;

  { // Node ID: 2016 (NodeSlice)
    const HWOffsetFix<44,-44,UNSIGNED> &id2016in_a = id2015out_o;

    id2016out_result = (slice<33,11>(id2016in_a));
  }
  { // Node ID: 2773 (NodeROM)
    const HWRawBits<11> &id2773in_addr = id2016out_result;

    HWRawBits<180> id2773x_1;

    switch(((long)((id2773in_addr.getValueAsLong())<(2048l)))) {
      case 0l:
        id2773x_1 = (c_hw_bit_180_undef);
        break;
      case 1l:
        id2773x_1 = (id2773sta_rom_store[(id2773in_addr.getValueAsLong())]);
        break;
      default:
        id2773x_1 = (c_hw_bit_180_undef);
        break;
    }
    id2773out_dout[(getCycle()+2)%3] = (id2773x_1);
  }
  HWRawBits<33> id2017out_result;

  { // Node ID: 2017 (NodeSlice)
    const HWOffsetFix<44,-44,UNSIGNED> &id2017in_a = id2015out_o;

    id2017out_result = (slice<0,33>(id2017in_a));
  }
  HWOffsetFix<33,-33,UNSIGNED> id2018out_output;

  { // Node ID: 2018 (NodeReinterpret)
    const HWRawBits<33> &id2018in_input = id2017out_result;

    id2018out_output = (cast_bits2fixed<33,-33,UNSIGNED>(id2018in_input));
  }
  { // Node ID: 3411 (NodeFIFO)
    const HWOffsetFix<33,-33,UNSIGNED> &id3411in_input = id2018out_output;

    id3411out_output[(getCycle()+2)%3] = id3411in_input;
  }
  HWFloat<11,53> id2709out_o;

  { // Node ID: 2709 (NodeCast)
    const HWFloat<11,45> &id2709in_i = id2700out_result;

    id2709out_o = (cast_float2float<11,53>(id2709in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2716 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2716in_output_control = id3416out_output[getCycle()%10];
    const HWFloat<11,53> &id2716in_data = id2709out_o;

    bool id2716x_1;

    (id2716x_1) = ((id2716in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2716x_1)) {
      writeOutput(m_u_out, id2716in_data);
    }
  }
  { // Node ID: 3443 (NodeConstantRawBits)
  }
  { // Node ID: 2719 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2719in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2719in_b = id3443out_value;

    id2719out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2719in_a,id2719in_b));
  }
  { // Node ID: 3006 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3006in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3006in_b = id2719out_result[getCycle()%2];

    id3006out_result[(getCycle()+1)%2] = (eq_fixed(id3006in_a,id3006in_b));
  }
  { // Node ID: 2721 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2722out_result;

  { // Node ID: 2722 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2722in_a = id2721out_io_ca_out_force_disabled;

    id2722out_result = (not_fixed(id2722in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2723out_result;

  { // Node ID: 2723 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2723in_a = id3006out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2723in_b = id2722out_result;

    HWOffsetFix<1,0,UNSIGNED> id2723x_1;

    (id2723x_1) = (and_fixed(id2723in_a,id2723in_b));
    id2723out_result = (id2723x_1);
  }
  { // Node ID: 3418 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3418in_input = id2723out_result;

    id3418out_output[(getCycle()+9)%10] = id3418in_input;
  }
  HWFloat<11,53> id2717out_o;

  { // Node ID: 2717 (NodeCast)
    const HWFloat<11,45> &id2717in_i = id2077out_result;

    id2717out_o = (cast_float2float<11,53>(id2717in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2724 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2724in_output_control = id3418out_output[getCycle()%10];
    const HWFloat<11,53> &id2724in_data = id2717out_o;

    bool id2724x_1;

    (id2724x_1) = ((id2724in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2724x_1)) {
      writeOutput(m_ca_out, id2724in_data);
    }
  }
  { // Node ID: 3442 (NodeConstantRawBits)
  }
  { // Node ID: 2727 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2727in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2727in_b = id3442out_value;

    id2727out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2727in_a,id2727in_b));
  }
  { // Node ID: 3007 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3007in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3007in_b = id2727out_result[getCycle()%2];

    id3007out_result[(getCycle()+1)%2] = (eq_fixed(id3007in_a,id3007in_b));
  }
  { // Node ID: 2729 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2730out_result;

  { // Node ID: 2730 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2730in_a = id2729out_io_x1_out_force_disabled;

    id2730out_result = (not_fixed(id2730in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2731out_result;

  { // Node ID: 2731 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2731in_a = id3007out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2731in_b = id2730out_result;

    HWOffsetFix<1,0,UNSIGNED> id2731x_1;

    (id2731x_1) = (and_fixed(id2731in_a,id2731in_b));
    id2731out_result = (id2731x_1);
  }
  { // Node ID: 3420 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3420in_input = id2731out_result;

    id3420out_output[(getCycle()+8)%9] = id3420in_input;
  }
  HWFloat<11,53> id2725out_o;

  { // Node ID: 2725 (NodeCast)
    const HWFloat<11,45> &id2725in_i = id1959out_result;

    id2725out_o = (cast_float2float<11,53>(id2725in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2732 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2732in_output_control = id3420out_output[getCycle()%9];
    const HWFloat<11,53> &id2732in_data = id2725out_o;

    bool id2732x_1;

    (id2732x_1) = ((id2732in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2732x_1)) {
      writeOutput(m_x1_out, id2732in_data);
    }
  }
  { // Node ID: 3441 (NodeConstantRawBits)
  }
  { // Node ID: 2735 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2735in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2735in_b = id3441out_value;

    id2735out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2735in_a,id2735in_b));
  }
  { // Node ID: 3008 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3008in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3008in_b = id2735out_result[getCycle()%2];

    id3008out_result[(getCycle()+1)%2] = (eq_fixed(id3008in_a,id3008in_b));
  }
  { // Node ID: 2737 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2738out_result;

  { // Node ID: 2738 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2738in_a = id2737out_io_m_out_force_disabled;

    id2738out_result = (not_fixed(id2738in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2739out_result;

  { // Node ID: 2739 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2739in_a = id3008out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2739in_b = id2738out_result;

    HWOffsetFix<1,0,UNSIGNED> id2739x_1;

    (id2739x_1) = (and_fixed(id2739in_a,id2739in_b));
    id2739out_result = (id2739x_1);
  }
  { // Node ID: 3422 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3422in_input = id2739out_result;

    id3422out_output[(getCycle()+8)%9] = id3422in_input;
  }
  HWFloat<11,53> id2733out_o;

  { // Node ID: 2733 (NodeCast)
    const HWFloat<11,45> &id2733in_i = id1965out_result;

    id2733out_o = (cast_float2float<11,53>(id2733in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2740 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2740in_output_control = id3422out_output[getCycle()%9];
    const HWFloat<11,53> &id2740in_data = id2733out_o;

    bool id2740x_1;

    (id2740x_1) = ((id2740in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2740x_1)) {
      writeOutput(m_m_out, id2740in_data);
    }
  }
  { // Node ID: 3440 (NodeConstantRawBits)
  }
  { // Node ID: 2743 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2743in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2743in_b = id3440out_value;

    id2743out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2743in_a,id2743in_b));
  }
  { // Node ID: 3009 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3009in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3009in_b = id2743out_result[getCycle()%2];

    id3009out_result[(getCycle()+1)%2] = (eq_fixed(id3009in_a,id3009in_b));
  }
  { // Node ID: 2745 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2746out_result;

  { // Node ID: 2746 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2746in_a = id2745out_io_f_out_force_disabled;

    id2746out_result = (not_fixed(id2746in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2747out_result;

  { // Node ID: 2747 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2747in_a = id3009out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2747in_b = id2746out_result;

    HWOffsetFix<1,0,UNSIGNED> id2747x_1;

    (id2747x_1) = (and_fixed(id2747in_a,id2747in_b));
    id2747out_result = (id2747x_1);
  }
  { // Node ID: 3424 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3424in_input = id2747out_result;

    id3424out_output[(getCycle()+8)%9] = id3424in_input;
  }
  HWFloat<11,53> id2741out_o;

  { // Node ID: 2741 (NodeCast)
    const HWFloat<11,45> &id2741in_i = id1989out_result;

    id2741out_o = (cast_float2float<11,53>(id2741in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2748 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2748in_output_control = id3424out_output[getCycle()%9];
    const HWFloat<11,53> &id2748in_data = id2741out_o;

    bool id2748x_1;

    (id2748x_1) = ((id2748in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2748x_1)) {
      writeOutput(m_f_out, id2748in_data);
    }
  }
  { // Node ID: 3439 (NodeConstantRawBits)
  }
  { // Node ID: 2751 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2751in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2751in_b = id3439out_value;

    id2751out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2751in_a,id2751in_b));
  }
  { // Node ID: 3010 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3010in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3010in_b = id2751out_result[getCycle()%2];

    id3010out_result[(getCycle()+1)%2] = (eq_fixed(id3010in_a,id3010in_b));
  }
  { // Node ID: 2753 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2754out_result;

  { // Node ID: 2754 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2754in_a = id2753out_io_h_out_force_disabled;

    id2754out_result = (not_fixed(id2754in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2755out_result;

  { // Node ID: 2755 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2755in_a = id3010out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2755in_b = id2754out_result;

    HWOffsetFix<1,0,UNSIGNED> id2755x_1;

    (id2755x_1) = (and_fixed(id2755in_a,id2755in_b));
    id2755out_result = (id2755x_1);
  }
  { // Node ID: 3426 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3426in_input = id2755out_result;

    id3426out_output[(getCycle()+9)%10] = id3426in_input;
  }
  HWFloat<11,53> id2749out_o;

  { // Node ID: 2749 (NodeCast)
    const HWFloat<11,45> &id2749in_i = id1971out_result;

    id2749out_o = (cast_float2float<11,53>(id2749in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2756 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2756in_output_control = id3426out_output[getCycle()%10];
    const HWFloat<11,53> &id2756in_data = id2749out_o;

    bool id2756x_1;

    (id2756x_1) = ((id2756in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2756x_1)) {
      writeOutput(m_h_out, id2756in_data);
    }
  }
  { // Node ID: 3438 (NodeConstantRawBits)
  }
  { // Node ID: 2759 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2759in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2759in_b = id3438out_value;

    id2759out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2759in_a,id2759in_b));
  }
  { // Node ID: 3011 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3011in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3011in_b = id2759out_result[getCycle()%2];

    id3011out_result[(getCycle()+1)%2] = (eq_fixed(id3011in_a,id3011in_b));
  }
  { // Node ID: 2761 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2762out_result;

  { // Node ID: 2762 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2762in_a = id2761out_io_d_out_force_disabled;

    id2762out_result = (not_fixed(id2762in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2763out_result;

  { // Node ID: 2763 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2763in_a = id3011out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2763in_b = id2762out_result;

    HWOffsetFix<1,0,UNSIGNED> id2763x_1;

    (id2763x_1) = (and_fixed(id2763in_a,id2763in_b));
    id2763out_result = (id2763x_1);
  }
  { // Node ID: 3428 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3428in_input = id2763out_result;

    id3428out_output[(getCycle()+8)%9] = id3428in_input;
  }
  HWFloat<11,53> id2757out_o;

  { // Node ID: 2757 (NodeCast)
    const HWFloat<11,45> &id2757in_i = id1983out_result;

    id2757out_o = (cast_float2float<11,53>(id2757in_i));
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2764 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2764in_output_control = id3428out_output[getCycle()%9];
    const HWFloat<11,53> &id2764in_data = id2757out_o;

    bool id2764x_1;

    (id2764x_1) = ((id2764in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2764x_1)) {
      writeOutput(m_d_out, id2764in_data);
    }
  }
  { // Node ID: 3437 (NodeConstantRawBits)
  }
  { // Node ID: 2767 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id2767in_a = id3434out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id2767in_b = id3437out_value;

    id2767out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id2767in_a,id2767in_b));
  }
  { // Node ID: 3012 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id3012in_a = id3112out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id3012in_b = id2767out_result[getCycle()%2];

    id3012out_result[(getCycle()+1)%2] = (eq_fixed(id3012in_a,id3012in_b));
  }
  { // Node ID: 2769 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id2770out_result;

  { // Node ID: 2770 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id2770in_a = id2769out_io_j_out_force_disabled;

    id2770out_result = (not_fixed(id2770in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id2771out_result;

  { // Node ID: 2771 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id2771in_a = id3012out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id2771in_b = id2770out_result;

    HWOffsetFix<1,0,UNSIGNED> id2771x_1;

    (id2771x_1) = (and_fixed(id2771in_a,id2771in_b));
    id2771out_result = (id2771x_1);
  }
  { // Node ID: 3430 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id3430in_input = id2771out_result;

    id3430out_output[(getCycle()+9)%10] = id3430in_input;
  }
  HWFloat<11,53> id2765out_o;

  { // Node ID: 2765 (NodeCast)
    const HWFloat<11,45> &id2765in_i = id1977out_result;

    id2765out_o = (cast_float2float<11,53>(id2765in_i));
  }
  if ( (getFillLevel() >= (11l)) && (getFlushLevel() < (11l)|| !isFlushingActive() ))
  { // Node ID: 2772 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id2772in_output_control = id3430out_output[getCycle()%10];
    const HWFloat<11,53> &id2772in_data = id2765out_o;

    bool id2772x_1;

    (id2772x_1) = ((id2772in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(11l))&(isFlushingActive()))));
    if((id2772x_1)) {
      writeOutput(m_j_out, id2772in_data);
    }
  }
  { // Node ID: 2786 (NodeConstantRawBits)
  }
  { // Node ID: 3436 (NodeConstantRawBits)
  }
  { // Node ID: 2783 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (9l)))
  { // Node ID: 2784 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2784in_enable = id3436out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2784in_max = id2783out_value;

    HWOffsetFix<49,0,UNSIGNED> id2784x_1;
    HWOffsetFix<1,0,UNSIGNED> id2784x_2;
    HWOffsetFix<1,0,UNSIGNED> id2784x_3;
    HWOffsetFix<49,0,UNSIGNED> id2784x_4t_1e_1;

    id2784out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2784st_count)));
    (id2784x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2784st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2784x_2) = (gte_fixed((id2784x_1),id2784in_max));
    (id2784x_3) = (and_fixed((id2784x_2),id2784in_enable));
    id2784out_wrap = (id2784x_3);
    if((id2784in_enable.getValueAsBool())) {
      if(((id2784x_3).getValueAsBool())) {
        (id2784st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2784x_4t_1e_1) = (id2784x_1);
        (id2784st_count) = (id2784x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id2785out_output;

  { // Node ID: 2785 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id2785in_input = id2784out_count;

    id2785out_output = id2785in_input;
  }
  if ( (getFillLevel() >= (10l)) && (getFlushLevel() < (10l)|| !isFlushingActive() ))
  { // Node ID: 2787 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id2787in_load = id2786out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id2787in_data = id2785out_output;

    bool id2787x_1;

    (id2787x_1) = ((id2787in_load.getValueAsBool())&(!(((getFlushLevel())>=(10l))&(isFlushingActive()))));
    if((id2787x_1)) {
      setMappedRegValue("current_run_cycle_count", id2787in_data);
    }
  }
  { // Node ID: 3435 (NodeConstantRawBits)
  }
  { // Node ID: 2789 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 2790 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id2790in_enable = id3435out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id2790in_max = id2789out_value;

    HWOffsetFix<49,0,UNSIGNED> id2790x_1;
    HWOffsetFix<1,0,UNSIGNED> id2790x_2;
    HWOffsetFix<1,0,UNSIGNED> id2790x_3;
    HWOffsetFix<49,0,UNSIGNED> id2790x_4t_1e_1;

    id2790out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id2790st_count)));
    (id2790x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id2790st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id2790x_2) = (gte_fixed((id2790x_1),id2790in_max));
    (id2790x_3) = (and_fixed((id2790x_2),id2790in_enable));
    id2790out_wrap = (id2790x_3);
    if((id2790in_enable.getValueAsBool())) {
      if(((id2790x_3).getValueAsBool())) {
        (id2790st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id2790x_4t_1e_1) = (id2790x_1);
        (id2790st_count) = (id2790x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 2792 (NodeInputMappedReg)
  }
  { // Node ID: 3013 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id3013in_a = id2790out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id3013in_b = id2792out_run_cycle_count;

    id3013out_result[(getCycle()+1)%2] = (eq_fixed(id3013in_a,id3013in_b));
  }
  if ( (getFillLevel() >= (7l)))
  { // Node ID: 2791 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id2791in_start = id3013out_result[getCycle()%2];

    if((id2791in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
