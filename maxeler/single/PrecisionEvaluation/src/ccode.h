#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define TAUE          2.5
#define TAUN        250
#define ONE_O_TAUE    0.4
#define ONE_O_TAUN    0.004
#define EH            3
#define EN            1
#define ESTAR         0.8 //1.5415
#define EPS           0.01               //TAUE/TAUN
#define RE            1.204              //VARIED BETWEEN 0.5 AND 1.4
#define EXPRE         0.299991841        //EXP(-RE)
#define M            10
#define GAMMA         0.0011
#define ONE_O_ONE_MINUS_EXPRE   1.428554778


#define TVP      1.4506 //The same with Flavio's paper
#define TV1M     60.    //The same with Flavio's paper
#define TV2M     1150.  //The same with Flavio's paper

#define TWP      200.0   // We have TWP1 and TWP2 = TWP in Flavio's paper

//#define TW1P     200.0
//#define TW2P     200.0

#define TW1M     60.0    //The same with Flavio's paper
#define TW2M     15.     //The same with Flavio's paper
#define TS1      2.7342  //The same with Flavio's paper
#define TS2      16.     //The same with Flavio's paper
#define TFI      0.11    //The same with Flavio's paper
#define TO1      400.    //The same with Flavio's paper
#define TO2      6.      //The same with Flavio's paper
#define TSO1     30.0181 //The same with Flavio's paper
#define TSO2     0.9957  //The same with Flavio's paper

#define TSI      1.8875  // We have TSI1 and TSI2 = TSI in Flavio's paper
//#define TSI1     1.8875
//#define TSI2     1.8875


#define TWINF    0.07    //The same with Flavio's paper
#define THV      0.3     //EPUM // The same of Flavio's paper
#define THVM     0.006   //EPUQ // The same of Flavio's paper
#define THVINF   0.006   //EPUQ // The same of Flavio's paper
#define THW      0.13    //EPUP // The same of Flavio's paper
#define THWINF   0.006    //EPURR // In Flavio's paper 0.13
#define THSO     0.13    //EPUP // The same of Flavio's paper
#define THSI     0.13    //EPUP // The same of Flavio's paper
#define THO      0.006    //EPURR // The same of Flavio's paper
//#define KWP      5.7
#define KWM      65.     //The same of Flavio's paper
#define KS       2.0994  //The same of Flavio's paper
#define KSO      2.0458  //The same of Flavio's paper
//#define KSI      97.8
#define UWM      0.03    //The same of Flavio's paper
#define US       0.9087  //The same of Flavio's paper
#define UO       0.     // The same of Flavio's paper
#define UU       1.55   // The same of Flavio's paper
#define USO      0.65   // The same of Flavio's paper
#define SC       0.007
//#define WCP      0.15

#define WINFSTAR 0.94   // The same of Flavio's paper

#define TW2M_TW1M -45.0
#define TSO2_TSO1 -29.0224

#define TW2M_TW1M_DIVBY2 -22.5
#define TSO2_TSO1_DIVBY2 -14.5112


#define ATR_TVP        3.33 //The same with Flavio's paper
#define ATR_TV1M      19.2    //The same with Flavio's paper
#define ATR_TV2M      10.0  //The same with Flavio's paper

#define ATR_TWP      160.0   // We have TWP1 and TWP2 = TWP in Flavio's paper

#define ATR_TW1M      75.0    //The same with Flavio's paper
#define ATR_TW2M      75.0    //The same with Flavio's paper
#define ATR_TD         0.065
#define ATR_TSI       31.8364
#define ATR_TO        39.0
#define ATR_TA         0.009
#define ATR_UC         0.23
#define ATR_UV         0.055
#define ATR_UW         0.146
#define ATR_UO         0.0
#define ATR_UM         1.0
#define ATR_UCSI       0.8
#define ATR_USO        0.3

#define ATR_RSP        0.02
#define ATR_RSM        1.2
#define ATR_K          3.0
#define ATR_ASO        0.115
#define ATR_BSO        0.84
#define ATR_CSO        0.02


#define GS  0.09
#define	ENA 50
#define	GNAC 0.003
#define	GNA  4.


void cstep_karma(double * u, double * v, double dt) {


	    double  dfunc = pow((*v), 10);
	    double  rfunc = 1.428554778 - (*v);
	    double  hfunc = (1-tanh((*u)-EH)) * (*u) * (*u) * 0.5;
	   double   ffunc = -(*u) + (ESTAR-dfunc) * hfunc;
	      //gfunc = rfunc * (e[i] - EN > 0) - (1 - (e[i] -EN>0)) * n[i];
	     double h = ((*u) > EN);
	      //(a -n)*h  - (1-h)*n
	      //a*h -n*h  - (n - n*h)
	    double  gfunc = 1.428554778*h - (*v);
	    double  de    = ffunc * ONE_O_TAUE;
	     double dn    = gfunc * ONE_O_TAUN;

          (*u)       = (*u) + dt*de;
         (*v)  = (*v) + dt*dn;


}

void  cstep_epi ( double *ui, double *vi, double *wi, double *si,  double dt, double lap, double stim, int flag){

	  double tvm, vinf, winf;
	  double jfi, jso, jsi;
	  double twm, tso, ts, to, ds, dw, dv;

	  tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
      twm =    TW1M + (  TW2M_TW1M_DIVBY2)*(1.+tanh(  KWM*(*ui-  UWM)));
      tso =    TSO1 + (  TSO2_TSO1_DIVBY2)*(1.+tanh(  KSO*(*ui-  USO)));
      ts  = (*ui >   THW)  ?   TS2  :   TS1;
      to  = (*ui >   THO)  ?   TO2  :   TO1;

      vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  winf = (*ui >    THWINF) ?   WINFSTAR: 1.0-*ui/  TWINF;
      if (winf > 1) winf = 1.0;

      dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
      dw = (*ui >   THW) ? -*wi/  TWP : (winf-*wi)/twm;
      ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;

      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




	  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
	  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
	  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
	  //if (winf >one_o_twm) winf = one_o_twm;
	  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
	  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
	  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;





	  //Update gates

       *vi += dv*dt;
       *wi += dw*dt;
       *si += ds*dt;

	  //Compute currents
      jfi = (*ui >   THV)  ? -*vi * (*ui -   THV) * (  UU - *ui)/  TFI : 0.0;
      /*if (*ui >   THV){
         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
         	  }
           }*/
      jso = (*ui >   THSO) ? 1./tso : (*ui-  UO)/to;
      jsi = (*ui >   THSI) ? -*wi * *si/  TSI : 0.0;

      *ui = *ui  - (jfi+jso+jsi-stim)*dt + lap;
}



void init(double * u, double * x1, double * m, double * h, double * j, double * d, double *f, double * ca, double dt) {
      *u = -84.5737;
      *ca = 0.0000001;
      double ax1 = dt* ((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
     double bx1 = dt* (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));

      double am = dt *(-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
    double  bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));

    double  ah = dt*(0.126 * exp(-0.25 * ((*u) + 77.0)));
   double   bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));

     double aj = dt *(0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
     double bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));

    double  ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
    double  bd = dt *(0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));

    double  af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
    double  bf = dt * (0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));


   *x1 = (ax1 / (ax1 + bx1));
    *m = (am / (am + bm));
     *h = (ah / (ah + bh));
      *j = (aj / (aj + bj));
      *d = (ad / (ad + bd));
      *f = (af / (af + bf));

}

void beelerreuter_cstep(double * u, double * x1, double * m, double * h, double * j, double *d, double *f, double * ca, double dt) {


 double ax1 = dt *((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
  double        bx1 =  dt * (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));

  double        am = dt* (-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
  double        bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));

  double        ah = dt * (0.126 * exp(-0.25 * ((*u) + 77.0)));
  double        bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));

  double        aj = dt * (0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
  double        bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));

  double        ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
  double        bd =dt * (0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));

  double        af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
  double        bf = dt *(0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));

          *x1 = *(x1) + (ax1 * (1.0 - (*x1)) - bx1 * (*x1));
          *m = (*m) + (am * (1.0 - (*m)) - bm * (*m));
          *h =  (*h)+ (ah * (1.0 - (*h)) - bh * (*h));
          *j = (*j) + (aj * (1.0 - (*j)) - bj * (*j));
          *d = (*d) + (ad * (1.0 - (*d)) - bd * (*d));
          *f = (*f) + (af * (1.0 - (*f)) - bf * (*f));


         double es = (-82.3 - 13.0287 * log((*ca)));
         double is  =0.09*(*d)*(*f)*((*u)-es);
          *ca = *ca +dt*(-0.0000001*is+0.07*(0.0000001-(*ca)));

     double  xik1 = (exp(0.08 * ((*u) + 53.0)) + exp(0.04 * ((*u) + 53.0)));
   double       xik2 = (1.0 - exp(-0.04 * ((*u) + 23.0)));
          if (xik1 == 0.0) xik1 = 0.001;
          if (xik2 == 0.0) xik2 = 0.001;
   double       ik1 = (0.35 * (4.0 * (exp(0.04 * ((*u) + 85.0)) - 1.0) / xik1 + 0.2 * ((*u) + 23.0) / xik2));

        double  gix1 = (0.8 * (exp(0.04 * ((*u) + 77.0)) - 1.0) /exp(0.04 * ((*u) + 35.0)));
          double ix1 = (gix1 * (*x1));
          double ina = ((GNA * (*m) * (*m) * (*m) * (*h) * (*j) + GNAC) * ((*u) - ENA));
          (*u) = (*u) -( dt * (ik1 + ix1 + ina + is));

}

