#include <stdio.h>
#include <stdint.h>
#include "ccode.h"

#include "Maxfiles.h"

void initBellerReuter(float * u, float * x1, float * m, float * h, float * j, float * d, float *f, float * ca, float dt) {
      *u = -84.5737;
      *ca = 0.0000001;
      float ax1 = dt* ((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
     float bx1 = dt* (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));

      float am = dt *(-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
    float  bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));

    float  ah = dt*(0.126 * exp(-0.25 * ((*u) + 77.0)));
   float   bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));

     float aj = dt *(0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
     float bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));

    float  ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
    float  bd = dt *(0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));

    float  af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
    float  bf = dt * (0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));


   *x1 = (ax1 / (ax1 + bx1));
    *m = (am / (am + bm));
     *h = (ah / (ah + bh));
      *j = (aj / (aj + bj));
      *d = (ad / (ad + bd));
      *f = (af / (af + bf));
      printf("u: %g\nca: %g\nx1: %f\nm: %f\nh: %f\nj: %f\nf: %f\nd: %f\n", *u, *ca, *x1 ,*m, *h, *j, *f, *d);

}


void runBeelerReuter(float * u_out, float dt, int num_iteration) {

	int  sizeBytes = (num_iteration) * sizeof(float);
	printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
	//float * u_out = malloc(sizeBytes);
	float * ca_out = malloc(sizeBytes);
	float * x1_out = malloc(sizeBytes);
	float * h_out =  malloc(sizeBytes);
	float * f_out = malloc(sizeBytes);
	float * d_out = malloc(sizeBytes);
	float * m_out = malloc(sizeBytes);
	float * j_out = malloc(sizeBytes);

	float u, ca, f, x1, d,h,m,j;

	initBellerReuter(&u, &x1, &m ,&h, &j, &d, &f, &ca, dt);
	u = -50;
	ca = 0.0000001;

	struct timeval start, end;
			long mtime, seconds, useconds;


			max_file_t *myMaxFile = BeelerReuterDFE_init();
			max_engine_t *myDFE = max_load(myMaxFile, "local:*");


			BeelerReuterDFE_actions_t actions;

			actions.param_length = num_iteration;
			actions.param_dt = dt;

			actions.param_u_in = u;
			actions.param_ca_in = ca;
			actions.param_f_in = f;
			actions.param_d_in = d;
			actions.param_x1_in = x1;
			actions.param_m_in = m;
			actions.param_j_in = j;
			actions.param_h_in = h;

			actions.param_num_iteration = num_iteration;
			actions.outstream_u_out = u_out;
			actions.outstream_ca_out = ca_out;
			actions.outstream_x1_out = x1_out;
			actions.outstream_h_out = h_out;
			actions.outstream_d_out = d_out;
			actions.outstream_f_out = f_out;
			actions.outstream_m_out = m_out;
			actions.outstream_j_out = j_out;

			printf("Running DFE\n");
						gettimeofday(&start, NULL);

				BeelerReuterDFE_run(myDFE, &actions);
				//memcpy(us, u_out, sizeBytes);
				gettimeofday(&end, NULL);
					  seconds = end.tv_sec - start.tv_sec;
					  		useconds = end.tv_usec - start.tv_usec;

					  		mtime = ((seconds) * 1000 + useconds / 1000.0);
				printf("DFE elapsed time: %ld milliseconds \n", mtime);

				free(ca_out);
							ca_out = NULL;
							free(x1_out);
							x1_out = NULL;
							free(h_out);
							h_out = NULL;
							free(f_out);
							f_out = NULL;
							free(d_out);
							d_out = NULL;
							free(m_out);
							m_out = NULL;
							free(j_out);
							j_out = NULL;
							max_unload(myDFE);


}

void runKarma(float * u_out, float dt, int num_iteration) {

	int sizeBytes = num_iteration * sizeof(float);
		printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
		float * v_out = malloc(sizeBytes);


		struct timeval start, end;
				long mtime, seconds, useconds;


				max_file_t *myMaxFile = KarmaDFE_init();
				max_engine_t *myDFE = max_load(myMaxFile, "local:*");


				KarmaDFE_actions_t actions;

				actions.param_length = num_iteration;
				actions.param_dt = dt;

				actions.param_num_iteration = num_iteration;
					actions.outstream_u_out = u_out;
					actions.outstream_v_out = v_out;

					printf("Running DFE\n");
										gettimeofday(&start, NULL);

								KarmaDFE_run(myDFE, &actions);
								//memcpy(us, u_out, sizeBytes);
								gettimeofday(&end, NULL);
									  seconds = end.tv_sec - start.tv_sec;
									  		useconds = end.tv_usec - start.tv_usec;

									  		mtime = ((seconds) * 1000 + useconds / 1000.0);
								printf("DFE elapsed time: %ld milliseconds \n", mtime);

								free(v_out);
								v_out = NULL;
								max_unload(myDFE);

}

void runMinimal(float * u_out, float dt, int num_iteration) {


	int  sizeBytes = (num_iteration) * sizeof(float);
printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
float * v_out = malloc(sizeBytes);
float * w_out = malloc(sizeBytes);
float * s_out =  malloc(sizeBytes);
float * stim = malloc(sizeBytes);
int alive = 0;
struct timeval start, end;
		long mtime, seconds, useconds;


		//	initStim(stim);


max_file_t *myMaxFile = MinimalModelDFE_init();
max_engine_t *myDFE = max_load(myMaxFile, "local:*");


MinimalModelDFE_actions_t actions;

actions.param_length = num_iteration;
actions.param_dt = dt;
actions.param_num_iteration = num_iteration;
//actions.outstream_stim_out = stim;
actions.outstream_u_out = u_out;
actions.outstream_v_out = v_out;
actions.outstream_w_out = w_out;
actions.outstream_s_out = s_out;
//actions.outscalar_SingleCellDFEKernel_alive = &alive;






printf("Running DFE\n");
			gettimeofday(&start, NULL);

MinimalModelDFE_run(myDFE, &actions);
	//memcpy(us, u_out, sizeBytes);
	gettimeofday(&end, NULL);
		  seconds = end.tv_sec - start.tv_sec;
		  		useconds = end.tv_usec - start.tv_usec;

		  		mtime = ((seconds) * 1000 + useconds / 1000.0);
	printf("DFE elapsed time: %ld milliseconds \n", mtime);

	free(v_out);
		v_out = NULL;
		free(w_out);
		w_out = NULL;
		free(s_out);
		s_out = NULL;
		free(stim);
		stim = NULL;
		max_unload(myDFE);


}


void compute_lse_cepi (double dt)
{

      float t_sim_time      = 1000.0; //in ms


      int  num_element  = (int)floor(t_sim_time/dt + 0.5);

      float * t0_result_karma     =  malloc(num_element * sizeof(float));
      float * t0_result_br    =  malloc(num_element * sizeof(float));
      float * t0_result_minimal     = malloc(num_element * sizeof(float));
      runKarma(t0_result_karma, dt, num_element);
    // runBeelerReuter(t0_result_br, dt, num_element);
      //runMinimal(t0_result_minimal, dt, num_element);



      double * t1_result_karma     = (double *) calloc (num_element+1, sizeof(double));
      double * t1_result_br     = (double *) calloc (num_element+1, sizeof(double));
      double * t1_result_minimal = (double *) calloc (num_element+1, sizeof(double));
      double u = 1.5;
      double v = 0;

      double u_br = 0;
     double  x1 = 0;
  	 double m = 0;
  	 double  h = 0;
  	double  j = 0;
  	double  d = 0;
  	double f = 0;
  	double ca = 0;

  	init(&u_br, &x1, &m, &h, &j, &d, &f, &ca, dt);
  	u_br = -50;

    double   t1_u          = 1.0;
       double   t1_v          = 1.0;
       double   t1_w          = 1.0;
 	  double   t1_s          = 0.0;

      for (int i=0; i < num_element; i++)
      {
    	  cstep_karma(&u, &v, dt);
   	 t1_result_karma[i] = u;

     // beelerreuter_cstep(&u_br, &x1, &m, &h, &j,&d, &f, &ca, dt);
    	// t1_result_br[i] = u_br;
//
  //  	 cstep_epi(&t1_u, &t1_v, &t1_w, &t1_s, dt, 0,0,0);
    //	 t1_result_minimal[i] = t1_u;
      }

 	  //printf("%f\t%f\n", t1_t1, t1_u);

	  double lse_karma = 0.0;
	  double lse_br = 0.0;
	  double lse_minimal = 0.0;

	  for (int i=0; i < num_element; i++)
	  {
	      lse_karma += pow(((t1_result_karma[i]) - (t0_result_karma[i])),2);
	    //   lse_br += pow(((t1_result_br[i]) - (t0_result_br[i])),2);
	      //lse_minimal += pow(((t1_result_minimal[i]) - (t0_result_minimal[i])),2);
	  }

	  printf("KARMA: lse = %g\n", lse_karma);
	  printf("BEELER-REUTER: lse = %g\n", lse_br);
	  printf("MINIMAL: lse = %g\n", lse_minimal);


	  free(t0_result_karma);
	  free(t1_result_karma);
	  free(t0_result_br);
	  free(t1_result_br);
	  free(t0_result_minimal);
	  free(t1_result_minimal);


}

int main(void) {

	compute_lse_cepi(0.1/64);
	//compute_lse_cepi(0.1, runMinimal);

	return 0;
}
