#include "stdsimheader.h"

using namespace maxcompilersim;

namespace maxcompilersim {
// Templated Types used in the kernel
template class HWOffsetFix<47,0,UNSIGNED>;
template class HWOffsetFix<48,0,UNSIGNED>;
template class HWOffsetFix<33,-48,UNSIGNED>;
template class HWOffsetFix<49,0,UNSIGNED>;
template class HWOffsetFix<8,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<5,-5,UNSIGNED>;
template class HWRawBits<47>;
template class HWOffsetFix<64,-61,UNSIGNED>;
template class HWOffsetFix<10,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<47,-72,UNSIGNED>;
template class HWOffsetFix<64,-89,UNSIGNED>;
template class HWOffsetFix<48,-48,UNSIGNED>;
template class HWOffsetFix<11,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<1,0,UNSIGNED>;
template class HWOffsetFix<64,-62,UNSIGNED>;
template class HWOffsetFix<54,-51,UNSIGNED>;
template class HWOffsetFix<61,-51,TWOSCOMPLEMENT>;
template class HWRawBits<10>;
template class HWOffsetFix<10,-15,UNSIGNED>;
template class HWOffsetFix<4,0,UNSIGNED>;
template class HWOffsetFix<64,-77,UNSIGNED>;
template class HWOffsetFix<48,-47,UNSIGNED>;
template class HWOffsetFix<10,-25,UNSIGNED>;
template class HWOffsetFix<47,-47,UNSIGNED>;
template class HWOffsetFix<113,-102,TWOSCOMPLEMENT>;
template class HWOffsetFix<8,0,UNSIGNED>;
template class HWOffsetFix<5,0,UNSIGNED>;
template class HWOffsetFix<32,0,UNSIGNED>;
template class HWOffsetFix<33,0,UNSIGNED>;
template class HWOffsetFix<53,-51,UNSIGNED>;
template class HWRawBits<56>;
template class HWOffsetFix<64,-63,UNSIGNED>;
template class HWOffsetFix<21,-21,UNSIGNED>;
template class HWRawBits<2>;
template class HWFloat<11,53>;
template class HWOffsetFix<43,-48,UNSIGNED>;
template class HWRawBits<5>;
template class HWRawBits<1>;
template class HWFloat<8,48>;
template class HWOffsetFix<11,0,UNSIGNED>;
template class HWOffsetFix<10,0,UNSIGNED>;
template class HWOffsetFix<12,0,UNSIGNED>;
template class HWOffsetFix<64,-68,UNSIGNED>;
template class HWOffsetFix<26,-51,UNSIGNED>;
template class HWOffsetFix<52,-51,UNSIGNED>;
template class HWRawBits<9>;
template class HWRawBits<8>;
// add. templates from the default formatter 


// Templated Methods/Functions
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<4,0,UNSIGNED> &a, const HWOffsetFix<4,0,UNSIGNED> &b );
template HWRawBits<5> cast_fixed2bits<>(const HWOffsetFix<5,-5,UNSIGNED> &a);
template HWOffsetFix<53,-51,UNSIGNED>add_fixed <53,-51,UNSIGNED,TONEAREVEN,43,-48,UNSIGNED,52,-51,UNSIGNED, false>(const HWOffsetFix<43,-48,UNSIGNED> &a, const HWOffsetFix<52,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWRawBits<2> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b);
template HWFloat<8,48>sub_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<54,-51,UNSIGNED>add_fixed <54,-51,UNSIGNED,TONEAREVEN,33,-48,UNSIGNED,53,-51,UNSIGNED, false>(const HWOffsetFix<33,-48,UNSIGNED> &a, const HWOffsetFix<53,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gt_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<11,0,UNSIGNED> &a, const HWOffsetFix<11,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>or_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>not_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a );
template HWRawBits<47> slice<0,47>(const HWFloat<8,48> &a);
template HWOffsetFix<10,0,UNSIGNED> cast_bits2fixed<10,0,UNSIGNED>(const HWRawBits<10> &a);
template HWOffsetFix<64,-62,UNSIGNED>add_fixed <64,-62,UNSIGNED,TONEAREVEN,53,-51,UNSIGNED,64,-68,UNSIGNED, false>(const HWOffsetFix<53,-51,UNSIGNED> &a, const HWOffsetFix<64,-68,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>eq_bits<>(const HWRawBits<8> &a,  const HWRawBits<8> &b );
template HWOffsetFix<10,0,TWOSCOMPLEMENT> cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<11,0,UNSIGNED> cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<12,0,UNSIGNED> &a);
template void KernelManagerBlockSync::writeOutput< HWFloat<11,53> >(const t_port_number port_number, const HWFloat<11,53> &value);
template HWFloat<8,48> cast_float2float<8,48>(const HWFloat<11,53> &a);
template HWOffsetFix<52,-51,UNSIGNED>add_fixed <52,-51,UNSIGNED,TONEAREVEN,48,-48,UNSIGNED,26,-51,UNSIGNED, false>(const HWOffsetFix<48,-48,UNSIGNED> &a, const HWOffsetFix<26,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<5,-5,UNSIGNED> cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b );
template HWOffsetFix<1,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<1,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<48,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name);
template HWRawBits<56> cat<>(const HWRawBits<9> &a, const HWOffsetFix<47,0,UNSIGNED> &b);
template HWOffsetFix<8,0,TWOSCOMPLEMENT> cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<26,-51,UNSIGNED> cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b );
template HWFloat<11,53> KernelManagerBlockSync::getMappedRegValue< HWFloat<11,53> >(const std::string &name);
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<33,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>neq_bits<>(const HWRawBits<47> &a,  const HWRawBits<47> &b );
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-15,UNSIGNED> &a);
template HWOffsetFix<12,0,UNSIGNED> cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<11,0,UNSIGNED> &a);
template HWOffsetFix<26,-51,UNSIGNED> cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(const HWOffsetFix<47,-72,UNSIGNED> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT>add_fixed <11,0,TWOSCOMPLEMENT,TRUNCATE,11,0,TWOSCOMPLEMENT,11,0,TWOSCOMPLEMENT, false>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b , EXCEPTOVERFLOW);
template HWOffsetFix<64,-77,UNSIGNED>mul_fixed <64,-77,UNSIGNED,TONEAREVEN,53,-51,UNSIGNED,33,-48,UNSIGNED, false>(const HWOffsetFix<53,-51,UNSIGNED> &a, const HWOffsetFix<33,-48,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b);
template HWOffsetFix<47,-47,UNSIGNED> cast_fixed2fixed<47,-47,UNSIGNED,TONEAREVEN>(const HWOffsetFix<48,-47,UNSIGNED> &a);
template HWOffsetFix<12,0,UNSIGNED>add_fixed <12,0,UNSIGNED,TRUNCATE,12,0,UNSIGNED,12,0,UNSIGNED, false>(const HWOffsetFix<12,0,UNSIGNED> &a, const HWOffsetFix<12,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<53,-51,UNSIGNED> cast_fixed2fixed<53,-51,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-62,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>lt_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<33,0,UNSIGNED>add_fixed <33,0,UNSIGNED,TRUNCATE,33,0,UNSIGNED,33,0,UNSIGNED, false>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,48> cast_bits2float<8,48>(const HWRawBits<56> &a);
template HWRawBits<8> slice<47,8>(const HWFloat<8,48> &a);
template HWRawBits<1> slice<10,1>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWFloat<8,48>neg_float<>(const HWFloat<8,48> &a );
template HWOffsetFix<11,0,UNSIGNED>sub_fixed <11,0,UNSIGNED,TONEAREVEN,11,0,UNSIGNED,11,0,UNSIGNED, false>(const HWOffsetFix<11,0,UNSIGNED> &a, const HWOffsetFix<11,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED> cast_bits2fixed<1,0,UNSIGNED>(const HWRawBits<1> &a);
template HWOffsetFix<10,-25,UNSIGNED> cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a);
template HWRawBits<56> cat<>(const HWRawBits<9> &a, const HWOffsetFix<47,-47,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWFloat<8,48>add_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<64,-63,UNSIGNED>add_fixed <64,-63,UNSIGNED,TONEAREVEN,52,-51,UNSIGNED,64,-89,UNSIGNED, false>(const HWOffsetFix<52,-51,UNSIGNED> &a, const HWOffsetFix<64,-89,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<11,53> cast_float2float<11,53>(const HWFloat<8,48> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<12,0,UNSIGNED> &a, const HWOffsetFix<12,0,UNSIGNED> &b );
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,TWOSCOMPLEMENT> &b);
template HWRawBits<10> cast_fixed2bits<>(const HWOffsetFix<10,-25,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<48,0,UNSIGNED> &a, const HWOffsetFix<48,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<48,-47,UNSIGNED> &a, const HWOffsetFix<48,-47,UNSIGNED> &b );
template HWFloat<8,48>mul_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<10,-15,UNSIGNED> cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a);
template HWOffsetFix<113,-102,TWOSCOMPLEMENT>mul_fixed <113,-102,TWOSCOMPLEMENT,TONEAREVEN,61,-51,TWOSCOMPLEMENT,52,-51,UNSIGNED, true>(const HWOffsetFix<61,-51,TWOSCOMPLEMENT> &a, const HWOffsetFix<52,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<33,0,UNSIGNED> cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWOffsetFix<61,-51,TWOSCOMPLEMENT> cast_fixed2fixed<61,-51,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<113,-102,TWOSCOMPLEMENT> &a, HWOffsetFix<1,0,UNSIGNED>* exception );
template HWOffsetFix<54,-51,UNSIGNED> cast_fixed2fixed<54,-51,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-61,UNSIGNED> &a);
template void KernelManagerBlockSync::setMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name, const HWOffsetFix<48,0,UNSIGNED> & value);
template HWOffsetFix<64,-68,UNSIGNED>mul_fixed <64,-68,UNSIGNED,TONEAREVEN,52,-51,UNSIGNED,43,-48,UNSIGNED, false>(const HWOffsetFix<52,-51,UNSIGNED> &a, const HWOffsetFix<43,-48,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<48,-47,UNSIGNED> cast_fixed2fixed<48,-47,UNSIGNED,TONEAREVEN>(const HWOffsetFix<54,-51,UNSIGNED> &a);
template HWOffsetFix<48,0,UNSIGNED> cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<49,0,UNSIGNED> &a);
template HWOffsetFix<64,-61,UNSIGNED>add_fixed <64,-61,UNSIGNED,TONEAREVEN,54,-51,UNSIGNED,64,-77,UNSIGNED, false>(const HWOffsetFix<54,-51,UNSIGNED> &a, const HWOffsetFix<64,-77,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<11,0,TWOSCOMPLEMENT> cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<10,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<49,0,UNSIGNED>add_fixed <49,0,UNSIGNED,TRUNCATE,49,0,UNSIGNED,49,0,UNSIGNED, false>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<61,-51,TWOSCOMPLEMENT> cast_float2fixed<61,-51,TWOSCOMPLEMENT,TONEAREVEN>(const HWFloat<8,48> &a, HWOffsetFix<4,0,UNSIGNED>* exception );
template HWOffsetFix<52,-51,UNSIGNED> cast_fixed2fixed<52,-51,UNSIGNED,TONEAREVEN>(const HWOffsetFix<64,-63,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWOffsetFix<47,-72,UNSIGNED>mul_fixed <47,-72,UNSIGNED,TONEAREVEN,21,-21,UNSIGNED,26,-51,UNSIGNED, false>(const HWOffsetFix<21,-21,UNSIGNED> &a, const HWOffsetFix<26,-51,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<32,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<32,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<5,0,UNSIGNED> cast_bits2fixed<5,0,UNSIGNED>(const HWRawBits<5> &a);
template HWFloat<8,48>div_float<>(const HWFloat<8,48> &a, const HWFloat<8,48> &b );
template HWOffsetFix<1,0,UNSIGNED>and_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<64,-89,UNSIGNED>mul_fixed <64,-89,UNSIGNED,TONEAREVEN,26,-51,UNSIGNED,48,-48,UNSIGNED, false>(const HWOffsetFix<26,-51,UNSIGNED> &a, const HWOffsetFix<48,-48,UNSIGNED> &b , EXCEPTOVERFLOW);


// Additional Code

} // End of maxcompilersim namespace
