#include "stdsimheader.h"

namespace maxcompilersim {

void MinimalModelDFEKernel::execute0() {
  { // Node ID: 40 (NodeConstantRawBits)
  }
  { // Node ID: 611 (NodeConstantRawBits)
  }
  { // Node ID: 541 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id541in_input = id611out_value;

    id541out_output[(getCycle()+1)%2] = id541in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 43 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id43in_enable = id40out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id43in_max = id541out_output[getCycle()%2];

    HWOffsetFix<12,0,UNSIGNED> id43x_1;
    HWOffsetFix<1,0,UNSIGNED> id43x_2;
    HWOffsetFix<1,0,UNSIGNED> id43x_3;
    HWOffsetFix<12,0,UNSIGNED> id43x_4t_1e_1;

    id43out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id43st_count)));
    (id43x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id43st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id43x_2) = (gte_fixed((id43x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id43in_max))));
    (id43x_3) = (and_fixed((id43x_2),id43in_enable));
    id43out_wrap = (id43x_3);
    if((id43in_enable.getValueAsBool())) {
      if(((id43x_3).getValueAsBool())) {
        (id43st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id43x_4t_1e_1) = (id43x_1);
        (id43st_count) = (id43x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 653 (NodeConstantRawBits)
  }
  { // Node ID: 444 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id444in_a = id611out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id444in_b = id653out_value;

    id444out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id444in_a,id444in_b));
  }
  { // Node ID: 489 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id489in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id489in_b = id444out_result[getCycle()%2];

    id489out_result[(getCycle()+1)%2] = (eq_fixed(id489in_a,id489in_b));
  }
  { // Node ID: 446 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id447out_result;

  { // Node ID: 447 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id447in_a = id446out_io_u_out_force_disabled;

    id447out_result = (not_fixed(id447in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id448out_result;

  { // Node ID: 448 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id448in_a = id489out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id448in_b = id447out_result;

    HWOffsetFix<1,0,UNSIGNED> id448x_1;

    (id448x_1) = (and_fixed(id448in_a,id448in_b));
    id448out_result = (id448x_1);
  }
  { // Node ID: 597 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id597in_input = id448out_result;

    id597out_output[(getCycle()+163)%164] = id597in_input;
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 42 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id42in_enable = id43out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id42in_max = id41out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id42x_1;
    HWOffsetFix<1,0,UNSIGNED> id42x_2;
    HWOffsetFix<1,0,UNSIGNED> id42x_3;
    HWOffsetFix<33,0,UNSIGNED> id42x_4t_1e_1;

    id42out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id42st_count)));
    (id42x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id42st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id42x_2) = (gte_fixed((id42x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id42in_max))));
    (id42x_3) = (and_fixed((id42x_2),id42in_enable));
    id42out_wrap = (id42x_3);
    if((id42in_enable.getValueAsBool())) {
      if(((id42x_3).getValueAsBool())) {
        (id42st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id42x_4t_1e_1) = (id42x_1);
        (id42st_count) = (id42x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 652 (NodeConstantRawBits)
  }
  { // Node ID: 490 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id490in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id490in_b = id652out_value;

    id490out_result[(getCycle()+1)%2] = (eq_fixed(id490in_a,id490in_b));
  }
  HWFloat<11,53> id537out_output;

  { // Node ID: 537 (NodeStreamOffset)
    const HWFloat<11,53> &id537in_input = id437out_result[getCycle()%15];

    id537out_output = id537in_input;
  }
  { // Node ID: 38 (NodeConstantRawBits)
  }
  { // Node ID: 48 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id48in_sel = id490out_result[getCycle()%2];
    const HWFloat<11,53> &id48in_option0 = id537out_output;
    const HWFloat<11,53> &id48in_option1 = id38out_value;

    HWFloat<11,53> id48x_1;

    switch((id48in_sel.getValueAsLong())) {
      case 0l:
        id48x_1 = id48in_option0;
        break;
      case 1l:
        id48x_1 = id48in_option1;
        break;
      default:
        id48x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id48out_result[(getCycle()+1)%2] = (id48x_1);
  }
  { // Node ID: 563 (NodeFIFO)
    const HWFloat<11,53> &id563in_input = id48out_result[getCycle()%2];

    id563out_output[(getCycle()+9)%10] = id563in_input;
  }
  { // Node ID: 600 (NodeFIFO)
    const HWFloat<11,53> &id600in_input = id563out_output[getCycle()%10];

    id600out_output[(getCycle()+1)%2] = id600in_input;
  }
  { // Node ID: 601 (NodeFIFO)
    const HWFloat<11,53> &id601in_input = id600out_output[getCycle()%2];

    id601out_output[(getCycle()+2)%3] = id601in_input;
  }
  { // Node ID: 602 (NodeFIFO)
    const HWFloat<11,53> &id602in_input = id601out_output[getCycle()%3];

    id602out_output[(getCycle()+12)%13] = id602in_input;
  }
  { // Node ID: 603 (NodeFIFO)
    const HWFloat<11,53> &id603in_input = id602out_output[getCycle()%13];

    id603out_output[(getCycle()+17)%18] = id603in_input;
  }
  { // Node ID: 604 (NodeFIFO)
    const HWFloat<11,53> &id604in_input = id603out_output[getCycle()%18];

    id604out_output[(getCycle()+9)%10] = id604in_input;
  }
  { // Node ID: 605 (NodeFIFO)
    const HWFloat<11,53> &id605in_input = id604out_output[getCycle()%10];

    id605out_output[(getCycle()+55)%56] = id605in_input;
  }
  { // Node ID: 606 (NodeFIFO)
    const HWFloat<11,53> &id606in_input = id605out_output[getCycle()%56];

    id606out_output[(getCycle()+14)%15] = id606in_input;
  }
  { // Node ID: 607 (NodeFIFO)
    const HWFloat<11,53> &id607in_input = id606out_output[getCycle()%15];

    id607out_output[(getCycle()+29)%30] = id607in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  { // Node ID: 414 (NodeGt)
    const HWFloat<11,53> &id414in_a = id605out_output[getCycle()%56];
    const HWFloat<11,53> &id414in_b = id15out_value;

    id414out_result[(getCycle()+2)%3] = (gt_float(id414in_a,id414in_b));
  }
  { // Node ID: 37 (NodeConstantRawBits)
  }
  { // Node ID: 651 (NodeConstantRawBits)
  }
  { // Node ID: 491 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id491in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id491in_b = id651out_value;

    id491out_result[(getCycle()+1)%2] = (eq_fixed(id491in_a,id491in_b));
  }
  HWFloat<11,53> id538out_output;

  { // Node ID: 538 (NodeStreamOffset)
    const HWFloat<11,53> &id538in_input = id409out_result[getCycle()%15];

    id538out_output = id538in_input;
  }
  { // Node ID: 543 (NodeFIFO)
    const HWFloat<11,53> &id543in_input = id538out_output;

    id543out_output[(getCycle()+136)%137] = id543in_input;
  }
  { // Node ID: 51 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id51in_sel = id491out_result[getCycle()%2];
    const HWFloat<11,53> &id51in_option0 = id543out_output[getCycle()%137];
    const HWFloat<11,53> &id51in_option1 = id38out_value;

    HWFloat<11,53> id51x_1;

    switch((id51in_sel.getValueAsLong())) {
      case 0l:
        id51x_1 = id51in_option0;
        break;
      case 1l:
        id51x_1 = id51in_option1;
        break;
      default:
        id51x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id51out_result[(getCycle()+1)%2] = (id51x_1);
  }
  { // Node ID: 544 (NodeFIFO)
    const HWFloat<11,53> &id544in_input = id51out_result[getCycle()%2];

    id544out_output[(getCycle()+12)%13] = id544in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id287out_result;

  { // Node ID: 287 (NodeGt)
    const HWFloat<11,53> &id287in_a = id48out_result[getCycle()%2];
    const HWFloat<11,53> &id287in_b = id15out_value;

    id287out_result = (gt_float(id287in_a,id287in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id278out_result;

  { // Node ID: 278 (NodeGt)
    const HWFloat<11,53> &id278in_a = id48out_result[getCycle()%2];
    const HWFloat<11,53> &id278in_b = id17out_value;

    id278out_result = (gt_float(id278in_a,id278in_b));
  }
  HWFloat<11,53> id279out_result;

  { // Node ID: 279 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id279in_sel = id278out_result;
    const HWFloat<11,53> &id279in_option0 = id38out_value;
    const HWFloat<11,53> &id279in_option1 = id37out_value;

    HWFloat<11,53> id279x_1;

    switch((id279in_sel.getValueAsLong())) {
      case 0l:
        id279x_1 = id279in_option0;
        break;
      case 1l:
        id279x_1 = id279in_option1;
        break;
      default:
        id279x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id279out_result = (id279x_1);
  }
  HWFloat<11,53> id290out_result;

  { // Node ID: 290 (NodeSub)
    const HWFloat<11,53> &id290in_a = id279out_result;
    const HWFloat<11,53> &id290in_b = id51out_result[getCycle()%2];

    id290out_result = (sub_float(id290in_a,id290in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<11,53> &id58in_a = id48out_result[getCycle()%2];
    const HWFloat<11,53> &id58in_b = id16out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<11,53> id59out_result;

  { // Node ID: 59 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id59in_sel = id58out_result;
    const HWFloat<11,53> &id59in_option0 = id1out_value;
    const HWFloat<11,53> &id59in_option1 = id2out_value;

    HWFloat<11,53> id59x_1;

    switch((id59in_sel.getValueAsLong())) {
      case 0l:
        id59x_1 = id59in_option0;
        break;
      case 1l:
        id59x_1 = id59in_option1;
        break;
      default:
        id59x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id59out_result = (id59x_1);
  }
  HWFloat<11,53> id291out_result;

  { // Node ID: 291 (NodeDiv)
    const HWFloat<11,53> &id291in_a = id290out_result;
    const HWFloat<11,53> &id291in_b = id59out_result;

    id291out_result = (div_float(id291in_a,id291in_b));
  }
  HWFloat<11,53> id288out_result;

  { // Node ID: 288 (NodeNeg)
    const HWFloat<11,53> &id288in_a = id51out_result[getCycle()%2];

    id288out_result = (neg_float(id288in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  HWFloat<11,53> id289out_result;

  { // Node ID: 289 (NodeDiv)
    const HWFloat<11,53> &id289in_a = id288out_result;
    const HWFloat<11,53> &id289in_b = id0out_value;

    id289out_result = (div_float(id289in_a,id289in_b));
  }
  HWFloat<11,53> id292out_result;

  { // Node ID: 292 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id292in_sel = id287out_result;
    const HWFloat<11,53> &id292in_option0 = id291out_result;
    const HWFloat<11,53> &id292in_option1 = id289out_result;

    HWFloat<11,53> id292x_1;

    switch((id292in_sel.getValueAsLong())) {
      case 0l:
        id292x_1 = id292in_option0;
        break;
      case 1l:
        id292x_1 = id292in_option1;
        break;
      default:
        id292x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id292out_result = (id292x_1);
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 408 (NodeMul)
    const HWFloat<11,53> &id408in_a = id292out_result;
    const HWFloat<11,53> &id408in_b = id44out_dt;

    id408out_result[(getCycle()+12)%13] = (mul_float(id408in_a,id408in_b));
  }
  { // Node ID: 409 (NodeAdd)
    const HWFloat<11,53> &id409in_a = id544out_output[getCycle()%13];
    const HWFloat<11,53> &id409in_b = id408out_result[getCycle()%13];

    id409out_result[(getCycle()+14)%15] = (add_float(id409in_a,id409in_b));
  }
  HWFloat<11,53> id415out_result;

  { // Node ID: 415 (NodeNeg)
    const HWFloat<11,53> &id415in_a = id409out_result[getCycle()%15];

    id415out_result = (neg_float(id415in_a));
  }
  { // Node ID: 416 (NodeSub)
    const HWFloat<11,53> &id416in_a = id601out_output[getCycle()%3];
    const HWFloat<11,53> &id416in_b = id15out_value;

    id416out_result[(getCycle()+14)%15] = (sub_float(id416in_a,id416in_b));
  }
  { // Node ID: 417 (NodeMul)
    const HWFloat<11,53> &id417in_a = id415out_result;
    const HWFloat<11,53> &id417in_b = id416out_result[getCycle()%15];

    id417out_result[(getCycle()+12)%13] = (mul_float(id417in_a,id417in_b));
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  { // Node ID: 418 (NodeSub)
    const HWFloat<11,53> &id418in_a = id29out_value;
    const HWFloat<11,53> &id418in_b = id602out_output[getCycle()%13];

    id418out_result[(getCycle()+14)%15] = (sub_float(id418in_a,id418in_b));
  }
  { // Node ID: 419 (NodeMul)
    const HWFloat<11,53> &id419in_a = id417out_result[getCycle()%13];
    const HWFloat<11,53> &id419in_b = id418out_result[getCycle()%15];

    id419out_result[(getCycle()+12)%13] = (mul_float(id419in_a,id419in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 420 (NodeDiv)
    const HWFloat<11,53> &id420in_a = id419out_result[getCycle()%13];
    const HWFloat<11,53> &id420in_b = id8out_value;

    id420out_result[(getCycle()+57)%58] = (div_float(id420in_a,id420in_b));
  }
  { // Node ID: 421 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id421in_sel = id414out_result[getCycle()%3];
    const HWFloat<11,53> &id421in_option0 = id37out_value;
    const HWFloat<11,53> &id421in_option1 = id420out_result[getCycle()%58];

    HWFloat<11,53> id421x_1;

    switch((id421in_sel.getValueAsLong())) {
      case 0l:
        id421x_1 = id421in_option0;
        break;
      case 1l:
        id421x_1 = id421in_option1;
        break;
      default:
        id421x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id421out_result[(getCycle()+1)%2] = (id421x_1);
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  { // Node ID: 422 (NodeGt)
    const HWFloat<11,53> &id422in_a = id605out_output[getCycle()%56];
    const HWFloat<11,53> &id422in_b = id20out_value;

    id422out_result[(getCycle()+2)%3] = (gt_float(id422in_a,id422in_b));
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id276out_result;

  { // Node ID: 276 (NodeGt)
    const HWFloat<11,53> &id276in_a = id604out_output[getCycle()%10];
    const HWFloat<11,53> &id276in_b = id22out_value;

    id276out_result = (gt_float(id276in_a,id276in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  HWFloat<11,53> id277out_result;

  { // Node ID: 277 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id277in_sel = id276out_result;
    const HWFloat<11,53> &id277in_option0 = id9out_value;
    const HWFloat<11,53> &id277in_option1 = id10out_value;

    HWFloat<11,53> id277x_1;

    switch((id277in_sel.getValueAsLong())) {
      case 0l:
        id277x_1 = id277in_option0;
        break;
      case 1l:
        id277x_1 = id277in_option1;
        break;
      default:
        id277x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id277out_result = (id277x_1);
  }
  { // Node ID: 426 (NodeDiv)
    const HWFloat<11,53> &id426in_a = id604out_output[getCycle()%10];
    const HWFloat<11,53> &id426in_b = id277out_result;

    id426out_result[(getCycle()+57)%58] = (div_float(id426in_a,id426in_b));
  }
  { // Node ID: 650 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 36 (NodeConstantRawBits)
  }
  { // Node ID: 649 (NodeConstantRawBits)
  }
  { // Node ID: 648 (NodeConstantRawBits)
  }
  { // Node ID: 647 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  HWFloat<11,53> id167out_result;

  { // Node ID: 167 (NodeSub)
    const HWFloat<11,53> &id167in_a = id603out_output[getCycle()%18];
    const HWFloat<11,53> &id167in_b = id30out_value;

    id167out_result = (sub_float(id167in_a,id167in_b));
  }
  HWFloat<11,53> id168out_result;

  { // Node ID: 168 (NodeMul)
    const HWFloat<11,53> &id168in_a = id25out_value;
    const HWFloat<11,53> &id168in_b = id167out_result;

    id168out_result = (mul_float(id168in_a,id168in_b));
  }
  { // Node ID: 528 (NodePO2FPMult)
    const HWFloat<11,53> &id528in_floatIn = id168out_result;

    id528out_floatOut[(getCycle()+1)%2] = (mul_float(id528in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id247out_result;

  { // Node ID: 247 (NodeSlice)
    const HWFloat<11,53> &id247in_a = id528out_floatOut[getCycle()%2];

    id247out_result = (slice<52,11>(id247in_a));
  }
  { // Node ID: 248 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id492out_result;

  { // Node ID: 492 (NodeEqInlined)
    const HWRawBits<11> &id492in_a = id247out_result;
    const HWRawBits<11> &id492in_b = id248out_value;

    id492out_result = (eq_bits(id492in_a,id492in_b));
  }
  HWRawBits<52> id246out_result;

  { // Node ID: 246 (NodeSlice)
    const HWFloat<11,53> &id246in_a = id528out_floatOut[getCycle()%2];

    id246out_result = (slice<0,52>(id246in_a));
  }
  { // Node ID: 646 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id493out_result;

  { // Node ID: 493 (NodeNeqInlined)
    const HWRawBits<52> &id493in_a = id246out_result;
    const HWRawBits<52> &id493in_b = id646out_value;

    id493out_result = (neq_bits(id493in_a,id493in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id252out_result;

  { // Node ID: 252 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id252in_a = id492out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id252in_b = id493out_result;

    HWOffsetFix<1,0,UNSIGNED> id252x_1;

    (id252x_1) = (and_fixed(id252in_a,id252in_b));
    id252out_result = (id252x_1);
  }
  { // Node ID: 560 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id560in_input = id252out_result;

    id560out_output[(getCycle()+8)%9] = id560in_input;
  }
  { // Node ID: 171 (NodeConstantRawBits)
  }
  HWFloat<11,53> id172out_output;
  HWOffsetFix<1,0,UNSIGNED> id172out_output_doubt;

  { // Node ID: 172 (NodeDoubtBitOp)
    const HWFloat<11,53> &id172in_input = id528out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id172in_doubt = id171out_value;

    id172out_output = id172in_input;
    id172out_output_doubt = id172in_doubt;
  }
  { // Node ID: 173 (NodeCast)
    const HWFloat<11,53> &id173in_i = id172out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id173in_i_doubt = id172out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id173x_1;

    id173out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id173in_i,(&(id173x_1))));
    id173out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id173x_1),(c_hw_fix_4_0_uns_bits))),id173in_i_doubt));
  }
  { // Node ID: 176 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id175out_result;
  HWOffsetFix<1,0,UNSIGNED> id175out_result_doubt;

  { // Node ID: 175 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id175in_a = id173out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id175in_a_doubt = id173out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id175in_b = id176out_value;

    HWOffsetFix<1,0,UNSIGNED> id175x_1;

    id175out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id175in_a,id175in_b,(&(id175x_1))));
    id175out_result_doubt = (or_fixed((neq_fixed((id175x_1),(c_hw_fix_1_0_uns_bits_1))),id175in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id177out_o;
  HWOffsetFix<1,0,UNSIGNED> id177out_o_doubt;

  { // Node ID: 177 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id177in_i = id175out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id177in_i_doubt = id175out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id177x_1;

    id177out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id177in_i,(&(id177x_1))));
    id177out_o_doubt = (or_fixed((neq_fixed((id177x_1),(c_hw_fix_1_0_uns_bits_1))),id177in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id186out_output;

  { // Node ID: 186 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id186in_input = id177out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id186in_input_doubt = id177out_o_doubt;

    id186out_output = id186in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id187out_o;

  { // Node ID: 187 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id187in_i = id186out_output;

    id187out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id187in_i));
  }
  { // Node ID: 551 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id551in_input = id187out_o;

    id551out_output[(getCycle()+2)%3] = id551in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id214out_o;

  { // Node ID: 214 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id214in_i = id551out_output[getCycle()%3];

    id214out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id214in_i));
  }
  { // Node ID: 217 (NodeConstantRawBits)
  }
  { // Node ID: 519 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id190out_o;

  { // Node ID: 190 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id190in_i = id186out_output;

    id190out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id190in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id262out_output;

  { // Node ID: 262 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id262in_input = id190out_o;

    id262out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id262in_input))));
  }
  { // Node ID: 263 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id263in_addr = id262out_output;

    HWOffsetFix<38,-53,UNSIGNED> id263x_1;

    switch(((long)((id263in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id263x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id263x_1 = (id263sta_rom_store[(id263in_addr.getValueAsLong())]);
        break;
      default:
        id263x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id263out_dout[(getCycle()+2)%3] = (id263x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id189out_o;

  { // Node ID: 189 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id189in_i = id186out_output;

    id189out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id189in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id259out_output;

  { // Node ID: 259 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id259in_input = id189out_o;

    id259out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id259in_input))));
  }
  { // Node ID: 260 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id260in_addr = id259out_output;

    HWOffsetFix<48,-53,UNSIGNED> id260x_1;

    switch(((long)((id260in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id260x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id260x_1 = (id260sta_rom_store[(id260in_addr.getValueAsLong())]);
        break;
      default:
        id260x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id260out_dout[(getCycle()+2)%3] = (id260x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id188out_o;

  { // Node ID: 188 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id188in_i = id186out_output;

    id188out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id188in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id256out_output;

  { // Node ID: 256 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id256in_input = id188out_o;

    id256out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id256in_input))));
  }
  { // Node ID: 257 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id257in_addr = id256out_output;

    HWOffsetFix<53,-53,UNSIGNED> id257x_1;

    switch(((long)((id257in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id257x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id257x_1 = (id257sta_rom_store[(id257in_addr.getValueAsLong())]);
        break;
      default:
        id257x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id257out_dout[(getCycle()+2)%3] = (id257x_1);
  }
  { // Node ID: 194 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id191out_o;

  { // Node ID: 191 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id191in_i = id186out_output;

    id191out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id191in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id193out_result;

  { // Node ID: 193 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id193in_a = id194out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id193in_b = id191out_o;

    id193out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id193in_a,id193in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id195out_o;

  { // Node ID: 195 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id195in_i = id193out_result;

    id195out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id195in_i));
  }
  { // Node ID: 552 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id552in_input = id195out_o;

    id552out_output[(getCycle()+2)%3] = id552in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id196out_result;

  { // Node ID: 196 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id196in_a = id257out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id196in_b = id552out_output[getCycle()%3];

    id196out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id196in_a,id196in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id197out_result;

  { // Node ID: 197 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id197in_a = id552out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id197in_b = id257out_dout[getCycle()%3];

    id197out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id197in_a,id197in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id198out_result;

  { // Node ID: 198 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id198in_a = id196out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id198in_b = id197out_result;

    id198out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id198in_a,id198in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id199out_o;

  { // Node ID: 199 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id199in_i = id198out_result;

    id199out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id199in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id200out_result;

  { // Node ID: 200 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id200in_a = id260out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id200in_b = id199out_o;

    id200out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id200in_a,id200in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id201out_result;

  { // Node ID: 201 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id201in_a = id199out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id201in_b = id260out_dout[getCycle()%3];

    id201out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id201in_a,id201in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id202out_result;

  { // Node ID: 202 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id202in_a = id200out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id202in_b = id201out_result;

    id202out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id202in_a,id202in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id203out_o;

  { // Node ID: 203 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id203in_i = id202out_result;

    id203out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id203in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id204out_result;

  { // Node ID: 204 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id204in_a = id263out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id204in_b = id203out_o;

    id204out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id204in_a,id204in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id205out_result;

  { // Node ID: 205 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id205in_a = id203out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id205in_b = id263out_dout[getCycle()%3];

    id205out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id205in_a,id205in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id206out_result;

  { // Node ID: 206 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id206in_a = id204out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id206in_b = id205out_result;

    id206out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id206in_a,id206in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id207out_o;

  { // Node ID: 207 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id207in_i = id206out_result;

    id207out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id207in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id208out_o;

  { // Node ID: 208 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id208in_i = id207out_o;

    id208out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id208in_i));
  }
  { // Node ID: 645 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id494out_result;

  { // Node ID: 494 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id494in_a = id208out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id494in_b = id645out_value;

    id494out_result = (gte_fixed(id494in_a,id494in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id527out_result;

  { // Node ID: 527 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id527in_a = id214out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id527in_b = id217out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id527in_c = id519out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id527in_condb = id494out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id527x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id527x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id527x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id527x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id527x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id527x_1 = id527in_a;
        break;
      default:
        id527x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id527in_condb.getValueAsLong())) {
      case 0l:
        id527x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id527x_2 = id527in_b;
        break;
      default:
        id527x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id527x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id527x_3 = id527in_c;
        break;
      default:
        id527x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id527x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id527x_1),(id527x_2))),(id527x_3)));
    id527out_result = (id527x_4);
  }
  HWRawBits<1> id495out_result;

  { // Node ID: 495 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id495in_a = id527out_result;

    id495out_result = (slice<13,1>(id495in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id496out_output;

  { // Node ID: 496 (NodeReinterpret)
    const HWRawBits<1> &id496in_input = id495out_result;

    id496out_output = (cast_bits2fixed<1,0,UNSIGNED>(id496in_input));
  }
  { // Node ID: 644 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id179out_result;

  { // Node ID: 179 (NodeGt)
    const HWFloat<11,53> &id179in_a = id528out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id179in_b = id644out_value;

    id179out_result = (gt_float(id179in_a,id179in_b));
  }
  { // Node ID: 554 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id554in_input = id179out_result;

    id554out_output[(getCycle()+6)%7] = id554in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id180out_output;

  { // Node ID: 180 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id180in_input = id177out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id180in_input_doubt = id177out_o_doubt;

    id180out_output = id180in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id181out_result;

  { // Node ID: 181 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id181in_a = id554out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id181in_b = id180out_output;

    HWOffsetFix<1,0,UNSIGNED> id181x_1;

    (id181x_1) = (and_fixed(id181in_a,id181in_b));
    id181out_result = (id181x_1);
  }
  { // Node ID: 555 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id555in_input = id181out_result;

    id555out_output[(getCycle()+2)%3] = id555in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id223out_result;

  { // Node ID: 223 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id223in_a = id555out_output[getCycle()%3];

    id223out_result = (not_fixed(id223in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id224out_result;

  { // Node ID: 224 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id224in_a = id496out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id224in_b = id223out_result;

    HWOffsetFix<1,0,UNSIGNED> id224x_1;

    (id224x_1) = (and_fixed(id224in_a,id224in_b));
    id224out_result = (id224x_1);
  }
  { // Node ID: 643 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id183out_result;

  { // Node ID: 183 (NodeLt)
    const HWFloat<11,53> &id183in_a = id528out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id183in_b = id643out_value;

    id183out_result = (lt_float(id183in_a,id183in_b));
  }
  { // Node ID: 556 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id556in_input = id183out_result;

    id556out_output[(getCycle()+6)%7] = id556in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id184out_output;

  { // Node ID: 184 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id184in_input = id177out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id184in_input_doubt = id177out_o_doubt;

    id184out_output = id184in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id185out_result;

  { // Node ID: 185 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id185in_a = id556out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id185in_b = id184out_output;

    HWOffsetFix<1,0,UNSIGNED> id185x_1;

    (id185x_1) = (and_fixed(id185in_a,id185in_b));
    id185out_result = (id185x_1);
  }
  { // Node ID: 557 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id557in_input = id185out_result;

    id557out_output[(getCycle()+2)%3] = id557in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id225out_result;

  { // Node ID: 225 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id225in_a = id224out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id225in_b = id557out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id225x_1;

    (id225x_1) = (or_fixed(id225in_a,id225in_b));
    id225out_result = (id225x_1);
  }
  { // Node ID: 642 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id497out_result;

  { // Node ID: 497 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id497in_a = id527out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id497in_b = id642out_value;

    id497out_result = (gte_fixed(id497in_a,id497in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id232out_result;

  { // Node ID: 232 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id232in_a = id557out_output[getCycle()%3];

    id232out_result = (not_fixed(id232in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id233out_result;

  { // Node ID: 233 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id233in_a = id497out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id233in_b = id232out_result;

    HWOffsetFix<1,0,UNSIGNED> id233x_1;

    (id233x_1) = (and_fixed(id233in_a,id233in_b));
    id233out_result = (id233x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id234out_result;

  { // Node ID: 234 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id234in_a = id233out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id234in_b = id555out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id234x_1;

    (id234x_1) = (or_fixed(id234in_a,id234in_b));
    id234out_result = (id234x_1);
  }
  HWRawBits<2> id235out_result;

  { // Node ID: 235 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id235in_in0 = id225out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id235in_in1 = id234out_result;

    id235out_result = (cat(id235in_in0,id235in_in1));
  }
  { // Node ID: 227 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id226out_o;

  { // Node ID: 226 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id226in_i = id527out_result;

    id226out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id226in_i));
  }
  { // Node ID: 211 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id212out_result;

  { // Node ID: 212 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id212in_sel = id494out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id212in_option0 = id208out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id212in_option1 = id211out_value;

    HWOffsetFix<53,-52,UNSIGNED> id212x_1;

    switch((id212in_sel.getValueAsLong())) {
      case 0l:
        id212x_1 = id212in_option0;
        break;
      case 1l:
        id212x_1 = id212in_option1;
        break;
      default:
        id212x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id212out_result = (id212x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id213out_o;

  { // Node ID: 213 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id213in_i = id212out_result;

    id213out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id213in_i));
  }
  HWRawBits<64> id228out_result;

  { // Node ID: 228 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id228in_in0 = id227out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id228in_in1 = id226out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id228in_in2 = id213out_o;

    id228out_result = (cat((cat(id228in_in0,id228in_in1)),id228in_in2));
  }
  HWFloat<11,53> id229out_output;

  { // Node ID: 229 (NodeReinterpret)
    const HWRawBits<64> &id229in_input = id228out_result;

    id229out_output = (cast_bits2float<11,53>(id229in_input));
  }
  { // Node ID: 236 (NodeConstantRawBits)
  }
  { // Node ID: 237 (NodeConstantRawBits)
  }
  { // Node ID: 239 (NodeConstantRawBits)
  }
  HWRawBits<64> id498out_result;

  { // Node ID: 498 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id498in_in0 = id236out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id498in_in1 = id237out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id498in_in2 = id239out_value;

    id498out_result = (cat((cat(id498in_in0,id498in_in1)),id498in_in2));
  }
  HWFloat<11,53> id241out_output;

  { // Node ID: 241 (NodeReinterpret)
    const HWRawBits<64> &id241in_input = id498out_result;

    id241out_output = (cast_bits2float<11,53>(id241in_input));
  }
  { // Node ID: 486 (NodeConstantRawBits)
  }
  HWFloat<11,53> id244out_result;

  { // Node ID: 244 (NodeMux)
    const HWRawBits<2> &id244in_sel = id235out_result;
    const HWFloat<11,53> &id244in_option0 = id229out_output;
    const HWFloat<11,53> &id244in_option1 = id241out_output;
    const HWFloat<11,53> &id244in_option2 = id486out_value;
    const HWFloat<11,53> &id244in_option3 = id241out_output;

    HWFloat<11,53> id244x_1;

    switch((id244in_sel.getValueAsLong())) {
      case 0l:
        id244x_1 = id244in_option0;
        break;
      case 1l:
        id244x_1 = id244in_option1;
        break;
      case 2l:
        id244x_1 = id244in_option2;
        break;
      case 3l:
        id244x_1 = id244in_option3;
        break;
      default:
        id244x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id244out_result = (id244x_1);
  }
  { // Node ID: 641 (NodeConstantRawBits)
  }
  HWFloat<11,53> id254out_result;

  { // Node ID: 254 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id254in_sel = id560out_output[getCycle()%9];
    const HWFloat<11,53> &id254in_option0 = id244out_result;
    const HWFloat<11,53> &id254in_option1 = id641out_value;

    HWFloat<11,53> id254x_1;

    switch((id254in_sel.getValueAsLong())) {
      case 0l:
        id254x_1 = id254in_option0;
        break;
      case 1l:
        id254x_1 = id254in_option1;
        break;
      default:
        id254x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id254out_result = (id254x_1);
  }
  { // Node ID: 640 (NodeConstantRawBits)
  }
  HWFloat<11,53> id265out_result;

  { // Node ID: 265 (NodeAdd)
    const HWFloat<11,53> &id265in_a = id254out_result;
    const HWFloat<11,53> &id265in_b = id640out_value;

    id265out_result = (add_float(id265in_a,id265in_b));
  }
  HWFloat<11,53> id267out_result;

  { // Node ID: 267 (NodeDiv)
    const HWFloat<11,53> &id267in_a = id647out_value;
    const HWFloat<11,53> &id267in_b = id265out_result;

    id267out_result = (div_float(id267in_a,id267in_b));
  }
  HWFloat<11,53> id269out_result;

  { // Node ID: 269 (NodeSub)
    const HWFloat<11,53> &id269in_a = id648out_value;
    const HWFloat<11,53> &id269in_b = id267out_result;

    id269out_result = (sub_float(id269in_a,id269in_b));
  }
  HWFloat<11,53> id271out_result;

  { // Node ID: 271 (NodeAdd)
    const HWFloat<11,53> &id271in_a = id649out_value;
    const HWFloat<11,53> &id271in_b = id269out_result;

    id271out_result = (add_float(id271in_a,id271in_b));
  }
  HWFloat<11,53> id272out_result;

  { // Node ID: 272 (NodeMul)
    const HWFloat<11,53> &id272in_a = id36out_value;
    const HWFloat<11,53> &id272in_b = id271out_result;

    id272out_result = (mul_float(id272in_a,id272in_b));
  }
  HWFloat<11,53> id273out_result;

  { // Node ID: 273 (NodeAdd)
    const HWFloat<11,53> &id273in_a = id11out_value;
    const HWFloat<11,53> &id273in_b = id272out_result;

    id273out_result = (add_float(id273in_a,id273in_b));
  }
  { // Node ID: 424 (NodeDiv)
    const HWFloat<11,53> &id424in_a = id650out_value;
    const HWFloat<11,53> &id424in_b = id273out_result;

    id424out_result[(getCycle()+57)%58] = (div_float(id424in_a,id424in_b));
  }
  { // Node ID: 427 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id427in_sel = id422out_result[getCycle()%3];
    const HWFloat<11,53> &id427in_option0 = id426out_result[getCycle()%58];
    const HWFloat<11,53> &id427in_option1 = id424out_result[getCycle()%58];

    HWFloat<11,53> id427x_1;

    switch((id427in_sel.getValueAsLong())) {
      case 0l:
        id427x_1 = id427in_option0;
        break;
      case 1l:
        id427x_1 = id427in_option1;
        break;
      default:
        id427x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id427out_result[(getCycle()+1)%2] = (id427x_1);
  }
  { // Node ID: 434 (NodeAdd)
    const HWFloat<11,53> &id434in_a = id421out_result[getCycle()%2];
    const HWFloat<11,53> &id434in_b = id427out_result[getCycle()%2];

    id434out_result[(getCycle()+14)%15] = (add_float(id434in_a,id434in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  { // Node ID: 428 (NodeGt)
    const HWFloat<11,53> &id428in_a = id606out_output[getCycle()%15];
    const HWFloat<11,53> &id428in_b = id21out_value;

    id428out_result[(getCycle()+2)%3] = (gt_float(id428in_a,id428in_b));
  }
  { // Node ID: 432 (NodeConstantRawBits)
  }
  { // Node ID: 639 (NodeConstantRawBits)
  }
  { // Node ID: 499 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id499in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id499in_b = id639out_value;

    id499out_result[(getCycle()+1)%2] = (eq_fixed(id499in_a,id499in_b));
  }
  { // Node ID: 609 (NodeFIFO)
    const HWFloat<11,53> &id609in_input = id579out_output[getCycle()%18];

    id609out_output[(getCycle()+110)%111] = id609in_input;
  }
  HWFloat<11,53> id539out_output;

  { // Node ID: 539 (NodeStreamOffset)
    const HWFloat<11,53> &id539in_input = id609out_output[getCycle()%111];

    id539out_output = id539in_input;
  }
  { // Node ID: 54 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id54in_sel = id499out_result[getCycle()%2];
    const HWFloat<11,53> &id54in_option0 = id539out_output;
    const HWFloat<11,53> &id54in_option1 = id38out_value;

    HWFloat<11,53> id54x_1;

    switch((id54in_sel.getValueAsLong())) {
      case 0l:
        id54x_1 = id54in_option0;
        break;
      case 1l:
        id54x_1 = id54in_option1;
        break;
      default:
        id54x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id54out_result[(getCycle()+1)%2] = (id54x_1);
  }
  { // Node ID: 566 (NodeFIFO)
    const HWFloat<11,53> &id566in_input = id54out_result[getCycle()%2];

    id566out_output[(getCycle()+9)%10] = id566in_input;
  }
  { // Node ID: 608 (NodeFIFO)
    const HWFloat<11,53> &id608in_input = id566out_output[getCycle()%10];

    id608out_output[(getCycle()+12)%13] = id608in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id293out_result;

  { // Node ID: 293 (NodeGt)
    const HWFloat<11,53> &id293in_a = id563out_output[getCycle()%10];
    const HWFloat<11,53> &id293in_b = id18out_value;

    id293out_result = (gt_float(id293in_a,id293in_b));
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id280out_result;

  { // Node ID: 280 (NodeGt)
    const HWFloat<11,53> &id280in_a = id563out_output[getCycle()%10];
    const HWFloat<11,53> &id280in_b = id19out_value;

    id280out_result = (gt_float(id280in_a,id280in_b));
  }
  { // Node ID: 638 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  HWFloat<11,53> id281out_result;

  { // Node ID: 281 (NodeDiv)
    const HWFloat<11,53> &id281in_a = id563out_output[getCycle()%10];
    const HWFloat<11,53> &id281in_b = id14out_value;

    id281out_result = (div_float(id281in_a,id281in_b));
  }
  HWFloat<11,53> id283out_result;

  { // Node ID: 283 (NodeSub)
    const HWFloat<11,53> &id283in_a = id638out_value;
    const HWFloat<11,53> &id283in_b = id281out_result;

    id283out_result = (sub_float(id283in_a,id283in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  HWFloat<11,53> id284out_result;

  { // Node ID: 284 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id284in_sel = id280out_result;
    const HWFloat<11,53> &id284in_option0 = id283out_result;
    const HWFloat<11,53> &id284in_option1 = id32out_value;

    HWFloat<11,53> id284x_1;

    switch((id284in_sel.getValueAsLong())) {
      case 0l:
        id284x_1 = id284in_option0;
        break;
      case 1l:
        id284x_1 = id284in_option1;
        break;
      default:
        id284x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id284out_result = (id284x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeGt)
    const HWFloat<11,53> &id285in_a = id284out_result;
    const HWFloat<11,53> &id285in_b = id38out_value;

    id285out_result = (gt_float(id285in_a,id285in_b));
  }
  HWFloat<11,53> id286out_result;

  { // Node ID: 286 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id286in_sel = id285out_result;
    const HWFloat<11,53> &id286in_option0 = id284out_result;
    const HWFloat<11,53> &id286in_option1 = id38out_value;

    HWFloat<11,53> id286x_1;

    switch((id286in_sel.getValueAsLong())) {
      case 0l:
        id286x_1 = id286in_option0;
        break;
      case 1l:
        id286x_1 = id286in_option1;
        break;
      default:
        id286x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id286out_result = (id286x_1);
  }
  HWFloat<11,53> id296out_result;

  { // Node ID: 296 (NodeSub)
    const HWFloat<11,53> &id296in_a = id286out_result;
    const HWFloat<11,53> &id296in_b = id566out_output[getCycle()%10];

    id296out_result = (sub_float(id296in_a,id296in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  { // Node ID: 637 (NodeConstantRawBits)
  }
  { // Node ID: 636 (NodeConstantRawBits)
  }
  { // Node ID: 635 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  HWFloat<11,53> id60out_result;

  { // Node ID: 60 (NodeSub)
    const HWFloat<11,53> &id60in_a = id48out_result[getCycle()%2];
    const HWFloat<11,53> &id60in_b = id26out_value;

    id60out_result = (sub_float(id60in_a,id60in_b));
  }
  HWFloat<11,53> id61out_result;

  { // Node ID: 61 (NodeMul)
    const HWFloat<11,53> &id61in_a = id23out_value;
    const HWFloat<11,53> &id61in_b = id60out_result;

    id61out_result = (mul_float(id61in_a,id61in_b));
  }
  { // Node ID: 529 (NodePO2FPMult)
    const HWFloat<11,53> &id529in_floatIn = id61out_result;

    id529out_floatOut[(getCycle()+1)%2] = (mul_float(id529in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id140out_result;

  { // Node ID: 140 (NodeSlice)
    const HWFloat<11,53> &id140in_a = id529out_floatOut[getCycle()%2];

    id140out_result = (slice<52,11>(id140in_a));
  }
  { // Node ID: 141 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id500out_result;

  { // Node ID: 500 (NodeEqInlined)
    const HWRawBits<11> &id500in_a = id140out_result;
    const HWRawBits<11> &id500in_b = id141out_value;

    id500out_result = (eq_bits(id500in_a,id500in_b));
  }
  HWRawBits<52> id139out_result;

  { // Node ID: 139 (NodeSlice)
    const HWFloat<11,53> &id139in_a = id529out_floatOut[getCycle()%2];

    id139out_result = (slice<0,52>(id139in_a));
  }
  { // Node ID: 634 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id501out_result;

  { // Node ID: 501 (NodeNeqInlined)
    const HWRawBits<52> &id501in_a = id139out_result;
    const HWRawBits<52> &id501in_b = id634out_value;

    id501out_result = (neq_bits(id501in_a,id501in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id145out_result;

  { // Node ID: 145 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id145in_a = id500out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id145in_b = id501out_result;

    HWOffsetFix<1,0,UNSIGNED> id145x_1;

    (id145x_1) = (and_fixed(id145in_a,id145in_b));
    id145out_result = (id145x_1);
  }
  { // Node ID: 576 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id576in_input = id145out_result;

    id576out_output[(getCycle()+8)%9] = id576in_input;
  }
  { // Node ID: 64 (NodeConstantRawBits)
  }
  HWFloat<11,53> id65out_output;
  HWOffsetFix<1,0,UNSIGNED> id65out_output_doubt;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWFloat<11,53> &id65in_input = id529out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id65in_doubt = id64out_value;

    id65out_output = id65in_input;
    id65out_output_doubt = id65in_doubt;
  }
  { // Node ID: 66 (NodeCast)
    const HWFloat<11,53> &id66in_i = id65out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id66in_i_doubt = id65out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id66x_1;

    id66out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id66in_i,(&(id66x_1))));
    id66out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id66x_1),(c_hw_fix_4_0_uns_bits))),id66in_i_doubt));
  }
  { // Node ID: 69 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id68out_result;
  HWOffsetFix<1,0,UNSIGNED> id68out_result_doubt;

  { // Node ID: 68 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id68in_a = id66out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id68in_a_doubt = id66out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id68in_b = id69out_value;

    HWOffsetFix<1,0,UNSIGNED> id68x_1;

    id68out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id68in_a,id68in_b,(&(id68x_1))));
    id68out_result_doubt = (or_fixed((neq_fixed((id68x_1),(c_hw_fix_1_0_uns_bits_1))),id68in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id70out_o;
  HWOffsetFix<1,0,UNSIGNED> id70out_o_doubt;

  { // Node ID: 70 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id70in_i = id68out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id70in_i_doubt = id68out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id70x_1;

    id70out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id70in_i,(&(id70x_1))));
    id70out_o_doubt = (or_fixed((neq_fixed((id70x_1),(c_hw_fix_1_0_uns_bits_1))),id70in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id79out_output;

  { // Node ID: 79 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id79in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_input_doubt = id70out_o_doubt;

    id79out_output = id79in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id80out_o;

  { // Node ID: 80 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id80in_i = id79out_output;

    id80out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id80in_i));
  }
  { // Node ID: 567 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id567in_input = id80out_o;

    id567out_output[(getCycle()+2)%3] = id567in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id107out_o;

  { // Node ID: 107 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id107in_i = id567out_output[getCycle()%3];

    id107out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id107in_i));
  }
  { // Node ID: 110 (NodeConstantRawBits)
  }
  { // Node ID: 521 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id83out_o;

  { // Node ID: 83 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id83in_i = id79out_output;

    id83out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id83in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id155out_output;

  { // Node ID: 155 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id155in_input = id83out_o;

    id155out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id155in_input))));
  }
  { // Node ID: 156 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id156in_addr = id155out_output;

    HWOffsetFix<38,-53,UNSIGNED> id156x_1;

    switch(((long)((id156in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id156x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id156x_1 = (id156sta_rom_store[(id156in_addr.getValueAsLong())]);
        break;
      default:
        id156x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id156out_dout[(getCycle()+2)%3] = (id156x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id82in_i = id79out_output;

    id82out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id82in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id152out_output;

  { // Node ID: 152 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id152in_input = id82out_o;

    id152out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id152in_input))));
  }
  { // Node ID: 153 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id153in_addr = id152out_output;

    HWOffsetFix<48,-53,UNSIGNED> id153x_1;

    switch(((long)((id153in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id153x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id153x_1 = (id153sta_rom_store[(id153in_addr.getValueAsLong())]);
        break;
      default:
        id153x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id153out_dout[(getCycle()+2)%3] = (id153x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id81out_o;

  { // Node ID: 81 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id81in_i = id79out_output;

    id81out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id81in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id149out_output;

  { // Node ID: 149 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id149in_input = id81out_o;

    id149out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id149in_input))));
  }
  { // Node ID: 150 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id150in_addr = id149out_output;

    HWOffsetFix<53,-53,UNSIGNED> id150x_1;

    switch(((long)((id150in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id150x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id150x_1 = (id150sta_rom_store[(id150in_addr.getValueAsLong())]);
        break;
      default:
        id150x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id150out_dout[(getCycle()+2)%3] = (id150x_1);
  }
  { // Node ID: 87 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id84out_o;

  { // Node ID: 84 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id84in_i = id79out_output;

    id84out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id84in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id86out_result;

  { // Node ID: 86 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id86in_a = id87out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id86in_b = id84out_o;

    id86out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id86in_a,id86in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id88out_o;

  { // Node ID: 88 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id88in_i = id86out_result;

    id88out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id88in_i));
  }
  { // Node ID: 568 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id568in_input = id88out_o;

    id568out_output[(getCycle()+2)%3] = id568in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id89out_result;

  { // Node ID: 89 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id89in_a = id150out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id89in_b = id568out_output[getCycle()%3];

    id89out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id89in_a,id89in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id90out_result;

  { // Node ID: 90 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id90in_a = id568out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id90in_b = id150out_dout[getCycle()%3];

    id90out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id90in_a,id90in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id91out_result;

  { // Node ID: 91 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id91in_a = id89out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id91in_b = id90out_result;

    id91out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id91in_a,id91in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id92out_o;

  { // Node ID: 92 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id92in_i = id91out_result;

    id92out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id92in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id93out_result;

  { // Node ID: 93 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id93in_a = id153out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id93in_b = id92out_o;

    id93out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id93in_a,id93in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id94out_result;

  { // Node ID: 94 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id94in_a = id92out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id94in_b = id153out_dout[getCycle()%3];

    id94out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id94in_a,id94in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id95out_result;

  { // Node ID: 95 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id95in_a = id93out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id95in_b = id94out_result;

    id95out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id95in_a,id95in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id96out_o;

  { // Node ID: 96 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id96in_i = id95out_result;

    id96out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id96in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id97out_result;

  { // Node ID: 97 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id97in_a = id156out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id97in_b = id96out_o;

    id97out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id97in_a,id97in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id98out_result;

  { // Node ID: 98 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id98in_a = id96out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id98in_b = id156out_dout[getCycle()%3];

    id98out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id98in_a,id98in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id99out_result;

  { // Node ID: 99 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id99in_a = id97out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id99in_b = id98out_result;

    id99out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id99in_a,id99in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id100out_o;

  { // Node ID: 100 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id100in_i = id99out_result;

    id100out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id100in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id101out_o;

  { // Node ID: 101 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id101in_i = id100out_o;

    id101out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id101in_i));
  }
  { // Node ID: 633 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id502out_result;

  { // Node ID: 502 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id502in_a = id101out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id502in_b = id633out_value;

    id502out_result = (gte_fixed(id502in_a,id502in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id526out_result;

  { // Node ID: 526 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id526in_a = id107out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id526in_b = id110out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id526in_c = id521out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id526in_condb = id502out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id526x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id526x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id526x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id526x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id526x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id526x_1 = id526in_a;
        break;
      default:
        id526x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id526in_condb.getValueAsLong())) {
      case 0l:
        id526x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id526x_2 = id526in_b;
        break;
      default:
        id526x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id526x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id526x_3 = id526in_c;
        break;
      default:
        id526x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id526x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id526x_1),(id526x_2))),(id526x_3)));
    id526out_result = (id526x_4);
  }
  HWRawBits<1> id503out_result;

  { // Node ID: 503 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id503in_a = id526out_result;

    id503out_result = (slice<13,1>(id503in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id504out_output;

  { // Node ID: 504 (NodeReinterpret)
    const HWRawBits<1> &id504in_input = id503out_result;

    id504out_output = (cast_bits2fixed<1,0,UNSIGNED>(id504in_input));
  }
  { // Node ID: 632 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id72out_result;

  { // Node ID: 72 (NodeGt)
    const HWFloat<11,53> &id72in_a = id529out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id72in_b = id632out_value;

    id72out_result = (gt_float(id72in_a,id72in_b));
  }
  { // Node ID: 570 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id570in_input = id72out_result;

    id570out_output[(getCycle()+6)%7] = id570in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id73out_output;

  { // Node ID: 73 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id73in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id73in_input_doubt = id70out_o_doubt;

    id73out_output = id73in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id74out_result;

  { // Node ID: 74 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id74in_a = id570out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id74in_b = id73out_output;

    HWOffsetFix<1,0,UNSIGNED> id74x_1;

    (id74x_1) = (and_fixed(id74in_a,id74in_b));
    id74out_result = (id74x_1);
  }
  { // Node ID: 571 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id571in_input = id74out_result;

    id571out_output[(getCycle()+2)%3] = id571in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id116out_result;

  { // Node ID: 116 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id116in_a = id571out_output[getCycle()%3];

    id116out_result = (not_fixed(id116in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id117out_result;

  { // Node ID: 117 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id117in_a = id504out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id117in_b = id116out_result;

    HWOffsetFix<1,0,UNSIGNED> id117x_1;

    (id117x_1) = (and_fixed(id117in_a,id117in_b));
    id117out_result = (id117x_1);
  }
  { // Node ID: 631 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeLt)
    const HWFloat<11,53> &id76in_a = id529out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id76in_b = id631out_value;

    id76out_result = (lt_float(id76in_a,id76in_b));
  }
  { // Node ID: 572 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id572in_input = id76out_result;

    id572out_output[(getCycle()+6)%7] = id572in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id77out_output;

  { // Node ID: 77 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id77in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id77in_input_doubt = id70out_o_doubt;

    id77out_output = id77in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id78out_result;

  { // Node ID: 78 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id78in_a = id572out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id78in_b = id77out_output;

    HWOffsetFix<1,0,UNSIGNED> id78x_1;

    (id78x_1) = (and_fixed(id78in_a,id78in_b));
    id78out_result = (id78x_1);
  }
  { // Node ID: 573 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id573in_input = id78out_result;

    id573out_output[(getCycle()+2)%3] = id573in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id118out_result;

  { // Node ID: 118 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id118in_a = id117out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id118in_b = id573out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id118x_1;

    (id118x_1) = (or_fixed(id118in_a,id118in_b));
    id118out_result = (id118x_1);
  }
  { // Node ID: 630 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id505out_result;

  { // Node ID: 505 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id505in_a = id526out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id505in_b = id630out_value;

    id505out_result = (gte_fixed(id505in_a,id505in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id125out_result;

  { // Node ID: 125 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id125in_a = id573out_output[getCycle()%3];

    id125out_result = (not_fixed(id125in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id126out_result;

  { // Node ID: 126 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id126in_a = id505out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id126in_b = id125out_result;

    HWOffsetFix<1,0,UNSIGNED> id126x_1;

    (id126x_1) = (and_fixed(id126in_a,id126in_b));
    id126out_result = (id126x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id127out_result;

  { // Node ID: 127 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id127in_a = id126out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id127in_b = id571out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id127x_1;

    (id127x_1) = (or_fixed(id127in_a,id127in_b));
    id127out_result = (id127x_1);
  }
  HWRawBits<2> id128out_result;

  { // Node ID: 128 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id128in_in0 = id118out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id128in_in1 = id127out_result;

    id128out_result = (cat(id128in_in0,id128in_in1));
  }
  { // Node ID: 120 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id119out_o;

  { // Node ID: 119 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id119in_i = id526out_result;

    id119out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id119in_i));
  }
  { // Node ID: 104 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id105out_result;

  { // Node ID: 105 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id105in_sel = id502out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id105in_option0 = id101out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id105in_option1 = id104out_value;

    HWOffsetFix<53,-52,UNSIGNED> id105x_1;

    switch((id105in_sel.getValueAsLong())) {
      case 0l:
        id105x_1 = id105in_option0;
        break;
      case 1l:
        id105x_1 = id105in_option1;
        break;
      default:
        id105x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id105out_result = (id105x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id106out_o;

  { // Node ID: 106 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id106in_i = id105out_result;

    id106out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id106in_i));
  }
  HWRawBits<64> id121out_result;

  { // Node ID: 121 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id121in_in0 = id120out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id121in_in1 = id119out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id121in_in2 = id106out_o;

    id121out_result = (cat((cat(id121in_in0,id121in_in1)),id121in_in2));
  }
  HWFloat<11,53> id122out_output;

  { // Node ID: 122 (NodeReinterpret)
    const HWRawBits<64> &id122in_input = id121out_result;

    id122out_output = (cast_bits2float<11,53>(id122in_input));
  }
  { // Node ID: 129 (NodeConstantRawBits)
  }
  { // Node ID: 130 (NodeConstantRawBits)
  }
  { // Node ID: 132 (NodeConstantRawBits)
  }
  HWRawBits<64> id506out_result;

  { // Node ID: 506 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id506in_in0 = id129out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id506in_in1 = id130out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id506in_in2 = id132out_value;

    id506out_result = (cat((cat(id506in_in0,id506in_in1)),id506in_in2));
  }
  HWFloat<11,53> id134out_output;

  { // Node ID: 134 (NodeReinterpret)
    const HWRawBits<64> &id134in_input = id506out_result;

    id134out_output = (cast_bits2float<11,53>(id134in_input));
  }
  { // Node ID: 487 (NodeConstantRawBits)
  }
  HWFloat<11,53> id137out_result;

  { // Node ID: 137 (NodeMux)
    const HWRawBits<2> &id137in_sel = id128out_result;
    const HWFloat<11,53> &id137in_option0 = id122out_output;
    const HWFloat<11,53> &id137in_option1 = id134out_output;
    const HWFloat<11,53> &id137in_option2 = id487out_value;
    const HWFloat<11,53> &id137in_option3 = id134out_output;

    HWFloat<11,53> id137x_1;

    switch((id137in_sel.getValueAsLong())) {
      case 0l:
        id137x_1 = id137in_option0;
        break;
      case 1l:
        id137x_1 = id137in_option1;
        break;
      case 2l:
        id137x_1 = id137in_option2;
        break;
      case 3l:
        id137x_1 = id137in_option3;
        break;
      default:
        id137x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id137out_result = (id137x_1);
  }
  { // Node ID: 629 (NodeConstantRawBits)
  }
  HWFloat<11,53> id147out_result;

  { // Node ID: 147 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id147in_sel = id576out_output[getCycle()%9];
    const HWFloat<11,53> &id147in_option0 = id137out_result;
    const HWFloat<11,53> &id147in_option1 = id629out_value;

    HWFloat<11,53> id147x_1;

    switch((id147in_sel.getValueAsLong())) {
      case 0l:
        id147x_1 = id147in_option0;
        break;
      case 1l:
        id147x_1 = id147in_option1;
        break;
      default:
        id147x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id147out_result = (id147x_1);
  }
  { // Node ID: 628 (NodeConstantRawBits)
  }
  HWFloat<11,53> id158out_result;

  { // Node ID: 158 (NodeAdd)
    const HWFloat<11,53> &id158in_a = id147out_result;
    const HWFloat<11,53> &id158in_b = id628out_value;

    id158out_result = (add_float(id158in_a,id158in_b));
  }
  HWFloat<11,53> id160out_result;

  { // Node ID: 160 (NodeDiv)
    const HWFloat<11,53> &id160in_a = id635out_value;
    const HWFloat<11,53> &id160in_b = id158out_result;

    id160out_result = (div_float(id160in_a,id160in_b));
  }
  HWFloat<11,53> id162out_result;

  { // Node ID: 162 (NodeSub)
    const HWFloat<11,53> &id162in_a = id636out_value;
    const HWFloat<11,53> &id162in_b = id160out_result;

    id162out_result = (sub_float(id162in_a,id162in_b));
  }
  HWFloat<11,53> id164out_result;

  { // Node ID: 164 (NodeAdd)
    const HWFloat<11,53> &id164in_a = id637out_value;
    const HWFloat<11,53> &id164in_b = id162out_result;

    id164out_result = (add_float(id164in_a,id164in_b));
  }
  HWFloat<11,53> id165out_result;

  { // Node ID: 165 (NodeMul)
    const HWFloat<11,53> &id165in_a = id35out_value;
    const HWFloat<11,53> &id165in_b = id164out_result;

    id165out_result = (mul_float(id165in_a,id165in_b));
  }
  HWFloat<11,53> id166out_result;

  { // Node ID: 166 (NodeAdd)
    const HWFloat<11,53> &id166in_a = id4out_value;
    const HWFloat<11,53> &id166in_b = id165out_result;

    id166out_result = (add_float(id166in_a,id166in_b));
  }
  HWFloat<11,53> id297out_result;

  { // Node ID: 297 (NodeDiv)
    const HWFloat<11,53> &id297in_a = id296out_result;
    const HWFloat<11,53> &id297in_b = id166out_result;

    id297out_result = (div_float(id297in_a,id297in_b));
  }
  HWFloat<11,53> id294out_result;

  { // Node ID: 294 (NodeNeg)
    const HWFloat<11,53> &id294in_a = id566out_output[getCycle()%10];

    id294out_result = (neg_float(id294in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<11,53> id295out_result;

  { // Node ID: 295 (NodeDiv)
    const HWFloat<11,53> &id295in_a = id294out_result;
    const HWFloat<11,53> &id295in_b = id3out_value;

    id295out_result = (div_float(id295in_a,id295in_b));
  }
  HWFloat<11,53> id298out_result;

  { // Node ID: 298 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id298in_sel = id293out_result;
    const HWFloat<11,53> &id298in_option0 = id297out_result;
    const HWFloat<11,53> &id298in_option1 = id295out_result;

    HWFloat<11,53> id298x_1;

    switch((id298in_sel.getValueAsLong())) {
      case 0l:
        id298x_1 = id298in_option0;
        break;
      case 1l:
        id298x_1 = id298in_option1;
        break;
      default:
        id298x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id298out_result = (id298x_1);
  }
  { // Node ID: 410 (NodeMul)
    const HWFloat<11,53> &id410in_a = id298out_result;
    const HWFloat<11,53> &id410in_b = id44out_dt;

    id410out_result[(getCycle()+12)%13] = (mul_float(id410in_a,id410in_b));
  }
  { // Node ID: 411 (NodeAdd)
    const HWFloat<11,53> &id411in_a = id608out_output[getCycle()%13];
    const HWFloat<11,53> &id411in_b = id410out_result[getCycle()%13];

    id411out_result[(getCycle()+14)%15] = (add_float(id411in_a,id411in_b));
  }
  { // Node ID: 579 (NodeFIFO)
    const HWFloat<11,53> &id579in_input = id411out_result[getCycle()%15];

    id579out_output[(getCycle()+17)%18] = id579in_input;
  }
  HWFloat<11,53> id429out_result;

  { // Node ID: 429 (NodeNeg)
    const HWFloat<11,53> &id429in_a = id579out_output[getCycle()%18];

    id429out_result = (neg_float(id429in_a));
  }
  { // Node ID: 627 (NodeConstantRawBits)
  }
  { // Node ID: 507 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id507in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id507in_b = id627out_value;

    id507out_result[(getCycle()+1)%2] = (eq_fixed(id507in_a,id507in_b));
  }
  HWFloat<11,53> id540out_output;

  { // Node ID: 540 (NodeStreamOffset)
    const HWFloat<11,53> &id540in_input = id580out_output[getCycle()%17];

    id540out_output = id540in_input;
  }
  { // Node ID: 581 (NodeFIFO)
    const HWFloat<11,53> &id581in_input = id540out_output;

    id581out_output[(getCycle()+110)%111] = id581in_input;
  }
  { // Node ID: 57 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id57in_sel = id507out_result[getCycle()%2];
    const HWFloat<11,53> &id57in_option0 = id581out_output[getCycle()%111];
    const HWFloat<11,53> &id57in_option1 = id37out_value;

    HWFloat<11,53> id57x_1;

    switch((id57in_sel.getValueAsLong())) {
      case 0l:
        id57x_1 = id57in_option0;
        break;
      case 1l:
        id57x_1 = id57in_option1;
        break;
      default:
        id57x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id57out_result[(getCycle()+1)%2] = (id57x_1);
  }
  { // Node ID: 592 (NodeFIFO)
    const HWFloat<11,53> &id592in_input = id57out_result[getCycle()%2];

    id592out_output[(getCycle()+10)%11] = id592in_input;
  }
  { // Node ID: 610 (NodeFIFO)
    const HWFloat<11,53> &id610in_input = id592out_output[getCycle()%11];

    id610out_output[(getCycle()+12)%13] = id610in_input;
  }
  { // Node ID: 626 (NodeConstantRawBits)
  }
  { // Node ID: 625 (NodeConstantRawBits)
  }
  { // Node ID: 624 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeConstantRawBits)
  }
  HWFloat<11,53> id299out_result;

  { // Node ID: 299 (NodeSub)
    const HWFloat<11,53> &id299in_a = id48out_result[getCycle()%2];
    const HWFloat<11,53> &id299in_b = id27out_value;

    id299out_result = (sub_float(id299in_a,id299in_b));
  }
  HWFloat<11,53> id300out_result;

  { // Node ID: 300 (NodeMul)
    const HWFloat<11,53> &id300in_a = id24out_value;
    const HWFloat<11,53> &id300in_b = id299out_result;

    id300out_result = (mul_float(id300in_a,id300in_b));
  }
  { // Node ID: 530 (NodePO2FPMult)
    const HWFloat<11,53> &id530in_floatIn = id300out_result;

    id530out_floatOut[(getCycle()+1)%2] = (mul_float(id530in_floatIn,(c_hw_flt_11_53_2_0val)));
  }
  HWRawBits<11> id379out_result;

  { // Node ID: 379 (NodeSlice)
    const HWFloat<11,53> &id379in_a = id530out_floatOut[getCycle()%2];

    id379out_result = (slice<52,11>(id379in_a));
  }
  { // Node ID: 380 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id508out_result;

  { // Node ID: 508 (NodeEqInlined)
    const HWRawBits<11> &id508in_a = id379out_result;
    const HWRawBits<11> &id508in_b = id380out_value;

    id508out_result = (eq_bits(id508in_a,id508in_b));
  }
  HWRawBits<52> id378out_result;

  { // Node ID: 378 (NodeSlice)
    const HWFloat<11,53> &id378in_a = id530out_floatOut[getCycle()%2];

    id378out_result = (slice<0,52>(id378in_a));
  }
  { // Node ID: 623 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id509out_result;

  { // Node ID: 509 (NodeNeqInlined)
    const HWRawBits<52> &id509in_a = id378out_result;
    const HWRawBits<52> &id509in_b = id623out_value;

    id509out_result = (neq_bits(id509in_a,id509in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id384out_result;

  { // Node ID: 384 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id384in_a = id508out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id384in_b = id509out_result;

    HWOffsetFix<1,0,UNSIGNED> id384x_1;

    (id384x_1) = (and_fixed(id384in_a,id384in_b));
    id384out_result = (id384x_1);
  }
  { // Node ID: 591 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id591in_input = id384out_result;

    id591out_output[(getCycle()+8)%9] = id591in_input;
  }
  { // Node ID: 303 (NodeConstantRawBits)
  }
  HWFloat<11,53> id304out_output;
  HWOffsetFix<1,0,UNSIGNED> id304out_output_doubt;

  { // Node ID: 304 (NodeDoubtBitOp)
    const HWFloat<11,53> &id304in_input = id530out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id304in_doubt = id303out_value;

    id304out_output = id304in_input;
    id304out_output_doubt = id304in_doubt;
  }
  { // Node ID: 305 (NodeCast)
    const HWFloat<11,53> &id305in_i = id304out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id305in_i_doubt = id304out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id305x_1;

    id305out_o[(getCycle()+6)%7] = (cast_float2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id305in_i,(&(id305x_1))));
    id305out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id305x_1),(c_hw_fix_4_0_uns_bits))),id305in_i_doubt));
  }
  { // Node ID: 308 (NodeConstantRawBits)
  }
  HWOffsetFix<116,-102,TWOSCOMPLEMENT> id307out_result;
  HWOffsetFix<1,0,UNSIGNED> id307out_result_doubt;

  { // Node ID: 307 (NodeMul)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id307in_a = id305out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id307in_a_doubt = id305out_o_doubt[getCycle()%7];
    const HWOffsetFix<52,-51,UNSIGNED> &id307in_b = id308out_value;

    HWOffsetFix<1,0,UNSIGNED> id307x_1;

    id307out_result = (mul_fixed<116,-102,TWOSCOMPLEMENT,TONEAREVEN>(id307in_a,id307in_b,(&(id307x_1))));
    id307out_result_doubt = (or_fixed((neq_fixed((id307x_1),(c_hw_fix_1_0_uns_bits_1))),id307in_a_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id309out_o;
  HWOffsetFix<1,0,UNSIGNED> id309out_o_doubt;

  { // Node ID: 309 (NodeCast)
    const HWOffsetFix<116,-102,TWOSCOMPLEMENT> &id309in_i = id307out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id309in_i_doubt = id307out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id309x_1;

    id309out_o = (cast_fixed2fixed<64,-51,TWOSCOMPLEMENT,TONEAREVEN>(id309in_i,(&(id309x_1))));
    id309out_o_doubt = (or_fixed((neq_fixed((id309x_1),(c_hw_fix_1_0_uns_bits_1))),id309in_i_doubt));
  }
  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id318out_output;

  { // Node ID: 318 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id318in_input = id309out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id318in_input_doubt = id309out_o_doubt;

    id318out_output = id318in_input;
  }
  HWOffsetFix<13,0,TWOSCOMPLEMENT> id319out_o;

  { // Node ID: 319 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id319in_i = id318out_output;

    id319out_o = (cast_fixed2fixed<13,0,TWOSCOMPLEMENT,TRUNCATE>(id319in_i));
  }
  { // Node ID: 582 (NodeFIFO)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id582in_input = id319out_o;

    id582out_output[(getCycle()+2)%3] = id582in_input;
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id346out_o;

  { // Node ID: 346 (NodeCast)
    const HWOffsetFix<13,0,TWOSCOMPLEMENT> &id346in_i = id582out_output[getCycle()%3];

    id346out_o = (cast_fixed2fixed<14,0,TWOSCOMPLEMENT,TONEAREVEN>(id346in_i));
  }
  { // Node ID: 349 (NodeConstantRawBits)
  }
  { // Node ID: 523 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-25,UNSIGNED> id322out_o;

  { // Node ID: 322 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id322in_i = id318out_output;

    id322out_o = (cast_fixed2fixed<10,-25,UNSIGNED,TRUNCATE>(id322in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id394out_output;

  { // Node ID: 394 (NodeReinterpret)
    const HWOffsetFix<10,-25,UNSIGNED> &id394in_input = id322out_o;

    id394out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id394in_input))));
  }
  { // Node ID: 395 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id395in_addr = id394out_output;

    HWOffsetFix<38,-53,UNSIGNED> id395x_1;

    switch(((long)((id395in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id395x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
      case 1l:
        id395x_1 = (id395sta_rom_store[(id395in_addr.getValueAsLong())]);
        break;
      default:
        id395x_1 = (c_hw_fix_38_n53_uns_undef);
        break;
    }
    id395out_dout[(getCycle()+2)%3] = (id395x_1);
  }
  HWOffsetFix<10,-15,UNSIGNED> id321out_o;

  { // Node ID: 321 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id321in_i = id318out_output;

    id321out_o = (cast_fixed2fixed<10,-15,UNSIGNED,TRUNCATE>(id321in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id391out_output;

  { // Node ID: 391 (NodeReinterpret)
    const HWOffsetFix<10,-15,UNSIGNED> &id391in_input = id321out_o;

    id391out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id391in_input))));
  }
  { // Node ID: 392 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id392in_addr = id391out_output;

    HWOffsetFix<48,-53,UNSIGNED> id392x_1;

    switch(((long)((id392in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id392x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
      case 1l:
        id392x_1 = (id392sta_rom_store[(id392in_addr.getValueAsLong())]);
        break;
      default:
        id392x_1 = (c_hw_fix_48_n53_uns_undef);
        break;
    }
    id392out_dout[(getCycle()+2)%3] = (id392x_1);
  }
  HWOffsetFix<5,-5,UNSIGNED> id320out_o;

  { // Node ID: 320 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id320in_i = id318out_output;

    id320out_o = (cast_fixed2fixed<5,-5,UNSIGNED,TRUNCATE>(id320in_i));
  }
  HWOffsetFix<5,0,UNSIGNED> id388out_output;

  { // Node ID: 388 (NodeReinterpret)
    const HWOffsetFix<5,-5,UNSIGNED> &id388in_input = id320out_o;

    id388out_output = (cast_bits2fixed<5,0,UNSIGNED>((cast_fixed2bits(id388in_input))));
  }
  { // Node ID: 389 (NodeROM)
    const HWOffsetFix<5,0,UNSIGNED> &id389in_addr = id388out_output;

    HWOffsetFix<53,-53,UNSIGNED> id389x_1;

    switch(((long)((id389in_addr.getValueAsLong())<(32l)))) {
      case 0l:
        id389x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
      case 1l:
        id389x_1 = (id389sta_rom_store[(id389in_addr.getValueAsLong())]);
        break;
      default:
        id389x_1 = (c_hw_fix_53_n53_uns_undef);
        break;
    }
    id389out_dout[(getCycle()+2)%3] = (id389x_1);
  }
  { // Node ID: 326 (NodeConstantRawBits)
  }
  HWOffsetFix<26,-51,UNSIGNED> id323out_o;

  { // Node ID: 323 (NodeCast)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id323in_i = id318out_output;

    id323out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TRUNCATE>(id323in_i));
  }
  HWOffsetFix<47,-72,UNSIGNED> id325out_result;

  { // Node ID: 325 (NodeMul)
    const HWOffsetFix<21,-21,UNSIGNED> &id325in_a = id326out_value;
    const HWOffsetFix<26,-51,UNSIGNED> &id325in_b = id323out_o;

    id325out_result = (mul_fixed<47,-72,UNSIGNED,TONEAREVEN>(id325in_a,id325in_b));
  }
  HWOffsetFix<26,-51,UNSIGNED> id327out_o;

  { // Node ID: 327 (NodeCast)
    const HWOffsetFix<47,-72,UNSIGNED> &id327in_i = id325out_result;

    id327out_o = (cast_fixed2fixed<26,-51,UNSIGNED,TONEAREVEN>(id327in_i));
  }
  { // Node ID: 583 (NodeFIFO)
    const HWOffsetFix<26,-51,UNSIGNED> &id583in_input = id327out_o;

    id583out_output[(getCycle()+2)%3] = id583in_input;
  }
  HWOffsetFix<54,-53,UNSIGNED> id328out_result;

  { // Node ID: 328 (NodeAdd)
    const HWOffsetFix<53,-53,UNSIGNED> &id328in_a = id389out_dout[getCycle()%3];
    const HWOffsetFix<26,-51,UNSIGNED> &id328in_b = id583out_output[getCycle()%3];

    id328out_result = (add_fixed<54,-53,UNSIGNED,TONEAREVEN>(id328in_a,id328in_b));
  }
  HWOffsetFix<64,-89,UNSIGNED> id329out_result;

  { // Node ID: 329 (NodeMul)
    const HWOffsetFix<26,-51,UNSIGNED> &id329in_a = id583out_output[getCycle()%3];
    const HWOffsetFix<53,-53,UNSIGNED> &id329in_b = id389out_dout[getCycle()%3];

    id329out_result = (mul_fixed<64,-89,UNSIGNED,TONEAREVEN>(id329in_a,id329in_b));
  }
  HWOffsetFix<64,-63,UNSIGNED> id330out_result;

  { // Node ID: 330 (NodeAdd)
    const HWOffsetFix<54,-53,UNSIGNED> &id330in_a = id328out_result;
    const HWOffsetFix<64,-89,UNSIGNED> &id330in_b = id329out_result;

    id330out_result = (add_fixed<64,-63,UNSIGNED,TONEAREVEN>(id330in_a,id330in_b));
  }
  HWOffsetFix<58,-57,UNSIGNED> id331out_o;

  { // Node ID: 331 (NodeCast)
    const HWOffsetFix<64,-63,UNSIGNED> &id331in_i = id330out_result;

    id331out_o = (cast_fixed2fixed<58,-57,UNSIGNED,TONEAREVEN>(id331in_i));
  }
  HWOffsetFix<59,-57,UNSIGNED> id332out_result;

  { // Node ID: 332 (NodeAdd)
    const HWOffsetFix<48,-53,UNSIGNED> &id332in_a = id392out_dout[getCycle()%3];
    const HWOffsetFix<58,-57,UNSIGNED> &id332in_b = id331out_o;

    id332out_result = (add_fixed<59,-57,UNSIGNED,TONEAREVEN>(id332in_a,id332in_b));
  }
  HWOffsetFix<64,-68,UNSIGNED> id333out_result;

  { // Node ID: 333 (NodeMul)
    const HWOffsetFix<58,-57,UNSIGNED> &id333in_a = id331out_o;
    const HWOffsetFix<48,-53,UNSIGNED> &id333in_b = id392out_dout[getCycle()%3];

    id333out_result = (mul_fixed<64,-68,UNSIGNED,TONEAREVEN>(id333in_a,id333in_b));
  }
  HWOffsetFix<64,-62,UNSIGNED> id334out_result;

  { // Node ID: 334 (NodeAdd)
    const HWOffsetFix<59,-57,UNSIGNED> &id334in_a = id332out_result;
    const HWOffsetFix<64,-68,UNSIGNED> &id334in_b = id333out_result;

    id334out_result = (add_fixed<64,-62,UNSIGNED,TONEAREVEN>(id334in_a,id334in_b));
  }
  HWOffsetFix<59,-57,UNSIGNED> id335out_o;

  { // Node ID: 335 (NodeCast)
    const HWOffsetFix<64,-62,UNSIGNED> &id335in_i = id334out_result;

    id335out_o = (cast_fixed2fixed<59,-57,UNSIGNED,TONEAREVEN>(id335in_i));
  }
  HWOffsetFix<60,-57,UNSIGNED> id336out_result;

  { // Node ID: 336 (NodeAdd)
    const HWOffsetFix<38,-53,UNSIGNED> &id336in_a = id395out_dout[getCycle()%3];
    const HWOffsetFix<59,-57,UNSIGNED> &id336in_b = id335out_o;

    id336out_result = (add_fixed<60,-57,UNSIGNED,TONEAREVEN>(id336in_a,id336in_b));
  }
  HWOffsetFix<64,-77,UNSIGNED> id337out_result;

  { // Node ID: 337 (NodeMul)
    const HWOffsetFix<59,-57,UNSIGNED> &id337in_a = id335out_o;
    const HWOffsetFix<38,-53,UNSIGNED> &id337in_b = id395out_dout[getCycle()%3];

    id337out_result = (mul_fixed<64,-77,UNSIGNED,TONEAREVEN>(id337in_a,id337in_b));
  }
  HWOffsetFix<64,-61,UNSIGNED> id338out_result;

  { // Node ID: 338 (NodeAdd)
    const HWOffsetFix<60,-57,UNSIGNED> &id338in_a = id336out_result;
    const HWOffsetFix<64,-77,UNSIGNED> &id338in_b = id337out_result;

    id338out_result = (add_fixed<64,-61,UNSIGNED,TONEAREVEN>(id338in_a,id338in_b));
  }
  HWOffsetFix<60,-57,UNSIGNED> id339out_o;

  { // Node ID: 339 (NodeCast)
    const HWOffsetFix<64,-61,UNSIGNED> &id339in_i = id338out_result;

    id339out_o = (cast_fixed2fixed<60,-57,UNSIGNED,TONEAREVEN>(id339in_i));
  }
  HWOffsetFix<53,-52,UNSIGNED> id340out_o;

  { // Node ID: 340 (NodeCast)
    const HWOffsetFix<60,-57,UNSIGNED> &id340in_i = id339out_o;

    id340out_o = (cast_fixed2fixed<53,-52,UNSIGNED,TONEAREVEN>(id340in_i));
  }
  { // Node ID: 622 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id510out_result;

  { // Node ID: 510 (NodeGteInlined)
    const HWOffsetFix<53,-52,UNSIGNED> &id510in_a = id340out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id510in_b = id622out_value;

    id510out_result = (gte_fixed(id510in_a,id510in_b));
  }
  HWOffsetFix<14,0,TWOSCOMPLEMENT> id525out_result;

  { // Node ID: 525 (NodeCondTriAdd)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id525in_a = id346out_o;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id525in_b = id349out_value;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id525in_c = id523out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id525in_condb = id510out_result;

    HWOffsetFix<14,0,TWOSCOMPLEMENT> id525x_1;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id525x_2;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id525x_3;
    HWOffsetFix<14,0,TWOSCOMPLEMENT> id525x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id525x_1 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id525x_1 = id525in_a;
        break;
      default:
        id525x_1 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch((id525in_condb.getValueAsLong())) {
      case 0l:
        id525x_2 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id525x_2 = id525in_b;
        break;
      default:
        id525x_2 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id525x_3 = (c_hw_fix_14_0_sgn_bits_2);
        break;
      case 1l:
        id525x_3 = id525in_c;
        break;
      default:
        id525x_3 = (c_hw_fix_14_0_sgn_undef);
        break;
    }
    (id525x_4) = (add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<14,0,TWOSCOMPLEMENT,TRUNCATE>((id525x_1),(id525x_2))),(id525x_3)));
    id525out_result = (id525x_4);
  }
  HWRawBits<1> id511out_result;

  { // Node ID: 511 (NodeSlice)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id511in_a = id525out_result;

    id511out_result = (slice<13,1>(id511in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id512out_output;

  { // Node ID: 512 (NodeReinterpret)
    const HWRawBits<1> &id512in_input = id511out_result;

    id512out_output = (cast_bits2fixed<1,0,UNSIGNED>(id512in_input));
  }
  { // Node ID: 621 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id311out_result;

  { // Node ID: 311 (NodeGt)
    const HWFloat<11,53> &id311in_a = id530out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id311in_b = id621out_value;

    id311out_result = (gt_float(id311in_a,id311in_b));
  }
  { // Node ID: 585 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id585in_input = id311out_result;

    id585out_output[(getCycle()+6)%7] = id585in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id312out_output;

  { // Node ID: 312 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id312in_input = id309out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id312in_input_doubt = id309out_o_doubt;

    id312out_output = id312in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_a = id585out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id313in_b = id312out_output;

    HWOffsetFix<1,0,UNSIGNED> id313x_1;

    (id313x_1) = (and_fixed(id313in_a,id313in_b));
    id313out_result = (id313x_1);
  }
  { // Node ID: 586 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id586in_input = id313out_result;

    id586out_output[(getCycle()+2)%3] = id586in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id355out_result;

  { // Node ID: 355 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id355in_a = id586out_output[getCycle()%3];

    id355out_result = (not_fixed(id355in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id356out_result;

  { // Node ID: 356 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id356in_a = id512out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id356in_b = id355out_result;

    HWOffsetFix<1,0,UNSIGNED> id356x_1;

    (id356x_1) = (and_fixed(id356in_a,id356in_b));
    id356out_result = (id356x_1);
  }
  { // Node ID: 620 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id315out_result;

  { // Node ID: 315 (NodeLt)
    const HWFloat<11,53> &id315in_a = id530out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id315in_b = id620out_value;

    id315out_result = (lt_float(id315in_a,id315in_b));
  }
  { // Node ID: 587 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id587in_input = id315out_result;

    id587out_output[(getCycle()+6)%7] = id587in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id316out_output;

  { // Node ID: 316 (NodeDoubtBitOp)
    const HWOffsetFix<64,-51,TWOSCOMPLEMENT> &id316in_input = id309out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id316in_input_doubt = id309out_o_doubt;

    id316out_output = id316in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id317out_result;

  { // Node ID: 317 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id317in_a = id587out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id317in_b = id316out_output;

    HWOffsetFix<1,0,UNSIGNED> id317x_1;

    (id317x_1) = (and_fixed(id317in_a,id317in_b));
    id317out_result = (id317x_1);
  }
  { // Node ID: 588 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id588in_input = id317out_result;

    id588out_output[(getCycle()+2)%3] = id588in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id357in_a = id356out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id357in_b = id588out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id357x_1;

    (id357x_1) = (or_fixed(id357in_a,id357in_b));
    id357out_result = (id357x_1);
  }
  { // Node ID: 619 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id513out_result;

  { // Node ID: 513 (NodeGteInlined)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id513in_a = id525out_result;
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id513in_b = id619out_value;

    id513out_result = (gte_fixed(id513in_a,id513in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id364out_result;

  { // Node ID: 364 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id364in_a = id588out_output[getCycle()%3];

    id364out_result = (not_fixed(id364in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id365out_result;

  { // Node ID: 365 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id365in_a = id513out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id365in_b = id364out_result;

    HWOffsetFix<1,0,UNSIGNED> id365x_1;

    (id365x_1) = (and_fixed(id365in_a,id365in_b));
    id365out_result = (id365x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id366out_result;

  { // Node ID: 366 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id366in_a = id365out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id366in_b = id586out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id366x_1;

    (id366x_1) = (or_fixed(id366in_a,id366in_b));
    id366out_result = (id366x_1);
  }
  HWRawBits<2> id367out_result;

  { // Node ID: 367 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id367in_in0 = id357out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id367in_in1 = id366out_result;

    id367out_result = (cat(id367in_in0,id367in_in1));
  }
  { // Node ID: 359 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id358out_o;

  { // Node ID: 358 (NodeCast)
    const HWOffsetFix<14,0,TWOSCOMPLEMENT> &id358in_i = id525out_result;

    id358out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id358in_i));
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  HWOffsetFix<53,-52,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id510out_result;
    const HWOffsetFix<53,-52,UNSIGNED> &id344in_option0 = id340out_o;
    const HWOffsetFix<53,-52,UNSIGNED> &id344in_option1 = id343out_value;

    HWOffsetFix<53,-52,UNSIGNED> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_fix_53_n52_uns_undef);
        break;
    }
    id344out_result = (id344x_1);
  }
  HWOffsetFix<52,-52,UNSIGNED> id345out_o;

  { // Node ID: 345 (NodeCast)
    const HWOffsetFix<53,-52,UNSIGNED> &id345in_i = id344out_result;

    id345out_o = (cast_fixed2fixed<52,-52,UNSIGNED,TONEAREVEN>(id345in_i));
  }
  HWRawBits<64> id360out_result;

  { // Node ID: 360 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id360in_in0 = id359out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id360in_in1 = id358out_o;
    const HWOffsetFix<52,-52,UNSIGNED> &id360in_in2 = id345out_o;

    id360out_result = (cat((cat(id360in_in0,id360in_in1)),id360in_in2));
  }
  HWFloat<11,53> id361out_output;

  { // Node ID: 361 (NodeReinterpret)
    const HWRawBits<64> &id361in_input = id360out_result;

    id361out_output = (cast_bits2float<11,53>(id361in_input));
  }
  { // Node ID: 368 (NodeConstantRawBits)
  }
  { // Node ID: 369 (NodeConstantRawBits)
  }
  { // Node ID: 371 (NodeConstantRawBits)
  }
  HWRawBits<64> id514out_result;

  { // Node ID: 514 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id514in_in0 = id368out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id514in_in1 = id369out_value;
    const HWOffsetFix<52,0,UNSIGNED> &id514in_in2 = id371out_value;

    id514out_result = (cat((cat(id514in_in0,id514in_in1)),id514in_in2));
  }
  HWFloat<11,53> id373out_output;

  { // Node ID: 373 (NodeReinterpret)
    const HWRawBits<64> &id373in_input = id514out_result;

    id373out_output = (cast_bits2float<11,53>(id373in_input));
  }
  { // Node ID: 488 (NodeConstantRawBits)
  }
  HWFloat<11,53> id376out_result;

  { // Node ID: 376 (NodeMux)
    const HWRawBits<2> &id376in_sel = id367out_result;
    const HWFloat<11,53> &id376in_option0 = id361out_output;
    const HWFloat<11,53> &id376in_option1 = id373out_output;
    const HWFloat<11,53> &id376in_option2 = id488out_value;
    const HWFloat<11,53> &id376in_option3 = id373out_output;

    HWFloat<11,53> id376x_1;

    switch((id376in_sel.getValueAsLong())) {
      case 0l:
        id376x_1 = id376in_option0;
        break;
      case 1l:
        id376x_1 = id376in_option1;
        break;
      case 2l:
        id376x_1 = id376in_option2;
        break;
      case 3l:
        id376x_1 = id376in_option3;
        break;
      default:
        id376x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id376out_result = (id376x_1);
  }
  { // Node ID: 618 (NodeConstantRawBits)
  }
  HWFloat<11,53> id386out_result;

  { // Node ID: 386 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id386in_sel = id591out_output[getCycle()%9];
    const HWFloat<11,53> &id386in_option0 = id376out_result;
    const HWFloat<11,53> &id386in_option1 = id618out_value;

    HWFloat<11,53> id386x_1;

    switch((id386in_sel.getValueAsLong())) {
      case 0l:
        id386x_1 = id386in_option0;
        break;
      case 1l:
        id386x_1 = id386in_option1;
        break;
      default:
        id386x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id386out_result = (id386x_1);
  }
  { // Node ID: 617 (NodeConstantRawBits)
  }
  HWFloat<11,53> id397out_result;

  { // Node ID: 397 (NodeAdd)
    const HWFloat<11,53> &id397in_a = id386out_result;
    const HWFloat<11,53> &id397in_b = id617out_value;

    id397out_result = (add_float(id397in_a,id397in_b));
  }
  HWFloat<11,53> id399out_result;

  { // Node ID: 399 (NodeDiv)
    const HWFloat<11,53> &id399in_a = id624out_value;
    const HWFloat<11,53> &id399in_b = id397out_result;

    id399out_result = (div_float(id399in_a,id399in_b));
  }
  HWFloat<11,53> id401out_result;

  { // Node ID: 401 (NodeSub)
    const HWFloat<11,53> &id401in_a = id625out_value;
    const HWFloat<11,53> &id401in_b = id399out_result;

    id401out_result = (sub_float(id401in_a,id401in_b));
  }
  HWFloat<11,53> id403out_result;

  { // Node ID: 403 (NodeAdd)
    const HWFloat<11,53> &id403in_a = id626out_value;
    const HWFloat<11,53> &id403in_b = id401out_result;

    id403out_result = (add_float(id403in_a,id403in_b));
  }
  { // Node ID: 531 (NodePO2FPMult)
    const HWFloat<11,53> &id531in_floatIn = id403out_result;

    id531out_floatOut[(getCycle()+1)%2] = (mul_float(id531in_floatIn,(c_hw_flt_11_53_0_5val)));
  }
  HWFloat<11,53> id406out_result;

  { // Node ID: 406 (NodeSub)
    const HWFloat<11,53> &id406in_a = id531out_floatOut[getCycle()%2];
    const HWFloat<11,53> &id406in_b = id592out_output[getCycle()%11];

    id406out_result = (sub_float(id406in_a,id406in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id274out_result;

  { // Node ID: 274 (NodeGt)
    const HWFloat<11,53> &id274in_a = id600out_output[getCycle()%2];
    const HWFloat<11,53> &id274in_b = id18out_value;

    id274out_result = (gt_float(id274in_a,id274in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  HWFloat<11,53> id275out_result;

  { // Node ID: 275 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id275in_sel = id274out_result;
    const HWFloat<11,53> &id275in_option0 = id6out_value;
    const HWFloat<11,53> &id275in_option1 = id7out_value;

    HWFloat<11,53> id275x_1;

    switch((id275in_sel.getValueAsLong())) {
      case 0l:
        id275x_1 = id275in_option0;
        break;
      case 1l:
        id275x_1 = id275in_option1;
        break;
      default:
        id275x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id275out_result = (id275x_1);
  }
  HWFloat<11,53> id407out_result;

  { // Node ID: 407 (NodeDiv)
    const HWFloat<11,53> &id407in_a = id406out_result;
    const HWFloat<11,53> &id407in_b = id275out_result;

    id407out_result = (div_float(id407in_a,id407in_b));
  }
  { // Node ID: 412 (NodeMul)
    const HWFloat<11,53> &id412in_a = id407out_result;
    const HWFloat<11,53> &id412in_b = id44out_dt;

    id412out_result[(getCycle()+12)%13] = (mul_float(id412in_a,id412in_b));
  }
  { // Node ID: 413 (NodeAdd)
    const HWFloat<11,53> &id413in_a = id610out_output[getCycle()%13];
    const HWFloat<11,53> &id413in_b = id412out_result[getCycle()%13];

    id413out_result[(getCycle()+14)%15] = (add_float(id413in_a,id413in_b));
  }
  { // Node ID: 580 (NodeFIFO)
    const HWFloat<11,53> &id580in_input = id413out_result[getCycle()%15];

    id580out_output[(getCycle()+16)%17] = id580in_input;
  }
  { // Node ID: 430 (NodeMul)
    const HWFloat<11,53> &id430in_a = id429out_result;
    const HWFloat<11,53> &id430in_b = id580out_output[getCycle()%17];

    id430out_result[(getCycle()+12)%13] = (mul_float(id430in_a,id430in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  { // Node ID: 431 (NodeDiv)
    const HWFloat<11,53> &id431in_a = id430out_result[getCycle()%13];
    const HWFloat<11,53> &id431in_b = id13out_value;

    id431out_result[(getCycle()+57)%58] = (div_float(id431in_a,id431in_b));
  }
  { // Node ID: 433 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id433in_sel = id428out_result[getCycle()%3];
    const HWFloat<11,53> &id433in_option0 = id432out_value;
    const HWFloat<11,53> &id433in_option1 = id431out_result[getCycle()%58];

    HWFloat<11,53> id433x_1;

    switch((id433in_sel.getValueAsLong())) {
      case 0l:
        id433x_1 = id433in_option0;
        break;
      case 1l:
        id433x_1 = id433in_option1;
        break;
      default:
        id433x_1 = (c_hw_flt_11_53_undef);
        break;
    }
    id433out_result[(getCycle()+1)%2] = (id433x_1);
  }
  { // Node ID: 435 (NodeAdd)
    const HWFloat<11,53> &id435in_a = id434out_result[getCycle()%15];
    const HWFloat<11,53> &id435in_b = id433out_result[getCycle()%2];

    id435out_result[(getCycle()+14)%15] = (add_float(id435in_a,id435in_b));
  }
  { // Node ID: 436 (NodeMul)
    const HWFloat<11,53> &id436in_a = id435out_result[getCycle()%15];
    const HWFloat<11,53> &id436in_b = id44out_dt;

    id436out_result[(getCycle()+12)%13] = (mul_float(id436in_a,id436in_b));
  }
  { // Node ID: 437 (NodeSub)
    const HWFloat<11,53> &id437in_a = id607out_output[getCycle()%30];
    const HWFloat<11,53> &id437in_b = id436out_result[getCycle()%13];

    id437out_result[(getCycle()+14)%15] = (sub_float(id437in_a,id437in_b));
  }
  if ( (getFillLevel() >= (167l)) && (getFlushLevel() < (167l)|| !isFlushingActive() ))
  { // Node ID: 449 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id449in_output_control = id597out_output[getCycle()%164];
    const HWFloat<11,53> &id449in_data = id437out_result[getCycle()%15];

    bool id449x_1;

    (id449x_1) = ((id449in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(167l))&(isFlushingActive()))));
    if((id449x_1)) {
      writeOutput(m_u_out, id449in_data);
    }
  }
  { // Node ID: 616 (NodeConstantRawBits)
  }
  { // Node ID: 452 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id452in_a = id611out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id452in_b = id616out_value;

    id452out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id452in_a,id452in_b));
  }
  { // Node ID: 515 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id515in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id515in_b = id452out_result[getCycle()%2];

    id515out_result[(getCycle()+1)%2] = (eq_fixed(id515in_a,id515in_b));
  }
  { // Node ID: 454 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id455out_result;

  { // Node ID: 455 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id455in_a = id454out_io_v_out_force_disabled;

    id455out_result = (not_fixed(id455in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id456out_result;

  { // Node ID: 456 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id456in_a = id515out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id456in_b = id455out_result;

    HWOffsetFix<1,0,UNSIGNED> id456x_1;

    (id456x_1) = (and_fixed(id456in_a,id456in_b));
    id456out_result = (id456x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 457 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id457in_output_control = id456out_result;
    const HWFloat<11,53> &id457in_data = id543out_output[getCycle()%137];

    bool id457x_1;

    (id457x_1) = ((id457in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id457x_1)) {
      writeOutput(m_v_out, id457in_data);
    }
  }
  { // Node ID: 615 (NodeConstantRawBits)
  }
  { // Node ID: 460 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id460in_a = id611out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id460in_b = id615out_value;

    id460out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id460in_a,id460in_b));
  }
  { // Node ID: 516 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id516in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id516in_b = id460out_result[getCycle()%2];

    id516out_result[(getCycle()+1)%2] = (eq_fixed(id516in_a,id516in_b));
  }
  { // Node ID: 462 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id463out_result;

  { // Node ID: 463 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id463in_a = id462out_io_w_out_force_disabled;

    id463out_result = (not_fixed(id463in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id464out_result;

  { // Node ID: 464 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id464in_a = id516out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id464in_b = id463out_result;

    HWOffsetFix<1,0,UNSIGNED> id464x_1;

    (id464x_1) = (and_fixed(id464in_a,id464in_b));
    id464out_result = (id464x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 465 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id465in_output_control = id464out_result;
    const HWFloat<11,53> &id465in_data = id539out_output;

    bool id465x_1;

    (id465x_1) = ((id465in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id465x_1)) {
      writeOutput(m_w_out, id465in_data);
    }
  }
  { // Node ID: 614 (NodeConstantRawBits)
  }
  { // Node ID: 468 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id468in_a = id611out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id468in_b = id614out_value;

    id468out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id468in_a,id468in_b));
  }
  { // Node ID: 517 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id517in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id517in_b = id468out_result[getCycle()%2];

    id517out_result[(getCycle()+1)%2] = (eq_fixed(id517in_a,id517in_b));
  }
  { // Node ID: 470 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id471out_result;

  { // Node ID: 471 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id471in_a = id470out_io_s_out_force_disabled;

    id471out_result = (not_fixed(id471in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id472out_result;

  { // Node ID: 472 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id472in_a = id517out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id472in_b = id471out_result;

    HWOffsetFix<1,0,UNSIGNED> id472x_1;

    (id472x_1) = (and_fixed(id472in_a,id472in_b));
    id472out_result = (id472x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 473 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id473in_output_control = id472out_result;
    const HWFloat<11,53> &id473in_data = id581out_output[getCycle()%111];

    bool id473x_1;

    (id473x_1) = ((id473in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id473x_1)) {
      writeOutput(m_s_out, id473in_data);
    }
  }
  { // Node ID: 478 (NodeConstantRawBits)
  }
  { // Node ID: 613 (NodeConstantRawBits)
  }
  { // Node ID: 475 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 476 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id476in_enable = id613out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id476in_max = id475out_value;

    HWOffsetFix<49,0,UNSIGNED> id476x_1;
    HWOffsetFix<1,0,UNSIGNED> id476x_2;
    HWOffsetFix<1,0,UNSIGNED> id476x_3;
    HWOffsetFix<49,0,UNSIGNED> id476x_4t_1e_1;

    id476out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id476st_count)));
    (id476x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id476st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id476x_2) = (gte_fixed((id476x_1),id476in_max));
    (id476x_3) = (and_fixed((id476x_2),id476in_enable));
    id476out_wrap = (id476x_3);
    if((id476in_enable.getValueAsBool())) {
      if(((id476x_3).getValueAsBool())) {
        (id476st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id476x_4t_1e_1) = (id476x_1);
        (id476st_count) = (id476x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id477out_output;

  { // Node ID: 477 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id477in_input = id476out_count;

    id477out_output = id477in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 479 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id479in_load = id478out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id479in_data = id477out_output;

    bool id479x_1;

    (id479x_1) = ((id479in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id479x_1)) {
      setMappedRegValue("current_run_cycle_count", id479in_data);
    }
  }
  { // Node ID: 612 (NodeConstantRawBits)
  }
  { // Node ID: 481 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 482 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id482in_enable = id612out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id482in_max = id481out_value;

    HWOffsetFix<49,0,UNSIGNED> id482x_1;
    HWOffsetFix<1,0,UNSIGNED> id482x_2;
    HWOffsetFix<1,0,UNSIGNED> id482x_3;
    HWOffsetFix<49,0,UNSIGNED> id482x_4t_1e_1;

    id482out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id482st_count)));
    (id482x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id482st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id482x_2) = (gte_fixed((id482x_1),id482in_max));
    (id482x_3) = (and_fixed((id482x_2),id482in_enable));
    id482out_wrap = (id482x_3);
    if((id482in_enable.getValueAsBool())) {
      if(((id482x_3).getValueAsBool())) {
        (id482st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id482x_4t_1e_1) = (id482x_1);
        (id482st_count) = (id482x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 484 (NodeInputMappedReg)
  }
  { // Node ID: 518 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id518in_a = id482out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id518in_b = id484out_run_cycle_count;

    id518out_result[(getCycle()+1)%2] = (eq_fixed(id518in_a,id518in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 483 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id483in_start = id518out_result[getCycle()%2];

    if((id483in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
