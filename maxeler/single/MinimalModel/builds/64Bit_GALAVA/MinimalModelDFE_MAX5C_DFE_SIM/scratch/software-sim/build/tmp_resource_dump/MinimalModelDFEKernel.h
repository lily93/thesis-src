#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id40out_value;

  HWOffsetFix<11,0,UNSIGNED> id611out_value;

  HWOffsetFix<11,0,UNSIGNED> id541out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id43out_count;
  HWOffsetFix<1,0,UNSIGNED> id43out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id43st_count;

  HWOffsetFix<11,0,UNSIGNED> id653out_value;

  HWOffsetFix<11,0,UNSIGNED> id444out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id489out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id446out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id597out_output[164];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id42out_count;
  HWOffsetFix<1,0,UNSIGNED> id42out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id42st_count;

  HWOffsetFix<32,0,UNSIGNED> id652out_value;

  HWOffsetFix<1,0,UNSIGNED> id490out_result[2];

  HWFloat<11,53> id38out_value;

  HWFloat<11,53> id48out_result[2];

  HWFloat<11,53> id563out_output[10];

  HWFloat<11,53> id600out_output[2];

  HWFloat<11,53> id601out_output[3];

  HWFloat<11,53> id602out_output[13];

  HWFloat<11,53> id603out_output[18];

  HWFloat<11,53> id604out_output[10];

  HWFloat<11,53> id605out_output[56];

  HWFloat<11,53> id606out_output[15];

  HWFloat<11,53> id607out_output[30];

  HWFloat<11,53> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id414out_result[3];

  HWFloat<11,53> id37out_value;

  HWOffsetFix<32,0,UNSIGNED> id651out_value;

  HWOffsetFix<1,0,UNSIGNED> id491out_result[2];

  HWFloat<11,53> id543out_output[137];

  HWFloat<11,53> id51out_result[2];

  HWFloat<11,53> id544out_output[13];

  HWFloat<11,53> id17out_value;

  HWFloat<11,53> id16out_value;

  HWFloat<11,53> id1out_value;

  HWFloat<11,53> id2out_value;

  HWFloat<11,53> id0out_value;

  HWFloat<11,53> id44out_dt;

  HWFloat<11,53> id408out_result[13];

  HWFloat<11,53> id409out_result[15];

  HWFloat<11,53> id416out_result[15];

  HWFloat<11,53> id417out_result[13];

  HWFloat<11,53> id29out_value;

  HWFloat<11,53> id418out_result[15];

  HWFloat<11,53> id419out_result[13];

  HWFloat<11,53> id8out_value;

  HWFloat<11,53> id420out_result[58];

  HWFloat<11,53> id421out_result[2];

  HWFloat<11,53> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id422out_result[3];

  HWFloat<11,53> id22out_value;

  HWFloat<11,53> id9out_value;

  HWFloat<11,53> id10out_value;

  HWFloat<11,53> id426out_result[58];

  HWFloat<11,53> id650out_value;

  HWFloat<11,53> id11out_value;

  HWFloat<11,53> id36out_value;

  HWFloat<11,53> id649out_value;

  HWFloat<11,53> id648out_value;

  HWFloat<11,53> id647out_value;

  HWFloat<11,53> id25out_value;

  HWFloat<11,53> id30out_value;

  HWFloat<11,53> id528out_floatOut[2];

  HWRawBits<11> id248out_value;

  HWRawBits<52> id646out_value;

  HWOffsetFix<1,0,UNSIGNED> id560out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id171out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id173out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id173out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id176out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id551out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id217out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id519out_value;

  HWOffsetFix<38,-53,UNSIGNED> id263out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id263sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id260out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id260sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id257out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id257sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id194out_value;

  HWOffsetFix<26,-51,UNSIGNED> id552out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id645out_value;

  HWFloat<11,53> id644out_value;

  HWOffsetFix<1,0,UNSIGNED> id554out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id555out_output[3];

  HWFloat<11,53> id643out_value;

  HWOffsetFix<1,0,UNSIGNED> id556out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id557out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id642out_value;

  HWOffsetFix<1,0,UNSIGNED> id227out_value;

  HWOffsetFix<53,-52,UNSIGNED> id211out_value;

  HWOffsetFix<1,0,UNSIGNED> id236out_value;

  HWOffsetFix<11,0,UNSIGNED> id237out_value;

  HWOffsetFix<52,0,UNSIGNED> id239out_value;

  HWFloat<11,53> id486out_value;

  HWFloat<11,53> id641out_value;

  HWFloat<11,53> id640out_value;

  HWFloat<11,53> id424out_result[58];

  HWFloat<11,53> id427out_result[2];

  HWFloat<11,53> id434out_result[15];

  HWFloat<11,53> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id428out_result[3];

  HWFloat<11,53> id432out_value;

  HWOffsetFix<32,0,UNSIGNED> id639out_value;

  HWOffsetFix<1,0,UNSIGNED> id499out_result[2];

  HWFloat<11,53> id609out_output[111];

  HWFloat<11,53> id54out_result[2];

  HWFloat<11,53> id566out_output[10];

  HWFloat<11,53> id608out_output[13];

  HWFloat<11,53> id18out_value;

  HWFloat<11,53> id19out_value;

  HWFloat<11,53> id638out_value;

  HWFloat<11,53> id14out_value;

  HWFloat<11,53> id32out_value;

  HWFloat<11,53> id4out_value;

  HWFloat<11,53> id35out_value;

  HWFloat<11,53> id637out_value;

  HWFloat<11,53> id636out_value;

  HWFloat<11,53> id635out_value;

  HWFloat<11,53> id23out_value;

  HWFloat<11,53> id26out_value;

  HWFloat<11,53> id529out_floatOut[2];

  HWRawBits<11> id141out_value;

  HWRawBits<52> id634out_value;

  HWOffsetFix<1,0,UNSIGNED> id576out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id64out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id66out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id66out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id69out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id567out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id110out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id521out_value;

  HWOffsetFix<38,-53,UNSIGNED> id156out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id156sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id153out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id153sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id150out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id150sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id87out_value;

  HWOffsetFix<26,-51,UNSIGNED> id568out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id633out_value;

  HWFloat<11,53> id632out_value;

  HWOffsetFix<1,0,UNSIGNED> id570out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id571out_output[3];

  HWFloat<11,53> id631out_value;

  HWOffsetFix<1,0,UNSIGNED> id572out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id573out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id630out_value;

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<53,-52,UNSIGNED> id104out_value;

  HWOffsetFix<1,0,UNSIGNED> id129out_value;

  HWOffsetFix<11,0,UNSIGNED> id130out_value;

  HWOffsetFix<52,0,UNSIGNED> id132out_value;

  HWFloat<11,53> id487out_value;

  HWFloat<11,53> id629out_value;

  HWFloat<11,53> id628out_value;

  HWFloat<11,53> id3out_value;

  HWFloat<11,53> id410out_result[13];

  HWFloat<11,53> id411out_result[15];

  HWFloat<11,53> id579out_output[18];

  HWOffsetFix<32,0,UNSIGNED> id627out_value;

  HWOffsetFix<1,0,UNSIGNED> id507out_result[2];

  HWFloat<11,53> id581out_output[111];

  HWFloat<11,53> id57out_result[2];

  HWFloat<11,53> id592out_output[11];

  HWFloat<11,53> id610out_output[13];

  HWFloat<11,53> id626out_value;

  HWFloat<11,53> id625out_value;

  HWFloat<11,53> id624out_value;

  HWFloat<11,53> id24out_value;

  HWFloat<11,53> id27out_value;

  HWFloat<11,53> id530out_floatOut[2];

  HWRawBits<11> id380out_value;

  HWRawBits<52> id623out_value;

  HWOffsetFix<1,0,UNSIGNED> id591out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id303out_value;

  HWOffsetFix<64,-51,TWOSCOMPLEMENT> id305out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id305out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id308out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id582out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id349out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id523out_value;

  HWOffsetFix<38,-53,UNSIGNED> id395out_dout[3];

  HWOffsetFix<38,-53,UNSIGNED> id395sta_rom_store[1024];

  HWOffsetFix<48,-53,UNSIGNED> id392out_dout[3];

  HWOffsetFix<48,-53,UNSIGNED> id392sta_rom_store[1024];

  HWOffsetFix<53,-53,UNSIGNED> id389out_dout[3];

  HWOffsetFix<53,-53,UNSIGNED> id389sta_rom_store[32];

  HWOffsetFix<21,-21,UNSIGNED> id326out_value;

  HWOffsetFix<26,-51,UNSIGNED> id583out_output[3];

  HWOffsetFix<53,-52,UNSIGNED> id622out_value;

  HWFloat<11,53> id621out_value;

  HWOffsetFix<1,0,UNSIGNED> id585out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id586out_output[3];

  HWFloat<11,53> id620out_value;

  HWOffsetFix<1,0,UNSIGNED> id587out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id588out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id619out_value;

  HWOffsetFix<1,0,UNSIGNED> id359out_value;

  HWOffsetFix<53,-52,UNSIGNED> id343out_value;

  HWOffsetFix<1,0,UNSIGNED> id368out_value;

  HWOffsetFix<11,0,UNSIGNED> id369out_value;

  HWOffsetFix<52,0,UNSIGNED> id371out_value;

  HWFloat<11,53> id488out_value;

  HWFloat<11,53> id618out_value;

  HWFloat<11,53> id617out_value;

  HWFloat<11,53> id531out_floatOut[2];

  HWFloat<11,53> id6out_value;

  HWFloat<11,53> id7out_value;

  HWFloat<11,53> id412out_result[13];

  HWFloat<11,53> id413out_result[15];

  HWFloat<11,53> id580out_output[17];

  HWFloat<11,53> id430out_result[13];

  HWFloat<11,53> id13out_value;

  HWFloat<11,53> id431out_result[58];

  HWFloat<11,53> id433out_result[2];

  HWFloat<11,53> id435out_result[15];

  HWFloat<11,53> id436out_result[13];

  HWFloat<11,53> id437out_result[15];

  HWOffsetFix<11,0,UNSIGNED> id616out_value;

  HWOffsetFix<11,0,UNSIGNED> id452out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id515out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id454out_io_v_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id615out_value;

  HWOffsetFix<11,0,UNSIGNED> id460out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id516out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id462out_io_w_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id614out_value;

  HWOffsetFix<11,0,UNSIGNED> id468out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id517out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id470out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id478out_value;

  HWOffsetFix<1,0,UNSIGNED> id613out_value;

  HWOffsetFix<49,0,UNSIGNED> id475out_value;

  HWOffsetFix<48,0,UNSIGNED> id476out_count;
  HWOffsetFix<1,0,UNSIGNED> id476out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id476st_count;

  HWOffsetFix<1,0,UNSIGNED> id612out_value;

  HWOffsetFix<49,0,UNSIGNED> id481out_value;

  HWOffsetFix<48,0,UNSIGNED> id482out_count;
  HWOffsetFix<1,0,UNSIGNED> id482out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id482st_count;

  HWOffsetFix<48,0,UNSIGNED> id484out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id518out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits;
  const HWFloat<11,53> c_hw_flt_11_53_undef;
  const HWFloat<11,53> c_hw_flt_11_53_bits_1;
  const HWFloat<11,53> c_hw_flt_11_53_bits_2;
  const HWFloat<11,53> c_hw_flt_11_53_bits_3;
  const HWFloat<11,53> c_hw_flt_11_53_bits_4;
  const HWFloat<11,53> c_hw_flt_11_53_bits_5;
  const HWFloat<11,53> c_hw_flt_11_53_bits_6;
  const HWFloat<11,53> c_hw_flt_11_53_bits_7;
  const HWFloat<11,53> c_hw_flt_11_53_bits_8;
  const HWFloat<11,53> c_hw_flt_11_53_bits_9;
  const HWFloat<11,53> c_hw_flt_11_53_bits_10;
  const HWFloat<11,53> c_hw_flt_11_53_bits_11;
  const HWFloat<11,53> c_hw_flt_11_53_bits_12;
  const HWFloat<11,53> c_hw_flt_11_53_bits_13;
  const HWFloat<11,53> c_hw_flt_11_53_bits_14;
  const HWFloat<11,53> c_hw_flt_11_53_bits_15;
  const HWFloat<11,53> c_hw_flt_11_53_bits_16;
  const HWFloat<11,53> c_hw_flt_11_53_2_0val;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<52> c_hw_bit_52_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<38,-53,UNSIGNED> c_hw_fix_38_n53_uns_undef;
  const HWOffsetFix<48,-53,UNSIGNED> c_hw_fix_48_n53_uns_undef;
  const HWOffsetFix<53,-53,UNSIGNED> c_hw_fix_53_n53_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<26,-51,UNSIGNED> c_hw_fix_26_n51_uns_undef;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_bits_1;
  const HWOffsetFix<53,-52,UNSIGNED> c_hw_fix_53_n52_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<52,0,UNSIGNED> c_hw_fix_52_0_uns_bits;
  const HWFloat<11,53> c_hw_flt_11_53_bits_17;
  const HWFloat<11,53> c_hw_flt_11_53_bits_18;
  const HWFloat<11,53> c_hw_flt_11_53_bits_19;
  const HWFloat<11,53> c_hw_flt_11_53_bits_20;
  const HWFloat<11,53> c_hw_flt_11_53_bits_21;
  const HWFloat<11,53> c_hw_flt_11_53_bits_22;
  const HWFloat<11,53> c_hw_flt_11_53_bits_23;
  const HWFloat<11,53> c_hw_flt_11_53_bits_24;
  const HWFloat<11,53> c_hw_flt_11_53_bits_25;
  const HWFloat<11,53> c_hw_flt_11_53_0_5val;
  const HWFloat<11,53> c_hw_flt_11_53_bits_26;
  const HWFloat<11,53> c_hw_flt_11_53_bits_27;
  const HWFloat<11,53> c_hw_flt_11_53_bits_28;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
