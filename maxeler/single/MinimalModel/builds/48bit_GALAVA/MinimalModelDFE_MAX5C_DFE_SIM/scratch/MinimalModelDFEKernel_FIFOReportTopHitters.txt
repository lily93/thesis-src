Top 10 largest FIFOs
     Costs                                              From                                                To
         6                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:206
         4                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:215
         4                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:212
         4                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:213
         4                       MinimalModelKernel.maxj:158                       MinimalModelKernel.maxj:203
         4                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:169
         4                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:167
         4                       MinimalModelKernel.maxj:493                       MinimalModelKernel.maxj:493
         4                       MinimalModelKernel.maxj:157                       MinimalModelKernel.maxj:202
         3                       MinimalModelKernel.maxj:157                       MinimalModelKernel.maxj:176

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
         8                       MinimalModelKernel.maxj:213         4
         6                       MinimalModelKernel.maxj:206         3
         5                       MinimalModelKernel.maxj:176         2
         4                       MinimalModelKernel.maxj:203         2
         4                       MinimalModelKernel.maxj:215         2
         4                       MinimalModelKernel.maxj:212         2
         4                       MinimalModelKernel.maxj:169         2
         4                       MinimalModelKernel.maxj:202         2
         4                       MinimalModelKernel.maxj:167         2
         4                       MinimalModelKernel.maxj:493         2

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
        33                       MinimalModelKernel.maxj:155        16
         7                       MinimalModelKernel.maxj:157         3
         6                       MinimalModelKernel.maxj:158         3
         4                       MinimalModelKernel.maxj:203         2
         4                       MinimalModelKernel.maxj:202         2
         4                       MinimalModelKernel.maxj:221         2
         4                       MinimalModelKernel.maxj:222         2
         4                       MinimalModelKernel.maxj:220         2
         4                       MinimalModelKernel.maxj:493         2
         2                       MinimalModelKernel.maxj:156         1
