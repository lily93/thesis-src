#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id40out_value;

  HWOffsetFix<11,0,UNSIGNED> id564out_value;

  HWOffsetFix<11,0,UNSIGNED> id493out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id43out_count;
  HWOffsetFix<1,0,UNSIGNED> id43out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id43st_count;

  HWOffsetFix<11,0,UNSIGNED> id606out_value;

  HWOffsetFix<11,0,UNSIGNED> id396out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id441out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id398out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id550out_output[75];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id42out_count;
  HWOffsetFix<1,0,UNSIGNED> id42out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id42st_count;

  HWOffsetFix<32,0,UNSIGNED> id605out_value;

  HWOffsetFix<1,0,UNSIGNED> id442out_result[2];

  HWFloat<8,12> id38out_value;

  HWFloat<8,12> id48out_result[2];

  HWFloat<8,12> id497out_output[7];

  HWFloat<8,12> id554out_output[4];

  HWFloat<8,12> id555out_output[2];

  HWFloat<8,12> id556out_output[3];

  HWFloat<8,12> id557out_output[6];

  HWFloat<8,12> id558out_output[10];

  HWFloat<8,12> id559out_output[15];

  HWFloat<8,12> id560out_output[9];

  HWFloat<8,12> id561out_output[18];

  HWFloat<8,12> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id366out_result[3];

  HWFloat<8,12> id37out_value;

  HWOffsetFix<32,0,UNSIGNED> id604out_value;

  HWOffsetFix<1,0,UNSIGNED> id443out_result[2];

  HWFloat<8,12> id495out_output[60];

  HWFloat<8,12> id51out_result[2];

  HWFloat<8,12> id496out_output[7];

  HWFloat<8,12> id17out_value;

  HWFloat<8,12> id16out_value;

  HWFloat<8,12> id1out_value;

  HWFloat<8,12> id2out_value;

  HWFloat<8,12> id0out_value;

  HWFloat<11,53> id44out_dt;

  HWFloat<8,12> id45out_o[4];

  HWFloat<8,12> id360out_result[7];

  HWFloat<8,12> id361out_result[9];

  HWFloat<8,12> id368out_result[9];

  HWFloat<8,12> id369out_result[7];

  HWFloat<8,12> id29out_value;

  HWFloat<8,12> id370out_result[9];

  HWFloat<8,12> id371out_result[7];

  HWFloat<8,12> id8out_value;

  HWFloat<8,12> id372out_result[17];

  HWFloat<8,12> id373out_result[2];

  HWFloat<8,12> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id374out_result[3];

  HWFloat<8,12> id22out_value;

  HWFloat<8,12> id9out_value;

  HWFloat<8,12> id10out_value;

  HWFloat<8,12> id378out_result[17];

  HWFloat<8,12> id603out_value;

  HWFloat<8,12> id11out_value;

  HWFloat<8,12> id36out_value;

  HWFloat<8,12> id602out_value;

  HWFloat<8,12> id601out_value;

  HWFloat<8,12> id600out_value;

  HWFloat<8,12> id25out_value;

  HWFloat<8,12> id30out_value;

  HWFloat<8,12> id480out_floatOut[2];

  HWRawBits<8> id222out_value;

  HWRawBits<11> id599out_value;

  HWOffsetFix<1,0,UNSIGNED> id512out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id155out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id157out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id157out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id160out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id503out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id191out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id471out_value;

  HWOffsetFix<12,-12,UNSIGNED> id231out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id231sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id176out_value;

  HWOffsetFix<8,-14,UNSIGNED> id504out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id598out_value;

  HWFloat<8,12> id597out_value;

  HWOffsetFix<1,0,UNSIGNED> id506out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id507out_output[3];

  HWFloat<8,12> id596out_value;

  HWOffsetFix<1,0,UNSIGNED> id508out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id509out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id595out_value;

  HWOffsetFix<1,0,UNSIGNED> id201out_value;

  HWOffsetFix<12,-11,UNSIGNED> id185out_value;

  HWOffsetFix<1,0,UNSIGNED> id210out_value;

  HWOffsetFix<8,0,UNSIGNED> id211out_value;

  HWOffsetFix<11,0,UNSIGNED> id213out_value;

  HWFloat<8,12> id438out_value;

  HWFloat<8,12> id594out_value;

  HWFloat<8,12> id593out_value;

  HWFloat<8,12> id376out_result[17];

  HWFloat<8,12> id379out_result[2];

  HWFloat<8,12> id386out_result[9];

  HWFloat<8,12> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id380out_result[3];

  HWFloat<8,12> id384out_value;

  HWOffsetFix<32,0,UNSIGNED> id592out_value;

  HWOffsetFix<1,0,UNSIGNED> id451out_result[2];

  HWFloat<8,12> id515out_output[46];

  HWFloat<8,12> id54out_result[2];

  HWFloat<8,12> id519out_output[10];

  HWFloat<8,12> id562out_output[7];

  HWFloat<8,12> id18out_value;

  HWFloat<8,12> id19out_value;

  HWFloat<8,12> id591out_value;

  HWFloat<8,12> id14out_value;

  HWFloat<8,12> id32out_value;

  HWFloat<8,12> id4out_value;

  HWFloat<8,12> id35out_value;

  HWFloat<8,12> id590out_value;

  HWFloat<8,12> id589out_value;

  HWFloat<8,12> id588out_value;

  HWFloat<8,12> id23out_value;

  HWFloat<8,12> id26out_value;

  HWFloat<8,12> id481out_floatOut[2];

  HWRawBits<8> id131out_value;

  HWRawBits<11> id587out_value;

  HWOffsetFix<1,0,UNSIGNED> id529out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id64out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id66out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id66out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id69out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id520out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id100out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id473out_value;

  HWOffsetFix<12,-12,UNSIGNED> id140out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id140sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id85out_value;

  HWOffsetFix<8,-14,UNSIGNED> id521out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id586out_value;

  HWFloat<8,12> id585out_value;

  HWOffsetFix<1,0,UNSIGNED> id523out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id524out_output[3];

  HWFloat<8,12> id584out_value;

  HWOffsetFix<1,0,UNSIGNED> id525out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id526out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id583out_value;

  HWOffsetFix<1,0,UNSIGNED> id110out_value;

  HWOffsetFix<12,-11,UNSIGNED> id94out_value;

  HWOffsetFix<1,0,UNSIGNED> id119out_value;

  HWOffsetFix<8,0,UNSIGNED> id120out_value;

  HWOffsetFix<11,0,UNSIGNED> id122out_value;

  HWFloat<8,12> id439out_value;

  HWFloat<8,12> id582out_value;

  HWFloat<8,12> id581out_value;

  HWFloat<8,12> id3out_value;

  HWFloat<8,12> id362out_result[7];

  HWFloat<8,12> id363out_result[9];

  HWFloat<8,12> id514out_output[6];

  HWOffsetFix<32,0,UNSIGNED> id580out_value;

  HWOffsetFix<1,0,UNSIGNED> id459out_result[2];

  HWFloat<8,12> id534out_output[46];

  HWFloat<8,12> id57out_result[2];

  HWFloat<8,12> id545out_output[11];

  HWFloat<8,12> id563out_output[7];

  HWFloat<8,12> id579out_value;

  HWFloat<8,12> id578out_value;

  HWFloat<8,12> id577out_value;

  HWFloat<8,12> id24out_value;

  HWFloat<8,12> id27out_value;

  HWFloat<8,12> id482out_floatOut[2];

  HWRawBits<8> id338out_value;

  HWRawBits<11> id576out_value;

  HWOffsetFix<1,0,UNSIGNED> id544out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id271out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id273out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id273out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id276out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id535out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id307out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id475out_value;

  HWOffsetFix<12,-12,UNSIGNED> id347out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id347sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id292out_value;

  HWOffsetFix<8,-14,UNSIGNED> id536out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id575out_value;

  HWFloat<8,12> id574out_value;

  HWOffsetFix<1,0,UNSIGNED> id538out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id539out_output[3];

  HWFloat<8,12> id573out_value;

  HWOffsetFix<1,0,UNSIGNED> id540out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id541out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id572out_value;

  HWOffsetFix<1,0,UNSIGNED> id317out_value;

  HWOffsetFix<12,-11,UNSIGNED> id301out_value;

  HWOffsetFix<1,0,UNSIGNED> id326out_value;

  HWOffsetFix<8,0,UNSIGNED> id327out_value;

  HWOffsetFix<11,0,UNSIGNED> id329out_value;

  HWFloat<8,12> id440out_value;

  HWFloat<8,12> id571out_value;

  HWFloat<8,12> id570out_value;

  HWFloat<8,12> id483out_floatOut[2];

  HWFloat<8,12> id6out_value;

  HWFloat<8,12> id7out_value;

  HWFloat<8,12> id364out_result[7];

  HWFloat<8,12> id365out_result[9];

  HWFloat<8,12> id533out_output[5];

  HWFloat<8,12> id382out_result[7];

  HWFloat<8,12> id13out_value;

  HWFloat<8,12> id383out_result[17];

  HWFloat<8,12> id385out_result[2];

  HWFloat<8,12> id387out_result[9];

  HWFloat<8,12> id388out_result[7];

  HWFloat<8,12> id389out_result[9];

  HWOffsetFix<11,0,UNSIGNED> id569out_value;

  HWOffsetFix<11,0,UNSIGNED> id404out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id467out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id406out_io_v_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id568out_value;

  HWOffsetFix<11,0,UNSIGNED> id412out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id468out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id414out_io_w_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id567out_value;

  HWOffsetFix<11,0,UNSIGNED> id420out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id469out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id422out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id430out_value;

  HWOffsetFix<1,0,UNSIGNED> id566out_value;

  HWOffsetFix<49,0,UNSIGNED> id427out_value;

  HWOffsetFix<48,0,UNSIGNED> id428out_count;
  HWOffsetFix<1,0,UNSIGNED> id428out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id428st_count;

  HWOffsetFix<1,0,UNSIGNED> id565out_value;

  HWOffsetFix<49,0,UNSIGNED> id433out_value;

  HWOffsetFix<48,0,UNSIGNED> id434out_count;
  HWOffsetFix<1,0,UNSIGNED> id434out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id434st_count;

  HWOffsetFix<48,0,UNSIGNED> id436out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id470out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,12> c_hw_flt_8_12_bits;
  const HWFloat<8,12> c_hw_flt_8_12_undef;
  const HWFloat<8,12> c_hw_flt_8_12_bits_1;
  const HWFloat<8,12> c_hw_flt_8_12_bits_2;
  const HWFloat<8,12> c_hw_flt_8_12_bits_3;
  const HWFloat<8,12> c_hw_flt_8_12_bits_4;
  const HWFloat<8,12> c_hw_flt_8_12_bits_5;
  const HWFloat<8,12> c_hw_flt_8_12_bits_6;
  const HWFloat<8,12> c_hw_flt_8_12_bits_7;
  const HWFloat<8,12> c_hw_flt_8_12_bits_8;
  const HWFloat<8,12> c_hw_flt_8_12_bits_9;
  const HWFloat<8,12> c_hw_flt_8_12_bits_10;
  const HWFloat<8,12> c_hw_flt_8_12_bits_11;
  const HWFloat<8,12> c_hw_flt_8_12_bits_12;
  const HWFloat<8,12> c_hw_flt_8_12_bits_13;
  const HWFloat<8,12> c_hw_flt_8_12_bits_14;
  const HWFloat<8,12> c_hw_flt_8_12_bits_15;
  const HWFloat<8,12> c_hw_flt_8_12_bits_16;
  const HWFloat<8,12> c_hw_flt_8_12_2_0val;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<12,-12,UNSIGNED> c_hw_fix_12_n12_uns_undef;
  const HWOffsetFix<8,-8,UNSIGNED> c_hw_fix_8_n8_uns_bits;
  const HWOffsetFix<8,-14,UNSIGNED> c_hw_fix_8_n14_uns_undef;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits_1;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWFloat<8,12> c_hw_flt_8_12_bits_17;
  const HWFloat<8,12> c_hw_flt_8_12_bits_18;
  const HWFloat<8,12> c_hw_flt_8_12_bits_19;
  const HWFloat<8,12> c_hw_flt_8_12_bits_20;
  const HWFloat<8,12> c_hw_flt_8_12_bits_21;
  const HWFloat<8,12> c_hw_flt_8_12_bits_22;
  const HWFloat<8,12> c_hw_flt_8_12_bits_23;
  const HWFloat<8,12> c_hw_flt_8_12_bits_24;
  const HWFloat<8,12> c_hw_flt_8_12_bits_25;
  const HWFloat<8,12> c_hw_flt_8_12_0_5val;
  const HWFloat<8,12> c_hw_flt_8_12_bits_26;
  const HWFloat<8,12> c_hw_flt_8_12_bits_27;
  const HWFloat<8,12> c_hw_flt_8_12_bits_28;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
