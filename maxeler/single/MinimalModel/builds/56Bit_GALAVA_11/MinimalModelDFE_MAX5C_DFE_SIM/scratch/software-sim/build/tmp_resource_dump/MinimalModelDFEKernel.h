#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id40out_value;

  HWOffsetFix<11,0,UNSIGNED> id612out_value;

  HWOffsetFix<11,0,UNSIGNED> id541out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id43out_count;
  HWOffsetFix<1,0,UNSIGNED> id43out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id43st_count;

  HWOffsetFix<11,0,UNSIGNED> id654out_value;

  HWOffsetFix<11,0,UNSIGNED> id444out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id489out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id446out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id598out_output[160];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id42out_count;
  HWOffsetFix<1,0,UNSIGNED> id42out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id42st_count;

  HWOffsetFix<32,0,UNSIGNED> id653out_value;

  HWOffsetFix<1,0,UNSIGNED> id490out_result[2];

  HWFloat<11,45> id38out_value;

  HWFloat<11,45> id48out_result[2];

  HWFloat<11,45> id564out_output[10];

  HWFloat<11,45> id602out_output[2];

  HWFloat<11,45> id603out_output[6];

  HWFloat<11,45> id604out_output[16];

  HWFloat<11,45> id605out_output[19];

  HWFloat<11,45> id606out_output[10];

  HWFloat<11,45> id607out_output[48];

  HWFloat<11,45> id608out_output[13];

  HWFloat<11,45> id609out_output[31];

  HWFloat<11,45> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id414out_result[3];

  HWFloat<11,45> id37out_value;

  HWOffsetFix<32,0,UNSIGNED> id652out_value;

  HWOffsetFix<1,0,UNSIGNED> id491out_result[2];

  HWFloat<11,45> id543out_output[132];

  HWFloat<11,45> id51out_result[2];

  HWFloat<11,45> id544out_output[16];

  HWFloat<11,45> id17out_value;

  HWFloat<11,45> id16out_value;

  HWFloat<11,45> id1out_value;

  HWFloat<11,45> id2out_value;

  HWFloat<11,45> id0out_value;

  HWFloat<11,53> id44out_dt;

  HWFloat<11,45> id45out_o[4];

  HWFloat<11,45> id408out_result[16];

  HWFloat<11,45> id409out_result[13];

  HWFloat<11,45> id416out_result[13];

  HWFloat<11,45> id417out_result[16];

  HWFloat<11,45> id29out_value;

  HWFloat<11,45> id418out_result[13];

  HWFloat<11,45> id419out_result[16];

  HWFloat<11,45> id8out_value;

  HWFloat<11,45> id420out_result[50];

  HWFloat<11,45> id421out_result[2];

  HWFloat<11,45> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id422out_result[3];

  HWFloat<11,45> id22out_value;

  HWFloat<11,45> id9out_value;

  HWFloat<11,45> id10out_value;

  HWFloat<11,45> id426out_result[50];

  HWFloat<11,45> id651out_value;

  HWFloat<11,45> id11out_value;

  HWFloat<11,45> id36out_value;

  HWFloat<11,45> id650out_value;

  HWFloat<11,45> id649out_value;

  HWFloat<11,45> id648out_value;

  HWFloat<11,45> id25out_value;

  HWFloat<11,45> id30out_value;

  HWFloat<11,45> id528out_floatOut[2];

  HWRawBits<11> id248out_value;

  HWRawBits<44> id647out_value;

  HWOffsetFix<1,0,UNSIGNED> id560out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id171out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id173out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id173out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id176out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id551out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id217out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id519out_value;

  HWOffsetFix<32,-45,UNSIGNED> id263out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id263sta_rom_store[1024];

  HWOffsetFix<42,-45,UNSIGNED> id260out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id260sta_rom_store[1024];

  HWOffsetFix<45,-45,UNSIGNED> id257out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id257sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id194out_value;

  HWOffsetFix<25,-48,UNSIGNED> id552out_output[3];

  HWOffsetFix<45,-44,UNSIGNED> id646out_value;

  HWFloat<11,45> id645out_value;

  HWOffsetFix<1,0,UNSIGNED> id554out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id555out_output[3];

  HWFloat<11,45> id644out_value;

  HWOffsetFix<1,0,UNSIGNED> id556out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id557out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id643out_value;

  HWOffsetFix<1,0,UNSIGNED> id227out_value;

  HWOffsetFix<45,-44,UNSIGNED> id211out_value;

  HWOffsetFix<1,0,UNSIGNED> id236out_value;

  HWOffsetFix<11,0,UNSIGNED> id237out_value;

  HWOffsetFix<44,0,UNSIGNED> id239out_value;

  HWFloat<11,45> id486out_value;

  HWFloat<11,45> id642out_value;

  HWFloat<11,45> id641out_value;

  HWFloat<11,45> id424out_result[50];

  HWFloat<11,45> id427out_result[2];

  HWFloat<11,45> id434out_result[13];

  HWFloat<11,45> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id428out_result[3];

  HWFloat<11,45> id432out_value;

  HWOffsetFix<32,0,UNSIGNED> id640out_value;

  HWOffsetFix<1,0,UNSIGNED> id499out_result[2];

  HWFloat<11,45> id563out_output[105];

  HWFloat<11,45> id54out_result[2];

  HWFloat<11,45> id567out_output[10];

  HWFloat<11,45> id610out_output[16];

  HWFloat<11,45> id18out_value;

  HWFloat<11,45> id19out_value;

  HWFloat<11,45> id639out_value;

  HWFloat<11,45> id14out_value;

  HWFloat<11,45> id32out_value;

  HWFloat<11,45> id4out_value;

  HWFloat<11,45> id35out_value;

  HWFloat<11,45> id638out_value;

  HWFloat<11,45> id637out_value;

  HWFloat<11,45> id636out_value;

  HWFloat<11,45> id23out_value;

  HWFloat<11,45> id26out_value;

  HWFloat<11,45> id529out_floatOut[2];

  HWRawBits<11> id141out_value;

  HWRawBits<44> id635out_value;

  HWOffsetFix<1,0,UNSIGNED> id577out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id64out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id66out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id66out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id69out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id568out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id110out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id521out_value;

  HWOffsetFix<32,-45,UNSIGNED> id156out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id156sta_rom_store[1024];

  HWOffsetFix<42,-45,UNSIGNED> id153out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id153sta_rom_store[1024];

  HWOffsetFix<45,-45,UNSIGNED> id150out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id150sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id87out_value;

  HWOffsetFix<25,-48,UNSIGNED> id569out_output[3];

  HWOffsetFix<45,-44,UNSIGNED> id634out_value;

  HWFloat<11,45> id633out_value;

  HWOffsetFix<1,0,UNSIGNED> id571out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id572out_output[3];

  HWFloat<11,45> id632out_value;

  HWOffsetFix<1,0,UNSIGNED> id573out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id574out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id631out_value;

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<45,-44,UNSIGNED> id104out_value;

  HWOffsetFix<1,0,UNSIGNED> id129out_value;

  HWOffsetFix<11,0,UNSIGNED> id130out_value;

  HWOffsetFix<44,0,UNSIGNED> id132out_value;

  HWFloat<11,45> id487out_value;

  HWFloat<11,45> id630out_value;

  HWFloat<11,45> id629out_value;

  HWFloat<11,45> id3out_value;

  HWFloat<11,45> id410out_result[16];

  HWFloat<11,45> id411out_result[13];

  HWFloat<11,45> id562out_output[19];

  HWOffsetFix<32,0,UNSIGNED> id628out_value;

  HWOffsetFix<1,0,UNSIGNED> id507out_result[2];

  HWFloat<11,45> id582out_output[105];

  HWFloat<11,45> id57out_result[2];

  HWFloat<11,45> id593out_output[11];

  HWFloat<11,45> id611out_output[16];

  HWFloat<11,45> id627out_value;

  HWFloat<11,45> id626out_value;

  HWFloat<11,45> id625out_value;

  HWFloat<11,45> id24out_value;

  HWFloat<11,45> id27out_value;

  HWFloat<11,45> id530out_floatOut[2];

  HWRawBits<11> id380out_value;

  HWRawBits<44> id624out_value;

  HWOffsetFix<1,0,UNSIGNED> id592out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id303out_value;

  HWOffsetFix<61,-48,TWOSCOMPLEMENT> id305out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id305out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id308out_value;

  HWOffsetFix<13,0,TWOSCOMPLEMENT> id583out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id349out_value;

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id523out_value;

  HWOffsetFix<32,-45,UNSIGNED> id395out_dout[3];

  HWOffsetFix<32,-45,UNSIGNED> id395sta_rom_store[1024];

  HWOffsetFix<42,-45,UNSIGNED> id392out_dout[3];

  HWOffsetFix<42,-45,UNSIGNED> id392sta_rom_store[1024];

  HWOffsetFix<45,-45,UNSIGNED> id389out_dout[3];

  HWOffsetFix<45,-45,UNSIGNED> id389sta_rom_store[8];

  HWOffsetFix<21,-21,UNSIGNED> id326out_value;

  HWOffsetFix<25,-48,UNSIGNED> id584out_output[3];

  HWOffsetFix<45,-44,UNSIGNED> id623out_value;

  HWFloat<11,45> id622out_value;

  HWOffsetFix<1,0,UNSIGNED> id586out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id587out_output[3];

  HWFloat<11,45> id621out_value;

  HWOffsetFix<1,0,UNSIGNED> id588out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id589out_output[3];

  HWOffsetFix<14,0,TWOSCOMPLEMENT> id620out_value;

  HWOffsetFix<1,0,UNSIGNED> id359out_value;

  HWOffsetFix<45,-44,UNSIGNED> id343out_value;

  HWOffsetFix<1,0,UNSIGNED> id368out_value;

  HWOffsetFix<11,0,UNSIGNED> id369out_value;

  HWOffsetFix<44,0,UNSIGNED> id371out_value;

  HWFloat<11,45> id488out_value;

  HWFloat<11,45> id619out_value;

  HWFloat<11,45> id618out_value;

  HWFloat<11,45> id531out_floatOut[2];

  HWFloat<11,45> id6out_value;

  HWFloat<11,45> id7out_value;

  HWFloat<11,45> id412out_result[16];

  HWFloat<11,45> id413out_result[13];

  HWFloat<11,45> id581out_output[18];

  HWFloat<11,45> id430out_result[16];

  HWFloat<11,45> id13out_value;

  HWFloat<11,45> id431out_result[50];

  HWFloat<11,45> id433out_result[2];

  HWFloat<11,45> id435out_result[13];

  HWFloat<11,45> id436out_result[16];

  HWFloat<11,45> id437out_result[13];

  HWOffsetFix<11,0,UNSIGNED> id617out_value;

  HWOffsetFix<11,0,UNSIGNED> id452out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id515out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id454out_io_v_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id616out_value;

  HWOffsetFix<11,0,UNSIGNED> id460out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id516out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id462out_io_w_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id615out_value;

  HWOffsetFix<11,0,UNSIGNED> id468out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id517out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id470out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id478out_value;

  HWOffsetFix<1,0,UNSIGNED> id614out_value;

  HWOffsetFix<49,0,UNSIGNED> id475out_value;

  HWOffsetFix<48,0,UNSIGNED> id476out_count;
  HWOffsetFix<1,0,UNSIGNED> id476out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id476st_count;

  HWOffsetFix<1,0,UNSIGNED> id613out_value;

  HWOffsetFix<49,0,UNSIGNED> id481out_value;

  HWOffsetFix<48,0,UNSIGNED> id482out_count;
  HWOffsetFix<1,0,UNSIGNED> id482out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id482st_count;

  HWOffsetFix<48,0,UNSIGNED> id484out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id518out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits;
  const HWFloat<11,45> c_hw_flt_11_45_undef;
  const HWFloat<11,45> c_hw_flt_11_45_bits_1;
  const HWFloat<11,45> c_hw_flt_11_45_bits_2;
  const HWFloat<11,45> c_hw_flt_11_45_bits_3;
  const HWFloat<11,45> c_hw_flt_11_45_bits_4;
  const HWFloat<11,45> c_hw_flt_11_45_bits_5;
  const HWFloat<11,45> c_hw_flt_11_45_bits_6;
  const HWFloat<11,45> c_hw_flt_11_45_bits_7;
  const HWFloat<11,45> c_hw_flt_11_45_bits_8;
  const HWFloat<11,45> c_hw_flt_11_45_bits_9;
  const HWFloat<11,45> c_hw_flt_11_45_bits_10;
  const HWFloat<11,45> c_hw_flt_11_45_bits_11;
  const HWFloat<11,45> c_hw_flt_11_45_bits_12;
  const HWFloat<11,45> c_hw_flt_11_45_bits_13;
  const HWFloat<11,45> c_hw_flt_11_45_bits_14;
  const HWFloat<11,45> c_hw_flt_11_45_bits_15;
  const HWFloat<11,45> c_hw_flt_11_45_bits_16;
  const HWFloat<11,45> c_hw_flt_11_45_2_0val;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<44> c_hw_bit_44_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<13,0,TWOSCOMPLEMENT> c_hw_fix_13_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_1;
  const HWOffsetFix<32,-45,UNSIGNED> c_hw_fix_32_n45_uns_undef;
  const HWOffsetFix<42,-45,UNSIGNED> c_hw_fix_42_n45_uns_undef;
  const HWOffsetFix<45,-45,UNSIGNED> c_hw_fix_45_n45_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<25,-48,UNSIGNED> c_hw_fix_25_n48_uns_undef;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_2;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_undef;
  const HWOffsetFix<14,0,TWOSCOMPLEMENT> c_hw_fix_14_0_sgn_bits_3;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_bits_1;
  const HWOffsetFix<45,-44,UNSIGNED> c_hw_fix_45_n44_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWOffsetFix<44,0,UNSIGNED> c_hw_fix_44_0_uns_bits;
  const HWFloat<11,45> c_hw_flt_11_45_bits_17;
  const HWFloat<11,45> c_hw_flt_11_45_bits_18;
  const HWFloat<11,45> c_hw_flt_11_45_bits_19;
  const HWFloat<11,45> c_hw_flt_11_45_bits_20;
  const HWFloat<11,45> c_hw_flt_11_45_bits_21;
  const HWFloat<11,45> c_hw_flt_11_45_bits_22;
  const HWFloat<11,45> c_hw_flt_11_45_bits_23;
  const HWFloat<11,45> c_hw_flt_11_45_bits_24;
  const HWFloat<11,45> c_hw_flt_11_45_bits_25;
  const HWFloat<11,45> c_hw_flt_11_45_0_5val;
  const HWFloat<11,45> c_hw_flt_11_45_bits_26;
  const HWFloat<11,45> c_hw_flt_11_45_bits_27;
  const HWFloat<11,45> c_hw_flt_11_45_bits_28;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
