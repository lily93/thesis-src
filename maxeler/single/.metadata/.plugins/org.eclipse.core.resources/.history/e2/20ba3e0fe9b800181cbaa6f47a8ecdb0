package karma.model.cell;

import com.maxeler.maxblox.funceval.MathPow;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Optimization.PipelinedOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.RoundingMode;
import com.maxeler.maxcompiler.v2.kernelcompiler.op_management.MathOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEFix;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEFix.SignMode;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEFloat;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFERawBits;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;
import com.maxeler.maxcompiler.v2.utils.MathUtils;

public class KarmaKernel extends Kernel {
	static final DFEFloat scalarType = dfeFloat(11, 53);
	 static final DFEFloat calculationType = dfeFloat(11, 45);
		
	 final DFEVar TAUE =constant.var(calculationType, 2.5);
	 final DFEVar TAUN   = constant.var(calculationType, 250);
	 final DFEVar ONE_O_TAUE =constant.var(calculationType, 0.4);
	 final DFEVar ONE_O_TAUN   = constant.var(calculationType, 0.004);
	 final DFEVar EH =constant.var(calculationType,3);
	 final DFEVar EN = constant.var(calculationType, 1);
	 final DFEVar ESTAR = constant.var(calculationType, 0.8);  //1.5415
	 final DFEVar EPS = constant.var(calculationType,0.01); //TAUE/TAUN
	 final DFEVar RE = constant.var(calculationType, 1.204);              //VARIED BETWEEN 0.5 AND 1.4
	 final DFEVar EXPRE = constant.var(calculationType,  0.299991841);        //EXP(-RE)
	 final DFEVar M  = constant.var(calculationType,  10);
	 final DFEVar GAMMA    = constant.var(calculationType, 0.0011);
	 final DFEVar ONE_O_ONE_MINUS_EXPRE   = constant.var(calculationType, 1.428554778);


	public KarmaKernel(final KernelParameters parameters) {
		super(parameters);

		// Input
		//create autoloop offset to create backwards edge for calculation
				OffsetExpr loopLength = stream.makeOffsetAutoLoop("loopLength"); //
				DFEVar loopLengthVal = loopLength.getDFEVar(this, dfeUInt(11));





				CounterChain chain = control.count.makeCounterChain();
				final DFEVar nx =io.scalarInput("num_iteration", dfeUInt(32));

				DFEVar step = chain.addCounter(nx, 1);//counter for number of steps;
										 //
				DFEVar loopCounter = chain.addCounter(loopLengthVal, 1); //counter for validation of values;
				DFEVar dtIn = io.scalarInput("dt", dfeFloat(11,53));
				DFEVar dt = dtIn.cast(calculationType);
				DFEVar carriedU = calculationType.newInstance(this);
				DFEVar carriedV = calculationType.newInstance(this);
		 	  DFEVar u = step===0? 1.5 : carriedU;
		 	  DFEVar v = step===0? 0 : carriedV;
		 	  carriedU.simWatch("carriedU");
		 	// optimization.pushEnableSaturatingArithmetic(true);
		 	  optimization.pushPipeliningFactor(0, PipelinedOps.ALL);
		    DFEVar  dfunc = pow(v, 10);
		    DFEVar  rfunc = 1.428554778 - v;
		 
		    DFEVar  hfunc = (1-tanh(u-EH)) * u* u * 0.5;
		  		   DFEVar   ffunc = -u + (ESTAR-dfunc) * hfunc;
	      DFEVar h = (u > EN).cast(calculationType);
		      //(a -n)*h  - (1-h)*n
		      //a*h -n*h  - (n - n*h)
		    DFEVar  gfunc = 1.428554778*h - (v); 
		    DFEVar  de    = ffunc * ONE_O_TAUE;
		     DFEVar dn    = gfunc * ONE_O_TAUN;
		      
	          DFEVar uNext = u + dt*de;
	         DFEVar vNext  = v + dt*dn;
	         optimization.popPipeliningFactor(PipelinedOps.ALL);
	        // optimization.popEnableSaturatingArithmetic();
	         
	     	DFEVar uOffset = stream.offset(uNext, -loopLength);
			DFEVar vOffset = stream.offset(vNext, -loopLength);

			carriedU<== uOffset;
			carriedV<==vOffset;
			
			//optimization.pushEnableSaturatingArithmetic(true);
			DFEVar uout = uNext.cast(scalarType);
			DFEVar vout = vNext.cast(scalarType);
			//optimization.popEnableSaturatingArithmetic();
	     	io.output("u_out", uout, scalarType,loopCounter === (loopLengthVal-1));	
	    	io.output("v_out", vout, scalarType,loopCounter === (loopLengthVal-1));
	
	}
	
	
	
	DFEVar pow (DFEVar a, int b) {
		DFEVar temp;
		
		if (b == 0)
	        return constant.var(calculationType, 1);

	    temp = pow(a, (b / 2));
	    if ((b % 2) == 0)
	        return temp * temp;
	    else
	        return a * temp * temp;
	}

	
	protected DFEVar tanh(DFEVar x) {
		//tangens hyperbolicus: 1-(2/(e^(2*x)+1))
			optimization.pushPipeliningFactor(0, PipelinedOps.ALL);
			optimization.pushEnableSaturatingArithmetic(true);
			x = (2*x);
			DFEVar v = KernelMath.exp(x);
			
			DFEVar approximation = 1- (2/(v+1));
			optimization.popPipeliningFactor(PipelinedOps.ALL);
			optimization.popEnableSaturatingArithmetic();
		
			return approximation.cast(calculationType);

	}

}