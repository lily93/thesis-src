package beelerreuter.model.cell;

import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Optimization.PipelinedOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.statemachine.statements.SimPrintf;


public class BeelerReuterKernel extends Kernel {
	 static final DFEType scalarType = dfeFloat(11, 53);
	 static final DFEType calculationType = dfeFloat(11, 53);
	 static final DFEVectorType<DFEVar> vectorType =
			  new DFEVectorType<DFEVar>(calculationType, 8);
	 
	
	//Constants
	 
	final DFEVar EPI_TVP = constant.var(calculationType, 1.4506);

final DFEVar GS = constant.var(calculationType,  0.09);
final DFEVar	ENA = constant.var(calculationType, 50);
final DFEVar	GNAC = constant.var(calculationType, 0.003);
final DFEVar	GNA = constant.var(calculationType , 4);


	final DFEVar Zero = constant.var(calculationType, 0.0);
	final DFEVar One = constant.var(calculationType, 1.0);



	
	 
	 
	BeelerReuterKernel(KernelParameters parameters) {
		super(parameters);

		//time given by cpu







		//create autoloop offset to create backwards edge for calculation
		OffsetExpr loopLength = stream.makeOffsetAutoLoop("loopLength"); //
		DFEVar loopLengthVal = loopLength.getDFEVar(this, dfeUInt(11));





		CounterChain chain = control.count.makeCounterChain();
		final DFEVar nx =io.scalarInput("num_iteration", dfeUInt(32));

		DFEVar step = chain.addCounter(nx, 1);//counter for number of steps;
								 //
		DFEVar loopCounter = chain.addCounter(loopLengthVal, 1); //counter for validation of values;
		DFEVar dt = io.scalarInput("dt", scalarType);
		dt = dt.cast(calculationType);
		
	//	DFEVar stim_in = io.input("stim_in",scalarType,loopCounter === (loopLengthVal-1));
		DFEVector<DFEVar> valueVector = vectorType.newInstance(this);
		DFEVar currentU = step===0 ? io.scalarInput("u_in", calculationType): valueVector[0];
		DFEVar currentCa = step===0 ? io.scalarInput("ca_in", calculationType) : valueVector[1];
		DFEVar currentX1 = step===0 ? io.scalarInput("x1_in", calculationType): valueVector[2];
		DFEVar currentM = step===0? io.scalarInput("m_in", calculationType) :valueVector[3];
		DFEVar currentF = step===0? io.scalarInput("f_in", calculationType): valueVector[4];
		DFEVar currentH = step===0? io.scalarInput("h_in", calculationType) : valueVector [5];
		DFEVar currentD = step===0 ? io.scalarInput("d_in", calculationType): valueVector[6];
		DFEVar currentJ = step===0? io.scalarInput("j_in", calculationType) :valueVector[7];

		optimization.pushPipeliningFactor(0, PipelinedOps.ALL);

		 DFEVar ax1 = dt *((5.0*Math.pow(10,-4)) * KernelMath.exp(0.083 * ((currentU) + 50.0)) / (KernelMath.exp(0.057 * ((currentU) + 50.0)) + 1.0));
		  DFEVar        bx1 =  dt * (0.0013 * KernelMath.exp(-0.06 * ((currentU) + 20.0)) / (KernelMath.exp(-0.04 * ((currentU) + 20.0)) + 1.0));
		          
		  DFEVar        am = dt* (-((currentU) + 47.0) / (KernelMath.exp(-0.1 * ((currentU) + 47.0)) - 1.0));
		  DFEVar        bm = dt *(40.0 * KernelMath.exp(-0.056 * ((currentU) + 72.0)));
		          
		  DFEVar        ah = dt * (0.126 * KernelMath.exp(-0.25 * ((currentU) + 77.0)));
		  DFEVar        bh = dt *(1.7 / (KernelMath.exp(-0.082 * ((currentU) + 22.5)) + 1.0));
		          
		  DFEVar        aj = dt * (0.055 * KernelMath.exp(-0.25 * ((currentU) + 78.0)) / (KernelMath.exp(-0.2 * ((currentU) + 78.0)) + 1.0));
		  DFEVar        bj = dt * (0.3 / (KernelMath.exp(-0.1 * ((currentU) + 32.0)) + 1.0));
		          
		  DFEVar        ad = dt * (0.095 * KernelMath.exp(-0.01 * ((currentU) - 5.0)) / (KernelMath.exp(-0.072 * ((currentU) - 5.0)) + 1.0));
		  DFEVar        bd =dt * (0.07 * KernelMath.exp(-0.017 * ((currentU) + 44.0)) / (KernelMath.exp(0.05 * ((currentU) + 44.0)) + 1.0));
		          
		  DFEVar        af = dt * (0.012 * KernelMath.exp(-0.008 * ((currentU) + 28.0)) / (KernelMath.exp(0.15 * ((currentU) + 28.0)) + 1.0));
		  DFEVar        bf = dt *(0.0065 * KernelMath.exp(-0.02 * ((currentU) + 30.0)) / (KernelMath.exp(-0.2 * ((currentU) + 30.0)) + 1.0));
		 
		          currentX1 = (currentX1) + (ax1 * (1.0 - (currentX1)) - bx1 * (currentX1));
		          currentM = (currentM) + (am * (1.0 - (currentM)) - bm * (currentM));
		          currentH =  (currentH)+ (ah * (1.0 - (currentH)) - bh * (currentH));
		          currentJ = (currentJ) + (aj * (1.0 - (currentJ)) - bj * (currentJ));
		          currentD = (currentD) + (ad * (1.0 - (currentD)) - bd * (currentD));
		          currentF = (currentF) + (af * (1.0 - (currentF)) - bf * (currentF));
		          

		         DFEVar es = (-82.3 - 13.0287 * KernelMath.log((currentCa), calculationType));
		         DFEVar is  =0.09*(currentD)*(currentF)*((currentU)-es);
		          currentCa = currentCa +dt*(-0.0000001*is+0.07*(0.0000001-(currentCa)));

		     DFEVar  xik1 = (KernelMath.exp(0.08 * ((currentU) + 53.0)) + KernelMath.exp(0.04 * ((currentU) + 53.0)));
		   DFEVar       xik2 = (1.0 - KernelMath.exp(-0.04 * ((currentU) + 23.0)));
		   xik1= (xik1 === Zero)? 0.001: xik1;
		   xik2 = (xik2 ===Zero)?  0.001: xik2;
		   DFEVar       ik1 = (0.35 * (4.0 * (KernelMath.exp(0.04 * ((currentU) + 85.0)) - 1.0) / xik1 + 0.2 * ((currentU) + 23.0) / xik2));
		  
		        DFEVar  gix1 = (0.8 * (KernelMath.exp(0.04 * ((currentU) + 77.0)) - 1.0) /KernelMath.exp(0.04 * ((currentU) + 35.0)));
		          DFEVar ix1 = (gix1 * (currentX1));
		          DFEVar ina = ((GNA * (currentM) * (currentM) * (currentM) * (currentH) * (currentJ) + GNAC) * ((currentU) - ENA));
		          currentU = currentU -( dt * (ik1 + ix1 + ina + is));
		          optimization.popPipeliningFactor(PipelinedOps.ALL);    
		          DFEVar uOffset = stream.offset(currentU, -loopLength);
		          DFEVar caOffset = stream.offset(currentCa, -loopLength);
		          DFEVar jOffset = stream.offset(currentJ, -loopLength);
		          DFEVar hOffset = stream.offset(currentH, -loopLength);
		          DFEVar x1Offset = stream.offset(currentX1, -loopLength);
		          DFEVar fOffset = stream.offset(currentF, -loopLength);
		          DFEVar mOffset = stream.offset(currentM, -loopLength);
		          DFEVar dOffset = stream.offset(currentD, -loopLength);
		          
		          valueVector[0] <== uOffset;
		          valueVector[1] <== caOffset;
		          valueVector[2] <==x1Offset;
		          valueVector[3] <== mOffset;
		          valueVector[4] <== fOffset;
		          valueVector[5] <== hOffset;
		          valueVector[6] <==dOffset;
		          valueVector[7] <==jOffset;	
		        
		          
		          
		      	io.output("u_out", currentU.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("ca_out", currentCa.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("x1_out", currentX1.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("m_out", currentM.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("f_out", currentF.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("h_out", currentH.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("d_out", currentD.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		      	io.output("j_out", currentJ.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
	}

	


	/***
	 *
	 * @param x value to check
	 * @return heaviside(x) returns the value 0 for x < 0, 1 for x > 0, and 1/2 for x = 0.
	 */

	protected DFEVar heavisidefun(DFEVar x) {

//		//check if the value is below or above zero
		DFEVar c = x<0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
//
//		//check if the value is zero
		DFEVar z = x===0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
//
//		//assign values regarding to checks - first if value is below or above zero
		DFEVar ret = c?constant.var(scalarType, 0.0) : constant.var(scalarType, 1.0);
//
//		//if x is zero, z is true, as c would not consider this case, we can disregard the value of c if z is true
		ret = z? constant.var(scalarType, 0.5) : ret;

	
		return ret;




	}


	protected DFEVar tanh(DFEVar x) {
		//tangens hyperbolicus: 1-(2/(e^(2*x)+1))

			DFEVar v = KernelMath.exp(2*x);
			DFEVar approximation = 1- (2/(v+1));
			return approximation;

	}





}