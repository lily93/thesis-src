#include "stdsimheader.h"

namespace maxcompilersim {

void MinimalModel64DFEKernel::execute0() {
  { // Node ID: 456 (NodeConstantRawBits)
  }
  { // Node ID: 466 (NodeConstantRawBits)
  }
  { // Node ID: 453 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 454 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id454in_enable = id466out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id454in_max = id453out_value;

    HWOffsetFix<49,0,UNSIGNED> id454x_1;
    HWOffsetFix<1,0,UNSIGNED> id454x_2;
    HWOffsetFix<1,0,UNSIGNED> id454x_3;
    HWOffsetFix<49,0,UNSIGNED> id454x_4t_1e_1;

    id454out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id454st_count)));
    (id454x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id454st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id454x_2) = (gte_fixed((id454x_1),id454in_max));
    (id454x_3) = (and_fixed((id454x_2),id454in_enable));
    id454out_wrap = (id454x_3);
    if((id454in_enable.getValueAsBool())) {
      if(((id454x_3).getValueAsBool())) {
        (id454st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id454x_4t_1e_1) = (id454x_1);
        (id454st_count) = (id454x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id455out_output;

  { // Node ID: 455 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id455in_input = id454out_count;

    id455out_output = id455in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 457 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id457in_load = id456out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id457in_data = id455out_output;

    bool id457x_1;

    (id457x_1) = ((id457in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id457x_1)) {
      setMappedRegValue("current_run_cycle_count", id457in_data);
    }
  }
  { // Node ID: 465 (NodeConstantRawBits)
  }
  { // Node ID: 459 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 460 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id460in_enable = id465out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id460in_max = id459out_value;

    HWOffsetFix<49,0,UNSIGNED> id460x_1;
    HWOffsetFix<1,0,UNSIGNED> id460x_2;
    HWOffsetFix<1,0,UNSIGNED> id460x_3;
    HWOffsetFix<49,0,UNSIGNED> id460x_4t_1e_1;

    id460out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id460st_count)));
    (id460x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id460st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id460x_2) = (gte_fixed((id460x_1),id460in_max));
    (id460x_3) = (and_fixed((id460x_2),id460in_enable));
    id460out_wrap = (id460x_3);
    if((id460in_enable.getValueAsBool())) {
      if(((id460x_3).getValueAsBool())) {
        (id460st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id460x_4t_1e_1) = (id460x_1);
        (id460st_count) = (id460x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 462 (NodeInputMappedReg)
  }
  { // Node ID: 464 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id464in_a = id460out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id464in_b = id462out_run_cycle_count;

    id464out_result[(getCycle()+1)%2] = (eq_fixed(id464in_a,id464in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 461 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id461in_start = id464out_result[getCycle()%2];

    if((id461in_start.getValueAsBool())) {
      startFlushing();
    }
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
};

};
