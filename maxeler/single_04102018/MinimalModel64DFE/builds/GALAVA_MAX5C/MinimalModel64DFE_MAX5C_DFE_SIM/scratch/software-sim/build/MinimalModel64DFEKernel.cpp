#include "stdsimheader.h"
#include "MinimalModel64DFEKernel.h"

namespace maxcompilersim {

MinimalModel64DFEKernel::MinimalModel64DFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 5, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 456 (NodeConstantRawBits)
    id456out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 466 (NodeConstantRawBits)
    id466out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 453 (NodeConstantRawBits)
    id453out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 457 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 465 (NodeConstantRawBits)
    id465out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 459 (NodeConstantRawBits)
    id459out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 462 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
  { // Node ID: 41 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 44 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
}

void MinimalModel64DFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void MinimalModel64DFEKernel::resetComputationAfterFlush() {
  { // Node ID: 454 (NodeCounter)

    (id454st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 460 (NodeCounter)

    (id460st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 462 (NodeInputMappedReg)
    id462out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
}

void MinimalModel64DFEKernel::updateState() {
  { // Node ID: 462 (NodeInputMappedReg)
    id462out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
}

void MinimalModel64DFEKernel::preExecute() {
}

void MinimalModel64DFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "MinimalModel64DFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int MinimalModel64DFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
