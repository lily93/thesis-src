#ifndef MINIMALMODEL64DFEKERNEL_H_
#define MINIMALMODEL64DFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModel64DFEKernel : public KernelManagerBlockSync {
public:
  MinimalModel64DFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  HWOffsetFix<1,0,UNSIGNED> id456out_value;

  HWOffsetFix<1,0,UNSIGNED> id466out_value;

  HWOffsetFix<49,0,UNSIGNED> id453out_value;

  HWOffsetFix<48,0,UNSIGNED> id454out_count;
  HWOffsetFix<1,0,UNSIGNED> id454out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id454st_count;

  HWOffsetFix<1,0,UNSIGNED> id465out_value;

  HWOffsetFix<49,0,UNSIGNED> id459out_value;

  HWOffsetFix<48,0,UNSIGNED> id460out_count;
  HWOffsetFix<1,0,UNSIGNED> id460out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id460st_count;

  HWOffsetFix<48,0,UNSIGNED> id462out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id464out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWFloat<11,53> id44out_dt;

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODEL64DFEKERNEL_H_ */
