#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  HWOffsetFix<1,0,UNSIGNED> id398out_value;

  HWOffsetFix<1,0,UNSIGNED> id408out_value;

  HWOffsetFix<49,0,UNSIGNED> id395out_value;

  HWOffsetFix<48,0,UNSIGNED> id396out_count;
  HWOffsetFix<1,0,UNSIGNED> id396out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id396st_count;

  HWOffsetFix<1,0,UNSIGNED> id407out_value;

  HWOffsetFix<49,0,UNSIGNED> id401out_value;

  HWOffsetFix<48,0,UNSIGNED> id402out_count;
  HWOffsetFix<1,0,UNSIGNED> id402out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id402st_count;

  HWOffsetFix<48,0,UNSIGNED> id404out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id406out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWFloat<11,53> id44out_dt;

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
