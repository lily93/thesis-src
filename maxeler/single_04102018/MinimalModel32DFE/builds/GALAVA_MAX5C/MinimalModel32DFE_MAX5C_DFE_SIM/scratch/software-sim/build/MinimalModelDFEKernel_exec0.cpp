#include "stdsimheader.h"

namespace maxcompilersim {

void MinimalModelDFEKernel::execute0() {
  { // Node ID: 398 (NodeConstantRawBits)
  }
  { // Node ID: 408 (NodeConstantRawBits)
  }
  { // Node ID: 395 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 396 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id396in_enable = id408out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id396in_max = id395out_value;

    HWOffsetFix<49,0,UNSIGNED> id396x_1;
    HWOffsetFix<1,0,UNSIGNED> id396x_2;
    HWOffsetFix<1,0,UNSIGNED> id396x_3;
    HWOffsetFix<49,0,UNSIGNED> id396x_4t_1e_1;

    id396out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id396st_count)));
    (id396x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id396st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id396x_2) = (gte_fixed((id396x_1),id396in_max));
    (id396x_3) = (and_fixed((id396x_2),id396in_enable));
    id396out_wrap = (id396x_3);
    if((id396in_enable.getValueAsBool())) {
      if(((id396x_3).getValueAsBool())) {
        (id396st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id396x_4t_1e_1) = (id396x_1);
        (id396st_count) = (id396x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id397out_output;

  { // Node ID: 397 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id397in_input = id396out_count;

    id397out_output = id397in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 399 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_load = id398out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id399in_data = id397out_output;

    bool id399x_1;

    (id399x_1) = ((id399in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id399x_1)) {
      setMappedRegValue("current_run_cycle_count", id399in_data);
    }
  }
  { // Node ID: 407 (NodeConstantRawBits)
  }
  { // Node ID: 401 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 402 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_enable = id407out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id402in_max = id401out_value;

    HWOffsetFix<49,0,UNSIGNED> id402x_1;
    HWOffsetFix<1,0,UNSIGNED> id402x_2;
    HWOffsetFix<1,0,UNSIGNED> id402x_3;
    HWOffsetFix<49,0,UNSIGNED> id402x_4t_1e_1;

    id402out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id402st_count)));
    (id402x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id402st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id402x_2) = (gte_fixed((id402x_1),id402in_max));
    (id402x_3) = (and_fixed((id402x_2),id402in_enable));
    id402out_wrap = (id402x_3);
    if((id402in_enable.getValueAsBool())) {
      if(((id402x_3).getValueAsBool())) {
        (id402st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id402x_4t_1e_1) = (id402x_1);
        (id402st_count) = (id402x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 404 (NodeInputMappedReg)
  }
  { // Node ID: 406 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id406in_a = id402out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id406in_b = id404out_run_cycle_count;

    id406out_result[(getCycle()+1)%2] = (eq_fixed(id406in_a,id406in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 403 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_start = id406out_result[getCycle()%2];

    if((id403in_start.getValueAsBool())) {
      startFlushing();
    }
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
};

};
