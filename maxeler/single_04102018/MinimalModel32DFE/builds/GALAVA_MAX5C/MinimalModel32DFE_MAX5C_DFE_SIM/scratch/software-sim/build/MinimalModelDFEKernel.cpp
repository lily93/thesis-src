#include "stdsimheader.h"
#include "MinimalModelDFEKernel.h"

namespace maxcompilersim {

MinimalModelDFEKernel::MinimalModelDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 5, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 398 (NodeConstantRawBits)
    id398out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 408 (NodeConstantRawBits)
    id408out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 395 (NodeConstantRawBits)
    id395out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 399 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 407 (NodeConstantRawBits)
    id407out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 401 (NodeConstantRawBits)
    id401out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 404 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
  { // Node ID: 41 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 44 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
}

void MinimalModelDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void MinimalModelDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 396 (NodeCounter)

    (id396st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 402 (NodeCounter)

    (id402st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 404 (NodeInputMappedReg)
    id404out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
}

void MinimalModelDFEKernel::updateState() {
  { // Node ID: 404 (NodeInputMappedReg)
    id404out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
}

void MinimalModelDFEKernel::preExecute() {
}

void MinimalModelDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "MinimalModelDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int MinimalModelDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
