#include "Maxfiles.h" 			// Includes .max files
#include <MaxSLiCInterface.h>	// Simple Live CPU interface
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>

#define NEXT_LINE(fp) while(getc(fp)!='\n');

 double dt = 0.0;
 int simTime = 0;
int num_iteration;

void init(float * u, float * x1, float * m, float * h, float * j, float * d, float *f, float * ca) {
      *u = -84.5737;
      *ca = 0.0000001;
      float ax1 = dt* ((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
     float bx1 = dt* (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));

      float am = dt *(-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
    float  bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));

    float  ah = dt*(0.126 * exp(-0.25 * ((*u) + 77.0)));
   float   bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));

     float aj = dt *(0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
     float bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));

    float  ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
    float  bd = dt *(0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));

    float  af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
    float  bf = dt * (0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));


   *x1 = (ax1 / (ax1 + bx1));
    *m = (am / (am + bm));
     *h = (ah / (ah + bh));
      *j = (aj / (aj + bj));
      *d = (ad / (ad + bd));
      *f = (af / (af + bf));
      printf("u: %g\nca: %g\nx1: %f\nm: %f\nh: %f\nj: %f\nf: %f\nd: %f\n", *u, *ca, *x1 ,*m, *h, *j, *f, *d);

}

void print(float * u_out, float  * ca_out, float * x1_out, float * h_out , float * f_out, float * d_out, float * m_out, float * j_out) {
	char filename[1024];
	sprintf(filename, "beelerreuter_out_maxeler_%f_%d_32.csv", dt ,simTime);

	if( access(filename, F_OK) != -1) {
			remove(filename);
		}
	FILE *fp = fopen(filename, "w");

		if(fp==NULL) {
			printf("no file found");
		}
		else {
			printf("u;v;w;s;stim\n");

			//fprintf(fp,"u;v;w;s;stim\n");
			for (int i = 0; i<num_iteration; i++) {

	printf("u: %g\n", u_out[i]);//, ca_out[i], x1_out[i], f_out[i], d_out[i], h_out[i], m_out[i], j_out[i]);
	fprintf(fp,"%g;%g;%g;%g;%g\;%g;%g;%g\n", u_out[i], ca_out[i], x1_out[i], f_out[i], d_out[i], h_out[i], m_out[i], j_out[i]);

}
			fclose(fp);
			fp = NULL;
	}
}


void runBeelerReuterDFE(float * u_out) {

	int  sizeBytes = (num_iteration) * sizeof(float);
printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
//float * u_out = malloc(sizeBytes);
float * ca_out = malloc(sizeBytes);
float * x1_out = malloc(sizeBytes);
float * h_out =  malloc(sizeBytes);
float * f_out = malloc(sizeBytes);
float * d_out = malloc(sizeBytes);
float * m_out = malloc(sizeBytes);
float * j_out = malloc(sizeBytes);

float u, ca, f, x1, d,h,m,j;

init(&u, &x1, &m ,&h, &j, &d, &f, &ca);
u = -50;
ca = 0.0000001;

struct timeval start, end;
		long mtime, seconds, useconds;


		max_file_t *myMaxFile = BeelerReuterDFE_init();
		max_engine_t *myDFE = max_load(myMaxFile, "local:*");


		BeelerReuterDFE_actions_t actions;

		actions.param_length = num_iteration;
		actions.param_dt = dt;

		actions.param_u_in = u;
		actions.param_ca_in = ca;
		actions.param_f_in = f;
		actions.param_d_in = d;
		actions.param_x1_in = x1;
		actions.param_m_in = m;
		actions.param_j_in = j;
		actions.param_h_in = h;

		actions.param_num_iteration = num_iteration;
		actions.outstream_u_out = u_out;
		actions.outstream_ca_out = ca_out;
		actions.outstream_x1_out = x1_out;
		actions.outstream_h_out = h_out;
		actions.outstream_d_out = d_out;
		actions.outstream_f_out = f_out;
		actions.outstream_m_out = m_out;
		actions.outstream_j_out = j_out;

		printf("Running DFE\n");
					gettimeofday(&start, NULL);

			BeelerReuterDFE_run(myDFE, &actions);
			//memcpy(us, u_out, sizeBytes);
			gettimeofday(&end, NULL);
				  seconds = end.tv_sec - start.tv_sec;
				  		useconds = end.tv_usec - start.tv_usec;

				  		mtime = ((seconds) * 1000 + useconds / 1000.0);
			printf("DFE elapsed time: %ld milliseconds \n", mtime);
			printf("Printing Data from DFE\n");

			print(u_out, ca_out, x1_out,h_out,f_out,d_out,m_out,j_out);

			free(ca_out);
			ca_out = NULL;
			free(x1_out);
			x1_out = NULL;
			free(h_out);
			h_out = NULL;
			free(f_out);
			f_out = NULL;
			free(d_out);
			d_out = NULL;
			free(m_out);
			m_out = NULL;
			free(j_out);
			j_out = NULL;
			max_unload(myDFE);



}


void initParam(char * params) {

	  FILE *fp;

	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.025;
		  simTime = 1000;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%f",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %f\n", dt);

	   fclose(fp);

	  }
	  num_iteration = (int)(simTime/dt);


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %f\n", dt);



}
float least_squares(float * u, float * un, int size, float dt1, float dt2) {
	float lse = 0;


	for(int i = 0; i<(size+1); i++) {
	lse += pow(((u[i]*dt1) - (un[i*2]* dt2 + un[(i*2)+1] * dt2)),2);
	//printf("#%d ERROR: %.15f\n", (i+1), lse);

	}
	//lse = lse/size;
	printf("LSE dt: %f vs dt/2: %f: %g\n", dt1, dt2, (lse/size));
	return lse;




}
void step_size_estimation_test() {
	char * errorfile = "error_br_maxeler.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");


	char * params = "params.dat";
	initParam(params);
	int num_iteration_old = num_iteration;
	int sizeBytes = num_iteration * sizeof(float);
	float * us_old = malloc(sizeBytes);

	runBeelerReuterDFE(us_old);
	int frac = 2;
	float dt_old = dt;
	dt = dt/frac;
	float err = 10;
	fclose(fp);
	while(dt>=0.000781) {


		num_iteration = (int)floor(simTime/dt + 0.5);
		sizeBytes = num_iteration * sizeof(float);
		float * us = malloc(sizeBytes);
		printf("RUNNING TEST.. DT (MS): %.20f \n", dt);
		runBeelerReuterDFE(us);
		err = least_squares(us_old, us, num_iteration_old, dt_old, dt);
		printf("DT(MS): %g\n ERROR: %g\n---------------------------------------\n", dt, (err/num_iteration_old));
		FILE *fp = fopen(errorfile, "a");
		fprintf(fp,"%g\n", (err/num_iteration_old));
		fclose(fp);
		err = (err/num_iteration_old);
		us_old = realloc(us_old,sizeBytes);
		if(us_old==NULL) {
			printf("memory allocation error");
			exit(1);
		}
		us_old = us;
		dt_old = dt;
		num_iteration_old = num_iteration;
		dt = dt/2;




	}





}


void test1() {
	dt = 0.1/64;
	simTime = 1000;
	num_iteration = (int)(simTime/dt);
	int sizeBytes = num_iteration * sizeof(float);
	float * u_out = malloc(sizeBytes);
	runBeelerReuterDFE(u_out);
}

int main(void) {

test1();
	return 0;
}
