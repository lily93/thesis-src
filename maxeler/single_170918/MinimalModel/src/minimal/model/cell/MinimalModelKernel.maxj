package minimal.model.cell;

import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Optimization.PipelinedOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.op_management.MathOps;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;

public class MinimalModelKernel extends Kernel {
	
	static final DFEType scalarType = dfeFloat(8, 24);
	 static final DFEType calculationType = dfeFloat(8, 24);
	 static final DFEVectorType<DFEVar> vectorType =
			  new DFEVectorType<DFEVar>(calculationType, 4);
	 
	
	//Constants
	 DFEVar TVP = constant.var(calculationType,      1.4506); //The same with Flavio's paper
	 DFEVar TV1M  =  constant.var(calculationType,  60)    ;//The same with Flavio's paper
	 DFEVar TV2M  =  constant.var(calculationType,  1150)  ;//The same with Flavio's paper

	 DFEVar TWP   =constant.var(calculationType,    200.0);   // We have TWP1 and TWP2 = TWP in Flavio's paper

	 //#define TW1P     200.0
	 //#define TW2P     200.0

	 DFEVar TW1M =   constant.var(calculationType,   60.0);    //The same with Flavio's paper
	 DFEVar TW2M =constant.var(calculationType,      15);     //The same with Flavio's paper
	 DFEVar TS1 = constant.var(calculationType,       2.7342);  //The same with Flavio's paper
	 DFEVar TS2 =  constant.var(calculationType,    16);     //The same with Flavio's paper
	 DFEVar TFI = constant.var(calculationType,      0.11);    //The same with Flavio's paper
	 DFEVar TO1 =  constant.var(calculationType,      400);    //The same with Flavio's paper
	 DFEVar TO2 =    constant.var(calculationType,  6);      //The same with Flavio's paper
	 DFEVar TSO1 =   constant.var(calculationType,  30.0181); //The same with Flavio's paper
	 DFEVar TSO2 =   constant.var(calculationType,  0.9957);  //The same with Flavio's paper

	 DFEVar TSI  =    constant.var(calculationType, 1.8875 ); // We have TSI1 and TSI2 = TSI in Flavio's paper
	 //#define TSI1     1.8875
	 //#define TSI2     1.8875


	 DFEVar TWINF  =  constant.var(calculationType, 0.07);    //The same with Flavio's paper
	 DFEVar THV  =    constant.var(calculationType, 0.3);     //EPUM // The same of Flavio's paper
	 DFEVar THVM  =    constant.var(calculationType, 0.006);   //EPUQ // The same of Flavio's paper
	 DFEVar THVINF  = constant.var(calculationType, 0.006);   //EPUQ // The same of Flavio's paper
	 DFEVar THW   =   constant.var(calculationType, 0.13);    //EPUP // The same of Flavio's paper
	 DFEVar THWINF  = constant.var(calculationType, 0.006);    //EPURR // In Flavio's paper 0.13
	 DFEVar THSO =   constant.var(calculationType,  0.13 );   //EPUP // The same of Flavio's paper
	 DFEVar THSI =    constant.var(calculationType,  0.13 );   //EPUP // The same of Flavio's paper
	 DFEVar THO =    constant.var(calculationType,  0.006);    //EPURR // The same of Flavio's paper
	 //#define KWP      5.7
	 DFEVar KWM  =    constant.var(calculationType, 65);    //The same of Flavio's paper
	 DFEVar KS  =    constant.var(calculationType,  2.0994);  //The same of Flavio's paper
	 DFEVar KSO =    constant.var(calculationType,  2.0458);  //The same of Flavio's paper
	 //#define KSI      97.8
	 DFEVar UWM  =    constant.var(calculationType, 0.03);    //The same of Flavio's paper
	 DFEVar US  =    constant.var(calculationType,  0.9087);  //The same of Flavio's paper
	 DFEVar UO =     constant.var(calculationType,  0);     // The same of Flavio's paper
	 DFEVar UU =     constant.var(calculationType,  1.55);   // The same of Flavio's paper
	 DFEVar USO  =     constant.var(calculationType, 0.65);   // The same of Flavio's paper
	 DFEVar SC  =    constant.var(calculationType,  0.007);
	 //#define WCP      0.15

	 DFEVar WINFSTAR =  constant.var(calculationType, 0.94);   // The same of Flavio's paper

	 DFEVar TW2M_TW1M =  constant.var(calculationType, -45.0);
	 DFEVar TSO2_TSO1 = constant.var(calculationType, -29.0224);

	 DFEVar TW2M_TW1M_DIVBY2 = constant.var(calculationType, -22.5);
	 DFEVar TSO2_TSO1_DIVBY2 = constant.var(calculationType, -14.5112);


	 
//	final DFEVar  TVP = constant.var(calculationType, 1.4506);
//	final DFEVar  TV1M = constant.var(calculationType, 60);
//	final DFEVar  TV2M = constant.var(calculationType, 1150);
//	final DFEVar  TWP = constant.var(calculationType, 200);
//	final DFEVar  TW1M = constant.var(calculationType, 60);
//	final DFEVar  TW2M = constant.var(calculationType, 15);
//	final DFEVar  TS1 = constant.var(calculationType, 2.7342);
//	final DFEVar  TS2 = constant.var(calculationType, 16);
//	final DFEVar  TFI = constant.var(calculationType, 0.11);
//	final DFEVar  TO1 = constant.var(calculationType, 400);
//	final DFEVar  TO2 = constant.var(calculationType, 6);
//	final DFEVar  TSO1 = constant.var(calculationType, 30.0181);
//	final DFEVar  TSO2 = constant.var(calculationType, 0.9957);
//	final DFEVar  TSI = constant.var(calculationType, 1.8875);
//	final DFEVar  TWINF = constant.var(calculationType, 0.07);
//	final DFEVar  THV = constant.var(calculationType, 0.3);
//	final DFEVar  THVM = constant.var(calculationType, 0.006);
//	final DFEVar  THVINF = constant.var(calculationType, 0.006);
//	final DFEVar  THW = constant.var(calculationType, 0.13);
//	final DFEVar  THSO = constant.var(calculationType, 0.006);
//	final DFEVar  THSI = constant.var(calculationType, 0.13);
//	final DFEVar  THO = constant.var(calculationType, 0.006);
//	final DFEVar  KWM = constant.var(calculationType, 65);
//	final DFEVar  KS = constant.var(calculationType, 2.0994);
//	final DFEVar  KSO = constant.var(calculationType, 2.0458);
//	final DFEVar  UWM = constant.var(calculationType, 0.03);
//	final DFEVar  US = constant.var(calculationType, 0.9087);
//	final DFEVar  U0 = constant.var(calculationType, 0);
//	final DFEVar  UU = constant.var(calculationType, 1.55);
//	final DFEVar  USO = constant.var(calculationType, 0.65);
//	final DFEVar TW2M_TW1M_DIVBY2 = constant.var(calculationType,-22.5);
//	final DFEVar TSO2_TSO1_DIVBY2 = constant.var(calculationType,-14.5112);
//	final DFEVar  WINFSTAR = constant.var(calculationType, 0.94); 
//	final DFEVar   THWINF = constant.var(calculationType,   0.006);
//
	final DFEVar Zero = constant.var(calculationType, 0.0);
	final DFEVar One = constant.var(calculationType, 1.0);




	public MinimalModelKernel(final KernelParameters parameters) {
		super(parameters);

		 
		


			//time given by cpu







			//create autoloop offset to create backwards edge for calculation
			OffsetExpr loopLength = stream.makeOffsetAutoLoop("loopLength"); //
			DFEVar loopLengthVal = loopLength.getDFEVar(this, dfeUInt(11));





			CounterChain chain = control.count.makeCounterChain();
			final DFEVar nx =io.scalarInput("num_iteration", dfeUInt(32));

			DFEVar step = chain.addCounter(nx, 1);//counter for number of steps;
									 //
			DFEVar loopCounter = chain.addCounter(loopLengthVal, 1); //counter for validation of values;
			DFEVar dt = io.scalarInput("dt", dfeFloat(11,53));
			dt = dt.cast(calculationType);
		//	DFEVar stim_in = io.input("stim_in",scalarType,loopCounter === (loopLengthVal-1));

			DFEVector<DFEVar> statevars = vectorType.newInstance(this);
			 DFEVar ui = step===0? One : statevars[0]; //initCondition? 0.0 : carriedU;
		     DFEVar    vi = step===0? One : statevars[1];
		     DFEVar   wi = step===0 ? One : statevars[2];
		     DFEVar     si = step===0? Zero : statevars[3];
			  DFEVar tvm, vinf, winf;
			  DFEVar jfi, jso, jsi;
			  DFEVar twm, tso, ts, to, ds, dw, dv;

			  optimization.pushPipeliningFactor(0.0,PipelinedOps.ALL);
			//  optimization.pushDSPFactor(0.0, MathOps.ALL);
			  tvm =  (ui >   THVM) ?   TV2M :   TV1M;
		      twm =    TW1M + (  TW2M_TW1M_DIVBY2)*(1+tanh( KWM*(ui-  UWM)));
		      tso =     TSO1 + (  TSO2_TSO1_DIVBY2)*(1+tanh(   KSO*(ui-   USO)));
		      ts  = (ui >    THW)  ?    TS2  :    TS1;
		      to  = (ui >    THO)  ?    TO2  :    TO1;

		      vinf = (ui >     THVINF) ? Zero : One;
			  winf = (ui >     THWINF) ?   WINFSTAR: (1.0-ui/   TWINF);
		      winf =  (winf > One) ? One : winf;

		      dv = (ui >    THV) ? -vi/   TVP : (vinf-vi)/tvm;
		      dw = (ui >    THW) ? -wi/   TWP : (winf-wi)/twm;
		      ds = (((1+tanh(   KS*(ui-   US)))/2) - si)/ts;

		      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
		      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
		      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




			  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
			  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
			  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
			  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
			  //if (winf >one_o_twm) winf = one_o_twm;
			  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
			  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
			  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;

		      optimization.popPipeliningFactor(PipelinedOps.ALL);



			  //Update gates

		       vi = vi +dv*dt;
		       wi = wi + dw*dt;
		       si = si+ ds*dt;

			  //Compute currents
		      jfi = (ui >    THV)  ? -vi * (ui -    THV) * (   UU - ui)/   TFI : Zero;
		      /*if (*ui >   THV){
		         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
		         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
		         	  }
		           }*/
		      jso = (ui >    THSO) ? 1/tso : (ui-   UO)/to;
		      jsi = (ui >    THSI) ? -wi * si/   TSI : 0.0;

		      ui = ui  - (jfi+jso+jsi)*dt;
		     // optimization.popDSPFactor();
		      
		      
		  	DFEVar uOffset = stream.offset(ui, -loopLength);
			DFEVar vOffset = stream.offset(vi, -loopLength);
			DFEVar wOffset = stream.offset(wi, -loopLength);
			DFEVar sOffset = stream.offset(si, -loopLength);
		
		//	DFEVar stimOffset = stream.offset(stim, -loopLength);

	        statevars[0] <== uOffset;
	        statevars[1] <==vOffset;
	        statevars[2] <== wOffset;
	        statevars[3] <== sOffset;
		
			io.output("u_out", ui.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
			io.output("v_out", statevars[1].cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
			io.output("w_out", statevars[2].cast(scalarType), scalarType,loopCounter === (loopLengthVal-1)) ;
			io.output("s_out", statevars[3].cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
		
			
		

		}

//		void old_cstep(DFEVar step, DFEVar dt, DFEVar loopLengthVal, DFEVector<DFEVar> valueVector, OffsetExpr loopLength, DFEVar loopCounter) {
//			DFEVar initCondition = (step===0);
//
//
//
//
//			
//			//init variables needed;
//
//			 DFEVar u = initCondition? One : valueVector[0]; //initCondition? 0.0 : carriedU;
//		     DFEVar    v = initCondition? One : valueVector[1];
//		     DFEVar   w = initCondition ? One : valueVector[2];
//		     DFEVar     s = initCondition? Zero : valueVector[3];
//		  //   DFEVar    t1 = initCondition? 0 : carriedT1;
//		    // DFEVar   t2 = initCondition? 0 : carriedT2;
//		     
//		   
//		     //define Conditions for calculation;
//
//				DFEVar calcCondition1 = u<0.006;
//				DFEVar calcCondition2 = u<0.13;
//				DFEVar calcCondition3 = u<0.3;
//				DFEVar calcDefault = u>0.3;
//
//				//Define current values;
//				DFEVar wCurr;
//				DFEVar uCurr;
//				DFEVar sCurr;
//				DFEVar vCurr;
//				DFEVar t1Curr;
//				DFEVar t2Curr;
//
//
//
//				//jfi; jso; jsi
//
//				DFEVar jfi;
//	            DFEVar jso;
//	            DFEVar jsi;
//	            // stim
//	           // DFEVar stim = stim_in; //(loopCounter === (loopLengthVal-1))? stim_in : carriedStim;
//
//
//			//calculation stimulus
//			 //t1Curr = t1 + dt;
//			 //t2Curr = t2 + dt;
////			 debug.simPrintf("#%d t1: %f\n",step,t1);
////			 debug.simPrintf("#%d t2: %f\n", step, t2);
//			 optimization.pushDSPFactor(1.0);
//			 optimization.pushPipeliningFactor(0.0, PipelinedOps.ALL);
//		//	DFEVar flag = heavisidefun(t1Curr - 0)*(1 - heavisidefun(t2Curr - 1)) + heavisidefun(t1Curr - 300)*(1 - heavisidefun(t2Curr - 301)) + heavisidefun(t1Curr - 700)*(1 - heavisidefun(t2Curr - 701));
//			//flag.simWatch("flag");
//			optimization.popPipeliningFactor(PipelinedOps.ALL);
//			//calculation
//			//dt = dt.cast(calculationType);
//			//DFEVar stim = flag===1? One : Zero;
//	optimization.pushPipeliningFactor(0.0, PipelinedOps.ALL);
//
//			 wCurr = calcCondition1 ? (w + ((1.0 -(u/ TWINF) - w)/( TW1M + ( TW2M -  TW1M) * 0.5 * (1+tanh( KWM*(u- UWM)))))*dt):
//				 	 calcCondition2 ? w + ((0.94-w)/( TW1M + ( TW2M -  TW1M) * 0.5 * (1+tanh( KWM*(u- UWM)))))*dt :
//				 	 calcCondition3 ? w + (-w/ TWP)*dt:
//				 		 			  w + (-w/ TWP)*dt;
//
//			 vCurr = calcCondition1 ? (v + ((1.0-v)/ TV1M)*dt): //u<0.006
//				 	 calcCondition2 ? (v + (-v/ TV2M)*dt) : //elseif u<0.13
//					 calcCondition3 ?( v + (-v/ TV2M)*dt): //elseif u<0.3
//						 			   v + (-v/ TVP)*dt; //else
//
//
//
//			 sCurr = calcCondition1 ? (s + ((((1+tanh( KS*(u -  US))) * 0.5) - s)/ TS1)*dt):
//				 	 calcCondition2 ? (s +((((1+tanh( KS*(u- US))) * 0.5) - s)/ TS1)*dt):
//				 	 calcCondition3 ? (s + ((((1.+tanh( KS*(u- US))) * 0.5) - s)/ TS2)*dt):
//				 		 			   s +((((1.+tanh( KS*(u -  US))) * 0.5) - s)/ TS2)*dt;
//
//
//			 jfi = calcCondition1 ? Zero :
//	        	   calcCondition2 ? Zero :
//	        	   calcCondition3 ? Zero :
//	        		  				(-v * (u -  THV) * ( UU - u)/ TFI);
//
//	         jso = calcCondition1 ? u/ TO1 :
//	        	   calcCondition2 ? u/ TO2 :
//	        	   calcCondition3 ? (1/( TSO1+(( TSO2- TSO1)*(1.+tanh( KSO*(u -  USO)))) * 0.5)) :
//	        		   				(1/( TSO1+(( TSO2 -  TSO1)*(1+tanh( KSO*(u -  USO)))) * 0.5));
//
//	         jsi = calcCondition1 ? Zero :
//	        	   calcCondition2 ? Zero :
//	        	   calcCondition3 ? (-w * s/ TSI) :
//	        		   				(-w * s/ TSI);
//
//
//	        	   optimization.popPipeliningFactor(PipelinedOps.ALL);
//	        	   optimization.popDSPFactor();
//
//
//	          //calculate u
//
//	          uCurr = u  - (jfi+jso+jsi)*dt; //substract stim if needed
//	          
//	  
//
//
//			//generate offset for backward edge
//			//DFEVar t1Offset = stream.offset(t1Curr, -loopLength);
//			//DFEVar t2Offset = stream.offset(t2Curr, -loopLength);
//			DFEVar uOffset = stream.offset(uCurr, -loopLength);
//			DFEVar vOffset = stream.offset(vCurr, -loopLength);
//			DFEVar wOffset = stream.offset(wCurr, -loopLength);
//			DFEVar sOffset = stream.offset(sCurr, -loopLength);
//		
//		//	DFEVar stimOffset = stream.offset(stim, -loopLength);
//
//	        valueVector[0] <== uOffset;
//	        valueVector[1] <==vOffset;
//	        valueVector[2] <== wOffset;
//	        valueVector[3] <== sOffset;
//
//
//			// At the foot of the loop, we add the backward edge
//			//carriedT1 <== t1Offset;
//			//carriedT2 <== t2Offset;
////			carriedU <== uOffset;
////			carriedV <== vOffset;
////			carriedW <== wOffset;
////			carriedS <== sOffset;
//		//	carriedStim <==stimOffset;
//
//
//			//stim.simWatch("stim");
//			io.output("u_out", uCurr.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
//			io.output("v_out", vCurr.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
//			io.output("w_out", wCurr.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1)) ;
//			io.output("s_out", sCurr.cast(scalarType), scalarType,loopCounter === (loopLengthVal-1));
//			//io.output("stim_out", stim, calculationType, loopCounter === (loopLengthVal-1));
//			//io.scalarOutput("alive", dfeUInt(32))<==step;
//
//		}


		/***
		 *
		 * @param x value to check
		 * @return heaviside(x) returns the value 0 for x < 0, 1 for x > 0, and 1/2 for x = 0.
		 */

		protected DFEVar heavisidefun(DFEVar x) {

//			//check if the value is below or above zero
			DFEVar c = x<0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
	//
//			//check if the value is zero
			DFEVar z = x===0.0? constant.var(dfeBool(), 1) : constant.var(dfeBool(), 0);
	//
//			//assign values regarding to checks - first if value is below or above zero
			DFEVar ret = c?constant.var(scalarType, 0.0) : constant.var(scalarType, 1.0);
	//
//			//if x is zero, z is true, as c would not consider this case, we can disregard the value of c if z is true
			ret = z? constant.var(scalarType, 0.5) : ret;

		
			return ret;




		}

		DFEVector<DFEVar> cstep_epi(OffsetExpr loopLength, DFEVector<DFEVar> statevars, DFEVar step, DFEVar dt, DFEVar stim) {
			 DFEVar ui = step===0? One : statevars[0]; //initCondition? 0.0 : carriedU;
		     DFEVar    vi = step===0? One : statevars[1];
		     DFEVar   wi = step===0 ? One : statevars[2];
		     DFEVar     si = step===0? Zero : statevars[3];
			  DFEVar tvm, vinf, winf;
			  DFEVar jfi, jso, jsi;
			  DFEVar twm, tso, ts, to, ds, dw, dv;

			  optimization.pushPipeliningFactor(0,PipelinedOps.ALL);
			  optimization.pushDSPFactor(0, MathOps.ALL);
			  tvm =  (ui >    THVM) ?    TV2M :    TV1M;
		      twm =     TW1M + (  TW2M_TW1M_DIVBY2)*(1+tanh(   KWM*(ui-   UWM)));
		      tso =     TSO1 + (  TSO2_TSO1_DIVBY2)*(1+tanh(   KSO*(ui-   USO)));
		      ts  = (ui >    THW)  ?    TS2  :    TS1;
		      to  = (ui >    THO)  ?    TO2  :    TO1;

		      vinf = (ui >     THVINF) ? Zero : One;
			  winf = (ui >     THWINF) ?   WINFSTAR: (1.0-ui/   TWINF);
		      winf =  (winf > One) ? One : winf;

		      dv = (ui >    THV) ? -vi/   TVP : (vinf-vi)/tvm;
		      dw = (ui >    THW) ? -wi/   TWP : (winf-wi)/twm;
		      ds = (((1.+tanh(   KS*(ui-   US)))/2.) - si)/ts;

		      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
		      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
		      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




			  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
			  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
			  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
			  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
			  //if (winf >one_o_twm) winf = one_o_twm;
			  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
			  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
			  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;





			  //Update gates

		       vi += dv*dt;
		       wi += dw*dt;
		       si += ds*dt;

			  //Compute currents
		      jfi = (ui >    THV)  ? -vi * (ui -    THV) * (   UU - ui)/   TFI : Zero;
		      /*if (*ui >   THV){
		         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
		         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
		         	  }
		           }*/
		      jso = (ui >    THSO) ? 1/tso : (ui-   UO)/to;
		      jsi = (ui >    THSI) ? -wi * si/   TSI : 0.0;

		      ui = ui  - (jfi+jso+jsi-stim)*dt;
		      optimization.popDSPFactor();
		      optimization.popPipeliningFactor(PipelinedOps.ALL);
		      
		  	DFEVar uOffset = stream.offset(ui, -loopLength);
			DFEVar vOffset = stream.offset(vi, -loopLength);
			DFEVar wOffset = stream.offset(wi, -loopLength);
			DFEVar sOffset = stream.offset(si, -loopLength);
		
		//	DFEVar stimOffset = stream.offset(stim, -loopLength);

	        statevars[0] <== uOffset;
	        statevars[1] <==vOffset;
	        statevars[2] <== wOffset;
	        statevars[3] <== sOffset;

			     return statevars;
		      
		}

		protected DFEVar tanh(DFEVar x) {
			//tangens hyperbolicus: 1-(2/(e^(2*x)+1))

				DFEVar v = KernelMath.exp(2*x);
				DFEVar approximation = 1- (2/(v+1));
				return approximation;

		}





	}