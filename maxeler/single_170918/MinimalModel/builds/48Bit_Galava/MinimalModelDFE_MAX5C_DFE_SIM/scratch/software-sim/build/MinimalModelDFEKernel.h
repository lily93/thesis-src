#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id40out_value;

  HWOffsetFix<11,0,UNSIGNED> id618out_value;

  HWOffsetFix<11,0,UNSIGNED> id43out_count;
  HWOffsetFix<1,0,UNSIGNED> id43out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id43st_count;

  HWOffsetFix<11,0,UNSIGNED> id541out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id660out_value;

  HWOffsetFix<11,0,UNSIGNED> id444out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id489out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id446out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id598out_output[145];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id42out_count;
  HWOffsetFix<1,0,UNSIGNED> id42out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id42st_count;

  HWOffsetFix<32,0,UNSIGNED> id659out_value;

  HWOffsetFix<1,0,UNSIGNED> id490out_result[2];

  HWFloat<8,40> id38out_value;

  HWFloat<8,40> id48out_result[2];

  HWFloat<8,40> id564out_output[10];

  HWFloat<8,40> id605out_output[2];

  HWFloat<8,40> id606out_output[3];

  HWFloat<8,40> id607out_output[13];

  HWFloat<8,40> id608out_output[16];

  HWFloat<8,40> id609out_output[10];

  HWFloat<8,40> id610out_output[43];

  HWFloat<8,40> id611out_output[13];

  HWFloat<8,40> id612out_output[28];

  HWFloat<8,40> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id414out_result[3];

  HWFloat<8,40> id37out_value;

  HWOffsetFix<32,0,UNSIGNED> id658out_value;

  HWOffsetFix<1,0,UNSIGNED> id491out_result[2];

  HWFloat<8,40> id600out_output[116];

  HWFloat<8,40> id613out_output[3];

  HWFloat<8,40> id51out_result[2];

  HWFloat<8,40> id544out_output[13];

  HWFloat<8,40> id17out_value;

  HWFloat<8,40> id16out_value;

  HWFloat<8,40> id1out_value;

  HWFloat<8,40> id2out_value;

  HWFloat<8,40> id0out_value;

  HWFloat<11,53> id44out_dt;

  HWFloat<8,40> id45out_o[4];

  HWFloat<8,40> id408out_result[13];

  HWFloat<8,40> id409out_result[13];

  HWFloat<8,40> id416out_result[13];

  HWFloat<8,40> id417out_result[13];

  HWFloat<8,40> id29out_value;

  HWFloat<8,40> id418out_result[13];

  HWFloat<8,40> id419out_result[13];

  HWFloat<8,40> id8out_value;

  HWFloat<8,40> id420out_result[45];

  HWFloat<8,40> id421out_result[2];

  HWFloat<8,40> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id422out_result[3];

  HWFloat<8,40> id22out_value;

  HWFloat<8,40> id9out_value;

  HWFloat<8,40> id10out_value;

  HWFloat<8,40> id426out_result[45];

  HWFloat<8,40> id657out_value;

  HWFloat<8,40> id11out_value;

  HWFloat<8,40> id36out_value;

  HWFloat<8,40> id656out_value;

  HWFloat<8,40> id655out_value;

  HWFloat<8,40> id654out_value;

  HWFloat<8,40> id25out_value;

  HWFloat<8,40> id30out_value;

  HWFloat<8,40> id528out_floatOut[2];

  HWRawBits<8> id248out_value;

  HWRawBits<39> id653out_value;

  HWOffsetFix<1,0,UNSIGNED> id560out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id171out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id173out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id173out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id176out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id551out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id217out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id519out_value;

  HWOffsetFix<29,-40,UNSIGNED> id263out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id263sta_rom_store[1024];

  HWOffsetFix<39,-40,UNSIGNED> id260out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id260sta_rom_store[1024];

  HWOffsetFix<40,-40,UNSIGNED> id257out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id257sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id194out_value;

  HWOffsetFix<22,-43,UNSIGNED> id552out_output[3];

  HWOffsetFix<40,-39,UNSIGNED> id652out_value;

  HWFloat<8,40> id651out_value;

  HWOffsetFix<1,0,UNSIGNED> id554out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id555out_output[3];

  HWFloat<8,40> id650out_value;

  HWOffsetFix<1,0,UNSIGNED> id556out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id557out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id649out_value;

  HWOffsetFix<1,0,UNSIGNED> id227out_value;

  HWOffsetFix<40,-39,UNSIGNED> id211out_value;

  HWOffsetFix<1,0,UNSIGNED> id236out_value;

  HWOffsetFix<8,0,UNSIGNED> id237out_value;

  HWOffsetFix<39,0,UNSIGNED> id239out_value;

  HWFloat<8,40> id486out_value;

  HWFloat<8,40> id648out_value;

  HWFloat<8,40> id647out_value;

  HWFloat<8,40> id424out_result[45];

  HWFloat<8,40> id427out_result[2];

  HWFloat<8,40> id434out_result[13];

  HWFloat<8,40> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id428out_result[3];

  HWFloat<8,40> id432out_value;

  HWOffsetFix<32,0,UNSIGNED> id646out_value;

  HWOffsetFix<1,0,UNSIGNED> id499out_result[2];

  HWFloat<8,40> id602out_output[92];

  HWFloat<8,40> id614out_output[3];

  HWFloat<8,40> id54out_result[2];

  HWFloat<8,40> id567out_output[10];

  HWFloat<8,40> id615out_output[13];

  HWFloat<8,40> id18out_value;

  HWFloat<8,40> id19out_value;

  HWFloat<8,40> id645out_value;

  HWFloat<8,40> id14out_value;

  HWFloat<8,40> id32out_value;

  HWFloat<8,40> id4out_value;

  HWFloat<8,40> id35out_value;

  HWFloat<8,40> id644out_value;

  HWFloat<8,40> id643out_value;

  HWFloat<8,40> id642out_value;

  HWFloat<8,40> id23out_value;

  HWFloat<8,40> id26out_value;

  HWFloat<8,40> id529out_floatOut[2];

  HWRawBits<8> id141out_value;

  HWRawBits<39> id641out_value;

  HWOffsetFix<1,0,UNSIGNED> id577out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id64out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id66out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id66out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id69out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id568out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id110out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id521out_value;

  HWOffsetFix<29,-40,UNSIGNED> id156out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id156sta_rom_store[1024];

  HWOffsetFix<39,-40,UNSIGNED> id153out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id153sta_rom_store[1024];

  HWOffsetFix<40,-40,UNSIGNED> id150out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id150sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id87out_value;

  HWOffsetFix<22,-43,UNSIGNED> id569out_output[3];

  HWOffsetFix<40,-39,UNSIGNED> id640out_value;

  HWFloat<8,40> id639out_value;

  HWOffsetFix<1,0,UNSIGNED> id571out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id572out_output[3];

  HWFloat<8,40> id638out_value;

  HWOffsetFix<1,0,UNSIGNED> id573out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id574out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id637out_value;

  HWOffsetFix<1,0,UNSIGNED> id120out_value;

  HWOffsetFix<40,-39,UNSIGNED> id104out_value;

  HWOffsetFix<1,0,UNSIGNED> id129out_value;

  HWOffsetFix<8,0,UNSIGNED> id130out_value;

  HWOffsetFix<39,0,UNSIGNED> id132out_value;

  HWFloat<8,40> id487out_value;

  HWFloat<8,40> id636out_value;

  HWFloat<8,40> id635out_value;

  HWFloat<8,40> id3out_value;

  HWFloat<8,40> id410out_result[13];

  HWFloat<8,40> id411out_result[13];

  HWFloat<8,40> id562out_output[16];

  HWOffsetFix<32,0,UNSIGNED> id634out_value;

  HWOffsetFix<1,0,UNSIGNED> id507out_result[2];

  HWFloat<8,40> id604out_output[92];

  HWFloat<8,40> id616out_output[3];

  HWFloat<8,40> id57out_result[2];

  HWFloat<8,40> id593out_output[11];

  HWFloat<8,40> id617out_output[13];

  HWFloat<8,40> id633out_value;

  HWFloat<8,40> id632out_value;

  HWFloat<8,40> id631out_value;

  HWFloat<8,40> id24out_value;

  HWFloat<8,40> id27out_value;

  HWFloat<8,40> id530out_floatOut[2];

  HWRawBits<8> id380out_value;

  HWRawBits<39> id630out_value;

  HWOffsetFix<1,0,UNSIGNED> id592out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id303out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id305out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id305out_o_doubt[7];

  HWOffsetFix<52,-51,UNSIGNED> id308out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id583out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id349out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id523out_value;

  HWOffsetFix<29,-40,UNSIGNED> id395out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id395sta_rom_store[1024];

  HWOffsetFix<39,-40,UNSIGNED> id392out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id392sta_rom_store[1024];

  HWOffsetFix<40,-40,UNSIGNED> id389out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id389sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id326out_value;

  HWOffsetFix<22,-43,UNSIGNED> id584out_output[3];

  HWOffsetFix<40,-39,UNSIGNED> id629out_value;

  HWFloat<8,40> id628out_value;

  HWOffsetFix<1,0,UNSIGNED> id586out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id587out_output[3];

  HWFloat<8,40> id627out_value;

  HWOffsetFix<1,0,UNSIGNED> id588out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id589out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id626out_value;

  HWOffsetFix<1,0,UNSIGNED> id359out_value;

  HWOffsetFix<40,-39,UNSIGNED> id343out_value;

  HWOffsetFix<1,0,UNSIGNED> id368out_value;

  HWOffsetFix<8,0,UNSIGNED> id369out_value;

  HWOffsetFix<39,0,UNSIGNED> id371out_value;

  HWFloat<8,40> id488out_value;

  HWFloat<8,40> id625out_value;

  HWFloat<8,40> id624out_value;

  HWFloat<8,40> id531out_floatOut[2];

  HWFloat<8,40> id6out_value;

  HWFloat<8,40> id7out_value;

  HWFloat<8,40> id412out_result[13];

  HWFloat<8,40> id413out_result[13];

  HWFloat<8,40> id581out_output[15];

  HWFloat<8,40> id430out_result[13];

  HWFloat<8,40> id13out_value;

  HWFloat<8,40> id431out_result[45];

  HWFloat<8,40> id433out_result[2];

  HWFloat<8,40> id435out_result[13];

  HWFloat<8,40> id436out_result[13];

  HWFloat<8,40> id437out_result[13];

  HWFloat<8,24> id442out_o[4];

  HWOffsetFix<11,0,UNSIGNED> id623out_value;

  HWOffsetFix<11,0,UNSIGNED> id452out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id515out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id454out_io_v_out_force_disabled;

  HWFloat<8,24> id450out_o[4];

  HWOffsetFix<11,0,UNSIGNED> id622out_value;

  HWOffsetFix<11,0,UNSIGNED> id460out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id516out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id462out_io_w_out_force_disabled;

  HWFloat<8,24> id458out_o[4];

  HWOffsetFix<11,0,UNSIGNED> id621out_value;

  HWOffsetFix<11,0,UNSIGNED> id468out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id517out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id470out_io_s_out_force_disabled;

  HWFloat<8,24> id466out_o[4];

  HWOffsetFix<1,0,UNSIGNED> id478out_value;

  HWOffsetFix<1,0,UNSIGNED> id620out_value;

  HWOffsetFix<49,0,UNSIGNED> id475out_value;

  HWOffsetFix<48,0,UNSIGNED> id476out_count;
  HWOffsetFix<1,0,UNSIGNED> id476out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id476st_count;

  HWOffsetFix<1,0,UNSIGNED> id619out_value;

  HWOffsetFix<49,0,UNSIGNED> id481out_value;

  HWOffsetFix<48,0,UNSIGNED> id482out_count;
  HWOffsetFix<1,0,UNSIGNED> id482out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id482st_count;

  HWOffsetFix<48,0,UNSIGNED> id484out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id518out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits;
  const HWFloat<8,40> c_hw_flt_8_40_undef;
  const HWFloat<8,40> c_hw_flt_8_40_bits_1;
  const HWFloat<8,40> c_hw_flt_8_40_bits_2;
  const HWFloat<8,40> c_hw_flt_8_40_bits_3;
  const HWFloat<8,40> c_hw_flt_8_40_bits_4;
  const HWFloat<8,40> c_hw_flt_8_40_bits_5;
  const HWFloat<8,40> c_hw_flt_8_40_bits_6;
  const HWFloat<8,40> c_hw_flt_8_40_bits_7;
  const HWFloat<8,40> c_hw_flt_8_40_bits_8;
  const HWFloat<8,40> c_hw_flt_8_40_bits_9;
  const HWFloat<8,40> c_hw_flt_8_40_bits_10;
  const HWFloat<8,40> c_hw_flt_8_40_bits_11;
  const HWFloat<8,40> c_hw_flt_8_40_bits_12;
  const HWFloat<8,40> c_hw_flt_8_40_bits_13;
  const HWFloat<8,40> c_hw_flt_8_40_bits_14;
  const HWFloat<8,40> c_hw_flt_8_40_bits_15;
  const HWFloat<8,40> c_hw_flt_8_40_bits_16;
  const HWFloat<8,40> c_hw_flt_8_40_2_0val;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<39> c_hw_bit_39_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<29,-40,UNSIGNED> c_hw_fix_29_n40_uns_undef;
  const HWOffsetFix<39,-40,UNSIGNED> c_hw_fix_39_n40_uns_undef;
  const HWOffsetFix<40,-40,UNSIGNED> c_hw_fix_40_n40_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<22,-43,UNSIGNED> c_hw_fix_22_n43_uns_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits_1;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<39,0,UNSIGNED> c_hw_fix_39_0_uns_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits_17;
  const HWFloat<8,40> c_hw_flt_8_40_bits_18;
  const HWFloat<8,40> c_hw_flt_8_40_bits_19;
  const HWFloat<8,40> c_hw_flt_8_40_bits_20;
  const HWFloat<8,40> c_hw_flt_8_40_bits_21;
  const HWFloat<8,40> c_hw_flt_8_40_bits_22;
  const HWFloat<8,40> c_hw_flt_8_40_bits_23;
  const HWFloat<8,40> c_hw_flt_8_40_bits_24;
  const HWFloat<8,40> c_hw_flt_8_40_bits_25;
  const HWFloat<8,40> c_hw_flt_8_40_0_5val;
  const HWFloat<8,40> c_hw_flt_8_40_bits_26;
  const HWFloat<8,40> c_hw_flt_8_40_bits_27;
  const HWFloat<8,40> c_hw_flt_8_40_bits_28;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
