#include "stdsimheader.h"
#include "MinimalModelDFEKernel.h"

namespace maxcompilersim {

MinimalModelDFEKernel::MinimalModelDFEKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 78, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x04al))))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_12_bits((HWFloat<8,12>(varint_u<20>(0x3f800l))))
, c_hw_flt_8_12_undef((HWFloat<8,12>()))
, c_hw_flt_8_12_bits_1((HWFloat<8,12>(varint_u<20>(0x3e99al))))
, c_hw_flt_8_12_bits_2((HWFloat<8,12>(varint_u<20>(0x00000l))))
, c_hw_flt_8_12_bits_3((HWFloat<8,12>(varint_u<20>(0x3bc4al))))
, c_hw_flt_8_12_bits_4((HWFloat<8,12>(varint_u<20>(0x42700l))))
, c_hw_flt_8_12_bits_5((HWFloat<8,12>(varint_u<20>(0x448fcl))))
, c_hw_flt_8_12_bits_6((HWFloat<8,12>(varint_u<20>(0x3fb9bl))))
, c_hw_flt_8_12_bits_7((HWFloat<8,12>(varint_u<20>(0x3fc66l))))
, c_hw_flt_8_12_bits_8((HWFloat<8,12>(varint_u<20>(0x3de14l))))
, c_hw_flt_8_12_bits_9((HWFloat<8,12>(varint_u<20>(0x3e052l))))
, c_hw_flt_8_12_bits_10((HWFloat<8,12>(varint_u<20>(0x43c80l))))
, c_hw_flt_8_12_bits_11((HWFloat<8,12>(varint_u<20>(0x40c00l))))
, c_hw_flt_8_12_bits_12((HWFloat<8,12>(varint_u<20>(0x41f02l))))
, c_hw_flt_8_12_bits_13((HWFloat<8,12>(varint_u<20>(0xc1683l))))
, c_hw_flt_8_12_bits_14((HWFloat<8,12>(varint_u<20>(0x40000l))))
, c_hw_flt_8_12_bits_15((HWFloat<8,12>(varint_u<20>(0x4002fl))))
, c_hw_flt_8_12_bits_16((HWFloat<8,12>(varint_u<20>(0x3f266l))))
, c_hw_flt_8_12_2_0val((HWFloat<8,12>(varint_u<20>(0x40000l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0xffl))))
, c_hw_bit_11_bits((HWRawBits<11>(varint_u<11>(0x000l))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_24_n23_uns_bits((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0xb8aa3bl))))
, c_hw_fix_10_0_sgn_undef((HWOffsetFix<10,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x001l))))
, c_hw_fix_11_0_sgn_bits_1((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x07fl))))
, c_hw_fix_12_n12_uns_undef((HWOffsetFix<12,-12,UNSIGNED>()))
, c_hw_fix_8_n8_uns_bits((HWOffsetFix<8,-8,UNSIGNED>(varint_u<8>(0xb1l))))
, c_hw_fix_8_n14_uns_undef((HWOffsetFix<8,-14,UNSIGNED>()))
, c_hw_fix_12_n11_uns_bits((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x800l))))
, c_hw_fix_11_0_sgn_bits_2((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x000l))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_3((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x0ffl))))
, c_hw_fix_12_n11_uns_bits_1((HWOffsetFix<12,-11,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_n11_uns_undef((HWOffsetFix<12,-11,UNSIGNED>()))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_11_0_uns_bits_2((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x000l))))
, c_hw_flt_8_12_bits_17((HWFloat<8,12>(varint_u<20>(0x7fc00l))))
, c_hw_flt_8_12_bits_18((HWFloat<8,12>(varint_u<20>(0x3d8f6l))))
, c_hw_flt_8_12_bits_19((HWFloat<8,12>(varint_u<20>(0x3f70al))))
, c_hw_flt_8_12_bits_20((HWFloat<8,12>(varint_u<20>(0xc1b40l))))
, c_hw_flt_8_12_bits_21((HWFloat<8,12>(varint_u<20>(0x42820l))))
, c_hw_flt_8_12_bits_22((HWFloat<8,12>(varint_u<20>(0x3cf5cl))))
, c_hw_flt_8_12_bits_23((HWFloat<8,12>(varint_u<20>(0x43480l))))
, c_hw_flt_8_12_bits_24((HWFloat<8,12>(varint_u<20>(0x40066l))))
, c_hw_flt_8_12_bits_25((HWFloat<8,12>(varint_u<20>(0x3f68al))))
, c_hw_flt_8_12_0_5val((HWFloat<8,12>(varint_u<20>(0x3f000l))))
, c_hw_flt_8_12_bits_26((HWFloat<8,12>(varint_u<20>(0x402f0l))))
, c_hw_flt_8_12_bits_27((HWFloat<8,12>(varint_u<20>(0x41800l))))
, c_hw_flt_8_12_bits_28((HWFloat<8,12>(varint_u<20>(0x3ff1al))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 40 (NodeConstantRawBits)
    id40out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 564 (NodeConstantRawBits)
    id564out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 606 (NodeConstantRawBits)
    id606out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 398 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 41 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 605 (NodeConstantRawBits)
    id605out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 38 (NodeConstantRawBits)
    id38out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 15 (NodeConstantRawBits)
    id15out_value = (c_hw_flt_8_12_bits_1);
  }
  { // Node ID: 37 (NodeConstantRawBits)
    id37out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 604 (NodeConstantRawBits)
    id604out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 17 (NodeConstantRawBits)
    id17out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 16 (NodeConstantRawBits)
    id16out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 1 (NodeConstantRawBits)
    id1out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_8_12_bits_5);
  }
  { // Node ID: 0 (NodeConstantRawBits)
    id0out_value = (c_hw_flt_8_12_bits_6);
  }
  { // Node ID: 44 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 29 (NodeConstantRawBits)
    id29out_value = (c_hw_flt_8_12_bits_7);
  }
  { // Node ID: 8 (NodeConstantRawBits)
    id8out_value = (c_hw_flt_8_12_bits_8);
  }
  { // Node ID: 20 (NodeConstantRawBits)
    id20out_value = (c_hw_flt_8_12_bits_9);
  }
  { // Node ID: 22 (NodeConstantRawBits)
    id22out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 9 (NodeConstantRawBits)
    id9out_value = (c_hw_flt_8_12_bits_10);
  }
  { // Node ID: 10 (NodeConstantRawBits)
    id10out_value = (c_hw_flt_8_12_bits_11);
  }
  { // Node ID: 603 (NodeConstantRawBits)
    id603out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 11 (NodeConstantRawBits)
    id11out_value = (c_hw_flt_8_12_bits_12);
  }
  { // Node ID: 36 (NodeConstantRawBits)
    id36out_value = (c_hw_flt_8_12_bits_13);
  }
  { // Node ID: 602 (NodeConstantRawBits)
    id602out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 601 (NodeConstantRawBits)
    id601out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 600 (NodeConstantRawBits)
    id600out_value = (c_hw_flt_8_12_bits_14);
  }
  { // Node ID: 25 (NodeConstantRawBits)
    id25out_value = (c_hw_flt_8_12_bits_15);
  }
  { // Node ID: 30 (NodeConstantRawBits)
    id30out_value = (c_hw_flt_8_12_bits_16);
  }
  { // Node ID: 222 (NodeConstantRawBits)
    id222out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 599 (NodeConstantRawBits)
    id599out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 155 (NodeConstantRawBits)
    id155out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 160 (NodeConstantRawBits)
    id160out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 191 (NodeConstantRawBits)
    id191out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 471 (NodeConstantRawBits)
    id471out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 231 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id231sta_rom_store, 12, 64); 
  }
  { // Node ID: 176 (NodeConstantRawBits)
    id176out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 598 (NodeConstantRawBits)
    id598out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 597 (NodeConstantRawBits)
    id597out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 596 (NodeConstantRawBits)
    id596out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 595 (NodeConstantRawBits)
    id595out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 201 (NodeConstantRawBits)
    id201out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 185 (NodeConstantRawBits)
    id185out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 210 (NodeConstantRawBits)
    id210out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 211 (NodeConstantRawBits)
    id211out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 213 (NodeConstantRawBits)
    id213out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 438 (NodeConstantRawBits)
    id438out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 594 (NodeConstantRawBits)
    id594out_value = (c_hw_flt_8_12_bits_17);
  }
  { // Node ID: 593 (NodeConstantRawBits)
    id593out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 21 (NodeConstantRawBits)
    id21out_value = (c_hw_flt_8_12_bits_9);
  }
  { // Node ID: 384 (NodeConstantRawBits)
    id384out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 592 (NodeConstantRawBits)
    id592out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 18 (NodeConstantRawBits)
    id18out_value = (c_hw_flt_8_12_bits_9);
  }
  { // Node ID: 19 (NodeConstantRawBits)
    id19out_value = (c_hw_flt_8_12_bits_3);
  }
  { // Node ID: 591 (NodeConstantRawBits)
    id591out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 14 (NodeConstantRawBits)
    id14out_value = (c_hw_flt_8_12_bits_18);
  }
  { // Node ID: 32 (NodeConstantRawBits)
    id32out_value = (c_hw_flt_8_12_bits_19);
  }
  { // Node ID: 4 (NodeConstantRawBits)
    id4out_value = (c_hw_flt_8_12_bits_4);
  }
  { // Node ID: 35 (NodeConstantRawBits)
    id35out_value = (c_hw_flt_8_12_bits_20);
  }
  { // Node ID: 590 (NodeConstantRawBits)
    id590out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 589 (NodeConstantRawBits)
    id589out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 588 (NodeConstantRawBits)
    id588out_value = (c_hw_flt_8_12_bits_14);
  }
  { // Node ID: 23 (NodeConstantRawBits)
    id23out_value = (c_hw_flt_8_12_bits_21);
  }
  { // Node ID: 26 (NodeConstantRawBits)
    id26out_value = (c_hw_flt_8_12_bits_22);
  }
  { // Node ID: 131 (NodeConstantRawBits)
    id131out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 587 (NodeConstantRawBits)
    id587out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 64 (NodeConstantRawBits)
    id64out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 69 (NodeConstantRawBits)
    id69out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 100 (NodeConstantRawBits)
    id100out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 473 (NodeConstantRawBits)
    id473out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 140 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id140sta_rom_store, 12, 64); 
  }
  { // Node ID: 85 (NodeConstantRawBits)
    id85out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 586 (NodeConstantRawBits)
    id586out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 585 (NodeConstantRawBits)
    id585out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 584 (NodeConstantRawBits)
    id584out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 583 (NodeConstantRawBits)
    id583out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 110 (NodeConstantRawBits)
    id110out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 94 (NodeConstantRawBits)
    id94out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 119 (NodeConstantRawBits)
    id119out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 120 (NodeConstantRawBits)
    id120out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 122 (NodeConstantRawBits)
    id122out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 439 (NodeConstantRawBits)
    id439out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 582 (NodeConstantRawBits)
    id582out_value = (c_hw_flt_8_12_bits_17);
  }
  { // Node ID: 581 (NodeConstantRawBits)
    id581out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_8_12_bits_23);
  }
  { // Node ID: 580 (NodeConstantRawBits)
    id580out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 579 (NodeConstantRawBits)
    id579out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 578 (NodeConstantRawBits)
    id578out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 577 (NodeConstantRawBits)
    id577out_value = (c_hw_flt_8_12_bits_14);
  }
  { // Node ID: 24 (NodeConstantRawBits)
    id24out_value = (c_hw_flt_8_12_bits_24);
  }
  { // Node ID: 27 (NodeConstantRawBits)
    id27out_value = (c_hw_flt_8_12_bits_25);
  }
  { // Node ID: 338 (NodeConstantRawBits)
    id338out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 576 (NodeConstantRawBits)
    id576out_value = (c_hw_bit_11_bits);
  }
  { // Node ID: 271 (NodeConstantRawBits)
    id271out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 276 (NodeConstantRawBits)
    id276out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 307 (NodeConstantRawBits)
    id307out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 475 (NodeConstantRawBits)
    id475out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 347 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x2d,
      0x5a,
      0x87,
      0xb5,
      0xe4,
      0x113,
      0x143,
      0x173,
      0x1a3,
      0x1d5,
      0x206,
      0x238,
      0x26b,
      0x29f,
      0x2d3,
      0x307,
      0x33c,
      0x372,
      0x3a8,
      0x3df,
      0x416,
      0x44e,
      0x487,
      0x4c0,
      0x4fa,
      0x534,
      0x56f,
      0x5ab,
      0x5e7,
      0x624,
      0x662,
      0x6a1,
      0x6e0,
      0x71f,
      0x760,
      0x7a1,
      0x7e3,
      0x826,
      0x869,
      0x8ad,
      0x8f2,
      0x937,
      0x97e,
      0x9c5,
      0xa0c,
      0xa55,
      0xa9e,
      0xae9,
      0xb34,
      0xb7f,
      0xbcc,
      0xc1a,
      0xc68,
      0xcb7,
      0xd07,
      0xd58,
      0xdaa,
      0xdfd,
      0xe50,
      0xea5,
      0xefa,
      0xf50,
      0xfa8,
    };
    setRom< HWOffsetFix<12,-12,UNSIGNED> > (data, id347sta_rom_store, 12, 64); 
  }
  { // Node ID: 292 (NodeConstantRawBits)
    id292out_value = (c_hw_fix_8_n8_uns_bits);
  }
  { // Node ID: 575 (NodeConstantRawBits)
    id575out_value = (c_hw_fix_12_n11_uns_bits);
  }
  { // Node ID: 574 (NodeConstantRawBits)
    id574out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 573 (NodeConstantRawBits)
    id573out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 572 (NodeConstantRawBits)
    id572out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 317 (NodeConstantRawBits)
    id317out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 301 (NodeConstantRawBits)
    id301out_value = (c_hw_fix_12_n11_uns_bits_1);
  }
  { // Node ID: 326 (NodeConstantRawBits)
    id326out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 327 (NodeConstantRawBits)
    id327out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 329 (NodeConstantRawBits)
    id329out_value = (c_hw_fix_11_0_uns_bits_2);
  }
  { // Node ID: 440 (NodeConstantRawBits)
    id440out_value = (c_hw_flt_8_12_bits_2);
  }
  { // Node ID: 571 (NodeConstantRawBits)
    id571out_value = (c_hw_flt_8_12_bits_17);
  }
  { // Node ID: 570 (NodeConstantRawBits)
    id570out_value = (c_hw_flt_8_12_bits);
  }
  { // Node ID: 6 (NodeConstantRawBits)
    id6out_value = (c_hw_flt_8_12_bits_26);
  }
  { // Node ID: 7 (NodeConstantRawBits)
    id7out_value = (c_hw_flt_8_12_bits_27);
  }
  { // Node ID: 13 (NodeConstantRawBits)
    id13out_value = (c_hw_flt_8_12_bits_28);
  }
  { // Node ID: 401 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 569 (NodeConstantRawBits)
    id569out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 406 (NodeInputMappedReg)
    registerMappedRegister("io_v_out_force_disabled", Data(1));
  }
  { // Node ID: 409 (NodeOutput)
    m_v_out = registerOutput("v_out",1 );
  }
  { // Node ID: 568 (NodeConstantRawBits)
    id568out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 414 (NodeInputMappedReg)
    registerMappedRegister("io_w_out_force_disabled", Data(1));
  }
  { // Node ID: 417 (NodeOutput)
    m_w_out = registerOutput("w_out",2 );
  }
  { // Node ID: 567 (NodeConstantRawBits)
    id567out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 422 (NodeInputMappedReg)
    registerMappedRegister("io_s_out_force_disabled", Data(1));
  }
  { // Node ID: 425 (NodeOutput)
    m_s_out = registerOutput("s_out",3 );
  }
  { // Node ID: 430 (NodeConstantRawBits)
    id430out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 566 (NodeConstantRawBits)
    id566out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 427 (NodeConstantRawBits)
    id427out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 431 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 565 (NodeConstantRawBits)
    id565out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 433 (NodeConstantRawBits)
    id433out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 436 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void MinimalModelDFEKernel::resetComputation() {
  resetComputationAfterFlush();
}

void MinimalModelDFEKernel::resetComputationAfterFlush() {
  { // Node ID: 493 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id493out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 43 (NodeCounter)

    (id43st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 398 (NodeInputMappedReg)
    id398out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 550 (NodeFIFO)

    for(int i=0; i<75; i++)
    {
      id550out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 42 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id42in_max = id41out_num_iteration;

    (id42st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 497 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id497out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 554 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id554out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 555 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id555out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 556 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id556out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 557 (NodeFIFO)

    for(int i=0; i<6; i++)
    {
      id557out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 558 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id558out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 559 (NodeFIFO)

    for(int i=0; i<15; i++)
    {
      id559out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 560 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id560out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 561 (NodeFIFO)

    for(int i=0; i<18; i++)
    {
      id561out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 495 (NodeFIFO)

    for(int i=0; i<60; i++)
    {
      id495out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 496 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id496out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 512 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id512out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 503 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id503out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 504 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id504out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 506 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id506out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 507 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id507out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 508 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id508out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 509 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id509out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 515 (NodeFIFO)

    for(int i=0; i<46; i++)
    {
      id515out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 519 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id519out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 562 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id562out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 529 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id529out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 520 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id520out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 521 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id521out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 523 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id523out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 524 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id524out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 525 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id525out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 526 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id526out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 514 (NodeFIFO)

    for(int i=0; i<6; i++)
    {
      id514out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 534 (NodeFIFO)

    for(int i=0; i<46; i++)
    {
      id534out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 545 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id545out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 563 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id563out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 544 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id544out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 535 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id535out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 536 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id536out_output[i] = (c_hw_fix_8_n14_uns_undef);
    }
  }
  { // Node ID: 538 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id538out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 539 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id539out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 540 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id540out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 541 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id541out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 533 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id533out_output[i] = (c_hw_flt_8_12_undef);
    }
  }
  { // Node ID: 406 (NodeInputMappedReg)
    id406out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 414 (NodeInputMappedReg)
    id414out_io_w_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_w_out_force_disabled");
  }
  { // Node ID: 422 (NodeInputMappedReg)
    id422out_io_s_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_s_out_force_disabled");
  }
  { // Node ID: 428 (NodeCounter)

    (id428st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 434 (NodeCounter)

    (id434st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 436 (NodeInputMappedReg)
    id436out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void MinimalModelDFEKernel::updateState() {
  { // Node ID: 398 (NodeInputMappedReg)
    id398out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 41 (NodeInputMappedReg)
    id41out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 44 (NodeInputMappedReg)
    id44out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 406 (NodeInputMappedReg)
    id406out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 414 (NodeInputMappedReg)
    id414out_io_w_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_w_out_force_disabled");
  }
  { // Node ID: 422 (NodeInputMappedReg)
    id422out_io_s_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_s_out_force_disabled");
  }
  { // Node ID: 436 (NodeInputMappedReg)
    id436out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void MinimalModelDFEKernel::preExecute() {
}

void MinimalModelDFEKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "MinimalModelDFEKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int MinimalModelDFEKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
