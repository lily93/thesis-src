#include "stdsimheader.h"

using namespace maxcompilersim;

namespace maxcompilersim {
// Templated Types used in the kernel
template class HWRawBits<20>;
template class HWOffsetFix<48,0,UNSIGNED>;
template class HWOffsetFix<24,-14,TWOSCOMPLEMENT>;
template class HWOffsetFix<49,0,UNSIGNED>;
template class HWOffsetFix<27,-26,UNSIGNED>;
template class HWOffsetFix<8,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<12,-12,UNSIGNED>;
template class HWFloat<8,12>;
template class HWOffsetFix<11,-11,UNSIGNED>;
template class HWOffsetFix<10,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<48,-37,TWOSCOMPLEMENT>;
template class HWOffsetFix<11,0,TWOSCOMPLEMENT>;
template class HWOffsetFix<16,-22,UNSIGNED>;
template class HWOffsetFix<20,-26,UNSIGNED>;
template class HWOffsetFix<8,-8,UNSIGNED>;
template class HWOffsetFix<1,0,UNSIGNED>;
template class HWOffsetFix<4,0,UNSIGNED>;
template class HWOffsetFix<8,-14,UNSIGNED>;
template class HWOffsetFix<8,0,UNSIGNED>;
template class HWOffsetFix<32,0,UNSIGNED>;
template class HWRawBits<11>;
template class HWOffsetFix<33,0,UNSIGNED>;
template class HWOffsetFix<6,0,UNSIGNED>;
template class HWRawBits<2>;
template class HWFloat<11,53>;
template class HWOffsetFix<24,-23,UNSIGNED>;
template class HWFloat<8,24>;
template class HWOffsetFix<6,-6,UNSIGNED>;
template class HWRawBits<1>;
template class HWOffsetFix<11,0,UNSIGNED>;
template class HWOffsetFix<15,-14,UNSIGNED>;
template class HWOffsetFix<12,-11,UNSIGNED>;
template class HWOffsetFix<12,0,UNSIGNED>;
template class HWRawBits<6>;
template class HWRawBits<9>;
template class HWRawBits<8>;
// add. templates from the default formatter 


// Templated Methods/Functions
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<4,0,UNSIGNED> &a, const HWOffsetFix<4,0,UNSIGNED> &b );
template HWOffsetFix<48,-37,TWOSCOMPLEMENT>mul_fixed <48,-37,TWOSCOMPLEMENT,TONEAREVEN,24,-14,TWOSCOMPLEMENT,24,-23,UNSIGNED, true>(const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &a, const HWOffsetFix<24,-23,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<12,-11,UNSIGNED> cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(const HWOffsetFix<15,-14,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>lt_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWRawBits<2> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>neq_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<11,0,UNSIGNED> &a, const HWOffsetFix<11,0,UNSIGNED> &b );
template HWRawBits<20> cat<>(const HWRawBits<9> &a, const HWOffsetFix<11,-11,UNSIGNED> &b);
template HWOffsetFix<1,0,UNSIGNED>or_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>not_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a );
template HWFloat<8,24> cast_float2float<8,24>(const HWFloat<8,12> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_bits<>(const HWRawBits<8> &a,  const HWRawBits<8> &b );
template HWOffsetFix<11,0,UNSIGNED> cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<12,0,UNSIGNED> &a);
template HWFloat<8,12>div_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWRawBits<11> slice<0,11>(const HWFloat<8,12> &a);
template HWOffsetFix<10,0,TWOSCOMPLEMENT> cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b );
template HWOffsetFix<1,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<1,0,UNSIGNED> >(const std::string &name);
template HWRawBits<20> cat<>(const HWRawBits<9> &a, const HWOffsetFix<11,0,UNSIGNED> &b);
template HWOffsetFix<48,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<11,-11,UNSIGNED> cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(const HWOffsetFix<12,-11,UNSIGNED> &a);
template HWOffsetFix<8,0,TWOSCOMPLEMENT> cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<8,-14,UNSIGNED> cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(const HWOffsetFix<16,-22,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b );
template HWFloat<11,53> KernelManagerBlockSync::getMappedRegValue< HWFloat<11,53> >(const std::string &name);
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<33,0,UNSIGNED> &a);
template HWRawBits<6> cast_fixed2bits<>(const HWOffsetFix<6,-6,UNSIGNED> &a);
template HWFloat<8,12>neg_float<>(const HWFloat<8,12> &a );
template HWOffsetFix<12,0,UNSIGNED> cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<11,0,UNSIGNED> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT>add_fixed <11,0,TWOSCOMPLEMENT,TRUNCATE,11,0,TWOSCOMPLEMENT,11,0,TWOSCOMPLEMENT, false>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a, const HWOffsetFix<11,0,TWOSCOMPLEMENT> &b , EXCEPTOVERFLOW);
template HWFloat<8,12> cast_bits2float<8,12>(const HWRawBits<20> &a);
template HWOffsetFix<15,-14,UNSIGNED> cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(const HWOffsetFix<27,-26,UNSIGNED> &a);
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b);
template HWOffsetFix<16,-22,UNSIGNED>mul_fixed <16,-22,UNSIGNED,TONEAREVEN,8,-8,UNSIGNED,8,-14,UNSIGNED, false>(const HWOffsetFix<8,-8,UNSIGNED> &a, const HWOffsetFix<8,-14,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<12,0,UNSIGNED>add_fixed <12,0,UNSIGNED,TRUNCATE,12,0,UNSIGNED,12,0,UNSIGNED, false>(const HWOffsetFix<12,0,UNSIGNED> &a, const HWOffsetFix<12,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<33,0,UNSIGNED>add_fixed <33,0,UNSIGNED,TRUNCATE,33,0,UNSIGNED,33,0,UNSIGNED, false>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<6,-6,UNSIGNED> cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gt_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<12,-11,UNSIGNED> &a, const HWOffsetFix<12,-11,UNSIGNED> &b );
template HWRawBits<1> slice<10,1>(const HWOffsetFix<11,0,TWOSCOMPLEMENT> &a);
template HWOffsetFix<11,0,UNSIGNED>sub_fixed <11,0,UNSIGNED,TONEAREVEN,11,0,UNSIGNED,11,0,UNSIGNED, false>(const HWOffsetFix<11,0,UNSIGNED> &a, const HWOffsetFix<11,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,12>mul_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWOffsetFix<1,0,UNSIGNED> cast_bits2fixed<1,0,UNSIGNED>(const HWRawBits<1> &a);
template HWOffsetFix<6,0,UNSIGNED> cast_bits2fixed<6,0,UNSIGNED>(const HWRawBits<6> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>neq_bits<>(const HWRawBits<11> &a,  const HWRawBits<11> &b );
template HWOffsetFix<8,-14,UNSIGNED> cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<12,0,UNSIGNED> &a, const HWOffsetFix<12,0,UNSIGNED> &b );
template HWRawBits<9> cat<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<8,0,TWOSCOMPLEMENT> &b);
template HWOffsetFix<15,-14,UNSIGNED>add_fixed <15,-14,UNSIGNED,TONEAREVEN,12,-12,UNSIGNED,8,-14,UNSIGNED, false>(const HWOffsetFix<12,-12,UNSIGNED> &a, const HWOffsetFix<8,-14,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<48,0,UNSIGNED> &a, const HWOffsetFix<48,0,UNSIGNED> &b );
template HWOffsetFix<24,-14,TWOSCOMPLEMENT> cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(const HWFloat<8,12> &a, HWOffsetFix<4,0,UNSIGNED>* exception );
template HWOffsetFix<27,-26,UNSIGNED>add_fixed <27,-26,UNSIGNED,TONEAREVEN,15,-14,UNSIGNED,20,-26,UNSIGNED, false>(const HWOffsetFix<15,-14,UNSIGNED> &a, const HWOffsetFix<20,-26,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<20,-26,UNSIGNED>mul_fixed <20,-26,UNSIGNED,TONEAREVEN,8,-14,UNSIGNED,12,-12,UNSIGNED, false>(const HWOffsetFix<8,-14,UNSIGNED> &a, const HWOffsetFix<12,-12,UNSIGNED> &b , EXCEPTOVERFLOW);
template void KernelManagerBlockSync::writeOutput< HWFloat<8,24> >(const t_port_number port_number, const HWFloat<8,24> &value);
template HWOffsetFix<24,-14,TWOSCOMPLEMENT> cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &a, HWOffsetFix<1,0,UNSIGNED>* exception );
template HWRawBits<8> slice<11,8>(const HWFloat<8,12> &a);
template HWFloat<8,12> cast_float2float<8,12>(const HWFloat<11,53> &a);
template HWOffsetFix<33,0,UNSIGNED> cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<32,0,UNSIGNED> &a);
template void KernelManagerBlockSync::setMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name, const HWOffsetFix<48,0,UNSIGNED> & value);
template HWOffsetFix<48,0,UNSIGNED> cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<49,0,UNSIGNED> &a);
template HWOffsetFix<11,0,TWOSCOMPLEMENT> cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(const HWOffsetFix<10,0,TWOSCOMPLEMENT> &a);
template HWFloat<8,12>sub_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWOffsetFix<49,0,UNSIGNED>add_fixed <49,0,UNSIGNED,TRUNCATE,49,0,UNSIGNED,49,0,UNSIGNED, false>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWFloat<8,12>add_float<>(const HWFloat<8,12> &a, const HWFloat<8,12> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template HWOffsetFix<32,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<32,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<1,0,UNSIGNED>and_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );


// Additional Code

} // End of maxcompilersim namespace
