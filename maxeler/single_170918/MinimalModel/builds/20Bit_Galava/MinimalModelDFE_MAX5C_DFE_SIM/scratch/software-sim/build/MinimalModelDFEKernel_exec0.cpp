#include "stdsimheader.h"

namespace maxcompilersim {

void MinimalModelDFEKernel::execute0() {
  { // Node ID: 40 (NodeConstantRawBits)
  }
  { // Node ID: 564 (NodeConstantRawBits)
  }
  { // Node ID: 493 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id493in_input = id564out_value;

    id493out_output[(getCycle()+1)%2] = id493in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 43 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id43in_enable = id40out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id43in_max = id493out_output[getCycle()%2];

    HWOffsetFix<12,0,UNSIGNED> id43x_1;
    HWOffsetFix<1,0,UNSIGNED> id43x_2;
    HWOffsetFix<1,0,UNSIGNED> id43x_3;
    HWOffsetFix<12,0,UNSIGNED> id43x_4t_1e_1;

    id43out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id43st_count)));
    (id43x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id43st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id43x_2) = (gte_fixed((id43x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id43in_max))));
    (id43x_3) = (and_fixed((id43x_2),id43in_enable));
    id43out_wrap = (id43x_3);
    if((id43in_enable.getValueAsBool())) {
      if(((id43x_3).getValueAsBool())) {
        (id43st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id43x_4t_1e_1) = (id43x_1);
        (id43st_count) = (id43x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 606 (NodeConstantRawBits)
  }
  { // Node ID: 396 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id396in_a = id564out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id396in_b = id606out_value;

    id396out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id396in_a,id396in_b));
  }
  { // Node ID: 441 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id441in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id441in_b = id396out_result[getCycle()%2];

    id441out_result[(getCycle()+1)%2] = (eq_fixed(id441in_a,id441in_b));
  }
  { // Node ID: 398 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id399out_result;

  { // Node ID: 399 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_a = id398out_io_u_out_force_disabled;

    id399out_result = (not_fixed(id399in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id400out_result;

  { // Node ID: 400 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id441out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id400in_b = id399out_result;

    HWOffsetFix<1,0,UNSIGNED> id400x_1;

    (id400x_1) = (and_fixed(id400in_a,id400in_b));
    id400out_result = (id400x_1);
  }
  { // Node ID: 550 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id550in_input = id400out_result;

    id550out_output[(getCycle()+74)%75] = id550in_input;
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 42 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id42in_enable = id43out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id42in_max = id41out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id42x_1;
    HWOffsetFix<1,0,UNSIGNED> id42x_2;
    HWOffsetFix<1,0,UNSIGNED> id42x_3;
    HWOffsetFix<33,0,UNSIGNED> id42x_4t_1e_1;

    id42out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id42st_count)));
    (id42x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id42st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id42x_2) = (gte_fixed((id42x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id42in_max))));
    (id42x_3) = (and_fixed((id42x_2),id42in_enable));
    id42out_wrap = (id42x_3);
    if((id42in_enable.getValueAsBool())) {
      if(((id42x_3).getValueAsBool())) {
        (id42st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id42x_4t_1e_1) = (id42x_1);
        (id42st_count) = (id42x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 605 (NodeConstantRawBits)
  }
  { // Node ID: 442 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id442in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id442in_b = id605out_value;

    id442out_result[(getCycle()+1)%2] = (eq_fixed(id442in_a,id442in_b));
  }
  HWFloat<8,12> id489out_output;

  { // Node ID: 489 (NodeStreamOffset)
    const HWFloat<8,12> &id489in_input = id389out_result[getCycle()%9];

    id489out_output = id489in_input;
  }
  { // Node ID: 38 (NodeConstantRawBits)
  }
  { // Node ID: 48 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id48in_sel = id442out_result[getCycle()%2];
    const HWFloat<8,12> &id48in_option0 = id489out_output;
    const HWFloat<8,12> &id48in_option1 = id38out_value;

    HWFloat<8,12> id48x_1;

    switch((id48in_sel.getValueAsLong())) {
      case 0l:
        id48x_1 = id48in_option0;
        break;
      case 1l:
        id48x_1 = id48in_option1;
        break;
      default:
        id48x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id48out_result[(getCycle()+1)%2] = (id48x_1);
  }
  { // Node ID: 497 (NodeFIFO)
    const HWFloat<8,12> &id497in_input = id48out_result[getCycle()%2];

    id497out_output[(getCycle()+6)%7] = id497in_input;
  }
  { // Node ID: 554 (NodeFIFO)
    const HWFloat<8,12> &id554in_input = id497out_output[getCycle()%7];

    id554out_output[(getCycle()+3)%4] = id554in_input;
  }
  { // Node ID: 555 (NodeFIFO)
    const HWFloat<8,12> &id555in_input = id554out_output[getCycle()%4];

    id555out_output[(getCycle()+1)%2] = id555in_input;
  }
  { // Node ID: 556 (NodeFIFO)
    const HWFloat<8,12> &id556in_input = id555out_output[getCycle()%2];

    id556out_output[(getCycle()+2)%3] = id556in_input;
  }
  { // Node ID: 557 (NodeFIFO)
    const HWFloat<8,12> &id557in_input = id556out_output[getCycle()%3];

    id557out_output[(getCycle()+5)%6] = id557in_input;
  }
  { // Node ID: 558 (NodeFIFO)
    const HWFloat<8,12> &id558in_input = id557out_output[getCycle()%6];

    id558out_output[(getCycle()+9)%10] = id558in_input;
  }
  { // Node ID: 559 (NodeFIFO)
    const HWFloat<8,12> &id559in_input = id558out_output[getCycle()%10];

    id559out_output[(getCycle()+14)%15] = id559in_input;
  }
  { // Node ID: 560 (NodeFIFO)
    const HWFloat<8,12> &id560in_input = id559out_output[getCycle()%15];

    id560out_output[(getCycle()+8)%9] = id560in_input;
  }
  { // Node ID: 561 (NodeFIFO)
    const HWFloat<8,12> &id561in_input = id560out_output[getCycle()%9];

    id561out_output[(getCycle()+17)%18] = id561in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  { // Node ID: 366 (NodeGt)
    const HWFloat<8,12> &id366in_a = id559out_output[getCycle()%15];
    const HWFloat<8,12> &id366in_b = id15out_value;

    id366out_result[(getCycle()+2)%3] = (gt_float(id366in_a,id366in_b));
  }
  { // Node ID: 37 (NodeConstantRawBits)
  }
  { // Node ID: 604 (NodeConstantRawBits)
  }
  { // Node ID: 443 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id443in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id443in_b = id604out_value;

    id443out_result[(getCycle()+1)%2] = (eq_fixed(id443in_a,id443in_b));
  }
  HWFloat<8,12> id490out_output;

  { // Node ID: 490 (NodeStreamOffset)
    const HWFloat<8,12> &id490in_input = id361out_result[getCycle()%9];

    id490out_output = id490in_input;
  }
  { // Node ID: 495 (NodeFIFO)
    const HWFloat<8,12> &id495in_input = id490out_output;

    id495out_output[(getCycle()+59)%60] = id495in_input;
  }
  { // Node ID: 51 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id51in_sel = id443out_result[getCycle()%2];
    const HWFloat<8,12> &id51in_option0 = id495out_output[getCycle()%60];
    const HWFloat<8,12> &id51in_option1 = id38out_value;

    HWFloat<8,12> id51x_1;

    switch((id51in_sel.getValueAsLong())) {
      case 0l:
        id51x_1 = id51in_option0;
        break;
      case 1l:
        id51x_1 = id51in_option1;
        break;
      default:
        id51x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id51out_result[(getCycle()+1)%2] = (id51x_1);
  }
  { // Node ID: 496 (NodeFIFO)
    const HWFloat<8,12> &id496in_input = id51out_result[getCycle()%2];

    id496out_output[(getCycle()+6)%7] = id496in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id255out_result;

  { // Node ID: 255 (NodeGt)
    const HWFloat<8,12> &id255in_a = id48out_result[getCycle()%2];
    const HWFloat<8,12> &id255in_b = id15out_value;

    id255out_result = (gt_float(id255in_a,id255in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id246out_result;

  { // Node ID: 246 (NodeGt)
    const HWFloat<8,12> &id246in_a = id48out_result[getCycle()%2];
    const HWFloat<8,12> &id246in_b = id17out_value;

    id246out_result = (gt_float(id246in_a,id246in_b));
  }
  HWFloat<8,12> id247out_result;

  { // Node ID: 247 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id247in_sel = id246out_result;
    const HWFloat<8,12> &id247in_option0 = id38out_value;
    const HWFloat<8,12> &id247in_option1 = id37out_value;

    HWFloat<8,12> id247x_1;

    switch((id247in_sel.getValueAsLong())) {
      case 0l:
        id247x_1 = id247in_option0;
        break;
      case 1l:
        id247x_1 = id247in_option1;
        break;
      default:
        id247x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id247out_result = (id247x_1);
  }
  HWFloat<8,12> id258out_result;

  { // Node ID: 258 (NodeSub)
    const HWFloat<8,12> &id258in_a = id247out_result;
    const HWFloat<8,12> &id258in_b = id51out_result[getCycle()%2];

    id258out_result = (sub_float(id258in_a,id258in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<8,12> &id58in_a = id48out_result[getCycle()%2];
    const HWFloat<8,12> &id58in_b = id16out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,12> id59out_result;

  { // Node ID: 59 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id59in_sel = id58out_result;
    const HWFloat<8,12> &id59in_option0 = id1out_value;
    const HWFloat<8,12> &id59in_option1 = id2out_value;

    HWFloat<8,12> id59x_1;

    switch((id59in_sel.getValueAsLong())) {
      case 0l:
        id59x_1 = id59in_option0;
        break;
      case 1l:
        id59x_1 = id59in_option1;
        break;
      default:
        id59x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id59out_result = (id59x_1);
  }
  HWFloat<8,12> id259out_result;

  { // Node ID: 259 (NodeDiv)
    const HWFloat<8,12> &id259in_a = id258out_result;
    const HWFloat<8,12> &id259in_b = id59out_result;

    id259out_result = (div_float(id259in_a,id259in_b));
  }
  HWFloat<8,12> id256out_result;

  { // Node ID: 256 (NodeNeg)
    const HWFloat<8,12> &id256in_a = id51out_result[getCycle()%2];

    id256out_result = (neg_float(id256in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  HWFloat<8,12> id257out_result;

  { // Node ID: 257 (NodeDiv)
    const HWFloat<8,12> &id257in_a = id256out_result;
    const HWFloat<8,12> &id257in_b = id0out_value;

    id257out_result = (div_float(id257in_a,id257in_b));
  }
  HWFloat<8,12> id260out_result;

  { // Node ID: 260 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id260in_sel = id255out_result;
    const HWFloat<8,12> &id260in_option0 = id259out_result;
    const HWFloat<8,12> &id260in_option1 = id257out_result;

    HWFloat<8,12> id260x_1;

    switch((id260in_sel.getValueAsLong())) {
      case 0l:
        id260x_1 = id260in_option0;
        break;
      case 1l:
        id260x_1 = id260in_option1;
        break;
      default:
        id260x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id260out_result = (id260x_1);
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 45 (NodeCast)
    const HWFloat<11,53> &id45in_i = id44out_dt;

    id45out_o[(getCycle()+3)%4] = (cast_float2float<8,12>(id45in_i));
  }
  { // Node ID: 360 (NodeMul)
    const HWFloat<8,12> &id360in_a = id260out_result;
    const HWFloat<8,12> &id360in_b = id45out_o[getCycle()%4];

    id360out_result[(getCycle()+6)%7] = (mul_float(id360in_a,id360in_b));
  }
  { // Node ID: 361 (NodeAdd)
    const HWFloat<8,12> &id361in_a = id496out_output[getCycle()%7];
    const HWFloat<8,12> &id361in_b = id360out_result[getCycle()%7];

    id361out_result[(getCycle()+8)%9] = (add_float(id361in_a,id361in_b));
  }
  HWFloat<8,12> id367out_result;

  { // Node ID: 367 (NodeNeg)
    const HWFloat<8,12> &id367in_a = id361out_result[getCycle()%9];

    id367out_result = (neg_float(id367in_a));
  }
  { // Node ID: 368 (NodeSub)
    const HWFloat<8,12> &id368in_a = id497out_output[getCycle()%7];
    const HWFloat<8,12> &id368in_b = id15out_value;

    id368out_result[(getCycle()+8)%9] = (sub_float(id368in_a,id368in_b));
  }
  { // Node ID: 369 (NodeMul)
    const HWFloat<8,12> &id369in_a = id367out_result;
    const HWFloat<8,12> &id369in_b = id368out_result[getCycle()%9];

    id369out_result[(getCycle()+6)%7] = (mul_float(id369in_a,id369in_b));
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  { // Node ID: 370 (NodeSub)
    const HWFloat<8,12> &id370in_a = id29out_value;
    const HWFloat<8,12> &id370in_b = id556out_output[getCycle()%3];

    id370out_result[(getCycle()+8)%9] = (sub_float(id370in_a,id370in_b));
  }
  { // Node ID: 371 (NodeMul)
    const HWFloat<8,12> &id371in_a = id369out_result[getCycle()%7];
    const HWFloat<8,12> &id371in_b = id370out_result[getCycle()%9];

    id371out_result[(getCycle()+6)%7] = (mul_float(id371in_a,id371in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 372 (NodeDiv)
    const HWFloat<8,12> &id372in_a = id371out_result[getCycle()%7];
    const HWFloat<8,12> &id372in_b = id8out_value;

    id372out_result[(getCycle()+16)%17] = (div_float(id372in_a,id372in_b));
  }
  { // Node ID: 373 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id373in_sel = id366out_result[getCycle()%3];
    const HWFloat<8,12> &id373in_option0 = id37out_value;
    const HWFloat<8,12> &id373in_option1 = id372out_result[getCycle()%17];

    HWFloat<8,12> id373x_1;

    switch((id373in_sel.getValueAsLong())) {
      case 0l:
        id373x_1 = id373in_option0;
        break;
      case 1l:
        id373x_1 = id373in_option1;
        break;
      default:
        id373x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id373out_result[(getCycle()+1)%2] = (id373x_1);
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  { // Node ID: 374 (NodeGt)
    const HWFloat<8,12> &id374in_a = id559out_output[getCycle()%15];
    const HWFloat<8,12> &id374in_b = id20out_value;

    id374out_result[(getCycle()+2)%3] = (gt_float(id374in_a,id374in_b));
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id244out_result;

  { // Node ID: 244 (NodeGt)
    const HWFloat<8,12> &id244in_a = id558out_output[getCycle()%10];
    const HWFloat<8,12> &id244in_b = id22out_value;

    id244out_result = (gt_float(id244in_a,id244in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  HWFloat<8,12> id245out_result;

  { // Node ID: 245 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id245in_sel = id244out_result;
    const HWFloat<8,12> &id245in_option0 = id9out_value;
    const HWFloat<8,12> &id245in_option1 = id10out_value;

    HWFloat<8,12> id245x_1;

    switch((id245in_sel.getValueAsLong())) {
      case 0l:
        id245x_1 = id245in_option0;
        break;
      case 1l:
        id245x_1 = id245in_option1;
        break;
      default:
        id245x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id245out_result = (id245x_1);
  }
  { // Node ID: 378 (NodeDiv)
    const HWFloat<8,12> &id378in_a = id558out_output[getCycle()%10];
    const HWFloat<8,12> &id378in_b = id245out_result;

    id378out_result[(getCycle()+16)%17] = (div_float(id378in_a,id378in_b));
  }
  { // Node ID: 603 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 36 (NodeConstantRawBits)
  }
  { // Node ID: 602 (NodeConstantRawBits)
  }
  { // Node ID: 601 (NodeConstantRawBits)
  }
  { // Node ID: 600 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  HWFloat<8,12> id151out_result;

  { // Node ID: 151 (NodeSub)
    const HWFloat<8,12> &id151in_a = id557out_output[getCycle()%6];
    const HWFloat<8,12> &id151in_b = id30out_value;

    id151out_result = (sub_float(id151in_a,id151in_b));
  }
  HWFloat<8,12> id152out_result;

  { // Node ID: 152 (NodeMul)
    const HWFloat<8,12> &id152in_a = id25out_value;
    const HWFloat<8,12> &id152in_b = id151out_result;

    id152out_result = (mul_float(id152in_a,id152in_b));
  }
  { // Node ID: 480 (NodePO2FPMult)
    const HWFloat<8,12> &id480in_floatIn = id152out_result;

    id480out_floatOut[(getCycle()+1)%2] = (mul_float(id480in_floatIn,(c_hw_flt_8_12_2_0val)));
  }
  HWRawBits<8> id221out_result;

  { // Node ID: 221 (NodeSlice)
    const HWFloat<8,12> &id221in_a = id480out_floatOut[getCycle()%2];

    id221out_result = (slice<11,8>(id221in_a));
  }
  { // Node ID: 222 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id444out_result;

  { // Node ID: 444 (NodeEqInlined)
    const HWRawBits<8> &id444in_a = id221out_result;
    const HWRawBits<8> &id444in_b = id222out_value;

    id444out_result = (eq_bits(id444in_a,id444in_b));
  }
  HWRawBits<11> id220out_result;

  { // Node ID: 220 (NodeSlice)
    const HWFloat<8,12> &id220in_a = id480out_floatOut[getCycle()%2];

    id220out_result = (slice<0,11>(id220in_a));
  }
  { // Node ID: 599 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id445out_result;

  { // Node ID: 445 (NodeNeqInlined)
    const HWRawBits<11> &id445in_a = id220out_result;
    const HWRawBits<11> &id445in_b = id599out_value;

    id445out_result = (neq_bits(id445in_a,id445in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id226out_result;

  { // Node ID: 226 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id226in_a = id444out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id226in_b = id445out_result;

    HWOffsetFix<1,0,UNSIGNED> id226x_1;

    (id226x_1) = (and_fixed(id226in_a,id226in_b));
    id226out_result = (id226x_1);
  }
  { // Node ID: 512 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id512in_input = id226out_result;

    id512out_output[(getCycle()+8)%9] = id512in_input;
  }
  { // Node ID: 155 (NodeConstantRawBits)
  }
  HWFloat<8,12> id156out_output;
  HWOffsetFix<1,0,UNSIGNED> id156out_output_doubt;

  { // Node ID: 156 (NodeDoubtBitOp)
    const HWFloat<8,12> &id156in_input = id480out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id156in_doubt = id155out_value;

    id156out_output = id156in_input;
    id156out_output_doubt = id156in_doubt;
  }
  { // Node ID: 157 (NodeCast)
    const HWFloat<8,12> &id157in_i = id156out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id157in_i_doubt = id156out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id157x_1;

    id157out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id157in_i,(&(id157x_1))));
    id157out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id157x_1),(c_hw_fix_4_0_uns_bits))),id157in_i_doubt));
  }
  { // Node ID: 160 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id159out_result;
  HWOffsetFix<1,0,UNSIGNED> id159out_result_doubt;

  { // Node ID: 159 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id159in_a = id157out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id159in_a_doubt = id157out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id159in_b = id160out_value;

    HWOffsetFix<1,0,UNSIGNED> id159x_1;

    id159out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id159in_a,id159in_b,(&(id159x_1))));
    id159out_result_doubt = (or_fixed((neq_fixed((id159x_1),(c_hw_fix_1_0_uns_bits_1))),id159in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id161out_o;
  HWOffsetFix<1,0,UNSIGNED> id161out_o_doubt;

  { // Node ID: 161 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id161in_i = id159out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id161in_i_doubt = id159out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id161x_1;

    id161out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id161in_i,(&(id161x_1))));
    id161out_o_doubt = (or_fixed((neq_fixed((id161x_1),(c_hw_fix_1_0_uns_bits_1))),id161in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id170out_output;

  { // Node ID: 170 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id170in_input = id161out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id170in_input_doubt = id161out_o_doubt;

    id170out_output = id170in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id171out_o;

  { // Node ID: 171 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id171in_i = id170out_output;

    id171out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id171in_i));
  }
  { // Node ID: 503 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id503in_input = id171out_o;

    id503out_output[(getCycle()+2)%3] = id503in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id188out_o;

  { // Node ID: 188 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id188in_i = id503out_output[getCycle()%3];

    id188out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id188in_i));
  }
  { // Node ID: 191 (NodeConstantRawBits)
  }
  { // Node ID: 471 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id172out_o;

  { // Node ID: 172 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id172in_i = id170out_output;

    id172out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id172in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id230out_output;

  { // Node ID: 230 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id230in_input = id172out_o;

    id230out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id230in_input))));
  }
  { // Node ID: 231 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id231in_addr = id230out_output;

    HWOffsetFix<12,-12,UNSIGNED> id231x_1;

    switch(((long)((id231in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id231x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id231x_1 = (id231sta_rom_store[(id231in_addr.getValueAsLong())]);
        break;
      default:
        id231x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id231out_dout[(getCycle()+2)%3] = (id231x_1);
  }
  { // Node ID: 176 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id173out_o;

  { // Node ID: 173 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id173in_i = id170out_output;

    id173out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id173in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id175out_result;

  { // Node ID: 175 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id175in_a = id176out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id175in_b = id173out_o;

    id175out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id175in_a,id175in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id177out_o;

  { // Node ID: 177 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id177in_i = id175out_result;

    id177out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id177in_i));
  }
  { // Node ID: 504 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id504in_input = id177out_o;

    id504out_output[(getCycle()+2)%3] = id504in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id178out_result;

  { // Node ID: 178 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id178in_a = id231out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id178in_b = id504out_output[getCycle()%3];

    id178out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id178in_a,id178in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id179out_result;

  { // Node ID: 179 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id179in_a = id504out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id179in_b = id231out_dout[getCycle()%3];

    id179out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id179in_a,id179in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id180out_result;

  { // Node ID: 180 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id180in_a = id178out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id180in_b = id179out_result;

    id180out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id180in_a,id180in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id181out_o;

  { // Node ID: 181 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id181in_i = id180out_result;

    id181out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id181in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id182out_o;

  { // Node ID: 182 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id182in_i = id181out_o;

    id182out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id182in_i));
  }
  { // Node ID: 598 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id446out_result;

  { // Node ID: 446 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id446in_a = id182out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id446in_b = id598out_value;

    id446out_result = (gte_fixed(id446in_a,id446in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id479out_result;

  { // Node ID: 479 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id479in_a = id188out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id479in_b = id191out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id479in_c = id471out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id479in_condb = id446out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id479x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id479x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id479x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id479x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id479x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id479x_1 = id479in_a;
        break;
      default:
        id479x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id479in_condb.getValueAsLong())) {
      case 0l:
        id479x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id479x_2 = id479in_b;
        break;
      default:
        id479x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id479x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id479x_3 = id479in_c;
        break;
      default:
        id479x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id479x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id479x_1),(id479x_2))),(id479x_3)));
    id479out_result = (id479x_4);
  }
  HWRawBits<1> id447out_result;

  { // Node ID: 447 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id447in_a = id479out_result;

    id447out_result = (slice<10,1>(id447in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id448out_output;

  { // Node ID: 448 (NodeReinterpret)
    const HWRawBits<1> &id448in_input = id447out_result;

    id448out_output = (cast_bits2fixed<1,0,UNSIGNED>(id448in_input));
  }
  { // Node ID: 597 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id163out_result;

  { // Node ID: 163 (NodeGt)
    const HWFloat<8,12> &id163in_a = id480out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id163in_b = id597out_value;

    id163out_result = (gt_float(id163in_a,id163in_b));
  }
  { // Node ID: 506 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id506in_input = id163out_result;

    id506out_output[(getCycle()+6)%7] = id506in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id164out_output;

  { // Node ID: 164 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id164in_input = id161out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id164in_input_doubt = id161out_o_doubt;

    id164out_output = id164in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id165out_result;

  { // Node ID: 165 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id165in_a = id506out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id165in_b = id164out_output;

    HWOffsetFix<1,0,UNSIGNED> id165x_1;

    (id165x_1) = (and_fixed(id165in_a,id165in_b));
    id165out_result = (id165x_1);
  }
  { // Node ID: 507 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id507in_input = id165out_result;

    id507out_output[(getCycle()+2)%3] = id507in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id197out_result;

  { // Node ID: 197 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id197in_a = id507out_output[getCycle()%3];

    id197out_result = (not_fixed(id197in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id198out_result;

  { // Node ID: 198 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id198in_a = id448out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id198in_b = id197out_result;

    HWOffsetFix<1,0,UNSIGNED> id198x_1;

    (id198x_1) = (and_fixed(id198in_a,id198in_b));
    id198out_result = (id198x_1);
  }
  { // Node ID: 596 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id167out_result;

  { // Node ID: 167 (NodeLt)
    const HWFloat<8,12> &id167in_a = id480out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id167in_b = id596out_value;

    id167out_result = (lt_float(id167in_a,id167in_b));
  }
  { // Node ID: 508 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id508in_input = id167out_result;

    id508out_output[(getCycle()+6)%7] = id508in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id168out_output;

  { // Node ID: 168 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id168in_input = id161out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id168in_input_doubt = id161out_o_doubt;

    id168out_output = id168in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id169out_result;

  { // Node ID: 169 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id169in_a = id508out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id169in_b = id168out_output;

    HWOffsetFix<1,0,UNSIGNED> id169x_1;

    (id169x_1) = (and_fixed(id169in_a,id169in_b));
    id169out_result = (id169x_1);
  }
  { // Node ID: 509 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id509in_input = id169out_result;

    id509out_output[(getCycle()+2)%3] = id509in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id199out_result;

  { // Node ID: 199 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id199in_a = id198out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id199in_b = id509out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id199x_1;

    (id199x_1) = (or_fixed(id199in_a,id199in_b));
    id199out_result = (id199x_1);
  }
  { // Node ID: 595 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id449out_result;

  { // Node ID: 449 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id449in_a = id479out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id449in_b = id595out_value;

    id449out_result = (gte_fixed(id449in_a,id449in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id206out_result;

  { // Node ID: 206 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id206in_a = id509out_output[getCycle()%3];

    id206out_result = (not_fixed(id206in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id207out_result;

  { // Node ID: 207 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id207in_a = id449out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id207in_b = id206out_result;

    HWOffsetFix<1,0,UNSIGNED> id207x_1;

    (id207x_1) = (and_fixed(id207in_a,id207in_b));
    id207out_result = (id207x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id208out_result;

  { // Node ID: 208 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id208in_a = id207out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id208in_b = id507out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id208x_1;

    (id208x_1) = (or_fixed(id208in_a,id208in_b));
    id208out_result = (id208x_1);
  }
  HWRawBits<2> id209out_result;

  { // Node ID: 209 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id209in_in0 = id199out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id209in_in1 = id208out_result;

    id209out_result = (cat(id209in_in0,id209in_in1));
  }
  { // Node ID: 201 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id200out_o;

  { // Node ID: 200 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id200in_i = id479out_result;

    id200out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id200in_i));
  }
  { // Node ID: 185 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id186out_result;

  { // Node ID: 186 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id186in_sel = id446out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id186in_option0 = id182out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id186in_option1 = id185out_value;

    HWOffsetFix<12,-11,UNSIGNED> id186x_1;

    switch((id186in_sel.getValueAsLong())) {
      case 0l:
        id186x_1 = id186in_option0;
        break;
      case 1l:
        id186x_1 = id186in_option1;
        break;
      default:
        id186x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id186out_result = (id186x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id187out_o;

  { // Node ID: 187 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id187in_i = id186out_result;

    id187out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id187in_i));
  }
  HWRawBits<20> id202out_result;

  { // Node ID: 202 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id202in_in0 = id201out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id202in_in1 = id200out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id202in_in2 = id187out_o;

    id202out_result = (cat((cat(id202in_in0,id202in_in1)),id202in_in2));
  }
  HWFloat<8,12> id203out_output;

  { // Node ID: 203 (NodeReinterpret)
    const HWRawBits<20> &id203in_input = id202out_result;

    id203out_output = (cast_bits2float<8,12>(id203in_input));
  }
  { // Node ID: 210 (NodeConstantRawBits)
  }
  { // Node ID: 211 (NodeConstantRawBits)
  }
  { // Node ID: 213 (NodeConstantRawBits)
  }
  HWRawBits<20> id450out_result;

  { // Node ID: 450 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id450in_in0 = id210out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id450in_in1 = id211out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id450in_in2 = id213out_value;

    id450out_result = (cat((cat(id450in_in0,id450in_in1)),id450in_in2));
  }
  HWFloat<8,12> id215out_output;

  { // Node ID: 215 (NodeReinterpret)
    const HWRawBits<20> &id215in_input = id450out_result;

    id215out_output = (cast_bits2float<8,12>(id215in_input));
  }
  { // Node ID: 438 (NodeConstantRawBits)
  }
  HWFloat<8,12> id218out_result;

  { // Node ID: 218 (NodeMux)
    const HWRawBits<2> &id218in_sel = id209out_result;
    const HWFloat<8,12> &id218in_option0 = id203out_output;
    const HWFloat<8,12> &id218in_option1 = id215out_output;
    const HWFloat<8,12> &id218in_option2 = id438out_value;
    const HWFloat<8,12> &id218in_option3 = id215out_output;

    HWFloat<8,12> id218x_1;

    switch((id218in_sel.getValueAsLong())) {
      case 0l:
        id218x_1 = id218in_option0;
        break;
      case 1l:
        id218x_1 = id218in_option1;
        break;
      case 2l:
        id218x_1 = id218in_option2;
        break;
      case 3l:
        id218x_1 = id218in_option3;
        break;
      default:
        id218x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id218out_result = (id218x_1);
  }
  { // Node ID: 594 (NodeConstantRawBits)
  }
  HWFloat<8,12> id228out_result;

  { // Node ID: 228 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id228in_sel = id512out_output[getCycle()%9];
    const HWFloat<8,12> &id228in_option0 = id218out_result;
    const HWFloat<8,12> &id228in_option1 = id594out_value;

    HWFloat<8,12> id228x_1;

    switch((id228in_sel.getValueAsLong())) {
      case 0l:
        id228x_1 = id228in_option0;
        break;
      case 1l:
        id228x_1 = id228in_option1;
        break;
      default:
        id228x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id228out_result = (id228x_1);
  }
  { // Node ID: 593 (NodeConstantRawBits)
  }
  HWFloat<8,12> id233out_result;

  { // Node ID: 233 (NodeAdd)
    const HWFloat<8,12> &id233in_a = id228out_result;
    const HWFloat<8,12> &id233in_b = id593out_value;

    id233out_result = (add_float(id233in_a,id233in_b));
  }
  HWFloat<8,12> id235out_result;

  { // Node ID: 235 (NodeDiv)
    const HWFloat<8,12> &id235in_a = id600out_value;
    const HWFloat<8,12> &id235in_b = id233out_result;

    id235out_result = (div_float(id235in_a,id235in_b));
  }
  HWFloat<8,12> id237out_result;

  { // Node ID: 237 (NodeSub)
    const HWFloat<8,12> &id237in_a = id601out_value;
    const HWFloat<8,12> &id237in_b = id235out_result;

    id237out_result = (sub_float(id237in_a,id237in_b));
  }
  HWFloat<8,12> id239out_result;

  { // Node ID: 239 (NodeAdd)
    const HWFloat<8,12> &id239in_a = id602out_value;
    const HWFloat<8,12> &id239in_b = id237out_result;

    id239out_result = (add_float(id239in_a,id239in_b));
  }
  HWFloat<8,12> id240out_result;

  { // Node ID: 240 (NodeMul)
    const HWFloat<8,12> &id240in_a = id36out_value;
    const HWFloat<8,12> &id240in_b = id239out_result;

    id240out_result = (mul_float(id240in_a,id240in_b));
  }
  HWFloat<8,12> id241out_result;

  { // Node ID: 241 (NodeAdd)
    const HWFloat<8,12> &id241in_a = id11out_value;
    const HWFloat<8,12> &id241in_b = id240out_result;

    id241out_result = (add_float(id241in_a,id241in_b));
  }
  { // Node ID: 376 (NodeDiv)
    const HWFloat<8,12> &id376in_a = id603out_value;
    const HWFloat<8,12> &id376in_b = id241out_result;

    id376out_result[(getCycle()+16)%17] = (div_float(id376in_a,id376in_b));
  }
  { // Node ID: 379 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id379in_sel = id374out_result[getCycle()%3];
    const HWFloat<8,12> &id379in_option0 = id378out_result[getCycle()%17];
    const HWFloat<8,12> &id379in_option1 = id376out_result[getCycle()%17];

    HWFloat<8,12> id379x_1;

    switch((id379in_sel.getValueAsLong())) {
      case 0l:
        id379x_1 = id379in_option0;
        break;
      case 1l:
        id379x_1 = id379in_option1;
        break;
      default:
        id379x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id379out_result[(getCycle()+1)%2] = (id379x_1);
  }
  { // Node ID: 386 (NodeAdd)
    const HWFloat<8,12> &id386in_a = id373out_result[getCycle()%2];
    const HWFloat<8,12> &id386in_b = id379out_result[getCycle()%2];

    id386out_result[(getCycle()+8)%9] = (add_float(id386in_a,id386in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  { // Node ID: 380 (NodeGt)
    const HWFloat<8,12> &id380in_a = id560out_output[getCycle()%9];
    const HWFloat<8,12> &id380in_b = id21out_value;

    id380out_result[(getCycle()+2)%3] = (gt_float(id380in_a,id380in_b));
  }
  { // Node ID: 384 (NodeConstantRawBits)
  }
  { // Node ID: 592 (NodeConstantRawBits)
  }
  { // Node ID: 451 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id451in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id451in_b = id592out_value;

    id451out_result[(getCycle()+1)%2] = (eq_fixed(id451in_a,id451in_b));
  }
  HWFloat<8,12> id491out_output;

  { // Node ID: 491 (NodeStreamOffset)
    const HWFloat<8,12> &id491in_input = id514out_output[getCycle()%6];

    id491out_output = id491in_input;
  }
  { // Node ID: 515 (NodeFIFO)
    const HWFloat<8,12> &id515in_input = id491out_output;

    id515out_output[(getCycle()+45)%46] = id515in_input;
  }
  { // Node ID: 54 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id54in_sel = id451out_result[getCycle()%2];
    const HWFloat<8,12> &id54in_option0 = id515out_output[getCycle()%46];
    const HWFloat<8,12> &id54in_option1 = id38out_value;

    HWFloat<8,12> id54x_1;

    switch((id54in_sel.getValueAsLong())) {
      case 0l:
        id54x_1 = id54in_option0;
        break;
      case 1l:
        id54x_1 = id54in_option1;
        break;
      default:
        id54x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id54out_result[(getCycle()+1)%2] = (id54x_1);
  }
  { // Node ID: 519 (NodeFIFO)
    const HWFloat<8,12> &id519in_input = id54out_result[getCycle()%2];

    id519out_output[(getCycle()+9)%10] = id519in_input;
  }
  { // Node ID: 562 (NodeFIFO)
    const HWFloat<8,12> &id562in_input = id519out_output[getCycle()%10];

    id562out_output[(getCycle()+6)%7] = id562in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id261out_result;

  { // Node ID: 261 (NodeGt)
    const HWFloat<8,12> &id261in_a = id554out_output[getCycle()%4];
    const HWFloat<8,12> &id261in_b = id18out_value;

    id261out_result = (gt_float(id261in_a,id261in_b));
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id248out_result;

  { // Node ID: 248 (NodeGt)
    const HWFloat<8,12> &id248in_a = id554out_output[getCycle()%4];
    const HWFloat<8,12> &id248in_b = id19out_value;

    id248out_result = (gt_float(id248in_a,id248in_b));
  }
  { // Node ID: 591 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  HWFloat<8,12> id249out_result;

  { // Node ID: 249 (NodeDiv)
    const HWFloat<8,12> &id249in_a = id554out_output[getCycle()%4];
    const HWFloat<8,12> &id249in_b = id14out_value;

    id249out_result = (div_float(id249in_a,id249in_b));
  }
  HWFloat<8,12> id251out_result;

  { // Node ID: 251 (NodeSub)
    const HWFloat<8,12> &id251in_a = id591out_value;
    const HWFloat<8,12> &id251in_b = id249out_result;

    id251out_result = (sub_float(id251in_a,id251in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  HWFloat<8,12> id252out_result;

  { // Node ID: 252 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id252in_sel = id248out_result;
    const HWFloat<8,12> &id252in_option0 = id251out_result;
    const HWFloat<8,12> &id252in_option1 = id32out_value;

    HWFloat<8,12> id252x_1;

    switch((id252in_sel.getValueAsLong())) {
      case 0l:
        id252x_1 = id252in_option0;
        break;
      case 1l:
        id252x_1 = id252in_option1;
        break;
      default:
        id252x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id252out_result = (id252x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id253out_result;

  { // Node ID: 253 (NodeGt)
    const HWFloat<8,12> &id253in_a = id252out_result;
    const HWFloat<8,12> &id253in_b = id38out_value;

    id253out_result = (gt_float(id253in_a,id253in_b));
  }
  HWFloat<8,12> id254out_result;

  { // Node ID: 254 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id254in_sel = id253out_result;
    const HWFloat<8,12> &id254in_option0 = id252out_result;
    const HWFloat<8,12> &id254in_option1 = id38out_value;

    HWFloat<8,12> id254x_1;

    switch((id254in_sel.getValueAsLong())) {
      case 0l:
        id254x_1 = id254in_option0;
        break;
      case 1l:
        id254x_1 = id254in_option1;
        break;
      default:
        id254x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id254out_result = (id254x_1);
  }
  HWFloat<8,12> id264out_result;

  { // Node ID: 264 (NodeSub)
    const HWFloat<8,12> &id264in_a = id254out_result;
    const HWFloat<8,12> &id264in_b = id519out_output[getCycle()%10];

    id264out_result = (sub_float(id264in_a,id264in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  { // Node ID: 590 (NodeConstantRawBits)
  }
  { // Node ID: 589 (NodeConstantRawBits)
  }
  { // Node ID: 588 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  HWFloat<8,12> id60out_result;

  { // Node ID: 60 (NodeSub)
    const HWFloat<8,12> &id60in_a = id48out_result[getCycle()%2];
    const HWFloat<8,12> &id60in_b = id26out_value;

    id60out_result = (sub_float(id60in_a,id60in_b));
  }
  HWFloat<8,12> id61out_result;

  { // Node ID: 61 (NodeMul)
    const HWFloat<8,12> &id61in_a = id23out_value;
    const HWFloat<8,12> &id61in_b = id60out_result;

    id61out_result = (mul_float(id61in_a,id61in_b));
  }
  { // Node ID: 481 (NodePO2FPMult)
    const HWFloat<8,12> &id481in_floatIn = id61out_result;

    id481out_floatOut[(getCycle()+1)%2] = (mul_float(id481in_floatIn,(c_hw_flt_8_12_2_0val)));
  }
  HWRawBits<8> id130out_result;

  { // Node ID: 130 (NodeSlice)
    const HWFloat<8,12> &id130in_a = id481out_floatOut[getCycle()%2];

    id130out_result = (slice<11,8>(id130in_a));
  }
  { // Node ID: 131 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id452out_result;

  { // Node ID: 452 (NodeEqInlined)
    const HWRawBits<8> &id452in_a = id130out_result;
    const HWRawBits<8> &id452in_b = id131out_value;

    id452out_result = (eq_bits(id452in_a,id452in_b));
  }
  HWRawBits<11> id129out_result;

  { // Node ID: 129 (NodeSlice)
    const HWFloat<8,12> &id129in_a = id481out_floatOut[getCycle()%2];

    id129out_result = (slice<0,11>(id129in_a));
  }
  { // Node ID: 587 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id453out_result;

  { // Node ID: 453 (NodeNeqInlined)
    const HWRawBits<11> &id453in_a = id129out_result;
    const HWRawBits<11> &id453in_b = id587out_value;

    id453out_result = (neq_bits(id453in_a,id453in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id135out_result;

  { // Node ID: 135 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id135in_a = id452out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id135in_b = id453out_result;

    HWOffsetFix<1,0,UNSIGNED> id135x_1;

    (id135x_1) = (and_fixed(id135in_a,id135in_b));
    id135out_result = (id135x_1);
  }
  { // Node ID: 529 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id529in_input = id135out_result;

    id529out_output[(getCycle()+8)%9] = id529in_input;
  }
  { // Node ID: 64 (NodeConstantRawBits)
  }
  HWFloat<8,12> id65out_output;
  HWOffsetFix<1,0,UNSIGNED> id65out_output_doubt;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWFloat<8,12> &id65in_input = id481out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id65in_doubt = id64out_value;

    id65out_output = id65in_input;
    id65out_output_doubt = id65in_doubt;
  }
  { // Node ID: 66 (NodeCast)
    const HWFloat<8,12> &id66in_i = id65out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id66in_i_doubt = id65out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id66x_1;

    id66out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id66in_i,(&(id66x_1))));
    id66out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id66x_1),(c_hw_fix_4_0_uns_bits))),id66in_i_doubt));
  }
  { // Node ID: 69 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id68out_result;
  HWOffsetFix<1,0,UNSIGNED> id68out_result_doubt;

  { // Node ID: 68 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id68in_a = id66out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id68in_a_doubt = id66out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id68in_b = id69out_value;

    HWOffsetFix<1,0,UNSIGNED> id68x_1;

    id68out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id68in_a,id68in_b,(&(id68x_1))));
    id68out_result_doubt = (or_fixed((neq_fixed((id68x_1),(c_hw_fix_1_0_uns_bits_1))),id68in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id70out_o;
  HWOffsetFix<1,0,UNSIGNED> id70out_o_doubt;

  { // Node ID: 70 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id70in_i = id68out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id70in_i_doubt = id68out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id70x_1;

    id70out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id70in_i,(&(id70x_1))));
    id70out_o_doubt = (or_fixed((neq_fixed((id70x_1),(c_hw_fix_1_0_uns_bits_1))),id70in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id79out_output;

  { // Node ID: 79 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id79in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_input_doubt = id70out_o_doubt;

    id79out_output = id79in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id80out_o;

  { // Node ID: 80 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id80in_i = id79out_output;

    id80out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id80in_i));
  }
  { // Node ID: 520 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id520in_input = id80out_o;

    id520out_output[(getCycle()+2)%3] = id520in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id97out_o;

  { // Node ID: 97 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id97in_i = id520out_output[getCycle()%3];

    id97out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id97in_i));
  }
  { // Node ID: 100 (NodeConstantRawBits)
  }
  { // Node ID: 473 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id81out_o;

  { // Node ID: 81 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id81in_i = id79out_output;

    id81out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id81in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id139out_output;

  { // Node ID: 139 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id139in_input = id81out_o;

    id139out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id139in_input))));
  }
  { // Node ID: 140 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id140in_addr = id139out_output;

    HWOffsetFix<12,-12,UNSIGNED> id140x_1;

    switch(((long)((id140in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id140x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id140x_1 = (id140sta_rom_store[(id140in_addr.getValueAsLong())]);
        break;
      default:
        id140x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id140out_dout[(getCycle()+2)%3] = (id140x_1);
  }
  { // Node ID: 85 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id82in_i = id79out_output;

    id82out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id82in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id84out_result;

  { // Node ID: 84 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id84in_a = id85out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id84in_b = id82out_o;

    id84out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id84in_a,id84in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id86out_o;

  { // Node ID: 86 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id86in_i = id84out_result;

    id86out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id86in_i));
  }
  { // Node ID: 521 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id521in_input = id86out_o;

    id521out_output[(getCycle()+2)%3] = id521in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id87out_result;

  { // Node ID: 87 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id87in_a = id140out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id87in_b = id521out_output[getCycle()%3];

    id87out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id87in_a,id87in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id88out_result;

  { // Node ID: 88 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id88in_a = id521out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id88in_b = id140out_dout[getCycle()%3];

    id88out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id88in_a,id88in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id89out_result;

  { // Node ID: 89 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id89in_a = id87out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id89in_b = id88out_result;

    id89out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id89in_a,id89in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id90out_o;

  { // Node ID: 90 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id90in_i = id89out_result;

    id90out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id90in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id91out_o;

  { // Node ID: 91 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id91in_i = id90out_o;

    id91out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id91in_i));
  }
  { // Node ID: 586 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id454out_result;

  { // Node ID: 454 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id454in_a = id91out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id454in_b = id586out_value;

    id454out_result = (gte_fixed(id454in_a,id454in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id478out_result;

  { // Node ID: 478 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id478in_a = id97out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id478in_b = id100out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id478in_c = id473out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id478in_condb = id454out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id478x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id478x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id478x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id478x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id478x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id478x_1 = id478in_a;
        break;
      default:
        id478x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id478in_condb.getValueAsLong())) {
      case 0l:
        id478x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id478x_2 = id478in_b;
        break;
      default:
        id478x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id478x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id478x_3 = id478in_c;
        break;
      default:
        id478x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id478x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id478x_1),(id478x_2))),(id478x_3)));
    id478out_result = (id478x_4);
  }
  HWRawBits<1> id455out_result;

  { // Node ID: 455 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id455in_a = id478out_result;

    id455out_result = (slice<10,1>(id455in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id456out_output;

  { // Node ID: 456 (NodeReinterpret)
    const HWRawBits<1> &id456in_input = id455out_result;

    id456out_output = (cast_bits2fixed<1,0,UNSIGNED>(id456in_input));
  }
  { // Node ID: 585 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id72out_result;

  { // Node ID: 72 (NodeGt)
    const HWFloat<8,12> &id72in_a = id481out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id72in_b = id585out_value;

    id72out_result = (gt_float(id72in_a,id72in_b));
  }
  { // Node ID: 523 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id523in_input = id72out_result;

    id523out_output[(getCycle()+6)%7] = id523in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id73out_output;

  { // Node ID: 73 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id73in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id73in_input_doubt = id70out_o_doubt;

    id73out_output = id73in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id74out_result;

  { // Node ID: 74 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id74in_a = id523out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id74in_b = id73out_output;

    HWOffsetFix<1,0,UNSIGNED> id74x_1;

    (id74x_1) = (and_fixed(id74in_a,id74in_b));
    id74out_result = (id74x_1);
  }
  { // Node ID: 524 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id524in_input = id74out_result;

    id524out_output[(getCycle()+2)%3] = id524in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id106out_result;

  { // Node ID: 106 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id106in_a = id524out_output[getCycle()%3];

    id106out_result = (not_fixed(id106in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id107out_result;

  { // Node ID: 107 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id107in_a = id456out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id107in_b = id106out_result;

    HWOffsetFix<1,0,UNSIGNED> id107x_1;

    (id107x_1) = (and_fixed(id107in_a,id107in_b));
    id107out_result = (id107x_1);
  }
  { // Node ID: 584 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeLt)
    const HWFloat<8,12> &id76in_a = id481out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id76in_b = id584out_value;

    id76out_result = (lt_float(id76in_a,id76in_b));
  }
  { // Node ID: 525 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id525in_input = id76out_result;

    id525out_output[(getCycle()+6)%7] = id525in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id77out_output;

  { // Node ID: 77 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id77in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id77in_input_doubt = id70out_o_doubt;

    id77out_output = id77in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id78out_result;

  { // Node ID: 78 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id78in_a = id525out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id78in_b = id77out_output;

    HWOffsetFix<1,0,UNSIGNED> id78x_1;

    (id78x_1) = (and_fixed(id78in_a,id78in_b));
    id78out_result = (id78x_1);
  }
  { // Node ID: 526 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id526in_input = id78out_result;

    id526out_output[(getCycle()+2)%3] = id526in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id108out_result;

  { // Node ID: 108 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id108in_a = id107out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id108in_b = id526out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id108x_1;

    (id108x_1) = (or_fixed(id108in_a,id108in_b));
    id108out_result = (id108x_1);
  }
  { // Node ID: 583 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id457out_result;

  { // Node ID: 457 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id457in_a = id478out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id457in_b = id583out_value;

    id457out_result = (gte_fixed(id457in_a,id457in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id115out_result;

  { // Node ID: 115 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id115in_a = id526out_output[getCycle()%3];

    id115out_result = (not_fixed(id115in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id116out_result;

  { // Node ID: 116 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id116in_a = id457out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id116in_b = id115out_result;

    HWOffsetFix<1,0,UNSIGNED> id116x_1;

    (id116x_1) = (and_fixed(id116in_a,id116in_b));
    id116out_result = (id116x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id117out_result;

  { // Node ID: 117 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id117in_a = id116out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id117in_b = id524out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id117x_1;

    (id117x_1) = (or_fixed(id117in_a,id117in_b));
    id117out_result = (id117x_1);
  }
  HWRawBits<2> id118out_result;

  { // Node ID: 118 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id118in_in0 = id108out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id118in_in1 = id117out_result;

    id118out_result = (cat(id118in_in0,id118in_in1));
  }
  { // Node ID: 110 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id109out_o;

  { // Node ID: 109 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id109in_i = id478out_result;

    id109out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id109in_i));
  }
  { // Node ID: 94 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id95out_result;

  { // Node ID: 95 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id95in_sel = id454out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id95in_option0 = id91out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id95in_option1 = id94out_value;

    HWOffsetFix<12,-11,UNSIGNED> id95x_1;

    switch((id95in_sel.getValueAsLong())) {
      case 0l:
        id95x_1 = id95in_option0;
        break;
      case 1l:
        id95x_1 = id95in_option1;
        break;
      default:
        id95x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id95out_result = (id95x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id96out_o;

  { // Node ID: 96 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id96in_i = id95out_result;

    id96out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id96in_i));
  }
  HWRawBits<20> id111out_result;

  { // Node ID: 111 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_in0 = id110out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id111in_in1 = id109out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id111in_in2 = id96out_o;

    id111out_result = (cat((cat(id111in_in0,id111in_in1)),id111in_in2));
  }
  HWFloat<8,12> id112out_output;

  { // Node ID: 112 (NodeReinterpret)
    const HWRawBits<20> &id112in_input = id111out_result;

    id112out_output = (cast_bits2float<8,12>(id112in_input));
  }
  { // Node ID: 119 (NodeConstantRawBits)
  }
  { // Node ID: 120 (NodeConstantRawBits)
  }
  { // Node ID: 122 (NodeConstantRawBits)
  }
  HWRawBits<20> id458out_result;

  { // Node ID: 458 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id458in_in0 = id119out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id458in_in1 = id120out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id458in_in2 = id122out_value;

    id458out_result = (cat((cat(id458in_in0,id458in_in1)),id458in_in2));
  }
  HWFloat<8,12> id124out_output;

  { // Node ID: 124 (NodeReinterpret)
    const HWRawBits<20> &id124in_input = id458out_result;

    id124out_output = (cast_bits2float<8,12>(id124in_input));
  }
  { // Node ID: 439 (NodeConstantRawBits)
  }
  HWFloat<8,12> id127out_result;

  { // Node ID: 127 (NodeMux)
    const HWRawBits<2> &id127in_sel = id118out_result;
    const HWFloat<8,12> &id127in_option0 = id112out_output;
    const HWFloat<8,12> &id127in_option1 = id124out_output;
    const HWFloat<8,12> &id127in_option2 = id439out_value;
    const HWFloat<8,12> &id127in_option3 = id124out_output;

    HWFloat<8,12> id127x_1;

    switch((id127in_sel.getValueAsLong())) {
      case 0l:
        id127x_1 = id127in_option0;
        break;
      case 1l:
        id127x_1 = id127in_option1;
        break;
      case 2l:
        id127x_1 = id127in_option2;
        break;
      case 3l:
        id127x_1 = id127in_option3;
        break;
      default:
        id127x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id127out_result = (id127x_1);
  }
  { // Node ID: 582 (NodeConstantRawBits)
  }
  HWFloat<8,12> id137out_result;

  { // Node ID: 137 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id137in_sel = id529out_output[getCycle()%9];
    const HWFloat<8,12> &id137in_option0 = id127out_result;
    const HWFloat<8,12> &id137in_option1 = id582out_value;

    HWFloat<8,12> id137x_1;

    switch((id137in_sel.getValueAsLong())) {
      case 0l:
        id137x_1 = id137in_option0;
        break;
      case 1l:
        id137x_1 = id137in_option1;
        break;
      default:
        id137x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id137out_result = (id137x_1);
  }
  { // Node ID: 581 (NodeConstantRawBits)
  }
  HWFloat<8,12> id142out_result;

  { // Node ID: 142 (NodeAdd)
    const HWFloat<8,12> &id142in_a = id137out_result;
    const HWFloat<8,12> &id142in_b = id581out_value;

    id142out_result = (add_float(id142in_a,id142in_b));
  }
  HWFloat<8,12> id144out_result;

  { // Node ID: 144 (NodeDiv)
    const HWFloat<8,12> &id144in_a = id588out_value;
    const HWFloat<8,12> &id144in_b = id142out_result;

    id144out_result = (div_float(id144in_a,id144in_b));
  }
  HWFloat<8,12> id146out_result;

  { // Node ID: 146 (NodeSub)
    const HWFloat<8,12> &id146in_a = id589out_value;
    const HWFloat<8,12> &id146in_b = id144out_result;

    id146out_result = (sub_float(id146in_a,id146in_b));
  }
  HWFloat<8,12> id148out_result;

  { // Node ID: 148 (NodeAdd)
    const HWFloat<8,12> &id148in_a = id590out_value;
    const HWFloat<8,12> &id148in_b = id146out_result;

    id148out_result = (add_float(id148in_a,id148in_b));
  }
  HWFloat<8,12> id149out_result;

  { // Node ID: 149 (NodeMul)
    const HWFloat<8,12> &id149in_a = id35out_value;
    const HWFloat<8,12> &id149in_b = id148out_result;

    id149out_result = (mul_float(id149in_a,id149in_b));
  }
  HWFloat<8,12> id150out_result;

  { // Node ID: 150 (NodeAdd)
    const HWFloat<8,12> &id150in_a = id4out_value;
    const HWFloat<8,12> &id150in_b = id149out_result;

    id150out_result = (add_float(id150in_a,id150in_b));
  }
  HWFloat<8,12> id265out_result;

  { // Node ID: 265 (NodeDiv)
    const HWFloat<8,12> &id265in_a = id264out_result;
    const HWFloat<8,12> &id265in_b = id150out_result;

    id265out_result = (div_float(id265in_a,id265in_b));
  }
  HWFloat<8,12> id262out_result;

  { // Node ID: 262 (NodeNeg)
    const HWFloat<8,12> &id262in_a = id519out_output[getCycle()%10];

    id262out_result = (neg_float(id262in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,12> id263out_result;

  { // Node ID: 263 (NodeDiv)
    const HWFloat<8,12> &id263in_a = id262out_result;
    const HWFloat<8,12> &id263in_b = id3out_value;

    id263out_result = (div_float(id263in_a,id263in_b));
  }
  HWFloat<8,12> id266out_result;

  { // Node ID: 266 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id266in_sel = id261out_result;
    const HWFloat<8,12> &id266in_option0 = id265out_result;
    const HWFloat<8,12> &id266in_option1 = id263out_result;

    HWFloat<8,12> id266x_1;

    switch((id266in_sel.getValueAsLong())) {
      case 0l:
        id266x_1 = id266in_option0;
        break;
      case 1l:
        id266x_1 = id266in_option1;
        break;
      default:
        id266x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id266out_result = (id266x_1);
  }
  { // Node ID: 362 (NodeMul)
    const HWFloat<8,12> &id362in_a = id266out_result;
    const HWFloat<8,12> &id362in_b = id45out_o[getCycle()%4];

    id362out_result[(getCycle()+6)%7] = (mul_float(id362in_a,id362in_b));
  }
  { // Node ID: 363 (NodeAdd)
    const HWFloat<8,12> &id363in_a = id562out_output[getCycle()%7];
    const HWFloat<8,12> &id363in_b = id362out_result[getCycle()%7];

    id363out_result[(getCycle()+8)%9] = (add_float(id363in_a,id363in_b));
  }
  { // Node ID: 514 (NodeFIFO)
    const HWFloat<8,12> &id514in_input = id363out_result[getCycle()%9];

    id514out_output[(getCycle()+5)%6] = id514in_input;
  }
  HWFloat<8,12> id381out_result;

  { // Node ID: 381 (NodeNeg)
    const HWFloat<8,12> &id381in_a = id514out_output[getCycle()%6];

    id381out_result = (neg_float(id381in_a));
  }
  { // Node ID: 580 (NodeConstantRawBits)
  }
  { // Node ID: 459 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id459in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id459in_b = id580out_value;

    id459out_result[(getCycle()+1)%2] = (eq_fixed(id459in_a,id459in_b));
  }
  HWFloat<8,12> id492out_output;

  { // Node ID: 492 (NodeStreamOffset)
    const HWFloat<8,12> &id492in_input = id533out_output[getCycle()%5];

    id492out_output = id492in_input;
  }
  { // Node ID: 534 (NodeFIFO)
    const HWFloat<8,12> &id534in_input = id492out_output;

    id534out_output[(getCycle()+45)%46] = id534in_input;
  }
  { // Node ID: 57 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id57in_sel = id459out_result[getCycle()%2];
    const HWFloat<8,12> &id57in_option0 = id534out_output[getCycle()%46];
    const HWFloat<8,12> &id57in_option1 = id37out_value;

    HWFloat<8,12> id57x_1;

    switch((id57in_sel.getValueAsLong())) {
      case 0l:
        id57x_1 = id57in_option0;
        break;
      case 1l:
        id57x_1 = id57in_option1;
        break;
      default:
        id57x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id57out_result[(getCycle()+1)%2] = (id57x_1);
  }
  { // Node ID: 545 (NodeFIFO)
    const HWFloat<8,12> &id545in_input = id57out_result[getCycle()%2];

    id545out_output[(getCycle()+10)%11] = id545in_input;
  }
  { // Node ID: 563 (NodeFIFO)
    const HWFloat<8,12> &id563in_input = id545out_output[getCycle()%11];

    id563out_output[(getCycle()+6)%7] = id563in_input;
  }
  { // Node ID: 579 (NodeConstantRawBits)
  }
  { // Node ID: 578 (NodeConstantRawBits)
  }
  { // Node ID: 577 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeConstantRawBits)
  }
  HWFloat<8,12> id267out_result;

  { // Node ID: 267 (NodeSub)
    const HWFloat<8,12> &id267in_a = id48out_result[getCycle()%2];
    const HWFloat<8,12> &id267in_b = id27out_value;

    id267out_result = (sub_float(id267in_a,id267in_b));
  }
  HWFloat<8,12> id268out_result;

  { // Node ID: 268 (NodeMul)
    const HWFloat<8,12> &id268in_a = id24out_value;
    const HWFloat<8,12> &id268in_b = id267out_result;

    id268out_result = (mul_float(id268in_a,id268in_b));
  }
  { // Node ID: 482 (NodePO2FPMult)
    const HWFloat<8,12> &id482in_floatIn = id268out_result;

    id482out_floatOut[(getCycle()+1)%2] = (mul_float(id482in_floatIn,(c_hw_flt_8_12_2_0val)));
  }
  HWRawBits<8> id337out_result;

  { // Node ID: 337 (NodeSlice)
    const HWFloat<8,12> &id337in_a = id482out_floatOut[getCycle()%2];

    id337out_result = (slice<11,8>(id337in_a));
  }
  { // Node ID: 338 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id460out_result;

  { // Node ID: 460 (NodeEqInlined)
    const HWRawBits<8> &id460in_a = id337out_result;
    const HWRawBits<8> &id460in_b = id338out_value;

    id460out_result = (eq_bits(id460in_a,id460in_b));
  }
  HWRawBits<11> id336out_result;

  { // Node ID: 336 (NodeSlice)
    const HWFloat<8,12> &id336in_a = id482out_floatOut[getCycle()%2];

    id336out_result = (slice<0,11>(id336in_a));
  }
  { // Node ID: 576 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id461out_result;

  { // Node ID: 461 (NodeNeqInlined)
    const HWRawBits<11> &id461in_a = id336out_result;
    const HWRawBits<11> &id461in_b = id576out_value;

    id461out_result = (neq_bits(id461in_a,id461in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id342out_result;

  { // Node ID: 342 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id342in_a = id460out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id342in_b = id461out_result;

    HWOffsetFix<1,0,UNSIGNED> id342x_1;

    (id342x_1) = (and_fixed(id342in_a,id342in_b));
    id342out_result = (id342x_1);
  }
  { // Node ID: 544 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id544in_input = id342out_result;

    id544out_output[(getCycle()+8)%9] = id544in_input;
  }
  { // Node ID: 271 (NodeConstantRawBits)
  }
  HWFloat<8,12> id272out_output;
  HWOffsetFix<1,0,UNSIGNED> id272out_output_doubt;

  { // Node ID: 272 (NodeDoubtBitOp)
    const HWFloat<8,12> &id272in_input = id482out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id272in_doubt = id271out_value;

    id272out_output = id272in_input;
    id272out_output_doubt = id272in_doubt;
  }
  { // Node ID: 273 (NodeCast)
    const HWFloat<8,12> &id273in_i = id272out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id273in_i_doubt = id272out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id273x_1;

    id273out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id273in_i,(&(id273x_1))));
    id273out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id273x_1),(c_hw_fix_4_0_uns_bits))),id273in_i_doubt));
  }
  { // Node ID: 276 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id275out_result;
  HWOffsetFix<1,0,UNSIGNED> id275out_result_doubt;

  { // Node ID: 275 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id275in_a = id273out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id275in_a_doubt = id273out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-23,UNSIGNED> &id275in_b = id276out_value;

    HWOffsetFix<1,0,UNSIGNED> id275x_1;

    id275out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id275in_a,id275in_b,(&(id275x_1))));
    id275out_result_doubt = (or_fixed((neq_fixed((id275x_1),(c_hw_fix_1_0_uns_bits_1))),id275in_a_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id277out_o;
  HWOffsetFix<1,0,UNSIGNED> id277out_o_doubt;

  { // Node ID: 277 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id277in_i = id275out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id277in_i_doubt = id275out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id277x_1;

    id277out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id277in_i,(&(id277x_1))));
    id277out_o_doubt = (or_fixed((neq_fixed((id277x_1),(c_hw_fix_1_0_uns_bits_1))),id277in_i_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id286out_output;

  { // Node ID: 286 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id286in_input = id277out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id286in_input_doubt = id277out_o_doubt;

    id286out_output = id286in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id287out_o;

  { // Node ID: 287 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id287in_i = id286out_output;

    id287out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id287in_i));
  }
  { // Node ID: 535 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id535in_input = id287out_o;

    id535out_output[(getCycle()+2)%3] = id535in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id304out_o;

  { // Node ID: 304 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id304in_i = id535out_output[getCycle()%3];

    id304out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id304in_i));
  }
  { // Node ID: 307 (NodeConstantRawBits)
  }
  { // Node ID: 475 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id288out_o;

  { // Node ID: 288 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id288in_i = id286out_output;

    id288out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id288in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id346out_output;

  { // Node ID: 346 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id346in_input = id288out_o;

    id346out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id346in_input))));
  }
  { // Node ID: 347 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id347in_addr = id346out_output;

    HWOffsetFix<12,-12,UNSIGNED> id347x_1;

    switch(((long)((id347in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id347x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id347x_1 = (id347sta_rom_store[(id347in_addr.getValueAsLong())]);
        break;
      default:
        id347x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id347out_dout[(getCycle()+2)%3] = (id347x_1);
  }
  { // Node ID: 292 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id289out_o;

  { // Node ID: 289 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id289in_i = id286out_output;

    id289out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id289in_i));
  }
  HWOffsetFix<16,-22,UNSIGNED> id291out_result;

  { // Node ID: 291 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id291in_a = id292out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id291in_b = id289out_o;

    id291out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id291in_a,id291in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id293out_o;

  { // Node ID: 293 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id293in_i = id291out_result;

    id293out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id293in_i));
  }
  { // Node ID: 536 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id536in_input = id293out_o;

    id536out_output[(getCycle()+2)%3] = id536in_input;
  }
  HWOffsetFix<15,-14,UNSIGNED> id294out_result;

  { // Node ID: 294 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id294in_a = id347out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id294in_b = id536out_output[getCycle()%3];

    id294out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id294in_a,id294in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id295out_result;

  { // Node ID: 295 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id295in_a = id536out_output[getCycle()%3];
    const HWOffsetFix<12,-12,UNSIGNED> &id295in_b = id347out_dout[getCycle()%3];

    id295out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id295in_a,id295in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id296out_result;

  { // Node ID: 296 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id296in_a = id294out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id296in_b = id295out_result;

    id296out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id296in_a,id296in_b));
  }
  HWOffsetFix<15,-14,UNSIGNED> id297out_o;

  { // Node ID: 297 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id297in_i = id296out_result;

    id297out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id297in_i));
  }
  HWOffsetFix<12,-11,UNSIGNED> id298out_o;

  { // Node ID: 298 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id298in_i = id297out_o;

    id298out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id298in_i));
  }
  { // Node ID: 575 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id462out_result;

  { // Node ID: 462 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id462in_a = id298out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id462in_b = id575out_value;

    id462out_result = (gte_fixed(id462in_a,id462in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id477out_result;

  { // Node ID: 477 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id477in_a = id304out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id477in_b = id307out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id477in_c = id475out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id477in_condb = id462out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id477x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id477x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id477x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id477x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id477x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id477x_1 = id477in_a;
        break;
      default:
        id477x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id477in_condb.getValueAsLong())) {
      case 0l:
        id477x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id477x_2 = id477in_b;
        break;
      default:
        id477x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id477x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id477x_3 = id477in_c;
        break;
      default:
        id477x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id477x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id477x_1),(id477x_2))),(id477x_3)));
    id477out_result = (id477x_4);
  }
  HWRawBits<1> id463out_result;

  { // Node ID: 463 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id463in_a = id477out_result;

    id463out_result = (slice<10,1>(id463in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id464out_output;

  { // Node ID: 464 (NodeReinterpret)
    const HWRawBits<1> &id464in_input = id463out_result;

    id464out_output = (cast_bits2fixed<1,0,UNSIGNED>(id464in_input));
  }
  { // Node ID: 574 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id279out_result;

  { // Node ID: 279 (NodeGt)
    const HWFloat<8,12> &id279in_a = id482out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id279in_b = id574out_value;

    id279out_result = (gt_float(id279in_a,id279in_b));
  }
  { // Node ID: 538 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id538in_input = id279out_result;

    id538out_output[(getCycle()+6)%7] = id538in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id280out_output;

  { // Node ID: 280 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id280in_input = id277out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id280in_input_doubt = id277out_o_doubt;

    id280out_output = id280in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id281out_result;

  { // Node ID: 281 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id281in_a = id538out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id281in_b = id280out_output;

    HWOffsetFix<1,0,UNSIGNED> id281x_1;

    (id281x_1) = (and_fixed(id281in_a,id281in_b));
    id281out_result = (id281x_1);
  }
  { // Node ID: 539 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id539in_input = id281out_result;

    id539out_output[(getCycle()+2)%3] = id539in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id313in_a = id539out_output[getCycle()%3];

    id313out_result = (not_fixed(id313in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id314out_result;

  { // Node ID: 314 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id314in_a = id464out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id314in_b = id313out_result;

    HWOffsetFix<1,0,UNSIGNED> id314x_1;

    (id314x_1) = (and_fixed(id314in_a,id314in_b));
    id314out_result = (id314x_1);
  }
  { // Node ID: 573 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id283out_result;

  { // Node ID: 283 (NodeLt)
    const HWFloat<8,12> &id283in_a = id482out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id283in_b = id573out_value;

    id283out_result = (lt_float(id283in_a,id283in_b));
  }
  { // Node ID: 540 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id540in_input = id283out_result;

    id540out_output[(getCycle()+6)%7] = id540in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id284out_output;

  { // Node ID: 284 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id284in_input = id277out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id284in_input_doubt = id277out_o_doubt;

    id284out_output = id284in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id285out_result;

  { // Node ID: 285 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id285in_a = id540out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id285in_b = id284out_output;

    HWOffsetFix<1,0,UNSIGNED> id285x_1;

    (id285x_1) = (and_fixed(id285in_a,id285in_b));
    id285out_result = (id285x_1);
  }
  { // Node ID: 541 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id541in_input = id285out_result;

    id541out_output[(getCycle()+2)%3] = id541in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id315out_result;

  { // Node ID: 315 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id315in_a = id314out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id315in_b = id541out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id315x_1;

    (id315x_1) = (or_fixed(id315in_a,id315in_b));
    id315out_result = (id315x_1);
  }
  { // Node ID: 572 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id465out_result;

  { // Node ID: 465 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id465in_a = id477out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id465in_b = id572out_value;

    id465out_result = (gte_fixed(id465in_a,id465in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id322out_result;

  { // Node ID: 322 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id322in_a = id541out_output[getCycle()%3];

    id322out_result = (not_fixed(id322in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id323out_result;

  { // Node ID: 323 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id323in_a = id465out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id323in_b = id322out_result;

    HWOffsetFix<1,0,UNSIGNED> id323x_1;

    (id323x_1) = (and_fixed(id323in_a,id323in_b));
    id323out_result = (id323x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id324out_result;

  { // Node ID: 324 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id324in_a = id323out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id324in_b = id539out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id324x_1;

    (id324x_1) = (or_fixed(id324in_a,id324in_b));
    id324out_result = (id324x_1);
  }
  HWRawBits<2> id325out_result;

  { // Node ID: 325 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id325in_in0 = id315out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id325in_in1 = id324out_result;

    id325out_result = (cat(id325in_in0,id325in_in1));
  }
  { // Node ID: 317 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id316out_o;

  { // Node ID: 316 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id316in_i = id477out_result;

    id316out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id316in_i));
  }
  { // Node ID: 301 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id302out_result;

  { // Node ID: 302 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id302in_sel = id462out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id302in_option0 = id298out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id302in_option1 = id301out_value;

    HWOffsetFix<12,-11,UNSIGNED> id302x_1;

    switch((id302in_sel.getValueAsLong())) {
      case 0l:
        id302x_1 = id302in_option0;
        break;
      case 1l:
        id302x_1 = id302in_option1;
        break;
      default:
        id302x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id302out_result = (id302x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id303out_o;

  { // Node ID: 303 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id303in_i = id302out_result;

    id303out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id303in_i));
  }
  HWRawBits<20> id318out_result;

  { // Node ID: 318 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id318in_in0 = id317out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id318in_in1 = id316out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id318in_in2 = id303out_o;

    id318out_result = (cat((cat(id318in_in0,id318in_in1)),id318in_in2));
  }
  HWFloat<8,12> id319out_output;

  { // Node ID: 319 (NodeReinterpret)
    const HWRawBits<20> &id319in_input = id318out_result;

    id319out_output = (cast_bits2float<8,12>(id319in_input));
  }
  { // Node ID: 326 (NodeConstantRawBits)
  }
  { // Node ID: 327 (NodeConstantRawBits)
  }
  { // Node ID: 329 (NodeConstantRawBits)
  }
  HWRawBits<20> id466out_result;

  { // Node ID: 466 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id466in_in0 = id326out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id466in_in1 = id327out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id466in_in2 = id329out_value;

    id466out_result = (cat((cat(id466in_in0,id466in_in1)),id466in_in2));
  }
  HWFloat<8,12> id331out_output;

  { // Node ID: 331 (NodeReinterpret)
    const HWRawBits<20> &id331in_input = id466out_result;

    id331out_output = (cast_bits2float<8,12>(id331in_input));
  }
  { // Node ID: 440 (NodeConstantRawBits)
  }
  HWFloat<8,12> id334out_result;

  { // Node ID: 334 (NodeMux)
    const HWRawBits<2> &id334in_sel = id325out_result;
    const HWFloat<8,12> &id334in_option0 = id319out_output;
    const HWFloat<8,12> &id334in_option1 = id331out_output;
    const HWFloat<8,12> &id334in_option2 = id440out_value;
    const HWFloat<8,12> &id334in_option3 = id331out_output;

    HWFloat<8,12> id334x_1;

    switch((id334in_sel.getValueAsLong())) {
      case 0l:
        id334x_1 = id334in_option0;
        break;
      case 1l:
        id334x_1 = id334in_option1;
        break;
      case 2l:
        id334x_1 = id334in_option2;
        break;
      case 3l:
        id334x_1 = id334in_option3;
        break;
      default:
        id334x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id334out_result = (id334x_1);
  }
  { // Node ID: 571 (NodeConstantRawBits)
  }
  HWFloat<8,12> id344out_result;

  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id544out_output[getCycle()%9];
    const HWFloat<8,12> &id344in_option0 = id334out_result;
    const HWFloat<8,12> &id344in_option1 = id571out_value;

    HWFloat<8,12> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id344out_result = (id344x_1);
  }
  { // Node ID: 570 (NodeConstantRawBits)
  }
  HWFloat<8,12> id349out_result;

  { // Node ID: 349 (NodeAdd)
    const HWFloat<8,12> &id349in_a = id344out_result;
    const HWFloat<8,12> &id349in_b = id570out_value;

    id349out_result = (add_float(id349in_a,id349in_b));
  }
  HWFloat<8,12> id351out_result;

  { // Node ID: 351 (NodeDiv)
    const HWFloat<8,12> &id351in_a = id577out_value;
    const HWFloat<8,12> &id351in_b = id349out_result;

    id351out_result = (div_float(id351in_a,id351in_b));
  }
  HWFloat<8,12> id353out_result;

  { // Node ID: 353 (NodeSub)
    const HWFloat<8,12> &id353in_a = id578out_value;
    const HWFloat<8,12> &id353in_b = id351out_result;

    id353out_result = (sub_float(id353in_a,id353in_b));
  }
  HWFloat<8,12> id355out_result;

  { // Node ID: 355 (NodeAdd)
    const HWFloat<8,12> &id355in_a = id579out_value;
    const HWFloat<8,12> &id355in_b = id353out_result;

    id355out_result = (add_float(id355in_a,id355in_b));
  }
  { // Node ID: 483 (NodePO2FPMult)
    const HWFloat<8,12> &id483in_floatIn = id355out_result;

    id483out_floatOut[(getCycle()+1)%2] = (mul_float(id483in_floatIn,(c_hw_flt_8_12_0_5val)));
  }
  HWFloat<8,12> id358out_result;

  { // Node ID: 358 (NodeSub)
    const HWFloat<8,12> &id358in_a = id483out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id358in_b = id545out_output[getCycle()%11];

    id358out_result = (sub_float(id358in_a,id358in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id242out_result;

  { // Node ID: 242 (NodeGt)
    const HWFloat<8,12> &id242in_a = id555out_output[getCycle()%2];
    const HWFloat<8,12> &id242in_b = id18out_value;

    id242out_result = (gt_float(id242in_a,id242in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  HWFloat<8,12> id243out_result;

  { // Node ID: 243 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id243in_sel = id242out_result;
    const HWFloat<8,12> &id243in_option0 = id6out_value;
    const HWFloat<8,12> &id243in_option1 = id7out_value;

    HWFloat<8,12> id243x_1;

    switch((id243in_sel.getValueAsLong())) {
      case 0l:
        id243x_1 = id243in_option0;
        break;
      case 1l:
        id243x_1 = id243in_option1;
        break;
      default:
        id243x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id243out_result = (id243x_1);
  }
  HWFloat<8,12> id359out_result;

  { // Node ID: 359 (NodeDiv)
    const HWFloat<8,12> &id359in_a = id358out_result;
    const HWFloat<8,12> &id359in_b = id243out_result;

    id359out_result = (div_float(id359in_a,id359in_b));
  }
  { // Node ID: 364 (NodeMul)
    const HWFloat<8,12> &id364in_a = id359out_result;
    const HWFloat<8,12> &id364in_b = id45out_o[getCycle()%4];

    id364out_result[(getCycle()+6)%7] = (mul_float(id364in_a,id364in_b));
  }
  { // Node ID: 365 (NodeAdd)
    const HWFloat<8,12> &id365in_a = id563out_output[getCycle()%7];
    const HWFloat<8,12> &id365in_b = id364out_result[getCycle()%7];

    id365out_result[(getCycle()+8)%9] = (add_float(id365in_a,id365in_b));
  }
  { // Node ID: 533 (NodeFIFO)
    const HWFloat<8,12> &id533in_input = id365out_result[getCycle()%9];

    id533out_output[(getCycle()+4)%5] = id533in_input;
  }
  { // Node ID: 382 (NodeMul)
    const HWFloat<8,12> &id382in_a = id381out_result;
    const HWFloat<8,12> &id382in_b = id533out_output[getCycle()%5];

    id382out_result[(getCycle()+6)%7] = (mul_float(id382in_a,id382in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  { // Node ID: 383 (NodeDiv)
    const HWFloat<8,12> &id383in_a = id382out_result[getCycle()%7];
    const HWFloat<8,12> &id383in_b = id13out_value;

    id383out_result[(getCycle()+16)%17] = (div_float(id383in_a,id383in_b));
  }
  { // Node ID: 385 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id385in_sel = id380out_result[getCycle()%3];
    const HWFloat<8,12> &id385in_option0 = id384out_value;
    const HWFloat<8,12> &id385in_option1 = id383out_result[getCycle()%17];

    HWFloat<8,12> id385x_1;

    switch((id385in_sel.getValueAsLong())) {
      case 0l:
        id385x_1 = id385in_option0;
        break;
      case 1l:
        id385x_1 = id385in_option1;
        break;
      default:
        id385x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id385out_result[(getCycle()+1)%2] = (id385x_1);
  }
  { // Node ID: 387 (NodeAdd)
    const HWFloat<8,12> &id387in_a = id386out_result[getCycle()%9];
    const HWFloat<8,12> &id387in_b = id385out_result[getCycle()%2];

    id387out_result[(getCycle()+8)%9] = (add_float(id387in_a,id387in_b));
  }
  { // Node ID: 388 (NodeMul)
    const HWFloat<8,12> &id388in_a = id387out_result[getCycle()%9];
    const HWFloat<8,12> &id388in_b = id45out_o[getCycle()%4];

    id388out_result[(getCycle()+6)%7] = (mul_float(id388in_a,id388in_b));
  }
  { // Node ID: 389 (NodeSub)
    const HWFloat<8,12> &id389in_a = id561out_output[getCycle()%18];
    const HWFloat<8,12> &id389in_b = id388out_result[getCycle()%7];

    id389out_result[(getCycle()+8)%9] = (sub_float(id389in_a,id389in_b));
  }
  HWFloat<8,24> id394out_o;

  { // Node ID: 394 (NodeCast)
    const HWFloat<8,12> &id394in_i = id389out_result[getCycle()%9];

    id394out_o = (cast_float2float<8,24>(id394in_i));
  }
  if ( (getFillLevel() >= (78l)) && (getFlushLevel() < (78l)|| !isFlushingActive() ))
  { // Node ID: 401 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id401in_output_control = id550out_output[getCycle()%75];
    const HWFloat<8,24> &id401in_data = id394out_o;

    bool id401x_1;

    (id401x_1) = ((id401in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(78l))&(isFlushingActive()))));
    if((id401x_1)) {
      writeOutput(m_u_out, id401in_data);
    }
  }
  { // Node ID: 569 (NodeConstantRawBits)
  }
  { // Node ID: 404 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id404in_a = id564out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id404in_b = id569out_value;

    id404out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id404in_a,id404in_b));
  }
  { // Node ID: 467 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id467in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id467in_b = id404out_result[getCycle()%2];

    id467out_result[(getCycle()+1)%2] = (eq_fixed(id467in_a,id467in_b));
  }
  { // Node ID: 406 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id407out_result;

  { // Node ID: 407 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id407in_a = id406out_io_v_out_force_disabled;

    id407out_result = (not_fixed(id407in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id408out_result;

  { // Node ID: 408 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id408in_a = id467out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id408in_b = id407out_result;

    HWOffsetFix<1,0,UNSIGNED> id408x_1;

    (id408x_1) = (and_fixed(id408in_a,id408in_b));
    id408out_result = (id408x_1);
  }
  HWFloat<8,24> id402out_o;

  { // Node ID: 402 (NodeCast)
    const HWFloat<8,12> &id402in_i = id495out_output[getCycle()%60];

    id402out_o = (cast_float2float<8,24>(id402in_i));
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 409 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id409in_output_control = id408out_result;
    const HWFloat<8,24> &id409in_data = id402out_o;

    bool id409x_1;

    (id409x_1) = ((id409in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id409x_1)) {
      writeOutput(m_v_out, id409in_data);
    }
  }
  { // Node ID: 568 (NodeConstantRawBits)
  }
  { // Node ID: 412 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id412in_a = id564out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id412in_b = id568out_value;

    id412out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id412in_a,id412in_b));
  }
  { // Node ID: 468 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id468in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id468in_b = id412out_result[getCycle()%2];

    id468out_result[(getCycle()+1)%2] = (eq_fixed(id468in_a,id468in_b));
  }
  { // Node ID: 414 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id415out_result;

  { // Node ID: 415 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id415in_a = id414out_io_w_out_force_disabled;

    id415out_result = (not_fixed(id415in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id416out_result;

  { // Node ID: 416 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id416in_a = id468out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id416in_b = id415out_result;

    HWOffsetFix<1,0,UNSIGNED> id416x_1;

    (id416x_1) = (and_fixed(id416in_a,id416in_b));
    id416out_result = (id416x_1);
  }
  HWFloat<8,24> id410out_o;

  { // Node ID: 410 (NodeCast)
    const HWFloat<8,12> &id410in_i = id515out_output[getCycle()%46];

    id410out_o = (cast_float2float<8,24>(id410in_i));
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 417 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id417in_output_control = id416out_result;
    const HWFloat<8,24> &id417in_data = id410out_o;

    bool id417x_1;

    (id417x_1) = ((id417in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id417x_1)) {
      writeOutput(m_w_out, id417in_data);
    }
  }
  { // Node ID: 567 (NodeConstantRawBits)
  }
  { // Node ID: 420 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id420in_a = id564out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id420in_b = id567out_value;

    id420out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id420in_a,id420in_b));
  }
  { // Node ID: 469 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id469in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id469in_b = id420out_result[getCycle()%2];

    id469out_result[(getCycle()+1)%2] = (eq_fixed(id469in_a,id469in_b));
  }
  { // Node ID: 422 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id423out_result;

  { // Node ID: 423 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id423in_a = id422out_io_s_out_force_disabled;

    id423out_result = (not_fixed(id423in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id424out_result;

  { // Node ID: 424 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id424in_a = id469out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id424in_b = id423out_result;

    HWOffsetFix<1,0,UNSIGNED> id424x_1;

    (id424x_1) = (and_fixed(id424in_a,id424in_b));
    id424out_result = (id424x_1);
  }
  HWFloat<8,24> id418out_o;

  { // Node ID: 418 (NodeCast)
    const HWFloat<8,12> &id418in_i = id534out_output[getCycle()%46];

    id418out_o = (cast_float2float<8,24>(id418in_i));
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 425 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id425in_output_control = id424out_result;
    const HWFloat<8,24> &id425in_data = id418out_o;

    bool id425x_1;

    (id425x_1) = ((id425in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id425x_1)) {
      writeOutput(m_s_out, id425in_data);
    }
  }
  { // Node ID: 430 (NodeConstantRawBits)
  }
  { // Node ID: 566 (NodeConstantRawBits)
  }
  { // Node ID: 427 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 428 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id428in_enable = id566out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id428in_max = id427out_value;

    HWOffsetFix<49,0,UNSIGNED> id428x_1;
    HWOffsetFix<1,0,UNSIGNED> id428x_2;
    HWOffsetFix<1,0,UNSIGNED> id428x_3;
    HWOffsetFix<49,0,UNSIGNED> id428x_4t_1e_1;

    id428out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id428st_count)));
    (id428x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id428st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id428x_2) = (gte_fixed((id428x_1),id428in_max));
    (id428x_3) = (and_fixed((id428x_2),id428in_enable));
    id428out_wrap = (id428x_3);
    if((id428in_enable.getValueAsBool())) {
      if(((id428x_3).getValueAsBool())) {
        (id428st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id428x_4t_1e_1) = (id428x_1);
        (id428st_count) = (id428x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id429out_output;

  { // Node ID: 429 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id429in_input = id428out_count;

    id429out_output = id429in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 431 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id431in_load = id430out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id431in_data = id429out_output;

    bool id431x_1;

    (id431x_1) = ((id431in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id431x_1)) {
      setMappedRegValue("current_run_cycle_count", id431in_data);
    }
  }
  { // Node ID: 565 (NodeConstantRawBits)
  }
  { // Node ID: 433 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 434 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id434in_enable = id565out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id434in_max = id433out_value;

    HWOffsetFix<49,0,UNSIGNED> id434x_1;
    HWOffsetFix<1,0,UNSIGNED> id434x_2;
    HWOffsetFix<1,0,UNSIGNED> id434x_3;
    HWOffsetFix<49,0,UNSIGNED> id434x_4t_1e_1;

    id434out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id434st_count)));
    (id434x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id434st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id434x_2) = (gte_fixed((id434x_1),id434in_max));
    (id434x_3) = (and_fixed((id434x_2),id434in_enable));
    id434out_wrap = (id434x_3);
    if((id434in_enable.getValueAsBool())) {
      if(((id434x_3).getValueAsBool())) {
        (id434st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id434x_4t_1e_1) = (id434x_1);
        (id434st_count) = (id434x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 436 (NodeInputMappedReg)
  }
  { // Node ID: 470 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id470in_a = id434out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id470in_b = id436out_run_cycle_count;

    id470out_result[(getCycle()+1)%2] = (eq_fixed(id470in_a,id470in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 435 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id435in_start = id470out_result[getCycle()%2];

    if((id435in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
