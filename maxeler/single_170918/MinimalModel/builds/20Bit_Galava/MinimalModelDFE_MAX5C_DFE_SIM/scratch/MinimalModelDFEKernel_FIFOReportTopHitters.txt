Top 10 largest FIFOs
     Costs                                              From                                                To
         4                       MinimalModelKernel.maxj:493                       MinimalModelKernel.maxj:493
         3                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:172
         3                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:206
         2                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:215
         2                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:212
         2                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:213
         2                       MinimalModelKernel.maxj:158                       MinimalModelKernel.maxj:203
         2                       MinimalModelKernel.maxj:157                       MinimalModelKernel.maxj:176
         2                       MinimalModelKernel.maxj:155                       MinimalModelKernel.maxj:169
         2                       MinimalModelKernel.maxj:157                       MinimalModelKernel.maxj:202

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
         4                       MinimalModelKernel.maxj:213         4
         4                       MinimalModelKernel.maxj:493         2
         3                       MinimalModelKernel.maxj:206         3
         3                       MinimalModelKernel.maxj:176         2
         3                       MinimalModelKernel.maxj:172         2
         2                       MinimalModelKernel.maxj:203         2
         2                       MinimalModelKernel.maxj:215         2
         2                       MinimalModelKernel.maxj:212         2
         2                       MinimalModelKernel.maxj:169         2
         2                       MinimalModelKernel.maxj:202         2

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
        17                       MinimalModelKernel.maxj:155        16
         4                       MinimalModelKernel.maxj:157         3
         4                       MinimalModelKernel.maxj:493         2
         3                       MinimalModelKernel.maxj:158         3
         2                       MinimalModelKernel.maxj:203         2
         2                       MinimalModelKernel.maxj:202         2
         2                       MinimalModelKernel.maxj:221         2
         2                       MinimalModelKernel.maxj:222         2
         2                       MinimalModelKernel.maxj:220         2
         1                       MinimalModelKernel.maxj:156         1
