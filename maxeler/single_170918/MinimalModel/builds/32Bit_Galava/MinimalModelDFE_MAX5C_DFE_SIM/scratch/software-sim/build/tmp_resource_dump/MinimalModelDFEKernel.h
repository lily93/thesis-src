#ifndef MINIMALMODELDFEKERNEL_H_
#define MINIMALMODELDFEKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class MinimalModelDFEKernel : public KernelManagerBlockSync {
public:
  MinimalModelDFEKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_v_out;
  t_port_number m_w_out;
  t_port_number m_s_out;
  HWOffsetFix<1,0,UNSIGNED> id40out_value;

  HWOffsetFix<11,0,UNSIGNED> id588out_value;

  HWOffsetFix<11,0,UNSIGNED> id517out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id43out_count;
  HWOffsetFix<1,0,UNSIGNED> id43out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id43st_count;

  HWOffsetFix<11,0,UNSIGNED> id630out_value;

  HWOffsetFix<11,0,UNSIGNED> id420out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id465out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id422out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id574out_output[107];

  HWOffsetFix<32,0,UNSIGNED> id41out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id42out_count;
  HWOffsetFix<1,0,UNSIGNED> id42out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id42st_count;

  HWOffsetFix<32,0,UNSIGNED> id629out_value;

  HWOffsetFix<1,0,UNSIGNED> id466out_result[2];

  HWFloat<8,24> id38out_value;

  HWFloat<8,24> id48out_result[2];

  HWFloat<8,24> id521out_output[9];

  HWFloat<8,24> id578out_output[2];

  HWFloat<8,24> id579out_output[2];

  HWFloat<8,24> id580out_output[7];

  HWFloat<8,24> id581out_output[11];

  HWFloat<8,24> id582out_output[10];

  HWFloat<8,24> id583out_output[27];

  HWFloat<8,24> id584out_output[12];

  HWFloat<8,24> id585out_output[23];

  HWFloat<8,24> id15out_value;

  HWOffsetFix<1,0,UNSIGNED> id390out_result[3];

  HWFloat<8,24> id37out_value;

  HWOffsetFix<32,0,UNSIGNED> id628out_value;

  HWOffsetFix<1,0,UNSIGNED> id467out_result[2];

  HWFloat<8,24> id519out_output[87];

  HWFloat<8,24> id51out_result[2];

  HWFloat<8,24> id520out_output[9];

  HWFloat<8,24> id17out_value;

  HWFloat<8,24> id16out_value;

  HWFloat<8,24> id1out_value;

  HWFloat<8,24> id2out_value;

  HWFloat<8,24> id0out_value;

  HWFloat<11,53> id44out_dt;

  HWFloat<8,24> id45out_o[4];

  HWFloat<8,24> id384out_result[9];

  HWFloat<8,24> id385out_result[12];

  HWFloat<8,24> id392out_result[12];

  HWFloat<8,24> id393out_result[9];

  HWFloat<8,24> id29out_value;

  HWFloat<8,24> id394out_result[12];

  HWFloat<8,24> id395out_result[9];

  HWFloat<8,24> id8out_value;

  HWFloat<8,24> id396out_result[29];

  HWFloat<8,24> id397out_result[2];

  HWFloat<8,24> id20out_value;

  HWOffsetFix<1,0,UNSIGNED> id398out_result[3];

  HWFloat<8,24> id22out_value;

  HWFloat<8,24> id9out_value;

  HWFloat<8,24> id10out_value;

  HWFloat<8,24> id402out_result[29];

  HWFloat<8,24> id627out_value;

  HWFloat<8,24> id11out_value;

  HWFloat<8,24> id36out_value;

  HWFloat<8,24> id626out_value;

  HWFloat<8,24> id625out_value;

  HWFloat<8,24> id624out_value;

  HWFloat<8,24> id25out_value;

  HWFloat<8,24> id30out_value;

  HWFloat<8,24> id504out_floatOut[2];

  HWRawBits<8> id235out_value;

  HWRawBits<23> id623out_value;

  HWOffsetFix<1,0,UNSIGNED> id536out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id163out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id165out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id165out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id168out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id527out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id204out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id495out_value;

  HWOffsetFix<22,-24,UNSIGNED> id247out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id247sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id244out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id244sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id185out_value;

  HWOffsetFix<14,-26,UNSIGNED> id528out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id622out_value;

  HWFloat<8,24> id621out_value;

  HWOffsetFix<1,0,UNSIGNED> id530out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id531out_output[3];

  HWFloat<8,24> id620out_value;

  HWOffsetFix<1,0,UNSIGNED> id532out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id533out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id619out_value;

  HWOffsetFix<1,0,UNSIGNED> id214out_value;

  HWOffsetFix<24,-23,UNSIGNED> id198out_value;

  HWOffsetFix<1,0,UNSIGNED> id223out_value;

  HWOffsetFix<8,0,UNSIGNED> id224out_value;

  HWOffsetFix<23,0,UNSIGNED> id226out_value;

  HWFloat<8,24> id462out_value;

  HWFloat<8,24> id618out_value;

  HWFloat<8,24> id617out_value;

  HWFloat<8,24> id400out_result[29];

  HWFloat<8,24> id403out_result[2];

  HWFloat<8,24> id410out_result[12];

  HWFloat<8,24> id21out_value;

  HWOffsetFix<1,0,UNSIGNED> id404out_result[3];

  HWFloat<8,24> id408out_value;

  HWOffsetFix<32,0,UNSIGNED> id616out_value;

  HWOffsetFix<1,0,UNSIGNED> id475out_result[2];

  HWFloat<8,24> id539out_output[68];

  HWFloat<8,24> id54out_result[2];

  HWFloat<8,24> id543out_output[10];

  HWFloat<8,24> id586out_output[9];

  HWFloat<8,24> id18out_value;

  HWFloat<8,24> id19out_value;

  HWFloat<8,24> id615out_value;

  HWFloat<8,24> id14out_value;

  HWFloat<8,24> id32out_value;

  HWFloat<8,24> id4out_value;

  HWFloat<8,24> id35out_value;

  HWFloat<8,24> id614out_value;

  HWFloat<8,24> id613out_value;

  HWFloat<8,24> id612out_value;

  HWFloat<8,24> id23out_value;

  HWFloat<8,24> id26out_value;

  HWFloat<8,24> id505out_floatOut[2];

  HWRawBits<8> id136out_value;

  HWRawBits<23> id611out_value;

  HWOffsetFix<1,0,UNSIGNED> id553out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id64out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id66out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id66out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id69out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id544out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id105out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id497out_value;

  HWOffsetFix<22,-24,UNSIGNED> id148out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id148sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id145out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id145sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id86out_value;

  HWOffsetFix<14,-26,UNSIGNED> id545out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id610out_value;

  HWFloat<8,24> id609out_value;

  HWOffsetFix<1,0,UNSIGNED> id547out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id548out_output[3];

  HWFloat<8,24> id608out_value;

  HWOffsetFix<1,0,UNSIGNED> id549out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id550out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id607out_value;

  HWOffsetFix<1,0,UNSIGNED> id115out_value;

  HWOffsetFix<24,-23,UNSIGNED> id99out_value;

  HWOffsetFix<1,0,UNSIGNED> id124out_value;

  HWOffsetFix<8,0,UNSIGNED> id125out_value;

  HWOffsetFix<23,0,UNSIGNED> id127out_value;

  HWFloat<8,24> id463out_value;

  HWFloat<8,24> id606out_value;

  HWFloat<8,24> id605out_value;

  HWFloat<8,24> id3out_value;

  HWFloat<8,24> id386out_result[9];

  HWFloat<8,24> id387out_result[12];

  HWFloat<8,24> id538out_output[11];

  HWOffsetFix<32,0,UNSIGNED> id604out_value;

  HWOffsetFix<1,0,UNSIGNED> id483out_result[2];

  HWFloat<8,24> id558out_output[68];

  HWFloat<8,24> id57out_result[2];

  HWFloat<8,24> id569out_output[11];

  HWFloat<8,24> id587out_output[9];

  HWFloat<8,24> id603out_value;

  HWFloat<8,24> id602out_value;

  HWFloat<8,24> id601out_value;

  HWFloat<8,24> id24out_value;

  HWFloat<8,24> id27out_value;

  HWFloat<8,24> id506out_floatOut[2];

  HWRawBits<8> id359out_value;

  HWRawBits<23> id600out_value;

  HWOffsetFix<1,0,UNSIGNED> id568out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id287out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id289out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id289out_o_doubt[7];

  HWOffsetFix<35,-34,UNSIGNED> id292out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id559out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id328out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id499out_value;

  HWOffsetFix<22,-24,UNSIGNED> id371out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id371sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id368out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id368sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id309out_value;

  HWOffsetFix<14,-26,UNSIGNED> id560out_output[3];

  HWOffsetFix<24,-23,UNSIGNED> id599out_value;

  HWFloat<8,24> id598out_value;

  HWOffsetFix<1,0,UNSIGNED> id562out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id563out_output[3];

  HWFloat<8,24> id597out_value;

  HWOffsetFix<1,0,UNSIGNED> id564out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id565out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id596out_value;

  HWOffsetFix<1,0,UNSIGNED> id338out_value;

  HWOffsetFix<24,-23,UNSIGNED> id322out_value;

  HWOffsetFix<1,0,UNSIGNED> id347out_value;

  HWOffsetFix<8,0,UNSIGNED> id348out_value;

  HWOffsetFix<23,0,UNSIGNED> id350out_value;

  HWFloat<8,24> id464out_value;

  HWFloat<8,24> id595out_value;

  HWFloat<8,24> id594out_value;

  HWFloat<8,24> id507out_floatOut[2];

  HWFloat<8,24> id6out_value;

  HWFloat<8,24> id7out_value;

  HWFloat<8,24> id388out_result[9];

  HWFloat<8,24> id389out_result[12];

  HWFloat<8,24> id557out_output[10];

  HWFloat<8,24> id406out_result[9];

  HWFloat<8,24> id13out_value;

  HWFloat<8,24> id407out_result[29];

  HWFloat<8,24> id409out_result[2];

  HWFloat<8,24> id411out_result[12];

  HWFloat<8,24> id412out_result[9];

  HWFloat<8,24> id413out_result[12];

  HWOffsetFix<11,0,UNSIGNED> id593out_value;

  HWOffsetFix<11,0,UNSIGNED> id428out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id491out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id430out_io_v_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id592out_value;

  HWOffsetFix<11,0,UNSIGNED> id436out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id492out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id438out_io_w_out_force_disabled;

  HWOffsetFix<11,0,UNSIGNED> id591out_value;

  HWOffsetFix<11,0,UNSIGNED> id444out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id493out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id446out_io_s_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id454out_value;

  HWOffsetFix<1,0,UNSIGNED> id590out_value;

  HWOffsetFix<49,0,UNSIGNED> id451out_value;

  HWOffsetFix<48,0,UNSIGNED> id452out_count;
  HWOffsetFix<1,0,UNSIGNED> id452out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id452st_count;

  HWOffsetFix<1,0,UNSIGNED> id589out_value;

  HWOffsetFix<49,0,UNSIGNED> id457out_value;

  HWOffsetFix<48,0,UNSIGNED> id458out_count;
  HWOffsetFix<1,0,UNSIGNED> id458out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id458st_count;

  HWOffsetFix<48,0,UNSIGNED> id460out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id494out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWFloat<8,24> c_hw_flt_8_24_bits_1;
  const HWFloat<8,24> c_hw_flt_8_24_bits_2;
  const HWFloat<8,24> c_hw_flt_8_24_bits_3;
  const HWFloat<8,24> c_hw_flt_8_24_bits_4;
  const HWFloat<8,24> c_hw_flt_8_24_bits_5;
  const HWFloat<8,24> c_hw_flt_8_24_bits_6;
  const HWFloat<8,24> c_hw_flt_8_24_bits_7;
  const HWFloat<8,24> c_hw_flt_8_24_bits_8;
  const HWFloat<8,24> c_hw_flt_8_24_bits_9;
  const HWFloat<8,24> c_hw_flt_8_24_bits_10;
  const HWFloat<8,24> c_hw_flt_8_24_bits_11;
  const HWFloat<8,24> c_hw_flt_8_24_bits_12;
  const HWFloat<8,24> c_hw_flt_8_24_bits_13;
  const HWFloat<8,24> c_hw_flt_8_24_bits_14;
  const HWFloat<8,24> c_hw_flt_8_24_bits_15;
  const HWFloat<8,24> c_hw_flt_8_24_bits_16;
  const HWFloat<8,24> c_hw_flt_8_24_2_0val;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<23> c_hw_bit_23_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<35,-34,UNSIGNED> c_hw_fix_35_n34_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<22,-24,UNSIGNED> c_hw_fix_22_n24_uns_undef;
  const HWOffsetFix<24,-24,UNSIGNED> c_hw_fix_24_n24_uns_undef;
  const HWOffsetFix<14,-14,UNSIGNED> c_hw_fix_14_n14_uns_bits;
  const HWOffsetFix<14,-26,UNSIGNED> c_hw_fix_14_n26_uns_undef;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits_1;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<23,0,UNSIGNED> c_hw_fix_23_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits_17;
  const HWFloat<8,24> c_hw_flt_8_24_bits_18;
  const HWFloat<8,24> c_hw_flt_8_24_bits_19;
  const HWFloat<8,24> c_hw_flt_8_24_bits_20;
  const HWFloat<8,24> c_hw_flt_8_24_bits_21;
  const HWFloat<8,24> c_hw_flt_8_24_bits_22;
  const HWFloat<8,24> c_hw_flt_8_24_bits_23;
  const HWFloat<8,24> c_hw_flt_8_24_bits_24;
  const HWFloat<8,24> c_hw_flt_8_24_bits_25;
  const HWFloat<8,24> c_hw_flt_8_24_0_5val;
  const HWFloat<8,24> c_hw_flt_8_24_bits_26;
  const HWFloat<8,24> c_hw_flt_8_24_bits_27;
  const HWFloat<8,24> c_hw_flt_8_24_bits_28;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* MINIMALMODELDFEKERNEL_H_ */
