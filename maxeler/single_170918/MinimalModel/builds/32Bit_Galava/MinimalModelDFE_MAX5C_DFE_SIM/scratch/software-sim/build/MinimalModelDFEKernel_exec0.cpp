#include "stdsimheader.h"

namespace maxcompilersim {

void MinimalModelDFEKernel::execute0() {
  { // Node ID: 40 (NodeConstantRawBits)
  }
  { // Node ID: 588 (NodeConstantRawBits)
  }
  { // Node ID: 517 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id517in_input = id588out_value;

    id517out_output[(getCycle()+1)%2] = id517in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 43 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id43in_enable = id40out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id43in_max = id517out_output[getCycle()%2];

    HWOffsetFix<12,0,UNSIGNED> id43x_1;
    HWOffsetFix<1,0,UNSIGNED> id43x_2;
    HWOffsetFix<1,0,UNSIGNED> id43x_3;
    HWOffsetFix<12,0,UNSIGNED> id43x_4t_1e_1;

    id43out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id43st_count)));
    (id43x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id43st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id43x_2) = (gte_fixed((id43x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id43in_max))));
    (id43x_3) = (and_fixed((id43x_2),id43in_enable));
    id43out_wrap = (id43x_3);
    if((id43in_enable.getValueAsBool())) {
      if(((id43x_3).getValueAsBool())) {
        (id43st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id43x_4t_1e_1) = (id43x_1);
        (id43st_count) = (id43x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 630 (NodeConstantRawBits)
  }
  { // Node ID: 420 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id420in_a = id588out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id420in_b = id630out_value;

    id420out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id420in_a,id420in_b));
  }
  { // Node ID: 465 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id465in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id465in_b = id420out_result[getCycle()%2];

    id465out_result[(getCycle()+1)%2] = (eq_fixed(id465in_a,id465in_b));
  }
  { // Node ID: 422 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id423out_result;

  { // Node ID: 423 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id423in_a = id422out_io_u_out_force_disabled;

    id423out_result = (not_fixed(id423in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id424out_result;

  { // Node ID: 424 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id424in_a = id465out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id424in_b = id423out_result;

    HWOffsetFix<1,0,UNSIGNED> id424x_1;

    (id424x_1) = (and_fixed(id424in_a,id424in_b));
    id424out_result = (id424x_1);
  }
  { // Node ID: 574 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id574in_input = id424out_result;

    id574out_output[(getCycle()+106)%107] = id574in_input;
  }
  { // Node ID: 41 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 42 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id42in_enable = id43out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id42in_max = id41out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id42x_1;
    HWOffsetFix<1,0,UNSIGNED> id42x_2;
    HWOffsetFix<1,0,UNSIGNED> id42x_3;
    HWOffsetFix<33,0,UNSIGNED> id42x_4t_1e_1;

    id42out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id42st_count)));
    (id42x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id42st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id42x_2) = (gte_fixed((id42x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id42in_max))));
    (id42x_3) = (and_fixed((id42x_2),id42in_enable));
    id42out_wrap = (id42x_3);
    if((id42in_enable.getValueAsBool())) {
      if(((id42x_3).getValueAsBool())) {
        (id42st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id42x_4t_1e_1) = (id42x_1);
        (id42st_count) = (id42x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 629 (NodeConstantRawBits)
  }
  { // Node ID: 466 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id466in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id466in_b = id629out_value;

    id466out_result[(getCycle()+1)%2] = (eq_fixed(id466in_a,id466in_b));
  }
  HWFloat<8,24> id513out_output;

  { // Node ID: 513 (NodeStreamOffset)
    const HWFloat<8,24> &id513in_input = id413out_result[getCycle()%12];

    id513out_output = id513in_input;
  }
  { // Node ID: 38 (NodeConstantRawBits)
  }
  { // Node ID: 48 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id48in_sel = id466out_result[getCycle()%2];
    const HWFloat<8,24> &id48in_option0 = id513out_output;
    const HWFloat<8,24> &id48in_option1 = id38out_value;

    HWFloat<8,24> id48x_1;

    switch((id48in_sel.getValueAsLong())) {
      case 0l:
        id48x_1 = id48in_option0;
        break;
      case 1l:
        id48x_1 = id48in_option1;
        break;
      default:
        id48x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id48out_result[(getCycle()+1)%2] = (id48x_1);
  }
  { // Node ID: 521 (NodeFIFO)
    const HWFloat<8,24> &id521in_input = id48out_result[getCycle()%2];

    id521out_output[(getCycle()+8)%9] = id521in_input;
  }
  { // Node ID: 578 (NodeFIFO)
    const HWFloat<8,24> &id578in_input = id521out_output[getCycle()%9];

    id578out_output[(getCycle()+1)%2] = id578in_input;
  }
  { // Node ID: 579 (NodeFIFO)
    const HWFloat<8,24> &id579in_input = id578out_output[getCycle()%2];

    id579out_output[(getCycle()+1)%2] = id579in_input;
  }
  { // Node ID: 580 (NodeFIFO)
    const HWFloat<8,24> &id580in_input = id579out_output[getCycle()%2];

    id580out_output[(getCycle()+6)%7] = id580in_input;
  }
  { // Node ID: 581 (NodeFIFO)
    const HWFloat<8,24> &id581in_input = id580out_output[getCycle()%7];

    id581out_output[(getCycle()+10)%11] = id581in_input;
  }
  { // Node ID: 582 (NodeFIFO)
    const HWFloat<8,24> &id582in_input = id581out_output[getCycle()%11];

    id582out_output[(getCycle()+9)%10] = id582in_input;
  }
  { // Node ID: 583 (NodeFIFO)
    const HWFloat<8,24> &id583in_input = id582out_output[getCycle()%10];

    id583out_output[(getCycle()+26)%27] = id583in_input;
  }
  { // Node ID: 584 (NodeFIFO)
    const HWFloat<8,24> &id584in_input = id583out_output[getCycle()%27];

    id584out_output[(getCycle()+11)%12] = id584in_input;
  }
  { // Node ID: 585 (NodeFIFO)
    const HWFloat<8,24> &id585in_input = id584out_output[getCycle()%12];

    id585out_output[(getCycle()+22)%23] = id585in_input;
  }
  { // Node ID: 15 (NodeConstantRawBits)
  }
  { // Node ID: 390 (NodeGt)
    const HWFloat<8,24> &id390in_a = id583out_output[getCycle()%27];
    const HWFloat<8,24> &id390in_b = id15out_value;

    id390out_result[(getCycle()+2)%3] = (gt_float(id390in_a,id390in_b));
  }
  { // Node ID: 37 (NodeConstantRawBits)
  }
  { // Node ID: 628 (NodeConstantRawBits)
  }
  { // Node ID: 467 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id467in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id467in_b = id628out_value;

    id467out_result[(getCycle()+1)%2] = (eq_fixed(id467in_a,id467in_b));
  }
  HWFloat<8,24> id514out_output;

  { // Node ID: 514 (NodeStreamOffset)
    const HWFloat<8,24> &id514in_input = id385out_result[getCycle()%12];

    id514out_output = id514in_input;
  }
  { // Node ID: 519 (NodeFIFO)
    const HWFloat<8,24> &id519in_input = id514out_output;

    id519out_output[(getCycle()+86)%87] = id519in_input;
  }
  { // Node ID: 51 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id51in_sel = id467out_result[getCycle()%2];
    const HWFloat<8,24> &id51in_option0 = id519out_output[getCycle()%87];
    const HWFloat<8,24> &id51in_option1 = id38out_value;

    HWFloat<8,24> id51x_1;

    switch((id51in_sel.getValueAsLong())) {
      case 0l:
        id51x_1 = id51in_option0;
        break;
      case 1l:
        id51x_1 = id51in_option1;
        break;
      default:
        id51x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id51out_result[(getCycle()+1)%2] = (id51x_1);
  }
  { // Node ID: 520 (NodeFIFO)
    const HWFloat<8,24> &id520in_input = id51out_result[getCycle()%2];

    id520out_output[(getCycle()+8)%9] = id520in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id271out_result;

  { // Node ID: 271 (NodeGt)
    const HWFloat<8,24> &id271in_a = id48out_result[getCycle()%2];
    const HWFloat<8,24> &id271in_b = id15out_value;

    id271out_result = (gt_float(id271in_a,id271in_b));
  }
  { // Node ID: 17 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id262out_result;

  { // Node ID: 262 (NodeGt)
    const HWFloat<8,24> &id262in_a = id48out_result[getCycle()%2];
    const HWFloat<8,24> &id262in_b = id17out_value;

    id262out_result = (gt_float(id262in_a,id262in_b));
  }
  HWFloat<8,24> id263out_result;

  { // Node ID: 263 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id263in_sel = id262out_result;
    const HWFloat<8,24> &id263in_option0 = id38out_value;
    const HWFloat<8,24> &id263in_option1 = id37out_value;

    HWFloat<8,24> id263x_1;

    switch((id263in_sel.getValueAsLong())) {
      case 0l:
        id263x_1 = id263in_option0;
        break;
      case 1l:
        id263x_1 = id263in_option1;
        break;
      default:
        id263x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id263out_result = (id263x_1);
  }
  HWFloat<8,24> id274out_result;

  { // Node ID: 274 (NodeSub)
    const HWFloat<8,24> &id274in_a = id263out_result;
    const HWFloat<8,24> &id274in_b = id51out_result[getCycle()%2];

    id274out_result = (sub_float(id274in_a,id274in_b));
  }
  { // Node ID: 16 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id58out_result;

  { // Node ID: 58 (NodeGt)
    const HWFloat<8,24> &id58in_a = id48out_result[getCycle()%2];
    const HWFloat<8,24> &id58in_b = id16out_value;

    id58out_result = (gt_float(id58in_a,id58in_b));
  }
  { // Node ID: 1 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,24> id59out_result;

  { // Node ID: 59 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id59in_sel = id58out_result;
    const HWFloat<8,24> &id59in_option0 = id1out_value;
    const HWFloat<8,24> &id59in_option1 = id2out_value;

    HWFloat<8,24> id59x_1;

    switch((id59in_sel.getValueAsLong())) {
      case 0l:
        id59x_1 = id59in_option0;
        break;
      case 1l:
        id59x_1 = id59in_option1;
        break;
      default:
        id59x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id59out_result = (id59x_1);
  }
  HWFloat<8,24> id275out_result;

  { // Node ID: 275 (NodeDiv)
    const HWFloat<8,24> &id275in_a = id274out_result;
    const HWFloat<8,24> &id275in_b = id59out_result;

    id275out_result = (div_float(id275in_a,id275in_b));
  }
  HWFloat<8,24> id272out_result;

  { // Node ID: 272 (NodeNeg)
    const HWFloat<8,24> &id272in_a = id51out_result[getCycle()%2];

    id272out_result = (neg_float(id272in_a));
  }
  { // Node ID: 0 (NodeConstantRawBits)
  }
  HWFloat<8,24> id273out_result;

  { // Node ID: 273 (NodeDiv)
    const HWFloat<8,24> &id273in_a = id272out_result;
    const HWFloat<8,24> &id273in_b = id0out_value;

    id273out_result = (div_float(id273in_a,id273in_b));
  }
  HWFloat<8,24> id276out_result;

  { // Node ID: 276 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id276in_sel = id271out_result;
    const HWFloat<8,24> &id276in_option0 = id275out_result;
    const HWFloat<8,24> &id276in_option1 = id273out_result;

    HWFloat<8,24> id276x_1;

    switch((id276in_sel.getValueAsLong())) {
      case 0l:
        id276x_1 = id276in_option0;
        break;
      case 1l:
        id276x_1 = id276in_option1;
        break;
      default:
        id276x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id276out_result = (id276x_1);
  }
  { // Node ID: 44 (NodeInputMappedReg)
  }
  { // Node ID: 45 (NodeCast)
    const HWFloat<11,53> &id45in_i = id44out_dt;

    id45out_o[(getCycle()+3)%4] = (cast_float2float<8,24>(id45in_i));
  }
  { // Node ID: 384 (NodeMul)
    const HWFloat<8,24> &id384in_a = id276out_result;
    const HWFloat<8,24> &id384in_b = id45out_o[getCycle()%4];

    id384out_result[(getCycle()+8)%9] = (mul_float(id384in_a,id384in_b));
  }
  { // Node ID: 385 (NodeAdd)
    const HWFloat<8,24> &id385in_a = id520out_output[getCycle()%9];
    const HWFloat<8,24> &id385in_b = id384out_result[getCycle()%9];

    id385out_result[(getCycle()+11)%12] = (add_float(id385in_a,id385in_b));
  }
  HWFloat<8,24> id391out_result;

  { // Node ID: 391 (NodeNeg)
    const HWFloat<8,24> &id391in_a = id385out_result[getCycle()%12];

    id391out_result = (neg_float(id391in_a));
  }
  { // Node ID: 392 (NodeSub)
    const HWFloat<8,24> &id392in_a = id521out_output[getCycle()%9];
    const HWFloat<8,24> &id392in_b = id15out_value;

    id392out_result[(getCycle()+11)%12] = (sub_float(id392in_a,id392in_b));
  }
  { // Node ID: 393 (NodeMul)
    const HWFloat<8,24> &id393in_a = id391out_result;
    const HWFloat<8,24> &id393in_b = id392out_result[getCycle()%12];

    id393out_result[(getCycle()+8)%9] = (mul_float(id393in_a,id393in_b));
  }
  { // Node ID: 29 (NodeConstantRawBits)
  }
  { // Node ID: 394 (NodeSub)
    const HWFloat<8,24> &id394in_a = id29out_value;
    const HWFloat<8,24> &id394in_b = id580out_output[getCycle()%7];

    id394out_result[(getCycle()+11)%12] = (sub_float(id394in_a,id394in_b));
  }
  { // Node ID: 395 (NodeMul)
    const HWFloat<8,24> &id395in_a = id393out_result[getCycle()%9];
    const HWFloat<8,24> &id395in_b = id394out_result[getCycle()%12];

    id395out_result[(getCycle()+8)%9] = (mul_float(id395in_a,id395in_b));
  }
  { // Node ID: 8 (NodeConstantRawBits)
  }
  { // Node ID: 396 (NodeDiv)
    const HWFloat<8,24> &id396in_a = id395out_result[getCycle()%9];
    const HWFloat<8,24> &id396in_b = id8out_value;

    id396out_result[(getCycle()+28)%29] = (div_float(id396in_a,id396in_b));
  }
  { // Node ID: 397 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id397in_sel = id390out_result[getCycle()%3];
    const HWFloat<8,24> &id397in_option0 = id37out_value;
    const HWFloat<8,24> &id397in_option1 = id396out_result[getCycle()%29];

    HWFloat<8,24> id397x_1;

    switch((id397in_sel.getValueAsLong())) {
      case 0l:
        id397x_1 = id397in_option0;
        break;
      case 1l:
        id397x_1 = id397in_option1;
        break;
      default:
        id397x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id397out_result[(getCycle()+1)%2] = (id397x_1);
  }
  { // Node ID: 20 (NodeConstantRawBits)
  }
  { // Node ID: 398 (NodeGt)
    const HWFloat<8,24> &id398in_a = id583out_output[getCycle()%27];
    const HWFloat<8,24> &id398in_b = id20out_value;

    id398out_result[(getCycle()+2)%3] = (gt_float(id398in_a,id398in_b));
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id260out_result;

  { // Node ID: 260 (NodeGt)
    const HWFloat<8,24> &id260in_a = id582out_output[getCycle()%10];
    const HWFloat<8,24> &id260in_b = id22out_value;

    id260out_result = (gt_float(id260in_a,id260in_b));
  }
  { // Node ID: 9 (NodeConstantRawBits)
  }
  { // Node ID: 10 (NodeConstantRawBits)
  }
  HWFloat<8,24> id261out_result;

  { // Node ID: 261 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id261in_sel = id260out_result;
    const HWFloat<8,24> &id261in_option0 = id9out_value;
    const HWFloat<8,24> &id261in_option1 = id10out_value;

    HWFloat<8,24> id261x_1;

    switch((id261in_sel.getValueAsLong())) {
      case 0l:
        id261x_1 = id261in_option0;
        break;
      case 1l:
        id261x_1 = id261in_option1;
        break;
      default:
        id261x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id261out_result = (id261x_1);
  }
  { // Node ID: 402 (NodeDiv)
    const HWFloat<8,24> &id402in_a = id582out_output[getCycle()%10];
    const HWFloat<8,24> &id402in_b = id261out_result;

    id402out_result[(getCycle()+28)%29] = (div_float(id402in_a,id402in_b));
  }
  { // Node ID: 627 (NodeConstantRawBits)
  }
  { // Node ID: 11 (NodeConstantRawBits)
  }
  { // Node ID: 36 (NodeConstantRawBits)
  }
  { // Node ID: 626 (NodeConstantRawBits)
  }
  { // Node ID: 625 (NodeConstantRawBits)
  }
  { // Node ID: 624 (NodeConstantRawBits)
  }
  { // Node ID: 25 (NodeConstantRawBits)
  }
  { // Node ID: 30 (NodeConstantRawBits)
  }
  HWFloat<8,24> id159out_result;

  { // Node ID: 159 (NodeSub)
    const HWFloat<8,24> &id159in_a = id581out_output[getCycle()%11];
    const HWFloat<8,24> &id159in_b = id30out_value;

    id159out_result = (sub_float(id159in_a,id159in_b));
  }
  HWFloat<8,24> id160out_result;

  { // Node ID: 160 (NodeMul)
    const HWFloat<8,24> &id160in_a = id25out_value;
    const HWFloat<8,24> &id160in_b = id159out_result;

    id160out_result = (mul_float(id160in_a,id160in_b));
  }
  { // Node ID: 504 (NodePO2FPMult)
    const HWFloat<8,24> &id504in_floatIn = id160out_result;

    id504out_floatOut[(getCycle()+1)%2] = (mul_float(id504in_floatIn,(c_hw_flt_8_24_2_0val)));
  }
  HWRawBits<8> id234out_result;

  { // Node ID: 234 (NodeSlice)
    const HWFloat<8,24> &id234in_a = id504out_floatOut[getCycle()%2];

    id234out_result = (slice<23,8>(id234in_a));
  }
  { // Node ID: 235 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id468out_result;

  { // Node ID: 468 (NodeEqInlined)
    const HWRawBits<8> &id468in_a = id234out_result;
    const HWRawBits<8> &id468in_b = id235out_value;

    id468out_result = (eq_bits(id468in_a,id468in_b));
  }
  HWRawBits<23> id233out_result;

  { // Node ID: 233 (NodeSlice)
    const HWFloat<8,24> &id233in_a = id504out_floatOut[getCycle()%2];

    id233out_result = (slice<0,23>(id233in_a));
  }
  { // Node ID: 623 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id469out_result;

  { // Node ID: 469 (NodeNeqInlined)
    const HWRawBits<23> &id469in_a = id233out_result;
    const HWRawBits<23> &id469in_b = id623out_value;

    id469out_result = (neq_bits(id469in_a,id469in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id239out_result;

  { // Node ID: 239 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id239in_a = id468out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id239in_b = id469out_result;

    HWOffsetFix<1,0,UNSIGNED> id239x_1;

    (id239x_1) = (and_fixed(id239in_a,id239in_b));
    id239out_result = (id239x_1);
  }
  { // Node ID: 536 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id536in_input = id239out_result;

    id536out_output[(getCycle()+8)%9] = id536in_input;
  }
  { // Node ID: 163 (NodeConstantRawBits)
  }
  HWFloat<8,24> id164out_output;
  HWOffsetFix<1,0,UNSIGNED> id164out_output_doubt;

  { // Node ID: 164 (NodeDoubtBitOp)
    const HWFloat<8,24> &id164in_input = id504out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id164in_doubt = id163out_value;

    id164out_output = id164in_input;
    id164out_output_doubt = id164in_doubt;
  }
  { // Node ID: 165 (NodeCast)
    const HWFloat<8,24> &id165in_i = id164out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id165in_i_doubt = id164out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id165x_1;

    id165out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id165in_i,(&(id165x_1))));
    id165out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id165x_1),(c_hw_fix_4_0_uns_bits))),id165in_i_doubt));
  }
  { // Node ID: 168 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id167out_result;
  HWOffsetFix<1,0,UNSIGNED> id167out_result_doubt;

  { // Node ID: 167 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id167in_a = id165out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id167in_a_doubt = id165out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id167in_b = id168out_value;

    HWOffsetFix<1,0,UNSIGNED> id167x_1;

    id167out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id167in_a,id167in_b,(&(id167x_1))));
    id167out_result_doubt = (or_fixed((neq_fixed((id167x_1),(c_hw_fix_1_0_uns_bits_1))),id167in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id169out_o;
  HWOffsetFix<1,0,UNSIGNED> id169out_o_doubt;

  { // Node ID: 169 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id169in_i = id167out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id169in_i_doubt = id167out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id169x_1;

    id169out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id169in_i,(&(id169x_1))));
    id169out_o_doubt = (or_fixed((neq_fixed((id169x_1),(c_hw_fix_1_0_uns_bits_1))),id169in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id178out_output;

  { // Node ID: 178 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id178in_input = id169out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id178in_input_doubt = id169out_o_doubt;

    id178out_output = id178in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id179out_o;

  { // Node ID: 179 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id179in_i = id178out_output;

    id179out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id179in_i));
  }
  { // Node ID: 527 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id527in_input = id179out_o;

    id527out_output[(getCycle()+2)%3] = id527in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id201out_o;

  { // Node ID: 201 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id201in_i = id527out_output[getCycle()%3];

    id201out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id201in_i));
  }
  { // Node ID: 204 (NodeConstantRawBits)
  }
  { // Node ID: 495 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id181out_o;

  { // Node ID: 181 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id181in_i = id178out_output;

    id181out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id181in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id246out_output;

  { // Node ID: 246 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id246in_input = id181out_o;

    id246out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id246in_input))));
  }
  { // Node ID: 247 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id247in_addr = id246out_output;

    HWOffsetFix<22,-24,UNSIGNED> id247x_1;

    switch(((long)((id247in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id247x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id247x_1 = (id247sta_rom_store[(id247in_addr.getValueAsLong())]);
        break;
      default:
        id247x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id247out_dout[(getCycle()+2)%3] = (id247x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id180out_o;

  { // Node ID: 180 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id180in_i = id178out_output;

    id180out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id180in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id243out_output;

  { // Node ID: 243 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id243in_input = id180out_o;

    id243out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id243in_input))));
  }
  { // Node ID: 244 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id244in_addr = id243out_output;

    HWOffsetFix<24,-24,UNSIGNED> id244x_1;

    switch(((long)((id244in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id244x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id244x_1 = (id244sta_rom_store[(id244in_addr.getValueAsLong())]);
        break;
      default:
        id244x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id244out_dout[(getCycle()+2)%3] = (id244x_1);
  }
  { // Node ID: 185 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id182out_o;

  { // Node ID: 182 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id182in_i = id178out_output;

    id182out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id182in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id184out_result;

  { // Node ID: 184 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id184in_a = id185out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id184in_b = id182out_o;

    id184out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id184in_a,id184in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id186out_o;

  { // Node ID: 186 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id186in_i = id184out_result;

    id186out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id186in_i));
  }
  { // Node ID: 528 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id528in_input = id186out_o;

    id528out_output[(getCycle()+2)%3] = id528in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id187out_result;

  { // Node ID: 187 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id187in_a = id244out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id187in_b = id528out_output[getCycle()%3];

    id187out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id187in_a,id187in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id188out_result;

  { // Node ID: 188 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id188in_a = id528out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id188in_b = id244out_dout[getCycle()%3];

    id188out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id188in_a,id188in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id189out_result;

  { // Node ID: 189 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id189in_a = id187out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id189in_b = id188out_result;

    id189out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id189in_a,id189in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id190out_o;

  { // Node ID: 190 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id190in_i = id189out_result;

    id190out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id190in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id191out_result;

  { // Node ID: 191 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id191in_a = id247out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id191in_b = id190out_o;

    id191out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id191in_a,id191in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id192out_result;

  { // Node ID: 192 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id192in_a = id190out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id192in_b = id247out_dout[getCycle()%3];

    id192out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id192in_a,id192in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id193out_result;

  { // Node ID: 193 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id193in_a = id191out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id193in_b = id192out_result;

    id193out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id193in_a,id193in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id194out_o;

  { // Node ID: 194 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id194in_i = id193out_result;

    id194out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id194in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id195out_o;

  { // Node ID: 195 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id195in_i = id194out_o;

    id195out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id195in_i));
  }
  { // Node ID: 622 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id470out_result;

  { // Node ID: 470 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id470in_a = id195out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id470in_b = id622out_value;

    id470out_result = (gte_fixed(id470in_a,id470in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id503out_result;

  { // Node ID: 503 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id503in_a = id201out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id503in_b = id204out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id503in_c = id495out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id503in_condb = id470out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id503x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id503x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id503x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id503x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id503x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id503x_1 = id503in_a;
        break;
      default:
        id503x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id503in_condb.getValueAsLong())) {
      case 0l:
        id503x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id503x_2 = id503in_b;
        break;
      default:
        id503x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id503x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id503x_3 = id503in_c;
        break;
      default:
        id503x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id503x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id503x_1),(id503x_2))),(id503x_3)));
    id503out_result = (id503x_4);
  }
  HWRawBits<1> id471out_result;

  { // Node ID: 471 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id471in_a = id503out_result;

    id471out_result = (slice<10,1>(id471in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id472out_output;

  { // Node ID: 472 (NodeReinterpret)
    const HWRawBits<1> &id472in_input = id471out_result;

    id472out_output = (cast_bits2fixed<1,0,UNSIGNED>(id472in_input));
  }
  { // Node ID: 621 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id171out_result;

  { // Node ID: 171 (NodeGt)
    const HWFloat<8,24> &id171in_a = id504out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id171in_b = id621out_value;

    id171out_result = (gt_float(id171in_a,id171in_b));
  }
  { // Node ID: 530 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id530in_input = id171out_result;

    id530out_output[(getCycle()+6)%7] = id530in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id172out_output;

  { // Node ID: 172 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id172in_input = id169out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id172in_input_doubt = id169out_o_doubt;

    id172out_output = id172in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id173out_result;

  { // Node ID: 173 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id173in_a = id530out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id173in_b = id172out_output;

    HWOffsetFix<1,0,UNSIGNED> id173x_1;

    (id173x_1) = (and_fixed(id173in_a,id173in_b));
    id173out_result = (id173x_1);
  }
  { // Node ID: 531 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id531in_input = id173out_result;

    id531out_output[(getCycle()+2)%3] = id531in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id210out_result;

  { // Node ID: 210 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id210in_a = id531out_output[getCycle()%3];

    id210out_result = (not_fixed(id210in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id211out_result;

  { // Node ID: 211 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id211in_a = id472out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id211in_b = id210out_result;

    HWOffsetFix<1,0,UNSIGNED> id211x_1;

    (id211x_1) = (and_fixed(id211in_a,id211in_b));
    id211out_result = (id211x_1);
  }
  { // Node ID: 620 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id175out_result;

  { // Node ID: 175 (NodeLt)
    const HWFloat<8,24> &id175in_a = id504out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id175in_b = id620out_value;

    id175out_result = (lt_float(id175in_a,id175in_b));
  }
  { // Node ID: 532 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id532in_input = id175out_result;

    id532out_output[(getCycle()+6)%7] = id532in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id176out_output;

  { // Node ID: 176 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id176in_input = id169out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id176in_input_doubt = id169out_o_doubt;

    id176out_output = id176in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id177out_result;

  { // Node ID: 177 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id177in_a = id532out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id177in_b = id176out_output;

    HWOffsetFix<1,0,UNSIGNED> id177x_1;

    (id177x_1) = (and_fixed(id177in_a,id177in_b));
    id177out_result = (id177x_1);
  }
  { // Node ID: 533 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id533in_input = id177out_result;

    id533out_output[(getCycle()+2)%3] = id533in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id212out_result;

  { // Node ID: 212 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id212in_a = id211out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id212in_b = id533out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id212x_1;

    (id212x_1) = (or_fixed(id212in_a,id212in_b));
    id212out_result = (id212x_1);
  }
  { // Node ID: 619 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id473out_result;

  { // Node ID: 473 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id473in_a = id503out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id473in_b = id619out_value;

    id473out_result = (gte_fixed(id473in_a,id473in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id219out_result;

  { // Node ID: 219 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id219in_a = id533out_output[getCycle()%3];

    id219out_result = (not_fixed(id219in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id220out_result;

  { // Node ID: 220 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id220in_a = id473out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id220in_b = id219out_result;

    HWOffsetFix<1,0,UNSIGNED> id220x_1;

    (id220x_1) = (and_fixed(id220in_a,id220in_b));
    id220out_result = (id220x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id221out_result;

  { // Node ID: 221 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id221in_a = id220out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id221in_b = id531out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id221x_1;

    (id221x_1) = (or_fixed(id221in_a,id221in_b));
    id221out_result = (id221x_1);
  }
  HWRawBits<2> id222out_result;

  { // Node ID: 222 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id222in_in0 = id212out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id222in_in1 = id221out_result;

    id222out_result = (cat(id222in_in0,id222in_in1));
  }
  { // Node ID: 214 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id213out_o;

  { // Node ID: 213 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id213in_i = id503out_result;

    id213out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id213in_i));
  }
  { // Node ID: 198 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id199out_result;

  { // Node ID: 199 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id199in_sel = id470out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id199in_option0 = id195out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id199in_option1 = id198out_value;

    HWOffsetFix<24,-23,UNSIGNED> id199x_1;

    switch((id199in_sel.getValueAsLong())) {
      case 0l:
        id199x_1 = id199in_option0;
        break;
      case 1l:
        id199x_1 = id199in_option1;
        break;
      default:
        id199x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id199out_result = (id199x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id200out_o;

  { // Node ID: 200 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id200in_i = id199out_result;

    id200out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id200in_i));
  }
  HWRawBits<32> id215out_result;

  { // Node ID: 215 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id215in_in0 = id214out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id215in_in1 = id213out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id215in_in2 = id200out_o;

    id215out_result = (cat((cat(id215in_in0,id215in_in1)),id215in_in2));
  }
  HWFloat<8,24> id216out_output;

  { // Node ID: 216 (NodeReinterpret)
    const HWRawBits<32> &id216in_input = id215out_result;

    id216out_output = (cast_bits2float<8,24>(id216in_input));
  }
  { // Node ID: 223 (NodeConstantRawBits)
  }
  { // Node ID: 224 (NodeConstantRawBits)
  }
  { // Node ID: 226 (NodeConstantRawBits)
  }
  HWRawBits<32> id474out_result;

  { // Node ID: 474 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id474in_in0 = id223out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id474in_in1 = id224out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id474in_in2 = id226out_value;

    id474out_result = (cat((cat(id474in_in0,id474in_in1)),id474in_in2));
  }
  HWFloat<8,24> id228out_output;

  { // Node ID: 228 (NodeReinterpret)
    const HWRawBits<32> &id228in_input = id474out_result;

    id228out_output = (cast_bits2float<8,24>(id228in_input));
  }
  { // Node ID: 462 (NodeConstantRawBits)
  }
  HWFloat<8,24> id231out_result;

  { // Node ID: 231 (NodeMux)
    const HWRawBits<2> &id231in_sel = id222out_result;
    const HWFloat<8,24> &id231in_option0 = id216out_output;
    const HWFloat<8,24> &id231in_option1 = id228out_output;
    const HWFloat<8,24> &id231in_option2 = id462out_value;
    const HWFloat<8,24> &id231in_option3 = id228out_output;

    HWFloat<8,24> id231x_1;

    switch((id231in_sel.getValueAsLong())) {
      case 0l:
        id231x_1 = id231in_option0;
        break;
      case 1l:
        id231x_1 = id231in_option1;
        break;
      case 2l:
        id231x_1 = id231in_option2;
        break;
      case 3l:
        id231x_1 = id231in_option3;
        break;
      default:
        id231x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id231out_result = (id231x_1);
  }
  { // Node ID: 618 (NodeConstantRawBits)
  }
  HWFloat<8,24> id241out_result;

  { // Node ID: 241 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id241in_sel = id536out_output[getCycle()%9];
    const HWFloat<8,24> &id241in_option0 = id231out_result;
    const HWFloat<8,24> &id241in_option1 = id618out_value;

    HWFloat<8,24> id241x_1;

    switch((id241in_sel.getValueAsLong())) {
      case 0l:
        id241x_1 = id241in_option0;
        break;
      case 1l:
        id241x_1 = id241in_option1;
        break;
      default:
        id241x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id241out_result = (id241x_1);
  }
  { // Node ID: 617 (NodeConstantRawBits)
  }
  HWFloat<8,24> id249out_result;

  { // Node ID: 249 (NodeAdd)
    const HWFloat<8,24> &id249in_a = id241out_result;
    const HWFloat<8,24> &id249in_b = id617out_value;

    id249out_result = (add_float(id249in_a,id249in_b));
  }
  HWFloat<8,24> id251out_result;

  { // Node ID: 251 (NodeDiv)
    const HWFloat<8,24> &id251in_a = id624out_value;
    const HWFloat<8,24> &id251in_b = id249out_result;

    id251out_result = (div_float(id251in_a,id251in_b));
  }
  HWFloat<8,24> id253out_result;

  { // Node ID: 253 (NodeSub)
    const HWFloat<8,24> &id253in_a = id625out_value;
    const HWFloat<8,24> &id253in_b = id251out_result;

    id253out_result = (sub_float(id253in_a,id253in_b));
  }
  HWFloat<8,24> id255out_result;

  { // Node ID: 255 (NodeAdd)
    const HWFloat<8,24> &id255in_a = id626out_value;
    const HWFloat<8,24> &id255in_b = id253out_result;

    id255out_result = (add_float(id255in_a,id255in_b));
  }
  HWFloat<8,24> id256out_result;

  { // Node ID: 256 (NodeMul)
    const HWFloat<8,24> &id256in_a = id36out_value;
    const HWFloat<8,24> &id256in_b = id255out_result;

    id256out_result = (mul_float(id256in_a,id256in_b));
  }
  HWFloat<8,24> id257out_result;

  { // Node ID: 257 (NodeAdd)
    const HWFloat<8,24> &id257in_a = id11out_value;
    const HWFloat<8,24> &id257in_b = id256out_result;

    id257out_result = (add_float(id257in_a,id257in_b));
  }
  { // Node ID: 400 (NodeDiv)
    const HWFloat<8,24> &id400in_a = id627out_value;
    const HWFloat<8,24> &id400in_b = id257out_result;

    id400out_result[(getCycle()+28)%29] = (div_float(id400in_a,id400in_b));
  }
  { // Node ID: 403 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_sel = id398out_result[getCycle()%3];
    const HWFloat<8,24> &id403in_option0 = id402out_result[getCycle()%29];
    const HWFloat<8,24> &id403in_option1 = id400out_result[getCycle()%29];

    HWFloat<8,24> id403x_1;

    switch((id403in_sel.getValueAsLong())) {
      case 0l:
        id403x_1 = id403in_option0;
        break;
      case 1l:
        id403x_1 = id403in_option1;
        break;
      default:
        id403x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id403out_result[(getCycle()+1)%2] = (id403x_1);
  }
  { // Node ID: 410 (NodeAdd)
    const HWFloat<8,24> &id410in_a = id397out_result[getCycle()%2];
    const HWFloat<8,24> &id410in_b = id403out_result[getCycle()%2];

    id410out_result[(getCycle()+11)%12] = (add_float(id410in_a,id410in_b));
  }
  { // Node ID: 21 (NodeConstantRawBits)
  }
  { // Node ID: 404 (NodeGt)
    const HWFloat<8,24> &id404in_a = id584out_output[getCycle()%12];
    const HWFloat<8,24> &id404in_b = id21out_value;

    id404out_result[(getCycle()+2)%3] = (gt_float(id404in_a,id404in_b));
  }
  { // Node ID: 408 (NodeConstantRawBits)
  }
  { // Node ID: 616 (NodeConstantRawBits)
  }
  { // Node ID: 475 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id475in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id475in_b = id616out_value;

    id475out_result[(getCycle()+1)%2] = (eq_fixed(id475in_a,id475in_b));
  }
  HWFloat<8,24> id515out_output;

  { // Node ID: 515 (NodeStreamOffset)
    const HWFloat<8,24> &id515in_input = id538out_output[getCycle()%11];

    id515out_output = id515in_input;
  }
  { // Node ID: 539 (NodeFIFO)
    const HWFloat<8,24> &id539in_input = id515out_output;

    id539out_output[(getCycle()+67)%68] = id539in_input;
  }
  { // Node ID: 54 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id54in_sel = id475out_result[getCycle()%2];
    const HWFloat<8,24> &id54in_option0 = id539out_output[getCycle()%68];
    const HWFloat<8,24> &id54in_option1 = id38out_value;

    HWFloat<8,24> id54x_1;

    switch((id54in_sel.getValueAsLong())) {
      case 0l:
        id54x_1 = id54in_option0;
        break;
      case 1l:
        id54x_1 = id54in_option1;
        break;
      default:
        id54x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id54out_result[(getCycle()+1)%2] = (id54x_1);
  }
  { // Node ID: 543 (NodeFIFO)
    const HWFloat<8,24> &id543in_input = id54out_result[getCycle()%2];

    id543out_output[(getCycle()+9)%10] = id543in_input;
  }
  { // Node ID: 586 (NodeFIFO)
    const HWFloat<8,24> &id586in_input = id543out_output[getCycle()%10];

    id586out_output[(getCycle()+8)%9] = id586in_input;
  }
  { // Node ID: 18 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id277out_result;

  { // Node ID: 277 (NodeGt)
    const HWFloat<8,24> &id277in_a = id578out_output[getCycle()%2];
    const HWFloat<8,24> &id277in_b = id18out_value;

    id277out_result = (gt_float(id277in_a,id277in_b));
  }
  { // Node ID: 19 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id264out_result;

  { // Node ID: 264 (NodeGt)
    const HWFloat<8,24> &id264in_a = id578out_output[getCycle()%2];
    const HWFloat<8,24> &id264in_b = id19out_value;

    id264out_result = (gt_float(id264in_a,id264in_b));
  }
  { // Node ID: 615 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  HWFloat<8,24> id265out_result;

  { // Node ID: 265 (NodeDiv)
    const HWFloat<8,24> &id265in_a = id578out_output[getCycle()%2];
    const HWFloat<8,24> &id265in_b = id14out_value;

    id265out_result = (div_float(id265in_a,id265in_b));
  }
  HWFloat<8,24> id267out_result;

  { // Node ID: 267 (NodeSub)
    const HWFloat<8,24> &id267in_a = id615out_value;
    const HWFloat<8,24> &id267in_b = id265out_result;

    id267out_result = (sub_float(id267in_a,id267in_b));
  }
  { // Node ID: 32 (NodeConstantRawBits)
  }
  HWFloat<8,24> id268out_result;

  { // Node ID: 268 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id268in_sel = id264out_result;
    const HWFloat<8,24> &id268in_option0 = id267out_result;
    const HWFloat<8,24> &id268in_option1 = id32out_value;

    HWFloat<8,24> id268x_1;

    switch((id268in_sel.getValueAsLong())) {
      case 0l:
        id268x_1 = id268in_option0;
        break;
      case 1l:
        id268x_1 = id268in_option1;
        break;
      default:
        id268x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id268out_result = (id268x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id269out_result;

  { // Node ID: 269 (NodeGt)
    const HWFloat<8,24> &id269in_a = id268out_result;
    const HWFloat<8,24> &id269in_b = id38out_value;

    id269out_result = (gt_float(id269in_a,id269in_b));
  }
  HWFloat<8,24> id270out_result;

  { // Node ID: 270 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id270in_sel = id269out_result;
    const HWFloat<8,24> &id270in_option0 = id268out_result;
    const HWFloat<8,24> &id270in_option1 = id38out_value;

    HWFloat<8,24> id270x_1;

    switch((id270in_sel.getValueAsLong())) {
      case 0l:
        id270x_1 = id270in_option0;
        break;
      case 1l:
        id270x_1 = id270in_option1;
        break;
      default:
        id270x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id270out_result = (id270x_1);
  }
  HWFloat<8,24> id280out_result;

  { // Node ID: 280 (NodeSub)
    const HWFloat<8,24> &id280in_a = id270out_result;
    const HWFloat<8,24> &id280in_b = id543out_output[getCycle()%10];

    id280out_result = (sub_float(id280in_a,id280in_b));
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  { // Node ID: 35 (NodeConstantRawBits)
  }
  { // Node ID: 614 (NodeConstantRawBits)
  }
  { // Node ID: 613 (NodeConstantRawBits)
  }
  { // Node ID: 612 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeConstantRawBits)
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  HWFloat<8,24> id60out_result;

  { // Node ID: 60 (NodeSub)
    const HWFloat<8,24> &id60in_a = id48out_result[getCycle()%2];
    const HWFloat<8,24> &id60in_b = id26out_value;

    id60out_result = (sub_float(id60in_a,id60in_b));
  }
  HWFloat<8,24> id61out_result;

  { // Node ID: 61 (NodeMul)
    const HWFloat<8,24> &id61in_a = id23out_value;
    const HWFloat<8,24> &id61in_b = id60out_result;

    id61out_result = (mul_float(id61in_a,id61in_b));
  }
  { // Node ID: 505 (NodePO2FPMult)
    const HWFloat<8,24> &id505in_floatIn = id61out_result;

    id505out_floatOut[(getCycle()+1)%2] = (mul_float(id505in_floatIn,(c_hw_flt_8_24_2_0val)));
  }
  HWRawBits<8> id135out_result;

  { // Node ID: 135 (NodeSlice)
    const HWFloat<8,24> &id135in_a = id505out_floatOut[getCycle()%2];

    id135out_result = (slice<23,8>(id135in_a));
  }
  { // Node ID: 136 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id476out_result;

  { // Node ID: 476 (NodeEqInlined)
    const HWRawBits<8> &id476in_a = id135out_result;
    const HWRawBits<8> &id476in_b = id136out_value;

    id476out_result = (eq_bits(id476in_a,id476in_b));
  }
  HWRawBits<23> id134out_result;

  { // Node ID: 134 (NodeSlice)
    const HWFloat<8,24> &id134in_a = id505out_floatOut[getCycle()%2];

    id134out_result = (slice<0,23>(id134in_a));
  }
  { // Node ID: 611 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id477out_result;

  { // Node ID: 477 (NodeNeqInlined)
    const HWRawBits<23> &id477in_a = id134out_result;
    const HWRawBits<23> &id477in_b = id611out_value;

    id477out_result = (neq_bits(id477in_a,id477in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id140out_result;

  { // Node ID: 140 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id140in_a = id476out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id140in_b = id477out_result;

    HWOffsetFix<1,0,UNSIGNED> id140x_1;

    (id140x_1) = (and_fixed(id140in_a,id140in_b));
    id140out_result = (id140x_1);
  }
  { // Node ID: 553 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id553in_input = id140out_result;

    id553out_output[(getCycle()+8)%9] = id553in_input;
  }
  { // Node ID: 64 (NodeConstantRawBits)
  }
  HWFloat<8,24> id65out_output;
  HWOffsetFix<1,0,UNSIGNED> id65out_output_doubt;

  { // Node ID: 65 (NodeDoubtBitOp)
    const HWFloat<8,24> &id65in_input = id505out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id65in_doubt = id64out_value;

    id65out_output = id65in_input;
    id65out_output_doubt = id65in_doubt;
  }
  { // Node ID: 66 (NodeCast)
    const HWFloat<8,24> &id66in_i = id65out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id66in_i_doubt = id65out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id66x_1;

    id66out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id66in_i,(&(id66x_1))));
    id66out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id66x_1),(c_hw_fix_4_0_uns_bits))),id66in_i_doubt));
  }
  { // Node ID: 69 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id68out_result;
  HWOffsetFix<1,0,UNSIGNED> id68out_result_doubt;

  { // Node ID: 68 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id68in_a = id66out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id68in_a_doubt = id66out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id68in_b = id69out_value;

    HWOffsetFix<1,0,UNSIGNED> id68x_1;

    id68out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id68in_a,id68in_b,(&(id68x_1))));
    id68out_result_doubt = (or_fixed((neq_fixed((id68x_1),(c_hw_fix_1_0_uns_bits_1))),id68in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id70out_o;
  HWOffsetFix<1,0,UNSIGNED> id70out_o_doubt;

  { // Node ID: 70 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id70in_i = id68out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id70in_i_doubt = id68out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id70x_1;

    id70out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id70in_i,(&(id70x_1))));
    id70out_o_doubt = (or_fixed((neq_fixed((id70x_1),(c_hw_fix_1_0_uns_bits_1))),id70in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id79out_output;

  { // Node ID: 79 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id79in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_input_doubt = id70out_o_doubt;

    id79out_output = id79in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id80out_o;

  { // Node ID: 80 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id80in_i = id79out_output;

    id80out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id80in_i));
  }
  { // Node ID: 544 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id544in_input = id80out_o;

    id544out_output[(getCycle()+2)%3] = id544in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id102out_o;

  { // Node ID: 102 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id102in_i = id544out_output[getCycle()%3];

    id102out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id102in_i));
  }
  { // Node ID: 105 (NodeConstantRawBits)
  }
  { // Node ID: 497 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id82out_o;

  { // Node ID: 82 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id82in_i = id79out_output;

    id82out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id82in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id147out_output;

  { // Node ID: 147 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id147in_input = id82out_o;

    id147out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id147in_input))));
  }
  { // Node ID: 148 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id148in_addr = id147out_output;

    HWOffsetFix<22,-24,UNSIGNED> id148x_1;

    switch(((long)((id148in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id148x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id148x_1 = (id148sta_rom_store[(id148in_addr.getValueAsLong())]);
        break;
      default:
        id148x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id148out_dout[(getCycle()+2)%3] = (id148x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id81out_o;

  { // Node ID: 81 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id81in_i = id79out_output;

    id81out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id81in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id144out_output;

  { // Node ID: 144 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id144in_input = id81out_o;

    id144out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id144in_input))));
  }
  { // Node ID: 145 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id145in_addr = id144out_output;

    HWOffsetFix<24,-24,UNSIGNED> id145x_1;

    switch(((long)((id145in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id145x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id145x_1 = (id145sta_rom_store[(id145in_addr.getValueAsLong())]);
        break;
      default:
        id145x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id145out_dout[(getCycle()+2)%3] = (id145x_1);
  }
  { // Node ID: 86 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id83out_o;

  { // Node ID: 83 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id83in_i = id79out_output;

    id83out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id83in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id85out_result;

  { // Node ID: 85 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id85in_a = id86out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id85in_b = id83out_o;

    id85out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id85in_a,id85in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id87out_o;

  { // Node ID: 87 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id87in_i = id85out_result;

    id87out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id87in_i));
  }
  { // Node ID: 545 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id545in_input = id87out_o;

    id545out_output[(getCycle()+2)%3] = id545in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id88out_result;

  { // Node ID: 88 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id88in_a = id145out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id88in_b = id545out_output[getCycle()%3];

    id88out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id88in_a,id88in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id89out_result;

  { // Node ID: 89 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id89in_a = id545out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id89in_b = id145out_dout[getCycle()%3];

    id89out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id89in_a,id89in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id90out_result;

  { // Node ID: 90 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id90in_a = id88out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id90in_b = id89out_result;

    id90out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id90in_a,id90in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id91out_o;

  { // Node ID: 91 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id91in_i = id90out_result;

    id91out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id91in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id92out_result;

  { // Node ID: 92 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id92in_a = id148out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id92in_b = id91out_o;

    id92out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id92in_a,id92in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id93out_result;

  { // Node ID: 93 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id93in_a = id91out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id93in_b = id148out_dout[getCycle()%3];

    id93out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id93in_a,id93in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id94out_result;

  { // Node ID: 94 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id94in_a = id92out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id94in_b = id93out_result;

    id94out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id94in_a,id94in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id95out_o;

  { // Node ID: 95 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id95in_i = id94out_result;

    id95out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id95in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id96out_o;

  { // Node ID: 96 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id96in_i = id95out_o;

    id96out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id96in_i));
  }
  { // Node ID: 610 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id478out_result;

  { // Node ID: 478 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id478in_a = id96out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id478in_b = id610out_value;

    id478out_result = (gte_fixed(id478in_a,id478in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id502out_result;

  { // Node ID: 502 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id502in_a = id102out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id502in_b = id105out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id502in_c = id497out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id502in_condb = id478out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id502x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id502x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id502x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id502x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id502x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id502x_1 = id502in_a;
        break;
      default:
        id502x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id502in_condb.getValueAsLong())) {
      case 0l:
        id502x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id502x_2 = id502in_b;
        break;
      default:
        id502x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id502x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id502x_3 = id502in_c;
        break;
      default:
        id502x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id502x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id502x_1),(id502x_2))),(id502x_3)));
    id502out_result = (id502x_4);
  }
  HWRawBits<1> id479out_result;

  { // Node ID: 479 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id479in_a = id502out_result;

    id479out_result = (slice<10,1>(id479in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id480out_output;

  { // Node ID: 480 (NodeReinterpret)
    const HWRawBits<1> &id480in_input = id479out_result;

    id480out_output = (cast_bits2fixed<1,0,UNSIGNED>(id480in_input));
  }
  { // Node ID: 609 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id72out_result;

  { // Node ID: 72 (NodeGt)
    const HWFloat<8,24> &id72in_a = id505out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id72in_b = id609out_value;

    id72out_result = (gt_float(id72in_a,id72in_b));
  }
  { // Node ID: 547 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id547in_input = id72out_result;

    id547out_output[(getCycle()+6)%7] = id547in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id73out_output;

  { // Node ID: 73 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id73in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id73in_input_doubt = id70out_o_doubt;

    id73out_output = id73in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id74out_result;

  { // Node ID: 74 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id74in_a = id547out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id74in_b = id73out_output;

    HWOffsetFix<1,0,UNSIGNED> id74x_1;

    (id74x_1) = (and_fixed(id74in_a,id74in_b));
    id74out_result = (id74x_1);
  }
  { // Node ID: 548 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id548in_input = id74out_result;

    id548out_output[(getCycle()+2)%3] = id548in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id111out_result;

  { // Node ID: 111 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_a = id548out_output[getCycle()%3];

    id111out_result = (not_fixed(id111in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id112out_result;

  { // Node ID: 112 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id112in_a = id480out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id112in_b = id111out_result;

    HWOffsetFix<1,0,UNSIGNED> id112x_1;

    (id112x_1) = (and_fixed(id112in_a,id112in_b));
    id112out_result = (id112x_1);
  }
  { // Node ID: 608 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id76out_result;

  { // Node ID: 76 (NodeLt)
    const HWFloat<8,24> &id76in_a = id505out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id76in_b = id608out_value;

    id76out_result = (lt_float(id76in_a,id76in_b));
  }
  { // Node ID: 549 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id549in_input = id76out_result;

    id549out_output[(getCycle()+6)%7] = id549in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id77out_output;

  { // Node ID: 77 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id77in_input = id70out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id77in_input_doubt = id70out_o_doubt;

    id77out_output = id77in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id78out_result;

  { // Node ID: 78 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id78in_a = id549out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id78in_b = id77out_output;

    HWOffsetFix<1,0,UNSIGNED> id78x_1;

    (id78x_1) = (and_fixed(id78in_a,id78in_b));
    id78out_result = (id78x_1);
  }
  { // Node ID: 550 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id550in_input = id78out_result;

    id550out_output[(getCycle()+2)%3] = id550in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id113out_result;

  { // Node ID: 113 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id113in_a = id112out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id113in_b = id550out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id113x_1;

    (id113x_1) = (or_fixed(id113in_a,id113in_b));
    id113out_result = (id113x_1);
  }
  { // Node ID: 607 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id481out_result;

  { // Node ID: 481 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id481in_a = id502out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id481in_b = id607out_value;

    id481out_result = (gte_fixed(id481in_a,id481in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id120out_result;

  { // Node ID: 120 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id120in_a = id550out_output[getCycle()%3];

    id120out_result = (not_fixed(id120in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id121out_result;

  { // Node ID: 121 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id121in_a = id481out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id121in_b = id120out_result;

    HWOffsetFix<1,0,UNSIGNED> id121x_1;

    (id121x_1) = (and_fixed(id121in_a,id121in_b));
    id121out_result = (id121x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id122out_result;

  { // Node ID: 122 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id122in_a = id121out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id122in_b = id548out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id122x_1;

    (id122x_1) = (or_fixed(id122in_a,id122in_b));
    id122out_result = (id122x_1);
  }
  HWRawBits<2> id123out_result;

  { // Node ID: 123 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id123in_in0 = id113out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id123in_in1 = id122out_result;

    id123out_result = (cat(id123in_in0,id123in_in1));
  }
  { // Node ID: 115 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id114out_o;

  { // Node ID: 114 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id114in_i = id502out_result;

    id114out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id114in_i));
  }
  { // Node ID: 99 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id100out_result;

  { // Node ID: 100 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id100in_sel = id478out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id100in_option0 = id96out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id100in_option1 = id99out_value;

    HWOffsetFix<24,-23,UNSIGNED> id100x_1;

    switch((id100in_sel.getValueAsLong())) {
      case 0l:
        id100x_1 = id100in_option0;
        break;
      case 1l:
        id100x_1 = id100in_option1;
        break;
      default:
        id100x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id100out_result = (id100x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id101out_o;

  { // Node ID: 101 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id101in_i = id100out_result;

    id101out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id101in_i));
  }
  HWRawBits<32> id116out_result;

  { // Node ID: 116 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id116in_in0 = id115out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id116in_in1 = id114out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id116in_in2 = id101out_o;

    id116out_result = (cat((cat(id116in_in0,id116in_in1)),id116in_in2));
  }
  HWFloat<8,24> id117out_output;

  { // Node ID: 117 (NodeReinterpret)
    const HWRawBits<32> &id117in_input = id116out_result;

    id117out_output = (cast_bits2float<8,24>(id117in_input));
  }
  { // Node ID: 124 (NodeConstantRawBits)
  }
  { // Node ID: 125 (NodeConstantRawBits)
  }
  { // Node ID: 127 (NodeConstantRawBits)
  }
  HWRawBits<32> id482out_result;

  { // Node ID: 482 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id482in_in0 = id124out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id482in_in1 = id125out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id482in_in2 = id127out_value;

    id482out_result = (cat((cat(id482in_in0,id482in_in1)),id482in_in2));
  }
  HWFloat<8,24> id129out_output;

  { // Node ID: 129 (NodeReinterpret)
    const HWRawBits<32> &id129in_input = id482out_result;

    id129out_output = (cast_bits2float<8,24>(id129in_input));
  }
  { // Node ID: 463 (NodeConstantRawBits)
  }
  HWFloat<8,24> id132out_result;

  { // Node ID: 132 (NodeMux)
    const HWRawBits<2> &id132in_sel = id123out_result;
    const HWFloat<8,24> &id132in_option0 = id117out_output;
    const HWFloat<8,24> &id132in_option1 = id129out_output;
    const HWFloat<8,24> &id132in_option2 = id463out_value;
    const HWFloat<8,24> &id132in_option3 = id129out_output;

    HWFloat<8,24> id132x_1;

    switch((id132in_sel.getValueAsLong())) {
      case 0l:
        id132x_1 = id132in_option0;
        break;
      case 1l:
        id132x_1 = id132in_option1;
        break;
      case 2l:
        id132x_1 = id132in_option2;
        break;
      case 3l:
        id132x_1 = id132in_option3;
        break;
      default:
        id132x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id132out_result = (id132x_1);
  }
  { // Node ID: 606 (NodeConstantRawBits)
  }
  HWFloat<8,24> id142out_result;

  { // Node ID: 142 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id142in_sel = id553out_output[getCycle()%9];
    const HWFloat<8,24> &id142in_option0 = id132out_result;
    const HWFloat<8,24> &id142in_option1 = id606out_value;

    HWFloat<8,24> id142x_1;

    switch((id142in_sel.getValueAsLong())) {
      case 0l:
        id142x_1 = id142in_option0;
        break;
      case 1l:
        id142x_1 = id142in_option1;
        break;
      default:
        id142x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id142out_result = (id142x_1);
  }
  { // Node ID: 605 (NodeConstantRawBits)
  }
  HWFloat<8,24> id150out_result;

  { // Node ID: 150 (NodeAdd)
    const HWFloat<8,24> &id150in_a = id142out_result;
    const HWFloat<8,24> &id150in_b = id605out_value;

    id150out_result = (add_float(id150in_a,id150in_b));
  }
  HWFloat<8,24> id152out_result;

  { // Node ID: 152 (NodeDiv)
    const HWFloat<8,24> &id152in_a = id612out_value;
    const HWFloat<8,24> &id152in_b = id150out_result;

    id152out_result = (div_float(id152in_a,id152in_b));
  }
  HWFloat<8,24> id154out_result;

  { // Node ID: 154 (NodeSub)
    const HWFloat<8,24> &id154in_a = id613out_value;
    const HWFloat<8,24> &id154in_b = id152out_result;

    id154out_result = (sub_float(id154in_a,id154in_b));
  }
  HWFloat<8,24> id156out_result;

  { // Node ID: 156 (NodeAdd)
    const HWFloat<8,24> &id156in_a = id614out_value;
    const HWFloat<8,24> &id156in_b = id154out_result;

    id156out_result = (add_float(id156in_a,id156in_b));
  }
  HWFloat<8,24> id157out_result;

  { // Node ID: 157 (NodeMul)
    const HWFloat<8,24> &id157in_a = id35out_value;
    const HWFloat<8,24> &id157in_b = id156out_result;

    id157out_result = (mul_float(id157in_a,id157in_b));
  }
  HWFloat<8,24> id158out_result;

  { // Node ID: 158 (NodeAdd)
    const HWFloat<8,24> &id158in_a = id4out_value;
    const HWFloat<8,24> &id158in_b = id157out_result;

    id158out_result = (add_float(id158in_a,id158in_b));
  }
  HWFloat<8,24> id281out_result;

  { // Node ID: 281 (NodeDiv)
    const HWFloat<8,24> &id281in_a = id280out_result;
    const HWFloat<8,24> &id281in_b = id158out_result;

    id281out_result = (div_float(id281in_a,id281in_b));
  }
  HWFloat<8,24> id278out_result;

  { // Node ID: 278 (NodeNeg)
    const HWFloat<8,24> &id278in_a = id543out_output[getCycle()%10];

    id278out_result = (neg_float(id278in_a));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,24> id279out_result;

  { // Node ID: 279 (NodeDiv)
    const HWFloat<8,24> &id279in_a = id278out_result;
    const HWFloat<8,24> &id279in_b = id3out_value;

    id279out_result = (div_float(id279in_a,id279in_b));
  }
  HWFloat<8,24> id282out_result;

  { // Node ID: 282 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id282in_sel = id277out_result;
    const HWFloat<8,24> &id282in_option0 = id281out_result;
    const HWFloat<8,24> &id282in_option1 = id279out_result;

    HWFloat<8,24> id282x_1;

    switch((id282in_sel.getValueAsLong())) {
      case 0l:
        id282x_1 = id282in_option0;
        break;
      case 1l:
        id282x_1 = id282in_option1;
        break;
      default:
        id282x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id282out_result = (id282x_1);
  }
  { // Node ID: 386 (NodeMul)
    const HWFloat<8,24> &id386in_a = id282out_result;
    const HWFloat<8,24> &id386in_b = id45out_o[getCycle()%4];

    id386out_result[(getCycle()+8)%9] = (mul_float(id386in_a,id386in_b));
  }
  { // Node ID: 387 (NodeAdd)
    const HWFloat<8,24> &id387in_a = id586out_output[getCycle()%9];
    const HWFloat<8,24> &id387in_b = id386out_result[getCycle()%9];

    id387out_result[(getCycle()+11)%12] = (add_float(id387in_a,id387in_b));
  }
  { // Node ID: 538 (NodeFIFO)
    const HWFloat<8,24> &id538in_input = id387out_result[getCycle()%12];

    id538out_output[(getCycle()+10)%11] = id538in_input;
  }
  HWFloat<8,24> id405out_result;

  { // Node ID: 405 (NodeNeg)
    const HWFloat<8,24> &id405in_a = id538out_output[getCycle()%11];

    id405out_result = (neg_float(id405in_a));
  }
  { // Node ID: 604 (NodeConstantRawBits)
  }
  { // Node ID: 483 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id483in_a = id42out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id483in_b = id604out_value;

    id483out_result[(getCycle()+1)%2] = (eq_fixed(id483in_a,id483in_b));
  }
  HWFloat<8,24> id516out_output;

  { // Node ID: 516 (NodeStreamOffset)
    const HWFloat<8,24> &id516in_input = id557out_output[getCycle()%10];

    id516out_output = id516in_input;
  }
  { // Node ID: 558 (NodeFIFO)
    const HWFloat<8,24> &id558in_input = id516out_output;

    id558out_output[(getCycle()+67)%68] = id558in_input;
  }
  { // Node ID: 57 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id57in_sel = id483out_result[getCycle()%2];
    const HWFloat<8,24> &id57in_option0 = id558out_output[getCycle()%68];
    const HWFloat<8,24> &id57in_option1 = id37out_value;

    HWFloat<8,24> id57x_1;

    switch((id57in_sel.getValueAsLong())) {
      case 0l:
        id57x_1 = id57in_option0;
        break;
      case 1l:
        id57x_1 = id57in_option1;
        break;
      default:
        id57x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id57out_result[(getCycle()+1)%2] = (id57x_1);
  }
  { // Node ID: 569 (NodeFIFO)
    const HWFloat<8,24> &id569in_input = id57out_result[getCycle()%2];

    id569out_output[(getCycle()+10)%11] = id569in_input;
  }
  { // Node ID: 587 (NodeFIFO)
    const HWFloat<8,24> &id587in_input = id569out_output[getCycle()%11];

    id587out_output[(getCycle()+8)%9] = id587in_input;
  }
  { // Node ID: 603 (NodeConstantRawBits)
  }
  { // Node ID: 602 (NodeConstantRawBits)
  }
  { // Node ID: 601 (NodeConstantRawBits)
  }
  { // Node ID: 24 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeConstantRawBits)
  }
  HWFloat<8,24> id283out_result;

  { // Node ID: 283 (NodeSub)
    const HWFloat<8,24> &id283in_a = id48out_result[getCycle()%2];
    const HWFloat<8,24> &id283in_b = id27out_value;

    id283out_result = (sub_float(id283in_a,id283in_b));
  }
  HWFloat<8,24> id284out_result;

  { // Node ID: 284 (NodeMul)
    const HWFloat<8,24> &id284in_a = id24out_value;
    const HWFloat<8,24> &id284in_b = id283out_result;

    id284out_result = (mul_float(id284in_a,id284in_b));
  }
  { // Node ID: 506 (NodePO2FPMult)
    const HWFloat<8,24> &id506in_floatIn = id284out_result;

    id506out_floatOut[(getCycle()+1)%2] = (mul_float(id506in_floatIn,(c_hw_flt_8_24_2_0val)));
  }
  HWRawBits<8> id358out_result;

  { // Node ID: 358 (NodeSlice)
    const HWFloat<8,24> &id358in_a = id506out_floatOut[getCycle()%2];

    id358out_result = (slice<23,8>(id358in_a));
  }
  { // Node ID: 359 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id484out_result;

  { // Node ID: 484 (NodeEqInlined)
    const HWRawBits<8> &id484in_a = id358out_result;
    const HWRawBits<8> &id484in_b = id359out_value;

    id484out_result = (eq_bits(id484in_a,id484in_b));
  }
  HWRawBits<23> id357out_result;

  { // Node ID: 357 (NodeSlice)
    const HWFloat<8,24> &id357in_a = id506out_floatOut[getCycle()%2];

    id357out_result = (slice<0,23>(id357in_a));
  }
  { // Node ID: 600 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id485out_result;

  { // Node ID: 485 (NodeNeqInlined)
    const HWRawBits<23> &id485in_a = id357out_result;
    const HWRawBits<23> &id485in_b = id600out_value;

    id485out_result = (neq_bits(id485in_a,id485in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id363out_result;

  { // Node ID: 363 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id363in_a = id484out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id363in_b = id485out_result;

    HWOffsetFix<1,0,UNSIGNED> id363x_1;

    (id363x_1) = (and_fixed(id363in_a,id363in_b));
    id363out_result = (id363x_1);
  }
  { // Node ID: 568 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id568in_input = id363out_result;

    id568out_output[(getCycle()+8)%9] = id568in_input;
  }
  { // Node ID: 287 (NodeConstantRawBits)
  }
  HWFloat<8,24> id288out_output;
  HWOffsetFix<1,0,UNSIGNED> id288out_output_doubt;

  { // Node ID: 288 (NodeDoubtBitOp)
    const HWFloat<8,24> &id288in_input = id506out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id288in_doubt = id287out_value;

    id288out_output = id288in_input;
    id288out_output_doubt = id288in_doubt;
  }
  { // Node ID: 289 (NodeCast)
    const HWFloat<8,24> &id289in_i = id288out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id289in_i_doubt = id288out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id289x_1;

    id289out_o[(getCycle()+6)%7] = (cast_float2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id289in_i,(&(id289x_1))));
    id289out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id289x_1),(c_hw_fix_4_0_uns_bits))),id289in_i_doubt));
  }
  { // Node ID: 292 (NodeConstantRawBits)
  }
  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id291out_result;
  HWOffsetFix<1,0,UNSIGNED> id291out_result_doubt;

  { // Node ID: 291 (NodeMul)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id291in_a = id289out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id291in_a_doubt = id289out_o_doubt[getCycle()%7];
    const HWOffsetFix<35,-34,UNSIGNED> &id291in_b = id292out_value;

    HWOffsetFix<1,0,UNSIGNED> id291x_1;

    id291out_result = (mul_fixed<71,-60,TWOSCOMPLEMENT,TONEAREVEN>(id291in_a,id291in_b,(&(id291x_1))));
    id291out_result_doubt = (or_fixed((neq_fixed((id291x_1),(c_hw_fix_1_0_uns_bits_1))),id291in_a_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id293out_o;
  HWOffsetFix<1,0,UNSIGNED> id293out_o_doubt;

  { // Node ID: 293 (NodeCast)
    const HWOffsetFix<71,-60,TWOSCOMPLEMENT> &id293in_i = id291out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id293in_i_doubt = id291out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id293x_1;

    id293out_o = (cast_fixed2fixed<36,-26,TWOSCOMPLEMENT,TONEAREVEN>(id293in_i,(&(id293x_1))));
    id293out_o_doubt = (or_fixed((neq_fixed((id293x_1),(c_hw_fix_1_0_uns_bits_1))),id293in_i_doubt));
  }
  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id302out_output;

  { // Node ID: 302 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id302in_input = id293out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id302in_input_doubt = id293out_o_doubt;

    id302out_output = id302in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id303out_o;

  { // Node ID: 303 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id303in_i = id302out_output;

    id303out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id303in_i));
  }
  { // Node ID: 559 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id559in_input = id303out_o;

    id559out_output[(getCycle()+2)%3] = id559in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id325out_o;

  { // Node ID: 325 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id325in_i = id559out_output[getCycle()%3];

    id325out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id325in_i));
  }
  { // Node ID: 328 (NodeConstantRawBits)
  }
  { // Node ID: 499 (NodeConstantRawBits)
  }
  HWOffsetFix<10,-12,UNSIGNED> id305out_o;

  { // Node ID: 305 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id305in_i = id302out_output;

    id305out_o = (cast_fixed2fixed<10,-12,UNSIGNED,TRUNCATE>(id305in_i));
  }
  HWOffsetFix<10,0,UNSIGNED> id370out_output;

  { // Node ID: 370 (NodeReinterpret)
    const HWOffsetFix<10,-12,UNSIGNED> &id370in_input = id305out_o;

    id370out_output = (cast_bits2fixed<10,0,UNSIGNED>((cast_fixed2bits(id370in_input))));
  }
  { // Node ID: 371 (NodeROM)
    const HWOffsetFix<10,0,UNSIGNED> &id371in_addr = id370out_output;

    HWOffsetFix<22,-24,UNSIGNED> id371x_1;

    switch(((long)((id371in_addr.getValueAsLong())<(1024l)))) {
      case 0l:
        id371x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
      case 1l:
        id371x_1 = (id371sta_rom_store[(id371in_addr.getValueAsLong())]);
        break;
      default:
        id371x_1 = (c_hw_fix_22_n24_uns_undef);
        break;
    }
    id371out_dout[(getCycle()+2)%3] = (id371x_1);
  }
  HWOffsetFix<2,-2,UNSIGNED> id304out_o;

  { // Node ID: 304 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id304in_i = id302out_output;

    id304out_o = (cast_fixed2fixed<2,-2,UNSIGNED,TRUNCATE>(id304in_i));
  }
  HWOffsetFix<2,0,UNSIGNED> id367out_output;

  { // Node ID: 367 (NodeReinterpret)
    const HWOffsetFix<2,-2,UNSIGNED> &id367in_input = id304out_o;

    id367out_output = (cast_bits2fixed<2,0,UNSIGNED>((cast_fixed2bits(id367in_input))));
  }
  { // Node ID: 368 (NodeROM)
    const HWOffsetFix<2,0,UNSIGNED> &id368in_addr = id367out_output;

    HWOffsetFix<24,-24,UNSIGNED> id368x_1;

    switch(((long)((id368in_addr.getValueAsLong())<(4l)))) {
      case 0l:
        id368x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
      case 1l:
        id368x_1 = (id368sta_rom_store[(id368in_addr.getValueAsLong())]);
        break;
      default:
        id368x_1 = (c_hw_fix_24_n24_uns_undef);
        break;
    }
    id368out_dout[(getCycle()+2)%3] = (id368x_1);
  }
  { // Node ID: 309 (NodeConstantRawBits)
  }
  HWOffsetFix<14,-26,UNSIGNED> id306out_o;

  { // Node ID: 306 (NodeCast)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id306in_i = id302out_output;

    id306out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TRUNCATE>(id306in_i));
  }
  HWOffsetFix<28,-40,UNSIGNED> id308out_result;

  { // Node ID: 308 (NodeMul)
    const HWOffsetFix<14,-14,UNSIGNED> &id308in_a = id309out_value;
    const HWOffsetFix<14,-26,UNSIGNED> &id308in_b = id306out_o;

    id308out_result = (mul_fixed<28,-40,UNSIGNED,TONEAREVEN>(id308in_a,id308in_b));
  }
  HWOffsetFix<14,-26,UNSIGNED> id310out_o;

  { // Node ID: 310 (NodeCast)
    const HWOffsetFix<28,-40,UNSIGNED> &id310in_i = id308out_result;

    id310out_o = (cast_fixed2fixed<14,-26,UNSIGNED,TONEAREVEN>(id310in_i));
  }
  { // Node ID: 560 (NodeFIFO)
    const HWOffsetFix<14,-26,UNSIGNED> &id560in_input = id310out_o;

    id560out_output[(getCycle()+2)%3] = id560in_input;
  }
  HWOffsetFix<27,-26,UNSIGNED> id311out_result;

  { // Node ID: 311 (NodeAdd)
    const HWOffsetFix<24,-24,UNSIGNED> &id311in_a = id368out_dout[getCycle()%3];
    const HWOffsetFix<14,-26,UNSIGNED> &id311in_b = id560out_output[getCycle()%3];

    id311out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id311in_a,id311in_b));
  }
  HWOffsetFix<38,-50,UNSIGNED> id312out_result;

  { // Node ID: 312 (NodeMul)
    const HWOffsetFix<14,-26,UNSIGNED> &id312in_a = id560out_output[getCycle()%3];
    const HWOffsetFix<24,-24,UNSIGNED> &id312in_b = id368out_dout[getCycle()%3];

    id312out_result = (mul_fixed<38,-50,UNSIGNED,TONEAREVEN>(id312in_a,id312in_b));
  }
  HWOffsetFix<51,-50,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeAdd)
    const HWOffsetFix<27,-26,UNSIGNED> &id313in_a = id311out_result;
    const HWOffsetFix<38,-50,UNSIGNED> &id313in_b = id312out_result;

    id313out_result = (add_fixed<51,-50,UNSIGNED,TONEAREVEN>(id313in_a,id313in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id314out_o;

  { // Node ID: 314 (NodeCast)
    const HWOffsetFix<51,-50,UNSIGNED> &id314in_i = id313out_result;

    id314out_o = (cast_fixed2fixed<27,-26,UNSIGNED,TONEAREVEN>(id314in_i));
  }
  HWOffsetFix<28,-26,UNSIGNED> id315out_result;

  { // Node ID: 315 (NodeAdd)
    const HWOffsetFix<22,-24,UNSIGNED> &id315in_a = id371out_dout[getCycle()%3];
    const HWOffsetFix<27,-26,UNSIGNED> &id315in_b = id314out_o;

    id315out_result = (add_fixed<28,-26,UNSIGNED,TONEAREVEN>(id315in_a,id315in_b));
  }
  HWOffsetFix<49,-50,UNSIGNED> id316out_result;

  { // Node ID: 316 (NodeMul)
    const HWOffsetFix<27,-26,UNSIGNED> &id316in_a = id314out_o;
    const HWOffsetFix<22,-24,UNSIGNED> &id316in_b = id371out_dout[getCycle()%3];

    id316out_result = (mul_fixed<49,-50,UNSIGNED,TONEAREVEN>(id316in_a,id316in_b));
  }
  HWOffsetFix<52,-50,UNSIGNED> id317out_result;

  { // Node ID: 317 (NodeAdd)
    const HWOffsetFix<28,-26,UNSIGNED> &id317in_a = id315out_result;
    const HWOffsetFix<49,-50,UNSIGNED> &id317in_b = id316out_result;

    id317out_result = (add_fixed<52,-50,UNSIGNED,TONEAREVEN>(id317in_a,id317in_b));
  }
  HWOffsetFix<28,-26,UNSIGNED> id318out_o;

  { // Node ID: 318 (NodeCast)
    const HWOffsetFix<52,-50,UNSIGNED> &id318in_i = id317out_result;

    id318out_o = (cast_fixed2fixed<28,-26,UNSIGNED,TONEAREVEN>(id318in_i));
  }
  HWOffsetFix<24,-23,UNSIGNED> id319out_o;

  { // Node ID: 319 (NodeCast)
    const HWOffsetFix<28,-26,UNSIGNED> &id319in_i = id318out_o;

    id319out_o = (cast_fixed2fixed<24,-23,UNSIGNED,TONEAREVEN>(id319in_i));
  }
  { // Node ID: 599 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id486out_result;

  { // Node ID: 486 (NodeGteInlined)
    const HWOffsetFix<24,-23,UNSIGNED> &id486in_a = id319out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id486in_b = id599out_value;

    id486out_result = (gte_fixed(id486in_a,id486in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id501out_result;

  { // Node ID: 501 (NodeCondTriAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id501in_a = id325out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id501in_b = id328out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id501in_c = id499out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id501in_condb = id486out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id501x_1;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id501x_2;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id501x_3;
    HWOffsetFix<11,0,TWOSCOMPLEMENT> id501x_4;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id501x_1 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id501x_1 = id501in_a;
        break;
      default:
        id501x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch((id501in_condb.getValueAsLong())) {
      case 0l:
        id501x_2 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id501x_2 = id501in_b;
        break;
      default:
        id501x_2 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id501x_3 = (c_hw_fix_11_0_sgn_bits_2);
        break;
      case 1l:
        id501x_3 = id501in_c;
        break;
      default:
        id501x_3 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    (id501x_4) = (add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((add_fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id501x_1),(id501x_2))),(id501x_3)));
    id501out_result = (id501x_4);
  }
  HWRawBits<1> id487out_result;

  { // Node ID: 487 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id487in_a = id501out_result;

    id487out_result = (slice<10,1>(id487in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id488out_output;

  { // Node ID: 488 (NodeReinterpret)
    const HWRawBits<1> &id488in_input = id487out_result;

    id488out_output = (cast_bits2fixed<1,0,UNSIGNED>(id488in_input));
  }
  { // Node ID: 598 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id295out_result;

  { // Node ID: 295 (NodeGt)
    const HWFloat<8,24> &id295in_a = id506out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id295in_b = id598out_value;

    id295out_result = (gt_float(id295in_a,id295in_b));
  }
  { // Node ID: 562 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id562in_input = id295out_result;

    id562out_output[(getCycle()+6)%7] = id562in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id296out_output;

  { // Node ID: 296 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id296in_input = id293out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id296in_input_doubt = id293out_o_doubt;

    id296out_output = id296in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id297out_result;

  { // Node ID: 297 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id297in_a = id562out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id297in_b = id296out_output;

    HWOffsetFix<1,0,UNSIGNED> id297x_1;

    (id297x_1) = (and_fixed(id297in_a,id297in_b));
    id297out_result = (id297x_1);
  }
  { // Node ID: 563 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id563in_input = id297out_result;

    id563out_output[(getCycle()+2)%3] = id563in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id334out_result;

  { // Node ID: 334 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id334in_a = id563out_output[getCycle()%3];

    id334out_result = (not_fixed(id334in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id335out_result;

  { // Node ID: 335 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id335in_a = id488out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id335in_b = id334out_result;

    HWOffsetFix<1,0,UNSIGNED> id335x_1;

    (id335x_1) = (and_fixed(id335in_a,id335in_b));
    id335out_result = (id335x_1);
  }
  { // Node ID: 597 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id299out_result;

  { // Node ID: 299 (NodeLt)
    const HWFloat<8,24> &id299in_a = id506out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id299in_b = id597out_value;

    id299out_result = (lt_float(id299in_a,id299in_b));
  }
  { // Node ID: 564 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id564in_input = id299out_result;

    id564out_output[(getCycle()+6)%7] = id564in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id300out_output;

  { // Node ID: 300 (NodeDoubtBitOp)
    const HWOffsetFix<36,-26,TWOSCOMPLEMENT> &id300in_input = id293out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id300in_input_doubt = id293out_o_doubt;

    id300out_output = id300in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id301out_result;

  { // Node ID: 301 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id301in_a = id564out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id301in_b = id300out_output;

    HWOffsetFix<1,0,UNSIGNED> id301x_1;

    (id301x_1) = (and_fixed(id301in_a,id301in_b));
    id301out_result = (id301x_1);
  }
  { // Node ID: 565 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id565in_input = id301out_result;

    id565out_output[(getCycle()+2)%3] = id565in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id336out_result;

  { // Node ID: 336 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id336in_a = id335out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id336in_b = id565out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id336x_1;

    (id336x_1) = (or_fixed(id336in_a,id336in_b));
    id336out_result = (id336x_1);
  }
  { // Node ID: 596 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id489out_result;

  { // Node ID: 489 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id489in_a = id501out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id489in_b = id596out_value;

    id489out_result = (gte_fixed(id489in_a,id489in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id343out_result;

  { // Node ID: 343 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id343in_a = id565out_output[getCycle()%3];

    id343out_result = (not_fixed(id343in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id344out_result;

  { // Node ID: 344 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_a = id489out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id344in_b = id343out_result;

    HWOffsetFix<1,0,UNSIGNED> id344x_1;

    (id344x_1) = (and_fixed(id344in_a,id344in_b));
    id344out_result = (id344x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id345out_result;

  { // Node ID: 345 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id345in_a = id344out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id345in_b = id563out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id345x_1;

    (id345x_1) = (or_fixed(id345in_a,id345in_b));
    id345out_result = (id345x_1);
  }
  HWRawBits<2> id346out_result;

  { // Node ID: 346 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id346in_in0 = id336out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id346in_in1 = id345out_result;

    id346out_result = (cat(id346in_in0,id346in_in1));
  }
  { // Node ID: 338 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id337out_o;

  { // Node ID: 337 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id337in_i = id501out_result;

    id337out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id337in_i));
  }
  { // Node ID: 322 (NodeConstantRawBits)
  }
  HWOffsetFix<24,-23,UNSIGNED> id323out_result;

  { // Node ID: 323 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id323in_sel = id486out_result;
    const HWOffsetFix<24,-23,UNSIGNED> &id323in_option0 = id319out_o;
    const HWOffsetFix<24,-23,UNSIGNED> &id323in_option1 = id322out_value;

    HWOffsetFix<24,-23,UNSIGNED> id323x_1;

    switch((id323in_sel.getValueAsLong())) {
      case 0l:
        id323x_1 = id323in_option0;
        break;
      case 1l:
        id323x_1 = id323in_option1;
        break;
      default:
        id323x_1 = (c_hw_fix_24_n23_uns_undef);
        break;
    }
    id323out_result = (id323x_1);
  }
  HWOffsetFix<23,-23,UNSIGNED> id324out_o;

  { // Node ID: 324 (NodeCast)
    const HWOffsetFix<24,-23,UNSIGNED> &id324in_i = id323out_result;

    id324out_o = (cast_fixed2fixed<23,-23,UNSIGNED,TONEAREVEN>(id324in_i));
  }
  HWRawBits<32> id339out_result;

  { // Node ID: 339 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id339in_in0 = id338out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id339in_in1 = id337out_o;
    const HWOffsetFix<23,-23,UNSIGNED> &id339in_in2 = id324out_o;

    id339out_result = (cat((cat(id339in_in0,id339in_in1)),id339in_in2));
  }
  HWFloat<8,24> id340out_output;

  { // Node ID: 340 (NodeReinterpret)
    const HWRawBits<32> &id340in_input = id339out_result;

    id340out_output = (cast_bits2float<8,24>(id340in_input));
  }
  { // Node ID: 347 (NodeConstantRawBits)
  }
  { // Node ID: 348 (NodeConstantRawBits)
  }
  { // Node ID: 350 (NodeConstantRawBits)
  }
  HWRawBits<32> id490out_result;

  { // Node ID: 490 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id490in_in0 = id347out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id490in_in1 = id348out_value;
    const HWOffsetFix<23,0,UNSIGNED> &id490in_in2 = id350out_value;

    id490out_result = (cat((cat(id490in_in0,id490in_in1)),id490in_in2));
  }
  HWFloat<8,24> id352out_output;

  { // Node ID: 352 (NodeReinterpret)
    const HWRawBits<32> &id352in_input = id490out_result;

    id352out_output = (cast_bits2float<8,24>(id352in_input));
  }
  { // Node ID: 464 (NodeConstantRawBits)
  }
  HWFloat<8,24> id355out_result;

  { // Node ID: 355 (NodeMux)
    const HWRawBits<2> &id355in_sel = id346out_result;
    const HWFloat<8,24> &id355in_option0 = id340out_output;
    const HWFloat<8,24> &id355in_option1 = id352out_output;
    const HWFloat<8,24> &id355in_option2 = id464out_value;
    const HWFloat<8,24> &id355in_option3 = id352out_output;

    HWFloat<8,24> id355x_1;

    switch((id355in_sel.getValueAsLong())) {
      case 0l:
        id355x_1 = id355in_option0;
        break;
      case 1l:
        id355x_1 = id355in_option1;
        break;
      case 2l:
        id355x_1 = id355in_option2;
        break;
      case 3l:
        id355x_1 = id355in_option3;
        break;
      default:
        id355x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id355out_result = (id355x_1);
  }
  { // Node ID: 595 (NodeConstantRawBits)
  }
  HWFloat<8,24> id365out_result;

  { // Node ID: 365 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id365in_sel = id568out_output[getCycle()%9];
    const HWFloat<8,24> &id365in_option0 = id355out_result;
    const HWFloat<8,24> &id365in_option1 = id595out_value;

    HWFloat<8,24> id365x_1;

    switch((id365in_sel.getValueAsLong())) {
      case 0l:
        id365x_1 = id365in_option0;
        break;
      case 1l:
        id365x_1 = id365in_option1;
        break;
      default:
        id365x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id365out_result = (id365x_1);
  }
  { // Node ID: 594 (NodeConstantRawBits)
  }
  HWFloat<8,24> id373out_result;

  { // Node ID: 373 (NodeAdd)
    const HWFloat<8,24> &id373in_a = id365out_result;
    const HWFloat<8,24> &id373in_b = id594out_value;

    id373out_result = (add_float(id373in_a,id373in_b));
  }
  HWFloat<8,24> id375out_result;

  { // Node ID: 375 (NodeDiv)
    const HWFloat<8,24> &id375in_a = id601out_value;
    const HWFloat<8,24> &id375in_b = id373out_result;

    id375out_result = (div_float(id375in_a,id375in_b));
  }
  HWFloat<8,24> id377out_result;

  { // Node ID: 377 (NodeSub)
    const HWFloat<8,24> &id377in_a = id602out_value;
    const HWFloat<8,24> &id377in_b = id375out_result;

    id377out_result = (sub_float(id377in_a,id377in_b));
  }
  HWFloat<8,24> id379out_result;

  { // Node ID: 379 (NodeAdd)
    const HWFloat<8,24> &id379in_a = id603out_value;
    const HWFloat<8,24> &id379in_b = id377out_result;

    id379out_result = (add_float(id379in_a,id379in_b));
  }
  { // Node ID: 507 (NodePO2FPMult)
    const HWFloat<8,24> &id507in_floatIn = id379out_result;

    id507out_floatOut[(getCycle()+1)%2] = (mul_float(id507in_floatIn,(c_hw_flt_8_24_0_5val)));
  }
  HWFloat<8,24> id382out_result;

  { // Node ID: 382 (NodeSub)
    const HWFloat<8,24> &id382in_a = id507out_floatOut[getCycle()%2];
    const HWFloat<8,24> &id382in_b = id569out_output[getCycle()%11];

    id382out_result = (sub_float(id382in_a,id382in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id258out_result;

  { // Node ID: 258 (NodeGt)
    const HWFloat<8,24> &id258in_a = id579out_output[getCycle()%2];
    const HWFloat<8,24> &id258in_b = id18out_value;

    id258out_result = (gt_float(id258in_a,id258in_b));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 7 (NodeConstantRawBits)
  }
  HWFloat<8,24> id259out_result;

  { // Node ID: 259 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id259in_sel = id258out_result;
    const HWFloat<8,24> &id259in_option0 = id6out_value;
    const HWFloat<8,24> &id259in_option1 = id7out_value;

    HWFloat<8,24> id259x_1;

    switch((id259in_sel.getValueAsLong())) {
      case 0l:
        id259x_1 = id259in_option0;
        break;
      case 1l:
        id259x_1 = id259in_option1;
        break;
      default:
        id259x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id259out_result = (id259x_1);
  }
  HWFloat<8,24> id383out_result;

  { // Node ID: 383 (NodeDiv)
    const HWFloat<8,24> &id383in_a = id382out_result;
    const HWFloat<8,24> &id383in_b = id259out_result;

    id383out_result = (div_float(id383in_a,id383in_b));
  }
  { // Node ID: 388 (NodeMul)
    const HWFloat<8,24> &id388in_a = id383out_result;
    const HWFloat<8,24> &id388in_b = id45out_o[getCycle()%4];

    id388out_result[(getCycle()+8)%9] = (mul_float(id388in_a,id388in_b));
  }
  { // Node ID: 389 (NodeAdd)
    const HWFloat<8,24> &id389in_a = id587out_output[getCycle()%9];
    const HWFloat<8,24> &id389in_b = id388out_result[getCycle()%9];

    id389out_result[(getCycle()+11)%12] = (add_float(id389in_a,id389in_b));
  }
  { // Node ID: 557 (NodeFIFO)
    const HWFloat<8,24> &id557in_input = id389out_result[getCycle()%12];

    id557out_output[(getCycle()+9)%10] = id557in_input;
  }
  { // Node ID: 406 (NodeMul)
    const HWFloat<8,24> &id406in_a = id405out_result;
    const HWFloat<8,24> &id406in_b = id557out_output[getCycle()%10];

    id406out_result[(getCycle()+8)%9] = (mul_float(id406in_a,id406in_b));
  }
  { // Node ID: 13 (NodeConstantRawBits)
  }
  { // Node ID: 407 (NodeDiv)
    const HWFloat<8,24> &id407in_a = id406out_result[getCycle()%9];
    const HWFloat<8,24> &id407in_b = id13out_value;

    id407out_result[(getCycle()+28)%29] = (div_float(id407in_a,id407in_b));
  }
  { // Node ID: 409 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id409in_sel = id404out_result[getCycle()%3];
    const HWFloat<8,24> &id409in_option0 = id408out_value;
    const HWFloat<8,24> &id409in_option1 = id407out_result[getCycle()%29];

    HWFloat<8,24> id409x_1;

    switch((id409in_sel.getValueAsLong())) {
      case 0l:
        id409x_1 = id409in_option0;
        break;
      case 1l:
        id409x_1 = id409in_option1;
        break;
      default:
        id409x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id409out_result[(getCycle()+1)%2] = (id409x_1);
  }
  { // Node ID: 411 (NodeAdd)
    const HWFloat<8,24> &id411in_a = id410out_result[getCycle()%12];
    const HWFloat<8,24> &id411in_b = id409out_result[getCycle()%2];

    id411out_result[(getCycle()+11)%12] = (add_float(id411in_a,id411in_b));
  }
  { // Node ID: 412 (NodeMul)
    const HWFloat<8,24> &id412in_a = id411out_result[getCycle()%12];
    const HWFloat<8,24> &id412in_b = id45out_o[getCycle()%4];

    id412out_result[(getCycle()+8)%9] = (mul_float(id412in_a,id412in_b));
  }
  { // Node ID: 413 (NodeSub)
    const HWFloat<8,24> &id413in_a = id585out_output[getCycle()%23];
    const HWFloat<8,24> &id413in_b = id412out_result[getCycle()%9];

    id413out_result[(getCycle()+11)%12] = (sub_float(id413in_a,id413in_b));
  }
  if ( (getFillLevel() >= (110l)) && (getFlushLevel() < (110l)|| !isFlushingActive() ))
  { // Node ID: 425 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id425in_output_control = id574out_output[getCycle()%107];
    const HWFloat<8,24> &id425in_data = id413out_result[getCycle()%12];

    bool id425x_1;

    (id425x_1) = ((id425in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(110l))&(isFlushingActive()))));
    if((id425x_1)) {
      writeOutput(m_u_out, id425in_data);
    }
  }
  { // Node ID: 593 (NodeConstantRawBits)
  }
  { // Node ID: 428 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id428in_a = id588out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id428in_b = id593out_value;

    id428out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id428in_a,id428in_b));
  }
  { // Node ID: 491 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id491in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id491in_b = id428out_result[getCycle()%2];

    id491out_result[(getCycle()+1)%2] = (eq_fixed(id491in_a,id491in_b));
  }
  { // Node ID: 430 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id431out_result;

  { // Node ID: 431 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id431in_a = id430out_io_v_out_force_disabled;

    id431out_result = (not_fixed(id431in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id432out_result;

  { // Node ID: 432 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id432in_a = id491out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id432in_b = id431out_result;

    HWOffsetFix<1,0,UNSIGNED> id432x_1;

    (id432x_1) = (and_fixed(id432in_a,id432in_b));
    id432out_result = (id432x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 433 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id433in_output_control = id432out_result;
    const HWFloat<8,24> &id433in_data = id519out_output[getCycle()%87];

    bool id433x_1;

    (id433x_1) = ((id433in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id433x_1)) {
      writeOutput(m_v_out, id433in_data);
    }
  }
  { // Node ID: 592 (NodeConstantRawBits)
  }
  { // Node ID: 436 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id436in_a = id588out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id436in_b = id592out_value;

    id436out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id436in_a,id436in_b));
  }
  { // Node ID: 492 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id492in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id492in_b = id436out_result[getCycle()%2];

    id492out_result[(getCycle()+1)%2] = (eq_fixed(id492in_a,id492in_b));
  }
  { // Node ID: 438 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id439out_result;

  { // Node ID: 439 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id439in_a = id438out_io_w_out_force_disabled;

    id439out_result = (not_fixed(id439in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id440out_result;

  { // Node ID: 440 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id440in_a = id492out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id440in_b = id439out_result;

    HWOffsetFix<1,0,UNSIGNED> id440x_1;

    (id440x_1) = (and_fixed(id440in_a,id440in_b));
    id440out_result = (id440x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 441 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id441in_output_control = id440out_result;
    const HWFloat<8,24> &id441in_data = id539out_output[getCycle()%68];

    bool id441x_1;

    (id441x_1) = ((id441in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id441x_1)) {
      writeOutput(m_w_out, id441in_data);
    }
  }
  { // Node ID: 591 (NodeConstantRawBits)
  }
  { // Node ID: 444 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id444in_a = id588out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id444in_b = id591out_value;

    id444out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id444in_a,id444in_b));
  }
  { // Node ID: 493 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id493in_a = id43out_count;
    const HWOffsetFix<11,0,UNSIGNED> &id493in_b = id444out_result[getCycle()%2];

    id493out_result[(getCycle()+1)%2] = (eq_fixed(id493in_a,id493in_b));
  }
  { // Node ID: 446 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id447out_result;

  { // Node ID: 447 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id447in_a = id446out_io_s_out_force_disabled;

    id447out_result = (not_fixed(id447in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id448out_result;

  { // Node ID: 448 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id448in_a = id493out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id448in_b = id447out_result;

    HWOffsetFix<1,0,UNSIGNED> id448x_1;

    (id448x_1) = (and_fixed(id448in_a,id448in_b));
    id448out_result = (id448x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 449 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id449in_output_control = id448out_result;
    const HWFloat<8,24> &id449in_data = id558out_output[getCycle()%68];

    bool id449x_1;

    (id449x_1) = ((id449in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id449x_1)) {
      writeOutput(m_s_out, id449in_data);
    }
  }
  { // Node ID: 454 (NodeConstantRawBits)
  }
  { // Node ID: 590 (NodeConstantRawBits)
  }
  { // Node ID: 451 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 452 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id452in_enable = id590out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id452in_max = id451out_value;

    HWOffsetFix<49,0,UNSIGNED> id452x_1;
    HWOffsetFix<1,0,UNSIGNED> id452x_2;
    HWOffsetFix<1,0,UNSIGNED> id452x_3;
    HWOffsetFix<49,0,UNSIGNED> id452x_4t_1e_1;

    id452out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id452st_count)));
    (id452x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id452st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id452x_2) = (gte_fixed((id452x_1),id452in_max));
    (id452x_3) = (and_fixed((id452x_2),id452in_enable));
    id452out_wrap = (id452x_3);
    if((id452in_enable.getValueAsBool())) {
      if(((id452x_3).getValueAsBool())) {
        (id452st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id452x_4t_1e_1) = (id452x_1);
        (id452st_count) = (id452x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id453out_output;

  { // Node ID: 453 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id453in_input = id452out_count;

    id453out_output = id453in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 455 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id455in_load = id454out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id455in_data = id453out_output;

    bool id455x_1;

    (id455x_1) = ((id455in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id455x_1)) {
      setMappedRegValue("current_run_cycle_count", id455in_data);
    }
  }
  { // Node ID: 589 (NodeConstantRawBits)
  }
  { // Node ID: 457 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 458 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id458in_enable = id589out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id458in_max = id457out_value;

    HWOffsetFix<49,0,UNSIGNED> id458x_1;
    HWOffsetFix<1,0,UNSIGNED> id458x_2;
    HWOffsetFix<1,0,UNSIGNED> id458x_3;
    HWOffsetFix<49,0,UNSIGNED> id458x_4t_1e_1;

    id458out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id458st_count)));
    (id458x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id458st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id458x_2) = (gte_fixed((id458x_1),id458in_max));
    (id458x_3) = (and_fixed((id458x_2),id458in_enable));
    id458out_wrap = (id458x_3);
    if((id458in_enable.getValueAsBool())) {
      if(((id458x_3).getValueAsBool())) {
        (id458st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id458x_4t_1e_1) = (id458x_1);
        (id458st_count) = (id458x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 460 (NodeInputMappedReg)
  }
  { // Node ID: 494 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id494in_a = id458out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id494in_b = id460out_run_cycle_count;

    id494out_result[(getCycle()+1)%2] = (eq_fixed(id494in_a,id494in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 459 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id459in_start = id494out_result[getCycle()%2];

    if((id459in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
