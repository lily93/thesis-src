#include "Maxfiles.h" 			// Includes .max files
#include <MaxSLiCInterface.h>	// Simple Live CPU interface
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>

#define NEXT_LINE(fp) while(getc(fp)!='\n');

double dt = 0;
int simTime = 0;
int num_iteration = 0;
void print(float * u_out, float  * v_out) {
	char filename[1024];
	sprintf(filename, "karma_out_maxeler_%f_%d_32.csv", dt ,simTime);

	if( access(filename, F_OK) != -1) {
			remove(filename);
		}
	FILE *fp = fopen(filename, "w");

		if(fp==NULL) {
			printf("no file found");
		}
		else {
			printf("u;v;w;s;stim\n");

			//fprintf(fp,"u;v;w;s;stim\n");
			for (int i = 0; i<num_iteration; i++) {

	//printf("u: %g\n", u_out[i]);//, ca_out[i], x1_out[i], f_out[i], d_out[i], h_out[i], m_out[i], j_out[i]);
	fprintf(fp,"%g;%g;\n", u_out[i], v_out[i]);

}
			fclose(fp);
			fp = NULL;
	}
}

void initParam(char * params) {

	  FILE *fp;

	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.1;
		  simTime = 1000;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%f",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %f\n", dt);

	   fclose(fp);

	  }
	  num_iteration = (int)(simTime/dt);


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %f\n", dt);



}
float least_squares(float * u, float * un, int size, float dt1, float dt2) {
	float lse = 0;


	for(int i = 0; i<(size+1); i++) {
	lse += pow(((u[i]*dt1) - (un[i*2]* dt2 + un[(i*2)+1] * dt2)),2);
	//printf("#%d ERROR: %.15f\n", (i+1), lse);

	}
	//lse = lse/size;
	printf("LSE dt: %f vs dt/2: %f: %g\n", dt1, dt2, (lse/size));
	return lse;




}

runKarmaDFE(float * u_out) {
	int sizeBytes = num_iteration * sizeof(float);
	printf("DT: %f ITERATIONS: %d\n", dt, num_iteration);
	float * v_out = malloc(sizeBytes);


	struct timeval start, end;
			long mtime, seconds, useconds;


			max_file_t *myMaxFile = KarmaDFE_init();
			max_engine_t *myDFE = max_load(myMaxFile, "local:*");


			KarmaDFE_actions_t actions;

			actions.param_length = num_iteration;
			actions.param_dt = dt;

			actions.param_num_iteration = num_iteration;
				actions.outstream_u_out = u_out;
				actions.outstream_v_out = v_out;

				printf("Running DFE\n");
									gettimeofday(&start, NULL);

							KarmaDFE_run(myDFE, &actions);
							//memcpy(us, u_out, sizeBytes);
							gettimeofday(&end, NULL);
								  seconds = end.tv_sec - start.tv_sec;
								  		useconds = end.tv_usec - start.tv_usec;

								  		mtime = ((seconds) * 1000 + useconds / 1000.0);
							printf("DFE elapsed time: %ld milliseconds \n", mtime);
							printf("Printing Data from DFE\n");

							print(u_out, v_out);


	free(v_out);
	v_out = NULL;
	max_unload(myDFE);

}

void precision_test() {
	char * errorfile = "error_karma_precision.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");

dt = 0.1/64;
simTime = 1000;
num_iteration = (int) simTime/dt;

	int sizeBytes = num_iteration * sizeof(float);
	float * us_p1 = malloc(sizeBytes);

	runKarmaDFE(us_p1);
	fclose(fp);
}

void step_size_estimation_test() {
	char * errorfile = "error_karma_maxeler.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");


	char * params = "params.dat";
	initParam(params);
	int num_iteration_old = num_iteration;
	int sizeBytes = num_iteration * sizeof(float);
	float * us_old = malloc(sizeBytes);

	runKarmaDFE(us_old);
	int frac = 2;
	float dt_old = dt;
	dt = dt/frac;
	float err = 10;
	fclose(fp);
	while(dt>=0.000781) {


		num_iteration = (int)floor(simTime/dt + 0.5);
		sizeBytes = num_iteration * sizeof(float);
		float * us = malloc(sizeBytes);
		printf("RUNNING TEST.. DT (MS): %.20f \n", dt);
		runKarmaDFE(us);
		err = least_squares(us_old, us, num_iteration_old, dt_old, dt);
		printf("DT(MS): %g\n ERROR: %g\n---------------------------------------\n", dt, (err/num_iteration_old));
		FILE *fp = fopen(errorfile, "a");
		fprintf(fp,"%g\n", (err/num_iteration_old));
		fclose(fp);
		err = (err/num_iteration_old);
		us_old = realloc(us_old,sizeBytes);
		if(us_old==NULL) {
			printf("memory allocation error");
			exit(1);
		}
		us_old = us;
		dt_old = dt;
		num_iteration_old = num_iteration;
		dt = dt/2;




	}





}


int main(void) {

	precision_test();
	return 0;
}
