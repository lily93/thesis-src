#include "stdsimheader.h"

namespace maxcompilersim {

void KarmaKernel::execute0() {
  { // Node ID: 522 (NodeConstantRawBits)
  }
  { // Node ID: 14 (NodeConstantRawBits)
  }
  { // Node ID: 556 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 17 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id17in_enable = id14out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id17in_max = id556out_value;

    HWOffsetFix<12,0,UNSIGNED> id17x_1;
    HWOffsetFix<1,0,UNSIGNED> id17x_2;
    HWOffsetFix<1,0,UNSIGNED> id17x_3;
    HWOffsetFix<12,0,UNSIGNED> id17x_4t_1e_1;

    id17out_count = (cast_fixed2fixed<11,0,UNSIGNED,TRUNCATE>((id17st_count)));
    (id17x_1) = (add_fixed<12,0,UNSIGNED,TRUNCATE>((id17st_count),(c_hw_fix_12_0_uns_bits_1)));
    (id17x_2) = (gte_fixed((id17x_1),(cast_fixed2fixed<12,0,UNSIGNED,TRUNCATE>(id17in_max))));
    (id17x_3) = (and_fixed((id17x_2),id17in_enable));
    id17out_wrap = (id17x_3);
    if((id17in_enable.getValueAsBool())) {
      if(((id17x_3).getValueAsBool())) {
        (id17st_count) = (c_hw_fix_12_0_uns_bits);
      }
      else {
        (id17x_4t_1e_1) = (id17x_1);
        (id17st_count) = (id17x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 15 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id16in_enable = id17out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    HWOffsetFix<33,0,UNSIGNED> id16x_1;
    HWOffsetFix<1,0,UNSIGNED> id16x_2;
    HWOffsetFix<1,0,UNSIGNED> id16x_3;
    HWOffsetFix<33,0,UNSIGNED> id16x_4t_1e_1;

    id16out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id16st_count)));
    (id16x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id16st_count),(c_hw_fix_33_0_uns_bits_1)));
    (id16x_2) = (gte_fixed((id16x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id16in_max))));
    (id16x_3) = (and_fixed((id16x_2),id16in_enable));
    id16out_wrap = (id16x_3);
    if((id16in_enable.getValueAsBool())) {
      if(((id16x_3).getValueAsBool())) {
        (id16st_count) = (c_hw_fix_33_0_uns_bits);
      }
      else {
        (id16x_4t_1e_1) = (id16x_1);
        (id16st_count) = (id16x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 574 (NodeConstantRawBits)
  }
  { // Node ID: 355 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id355in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id355in_b = id574out_value;

    id355out_result[(getCycle()+1)%2] = (eq_fixed(id355in_a,id355in_b));
  }
  { // Node ID: 543 (NodeFIFO)
    const HWFloat<8,12> &id543in_input = id23out_result[getCycle()%2];

    id543out_output[(getCycle()+9)%10] = id543in_input;
  }
  { // Node ID: 555 (NodeFIFO)
    const HWFloat<8,12> &id555in_input = id543out_output[getCycle()%10];

    id555out_output[(getCycle()+1)%2] = id555in_input;
  }
  { // Node ID: 18 (NodeInputMappedReg)
  }
  { // Node ID: 19 (NodeCast)
    const HWFloat<11,53> &id19in_i = id18out_dt;

    id19out_o[(getCycle()+3)%4] = (cast_float2float<8,12>(id19in_i));
  }
  { // Node ID: 6 (NodeConstantRawBits)
  }
  { // Node ID: 573 (NodeConstantRawBits)
  }
  { // Node ID: 356 (NodeEqInlined)
    const HWOffsetFix<32,0,UNSIGNED> &id356in_a = id16out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id356in_b = id573out_value;

    id356out_result[(getCycle()+1)%2] = (eq_fixed(id356in_a,id356in_b));
  }
  { // Node ID: 572 (NodeConstantRawBits)
  }
  { // Node ID: 5 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id313out_result;

  { // Node ID: 313 (NodeGt)
    const HWFloat<8,12> &id313in_a = id555out_output[getCycle()%2];
    const HWFloat<8,12> &id313in_b = id5out_value;

    id313out_result = (gt_float(id313in_a,id313in_b));
  }
  HWFloat<8,12> id314out_o;

  { // Node ID: 314 (NodeCast)
    const HWOffsetFix<1,0,UNSIGNED> &id314in_i = id313out_result;

    id314out_o = (cast_fixed2float<8,12>(id314in_i));
  }
  HWFloat<8,12> id316out_result;

  { // Node ID: 316 (NodeMul)
    const HWFloat<8,12> &id316in_a = id572out_value;
    const HWFloat<8,12> &id316in_b = id314out_o;

    id316out_result = (mul_float(id316in_a,id316in_b));
  }
  HWFloat<8,12> id317out_result;

  { // Node ID: 317 (NodeSub)
    const HWFloat<8,12> &id317in_a = id316out_result;
    const HWFloat<8,12> &id317in_b = id529out_output[getCycle()%11];

    id317out_result = (sub_float(id317in_a,id317in_b));
  }
  { // Node ID: 3 (NodeConstantRawBits)
  }
  HWFloat<8,12> id319out_result;

  { // Node ID: 319 (NodeMul)
    const HWFloat<8,12> &id319in_a = id317out_result;
    const HWFloat<8,12> &id319in_b = id3out_value;

    id319out_result = (mul_float(id319in_a,id319in_b));
  }
  HWFloat<8,12> id322out_result;

  { // Node ID: 322 (NodeMul)
    const HWFloat<8,12> &id322in_a = id19out_o[getCycle()%4];
    const HWFloat<8,12> &id322in_b = id319out_result;

    id322out_result = (mul_float(id322in_a,id322in_b));
  }
  HWFloat<8,12> id323out_result;

  { // Node ID: 323 (NodeAdd)
    const HWFloat<8,12> &id323in_a = id529out_output[getCycle()%11];
    const HWFloat<8,12> &id323in_b = id322out_result;

    id323out_result = (add_float(id323in_a,id323in_b));
  }
  HWFloat<8,12> id526out_output;

  { // Node ID: 526 (NodeStreamOffset)
    const HWFloat<8,12> &id526in_input = id323out_result;

    id526out_output = id526in_input;
  }
  { // Node ID: 26 (NodeConstantRawBits)
  }
  { // Node ID: 27 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_sel = id356out_result[getCycle()%2];
    const HWFloat<8,12> &id27in_option0 = id526out_output;
    const HWFloat<8,12> &id27in_option1 = id26out_value;

    HWFloat<8,12> id27x_1;

    switch((id27in_sel.getValueAsLong())) {
      case 0l:
        id27x_1 = id27in_option0;
        break;
      case 1l:
        id27x_1 = id27in_option1;
        break;
      default:
        id27x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  { // Node ID: 529 (NodeFIFO)
    const HWFloat<8,12> &id529in_input = id27out_result[getCycle()%2];

    id529out_output[(getCycle()+10)%11] = id529in_input;
  }
  HWFloat<8,12> id32out_result;

  { // Node ID: 32 (NodeMul)
    const HWFloat<8,12> &id32in_a = id529out_output[getCycle()%11];
    const HWFloat<8,12> &id32in_b = id529out_output[getCycle()%11];

    id32out_result = (mul_float(id32in_a,id32in_b));
  }
  HWFloat<8,12> id33out_result;

  { // Node ID: 33 (NodeMul)
    const HWFloat<8,12> &id33in_a = id529out_output[getCycle()%11];
    const HWFloat<8,12> &id33in_b = id32out_result;

    id33out_result = (mul_float(id33in_a,id33in_b));
  }
  HWFloat<8,12> id34out_result;

  { // Node ID: 34 (NodeMul)
    const HWFloat<8,12> &id34in_a = id33out_result;
    const HWFloat<8,12> &id34in_b = id32out_result;

    id34out_result = (mul_float(id34in_a,id34in_b));
  }
  HWFloat<8,12> id35out_result;

  { // Node ID: 35 (NodeMul)
    const HWFloat<8,12> &id35in_a = id34out_result;
    const HWFloat<8,12> &id35in_b = id34out_result;

    id35out_result = (mul_float(id35in_a,id35in_b));
  }
  HWFloat<8,12> id310out_result;

  { // Node ID: 310 (NodeSub)
    const HWFloat<8,12> &id310in_a = id6out_value;
    const HWFloat<8,12> &id310in_b = id35out_result;

    id310out_result = (sub_float(id310in_a,id310in_b));
  }
  { // Node ID: 571 (NodeConstantRawBits)
  }
  { // Node ID: 570 (NodeConstantRawBits)
  }
  { // Node ID: 569 (NodeConstantRawBits)
  }
  HWRawBits<8> id285out_result;

  { // Node ID: 285 (NodeSlice)
    const HWFloat<8,12> &id285in_a = id501out_floatOut[getCycle()%2];

    id285out_result = (slice<11,8>(id285in_a));
  }
  { // Node ID: 286 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id357out_result;

  { // Node ID: 357 (NodeEqInlined)
    const HWRawBits<8> &id357in_a = id285out_result;
    const HWRawBits<8> &id357in_b = id286out_value;

    id357out_result = (eq_bits(id357in_a,id357in_b));
  }
  HWRawBits<11> id284out_result;

  { // Node ID: 284 (NodeSlice)
    const HWFloat<8,12> &id284in_a = id501out_floatOut[getCycle()%2];

    id284out_result = (slice<0,11>(id284in_a));
  }
  { // Node ID: 568 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id358out_result;

  { // Node ID: 358 (NodeNeqInlined)
    const HWRawBits<11> &id358in_a = id284out_result;
    const HWRawBits<11> &id358in_b = id568out_value;

    id358out_result = (neq_bits(id358in_a,id358in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id290out_result;

  { // Node ID: 290 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id290in_a = id357out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id290in_b = id358out_result;

    HWOffsetFix<1,0,UNSIGNED> id290x_1;

    (id290x_1) = (and_fixed(id290in_a,id290in_b));
    id290out_result = (id290x_1);
  }
  { // Node ID: 542 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id542in_input = id290out_result;

    id542out_output[(getCycle()+8)%9] = id542in_input;
  }
  HWRawBits<1> id44out_result;

  { // Node ID: 44 (NodeSlice)
    const HWOffsetFix<4,0,UNSIGNED> &id44in_a = id43out_exception[getCycle()%7];

    id44out_result = (slice<1,1>(id44in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id45out_output;

  { // Node ID: 45 (NodeReinterpret)
    const HWRawBits<1> &id45in_input = id44out_result;

    id45out_output = (cast_bits2fixed<1,0,UNSIGNED>(id45in_input));
  }
  { // Node ID: 46 (NodeConstantRawBits)
  }
  HWRawBits<1> id359out_result;
  HWOffsetFix<1,0,UNSIGNED> id359out_result_doubt;

  { // Node ID: 359 (NodeSlice)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id359in_a = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id359in_a_doubt = id43out_o_doubt[getCycle()%7];

    id359out_result = (slice<23,1>(id359in_a));
    id359out_result_doubt = id359in_a_doubt;
  }
  HWRawBits<1> id360out_result;
  HWOffsetFix<1,0,UNSIGNED> id360out_result_doubt;

  { // Node ID: 360 (NodeNot)
    const HWRawBits<1> &id360in_a = id359out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id360in_a_doubt = id359out_result_doubt;

    id360out_result = (not_bits(id360in_a));
    id360out_result_doubt = id360in_a_doubt;
  }
  HWRawBits<24> id383out_result;
  HWOffsetFix<1,0,UNSIGNED> id383out_result_doubt;

  { // Node ID: 383 (NodeCat)
    const HWRawBits<1> &id383in_in0 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in0_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in1 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in1_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in2 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in2_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in3 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in3_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in4 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in4_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in5 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in5_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in6 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in6_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in7 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in7_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in8 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in8_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in9 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in9_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in10 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in10_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in11 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in11_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in12 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in12_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in13 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in13_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in14 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in14_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in15 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in15_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in16 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in16_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in17 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in17_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in18 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in18_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in19 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in19_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in20 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in20_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in21 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in21_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in22 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in22_doubt = id360out_result_doubt;
    const HWRawBits<1> &id383in_in23 = id360out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id383in_in23_doubt = id360out_result_doubt;

    id383out_result = (cat((cat((cat((cat((cat(id383in_in0,id383in_in1)),id383in_in2)),(cat((cat(id383in_in3,id383in_in4)),id383in_in5)))),(cat((cat((cat(id383in_in6,id383in_in7)),id383in_in8)),(cat((cat(id383in_in9,id383in_in10)),id383in_in11)))))),(cat((cat((cat((cat(id383in_in12,id383in_in13)),id383in_in14)),(cat((cat(id383in_in15,id383in_in16)),id383in_in17)))),(cat((cat((cat(id383in_in18,id383in_in19)),id383in_in20)),(cat((cat(id383in_in21,id383in_in22)),id383in_in23))))))));
    id383out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id383in_in0_doubt,id383in_in1_doubt)),id383in_in2_doubt)),id383in_in3_doubt)),id383in_in4_doubt)),id383in_in5_doubt)),id383in_in6_doubt)),id383in_in7_doubt)),id383in_in8_doubt)),id383in_in9_doubt)),id383in_in10_doubt)),id383in_in11_doubt)),id383in_in12_doubt)),id383in_in13_doubt)),id383in_in14_doubt)),id383in_in15_doubt)),id383in_in16_doubt)),id383in_in17_doubt)),id383in_in18_doubt)),id383in_in19_doubt)),id383in_in20_doubt)),id383in_in21_doubt)),id383in_in22_doubt)),id383in_in23_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id72out_output;
  HWOffsetFix<1,0,UNSIGNED> id72out_output_doubt;

  { // Node ID: 72 (NodeReinterpret)
    const HWRawBits<24> &id72in_input = id383out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id72in_input_doubt = id383out_result_doubt;

    id72out_output = (cast_bits2fixed<24,-14,TWOSCOMPLEMENT>(id72in_input));
    id72out_output_doubt = id72in_input_doubt;
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id73out_result;
  HWOffsetFix<1,0,UNSIGNED> id73out_result_doubt;

  { // Node ID: 73 (NodeXor)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id73in_a = id46out_value;
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id73in_b = id72out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id73in_b_doubt = id72out_output_doubt;

    HWOffsetFix<24,-14,TWOSCOMPLEMENT> id73x_1;

    (id73x_1) = (xor_fixed(id73in_a,id73in_b));
    id73out_result = (id73x_1);
    id73out_result_doubt = id73in_b_doubt;
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id74out_result;
  HWOffsetFix<1,0,UNSIGNED> id74out_result_doubt;

  { // Node ID: 74 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id74in_sel = id45out_output;
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id74in_option0 = id43out_o[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id74in_option0_doubt = id43out_o_doubt[getCycle()%7];
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id74in_option1 = id73out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id74in_option1_doubt = id73out_result_doubt;

    HWOffsetFix<24,-14,TWOSCOMPLEMENT> id74x_1;
    HWOffsetFix<1,0,UNSIGNED> id74x_2;

    switch((id74in_sel.getValueAsLong())) {
      case 0l:
        id74x_1 = id74in_option0;
        break;
      case 1l:
        id74x_1 = id74in_option1;
        break;
      default:
        id74x_1 = (c_hw_fix_24_n14_sgn_undef);
        break;
    }
    switch((id74in_sel.getValueAsLong())) {
      case 0l:
        id74x_2 = id74in_option0_doubt;
        break;
      case 1l:
        id74x_2 = id74in_option1_doubt;
        break;
      default:
        id74x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id74out_result = (id74x_1);
    id74out_result_doubt = (id74x_2);
  }
  { // Node ID: 77 (NodeConstantRawBits)
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id76out_result;
  HWOffsetFix<1,0,UNSIGNED> id76out_result_doubt;
  HWOffsetFix<1,0,UNSIGNED> id76out_exception;

  { // Node ID: 76 (NodeMul)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id76in_a = id74out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id76in_a_doubt = id74out_result_doubt;
    const HWOffsetFix<24,-23,UNSIGNED> &id76in_b = id77out_value;

    HWOffsetFix<1,0,UNSIGNED> id76x_1;

    id76out_result = (mul_fixed<48,-37,TWOSCOMPLEMENT,TONEAREVEN>(id76in_a,id76in_b,(&(id76x_1))));
    id76out_result_doubt = (or_fixed((neq_fixed((id76x_1),(c_hw_fix_1_0_uns_bits_1))),id76in_a_doubt));
    id76out_exception = (id76x_1);
  }
  { // Node ID: 79 (NodeConstantRawBits)
  }
  HWRawBits<1> id384out_result;
  HWOffsetFix<1,0,UNSIGNED> id384out_result_doubt;

  { // Node ID: 384 (NodeSlice)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id384in_a = id76out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id384in_a_doubt = id76out_result_doubt;

    id384out_result = (slice<47,1>(id384in_a));
    id384out_result_doubt = id384in_a_doubt;
  }
  HWRawBits<1> id385out_result;
  HWOffsetFix<1,0,UNSIGNED> id385out_result_doubt;

  { // Node ID: 385 (NodeNot)
    const HWRawBits<1> &id385in_a = id384out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id385in_a_doubt = id384out_result_doubt;

    id385out_result = (not_bits(id385in_a));
    id385out_result_doubt = id385in_a_doubt;
  }
  HWRawBits<48> id432out_result;
  HWOffsetFix<1,0,UNSIGNED> id432out_result_doubt;

  { // Node ID: 432 (NodeCat)
    const HWRawBits<1> &id432in_in0 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in0_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in1 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in1_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in2 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in2_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in3 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in3_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in4 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in4_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in5 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in5_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in6 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in6_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in7 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in7_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in8 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in8_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in9 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in9_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in10 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in10_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in11 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in11_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in12 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in12_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in13 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in13_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in14 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in14_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in15 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in15_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in16 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in16_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in17 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in17_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in18 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in18_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in19 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in19_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in20 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in20_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in21 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in21_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in22 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in22_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in23 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in23_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in24 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in24_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in25 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in25_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in26 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in26_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in27 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in27_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in28 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in28_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in29 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in29_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in30 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in30_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in31 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in31_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in32 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in32_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in33 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in33_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in34 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in34_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in35 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in35_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in36 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in36_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in37 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in37_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in38 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in38_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in39 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in39_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in40 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in40_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in41 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in41_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in42 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in42_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in43 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in43_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in44 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in44_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in45 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in45_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in46 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in46_doubt = id385out_result_doubt;
    const HWRawBits<1> &id432in_in47 = id385out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id432in_in47_doubt = id385out_result_doubt;

    id432out_result = (cat((cat((cat((cat((cat((cat(id432in_in0,id432in_in1)),id432in_in2)),(cat((cat(id432in_in3,id432in_in4)),id432in_in5)))),(cat((cat((cat(id432in_in6,id432in_in7)),id432in_in8)),(cat((cat(id432in_in9,id432in_in10)),id432in_in11)))))),(cat((cat((cat((cat(id432in_in12,id432in_in13)),id432in_in14)),(cat((cat(id432in_in15,id432in_in16)),id432in_in17)))),(cat((cat((cat(id432in_in18,id432in_in19)),id432in_in20)),(cat((cat(id432in_in21,id432in_in22)),id432in_in23)))))))),(cat((cat((cat((cat((cat(id432in_in24,id432in_in25)),id432in_in26)),(cat((cat(id432in_in27,id432in_in28)),id432in_in29)))),(cat((cat((cat(id432in_in30,id432in_in31)),id432in_in32)),(cat((cat(id432in_in33,id432in_in34)),id432in_in35)))))),(cat((cat((cat((cat(id432in_in36,id432in_in37)),id432in_in38)),(cat((cat(id432in_in39,id432in_in40)),id432in_in41)))),(cat((cat((cat(id432in_in42,id432in_in43)),id432in_in44)),(cat((cat(id432in_in45,id432in_in46)),id432in_in47))))))))));
    id432out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id432in_in0_doubt,id432in_in1_doubt)),id432in_in2_doubt)),id432in_in3_doubt)),id432in_in4_doubt)),id432in_in5_doubt)),id432in_in6_doubt)),id432in_in7_doubt)),id432in_in8_doubt)),id432in_in9_doubt)),id432in_in10_doubt)),id432in_in11_doubt)),id432in_in12_doubt)),id432in_in13_doubt)),id432in_in14_doubt)),id432in_in15_doubt)),id432in_in16_doubt)),id432in_in17_doubt)),id432in_in18_doubt)),id432in_in19_doubt)),id432in_in20_doubt)),id432in_in21_doubt)),id432in_in22_doubt)),id432in_in23_doubt)),id432in_in24_doubt)),id432in_in25_doubt)),id432in_in26_doubt)),id432in_in27_doubt)),id432in_in28_doubt)),id432in_in29_doubt)),id432in_in30_doubt)),id432in_in31_doubt)),id432in_in32_doubt)),id432in_in33_doubt)),id432in_in34_doubt)),id432in_in35_doubt)),id432in_in36_doubt)),id432in_in37_doubt)),id432in_in38_doubt)),id432in_in39_doubt)),id432in_in40_doubt)),id432in_in41_doubt)),id432in_in42_doubt)),id432in_in43_doubt)),id432in_in44_doubt)),id432in_in45_doubt)),id432in_in46_doubt)),id432in_in47_doubt));
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id129out_output;
  HWOffsetFix<1,0,UNSIGNED> id129out_output_doubt;

  { // Node ID: 129 (NodeReinterpret)
    const HWRawBits<48> &id129in_input = id432out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id129in_input_doubt = id432out_result_doubt;

    id129out_output = (cast_bits2fixed<48,-37,TWOSCOMPLEMENT>(id129in_input));
    id129out_output_doubt = id129in_input_doubt;
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id130out_result;
  HWOffsetFix<1,0,UNSIGNED> id130out_result_doubt;

  { // Node ID: 130 (NodeXor)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id130in_a = id79out_value;
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id130in_b = id129out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id130in_b_doubt = id129out_output_doubt;

    HWOffsetFix<48,-37,TWOSCOMPLEMENT> id130x_1;

    (id130x_1) = (xor_fixed(id130in_a,id130in_b));
    id130out_result = (id130x_1);
    id130out_result_doubt = id130in_b_doubt;
  }
  HWOffsetFix<48,-37,TWOSCOMPLEMENT> id131out_result;
  HWOffsetFix<1,0,UNSIGNED> id131out_result_doubt;

  { // Node ID: 131 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id131in_sel = id76out_exception;
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id131in_option0 = id76out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id131in_option0_doubt = id76out_result_doubt;
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id131in_option1 = id130out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id131in_option1_doubt = id130out_result_doubt;

    HWOffsetFix<48,-37,TWOSCOMPLEMENT> id131x_1;
    HWOffsetFix<1,0,UNSIGNED> id131x_2;

    switch((id131in_sel.getValueAsLong())) {
      case 0l:
        id131x_1 = id131in_option0;
        break;
      case 1l:
        id131x_1 = id131in_option1;
        break;
      default:
        id131x_1 = (c_hw_fix_48_n37_sgn_undef);
        break;
    }
    switch((id131in_sel.getValueAsLong())) {
      case 0l:
        id131x_2 = id131in_option0_doubt;
        break;
      case 1l:
        id131x_2 = id131in_option1_doubt;
        break;
      default:
        id131x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id131out_result = (id131x_1);
    id131out_result_doubt = (id131x_2);
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id132out_o;
  HWOffsetFix<1,0,UNSIGNED> id132out_o_doubt;
  HWOffsetFix<1,0,UNSIGNED> id132out_exception;

  { // Node ID: 132 (NodeCast)
    const HWOffsetFix<48,-37,TWOSCOMPLEMENT> &id132in_i = id131out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id132in_i_doubt = id131out_result_doubt;

    HWOffsetFix<1,0,UNSIGNED> id132x_1;

    id132out_o = (cast_fixed2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id132in_i,(&(id132x_1))));
    id132out_o_doubt = (or_fixed((neq_fixed((id132x_1),(c_hw_fix_1_0_uns_bits_1))),id132in_i_doubt));
    id132out_exception = (id132x_1);
  }
  { // Node ID: 134 (NodeConstantRawBits)
  }
  HWRawBits<1> id433out_result;
  HWOffsetFix<1,0,UNSIGNED> id433out_result_doubt;

  { // Node ID: 433 (NodeSlice)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id433in_a = id132out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id433in_a_doubt = id132out_o_doubt;

    id433out_result = (slice<23,1>(id433in_a));
    id433out_result_doubt = id433in_a_doubt;
  }
  HWRawBits<1> id434out_result;
  HWOffsetFix<1,0,UNSIGNED> id434out_result_doubt;

  { // Node ID: 434 (NodeNot)
    const HWRawBits<1> &id434in_a = id433out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id434in_a_doubt = id433out_result_doubt;

    id434out_result = (not_bits(id434in_a));
    id434out_result_doubt = id434in_a_doubt;
  }
  HWRawBits<24> id457out_result;
  HWOffsetFix<1,0,UNSIGNED> id457out_result_doubt;

  { // Node ID: 457 (NodeCat)
    const HWRawBits<1> &id457in_in0 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in0_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in1 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in1_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in2 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in2_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in3 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in3_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in4 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in4_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in5 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in5_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in6 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in6_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in7 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in7_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in8 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in8_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in9 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in9_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in10 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in10_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in11 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in11_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in12 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in12_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in13 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in13_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in14 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in14_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in15 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in15_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in16 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in16_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in17 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in17_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in18 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in18_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in19 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in19_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in20 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in20_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in21 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in21_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in22 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in22_doubt = id434out_result_doubt;
    const HWRawBits<1> &id457in_in23 = id434out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id457in_in23_doubt = id434out_result_doubt;

    id457out_result = (cat((cat((cat((cat((cat(id457in_in0,id457in_in1)),id457in_in2)),(cat((cat(id457in_in3,id457in_in4)),id457in_in5)))),(cat((cat((cat(id457in_in6,id457in_in7)),id457in_in8)),(cat((cat(id457in_in9,id457in_in10)),id457in_in11)))))),(cat((cat((cat((cat(id457in_in12,id457in_in13)),id457in_in14)),(cat((cat(id457in_in15,id457in_in16)),id457in_in17)))),(cat((cat((cat(id457in_in18,id457in_in19)),id457in_in20)),(cat((cat(id457in_in21,id457in_in22)),id457in_in23))))))));
    id457out_result_doubt = (or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed((or_fixed(id457in_in0_doubt,id457in_in1_doubt)),id457in_in2_doubt)),id457in_in3_doubt)),id457in_in4_doubt)),id457in_in5_doubt)),id457in_in6_doubt)),id457in_in7_doubt)),id457in_in8_doubt)),id457in_in9_doubt)),id457in_in10_doubt)),id457in_in11_doubt)),id457in_in12_doubt)),id457in_in13_doubt)),id457in_in14_doubt)),id457in_in15_doubt)),id457in_in16_doubt)),id457in_in17_doubt)),id457in_in18_doubt)),id457in_in19_doubt)),id457in_in20_doubt)),id457in_in21_doubt)),id457in_in22_doubt)),id457in_in23_doubt));
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id160out_output;
  HWOffsetFix<1,0,UNSIGNED> id160out_output_doubt;

  { // Node ID: 160 (NodeReinterpret)
    const HWRawBits<24> &id160in_input = id457out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id160in_input_doubt = id457out_result_doubt;

    id160out_output = (cast_bits2fixed<24,-14,TWOSCOMPLEMENT>(id160in_input));
    id160out_output_doubt = id160in_input_doubt;
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id161out_result;
  HWOffsetFix<1,0,UNSIGNED> id161out_result_doubt;

  { // Node ID: 161 (NodeXor)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id161in_a = id134out_value;
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id161in_b = id160out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id161in_b_doubt = id160out_output_doubt;

    HWOffsetFix<24,-14,TWOSCOMPLEMENT> id161x_1;

    (id161x_1) = (xor_fixed(id161in_a,id161in_b));
    id161out_result = (id161x_1);
    id161out_result_doubt = id161in_b_doubt;
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id162out_result;
  HWOffsetFix<1,0,UNSIGNED> id162out_result_doubt;

  { // Node ID: 162 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id162in_sel = id132out_exception;
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id162in_option0 = id132out_o;
    const HWOffsetFix<1,0,UNSIGNED> &id162in_option0_doubt = id132out_o_doubt;
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id162in_option1 = id161out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id162in_option1_doubt = id161out_result_doubt;

    HWOffsetFix<24,-14,TWOSCOMPLEMENT> id162x_1;
    HWOffsetFix<1,0,UNSIGNED> id162x_2;

    switch((id162in_sel.getValueAsLong())) {
      case 0l:
        id162x_1 = id162in_option0;
        break;
      case 1l:
        id162x_1 = id162in_option1;
        break;
      default:
        id162x_1 = (c_hw_fix_24_n14_sgn_undef);
        break;
    }
    switch((id162in_sel.getValueAsLong())) {
      case 0l:
        id162x_2 = id162in_option0_doubt;
        break;
      case 1l:
        id162x_2 = id162in_option1_doubt;
        break;
      default:
        id162x_2 = (c_hw_fix_1_0_uns_undef);
        break;
    }
    id162out_result = (id162x_1);
    id162out_result_doubt = (id162x_2);
  }
  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id171out_output;

  { // Node ID: 171 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id171in_input = id162out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id171in_input_doubt = id162out_result_doubt;

    id171out_output = id171in_input;
  }
  HWOffsetFix<10,0,TWOSCOMPLEMENT> id172out_o;

  { // Node ID: 172 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id172in_i = id171out_output;

    id172out_o = (cast_fixed2fixed<10,0,TWOSCOMPLEMENT,TRUNCATE>(id172in_i));
  }
  { // Node ID: 534 (NodeFIFO)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id534in_input = id172out_o;

    id534out_output[(getCycle()+2)%3] = id534in_input;
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id204out_o;

  { // Node ID: 204 (NodeCast)
    const HWOffsetFix<10,0,TWOSCOMPLEMENT> &id204in_i = id534out_output[getCycle()%3];

    id204out_o = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id204in_i));
  }
  { // Node ID: 567 (NodeConstantRawBits)
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id206out_result;
  HWOffsetFix<1,0,UNSIGNED> id206out_exception;

  { // Node ID: 206 (NodeAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id206in_a = id204out_o;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id206in_b = id567out_value;

    HWOffsetFix<1,0,UNSIGNED> id206x_1;

    id206out_result = (add_fixed<11,0,TWOSCOMPLEMENT,TONEAREVEN>(id206in_a,id206in_b,(&(id206x_1))));
    id206out_exception = (id206x_1);
  }
  { // Node ID: 208 (NodeConstantRawBits)
  }
  HWRawBits<1> id458out_result;

  { // Node ID: 458 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id458in_a = id206out_result;

    id458out_result = (slice<10,1>(id458in_a));
  }
  HWRawBits<1> id459out_result;

  { // Node ID: 459 (NodeNot)
    const HWRawBits<1> &id459in_a = id458out_result;

    id459out_result = (not_bits(id459in_a));
  }
  HWRawBits<11> id469out_result;

  { // Node ID: 469 (NodeCat)
    const HWRawBits<1> &id469in_in0 = id459out_result;
    const HWRawBits<1> &id469in_in1 = id459out_result;
    const HWRawBits<1> &id469in_in2 = id459out_result;
    const HWRawBits<1> &id469in_in3 = id459out_result;
    const HWRawBits<1> &id469in_in4 = id459out_result;
    const HWRawBits<1> &id469in_in5 = id459out_result;
    const HWRawBits<1> &id469in_in6 = id459out_result;
    const HWRawBits<1> &id469in_in7 = id459out_result;
    const HWRawBits<1> &id469in_in8 = id459out_result;
    const HWRawBits<1> &id469in_in9 = id459out_result;
    const HWRawBits<1> &id469in_in10 = id459out_result;

    id469out_result = (cat((cat((cat((cat(id469in_in0,id469in_in1)),id469in_in2)),(cat((cat(id469in_in3,id469in_in4)),id469in_in5)))),(cat((cat((cat(id469in_in6,id469in_in7)),id469in_in8)),(cat(id469in_in9,id469in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id221out_output;

  { // Node ID: 221 (NodeReinterpret)
    const HWRawBits<11> &id221in_input = id469out_result;

    id221out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id221in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id222out_result;

  { // Node ID: 222 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id222in_a = id208out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id222in_b = id221out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id222x_1;

    (id222x_1) = (xor_fixed(id222in_a,id222in_b));
    id222out_result = (id222x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id223out_result;

  { // Node ID: 223 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id223in_sel = id206out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id223in_option0 = id206out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id223in_option1 = id222out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id223x_1;

    switch((id223in_sel.getValueAsLong())) {
      case 0l:
        id223x_1 = id223in_option0;
        break;
      case 1l:
        id223x_1 = id223in_option1;
        break;
      default:
        id223x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id223out_result = (id223x_1);
  }
  { // Node ID: 224 (NodeConstantRawBits)
  }
  HWOffsetFix<6,-6,UNSIGNED> id173out_o;

  { // Node ID: 173 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id173in_i = id171out_output;

    id173out_o = (cast_fixed2fixed<6,-6,UNSIGNED,TRUNCATE>(id173in_i));
  }
  HWOffsetFix<6,0,UNSIGNED> id294out_output;

  { // Node ID: 294 (NodeReinterpret)
    const HWOffsetFix<6,-6,UNSIGNED> &id294in_input = id173out_o;

    id294out_output = (cast_bits2fixed<6,0,UNSIGNED>((cast_fixed2bits(id294in_input))));
  }
  { // Node ID: 295 (NodeROM)
    const HWOffsetFix<6,0,UNSIGNED> &id295in_addr = id294out_output;

    HWOffsetFix<12,-12,UNSIGNED> id295x_1;

    switch(((long)((id295in_addr.getValueAsLong())<(64l)))) {
      case 0l:
        id295x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
      case 1l:
        id295x_1 = (id295sta_rom_store[(id295in_addr.getValueAsLong())]);
        break;
      default:
        id295x_1 = (c_hw_fix_12_n12_uns_undef);
        break;
    }
    id295out_dout[(getCycle()+2)%3] = (id295x_1);
  }
  { // Node ID: 177 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id174out_o;

  { // Node ID: 174 (NodeCast)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id174in_i = id171out_output;

    id174out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TRUNCATE>(id174in_i));
  }
  { // Node ID: 535 (NodeFIFO)
    const HWOffsetFix<8,-14,UNSIGNED> &id535in_input = id174out_o;

    id535out_output[(getCycle()+2)%3] = id535in_input;
  }
  HWOffsetFix<16,-22,UNSIGNED> id176out_result;

  { // Node ID: 176 (NodeMul)
    const HWOffsetFix<8,-8,UNSIGNED> &id176in_a = id177out_value;
    const HWOffsetFix<8,-14,UNSIGNED> &id176in_b = id535out_output[getCycle()%3];

    id176out_result = (mul_fixed<16,-22,UNSIGNED,TONEAREVEN>(id176in_a,id176in_b));
  }
  HWOffsetFix<8,-14,UNSIGNED> id178out_o;
  HWOffsetFix<1,0,UNSIGNED> id178out_exception;

  { // Node ID: 178 (NodeCast)
    const HWOffsetFix<16,-22,UNSIGNED> &id178in_i = id176out_result;

    HWOffsetFix<1,0,UNSIGNED> id178x_1;

    id178out_o = (cast_fixed2fixed<8,-14,UNSIGNED,TONEAREVEN>(id178in_i,(&(id178x_1))));
    id178out_exception = (id178x_1);
  }
  { // Node ID: 180 (NodeConstantRawBits)
  }
  HWOffsetFix<8,-14,UNSIGNED> id181out_result;

  { // Node ID: 181 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id181in_sel = id178out_exception;
    const HWOffsetFix<8,-14,UNSIGNED> &id181in_option0 = id178out_o;
    const HWOffsetFix<8,-14,UNSIGNED> &id181in_option1 = id180out_value;

    HWOffsetFix<8,-14,UNSIGNED> id181x_1;

    switch((id181in_sel.getValueAsLong())) {
      case 0l:
        id181x_1 = id181in_option0;
        break;
      case 1l:
        id181x_1 = id181in_option1;
        break;
      default:
        id181x_1 = (c_hw_fix_8_n14_uns_undef);
        break;
    }
    id181out_result = (id181x_1);
  }
  HWOffsetFix<15,-14,UNSIGNED> id182out_result;

  { // Node ID: 182 (NodeAdd)
    const HWOffsetFix<12,-12,UNSIGNED> &id182in_a = id295out_dout[getCycle()%3];
    const HWOffsetFix<8,-14,UNSIGNED> &id182in_b = id181out_result;

    id182out_result = (add_fixed<15,-14,UNSIGNED,TONEAREVEN>(id182in_a,id182in_b));
  }
  HWOffsetFix<20,-26,UNSIGNED> id183out_result;

  { // Node ID: 183 (NodeMul)
    const HWOffsetFix<8,-14,UNSIGNED> &id183in_a = id181out_result;
    const HWOffsetFix<12,-12,UNSIGNED> &id183in_b = id295out_dout[getCycle()%3];

    id183out_result = (mul_fixed<20,-26,UNSIGNED,TONEAREVEN>(id183in_a,id183in_b));
  }
  HWOffsetFix<27,-26,UNSIGNED> id184out_result;
  HWOffsetFix<1,0,UNSIGNED> id184out_exception;

  { // Node ID: 184 (NodeAdd)
    const HWOffsetFix<15,-14,UNSIGNED> &id184in_a = id182out_result;
    const HWOffsetFix<20,-26,UNSIGNED> &id184in_b = id183out_result;

    HWOffsetFix<1,0,UNSIGNED> id184x_1;

    id184out_result = (add_fixed<27,-26,UNSIGNED,TONEAREVEN>(id184in_a,id184in_b,(&(id184x_1))));
    id184out_exception = (id184x_1);
  }
  { // Node ID: 186 (NodeConstantRawBits)
  }
  HWOffsetFix<27,-26,UNSIGNED> id187out_result;

  { // Node ID: 187 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id187in_sel = id184out_exception;
    const HWOffsetFix<27,-26,UNSIGNED> &id187in_option0 = id184out_result;
    const HWOffsetFix<27,-26,UNSIGNED> &id187in_option1 = id186out_value;

    HWOffsetFix<27,-26,UNSIGNED> id187x_1;

    switch((id187in_sel.getValueAsLong())) {
      case 0l:
        id187x_1 = id187in_option0;
        break;
      case 1l:
        id187x_1 = id187in_option1;
        break;
      default:
        id187x_1 = (c_hw_fix_27_n26_uns_undef);
        break;
    }
    id187out_result = (id187x_1);
  }
  HWOffsetFix<15,-14,UNSIGNED> id188out_o;
  HWOffsetFix<1,0,UNSIGNED> id188out_exception;

  { // Node ID: 188 (NodeCast)
    const HWOffsetFix<27,-26,UNSIGNED> &id188in_i = id187out_result;

    HWOffsetFix<1,0,UNSIGNED> id188x_1;

    id188out_o = (cast_fixed2fixed<15,-14,UNSIGNED,TONEAREVEN>(id188in_i,(&(id188x_1))));
    id188out_exception = (id188x_1);
  }
  { // Node ID: 190 (NodeConstantRawBits)
  }
  HWOffsetFix<15,-14,UNSIGNED> id191out_result;

  { // Node ID: 191 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id191in_sel = id188out_exception;
    const HWOffsetFix<15,-14,UNSIGNED> &id191in_option0 = id188out_o;
    const HWOffsetFix<15,-14,UNSIGNED> &id191in_option1 = id190out_value;

    HWOffsetFix<15,-14,UNSIGNED> id191x_1;

    switch((id191in_sel.getValueAsLong())) {
      case 0l:
        id191x_1 = id191in_option0;
        break;
      case 1l:
        id191x_1 = id191in_option1;
        break;
      default:
        id191x_1 = (c_hw_fix_15_n14_uns_undef);
        break;
    }
    id191out_result = (id191x_1);
  }
  HWOffsetFix<12,-11,UNSIGNED> id192out_o;
  HWOffsetFix<1,0,UNSIGNED> id192out_exception;

  { // Node ID: 192 (NodeCast)
    const HWOffsetFix<15,-14,UNSIGNED> &id192in_i = id191out_result;

    HWOffsetFix<1,0,UNSIGNED> id192x_1;

    id192out_o = (cast_fixed2fixed<12,-11,UNSIGNED,TONEAREVEN>(id192in_i,(&(id192x_1))));
    id192out_exception = (id192x_1);
  }
  { // Node ID: 194 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id195out_result;

  { // Node ID: 195 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id195in_sel = id192out_exception;
    const HWOffsetFix<12,-11,UNSIGNED> &id195in_option0 = id192out_o;
    const HWOffsetFix<12,-11,UNSIGNED> &id195in_option1 = id194out_value;

    HWOffsetFix<12,-11,UNSIGNED> id195x_1;

    switch((id195in_sel.getValueAsLong())) {
      case 0l:
        id195x_1 = id195in_option0;
        break;
      case 1l:
        id195x_1 = id195in_option1;
        break;
      default:
        id195x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id195out_result = (id195x_1);
  }
  { // Node ID: 566 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id470out_result;

  { // Node ID: 470 (NodeGteInlined)
    const HWOffsetFix<12,-11,UNSIGNED> &id470in_a = id195out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id470in_b = id566out_value;

    id470out_result = (gte_fixed(id470in_a,id470in_b));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id500out_result;
  HWOffsetFix<1,0,UNSIGNED> id500out_exception;

  { // Node ID: 500 (NodeCondAdd)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id500in_a = id223out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id500in_b = id224out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id500in_condb = id470out_result;

    HWOffsetFix<1,0,UNSIGNED> id500x_1;
    HWOffsetFix<1,0,UNSIGNED> id500x_2;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id500x_3;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id500x_4;
    HWOffsetFix<12,0,TWOSCOMPLEMENT> id500x_5;

    switch(((c_hw_fix_1_0_uns_bits).getValueAsLong())) {
      case 0l:
        id500x_3 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id500x_3 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id500in_a));
        break;
      default:
        id500x_3 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    switch((id500in_condb.getValueAsLong())) {
      case 0l:
        id500x_4 = (c_hw_fix_12_0_sgn_bits);
        break;
      case 1l:
        id500x_4 = (cast_fixed2fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>(id500in_b));
        break;
      default:
        id500x_4 = (c_hw_fix_12_0_sgn_undef);
        break;
    }
    (id500x_5) = (add_fixed<12,0,TWOSCOMPLEMENT,TRUNCATE>((id500x_3),(id500x_4),(&(id500x_1))));
    id500out_result = (cast_fixed2fixed<11,0,TWOSCOMPLEMENT,TRUNCATE>((id500x_5),(&(id500x_2))));
    id500out_exception = (or_fixed((id500x_1),(id500x_2)));
  }
  { // Node ID: 229 (NodeConstantRawBits)
  }
  HWRawBits<1> id471out_result;

  { // Node ID: 471 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id471in_a = id500out_result;

    id471out_result = (slice<10,1>(id471in_a));
  }
  HWRawBits<1> id472out_result;

  { // Node ID: 472 (NodeNot)
    const HWRawBits<1> &id472in_a = id471out_result;

    id472out_result = (not_bits(id472in_a));
  }
  HWRawBits<11> id482out_result;

  { // Node ID: 482 (NodeCat)
    const HWRawBits<1> &id482in_in0 = id472out_result;
    const HWRawBits<1> &id482in_in1 = id472out_result;
    const HWRawBits<1> &id482in_in2 = id472out_result;
    const HWRawBits<1> &id482in_in3 = id472out_result;
    const HWRawBits<1> &id482in_in4 = id472out_result;
    const HWRawBits<1> &id482in_in5 = id472out_result;
    const HWRawBits<1> &id482in_in6 = id472out_result;
    const HWRawBits<1> &id482in_in7 = id472out_result;
    const HWRawBits<1> &id482in_in8 = id472out_result;
    const HWRawBits<1> &id482in_in9 = id472out_result;
    const HWRawBits<1> &id482in_in10 = id472out_result;

    id482out_result = (cat((cat((cat((cat(id482in_in0,id482in_in1)),id482in_in2)),(cat((cat(id482in_in3,id482in_in4)),id482in_in5)))),(cat((cat((cat(id482in_in6,id482in_in7)),id482in_in8)),(cat(id482in_in9,id482in_in10))))));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id242out_output;

  { // Node ID: 242 (NodeReinterpret)
    const HWRawBits<11> &id242in_input = id482out_result;

    id242out_output = (cast_bits2fixed<11,0,TWOSCOMPLEMENT>(id242in_input));
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id243out_result;

  { // Node ID: 243 (NodeXor)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id243in_a = id229out_value;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id243in_b = id242out_output;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id243x_1;

    (id243x_1) = (xor_fixed(id243in_a,id243in_b));
    id243out_result = (id243x_1);
  }
  HWOffsetFix<11,0,TWOSCOMPLEMENT> id244out_result;

  { // Node ID: 244 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id244in_sel = id500out_exception;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id244in_option0 = id500out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id244in_option1 = id243out_result;

    HWOffsetFix<11,0,TWOSCOMPLEMENT> id244x_1;

    switch((id244in_sel.getValueAsLong())) {
      case 0l:
        id244x_1 = id244in_option0;
        break;
      case 1l:
        id244x_1 = id244in_option1;
        break;
      default:
        id244x_1 = (c_hw_fix_11_0_sgn_undef);
        break;
    }
    id244out_result = (id244x_1);
  }
  HWRawBits<1> id483out_result;

  { // Node ID: 483 (NodeSlice)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id483in_a = id244out_result;

    id483out_result = (slice<10,1>(id483in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id484out_output;

  { // Node ID: 484 (NodeReinterpret)
    const HWRawBits<1> &id484in_input = id483out_result;

    id484out_output = (cast_bits2fixed<1,0,UNSIGNED>(id484in_input));
  }
  { // Node ID: 565 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id164out_result;

  { // Node ID: 164 (NodeGt)
    const HWFloat<8,12> &id164in_a = id501out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id164in_b = id565out_value;

    id164out_result = (gt_float(id164in_a,id164in_b));
  }
  { // Node ID: 536 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id536in_input = id164out_result;

    id536out_output[(getCycle()+6)%7] = id536in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id165out_output;

  { // Node ID: 165 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id165in_input = id162out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id165in_input_doubt = id162out_result_doubt;

    id165out_output = id165in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id166out_result;

  { // Node ID: 166 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id166in_a = id536out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id166in_b = id165out_output;

    HWOffsetFix<1,0,UNSIGNED> id166x_1;

    (id166x_1) = (and_fixed(id166in_a,id166in_b));
    id166out_result = (id166x_1);
  }
  { // Node ID: 537 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id537in_input = id166out_result;

    id537out_output[(getCycle()+2)%3] = id537in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id247out_result;

  { // Node ID: 247 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id247in_a = id537out_output[getCycle()%3];

    id247out_result = (not_fixed(id247in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id248out_result;

  { // Node ID: 248 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id248in_a = id484out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id248in_b = id247out_result;

    HWOffsetFix<1,0,UNSIGNED> id248x_1;

    (id248x_1) = (and_fixed(id248in_a,id248in_b));
    id248out_result = (id248x_1);
  }
  { // Node ID: 564 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id168out_result;

  { // Node ID: 168 (NodeLt)
    const HWFloat<8,12> &id168in_a = id501out_floatOut[getCycle()%2];
    const HWFloat<8,12> &id168in_b = id564out_value;

    id168out_result = (lt_float(id168in_a,id168in_b));
  }
  { // Node ID: 538 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id538in_input = id168out_result;

    id538out_output[(getCycle()+6)%7] = id538in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id169out_output;

  { // Node ID: 169 (NodeDoubtBitOp)
    const HWOffsetFix<24,-14,TWOSCOMPLEMENT> &id169in_input = id162out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id169in_input_doubt = id162out_result_doubt;

    id169out_output = id169in_input_doubt;
  }
  HWOffsetFix<1,0,UNSIGNED> id170out_result;

  { // Node ID: 170 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id170in_a = id538out_output[getCycle()%7];
    const HWOffsetFix<1,0,UNSIGNED> &id170in_b = id169out_output;

    HWOffsetFix<1,0,UNSIGNED> id170x_1;

    (id170x_1) = (and_fixed(id170in_a,id170in_b));
    id170out_result = (id170x_1);
  }
  { // Node ID: 539 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id539in_input = id170out_result;

    id539out_output[(getCycle()+2)%3] = id539in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id249out_result;

  { // Node ID: 249 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id249in_a = id248out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id249in_b = id539out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id249x_1;

    (id249x_1) = (or_fixed(id249in_a,id249in_b));
    id249out_result = (id249x_1);
  }
  { // Node ID: 563 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id485out_result;

  { // Node ID: 485 (NodeGteInlined)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id485in_a = id244out_result;
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id485in_b = id563out_value;

    id485out_result = (gte_fixed(id485in_a,id485in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id270out_result;

  { // Node ID: 270 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id270in_a = id539out_output[getCycle()%3];

    id270out_result = (not_fixed(id270in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id271out_result;

  { // Node ID: 271 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id271in_a = id485out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id271in_b = id270out_result;

    HWOffsetFix<1,0,UNSIGNED> id271x_1;

    (id271x_1) = (and_fixed(id271in_a,id271in_b));
    id271out_result = (id271x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id272out_result;

  { // Node ID: 272 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id272in_a = id271out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id272in_b = id537out_output[getCycle()%3];

    HWOffsetFix<1,0,UNSIGNED> id272x_1;

    (id272x_1) = (or_fixed(id272in_a,id272in_b));
    id272out_result = (id272x_1);
  }
  HWRawBits<2> id273out_result;

  { // Node ID: 273 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id273in_in0 = id249out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id273in_in1 = id272out_result;

    id273out_result = (cat(id273in_in0,id273in_in1));
  }
  { // Node ID: 265 (NodeConstantRawBits)
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id250out_o;
  HWOffsetFix<1,0,UNSIGNED> id250out_exception;

  { // Node ID: 250 (NodeCast)
    const HWOffsetFix<11,0,TWOSCOMPLEMENT> &id250in_i = id244out_result;

    HWOffsetFix<1,0,UNSIGNED> id250x_1;

    id250out_o = (cast_fixed2fixed<8,0,TWOSCOMPLEMENT,TONEAREVEN>(id250in_i,(&(id250x_1))));
    id250out_exception = (id250x_1);
  }
  { // Node ID: 252 (NodeConstantRawBits)
  }
  HWRawBits<1> id486out_result;

  { // Node ID: 486 (NodeSlice)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id486in_a = id250out_o;

    id486out_result = (slice<7,1>(id486in_a));
  }
  HWRawBits<1> id487out_result;

  { // Node ID: 487 (NodeNot)
    const HWRawBits<1> &id487in_a = id486out_result;

    id487out_result = (not_bits(id487in_a));
  }
  HWRawBits<8> id494out_result;

  { // Node ID: 494 (NodeCat)
    const HWRawBits<1> &id494in_in0 = id487out_result;
    const HWRawBits<1> &id494in_in1 = id487out_result;
    const HWRawBits<1> &id494in_in2 = id487out_result;
    const HWRawBits<1> &id494in_in3 = id487out_result;
    const HWRawBits<1> &id494in_in4 = id487out_result;
    const HWRawBits<1> &id494in_in5 = id487out_result;
    const HWRawBits<1> &id494in_in6 = id487out_result;
    const HWRawBits<1> &id494in_in7 = id487out_result;

    id494out_result = (cat((cat((cat(id494in_in0,id494in_in1)),(cat(id494in_in2,id494in_in3)))),(cat((cat(id494in_in4,id494in_in5)),(cat(id494in_in6,id494in_in7))))));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id262out_output;

  { // Node ID: 262 (NodeReinterpret)
    const HWRawBits<8> &id262in_input = id494out_result;

    id262out_output = (cast_bits2fixed<8,0,TWOSCOMPLEMENT>(id262in_input));
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id263out_result;

  { // Node ID: 263 (NodeXor)
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id263in_a = id252out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id263in_b = id262out_output;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id263x_1;

    (id263x_1) = (xor_fixed(id263in_a,id263in_b));
    id263out_result = (id263x_1);
  }
  HWOffsetFix<8,0,TWOSCOMPLEMENT> id264out_result;

  { // Node ID: 264 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id264in_sel = id250out_exception;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id264in_option0 = id250out_o;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id264in_option1 = id263out_result;

    HWOffsetFix<8,0,TWOSCOMPLEMENT> id264x_1;

    switch((id264in_sel.getValueAsLong())) {
      case 0l:
        id264x_1 = id264in_option0;
        break;
      case 1l:
        id264x_1 = id264in_option1;
        break;
      default:
        id264x_1 = (c_hw_fix_8_0_sgn_undef);
        break;
    }
    id264out_result = (id264x_1);
  }
  { // Node ID: 198 (NodeConstantRawBits)
  }
  HWOffsetFix<12,-11,UNSIGNED> id199out_result;

  { // Node ID: 199 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id199in_sel = id470out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id199in_option0 = id195out_result;
    const HWOffsetFix<12,-11,UNSIGNED> &id199in_option1 = id198out_value;

    HWOffsetFix<12,-11,UNSIGNED> id199x_1;

    switch((id199in_sel.getValueAsLong())) {
      case 0l:
        id199x_1 = id199in_option0;
        break;
      case 1l:
        id199x_1 = id199in_option1;
        break;
      default:
        id199x_1 = (c_hw_fix_12_n11_uns_undef);
        break;
    }
    id199out_result = (id199x_1);
  }
  HWOffsetFix<11,-11,UNSIGNED> id200out_o;
  HWOffsetFix<1,0,UNSIGNED> id200out_exception;

  { // Node ID: 200 (NodeCast)
    const HWOffsetFix<12,-11,UNSIGNED> &id200in_i = id199out_result;

    HWOffsetFix<1,0,UNSIGNED> id200x_1;

    id200out_o = (cast_fixed2fixed<11,-11,UNSIGNED,TONEAREVEN>(id200in_i,(&(id200x_1))));
    id200out_exception = (id200x_1);
  }
  { // Node ID: 202 (NodeConstantRawBits)
  }
  HWOffsetFix<11,-11,UNSIGNED> id203out_result;

  { // Node ID: 203 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id203in_sel = id200out_exception;
    const HWOffsetFix<11,-11,UNSIGNED> &id203in_option0 = id200out_o;
    const HWOffsetFix<11,-11,UNSIGNED> &id203in_option1 = id202out_value;

    HWOffsetFix<11,-11,UNSIGNED> id203x_1;

    switch((id203in_sel.getValueAsLong())) {
      case 0l:
        id203x_1 = id203in_option0;
        break;
      case 1l:
        id203x_1 = id203in_option1;
        break;
      default:
        id203x_1 = (c_hw_fix_11_n11_uns_undef);
        break;
    }
    id203out_result = (id203x_1);
  }
  HWRawBits<20> id266out_result;

  { // Node ID: 266 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id266in_in0 = id265out_value;
    const HWOffsetFix<8,0,TWOSCOMPLEMENT> &id266in_in1 = id264out_result;
    const HWOffsetFix<11,-11,UNSIGNED> &id266in_in2 = id203out_result;

    id266out_result = (cat((cat(id266in_in0,id266in_in1)),id266in_in2));
  }
  HWFloat<8,12> id267out_output;

  { // Node ID: 267 (NodeReinterpret)
    const HWRawBits<20> &id267in_input = id266out_result;

    id267out_output = (cast_bits2float<8,12>(id267in_input));
  }
  { // Node ID: 274 (NodeConstantRawBits)
  }
  { // Node ID: 275 (NodeConstantRawBits)
  }
  { // Node ID: 277 (NodeConstantRawBits)
  }
  HWRawBits<20> id495out_result;

  { // Node ID: 495 (NodeCat)
    const HWOffsetFix<1,0,UNSIGNED> &id495in_in0 = id274out_value;
    const HWOffsetFix<8,0,UNSIGNED> &id495in_in1 = id275out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id495in_in2 = id277out_value;

    id495out_result = (cat((cat(id495in_in0,id495in_in1)),id495in_in2));
  }
  HWFloat<8,12> id279out_output;

  { // Node ID: 279 (NodeReinterpret)
    const HWRawBits<20> &id279in_input = id495out_result;

    id279out_output = (cast_bits2float<8,12>(id279in_input));
  }
  { // Node ID: 354 (NodeConstantRawBits)
  }
  HWFloat<8,12> id282out_result;

  { // Node ID: 282 (NodeMux)
    const HWRawBits<2> &id282in_sel = id273out_result;
    const HWFloat<8,12> &id282in_option0 = id267out_output;
    const HWFloat<8,12> &id282in_option1 = id279out_output;
    const HWFloat<8,12> &id282in_option2 = id354out_value;
    const HWFloat<8,12> &id282in_option3 = id279out_output;

    HWFloat<8,12> id282x_1;

    switch((id282in_sel.getValueAsLong())) {
      case 0l:
        id282x_1 = id282in_option0;
        break;
      case 1l:
        id282x_1 = id282in_option1;
        break;
      case 2l:
        id282x_1 = id282in_option2;
        break;
      case 3l:
        id282x_1 = id282in_option3;
        break;
      default:
        id282x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id282out_result = (id282x_1);
  }
  { // Node ID: 562 (NodeConstantRawBits)
  }
  HWFloat<8,12> id292out_result;

  { // Node ID: 292 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id292in_sel = id542out_output[getCycle()%9];
    const HWFloat<8,12> &id292in_option0 = id282out_result;
    const HWFloat<8,12> &id292in_option1 = id562out_value;

    HWFloat<8,12> id292x_1;

    switch((id292in_sel.getValueAsLong())) {
      case 0l:
        id292x_1 = id292in_option0;
        break;
      case 1l:
        id292x_1 = id292in_option1;
        break;
      default:
        id292x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id292out_result = (id292x_1);
  }
  { // Node ID: 561 (NodeConstantRawBits)
  }
  HWFloat<8,12> id297out_result;
  HWOffsetFix<4,0,UNSIGNED> id297out_exception;

  { // Node ID: 297 (NodeAdd)
    const HWFloat<8,12> &id297in_a = id292out_result;
    const HWFloat<8,12> &id297in_b = id561out_value;

    HWOffsetFix<4,0,UNSIGNED> id297x_1;

    id297out_result = (add_float(id297in_a,id297in_b,(&(id297x_1))));
    id297out_exception = (id297x_1);
  }
  HWFloat<8,12> id299out_result;
  HWOffsetFix<4,0,UNSIGNED> id299out_exception;

  { // Node ID: 299 (NodeDiv)
    const HWFloat<8,12> &id299in_a = id569out_value;
    const HWFloat<8,12> &id299in_b = id297out_result;

    HWOffsetFix<4,0,UNSIGNED> id299x_1;

    id299out_result = (div_float(id299in_a,id299in_b,(&(id299x_1))));
    id299out_exception = (id299x_1);
  }
  HWFloat<8,12> id301out_result;
  HWOffsetFix<4,0,UNSIGNED> id301out_exception;

  { // Node ID: 301 (NodeSub)
    const HWFloat<8,12> &id301in_a = id570out_value;
    const HWFloat<8,12> &id301in_b = id299out_result;

    HWOffsetFix<4,0,UNSIGNED> id301x_1;

    id301out_result = (sub_float(id301in_a,id301in_b,(&(id301x_1))));
    id301out_exception = (id301x_1);
  }
  HWFloat<8,12> id304out_result;

  { // Node ID: 304 (NodeSub)
    const HWFloat<8,12> &id304in_a = id571out_value;
    const HWFloat<8,12> &id304in_b = id301out_result;

    id304out_result = (sub_float(id304in_a,id304in_b));
  }
  HWFloat<8,12> id305out_result;

  { // Node ID: 305 (NodeMul)
    const HWFloat<8,12> &id305in_a = id304out_result;
    const HWFloat<8,12> &id305in_b = id543out_output[getCycle()%10];

    id305out_result = (mul_float(id305in_a,id305in_b));
  }
  HWFloat<8,12> id306out_result;

  { // Node ID: 306 (NodeMul)
    const HWFloat<8,12> &id306in_a = id305out_result;
    const HWFloat<8,12> &id306in_b = id543out_output[getCycle()%10];

    id306out_result = (mul_float(id306in_a,id306in_b));
  }
  { // Node ID: 502 (NodePO2FPMult)
    const HWFloat<8,12> &id502in_floatIn = id306out_result;

    id502out_floatOut[(getCycle()+1)%2] = (mul_float(id502in_floatIn,(c_hw_flt_8_12_0_5val)));
  }
  HWFloat<8,12> id311out_result;

  { // Node ID: 311 (NodeMul)
    const HWFloat<8,12> &id311in_a = id310out_result;
    const HWFloat<8,12> &id311in_b = id502out_floatOut[getCycle()%2];

    id311out_result = (mul_float(id311in_a,id311in_b));
  }
  HWFloat<8,12> id496out_result;

  { // Node ID: 496 (NodeSub)
    const HWFloat<8,12> &id496in_a = id311out_result;
    const HWFloat<8,12> &id496in_b = id555out_output[getCycle()%2];

    id496out_result = (sub_float(id496in_a,id496in_b));
  }
  { // Node ID: 2 (NodeConstantRawBits)
  }
  HWFloat<8,12> id318out_result;

  { // Node ID: 318 (NodeMul)
    const HWFloat<8,12> &id318in_a = id496out_result;
    const HWFloat<8,12> &id318in_b = id2out_value;

    id318out_result = (mul_float(id318in_a,id318in_b));
  }
  HWFloat<8,12> id320out_result;

  { // Node ID: 320 (NodeMul)
    const HWFloat<8,12> &id320in_a = id19out_o[getCycle()%4];
    const HWFloat<8,12> &id320in_b = id318out_result;

    id320out_result = (mul_float(id320in_a,id320in_b));
  }
  HWFloat<8,12> id321out_result;

  { // Node ID: 321 (NodeAdd)
    const HWFloat<8,12> &id321in_a = id555out_output[getCycle()%2];
    const HWFloat<8,12> &id321in_b = id320out_result;

    id321out_result = (add_float(id321in_a,id321in_b));
  }
  HWFloat<8,12> id527out_output;

  { // Node ID: 527 (NodeStreamOffset)
    const HWFloat<8,12> &id527in_input = id321out_result;

    id527out_output = id527in_input;
  }
  { // Node ID: 22 (NodeConstantRawBits)
  }
  { // Node ID: 23 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id23in_sel = id355out_result[getCycle()%2];
    const HWFloat<8,12> &id23in_option0 = id527out_output;
    const HWFloat<8,12> &id23in_option1 = id22out_value;

    HWFloat<8,12> id23x_1;

    switch((id23in_sel.getValueAsLong())) {
      case 0l:
        id23x_1 = id23in_option0;
        break;
      case 1l:
        id23x_1 = id23in_option1;
        break;
      default:
        id23x_1 = (c_hw_flt_8_12_undef);
        break;
    }
    id23out_result[(getCycle()+1)%2] = (id23x_1);
  }
  { // Node ID: 4 (NodeConstantRawBits)
  }
  HWFloat<8,12> id38out_result;

  { // Node ID: 38 (NodeSub)
    const HWFloat<8,12> &id38in_a = id23out_result[getCycle()%2];
    const HWFloat<8,12> &id38in_b = id4out_value;

    id38out_result = (sub_float(id38in_a,id38in_b));
  }
  { // Node ID: 501 (NodePO2FPMult)
    const HWFloat<8,12> &id501in_floatIn = id38out_result;

    HWOffsetFix<4,0,UNSIGNED> id501x_1;

    id501out_floatOut[(getCycle()+1)%2] = (mul_float(id501in_floatIn,(c_hw_flt_8_12_2_0val),(&(id501x_1))));
    id501out_exception[(getCycle()+1)%2] = (id501x_1);
  }
  { // Node ID: 41 (NodeConstantRawBits)
  }
  HWFloat<8,12> id42out_output;
  HWOffsetFix<1,0,UNSIGNED> id42out_output_doubt;

  { // Node ID: 42 (NodeDoubtBitOp)
    const HWFloat<8,12> &id42in_input = id501out_floatOut[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id42in_doubt = id41out_value;

    id42out_output = id42in_input;
    id42out_output_doubt = id42in_doubt;
  }
  { // Node ID: 43 (NodeCast)
    const HWFloat<8,12> &id43in_i = id42out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id43in_i_doubt = id42out_output_doubt;

    HWOffsetFix<4,0,UNSIGNED> id43x_1;

    id43out_o[(getCycle()+6)%7] = (cast_float2fixed<24,-14,TWOSCOMPLEMENT,TONEAREVEN>(id43in_i,(&(id43x_1))));
    id43out_o_doubt[(getCycle()+6)%7] = (or_fixed((neq_fixed((id43x_1),(c_hw_fix_4_0_uns_bits))),id43in_i_doubt));
    id43out_exception[(getCycle()+6)%7] = (id43x_1);
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 506 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id506in_exceptionVector = id43out_exception[getCycle()%7];

    (id506st_reg) = (or_fixed((id506st_reg),id506in_exceptionVector));
    id506out_exceptionOccurred[(getCycle()+1)%2] = (id506st_reg);
  }
  { // Node ID: 547 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id547in_input = id506out_exceptionOccurred[getCycle()%2];

    id547out_output[(getCycle()+2)%3] = id547in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 507 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id507in_exceptionVector = id76out_exception;

    (id507st_reg) = (or_fixed((id507st_reg),id507in_exceptionVector));
    id507out_exceptionOccurred[(getCycle()+1)%2] = (id507st_reg);
  }
  { // Node ID: 548 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id548in_input = id507out_exceptionOccurred[getCycle()%2];

    id548out_output[(getCycle()+2)%3] = id548in_input;
  }
  if ( (getFillLevel() >= (12l)))
  { // Node ID: 508 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id508in_exceptionVector = id132out_exception;

    (id508st_reg) = (or_fixed((id508st_reg),id508in_exceptionVector));
    id508out_exceptionOccurred[(getCycle()+1)%2] = (id508st_reg);
  }
  { // Node ID: 549 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id549in_input = id508out_exceptionOccurred[getCycle()%2];

    id549out_output[(getCycle()+2)%3] = id549in_input;
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 510 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id510in_exceptionVector = id178out_exception;

    (id510st_reg) = (or_fixed((id510st_reg),id510in_exceptionVector));
    id510out_exceptionOccurred[(getCycle()+1)%2] = (id510st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 511 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id511in_exceptionVector = id184out_exception;

    (id511st_reg) = (or_fixed((id511st_reg),id511in_exceptionVector));
    id511out_exceptionOccurred[(getCycle()+1)%2] = (id511st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 512 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id512in_exceptionVector = id188out_exception;

    (id512st_reg) = (or_fixed((id512st_reg),id512in_exceptionVector));
    id512out_exceptionOccurred[(getCycle()+1)%2] = (id512st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 513 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id513in_exceptionVector = id192out_exception;

    (id513st_reg) = (or_fixed((id513st_reg),id513in_exceptionVector));
    id513out_exceptionOccurred[(getCycle()+1)%2] = (id513st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 516 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id516in_exceptionVector = id200out_exception;

    (id516st_reg) = (or_fixed((id516st_reg),id516in_exceptionVector));
    id516out_exceptionOccurred[(getCycle()+1)%2] = (id516st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 509 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id509in_exceptionVector = id206out_exception;

    (id509st_reg) = (or_fixed((id509st_reg),id509in_exceptionVector));
    id509out_exceptionOccurred[(getCycle()+1)%2] = (id509st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 515 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id515in_exceptionVector = id250out_exception;

    (id515st_reg) = (or_fixed((id515st_reg),id515in_exceptionVector));
    id515out_exceptionOccurred[(getCycle()+1)%2] = (id515st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 517 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id517in_exceptionVector = id297out_exception;

    (id517st_reg) = (or_fixed((id517st_reg),id517in_exceptionVector));
    id517out_exceptionOccurred[(getCycle()+1)%2] = (id517st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 518 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id518in_exceptionVector = id299out_exception;

    (id518st_reg) = (or_fixed((id518st_reg),id518in_exceptionVector));
    id518out_exceptionOccurred[(getCycle()+1)%2] = (id518st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 519 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id519in_exceptionVector = id301out_exception;

    (id519st_reg) = (or_fixed((id519st_reg),id519in_exceptionVector));
    id519out_exceptionOccurred[(getCycle()+1)%2] = (id519st_reg);
  }
  if ( (getFillLevel() >= (14l)))
  { // Node ID: 514 (NodeExceptionMask)
    const HWOffsetFix<1,0,UNSIGNED> &id514in_exceptionVector = id500out_exception;

    (id514st_reg) = (or_fixed((id514st_reg),id514in_exceptionVector));
    id514out_exceptionOccurred[(getCycle()+1)%2] = (id514st_reg);
  }
  if ( (getFillLevel() >= (6l)))
  { // Node ID: 505 (NodeExceptionMask)
    const HWOffsetFix<4,0,UNSIGNED> &id505in_exceptionVector = id501out_exception[getCycle()%2];

    (id505st_reg) = (or_fixed((id505st_reg),id505in_exceptionVector));
    id505out_exceptionOccurred[(getCycle()+1)%2] = (id505st_reg);
  }
  { // Node ID: 550 (NodeFIFO)
    const HWOffsetFix<4,0,UNSIGNED> &id550in_input = id505out_exceptionOccurred[getCycle()%2];

    id550out_output[(getCycle()+8)%9] = id550in_input;
  }
  if ( (getFillLevel() >= (15l)))
  { // Node ID: 520 (NodeNumericExceptionsSim)
    const HWOffsetFix<4,0,UNSIGNED> &id520in_n43 = id547out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n76 = id548out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n132 = id549out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n178 = id510out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n184 = id511out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n188 = id512out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n192 = id513out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n200 = id516out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n206 = id509out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n250 = id515out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id520in_n297 = id517out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id520in_n299 = id518out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id520in_n301 = id519out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id520in_n500 = id514out_exceptionOccurred[getCycle()%2];
    const HWOffsetFix<4,0,UNSIGNED> &id520in_n501 = id550out_output[getCycle()%9];

    id520out_all_exceptions = (cat((cat((cat((cat(id520in_n43,id520in_n76)),(cat(id520in_n132,id520in_n178)))),(cat((cat(id520in_n184,id520in_n188)),(cat(id520in_n192,id520in_n200)))))),(cat((cat((cat(id520in_n206,id520in_n250)),(cat(id520in_n297,id520in_n299)))),(cat((cat(id520in_n301,id520in_n500)),id520in_n501))))));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 521 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id521in_load = id522out_value;
    const HWRawBits<30> &id521in_data = id520out_all_exceptions;

    bool id521x_1;

    (id521x_1) = ((id521in_load.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id521x_1)) {
      setMappedRegValue("all_numeric_exceptions", id521in_data);
    }
  }
  { // Node ID: 503 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 504 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id504in_output_control = id503out_value;
    const HWFloat<8,12> &id504in_data = id527out_output;

    bool id504x_1;

    (id504x_1) = ((id504in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id504x_1)) {
      writeOutput(m_internal_watch_carriedu_output, id504in_data);
    }
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 28 (NodeWatch)
  }
  { // Node ID: 551 (NodeFIFO)
    const HWOffsetFix<11,0,UNSIGNED> &id551in_input = id17out_count;

    id551out_output[(getCycle()+1)%2] = id551in_input;
  }
  { // Node ID: 560 (NodeConstantRawBits)
  }
  { // Node ID: 329 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id329in_a = id556out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id329in_b = id560out_value;

    id329out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id329in_a,id329in_b));
  }
  { // Node ID: 497 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id497in_a = id551out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id497in_b = id329out_result[getCycle()%2];

    id497out_result[(getCycle()+1)%2] = (eq_fixed(id497in_a,id497in_b));
  }
  { // Node ID: 331 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id332out_result;

  { // Node ID: 332 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id332in_a = id331out_io_u_out_force_disabled;

    id332out_result = (not_fixed(id332in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id333out_result;

  { // Node ID: 333 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id333in_a = id497out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id333in_b = id332out_result;

    HWOffsetFix<1,0,UNSIGNED> id333x_1;

    (id333x_1) = (and_fixed(id333in_a,id333in_b));
    id333out_result = (id333x_1);
  }
  { // Node ID: 552 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id552in_input = id333out_result;

    id552out_output[(getCycle()+10)%11] = id552in_input;
  }
  HWFloat<8,24> id326out_o;

  { // Node ID: 326 (NodeCast)
    const HWFloat<8,12> &id326in_i = id321out_result;

    id326out_o = (cast_float2float<8,24>(id326in_i));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 334 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id334in_output_control = id552out_output[getCycle()%11];
    const HWFloat<8,24> &id334in_data = id326out_o;

    bool id334x_1;

    (id334x_1) = ((id334in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id334x_1)) {
      writeOutput(m_u_out, id334in_data);
    }
  }
  { // Node ID: 559 (NodeConstantRawBits)
  }
  { // Node ID: 336 (NodeSub)
    const HWOffsetFix<11,0,UNSIGNED> &id336in_a = id556out_value;
    const HWOffsetFix<11,0,UNSIGNED> &id336in_b = id559out_value;

    id336out_result[(getCycle()+1)%2] = (sub_fixed<11,0,UNSIGNED,TONEAREVEN>(id336in_a,id336in_b));
  }
  { // Node ID: 498 (NodeEqInlined)
    const HWOffsetFix<11,0,UNSIGNED> &id498in_a = id551out_output[getCycle()%2];
    const HWOffsetFix<11,0,UNSIGNED> &id498in_b = id336out_result[getCycle()%2];

    id498out_result[(getCycle()+1)%2] = (eq_fixed(id498in_a,id498in_b));
  }
  { // Node ID: 338 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id339out_result;

  { // Node ID: 339 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id339in_a = id338out_io_v_out_force_disabled;

    id339out_result = (not_fixed(id339in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id340out_result;

  { // Node ID: 340 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id340in_a = id498out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id340in_b = id339out_result;

    HWOffsetFix<1,0,UNSIGNED> id340x_1;

    (id340x_1) = (and_fixed(id340in_a,id340in_b));
    id340out_result = (id340x_1);
  }
  { // Node ID: 554 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id554in_input = id340out_result;

    id554out_output[(getCycle()+10)%11] = id554in_input;
  }
  HWFloat<8,24> id327out_o;

  { // Node ID: 327 (NodeCast)
    const HWFloat<8,12> &id327in_i = id323out_result;

    id327out_o = (cast_float2float<8,24>(id327in_i));
  }
  if ( (getFillLevel() >= (15l)) && (getFlushLevel() < (15l)|| !isFlushingActive() ))
  { // Node ID: 341 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id341in_output_control = id554out_output[getCycle()%11];
    const HWFloat<8,24> &id341in_data = id327out_o;

    bool id341x_1;

    (id341x_1) = ((id341in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(15l))&(isFlushingActive()))));
    if((id341x_1)) {
      writeOutput(m_v_out, id341in_data);
    }
  }
  { // Node ID: 346 (NodeConstantRawBits)
  }
  { // Node ID: 558 (NodeConstantRawBits)
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 344 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_enable = id558out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id344in_max = id343out_value;

    HWOffsetFix<49,0,UNSIGNED> id344x_1;
    HWOffsetFix<1,0,UNSIGNED> id344x_2;
    HWOffsetFix<1,0,UNSIGNED> id344x_3;
    HWOffsetFix<49,0,UNSIGNED> id344x_4t_1e_1;

    id344out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id344st_count)));
    (id344x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id344st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id344x_2) = (gte_fixed((id344x_1),id344in_max));
    (id344x_3) = (and_fixed((id344x_2),id344in_enable));
    id344out_wrap = (id344x_3);
    if((id344in_enable.getValueAsBool())) {
      if(((id344x_3).getValueAsBool())) {
        (id344st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id344x_4t_1e_1) = (id344x_1);
        (id344st_count) = (id344x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id345out_output;

  { // Node ID: 345 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id345in_input = id344out_count;

    id345out_output = id345in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 347 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id347in_load = id346out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id347in_data = id345out_output;

    bool id347x_1;

    (id347x_1) = ((id347in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id347x_1)) {
      setMappedRegValue("current_run_cycle_count", id347in_data);
    }
  }
  { // Node ID: 557 (NodeConstantRawBits)
  }
  { // Node ID: 349 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 350 (NodeCounter)
    const HWOffsetFix<1,0,UNSIGNED> &id350in_enable = id557out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id350in_max = id349out_value;

    HWOffsetFix<49,0,UNSIGNED> id350x_1;
    HWOffsetFix<1,0,UNSIGNED> id350x_2;
    HWOffsetFix<1,0,UNSIGNED> id350x_3;
    HWOffsetFix<49,0,UNSIGNED> id350x_4t_1e_1;

    id350out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id350st_count)));
    (id350x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id350st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id350x_2) = (gte_fixed((id350x_1),id350in_max));
    (id350x_3) = (and_fixed((id350x_2),id350in_enable));
    id350out_wrap = (id350x_3);
    if((id350in_enable.getValueAsBool())) {
      if(((id350x_3).getValueAsBool())) {
        (id350st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id350x_4t_1e_1) = (id350x_1);
        (id350st_count) = (id350x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 352 (NodeInputMappedReg)
  }
  { // Node ID: 499 (NodeEqInlined)
    const HWOffsetFix<48,0,UNSIGNED> &id499in_a = id350out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id499in_b = id352out_run_cycle_count;

    id499out_result[(getCycle()+1)%2] = (eq_fixed(id499in_a,id499in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 351 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id351in_start = id499out_result[getCycle()%2];

    if((id351in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
