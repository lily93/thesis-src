#ifndef KARMAKERNEL_H_
#define KARMAKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaKernel : public KernelManagerBlockSync {
public:
  KarmaKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id632out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id666out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id684out_value;

  HWOffsetFix<1,0,UNSIGNED> id416out_result[2];

  HWFloat<8,24> id653out_output[10];

  HWFloat<8,24> id665out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<8,24> id19out_o[4];

  HWFloat<8,24> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id683out_value;

  HWOffsetFix<1,0,UNSIGNED> id417out_result[2];

  HWFloat<8,24> id682out_value;

  HWFloat<8,24> id5out_value;

  HWFloat<8,24> id3out_value;

  HWFloat<8,24> id26out_value;

  HWFloat<8,24> id27out_result[2];

  HWFloat<8,24> id639out_output[11];

  HWFloat<8,24> id681out_value;

  HWFloat<8,24> id680out_value;

  HWFloat<8,24> id679out_value;

  HWRawBits<8> id344out_value;

  HWRawBits<23> id678out_value;

  HWOffsetFix<1,0,UNSIGNED> id652out_output[9];

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<35,-34,UNSIGNED> id89out_value;

  HWOffsetFix<71,-60,TWOSCOMPLEMENT> id91out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id169out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id644out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id677out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id266out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id282out_value;

  HWOffsetFix<22,-24,UNSIGNED> id356out_dout[3];

  HWOffsetFix<22,-24,UNSIGNED> id356sta_rom_store[1024];

  HWOffsetFix<24,-24,UNSIGNED> id353out_dout[3];

  HWOffsetFix<24,-24,UNSIGNED> id353sta_rom_store[4];

  HWOffsetFix<14,-14,UNSIGNED> id225out_value;

  HWOffsetFix<14,-26,UNSIGNED> id645out_output[3];

  HWOffsetFix<14,-26,UNSIGNED> id228out_value;

  HWOffsetFix<51,-50,UNSIGNED> id234out_value;

  HWOffsetFix<27,-26,UNSIGNED> id238out_value;

  HWOffsetFix<52,-50,UNSIGNED> id244out_value;

  HWOffsetFix<28,-26,UNSIGNED> id248out_value;

  HWOffsetFix<24,-23,UNSIGNED> id252out_value;

  HWOffsetFix<24,-23,UNSIGNED> id676out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id287out_value;

  HWFloat<8,24> id675out_value;

  HWOffsetFix<1,0,UNSIGNED> id646out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id647out_output[3];

  HWFloat<8,24> id674out_value;

  HWOffsetFix<1,0,UNSIGNED> id648out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id649out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id673out_value;

  HWOffsetFix<1,0,UNSIGNED> id323out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id310out_value;

  HWOffsetFix<24,-23,UNSIGNED> id256out_value;

  HWOffsetFix<23,-23,UNSIGNED> id260out_value;

  HWOffsetFix<1,0,UNSIGNED> id332out_value;

  HWOffsetFix<8,0,UNSIGNED> id333out_value;

  HWOffsetFix<23,0,UNSIGNED> id335out_value;

  HWFloat<8,24> id415out_value;

  HWFloat<8,24> id672out_value;

  HWFloat<8,24> id671out_value;

  HWFloat<8,24> id610out_floatOut[2];

  HWFloat<8,24> id2out_value;

  HWFloat<8,24> id22out_value;

  HWFloat<8,24> id23out_result[2];

  HWFloat<8,24> id4out_value;

  HWFloat<8,24> id609out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id609out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<36,-26,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id614out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id614st_reg;

  HWOffsetFix<4,0,UNSIGNED> id657out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id615out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id615st_reg;

  HWOffsetFix<1,0,UNSIGNED> id658out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id616out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id616st_reg;

  HWOffsetFix<1,0,UNSIGNED> id659out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id618out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id618st_reg;

  HWOffsetFix<1,0,UNSIGNED> id619out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id619st_reg;

  HWOffsetFix<1,0,UNSIGNED> id620out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id620st_reg;

  HWOffsetFix<1,0,UNSIGNED> id621out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id621st_reg;

  HWOffsetFix<1,0,UNSIGNED> id622out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id622st_reg;

  HWOffsetFix<1,0,UNSIGNED> id623out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id623st_reg;

  HWOffsetFix<1,0,UNSIGNED> id626out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id626st_reg;

  HWOffsetFix<1,0,UNSIGNED> id617out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id617st_reg;

  HWOffsetFix<1,0,UNSIGNED> id625out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id625st_reg;

  HWOffsetFix<4,0,UNSIGNED> id627out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id627st_reg;

  HWOffsetFix<4,0,UNSIGNED> id628out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id628st_reg;

  HWOffsetFix<4,0,UNSIGNED> id629out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id629st_reg;

  HWOffsetFix<1,0,UNSIGNED> id624out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id624st_reg;

  HWOffsetFix<4,0,UNSIGNED> id613out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id613st_reg;

  HWOffsetFix<4,0,UNSIGNED> id660out_output[9];

  HWRawBits<32> id630out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id611out_value;

  HWOffsetFix<11,0,UNSIGNED> id661out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id670out_value;

  HWOffsetFix<11,0,UNSIGNED> id390out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id605out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id392out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id662out_output[11];

  HWOffsetFix<11,0,UNSIGNED> id669out_value;

  HWOffsetFix<11,0,UNSIGNED> id397out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id606out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id399out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id664out_output[11];

  HWOffsetFix<1,0,UNSIGNED> id407out_value;

  HWOffsetFix<1,0,UNSIGNED> id668out_value;

  HWOffsetFix<49,0,UNSIGNED> id404out_value;

  HWOffsetFix<48,0,UNSIGNED> id405out_count;
  HWOffsetFix<1,0,UNSIGNED> id405out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id405st_count;

  HWOffsetFix<1,0,UNSIGNED> id667out_value;

  HWOffsetFix<49,0,UNSIGNED> id410out_value;

  HWOffsetFix<48,0,UNSIGNED> id411out_count;
  HWOffsetFix<1,0,UNSIGNED> id411out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id411st_count;

  HWOffsetFix<48,0,UNSIGNED> id413out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id607out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWFloat<8,24> c_hw_flt_8_24_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits_1;
  const HWFloat<8,24> c_hw_flt_8_24_bits_2;
  const HWFloat<8,24> c_hw_flt_8_24_bits_3;
  const HWFloat<8,24> c_hw_flt_8_24_bits_4;
  const HWFloat<8,24> c_hw_flt_8_24_bits_5;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<23> c_hw_bit_23_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<36,-26,TWOSCOMPLEMENT> c_hw_fix_36_n26_sgn_bits;
  const HWOffsetFix<36,-26,TWOSCOMPLEMENT> c_hw_fix_36_n26_sgn_undef;
  const HWOffsetFix<35,-34,UNSIGNED> c_hw_fix_35_n34_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<71,-60,TWOSCOMPLEMENT> c_hw_fix_71_n60_sgn_bits;
  const HWOffsetFix<71,-60,TWOSCOMPLEMENT> c_hw_fix_71_n60_sgn_undef;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<22,-24,UNSIGNED> c_hw_fix_22_n24_uns_undef;
  const HWOffsetFix<24,-24,UNSIGNED> c_hw_fix_24_n24_uns_undef;
  const HWOffsetFix<14,-14,UNSIGNED> c_hw_fix_14_n14_uns_bits;
  const HWOffsetFix<14,-26,UNSIGNED> c_hw_fix_14_n26_uns_undef;
  const HWOffsetFix<14,-26,UNSIGNED> c_hw_fix_14_n26_uns_bits;
  const HWOffsetFix<51,-50,UNSIGNED> c_hw_fix_51_n50_uns_bits;
  const HWOffsetFix<51,-50,UNSIGNED> c_hw_fix_51_n50_uns_undef;
  const HWOffsetFix<27,-26,UNSIGNED> c_hw_fix_27_n26_uns_bits;
  const HWOffsetFix<27,-26,UNSIGNED> c_hw_fix_27_n26_uns_undef;
  const HWOffsetFix<52,-50,UNSIGNED> c_hw_fix_52_n50_uns_bits;
  const HWOffsetFix<52,-50,UNSIGNED> c_hw_fix_52_n50_uns_undef;
  const HWOffsetFix<28,-26,UNSIGNED> c_hw_fix_28_n26_uns_bits;
  const HWOffsetFix<28,-26,UNSIGNED> c_hw_fix_28_n26_uns_undef;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_undef;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits_1;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_bits;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_undef;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits_2;
  const HWOffsetFix<23,-23,UNSIGNED> c_hw_fix_23_n23_uns_bits;
  const HWOffsetFix<23,-23,UNSIGNED> c_hw_fix_23_n23_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<23,0,UNSIGNED> c_hw_fix_23_0_uns_bits;
  const HWFloat<8,24> c_hw_flt_8_24_bits_6;
  const HWFloat<8,24> c_hw_flt_8_24_0_5val;
  const HWFloat<8,24> c_hw_flt_8_24_bits_7;
  const HWFloat<8,24> c_hw_flt_8_24_bits_8;
  const HWFloat<8,24> c_hw_flt_8_24_bits_9;
  const HWFloat<8,24> c_hw_flt_8_24_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMAKERNEL_H_ */
