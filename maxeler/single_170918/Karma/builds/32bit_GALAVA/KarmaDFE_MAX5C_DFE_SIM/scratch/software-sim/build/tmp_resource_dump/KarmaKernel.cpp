#include "stdsimheader.h"
#include "KarmaKernel.h"

namespace maxcompilersim {

KarmaKernel::KarmaKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 16, 2, 3, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_11_0_uns_bits((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x00bl))))
, c_hw_fix_12_0_uns_bits((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_uns_bits_1((HWOffsetFix<12,0,UNSIGNED>(varint_u<12>(0x001l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_24_undef((HWFloat<8,24>()))
, c_hw_flt_8_24_bits((HWFloat<8,24>(varint_u<32>(0x3f4ccccdl))))
, c_hw_flt_8_24_bits_1((HWFloat<8,24>(varint_u<32>(0x3fb6dae2l))))
, c_hw_flt_8_24_bits_2((HWFloat<8,24>(varint_u<32>(0x3f800000l))))
, c_hw_flt_8_24_bits_3((HWFloat<8,24>(varint_u<32>(0x3b83126fl))))
, c_hw_flt_8_24_bits_4((HWFloat<8,24>(varint_u<32>(0x00000000l))))
, c_hw_flt_8_24_bits_5((HWFloat<8,24>(varint_u<32>(0x40000000l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0xffl))))
, c_hw_bit_23_bits((HWRawBits<23>(varint_u<23>(0x000000l))))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_36_n26_sgn_bits((HWOffsetFix<36,-26,TWOSCOMPLEMENT>(varint_u<36>(0x7ffffffffl))))
, c_hw_fix_36_n26_sgn_undef((HWOffsetFix<36,-26,TWOSCOMPLEMENT>()))
, c_hw_fix_35_n34_uns_bits((HWOffsetFix<35,-34,UNSIGNED>(varint_u<35>(0x5c551d94bl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_71_n60_sgn_bits((HWOffsetFix<71,-60,TWOSCOMPLEMENT>(varint_u<71>::init(2, 0xffffffffffffffffl, 0x3fl))))
, c_hw_fix_71_n60_sgn_undef((HWOffsetFix<71,-60,TWOSCOMPLEMENT>()))
, c_hw_fix_10_0_sgn_undef((HWOffsetFix<10,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x07fl))))
, c_hw_fix_11_0_sgn_bits_1((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x3ffl))))
, c_hw_fix_11_0_sgn_undef((HWOffsetFix<11,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_2((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x001l))))
, c_hw_fix_22_n24_uns_undef((HWOffsetFix<22,-24,UNSIGNED>()))
, c_hw_fix_24_n24_uns_undef((HWOffsetFix<24,-24,UNSIGNED>()))
, c_hw_fix_14_n14_uns_bits((HWOffsetFix<14,-14,UNSIGNED>(varint_u<14>(0x2c5dl))))
, c_hw_fix_14_n26_uns_undef((HWOffsetFix<14,-26,UNSIGNED>()))
, c_hw_fix_14_n26_uns_bits((HWOffsetFix<14,-26,UNSIGNED>(varint_u<14>(0x3fffl))))
, c_hw_fix_51_n50_uns_bits((HWOffsetFix<51,-50,UNSIGNED>(varint_u<51>(0x7ffffffffffffl))))
, c_hw_fix_51_n50_uns_undef((HWOffsetFix<51,-50,UNSIGNED>()))
, c_hw_fix_27_n26_uns_bits((HWOffsetFix<27,-26,UNSIGNED>(varint_u<27>(0x7ffffffl))))
, c_hw_fix_27_n26_uns_undef((HWOffsetFix<27,-26,UNSIGNED>()))
, c_hw_fix_52_n50_uns_bits((HWOffsetFix<52,-50,UNSIGNED>(varint_u<52>(0xfffffffffffffl))))
, c_hw_fix_52_n50_uns_undef((HWOffsetFix<52,-50,UNSIGNED>()))
, c_hw_fix_28_n26_uns_bits((HWOffsetFix<28,-26,UNSIGNED>(varint_u<28>(0xfffffffl))))
, c_hw_fix_28_n26_uns_undef((HWOffsetFix<28,-26,UNSIGNED>()))
, c_hw_fix_24_n23_uns_bits((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0xffffffl))))
, c_hw_fix_24_n23_uns_undef((HWOffsetFix<24,-23,UNSIGNED>()))
, c_hw_fix_24_n23_uns_bits_1((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0x800000l))))
, c_hw_fix_12_0_sgn_bits((HWOffsetFix<12,0,TWOSCOMPLEMENT>(varint_u<12>(0x000l))))
, c_hw_fix_12_0_sgn_undef((HWOffsetFix<12,0,TWOSCOMPLEMENT>()))
, c_hw_fix_11_0_sgn_bits_3((HWOffsetFix<11,0,TWOSCOMPLEMENT>(varint_u<11>(0x0ffl))))
, c_hw_fix_8_0_sgn_bits((HWOffsetFix<8,0,TWOSCOMPLEMENT>(varint_u<8>(0x7fl))))
, c_hw_fix_8_0_sgn_undef((HWOffsetFix<8,0,TWOSCOMPLEMENT>()))
, c_hw_fix_24_n23_uns_bits_2((HWOffsetFix<24,-23,UNSIGNED>(varint_u<24>(0x000000l))))
, c_hw_fix_23_n23_uns_bits((HWOffsetFix<23,-23,UNSIGNED>(varint_u<23>(0x7fffffl))))
, c_hw_fix_23_n23_uns_undef((HWOffsetFix<23,-23,UNSIGNED>()))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xffl))))
, c_hw_fix_23_0_uns_bits((HWOffsetFix<23,0,UNSIGNED>(varint_u<23>(0x000000l))))
, c_hw_flt_8_24_bits_6((HWFloat<8,24>(varint_u<32>(0x7fc00000l))))
, c_hw_flt_8_24_0_5val((HWFloat<8,24>(varint_u<32>(0x3f000000l))))
, c_hw_flt_8_24_bits_7((HWFloat<8,24>(varint_u<32>(0x3ecccccdl))))
, c_hw_flt_8_24_bits_8((HWFloat<8,24>(varint_u<32>(0x3fc00000l))))
, c_hw_flt_8_24_bits_9((HWFloat<8,24>(varint_u<32>(0x40400000l))))
, c_hw_flt_8_24_2_0val((HWFloat<8,24>(varint_u<32>(0x40000000l))))
, c_hw_fix_4_0_uns_bits((HWOffsetFix<4,0,UNSIGNED>(varint_u<4>(0x0l))))
, c_hw_fix_4_0_uns_undef((HWOffsetFix<4,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_undef((HWOffsetFix<11,0,UNSIGNED>()))
, c_hw_fix_11_0_uns_bits_1((HWOffsetFix<11,0,UNSIGNED>(varint_u<11>(0x001l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 632 (NodeConstantRawBits)
    id632out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 14 (NodeConstantRawBits)
    id14out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 666 (NodeConstantRawBits)
    id666out_value = (c_hw_fix_11_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    registerMappedRegister("num_iteration", Data(32));
  }
  { // Node ID: 684 (NodeConstantRawBits)
    id684out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 18 (NodeInputMappedReg)
    registerMappedRegister("dt", Data(64));
  }
  { // Node ID: 6 (NodeConstantRawBits)
    id6out_value = (c_hw_flt_8_24_bits);
  }
  { // Node ID: 683 (NodeConstantRawBits)
    id683out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 682 (NodeConstantRawBits)
    id682out_value = (c_hw_flt_8_24_bits_1);
  }
  { // Node ID: 5 (NodeConstantRawBits)
    id5out_value = (c_hw_flt_8_24_bits_2);
  }
  { // Node ID: 3 (NodeConstantRawBits)
    id3out_value = (c_hw_flt_8_24_bits_3);
  }
  { // Node ID: 26 (NodeConstantRawBits)
    id26out_value = (c_hw_flt_8_24_bits_4);
  }
  { // Node ID: 681 (NodeConstantRawBits)
    id681out_value = (c_hw_flt_8_24_bits_2);
  }
  { // Node ID: 680 (NodeConstantRawBits)
    id680out_value = (c_hw_flt_8_24_bits_2);
  }
  { // Node ID: 679 (NodeConstantRawBits)
    id679out_value = (c_hw_flt_8_24_bits_5);
  }
  { // Node ID: 344 (NodeConstantRawBits)
    id344out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 678 (NodeConstantRawBits)
    id678out_value = (c_hw_bit_23_bits);
  }
  { // Node ID: 46 (NodeConstantRawBits)
    id46out_value = (c_hw_fix_36_n26_sgn_bits);
  }
  { // Node ID: 89 (NodeConstantRawBits)
    id89out_value = (c_hw_fix_35_n34_uns_bits);
  }
  { // Node ID: 91 (NodeConstantRawBits)
    id91out_value = (c_hw_fix_71_n60_sgn_bits);
  }
  { // Node ID: 169 (NodeConstantRawBits)
    id169out_value = (c_hw_fix_36_n26_sgn_bits);
  }
  { // Node ID: 677 (NodeConstantRawBits)
    id677out_value = (c_hw_fix_11_0_sgn_bits);
  }
  { // Node ID: 266 (NodeConstantRawBits)
    id266out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 282 (NodeConstantRawBits)
    id282out_value = (c_hw_fix_11_0_sgn_bits_2);
  }
  { // Node ID: 356 (NodeROM)
    uint64_t data[] = {
      0x0,
      0xb17,
      0x162f,
      0x2148,
      0x2c60,
      0x377a,
      0x4293,
      0x4dae,
      0x58c8,
      0x63e4,
      0x6eff,
      0x7a1c,
      0x8538,
      0x9055,
      0x9b73,
      0xa691,
      0xb1b0,
      0xbccf,
      0xc7ee,
      0xd30e,
      0xde2f,
      0xe950,
      0xf471,
      0xff93,
      0x10ab6,
      0x115d9,
      0x120fc,
      0x12c20,
      0x13744,
      0x14269,
      0x14d8e,
      0x158b4,
      0x163db,
      0x16f01,
      0x17a29,
      0x18550,
      0x19079,
      0x19ba1,
      0x1a6cb,
      0x1b1f4,
      0x1bd1e,
      0x1c849,
      0x1d374,
      0x1dea0,
      0x1e9cc,
      0x1f4f9,
      0x20026,
      0x20b53,
      0x21681,
      0x221b0,
      0x22cdf,
      0x2380e,
      0x2433e,
      0x24e6f,
      0x259a0,
      0x264d1,
      0x27003,
      0x27b35,
      0x28668,
      0x2919c,
      0x29cd0,
      0x2a804,
      0x2b339,
      0x2be6e,
      0x2c9a4,
      0x2d4da,
      0x2e011,
      0x2eb48,
      0x2f680,
      0x301b8,
      0x30cf1,
      0x3182a,
      0x32364,
      0x32e9e,
      0x339d9,
      0x34514,
      0x3504f,
      0x35b8c,
      0x366c8,
      0x37205,
      0x37d43,
      0x38881,
      0x393c0,
      0x39eff,
      0x3aa3e,
      0x3b57e,
      0x3c0bf,
      0x3cc00,
      0x3d741,
      0x3e283,
      0x3edc6,
      0x3f908,
      0x4044c,
      0x40f90,
      0x41ad4,
      0x42619,
      0x4315f,
      0x43ca4,
      0x447eb,
      0x45332,
      0x45e79,
      0x469c1,
      0x47509,
      0x48052,
      0x48b9b,
      0x496e5,
      0x4a22f,
      0x4ad7a,
      0x4b8c5,
      0x4c411,
      0x4cf5d,
      0x4daaa,
      0x4e5f7,
      0x4f145,
      0x4fc93,
      0x507e2,
      0x51331,
      0x51e81,
      0x529d1,
      0x53521,
      0x54072,
      0x54bc4,
      0x55716,
      0x56269,
      0x56dbc,
      0x57910,
      0x58464,
      0x58fb8,
      0x59b0d,
      0x5a663,
      0x5b1b9,
      0x5bd0f,
      0x5c866,
      0x5d3be,
      0x5df16,
      0x5ea6e,
      0x5f5c7,
      0x60121,
      0x60c7b,
      0x617d5,
      0x62330,
      0x62e8c,
      0x639e8,
      0x64544,
      0x650a1,
      0x65bfe,
      0x6675c,
      0x672bb,
      0x67e19,
      0x68979,
      0x694d9,
      0x6a039,
      0x6ab9a,
      0x6b6fb,
      0x6c25d,
      0x6cdc0,
      0x6d922,
      0x6e486,
      0x6efe9,
      0x6fb4e,
      0x706b3,
      0x71218,
      0x71d7e,
      0x728e4,
      0x7344b,
      0x73fb2,
      0x74b1a,
      0x75682,
      0x761eb,
      0x76d54,
      0x778be,
      0x78428,
      0x78f93,
      0x79afe,
      0x7a66a,
      0x7b1d6,
      0x7bd43,
      0x7c8b0,
      0x7d41e,
      0x7df8c,
      0x7eafb,
      0x7f66a,
      0x801d9,
      0x80d4a,
      0x818ba,
      0x8242b,
      0x82f9d,
      0x83b0f,
      0x84682,
      0x851f5,
      0x85d69,
      0x868dd,
      0x87452,
      0x87fc7,
      0x88b3c,
      0x896b2,
      0x8a229,
      0x8ada0,
      0x8b918,
      0x8c490,
      0x8d009,
      0x8db82,
      0x8e6fb,
      0x8f275,
      0x8fdf0,
      0x9096b,
      0x914e7,
      0x92063,
      0x92bdf,
      0x9375c,
      0x942da,
      0x94e58,
      0x959d7,
      0x96556,
      0x970d5,
      0x97c56,
      0x987d6,
      0x99357,
      0x99ed9,
      0x9aa5b,
      0x9b5dd,
      0x9c160,
      0x9cce4,
      0x9d868,
      0x9e3ed,
      0x9ef72,
      0x9faf7,
      0xa067d,
      0xa1204,
      0xa1d8b,
      0xa2913,
      0xa349b,
      0xa4023,
      0xa4bac,
      0xa5736,
      0xa62c0,
      0xa6e4a,
      0xa79d6,
      0xa8561,
      0xa90ed,
      0xa9c7a,
      0xaa807,
      0xab394,
      0xabf22,
      0xacab1,
      0xad640,
      0xae1d0,
      0xaed60,
      0xaf8f0,
      0xb0481,
      0xb1013,
      0xb1ba5,
      0xb2738,
      0xb32cb,
      0xb3e5e,
      0xb49f2,
      0xb5587,
      0xb611c,
      0xb6cb1,
      0xb7848,
      0xb83de,
      0xb8f75,
      0xb9b0d,
      0xba6a5,
      0xbb23e,
      0xbbdd7,
      0xbc970,
      0xbd50a,
      0xbe0a5,
      0xbec40,
      0xbf7dc,
      0xc0378,
      0xc0f14,
      0xc1ab1,
      0xc264f,
      0xc31ed,
      0xc3d8c,
      0xc492b,
      0xc54cb,
      0xc606b,
      0xc6c0b,
      0xc77ad,
      0xc834e,
      0xc8ef0,
      0xc9a93,
      0xca636,
      0xcb1da,
      0xcbd7e,
      0xcc923,
      0xcd4c8,
      0xce06e,
      0xcec14,
      0xcf7ba,
      0xd0362,
      0xd0f09,
      0xd1ab2,
      0xd265a,
      0xd3204,
      0xd3dad,
      0xd4957,
      0xd5502,
      0xd60ad,
      0xd6c59,
      0xd7805,
      0xd83b2,
      0xd8f5f,
      0xd9b0d,
      0xda6bc,
      0xdb26a,
      0xdbe1a,
      0xdc9c9,
      0xdd57a,
      0xde12a,
      0xdecdc,
      0xdf88e,
      0xe0440,
      0xe0ff3,
      0xe1ba6,
      0xe275a,
      0xe330e,
      0xe3ec3,
      0xe4a79,
      0xe562e,
      0xe61e5,
      0xe6d9c,
      0xe7953,
      0xe850b,
      0xe90c3,
      0xe9c7c,
      0xea836,
      0xeb3f0,
      0xebfaa,
      0xecb65,
      0xed721,
      0xee2dd,
      0xeee99,
      0xefa56,
      0xf0613,
      0xf11d1,
      0xf1d90,
      0xf294f,
      0xf350f,
      0xf40cf,
      0xf4c8f,
      0xf5850,
      0xf6412,
      0xf6fd4,
      0xf7b96,
      0xf875a,
      0xf931d,
      0xf9ee1,
      0xfaaa6,
      0xfb66b,
      0xfc231,
      0xfcdf7,
      0xfd9bd,
      0xfe585,
      0xff14c,
      0xffd15,
      0x1008dd,
      0x1014a6,
      0x102070,
      0x102c3a,
      0x103805,
      0x1043d0,
      0x104f9c,
      0x105b68,
      0x106735,
      0x107303,
      0x107ed0,
      0x108a9f,
      0x10966e,
      0x10a23d,
      0x10ae0d,
      0x10b9dd,
      0x10c5ae,
      0x10d17f,
      0x10dd51,
      0x10e924,
      0x10f4f7,
      0x1100ca,
      0x110c9e,
      0x111873,
      0x112448,
      0x11301d,
      0x113bf3,
      0x1147ca,
      0x1153a1,
      0x115f78,
      0x116b50,
      0x117729,
      0x118302,
      0x118edb,
      0x119ab6,
      0x11a690,
      0x11b26b,
      0x11be47,
      0x11ca23,
      0x11d600,
      0x11e1dd,
      0x11edbb,
      0x11f999,
      0x120578,
      0x121157,
      0x121d37,
      0x122917,
      0x1234f8,
      0x1240d9,
      0x124cbb,
      0x12589d,
      0x126480,
      0x127063,
      0x127c47,
      0x12882c,
      0x129411,
      0x129ff6,
      0x12abdc,
      0x12b7c2,
      0x12c3a9,
      0x12cf91,
      0x12db79,
      0x12e761,
      0x12f34b,
      0x12ff34,
      0x130b1e,
      0x131709,
      0x1322f4,
      0x132edf,
      0x133acc,
      0x1346b8,
      0x1352a5,
      0x135e93,
      0x136a81,
      0x137670,
      0x13825f,
      0x138e4f,
      0x139a3f,
      0x13a630,
      0x13b221,
      0x13be13,
      0x13ca06,
      0x13d5f8,
      0x13e1ec,
      0x13ede0,
      0x13f9d4,
      0x1405c9,
      0x1411be,
      0x141db4,
      0x1429ab,
      0x1435a2,
      0x144199,
      0x144d91,
      0x14598a,
      0x146583,
      0x14717d,
      0x147d77,
      0x148971,
      0x14956d,
      0x14a168,
      0x14ad64,
      0x14b961,
      0x14c55e,
      0x14d15c,
      0x14dd5a,
      0x14e959,
      0x14f559,
      0x150158,
      0x150d59,
      0x15195a,
      0x15255b,
      0x15315d,
      0x153d5f,
      0x154962,
      0x155566,
      0x15616a,
      0x156d6e,
      0x157973,
      0x158579,
      0x15917f,
      0x159d85,
      0x15a98d,
      0x15b594,
      0x15c19c,
      0x15cda5,
      0x15d9ae,
      0x15e5b8,
      0x15f1c2,
      0x15fdcd,
      0x1609d8,
      0x1615e4,
      0x1621f0,
      0x162dfd,
      0x163a0b,
      0x164618,
      0x165227,
      0x165e36,
      0x166a45,
      0x167655,
      0x168266,
      0x168e77,
      0x169a88,
      0x16a69a,
      0x16b2ad,
      0x16bec0,
      0x16cad4,
      0x16d6e8,
      0x16e2fd,
      0x16ef12,
      0x16fb28,
      0x17073e,
      0x171355,
      0x171f6c,
      0x172b84,
      0x17379c,
      0x1743b5,
      0x174fce,
      0x175be8,
      0x176803,
      0x17741e,
      0x178039,
      0x178c55,
      0x179872,
      0x17a48f,
      0x17b0ad,
      0x17bccb,
      0x17c8e9,
      0x17d508,
      0x17e128,
      0x17ed48,
      0x17f969,
      0x18058a,
      0x1811ac,
      0x181dcf,
      0x1829f1,
      0x183615,
      0x184239,
      0x184e5d,
      0x185a82,
      0x1866a8,
      0x1872ce,
      0x187ef4,
      0x188b1b,
      0x189743,
      0x18a36b,
      0x18af94,
      0x18bbbd,
      0x18c7e6,
      0x18d411,
      0x18e03b,
      0x18ec67,
      0x18f892,
      0x1904bf,
      0x1910ec,
      0x191d19,
      0x192947,
      0x193575,
      0x1941a4,
      0x194dd4,
      0x195a04,
      0x196634,
      0x197266,
      0x197e97,
      0x198ac9,
      0x1996fc,
      0x19a32f,
      0x19af63,
      0x19bb97,
      0x19c7cc,
      0x19d401,
      0x19e037,
      0x19ec6d,
      0x19f8a4,
      0x1a04dc,
      0x1a1114,
      0x1a1d4c,
      0x1a2985,
      0x1a35bf,
      0x1a41f9,
      0x1a4e33,
      0x1a5a6f,
      0x1a66aa,
      0x1a72e6,
      0x1a7f23,
      0x1a8b60,
      0x1a979e,
      0x1aa3dc,
      0x1ab01b,
      0x1abc5b,
      0x1ac89b,
      0x1ad4db,
      0x1ae11c,
      0x1aed5d,
      0x1af9a0,
      0x1b05e2,
      0x1b1225,
      0x1b1e69,
      0x1b2aad,
      0x1b36f2,
      0x1b4337,
      0x1b4f7d,
      0x1b5bc3,
      0x1b680a,
      0x1b7451,
      0x1b8099,
      0x1b8ce1,
      0x1b992a,
      0x1ba574,
      0x1bb1be,
      0x1bbe08,
      0x1bca53,
      0x1bd69f,
      0x1be2eb,
      0x1bef38,
      0x1bfb85,
      0x1c07d3,
      0x1c1421,
      0x1c2070,
      0x1c2cbf,
      0x1c390f,
      0x1c455f,
      0x1c51b0,
      0x1c5e02,
      0x1c6a54,
      0x1c76a6,
      0x1c82f9,
      0x1c8f4d,
      0x1c9ba1,
      0x1ca7f6,
      0x1cb44b,
      0x1cc0a1,
      0x1cccf7,
      0x1cd94e,
      0x1ce5a5,
      0x1cf1fd,
      0x1cfe55,
      0x1d0aae,
      0x1d1708,
      0x1d2362,
      0x1d2fbc,
      0x1d3c17,
      0x1d4873,
      0x1d54cf,
      0x1d612c,
      0x1d6d89,
      0x1d79e7,
      0x1d8645,
      0x1d92a4,
      0x1d9f03,
      0x1dab63,
      0x1db7c4,
      0x1dc425,
      0x1dd086,
      0x1ddce8,
      0x1de94b,
      0x1df5ae,
      0x1e0212,
      0x1e0e76,
      0x1e1adb,
      0x1e2740,
      0x1e33a6,
      0x1e400c,
      0x1e4c73,
      0x1e58da,
      0x1e6542,
      0x1e71ab,
      0x1e7e14,
      0x1e8a7d,
      0x1e96e8,
      0x1ea352,
      0x1eafbd,
      0x1ebc29,
      0x1ec895,
      0x1ed502,
      0x1ee170,
      0x1eeddd,
      0x1efa4c,
      0x1f06bb,
      0x1f132a,
      0x1f1f9a,
      0x1f2c0b,
      0x1f387c,
      0x1f44ee,
      0x1f5160,
      0x1f5dd3,
      0x1f6a46,
      0x1f76ba,
      0x1f832e,
      0x1f8fa3,
      0x1f9c18,
      0x1fa88e,
      0x1fb505,
      0x1fc17c,
      0x1fcdf3,
      0x1fda6b,
      0x1fe6e4,
      0x1ff35d,
      0x1fffd7,
      0x200c51,
      0x2018cc,
      0x202548,
      0x2031c3,
      0x203e40,
      0x204abd,
      0x20573a,
      0x2063b9,
      0x207037,
      0x207cb6,
      0x208936,
      0x2095b6,
      0x20a237,
      0x20aeb8,
      0x20bb3a,
      0x20c7bd,
      0x20d440,
      0x20e0c3,
      0x20ed47,
      0x20f9cc,
      0x210651,
      0x2112d6,
      0x211f5d,
      0x212be3,
      0x21386b,
      0x2144f2,
      0x21517b,
      0x215e04,
      0x216a8d,
      0x217717,
      0x2183a2,
      0x21902d,
      0x219cb8,
      0x21a945,
      0x21b5d1,
      0x21c25f,
      0x21ceec,
      0x21db7b,
      0x21e80a,
      0x21f499,
      0x220129,
      0x220dba,
      0x221a4b,
      0x2226dc,
      0x22336e,
      0x224001,
      0x224c94,
      0x225928,
      0x2265bd,
      0x227251,
      0x227ee7,
      0x228b7d,
      0x229813,
      0x22a4aa,
      0x22b142,
      0x22bdda,
      0x22ca73,
      0x22d70c,
      0x22e3a6,
      0x22f040,
      0x22fcdb,
      0x230977,
      0x231613,
      0x2322af,
      0x232f4c,
      0x233bea,
      0x234888,
      0x235527,
      0x2361c6,
      0x236e66,
      0x237b06,
      0x2387a7,
      0x239448,
      0x23a0ea,
      0x23ad8d,
      0x23ba30,
      0x23c6d4,
      0x23d378,
      0x23e01d,
      0x23ecc2,
      0x23f968,
      0x24060e,
      0x2412b5,
      0x241f5c,
      0x242c04,
      0x2438ad,
      0x244556,
      0x245200,
      0x245eaa,
      0x246b55,
      0x247800,
      0x2484ac,
      0x249158,
      0x249e05,
      0x24aab3,
      0x24b761,
      0x24c40f,
      0x24d0bf,
      0x24dd6e,
      0x24ea1e,
      0x24f6cf,
      0x250381,
      0x251033,
      0x251ce5,
      0x252998,
      0x25364c,
      0x254300,
      0x254fb4,
      0x255c69,
      0x25691f,
      0x2575d6,
      0x25828c,
      0x258f44,
      0x259bfc,
      0x25a8b4,
      0x25b56d,
      0x25c227,
      0x25cee1,
      0x25db9c,
      0x25e857,
      0x25f513,
      0x2601cf,
      0x260e8c,
      0x261b4a,
      0x262808,
      0x2634c6,
      0x264185,
      0x264e45,
      0x265b05,
      0x2667c6,
      0x267487,
      0x268149,
      0x268e0c,
      0x269acf,
      0x26a792,
      0x26b456,
      0x26c11b,
      0x26cde0,
      0x26daa6,
      0x26e76c,
      0x26f433,
      0x2700fb,
      0x270dc3,
      0x271a8b,
      0x272754,
      0x27341e,
      0x2740e8,
      0x274db3,
      0x275a7e,
      0x27674a,
      0x277416,
      0x2780e3,
      0x278db1,
      0x279a7f,
      0x27a74d,
      0x27b41d,
      0x27c0ec,
      0x27cdbd,
      0x27da8e,
      0x27e75f,
      0x27f431,
      0x280103,
      0x280dd6,
      0x281aaa,
      0x28277e,
      0x283453,
      0x284128,
      0x284dfe,
      0x285ad5,
      0x2867ab,
      0x287483,
      0x28815b,
      0x288e34,
      0x289b0d,
      0x28a7e7,
      0x28b4c1,
      0x28c19c,
      0x28ce77,
      0x28db53,
      0x28e830,
      0x28f50d,
      0x2901ea,
      0x290ec9,
      0x291ba7,
      0x292887,
      0x293567,
      0x294247,
      0x294f28,
      0x295c09,
      0x2968ec,
      0x2975ce,
      0x2982b1,
      0x298f95,
      0x299c7a,
      0x29a95e,
      0x29b644,
      0x29c32a,
      0x29d011,
      0x29dcf8,
      0x29e9df,
      0x29f6c8,
      0x2a03b0,
      0x2a109a,
      0x2a1d84,
      0x2a2a6e,
      0x2a3759,
      0x2a4445,
      0x2a5131,
      0x2a5e1e,
      0x2a6b0b,
      0x2a77f9,
      0x2a84e7,
      0x2a91d6,
      0x2a9ec6,
      0x2aabb6,
      0x2ab8a6,
      0x2ac598,
      0x2ad289,
      0x2adf7c,
      0x2aec6f,
      0x2af962,
      0x2b0656,
      0x2b134b,
      0x2b2040,
      0x2b2d35,
      0x2b3a2c,
      0x2b4723,
      0x2b541a,
      0x2b6112,
      0x2b6e0a,
      0x2b7b03,
      0x2b87fd,
      0x2b94f7,
      0x2ba1f2,
      0x2baeed,
      0x2bbbe9,
      0x2bc8e6,
      0x2bd5e3,
      0x2be2e0,
      0x2befde,
      0x2bfcdd,
      0x2c09dc,
      0x2c16dc,
      0x2c23dc,
      0x2c30dd,
      0x2c3ddf,
      0x2c4ae1,
      0x2c57e4,
      0x2c64e7,
      0x2c71eb,
      0x2c7eef,
      0x2c8bf4,
      0x2c98f9,
      0x2ca5ff,
      0x2cb306,
      0x2cc00d,
      0x2ccd15,
      0x2cda1d,
      0x2ce726,
      0x2cf42f,
      0x2d0139,
      0x2d0e44,
      0x2d1b4f,
      0x2d285a,
      0x2d3567,
      0x2d4273,
      0x2d4f81,
      0x2d5c8f,
      0x2d699d,
      0x2d76ac,
      0x2d83bc,
      0x2d90cc,
      0x2d9ddd,
      0x2daaee,
      0x2db800,
      0x2dc512,
      0x2dd226,
      0x2ddf39,
      0x2dec4d,
      0x2df962,
      0x2e0677,
      0x2e138d,
      0x2e20a4,
      0x2e2dba,
      0x2e3ad2,
      0x2e47ea,
      0x2e5503,
      0x2e621c,
      0x2e6f36,
      0x2e7c50,
      0x2e896b,
      0x2e9687,
      0x2ea3a3,
      0x2eb0c0,
      0x2ebddd,
      0x2ecafb,
      0x2ed819,
      0x2ee538,
      0x2ef257,
      0x2eff77,
      0x2f0c98,
      0x2f19b9,
      0x2f26db,
      0x2f33fd,
      0x2f4120,
      0x2f4e44,
      0x2f5b68,
      0x2f688c,
      0x2f75b2,
      0x2f82d7,
      0x2f8ffe,
      0x2f9d25,
      0x2faa4c,
      0x2fb774,
      0x2fc49d,
      0x2fd1c6,
      0x2fdef0,
      0x2fec1a,
      0x2ff945,
      0x300670,
      0x30139c,
      0x3020c9,
      0x302df6,
      0x303b24,
      0x304852,
      0x305581,
      0x3062b1,
    };
    setRom< HWOffsetFix<22,-24,UNSIGNED> > (data, id356sta_rom_store, 22, 1024); 
  }
  { // Node ID: 353 (NodeROM)
    uint64_t data[] = {
      0x0,
      0x306fe1,
      0x6a09e6,
      0xae89fa,
    };
    setRom< HWOffsetFix<24,-24,UNSIGNED> > (data, id353sta_rom_store, 24, 4); 
  }
  { // Node ID: 225 (NodeConstantRawBits)
    id225out_value = (c_hw_fix_14_n14_uns_bits);
  }
  { // Node ID: 228 (NodeConstantRawBits)
    id228out_value = (c_hw_fix_14_n26_uns_bits);
  }
  { // Node ID: 234 (NodeConstantRawBits)
    id234out_value = (c_hw_fix_51_n50_uns_bits);
  }
  { // Node ID: 238 (NodeConstantRawBits)
    id238out_value = (c_hw_fix_27_n26_uns_bits);
  }
  { // Node ID: 244 (NodeConstantRawBits)
    id244out_value = (c_hw_fix_52_n50_uns_bits);
  }
  { // Node ID: 248 (NodeConstantRawBits)
    id248out_value = (c_hw_fix_28_n26_uns_bits);
  }
  { // Node ID: 252 (NodeConstantRawBits)
    id252out_value = (c_hw_fix_24_n23_uns_bits);
  }
  { // Node ID: 676 (NodeConstantRawBits)
    id676out_value = (c_hw_fix_24_n23_uns_bits_1);
  }
  { // Node ID: 287 (NodeConstantRawBits)
    id287out_value = (c_hw_fix_11_0_sgn_bits_1);
  }
  { // Node ID: 675 (NodeConstantRawBits)
    id675out_value = (c_hw_flt_8_24_bits_4);
  }
  { // Node ID: 674 (NodeConstantRawBits)
    id674out_value = (c_hw_flt_8_24_bits_4);
  }
  { // Node ID: 673 (NodeConstantRawBits)
    id673out_value = (c_hw_fix_11_0_sgn_bits_3);
  }
  { // Node ID: 323 (NodeConstantRawBits)
    id323out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 310 (NodeConstantRawBits)
    id310out_value = (c_hw_fix_8_0_sgn_bits);
  }
  { // Node ID: 256 (NodeConstantRawBits)
    id256out_value = (c_hw_fix_24_n23_uns_bits_2);
  }
  { // Node ID: 260 (NodeConstantRawBits)
    id260out_value = (c_hw_fix_23_n23_uns_bits);
  }
  { // Node ID: 332 (NodeConstantRawBits)
    id332out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 333 (NodeConstantRawBits)
    id333out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 335 (NodeConstantRawBits)
    id335out_value = (c_hw_fix_23_0_uns_bits);
  }
  { // Node ID: 415 (NodeConstantRawBits)
    id415out_value = (c_hw_flt_8_24_bits_4);
  }
  { // Node ID: 672 (NodeConstantRawBits)
    id672out_value = (c_hw_flt_8_24_bits_6);
  }
  { // Node ID: 671 (NodeConstantRawBits)
    id671out_value = (c_hw_flt_8_24_bits_2);
  }
  { // Node ID: 2 (NodeConstantRawBits)
    id2out_value = (c_hw_flt_8_24_bits_7);
  }
  { // Node ID: 22 (NodeConstantRawBits)
    id22out_value = (c_hw_flt_8_24_bits_8);
  }
  { // Node ID: 4 (NodeConstantRawBits)
    id4out_value = (c_hw_flt_8_24_bits_9);
  }
  { // Node ID: 41 (NodeConstantRawBits)
    id41out_value = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 631 (NodeOutputMappedReg)
    registerMappedRegister("all_numeric_exceptions", Data(32), false);
  }
  { // Node ID: 611 (NodeConstantRawBits)
    id611out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 612 (NodeOutput)
    m_internal_watch_carriedu_output = registerOutput("internal_watch_carriedu_output",2 );
  }
  { // Node ID: 670 (NodeConstantRawBits)
    id670out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 392 (NodeInputMappedReg)
    registerMappedRegister("io_u_out_force_disabled", Data(1));
  }
  { // Node ID: 395 (NodeOutput)
    m_u_out = registerOutput("u_out",0 );
  }
  { // Node ID: 669 (NodeConstantRawBits)
    id669out_value = (c_hw_fix_11_0_uns_bits_1);
  }
  { // Node ID: 399 (NodeInputMappedReg)
    registerMappedRegister("io_v_out_force_disabled", Data(1));
  }
  { // Node ID: 402 (NodeOutput)
    m_v_out = registerOutput("v_out",1 );
  }
  { // Node ID: 407 (NodeConstantRawBits)
    id407out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 668 (NodeConstantRawBits)
    id668out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 404 (NodeConstantRawBits)
    id404out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 408 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 667 (NodeConstantRawBits)
    id667out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 410 (NodeConstantRawBits)
    id410out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 413 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void KarmaKernel::resetComputation() {
  resetComputationAfterFlush();
}

void KarmaKernel::resetComputationAfterFlush() {
  { // Node ID: 17 (NodeCounter)

    (id17st_count) = (c_hw_fix_12_0_uns_bits);
  }
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 16 (NodeCounter)
    const HWOffsetFix<32,0,UNSIGNED> &id16in_max = id15out_num_iteration;

    (id16st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 653 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id653out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 665 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id665out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 639 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id639out_output[i] = (c_hw_flt_8_24_undef);
    }
  }
  { // Node ID: 652 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id652out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 644 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id644out_output[i] = (c_hw_fix_10_0_sgn_undef);
    }
  }
  { // Node ID: 645 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id645out_output[i] = (c_hw_fix_14_n26_uns_undef);
    }
  }
  { // Node ID: 646 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id646out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 647 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id647out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 648 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id648out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 649 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id649out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 614 (NodeExceptionMask)

    (id614st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 657 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id657out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 615 (NodeExceptionMask)

    (id615st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 658 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id658out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 616 (NodeExceptionMask)

    (id616st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 659 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id659out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 618 (NodeExceptionMask)

    (id618st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 619 (NodeExceptionMask)

    (id619st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 620 (NodeExceptionMask)

    (id620st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 621 (NodeExceptionMask)

    (id621st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 622 (NodeExceptionMask)

    (id622st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 623 (NodeExceptionMask)

    (id623st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 626 (NodeExceptionMask)

    (id626st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 617 (NodeExceptionMask)

    (id617st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 625 (NodeExceptionMask)

    (id625st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 627 (NodeExceptionMask)

    (id627st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 628 (NodeExceptionMask)

    (id628st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 629 (NodeExceptionMask)

    (id629st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 624 (NodeExceptionMask)

    (id624st_reg) = (c_hw_fix_1_0_uns_bits_1);
  }
  { // Node ID: 613 (NodeExceptionMask)

    (id613st_reg) = (c_hw_fix_4_0_uns_bits);
  }
  { // Node ID: 660 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id660out_output[i] = (c_hw_fix_4_0_uns_undef);
    }
  }
  { // Node ID: 661 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id661out_output[i] = (c_hw_fix_11_0_uns_undef);
    }
  }
  { // Node ID: 392 (NodeInputMappedReg)
    id392out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 662 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id662out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 399 (NodeInputMappedReg)
    id399out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 664 (NodeFIFO)

    for(int i=0; i<11; i++)
    {
      id664out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 405 (NodeCounter)

    (id405st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 411 (NodeCounter)

    (id411st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 413 (NodeInputMappedReg)
    id413out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaKernel::updateState() {
  { // Node ID: 15 (NodeInputMappedReg)
    id15out_num_iteration = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("num_iteration");
  }
  { // Node ID: 18 (NodeInputMappedReg)
    id18out_dt = getMappedRegValue<HWFloat<11,53> >("dt");
  }
  { // Node ID: 392 (NodeInputMappedReg)
    id392out_io_u_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_u_out_force_disabled");
  }
  { // Node ID: 399 (NodeInputMappedReg)
    id399out_io_v_out_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_v_out_force_disabled");
  }
  { // Node ID: 413 (NodeInputMappedReg)
    id413out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void KarmaKernel::preExecute() {
}

void KarmaKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "KarmaKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int KarmaKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
