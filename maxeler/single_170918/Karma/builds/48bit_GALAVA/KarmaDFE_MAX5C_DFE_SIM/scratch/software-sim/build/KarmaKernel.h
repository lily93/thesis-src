#ifndef KARMAKERNEL_H_
#define KARMAKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class KarmaKernel : public KernelManagerBlockSync {
public:
  KarmaKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_internal_watch_carriedu_output;
  t_port_number m_u_out;
  t_port_number m_v_out;
  HWOffsetFix<1,0,UNSIGNED> id792out_value;

  HWOffsetFix<1,0,UNSIGNED> id14out_value;

  HWOffsetFix<11,0,UNSIGNED> id826out_value;

  HWOffsetFix<11,0,UNSIGNED> id17out_count;
  HWOffsetFix<1,0,UNSIGNED> id17out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id17st_count;

  HWOffsetFix<32,0,UNSIGNED> id15out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id16out_count;
  HWOffsetFix<1,0,UNSIGNED> id16out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id16st_count;

  HWOffsetFix<32,0,UNSIGNED> id844out_value;

  HWOffsetFix<1,0,UNSIGNED> id504out_result[2];

  HWFloat<8,40> id813out_output[10];

  HWFloat<8,40> id825out_output[2];

  HWFloat<11,53> id18out_dt;

  HWFloat<8,40> id19out_o[4];

  HWFloat<8,40> id6out_value;

  HWOffsetFix<32,0,UNSIGNED> id843out_value;

  HWOffsetFix<1,0,UNSIGNED> id505out_result[2];

  HWFloat<8,40> id842out_value;

  HWFloat<8,40> id5out_value;

  HWFloat<8,40> id3out_value;

  HWFloat<8,40> id26out_value;

  HWFloat<8,40> id27out_result[2];

  HWFloat<8,40> id799out_output[11];

  HWFloat<8,40> id841out_value;

  HWFloat<8,40> id840out_value;

  HWFloat<8,40> id839out_value;

  HWRawBits<8> id429out_value;

  HWRawBits<39> id838out_value;

  HWOffsetFix<1,0,UNSIGNED> id812out_output[9];

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id46out_value;

  HWOffsetFix<52,-51,UNSIGNED> id106out_value;

  HWOffsetFix<105,-94,TWOSCOMPLEMENT> id108out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id220out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id804out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id837out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id351out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id367out_value;

  HWOffsetFix<29,-40,UNSIGNED> id444out_dout[3];

  HWOffsetFix<29,-40,UNSIGNED> id444sta_rom_store[1024];

  HWOffsetFix<39,-40,UNSIGNED> id441out_dout[3];

  HWOffsetFix<39,-40,UNSIGNED> id441sta_rom_store[1024];

  HWOffsetFix<40,-40,UNSIGNED> id438out_dout[3];

  HWOffsetFix<40,-40,UNSIGNED> id438sta_rom_store[2];

  HWOffsetFix<21,-21,UNSIGNED> id294out_value;

  HWOffsetFix<22,-43,UNSIGNED> id805out_output[3];

  HWOffsetFix<22,-43,UNSIGNED> id297out_value;

  HWOffsetFix<64,-63,UNSIGNED> id303out_value;

  HWOffsetFix<44,-43,UNSIGNED> id307out_value;

  HWOffsetFix<64,-64,UNSIGNED> id312out_value;

  HWOffsetFix<64,-62,UNSIGNED> id316out_value;

  HWOffsetFix<45,-43,UNSIGNED> id320out_value;

  HWOffsetFix<64,-73,UNSIGNED> id325out_value;

  HWOffsetFix<64,-61,UNSIGNED> id329out_value;

  HWOffsetFix<46,-43,UNSIGNED> id333out_value;

  HWOffsetFix<40,-39,UNSIGNED> id337out_value;

  HWOffsetFix<40,-39,UNSIGNED> id836out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id372out_value;

  HWFloat<8,40> id835out_value;

  HWOffsetFix<1,0,UNSIGNED> id806out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id807out_output[3];

  HWFloat<8,40> id834out_value;

  HWOffsetFix<1,0,UNSIGNED> id808out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id809out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id833out_value;

  HWOffsetFix<1,0,UNSIGNED> id408out_value;

  HWOffsetFix<8,0,TWOSCOMPLEMENT> id395out_value;

  HWOffsetFix<40,-39,UNSIGNED> id341out_value;

  HWOffsetFix<39,-39,UNSIGNED> id345out_value;

  HWOffsetFix<1,0,UNSIGNED> id417out_value;

  HWOffsetFix<8,0,UNSIGNED> id418out_value;

  HWOffsetFix<39,0,UNSIGNED> id420out_value;

  HWFloat<8,40> id503out_value;

  HWFloat<8,40> id832out_value;

  HWFloat<8,40> id831out_value;

  HWFloat<8,40> id766out_floatOut[2];

  HWFloat<8,40> id2out_value;

  HWFloat<8,40> id22out_value;

  HWFloat<8,40> id23out_result[2];

  HWFloat<8,40> id4out_value;

  HWFloat<8,40> id765out_floatOut[2];
  HWOffsetFix<4,0,UNSIGNED> id765out_exception[2];

  HWOffsetFix<1,0,UNSIGNED> id41out_value;

  HWOffsetFix<53,-43,TWOSCOMPLEMENT> id43out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id43out_o_doubt[7];
  HWOffsetFix<4,0,UNSIGNED> id43out_exception[7];

  HWOffsetFix<4,0,UNSIGNED> id770out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id770st_reg;

  HWOffsetFix<4,0,UNSIGNED> id817out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id771out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id771st_reg;

  HWOffsetFix<1,0,UNSIGNED> id818out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id772out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id772st_reg;

  HWOffsetFix<1,0,UNSIGNED> id819out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id774out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id774st_reg;

  HWOffsetFix<1,0,UNSIGNED> id775out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id775st_reg;

  HWOffsetFix<1,0,UNSIGNED> id776out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id776st_reg;

  HWOffsetFix<1,0,UNSIGNED> id777out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id777st_reg;

  HWOffsetFix<1,0,UNSIGNED> id778out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id778st_reg;

  HWOffsetFix<1,0,UNSIGNED> id779out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id779st_reg;

  HWOffsetFix<1,0,UNSIGNED> id780out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id780st_reg;

  HWOffsetFix<1,0,UNSIGNED> id781out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id781st_reg;

  HWOffsetFix<1,0,UNSIGNED> id782out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id782st_reg;

  HWOffsetFix<1,0,UNSIGNED> id783out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id783st_reg;

  HWOffsetFix<1,0,UNSIGNED> id786out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id786st_reg;

  HWOffsetFix<1,0,UNSIGNED> id773out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id773st_reg;

  HWOffsetFix<1,0,UNSIGNED> id785out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id785st_reg;

  HWOffsetFix<4,0,UNSIGNED> id787out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id787st_reg;

  HWOffsetFix<4,0,UNSIGNED> id788out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id788st_reg;

  HWOffsetFix<4,0,UNSIGNED> id789out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id789st_reg;

  HWOffsetFix<1,0,UNSIGNED> id784out_exceptionOccurred[2];

  HWOffsetFix<1,0,UNSIGNED> id784st_reg;

  HWOffsetFix<4,0,UNSIGNED> id769out_exceptionOccurred[2];

  HWOffsetFix<4,0,UNSIGNED> id769st_reg;

  HWOffsetFix<4,0,UNSIGNED> id820out_output[9];

  HWRawBits<36> id790out_all_exceptions;

  HWOffsetFix<1,0,UNSIGNED> id767out_value;

  HWOffsetFix<11,0,UNSIGNED> id821out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id830out_value;

  HWOffsetFix<11,0,UNSIGNED> id478out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id761out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id480out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id822out_output[14];

  HWFloat<8,24> id475out_o[4];

  HWOffsetFix<11,0,UNSIGNED> id829out_value;

  HWOffsetFix<11,0,UNSIGNED> id485out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id762out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id487out_io_v_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id824out_output[14];

  HWFloat<8,24> id476out_o[4];

  HWOffsetFix<1,0,UNSIGNED> id495out_value;

  HWOffsetFix<1,0,UNSIGNED> id828out_value;

  HWOffsetFix<49,0,UNSIGNED> id492out_value;

  HWOffsetFix<48,0,UNSIGNED> id493out_count;
  HWOffsetFix<1,0,UNSIGNED> id493out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id493st_count;

  HWOffsetFix<1,0,UNSIGNED> id827out_value;

  HWOffsetFix<49,0,UNSIGNED> id498out_value;

  HWOffsetFix<48,0,UNSIGNED> id499out_count;
  HWOffsetFix<1,0,UNSIGNED> id499out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id499st_count;

  HWOffsetFix<48,0,UNSIGNED> id501out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id763out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,40> c_hw_flt_8_40_undef;
  const HWFloat<8,40> c_hw_flt_8_40_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits_1;
  const HWFloat<8,40> c_hw_flt_8_40_bits_2;
  const HWFloat<8,40> c_hw_flt_8_40_bits_3;
  const HWFloat<8,40> c_hw_flt_8_40_bits_4;
  const HWFloat<8,40> c_hw_flt_8_40_bits_5;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<39> c_hw_bit_39_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<53,-43,TWOSCOMPLEMENT> c_hw_fix_53_n43_sgn_bits;
  const HWOffsetFix<53,-43,TWOSCOMPLEMENT> c_hw_fix_53_n43_sgn_undef;
  const HWOffsetFix<52,-51,UNSIGNED> c_hw_fix_52_n51_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<105,-94,TWOSCOMPLEMENT> c_hw_fix_105_n94_sgn_bits;
  const HWOffsetFix<105,-94,TWOSCOMPLEMENT> c_hw_fix_105_n94_sgn_undef;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<29,-40,UNSIGNED> c_hw_fix_29_n40_uns_undef;
  const HWOffsetFix<39,-40,UNSIGNED> c_hw_fix_39_n40_uns_undef;
  const HWOffsetFix<40,-40,UNSIGNED> c_hw_fix_40_n40_uns_undef;
  const HWOffsetFix<21,-21,UNSIGNED> c_hw_fix_21_n21_uns_bits;
  const HWOffsetFix<22,-43,UNSIGNED> c_hw_fix_22_n43_uns_undef;
  const HWOffsetFix<22,-43,UNSIGNED> c_hw_fix_22_n43_uns_bits;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_bits;
  const HWOffsetFix<64,-63,UNSIGNED> c_hw_fix_64_n63_uns_undef;
  const HWOffsetFix<44,-43,UNSIGNED> c_hw_fix_44_n43_uns_bits;
  const HWOffsetFix<44,-43,UNSIGNED> c_hw_fix_44_n43_uns_undef;
  const HWOffsetFix<64,-64,UNSIGNED> c_hw_fix_64_n64_uns_bits;
  const HWOffsetFix<64,-64,UNSIGNED> c_hw_fix_64_n64_uns_undef;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_bits;
  const HWOffsetFix<64,-62,UNSIGNED> c_hw_fix_64_n62_uns_undef;
  const HWOffsetFix<45,-43,UNSIGNED> c_hw_fix_45_n43_uns_bits;
  const HWOffsetFix<45,-43,UNSIGNED> c_hw_fix_45_n43_uns_undef;
  const HWOffsetFix<64,-73,UNSIGNED> c_hw_fix_64_n73_uns_bits;
  const HWOffsetFix<64,-73,UNSIGNED> c_hw_fix_64_n73_uns_undef;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_bits;
  const HWOffsetFix<64,-61,UNSIGNED> c_hw_fix_64_n61_uns_undef;
  const HWOffsetFix<46,-43,UNSIGNED> c_hw_fix_46_n43_uns_bits;
  const HWOffsetFix<46,-43,UNSIGNED> c_hw_fix_46_n43_uns_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits_1;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_bits;
  const HWOffsetFix<12,0,TWOSCOMPLEMENT> c_hw_fix_12_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_bits;
  const HWOffsetFix<8,0,TWOSCOMPLEMENT> c_hw_fix_8_0_sgn_undef;
  const HWOffsetFix<40,-39,UNSIGNED> c_hw_fix_40_n39_uns_bits_2;
  const HWOffsetFix<39,-39,UNSIGNED> c_hw_fix_39_n39_uns_bits;
  const HWOffsetFix<39,-39,UNSIGNED> c_hw_fix_39_n39_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<39,0,UNSIGNED> c_hw_fix_39_0_uns_bits;
  const HWFloat<8,40> c_hw_flt_8_40_bits_6;
  const HWFloat<8,40> c_hw_flt_8_40_0_5val;
  const HWFloat<8,40> c_hw_flt_8_40_bits_7;
  const HWFloat<8,40> c_hw_flt_8_40_bits_8;
  const HWFloat<8,40> c_hw_flt_8_40_bits_9;
  const HWFloat<8,40> c_hw_flt_8_40_2_0val;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* KARMAKERNEL_H_ */
