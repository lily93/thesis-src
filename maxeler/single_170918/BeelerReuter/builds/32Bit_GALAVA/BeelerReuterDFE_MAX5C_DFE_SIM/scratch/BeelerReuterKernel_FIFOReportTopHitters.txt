Top 10 largest FIFOs
     Costs                                              From                                                To
         3                        BeelerReuterKernel.maxj:78                       BeelerReuterKernel.maxj:104
         3                        BeelerReuterKernel.maxj:76                       BeelerReuterKernel.maxj:103
         3                        BeelerReuterKernel.maxj:80                       BeelerReuterKernel.maxj:105
         3                        BeelerReuterKernel.maxj:75                       BeelerReuterKernel.maxj:102
         3                        BeelerReuterKernel.maxj:79                       BeelerReuterKernel.maxj:106
         3                        BeelerReuterKernel.maxj:77                       BeelerReuterKernel.maxj:107
         2                       BeelerReuterKernel.maxj:118                       BeelerReuterKernel.maxj:118
         2                       BeelerReuterKernel.maxj:114                       BeelerReuterKernel.maxj:114
         2                        BeelerReuterKernel.maxj:85                        BeelerReuterKernel.maxj:85
         2                        BeelerReuterKernel.maxj:87                        BeelerReuterKernel.maxj:87

Top 10 lines with FIFO sinks
     Costs                                              LineFIFO Count
         3                        BeelerReuterKernel.maxj:87         3
         3                       BeelerReuterKernel.maxj:105         1
         3                       BeelerReuterKernel.maxj:104         1
         3                       BeelerReuterKernel.maxj:107         1
         3                       BeelerReuterKernel.maxj:106         1
         3                       BeelerReuterKernel.maxj:103         1
         3                       BeelerReuterKernel.maxj:102         1
         2                        BeelerReuterKernel.maxj:94         2
         2                        BeelerReuterKernel.maxj:84         2
         2                        BeelerReuterKernel.maxj:85         2

Top 10 lines with FIFO sources
     Costs                                              LineFIFO Count
         3                        BeelerReuterKernel.maxj:75         1
         3                        BeelerReuterKernel.maxj:76         1
         3                        BeelerReuterKernel.maxj:77         1
         3                        BeelerReuterKernel.maxj:78         1
         3                        BeelerReuterKernel.maxj:79         1
         3                        BeelerReuterKernel.maxj:80         1
         2                        BeelerReuterKernel.maxj:94         2
         2                        BeelerReuterKernel.maxj:73         2
         2                        BeelerReuterKernel.maxj:84         2
         2                        BeelerReuterKernel.maxj:85         2
