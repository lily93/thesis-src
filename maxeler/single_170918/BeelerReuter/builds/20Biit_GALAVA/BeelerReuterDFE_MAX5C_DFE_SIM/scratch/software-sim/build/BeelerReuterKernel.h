#ifndef BEELERREUTERKERNEL_H_
#define BEELERREUTERKERNEL_H_

// #include "KernelManagerBlockSync.h"


namespace maxcompilersim {

class BeelerReuterKernel : public KernelManagerBlockSync {
public:
  BeelerReuterKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  t_port_number m_u_out;
  t_port_number m_ca_out;
  t_port_number m_x1_out;
  t_port_number m_m_out;
  t_port_number m_f_out;
  t_port_number m_h_out;
  t_port_number m_d_out;
  t_port_number m_j_out;
  HWOffsetFix<1,0,UNSIGNED> id8out_value;

  HWOffsetFix<11,0,UNSIGNED> id3033out_value;

  HWOffsetFix<11,0,UNSIGNED> id11out_count;
  HWOffsetFix<1,0,UNSIGNED> id11out_wrap;

  HWOffsetFix<12,0,UNSIGNED> id11st_count;

  HWOffsetFix<11,0,UNSIGNED> id2712out_output[2];

  HWOffsetFix<11,0,UNSIGNED> id3298out_value;

  HWOffsetFix<11,0,UNSIGNED> id2311out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2419out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2313out_io_u_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3015out_output[10];

  HWOffsetFix<32,0,UNSIGNED> id9out_num_iteration;

  HWOffsetFix<32,0,UNSIGNED> id10out_count;
  HWOffsetFix<1,0,UNSIGNED> id10out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id10st_count;

  HWOffsetFix<32,0,UNSIGNED> id3297out_value;

  HWOffsetFix<1,0,UNSIGNED> id2420out_result[2];

  HWFloat<8,12> id3296out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1814out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2616out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3283out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3280out_value;

  HWOffsetFix<1,0,UNSIGNED> id1824out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1808out_value;

  HWOffsetFix<1,0,UNSIGNED> id1833out_value;

  HWOffsetFix<8,0,UNSIGNED> id1834out_value;

  HWOffsetFix<11,0,UNSIGNED> id1836out_value;

  HWFloat<8,12> id2395out_value;

  HWFloat<8,12> id3279out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1895out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2618out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3275out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3272out_value;

  HWOffsetFix<1,0,UNSIGNED> id1905out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1889out_value;

  HWOffsetFix<1,0,UNSIGNED> id1914out_value;

  HWOffsetFix<8,0,UNSIGNED> id1915out_value;

  HWOffsetFix<11,0,UNSIGNED> id1917out_value;

  HWFloat<8,12> id2396out_value;

  HWFloat<8,12> id3271out_value;

  HWFloat<8,12> id5out_value;

  HWFloat<8,12> id2021out_value;

  HWFloat<8,12> id3270out_value;

  HWFloat<8,12> id3269out_value;

  HWFloat<8,12> id3268out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1977out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2620out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3264out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3261out_value;

  HWOffsetFix<1,0,UNSIGNED> id1987out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1971out_value;

  HWOffsetFix<1,0,UNSIGNED> id1996out_value;

  HWOffsetFix<8,0,UNSIGNED> id1997out_value;

  HWOffsetFix<11,0,UNSIGNED> id1999out_value;

  HWFloat<8,12> id2397out_value;

  HWFloat<8,12> id3260out_value;

  HWFloat<8,12> id2024out_value;

  HWFloat<8,12> id3259out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2160out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2622out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3255out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3252out_value;

  HWOffsetFix<1,0,UNSIGNED> id2170out_value;

  HWOffsetFix<12,-11,UNSIGNED> id2154out_value;

  HWOffsetFix<1,0,UNSIGNED> id2179out_value;

  HWOffsetFix<8,0,UNSIGNED> id2180out_value;

  HWOffsetFix<11,0,UNSIGNED> id2182out_value;

  HWFloat<8,12> id2398out_value;

  HWFloat<8,12> id3251out_value;

  HWFloat<8,12> id3250out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2245out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2624out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3246out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3243out_value;

  HWOffsetFix<1,0,UNSIGNED> id2255out_value;

  HWOffsetFix<12,-11,UNSIGNED> id2239out_value;

  HWOffsetFix<1,0,UNSIGNED> id2264out_value;

  HWOffsetFix<8,0,UNSIGNED> id2265out_value;

  HWOffsetFix<11,0,UNSIGNED> id2267out_value;

  HWFloat<8,12> id2399out_value;

  HWFloat<8,12> id3242out_value;

  HWFloat<8,12> id3181out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id594out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2638out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3178out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3175out_value;

  HWOffsetFix<1,0,UNSIGNED> id604out_value;

  HWOffsetFix<12,-11,UNSIGNED> id588out_value;

  HWOffsetFix<1,0,UNSIGNED> id613out_value;

  HWOffsetFix<8,0,UNSIGNED> id614out_value;

  HWOffsetFix<11,0,UNSIGNED> id616out_value;

  HWFloat<8,12> id2406out_value;

  HWFloat<8,12> id3174out_value;

  HWFloat<8,12> id3173out_value;

  HWFloat<8,12> id3172out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id678out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2640out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3168out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3165out_value;

  HWOffsetFix<1,0,UNSIGNED> id688out_value;

  HWOffsetFix<12,-11,UNSIGNED> id672out_value;

  HWOffsetFix<1,0,UNSIGNED> id697out_value;

  HWOffsetFix<8,0,UNSIGNED> id698out_value;

  HWOffsetFix<11,0,UNSIGNED> id700out_value;

  HWFloat<8,12> id2407out_value;

  HWFloat<8,12> id3164out_value;

  HWFloat<8,12> id3163out_value;

  HWFloat<8,12> id3161out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id764out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2642out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3158out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3155out_value;

  HWOffsetFix<1,0,UNSIGNED> id774out_value;

  HWOffsetFix<12,-11,UNSIGNED> id758out_value;

  HWOffsetFix<1,0,UNSIGNED> id783out_value;

  HWOffsetFix<8,0,UNSIGNED> id784out_value;

  HWOffsetFix<11,0,UNSIGNED> id786out_value;

  HWFloat<8,12> id2408out_value;

  HWFloat<8,12> id3154out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id847out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2644out_value;

  HWOffsetFix<8,-8,UNSIGNED> id832out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3150out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3147out_value;

  HWOffsetFix<1,0,UNSIGNED> id857out_value;

  HWOffsetFix<12,-11,UNSIGNED> id841out_value;

  HWOffsetFix<1,0,UNSIGNED> id866out_value;

  HWOffsetFix<8,0,UNSIGNED> id867out_value;

  HWOffsetFix<11,0,UNSIGNED> id869out_value;

  HWFloat<8,12> id2409out_value;

  HWFloat<8,12> id3146out_value;

  HWFloat<8,12> id3145out_value;

  HWFloat<8,12> id3144out_value;

  HWFloat<8,12> id3143out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id932out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2646out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3139out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3136out_value;

  HWOffsetFix<1,0,UNSIGNED> id942out_value;

  HWOffsetFix<12,-11,UNSIGNED> id926out_value;

  HWOffsetFix<1,0,UNSIGNED> id951out_value;

  HWOffsetFix<8,0,UNSIGNED> id952out_value;

  HWOffsetFix<11,0,UNSIGNED> id954out_value;

  HWFloat<8,12> id2410out_value;

  HWFloat<8,12> id3135out_value;

  HWFloat<8,12> id3134out_value;

  HWFloat<8,12> id3out_value;

  HWFloat<8,12> id2out_value;

  HWFloat<8,12> id3133out_value;

  HWFloat<8,12> id3056out_value;

  HWFloat<8,12> id3055out_value;

  HWFloat<8,12> id3050out_value;

  HWRawBits<8> id1730out_value;

  HWRawBits<11> id3049out_value;

  HWFloat<8,12> id3048out_value;

  HWRawBits<19> id1740out_value;

  HWOffsetFix<1,0,UNSIGNED> id1701out_value;

  HWOffsetFix<9,0,TWOSCOMPLEMENT> id3047out_value;

  HWOffsetFix<9,0,TWOSCOMPLEMENT> id1706out_value;

  HWFloat<8,12> id3045out_value;

  HWFloat<8,12> id3044out_value;

  HWFloat<8,12> id3043out_value;

  HWFloat<8,12> id1753out_value;

  HWFloat<8,12> id16out_u_in;

  HWFloat<8,12> id17out_result[2];

  HWFloat<8,12> id2723out_output[2];

  HWFloat<8,12> id3030out_output[8];

  HWFloat<8,12> id3031out_output[2];

  HWFloat<11,53> id12out_dt;

  HWFloat<8,12> id13out_o[4];

  HWFloat<8,12> id3295out_value;

  HWFloat<8,12> id3294out_value;

  HWRawBits<8> id2097out_value;

  HWRawBits<11> id3293out_value;

  HWOffsetFix<1,0,UNSIGNED> id2722out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2030out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2032out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2032out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id2035out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2713out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2066out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2614out_value;

  HWOffsetFix<12,-12,UNSIGNED> id2106out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id2106sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id2051out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2714out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3292out_value;

  HWFloat<8,12> id3291out_value;

  HWOffsetFix<1,0,UNSIGNED> id2716out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2717out_output[3];

  HWFloat<8,12> id3290out_value;

  HWOffsetFix<1,0,UNSIGNED> id2718out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2719out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3289out_value;

  HWOffsetFix<1,0,UNSIGNED> id2076out_value;

  HWOffsetFix<12,-11,UNSIGNED> id2060out_value;

  HWOffsetFix<1,0,UNSIGNED> id2085out_value;

  HWOffsetFix<8,0,UNSIGNED> id2086out_value;

  HWOffsetFix<11,0,UNSIGNED> id2088out_value;

  HWFloat<8,12> id2394out_value;

  HWFloat<8,12> id3288out_value;

  HWFloat<8,12> id3287out_value;

  HWFloat<8,12> id2691out_floatOut[2];

  HWFloat<8,12> id3286out_value;

  HWFloat<8,12> id3285out_value;

  HWRawBits<8> id1845out_value;

  HWRawBits<11> id3284out_value;

  HWOffsetFix<1,0,UNSIGNED> id2733out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1778out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1780out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1780out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1783out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2724out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id1854out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1854sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1799out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2725out_output[3];

  HWFloat<8,12> id3282out_value;

  HWOffsetFix<1,0,UNSIGNED> id2727out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2728out_output[3];

  HWFloat<8,12> id3281out_value;

  HWOffsetFix<1,0,UNSIGNED> id2729out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2730out_output[3];

  HWFloat<8,12> id3278out_value;

  HWFloat<8,12> id3277out_value;

  HWRawBits<8> id1926out_value;

  HWRawBits<11> id3276out_value;

  HWOffsetFix<1,0,UNSIGNED> id2744out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1859out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1861out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1861out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1864out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2735out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id1935out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1935sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1880out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2736out_output[3];

  HWFloat<8,12> id3274out_value;

  HWOffsetFix<1,0,UNSIGNED> id2738out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2739out_output[3];

  HWFloat<8,12> id3273out_value;

  HWOffsetFix<1,0,UNSIGNED> id2740out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2741out_output[3];

  HWFloat<8,12> id3267out_value;

  HWFloat<8,12> id3266out_value;

  HWRawBits<8> id2008out_value;

  HWRawBits<11> id3265out_value;

  HWOffsetFix<1,0,UNSIGNED> id2756out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1941out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1943out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1943out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1946out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2747out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id2017out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id2017sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1962out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2748out_output[3];

  HWFloat<8,12> id3263out_value;

  HWOffsetFix<1,0,UNSIGNED> id2750out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2751out_output[3];

  HWFloat<8,12> id3262out_value;

  HWOffsetFix<1,0,UNSIGNED> id2752out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2753out_output[3];

  HWFloat<8,12> id3258out_value;

  HWFloat<8,12> id3257out_value;

  HWRawBits<8> id2191out_value;

  HWRawBits<11> id3256out_value;

  HWOffsetFix<1,0,UNSIGNED> id2767out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2124out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2126out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2126out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id2129out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2758out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id2200out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id2200sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id2145out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2759out_output[3];

  HWFloat<8,12> id3254out_value;

  HWOffsetFix<1,0,UNSIGNED> id2761out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2762out_output[3];

  HWFloat<8,12> id3253out_value;

  HWOffsetFix<1,0,UNSIGNED> id2763out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2764out_output[3];

  HWFloat<8,12> id3249out_value;

  HWFloat<8,12> id3248out_value;

  HWRawBits<8> id2276out_value;

  HWRawBits<11> id3247out_value;

  HWOffsetFix<1,0,UNSIGNED> id2778out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id2209out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id2211out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id2211out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id2214out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2769out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id2285out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id2285sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id2230out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2770out_output[3];

  HWFloat<8,12> id3245out_value;

  HWOffsetFix<1,0,UNSIGNED> id2772out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2773out_output[3];

  HWFloat<8,12> id3244out_value;

  HWOffsetFix<1,0,UNSIGNED> id2774out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2775out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3241out_value;

  HWOffsetFix<1,0,UNSIGNED> id2463out_result[2];

  HWFloat<8,12> id24out_x1_in;

  HWFloat<8,12> id25out_result[2];

  HWFloat<8,12> id2800out_output[9];

  HWFloat<8,12> id3240out_value;

  HWFloat<8,12> id3239out_value;

  HWFloat<8,12> id3238out_value;

  HWRawBits<8> id117out_value;

  HWRawBits<11> id3237out_value;

  HWOffsetFix<1,0,UNSIGNED> id2789out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id50out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id52out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id52out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id55out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2780out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id86out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2626out_value;

  HWOffsetFix<12,-12,UNSIGNED> id126out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id126sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id71out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2781out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3236out_value;

  HWFloat<8,12> id3235out_value;

  HWOffsetFix<1,0,UNSIGNED> id2783out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2784out_output[3];

  HWFloat<8,12> id3234out_value;

  HWOffsetFix<1,0,UNSIGNED> id2785out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2786out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3233out_value;

  HWOffsetFix<1,0,UNSIGNED> id96out_value;

  HWOffsetFix<12,-11,UNSIGNED> id80out_value;

  HWOffsetFix<1,0,UNSIGNED> id105out_value;

  HWOffsetFix<8,0,UNSIGNED> id106out_value;

  HWOffsetFix<11,0,UNSIGNED> id108out_value;

  HWFloat<8,12> id2400out_value;

  HWFloat<8,12> id3232out_value;

  HWFloat<8,12> id3231out_value;

  HWFloat<8,12> id3230out_value;

  HWRawBits<8> id200out_value;

  HWRawBits<11> id3229out_value;

  HWOffsetFix<1,0,UNSIGNED> id2799out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id133out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id135out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id135out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id138out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2790out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id169out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2628out_value;

  HWOffsetFix<12,-12,UNSIGNED> id209out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id209sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id154out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2791out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3228out_value;

  HWFloat<8,12> id3227out_value;

  HWOffsetFix<1,0,UNSIGNED> id2793out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2794out_output[3];

  HWFloat<8,12> id3226out_value;

  HWOffsetFix<1,0,UNSIGNED> id2795out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2796out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3225out_value;

  HWOffsetFix<1,0,UNSIGNED> id179out_value;

  HWOffsetFix<12,-11,UNSIGNED> id163out_value;

  HWOffsetFix<1,0,UNSIGNED> id188out_value;

  HWOffsetFix<8,0,UNSIGNED> id189out_value;

  HWOffsetFix<11,0,UNSIGNED> id191out_value;

  HWFloat<8,12> id2401out_value;

  HWFloat<8,12> id3224out_value;

  HWFloat<8,12> id3223out_value;

  HWFloat<8,12> id3222out_value;

  HWFloat<8,12> id3221out_value;

  HWFloat<8,12> id3220out_value;

  HWFloat<8,12> id3219out_value;

  HWRawBits<8> id285out_value;

  HWRawBits<11> id3218out_value;

  HWOffsetFix<1,0,UNSIGNED> id2810out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id218out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id220out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id220out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id223out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2801out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id254out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2630out_value;

  HWOffsetFix<12,-12,UNSIGNED> id294out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id294sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id239out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2802out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3217out_value;

  HWFloat<8,12> id3216out_value;

  HWOffsetFix<1,0,UNSIGNED> id2804out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2805out_output[3];

  HWFloat<8,12> id3215out_value;

  HWOffsetFix<1,0,UNSIGNED> id2806out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2807out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3214out_value;

  HWOffsetFix<1,0,UNSIGNED> id264out_value;

  HWOffsetFix<12,-11,UNSIGNED> id248out_value;

  HWOffsetFix<1,0,UNSIGNED> id273out_value;

  HWOffsetFix<8,0,UNSIGNED> id274out_value;

  HWOffsetFix<11,0,UNSIGNED> id276out_value;

  HWFloat<8,12> id2402out_value;

  HWFloat<8,12> id3213out_value;

  HWFloat<8,12> id3212out_value;

  HWFloat<8,12> id3211out_value;

  HWRawBits<8> id368out_value;

  HWRawBits<11> id3210out_value;

  HWOffsetFix<1,0,UNSIGNED> id2820out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id301out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id303out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id303out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id306out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2811out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id337out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2632out_value;

  HWOffsetFix<12,-12,UNSIGNED> id377out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id377sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id322out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2812out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3209out_value;

  HWFloat<8,12> id3208out_value;

  HWOffsetFix<1,0,UNSIGNED> id2814out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2815out_output[3];

  HWFloat<8,12> id3207out_value;

  HWOffsetFix<1,0,UNSIGNED> id2816out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2817out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3206out_value;

  HWOffsetFix<1,0,UNSIGNED> id347out_value;

  HWOffsetFix<12,-11,UNSIGNED> id331out_value;

  HWOffsetFix<1,0,UNSIGNED> id356out_value;

  HWOffsetFix<8,0,UNSIGNED> id357out_value;

  HWOffsetFix<11,0,UNSIGNED> id359out_value;

  HWFloat<8,12> id2403out_value;

  HWFloat<8,12> id3205out_value;

  HWFloat<8,12> id3204out_value;

  HWFloat<8,12> id2779out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3203out_value;

  HWOffsetFix<1,0,UNSIGNED> id2492out_result[2];

  HWFloat<8,12> id3202out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id425out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2634out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3198out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3195out_value;

  HWOffsetFix<1,0,UNSIGNED> id435out_value;

  HWOffsetFix<12,-11,UNSIGNED> id419out_value;

  HWOffsetFix<1,0,UNSIGNED> id444out_value;

  HWOffsetFix<8,0,UNSIGNED> id445out_value;

  HWOffsetFix<11,0,UNSIGNED> id447out_value;

  HWFloat<8,12> id2404out_value;

  HWFloat<8,12> id3194out_value;

  HWFloat<8,12> id3193out_value;

  HWFloat<8,12> id3192out_value;

  HWFloat<8,12> id3191out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id510out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2636out_value;

  HWOffsetFix<12,-11,UNSIGNED> id3187out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3184out_value;

  HWOffsetFix<1,0,UNSIGNED> id520out_value;

  HWOffsetFix<12,-11,UNSIGNED> id504out_value;

  HWOffsetFix<1,0,UNSIGNED> id529out_value;

  HWOffsetFix<8,0,UNSIGNED> id530out_value;

  HWOffsetFix<11,0,UNSIGNED> id532out_value;

  HWFloat<8,12> id2405out_value;

  HWFloat<8,12> id3183out_value;

  HWFloat<8,12> id2824out_output[2];

  HWFloat<8,12> id28out_m_in;

  HWFloat<8,12> id29out_result[2];

  HWFloat<8,12> id2836out_output[9];

  HWFloat<8,12> id3201out_value;

  HWFloat<8,12> id3200out_value;

  HWRawBits<8> id456out_value;

  HWRawBits<11> id3199out_value;

  HWOffsetFix<1,0,UNSIGNED> id2835out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id389out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id391out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id391out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id394out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2826out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id465out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id465sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id410out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2827out_output[3];

  HWFloat<8,12> id3197out_value;

  HWOffsetFix<1,0,UNSIGNED> id2829out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2830out_output[3];

  HWFloat<8,12> id3196out_value;

  HWOffsetFix<1,0,UNSIGNED> id2831out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2832out_output[3];

  HWFloat<8,12> id3190out_value;

  HWFloat<8,12> id3189out_value;

  HWRawBits<8> id541out_value;

  HWRawBits<11> id3188out_value;

  HWOffsetFix<1,0,UNSIGNED> id2846out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id474out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id476out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id476out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id479out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2837out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id550out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id550sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id495out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2838out_output[3];

  HWFloat<8,12> id3186out_value;

  HWOffsetFix<1,0,UNSIGNED> id2840out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2841out_output[3];

  HWFloat<8,12> id3185out_value;

  HWOffsetFix<1,0,UNSIGNED> id2842out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2843out_output[3];

  HWFloat<8,12> id2692out_floatOut[2];

  HWOffsetFix<32,0,UNSIGNED> id3182out_value;

  HWOffsetFix<1,0,UNSIGNED> id2507out_result[2];

  HWFloat<8,12> id36out_h_in;

  HWFloat<8,12> id37out_result[2];

  HWFloat<8,12> id2861out_output[10];

  HWFloat<8,12> id3180out_value;

  HWFloat<8,12> id2693out_floatOut[2];

  HWRawBits<8> id625out_value;

  HWRawBits<11> id3179out_value;

  HWOffsetFix<1,0,UNSIGNED> id2860out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id558out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id560out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id560out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id563out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2851out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id634out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id634sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id579out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2852out_output[3];

  HWFloat<8,12> id3177out_value;

  HWOffsetFix<1,0,UNSIGNED> id2854out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2855out_output[3];

  HWFloat<8,12> id3176out_value;

  HWOffsetFix<1,0,UNSIGNED> id2856out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2857out_output[3];

  HWFloat<8,12> id3171out_value;

  HWFloat<8,12> id3170out_value;

  HWRawBits<8> id709out_value;

  HWRawBits<11> id3169out_value;

  HWOffsetFix<1,0,UNSIGNED> id2872out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id642out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id644out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id644out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id647out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2863out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id718out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id718sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id663out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2864out_output[3];

  HWFloat<8,12> id3167out_value;

  HWOffsetFix<1,0,UNSIGNED> id2866out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2867out_output[3];

  HWFloat<8,12> id3166out_value;

  HWOffsetFix<1,0,UNSIGNED> id2868out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2869out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3162out_value;

  HWOffsetFix<1,0,UNSIGNED> id2522out_result[2];

  HWFloat<8,12> id44out_j_in;

  HWFloat<8,12> id45out_result[2];

  HWFloat<8,12> id2895out_output[10];

  HWFloat<8,12> id3160out_value;

  HWFloat<8,12> id2694out_floatOut[2];

  HWRawBits<8> id795out_value;

  HWRawBits<11> id3159out_value;

  HWOffsetFix<1,0,UNSIGNED> id2884out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id728out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id730out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id730out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id733out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2875out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id804out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id804sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id749out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2876out_output[3];

  HWFloat<8,12> id3157out_value;

  HWOffsetFix<1,0,UNSIGNED> id2878out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2879out_output[3];

  HWFloat<8,12> id3156out_value;

  HWOffsetFix<1,0,UNSIGNED> id2880out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2881out_output[3];

  HWFloat<8,12> id3153out_value;

  HWFloat<8,12> id3152out_value;

  HWRawBits<8> id878out_value;

  HWRawBits<11> id3151out_value;

  HWOffsetFix<1,0,UNSIGNED> id2894out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id811out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id813out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id813out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id816out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2886out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id887out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id887sta_rom_store[64];

  HWOffsetFix<8,-14,UNSIGNED> id2887out_output[3];

  HWFloat<8,12> id3149out_value;

  HWOffsetFix<1,0,UNSIGNED> id2888out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2889out_output[3];

  HWFloat<8,12> id3148out_value;

  HWOffsetFix<1,0,UNSIGNED> id2890out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2891out_output[3];

  HWFloat<8,12> id3142out_value;

  HWFloat<8,12> id3141out_value;

  HWRawBits<8> id963out_value;

  HWRawBits<11> id3140out_value;

  HWOffsetFix<1,0,UNSIGNED> id2906out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id896out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id898out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id898out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id901out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2897out_output[3];

  HWOffsetFix<12,-12,UNSIGNED> id972out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id972sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id917out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2898out_output[3];

  HWFloat<8,12> id3138out_value;

  HWOffsetFix<1,0,UNSIGNED> id2900out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2901out_output[3];

  HWFloat<8,12> id3137out_value;

  HWOffsetFix<1,0,UNSIGNED> id2902out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2903out_output[3];

  HWOffsetFix<32,0,UNSIGNED> id3132out_value;

  HWOffsetFix<1,0,UNSIGNED> id2544out_result[2];

  HWFloat<8,12> id40out_d_in;

  HWFloat<8,12> id41out_result[2];

  HWFloat<8,12> id2931out_output[9];

  HWFloat<8,12> id3131out_value;

  HWFloat<8,12> id3130out_value;

  HWFloat<8,12> id3129out_value;

  HWRawBits<8> id1049out_value;

  HWRawBits<11> id3128out_value;

  HWOffsetFix<1,0,UNSIGNED> id2920out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id982out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id984out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id984out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id987out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2911out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1018out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2648out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1058out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1058sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1003out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2912out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3127out_value;

  HWFloat<8,12> id3126out_value;

  HWOffsetFix<1,0,UNSIGNED> id2914out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2915out_output[3];

  HWFloat<8,12> id3125out_value;

  HWOffsetFix<1,0,UNSIGNED> id2916out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2917out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3124out_value;

  HWOffsetFix<1,0,UNSIGNED> id1028out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1012out_value;

  HWOffsetFix<1,0,UNSIGNED> id1037out_value;

  HWOffsetFix<8,0,UNSIGNED> id1038out_value;

  HWOffsetFix<11,0,UNSIGNED> id1040out_value;

  HWFloat<8,12> id2411out_value;

  HWFloat<8,12> id3123out_value;

  HWFloat<8,12> id3122out_value;

  HWFloat<8,12> id3121out_value;

  HWRawBits<8> id1132out_value;

  HWRawBits<11> id3120out_value;

  HWOffsetFix<1,0,UNSIGNED> id2930out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1065out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1067out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1067out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1070out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2921out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1101out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2650out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1141out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1141sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1086out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2922out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3119out_value;

  HWFloat<8,12> id3118out_value;

  HWOffsetFix<1,0,UNSIGNED> id2924out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2925out_output[3];

  HWFloat<8,12> id3117out_value;

  HWOffsetFix<1,0,UNSIGNED> id2926out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2927out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3116out_value;

  HWOffsetFix<1,0,UNSIGNED> id1111out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1095out_value;

  HWOffsetFix<1,0,UNSIGNED> id1120out_value;

  HWOffsetFix<8,0,UNSIGNED> id1121out_value;

  HWOffsetFix<11,0,UNSIGNED> id1123out_value;

  HWFloat<8,12> id2412out_value;

  HWFloat<8,12> id3115out_value;

  HWFloat<8,12> id3114out_value;

  HWFloat<8,12> id3113out_value;

  HWFloat<8,12> id3112out_value;

  HWFloat<8,12> id3111out_value;

  HWFloat<8,12> id3110out_value;

  HWRawBits<8> id1217out_value;

  HWRawBits<11> id3109out_value;

  HWOffsetFix<1,0,UNSIGNED> id2941out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1150out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1152out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1152out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1155out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2932out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1186out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2652out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1226out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1226sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1171out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2933out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3108out_value;

  HWFloat<8,12> id3107out_value;

  HWOffsetFix<1,0,UNSIGNED> id2935out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2936out_output[3];

  HWFloat<8,12> id3106out_value;

  HWOffsetFix<1,0,UNSIGNED> id2937out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2938out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3105out_value;

  HWOffsetFix<1,0,UNSIGNED> id1196out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1180out_value;

  HWOffsetFix<1,0,UNSIGNED> id1205out_value;

  HWOffsetFix<8,0,UNSIGNED> id1206out_value;

  HWOffsetFix<11,0,UNSIGNED> id1208out_value;

  HWFloat<8,12> id2413out_value;

  HWFloat<8,12> id3104out_value;

  HWFloat<8,12> id3103out_value;

  HWFloat<8,12> id3102out_value;

  HWRawBits<8> id1300out_value;

  HWRawBits<11> id3101out_value;

  HWOffsetFix<1,0,UNSIGNED> id2951out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1233out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1235out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1235out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1238out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2942out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1269out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2654out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1309out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1309sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1254out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2943out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3100out_value;

  HWFloat<8,12> id3099out_value;

  HWOffsetFix<1,0,UNSIGNED> id2945out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2946out_output[3];

  HWFloat<8,12> id3098out_value;

  HWOffsetFix<1,0,UNSIGNED> id2947out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2948out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3097out_value;

  HWOffsetFix<1,0,UNSIGNED> id1279out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1263out_value;

  HWOffsetFix<1,0,UNSIGNED> id1288out_value;

  HWOffsetFix<8,0,UNSIGNED> id1289out_value;

  HWOffsetFix<11,0,UNSIGNED> id1291out_value;

  HWFloat<8,12> id2414out_value;

  HWFloat<8,12> id3096out_value;

  HWFloat<8,12> id3095out_value;

  HWFloat<8,12> id2910out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3094out_value;

  HWOffsetFix<1,0,UNSIGNED> id2573out_result[2];

  HWFloat<8,12> id32out_f_in;

  HWFloat<8,12> id33out_result[2];

  HWFloat<8,12> id2976out_output[9];

  HWFloat<8,12> id3093out_value;

  HWFloat<8,12> id3092out_value;

  HWFloat<8,12> id3091out_value;

  HWRawBits<8> id1385out_value;

  HWRawBits<11> id3090out_value;

  HWOffsetFix<1,0,UNSIGNED> id2965out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1318out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1320out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1320out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1323out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2956out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1354out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2656out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1394out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1394sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1339out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2957out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3089out_value;

  HWFloat<8,12> id3088out_value;

  HWOffsetFix<1,0,UNSIGNED> id2959out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2960out_output[3];

  HWFloat<8,12> id3087out_value;

  HWOffsetFix<1,0,UNSIGNED> id2961out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2962out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3086out_value;

  HWOffsetFix<1,0,UNSIGNED> id1364out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1348out_value;

  HWOffsetFix<1,0,UNSIGNED> id1373out_value;

  HWOffsetFix<8,0,UNSIGNED> id1374out_value;

  HWOffsetFix<11,0,UNSIGNED> id1376out_value;

  HWFloat<8,12> id2415out_value;

  HWFloat<8,12> id3085out_value;

  HWFloat<8,12> id3084out_value;

  HWFloat<8,12> id3083out_value;

  HWRawBits<8> id1468out_value;

  HWRawBits<11> id3082out_value;

  HWOffsetFix<1,0,UNSIGNED> id2975out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1401out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1403out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1403out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1406out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2966out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1437out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2658out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1477out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1477sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1422out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2967out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3081out_value;

  HWFloat<8,12> id3080out_value;

  HWOffsetFix<1,0,UNSIGNED> id2969out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2970out_output[3];

  HWFloat<8,12> id3079out_value;

  HWOffsetFix<1,0,UNSIGNED> id2971out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2972out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3078out_value;

  HWOffsetFix<1,0,UNSIGNED> id1447out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1431out_value;

  HWOffsetFix<1,0,UNSIGNED> id1456out_value;

  HWOffsetFix<8,0,UNSIGNED> id1457out_value;

  HWOffsetFix<11,0,UNSIGNED> id1459out_value;

  HWFloat<8,12> id2416out_value;

  HWFloat<8,12> id3077out_value;

  HWFloat<8,12> id3076out_value;

  HWFloat<8,12> id3075out_value;

  HWFloat<8,12> id3074out_value;

  HWFloat<8,12> id3073out_value;

  HWFloat<8,12> id3072out_value;

  HWRawBits<8> id1553out_value;

  HWRawBits<11> id3071out_value;

  HWOffsetFix<1,0,UNSIGNED> id2986out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1486out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1488out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1488out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1491out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2977out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1522out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2660out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1562out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1562sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1507out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2978out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3070out_value;

  HWFloat<8,12> id3069out_value;

  HWOffsetFix<1,0,UNSIGNED> id2980out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2981out_output[3];

  HWFloat<8,12> id3068out_value;

  HWOffsetFix<1,0,UNSIGNED> id2982out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2983out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3067out_value;

  HWOffsetFix<1,0,UNSIGNED> id1532out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1516out_value;

  HWOffsetFix<1,0,UNSIGNED> id1541out_value;

  HWOffsetFix<8,0,UNSIGNED> id1542out_value;

  HWOffsetFix<11,0,UNSIGNED> id1544out_value;

  HWFloat<8,12> id2417out_value;

  HWFloat<8,12> id3066out_value;

  HWFloat<8,12> id3065out_value;

  HWFloat<8,12> id3064out_value;

  HWRawBits<8> id1636out_value;

  HWRawBits<11> id3063out_value;

  HWOffsetFix<1,0,UNSIGNED> id2996out_output[9];

  HWOffsetFix<1,0,UNSIGNED> id1569out_value;

  HWOffsetFix<24,-14,TWOSCOMPLEMENT> id1571out_o[7];
  HWOffsetFix<1,0,UNSIGNED> id1571out_o_doubt[7];

  HWOffsetFix<24,-23,UNSIGNED> id1574out_value;

  HWOffsetFix<10,0,TWOSCOMPLEMENT> id2987out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id1605out_value;

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id2662out_value;

  HWOffsetFix<12,-12,UNSIGNED> id1645out_dout[3];

  HWOffsetFix<12,-12,UNSIGNED> id1645sta_rom_store[64];

  HWOffsetFix<8,-8,UNSIGNED> id1590out_value;

  HWOffsetFix<8,-14,UNSIGNED> id2988out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id3062out_value;

  HWFloat<8,12> id3061out_value;

  HWOffsetFix<1,0,UNSIGNED> id2990out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2991out_output[3];

  HWFloat<8,12> id3060out_value;

  HWOffsetFix<1,0,UNSIGNED> id2992out_output[7];

  HWOffsetFix<1,0,UNSIGNED> id2993out_output[3];

  HWOffsetFix<11,0,TWOSCOMPLEMENT> id3059out_value;

  HWOffsetFix<1,0,UNSIGNED> id1615out_value;

  HWOffsetFix<12,-11,UNSIGNED> id1599out_value;

  HWOffsetFix<1,0,UNSIGNED> id1624out_value;

  HWOffsetFix<8,0,UNSIGNED> id1625out_value;

  HWOffsetFix<11,0,UNSIGNED> id1627out_value;

  HWFloat<8,12> id2418out_value;

  HWFloat<8,12> id3058out_value;

  HWFloat<8,12> id3057out_value;

  HWFloat<8,12> id2955out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id3054out_value;

  HWOffsetFix<1,0,UNSIGNED> id2602out_result[2];

  HWFloat<8,12> id3053out_value;

  HWFloat<8,12> id3052out_value;

  HWFloat<8,12> id3051out_value;

  HWFloat<8,12> id20out_ca_in;

  HWFloat<8,12> id21out_result[2];

  HWFloat<8,12> id3008out_output[8];

  HWFloat<8,12> id3032out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id1688out_value;

  HWOffsetFix<1,0,UNSIGNED> id3046out_value;

  HWOffsetFix<1,0,UNSIGNED> id3009out_output[3];

  HWOffsetFix<12,-11,UNSIGNED> id1696out_value;

  HWRawBits<48> id2373out_dout[3];

  HWRawBits<48> id2373sta_rom_store[512];

  HWOffsetFix<2,-2,UNSIGNED> id3010out_output[3];

  HWOffsetFix<11,0,UNSIGNED> id3042out_value;

  HWOffsetFix<11,0,UNSIGNED> id2319out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2606out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2321out_io_ca_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3017out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3041out_value;

  HWOffsetFix<11,0,UNSIGNED> id2327out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2607out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2329out_io_x1_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3019out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3040out_value;

  HWOffsetFix<11,0,UNSIGNED> id2335out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2608out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2337out_io_m_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3021out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3039out_value;

  HWOffsetFix<11,0,UNSIGNED> id2343out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2609out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2345out_io_f_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3023out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3038out_value;

  HWOffsetFix<11,0,UNSIGNED> id2351out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2610out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2353out_io_h_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3025out_output[10];

  HWOffsetFix<11,0,UNSIGNED> id3037out_value;

  HWOffsetFix<11,0,UNSIGNED> id2359out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2611out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2361out_io_d_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3027out_output[9];

  HWOffsetFix<11,0,UNSIGNED> id3036out_value;

  HWOffsetFix<11,0,UNSIGNED> id2367out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2612out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id2369out_io_j_out_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id3029out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id2386out_value;

  HWOffsetFix<1,0,UNSIGNED> id3035out_value;

  HWOffsetFix<49,0,UNSIGNED> id2383out_value;

  HWOffsetFix<48,0,UNSIGNED> id2384out_count;
  HWOffsetFix<1,0,UNSIGNED> id2384out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2384st_count;

  HWOffsetFix<1,0,UNSIGNED> id3034out_value;

  HWOffsetFix<49,0,UNSIGNED> id2389out_value;

  HWOffsetFix<48,0,UNSIGNED> id2390out_count;
  HWOffsetFix<1,0,UNSIGNED> id2390out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id2390st_count;

  HWOffsetFix<48,0,UNSIGNED> id2392out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id2613out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits;
  const HWOffsetFix<12,0,UNSIGNED> c_hw_fix_12_0_uns_bits_1;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_undef;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_1;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWFloat<8,12> c_hw_flt_8_12_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_1;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_2;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_undef;
  const HWOffsetFix<11,0,TWOSCOMPLEMENT> c_hw_fix_11_0_sgn_bits_3;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_bits_1;
  const HWOffsetFix<12,-11,UNSIGNED> c_hw_fix_12_n11_uns_undef;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<11,0,UNSIGNED> c_hw_fix_11_0_uns_bits_2;
  const HWFloat<8,12> c_hw_flt_8_12_bits_1;
  const HWFloat<8,12> c_hw_flt_8_12_undef;
  const HWFloat<8,12> c_hw_flt_8_12_bits_2;
  const HWFloat<8,12> c_hw_flt_8_12_bits_3;
  const HWFloat<8,12> c_hw_flt_8_12_bits_4;
  const HWFloat<8,12> c_hw_flt_8_12_bits_5;
  const HWFloat<8,12> c_hw_flt_8_12_bits_6;
  const HWFloat<8,12> c_hw_flt_8_12_bits_7;
  const HWFloat<8,12> c_hw_flt_8_12_bits_8;
  const HWFloat<8,12> c_hw_flt_8_12_bits_9;
  const HWFloat<8,12> c_hw_flt_8_12_bits_10;
  const HWOffsetFix<8,-8,UNSIGNED> c_hw_fix_8_n8_uns_bits;
  const HWFloat<8,12> c_hw_flt_8_12_bits_11;
  const HWFloat<8,12> c_hw_flt_8_12_bits_12;
  const HWFloat<8,12> c_hw_flt_8_12_bits_13;
  const HWFloat<8,12> c_hw_flt_8_12_bits_14;
  const HWFloat<8,12> c_hw_flt_8_12_bits_15;
  const HWFloat<8,12> c_hw_flt_8_12_bits_16;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWRawBits<11> c_hw_bit_11_bits;
  const HWRawBits<19> c_hw_bit_19_bits;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits_1;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_bits_2;
  const HWOffsetFix<9,0,TWOSCOMPLEMENT> c_hw_fix_9_0_sgn_undef;
  const HWFloat<8,12> c_hw_flt_8_12_bits_17;
  const HWFloat<8,12> c_hw_flt_8_12_bits_18;
  const HWFloat<8,12> c_hw_flt_8_12_bits_19;
  const HWFloat<8,12> c_hw_flt_8_12_bits_20;
  const HWFloat<8,12> c_hw_flt_8_12_bits_21;
  const HWOffsetFix<4,0,UNSIGNED> c_hw_fix_4_0_uns_bits;
  const HWOffsetFix<24,-23,UNSIGNED> c_hw_fix_24_n23_uns_bits;
  const HWOffsetFix<10,0,TWOSCOMPLEMENT> c_hw_fix_10_0_sgn_undef;
  const HWOffsetFix<12,-12,UNSIGNED> c_hw_fix_12_n12_uns_undef;
  const HWOffsetFix<8,-14,UNSIGNED> c_hw_fix_8_n14_uns_undef;
  const HWFloat<8,12> c_hw_flt_8_12_4_0val;
  const HWFloat<8,12> c_hw_flt_8_12_bits_22;
  const HWFloat<8,12> c_hw_flt_8_12_bits_23;
  const HWFloat<8,12> c_hw_flt_8_12_bits_24;
  const HWFloat<8,12> c_hw_flt_8_12_bits_25;
  const HWFloat<8,12> c_hw_flt_8_12_bits_26;
  const HWFloat<8,12> c_hw_flt_8_12_bits_27;
  const HWFloat<8,12> c_hw_flt_8_12_bits_28;
  const HWFloat<8,12> c_hw_flt_8_12_bits_29;
  const HWFloat<8,12> c_hw_flt_8_12_bits_30;
  const HWFloat<8,12> c_hw_flt_8_12_bits_31;
  const HWFloat<8,12> c_hw_flt_8_12_bits_32;
  const HWFloat<8,12> c_hw_flt_8_12_bits_33;
  const HWFloat<8,12> c_hw_flt_8_12_bits_34;
  const HWFloat<8,12> c_hw_flt_8_12_bits_35;
  const HWFloat<8,12> c_hw_flt_8_12_bits_36;
  const HWFloat<8,12> c_hw_flt_8_12_bits_37;
  const HWFloat<8,12> c_hw_flt_8_12_n0_25val;
  const HWFloat<8,12> c_hw_flt_8_12_bits_38;
  const HWFloat<8,12> c_hw_flt_8_12_bits_39;
  const HWFloat<8,12> c_hw_flt_8_12_bits_40;
  const HWFloat<8,12> c_hw_flt_8_12_bits_41;
  const HWFloat<8,12> c_hw_flt_8_12_bits_42;
  const HWFloat<8,12> c_hw_flt_8_12_bits_43;
  const HWFloat<8,12> c_hw_flt_8_12_bits_44;
  const HWFloat<8,12> c_hw_flt_8_12_bits_45;
  const HWFloat<8,12> c_hw_flt_8_12_bits_46;
  const HWFloat<8,12> c_hw_flt_8_12_bits_47;
  const HWFloat<8,12> c_hw_flt_8_12_bits_48;
  const HWFloat<8,12> c_hw_flt_8_12_bits_49;
  const HWFloat<8,12> c_hw_flt_8_12_bits_50;
  const HWFloat<8,12> c_hw_flt_8_12_bits_51;
  const HWFloat<8,12> c_hw_flt_8_12_bits_52;
  const HWFloat<8,12> c_hw_flt_8_12_bits_53;
  const HWFloat<8,12> c_hw_flt_8_12_bits_54;
  const HWFloat<8,12> c_hw_flt_8_12_bits_55;
  const HWFloat<8,12> c_hw_flt_8_12_bits_56;
  const HWFloat<8,12> c_hw_flt_8_12_bits_57;
  const HWFloat<8,12> c_hw_flt_8_12_bits_58;
  const HWFloat<8,12> c_hw_flt_8_12_bits_59;
  const HWRawBits<48> c_hw_bit_48_undef;
  const HWOffsetFix<2,-2,UNSIGNED> c_hw_fix_2_n2_uns_undef;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* BEELERREUTERKERNEL_H_ */
