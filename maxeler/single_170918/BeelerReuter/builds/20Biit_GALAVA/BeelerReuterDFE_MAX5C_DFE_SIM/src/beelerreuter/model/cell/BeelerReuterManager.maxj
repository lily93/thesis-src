package beelerreuter.model.cell;

import com.maxeler.maxcompiler.v2.build.EngineParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.managers.custom.CustomManager;
import com.maxeler.maxcompiler.v2.managers.custom.CustomManager.Config;
import com.maxeler.maxcompiler.v2.managers.custom.DFELink;
import com.maxeler.maxcompiler.v2.managers.custom.blocks.KernelBlock;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;
import com.maxeler.maxcompiler.v2.managers.standard.Manager;
import com.maxeler.maxcompiler.v2.managers.standard.Manager.IOType;
import com.maxeler.platform.max5.manager.MAX5CManager;

public class BeelerReuterManager extends MAX5CManager{
	private static final String s_kernelName = "BeelerReuterKernel";
	public BeelerReuterManager(EngineParameters arg0) {
		super(arg0);
		KernelBlock k = addKernel(
				new BeelerReuterKernel(makeKernelParameters(s_kernelName)));
		
		DFELink y1 = addStreamToCPU("u_out");
		y1 <== k.getOutput("u_out");
		DFELink y2 = addStreamToCPU("x1_out");
		y2 <== k.getOutput("x1_out");
		
		DFELink y3 = addStreamToCPU("m_out");
		y3 <== k.getOutput("m_out");
		
		DFELink y4 = addStreamToCPU("h_out");
		y4 <== k.getOutput("h_out");
		
		DFELink y5 = addStreamToCPU("j_out");
		y5 <== k.getOutput("j_out");
		
		DFELink y6 = addStreamToCPU("d_out");
		y6 <==k.getOutput("d_out");
		
		DFELink y7 = addStreamToCPU("f_out");
		y7 <== k.getOutput("f_out");
		
		DFELink y8 = addStreamToCPU("ca_out");
		y8 <== k.getOutput("ca_out");
		
		//config.setDefaultStreamClockFrequency(50);
	}

	


	public static void main(String[] args) {
		EngineParameters params = new EngineParameters(args);
		BeelerReuterManager manager = new BeelerReuterManager(params);

		// Instantiate the kernel
	
		
		manager.createSLiCinterface(interfaceDefault());
		manager.build();
	}

	private static EngineInterface interfaceDefault() {
		EngineInterface ei = new EngineInterface();

		InterfaceParam length = ei.addParam("length", CPUTypes.INT);
		InterfaceParam dt = ei.addParam("dt", CPUTypes.DOUBLE);
		InterfaceParam num_iteration = ei.addParam("num_iteration", CPUTypes.INT);
		
		InterfaceParam u = ei.addParam("u_in", CPUTypes.FLOAT);
		InterfaceParam ca = ei.addParam("ca_in", CPUTypes.FLOAT);
		InterfaceParam h = ei.addParam("h_in", CPUTypes.FLOAT);
		InterfaceParam x1 = ei.addParam("x1_in", CPUTypes.FLOAT);
		InterfaceParam j = ei.addParam("j_in", CPUTypes.FLOAT);
		InterfaceParam d = ei.addParam("d_in", CPUTypes.FLOAT);
		InterfaceParam f = ei.addParam("f_in", CPUTypes.FLOAT);
		InterfaceParam m = ei.addParam("m_in", CPUTypes.FLOAT);
		
		InterfaceParam lengthInBytes = length * CPUTypes.FLOAT.sizeInBytes();
		InterfaceParam loopLength = ei.getAutoLoopOffset(s_kernelName, "loopLength");
		ei.ignoreAutoLoopOffset(s_kernelName, "loopLength");
		ei.setTicks(s_kernelName, length * loopLength);

		ei.setScalar(s_kernelName, "dt", dt);
		
		ei.setScalar(s_kernelName, "u_in", u);
		ei.setScalar(s_kernelName, "ca_in", ca);
		ei.setScalar(s_kernelName, "h_in", h);
		ei.setScalar(s_kernelName, "x1_in", x1);
		ei.setScalar(s_kernelName, "j_in", j);
		ei.setScalar(s_kernelName, "d_in", d);
		ei.setScalar(s_kernelName, "f_in", f);
		ei.setScalar(s_kernelName, "m_in", m);
		
		ei.setScalar(s_kernelName, "num_iteration", num_iteration);

		ei.setStream("u_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("x1_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("m_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("h_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("j_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("d_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("f_out", CPUTypes.FLOAT, lengthInBytes);
		ei.setStream("ca_out", CPUTypes.FLOAT, lengthInBytes);
	

		return ei;
	}
}