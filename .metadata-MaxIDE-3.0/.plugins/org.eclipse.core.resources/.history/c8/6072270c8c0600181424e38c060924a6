package cell;



import com.maxeler.maxcompiler.v2.build.EngineParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;
import com.maxeler.maxcompiler.v2.managers.standard.Manager;
import com.maxeler.maxcompiler.v2.managers.standard.Manager.IOType;

public class CellManager {
	private static final String s_kernelName = "CellKernel";
	private static final int X = 8; // image row length (number of columns)

	public static void main(String[] args) {
		EngineParameters params = new EngineParameters(args);
		Manager manager = new Manager(params);

		// Instantiate the kernel
		Kernel kernel = new CellKernel(manager.makeKernelParameters(), X);


		manager.setKernel(kernel);
		manager.setIO(IOType.ALL_CPU); // Connect all kernel ports to the CPU
		manager.createSLiCinterface();
		manager.addMaxFileConstant("X", X);
		manager.build();
	}

	private static EngineInterface interfaceDefault() {
		EngineInterface ei = new EngineInterface();

		InterfaceParam length = ei.addParam("length", CPUTypes.INT);
		InterfaceParam lengthInBytes = length * CPUTypes.FLOAT.sizeInBytes();
		InterfaceParam loopLength = ei.getAutoLoopOffset("CellKernel", "loopLength");
		ei.ignoreAutoLoopOffset("CellKernel", "loopLength");

		ei.setTicks("CellKernel", length * loopLength);

		ei.setStream("output", CPUTypes.FLOAT, lengthInBytes / X);
		return ei;
	}
}
