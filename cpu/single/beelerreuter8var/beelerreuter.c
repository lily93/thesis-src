
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../header/model.h"

#define TAUE          2.5
#define TAUN        250
#define ONE_O_TAUE    0.4
#define ONE_O_TAUN    0.004
#define EH            3
#define EN            1
#define ESTAR         0.8 //1.5415
#define EPS           0.01               //TAUE/TAUN
#define RE            1.204              //VARIED BETWEEN 0.5 AND 1.4
#define EXPRE         0.299991841        //EXP(-RE)
#define M            10
#define GAMMA         0.0011
#define ONE_O_ONE_MINUS_EXPRE   1.428554778


#define NEXT_LINE(fp) while(getc(fp)!='\n');
#define e(n) *pow(10,n)



#define GS  0.09
#define	ENA 50
#define	GNAC 0.003
#define	GNA  4.


double dt = 0;
int simTime = 0;
int num_iteration=0; 
int mode = 0;
int preciscion = 1;
char  filename[];
const double vstar = 0.64;
const double gam = 1.92;
const double xm = 10;
const double delta = 0.23;
const double alpha = 7.5;

double heavisidefun(double x) {
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}
void init(double * u, double * x1, double * m, double * h, double * j, double * d, double *f, double * ca) {
      *u = -84.5737;
      *ca = 0.0000001;
      double ax1 = dt* ((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
     double bx1 = dt* (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));
      
      double am = dt *(-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
    double  bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));
      
    double  ah = dt*(0.126 * exp(-0.25 * ((*u) + 77.0)));
   double   bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));
      
     double aj = dt *(0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
     double bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));
      
    double  ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
    double  bd = dt *(0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));
      
    double  af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
    double  bf = dt * (0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));


   *x1 = (ax1 / (ax1 + bx1));
    *m = (am / (am + bm));
     *h = (ah / (ah + bh));
      *j = (aj / (aj + bj));
      *d = (ad / (ad + bd));
      *f = (af / (af + bf));
      
}

void beelerreuter_cstep(double * u, double * x1, double * m, double * h, double * j, double *d, double *f, double * ca) {
 

 double ax1 = dt *((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
  double        bx1 =  dt * (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));
          
  double        am = dt* (-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
  double        bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));
          
  double        ah = dt * (0.126 * exp(-0.25 * ((*u) + 77.0)));
  double        bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));
          
  double        aj = dt * (0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
  double        bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));
          
  double        ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
  double        bd =dt * (0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));
          
  double        af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
  double        bf = dt *(0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));
          
          *x1 = *(x1) + (ax1 * (1.0 - (*x1)) - bx1 * (*x1));
          *m = (*m) + (am * (1.0 - (*m)) - bm * (*m));
          *h =  (*h)+ (ah * (1.0 - (*h)) - bh * (*h));
          *j = (*j) + (aj * (1.0 - (*j)) - bj * (*j));
          *d = (*d) + (ad * (1.0 - (*d)) - bd * (*d));
          *f = (*f) + (af * (1.0 - (*f)) - bf * (*f));
          

         double es = (-82.3 - 13.0287 * log((*ca)));
         double is  =0.09*(*d)*(*f)*((*u)-es);
          *ca = *ca +dt*(-0.0000001*is+0.07*(0.0000001-(*ca)));

     double  xik1 = (exp(0.08 * ((*u) + 53.0)) + exp(0.04 * ((*u) + 53.0)));
   double       xik2 = (1.0 - exp(-0.04 * ((*u) + 23.0)));
          if (xik1 == 0.0) xik1 = 0.001;
          if (xik2 == 0.0) xik2 = 0.001;
   double       ik1 = (0.35 * (4.0 * (exp(0.04 * ((*u) + 85.0)) - 1.0) / xik1 + 0.2 * ((*u) + 23.0) / xik2));
  
        double  gix1 = (0.8 * (exp(0.04 * ((*u) + 77.0)) - 1.0) /exp(0.04 * ((*u) + 35.0)));
          double ix1 = (gix1 * (*x1));
          double ina = ((GNA * (*m) * (*m) * (*m) * (*h) * (*j) + GNAC) * ((*u) - ENA));
          (*u) = (*u) -( dt * (ik1 + ix1 + ina + is));
          
}




void createFile() {
		sprintf(filename, "../res/cpu_beelerreuter_cell_out_%f_%d_%d.csv", dt, mode, simTime);
		if( access(filename, F_OK) != -1) {
		remove(filename);
		}
}

void initParam(char * params) {

	  FILE *fp;
	
	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.05;
		  simTime = 1000;
		  mode = 1;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%lf",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %.20f\n", dt);
		fscanf(fp,"%d",&mode);     
		printf("READ MODE: %i\n", mode); 

	   fclose(fp);

	  }
	  num_iteration = (int)(simTime/dt);
	  preciscion = 1;


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %.20f\n", dt);
	  printf("mode     = %i\n", mode);
	  createFile();
		


}
void writeToFile(double u, double  x1, double m, double  h, double  j, double  d, double f, double ca) {
FILE *fp = fopen(filename, "a");
fprintf(fp, "%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", u,x1,m,h,j,d,f,ca);
fclose(fp);
}

double * runBeelerReuter() {
	int  sizeBytes = (num_iteration) * sizeof(double);
	double * stim_in = malloc(sizeBytes);
	double * us = malloc(sizeBytes);

double  u;
 double  x1;
 double m;
 double  h;
 double  j;
 double  d;
 double f;
 double ca;

init(&u, &x1, &m, &h, &j, &d, &f, &ca);
u= -50.0;
	
	struct timeval start, end;
			long mtime, seconds, useconds;


	
double t0_t1 = 0;
double t0_t2 = 0;
 double stim = 0.0;

	
printf("Running CPU\n");
	gettimeofday(&start, NULL);
	for(int i = 0; i<num_iteration; i++) {
       t0_t1 += dt;
	t0_t2 += dt;

 //stim = heavisidefun(t0_t1 - 0)*(1 - heavisidefun(t0_t2 - 1)) + heavisidefun(t0_t1 - 300)*(1 - heavisidefun(t0_t2 - 301)) + heavisidefun(t0_t1 - 700)*(1 - heavisidefun(t0_t2 - 701));	    
		beelerreuter_cstep(&u, &x1, &m, &h, &j, &d, &f, &ca);//, &w, &s, stim);
		writeToFile(u, x1, m, h, j, d,f,ca);
		us[i] = u;
		
		

	}
gettimeofday(&end, NULL);
	  seconds = end.tv_sec - start.tv_sec;
	  		useconds = end.tv_usec - start.tv_usec;

	  		mtime = ((seconds) * 1000 + useconds / 1000.0);

	  		printf("CPU elapsed time: %ld milliseconds \n", mtime);
return us;

}

double least_squares(double * u, double * un, int size, double dt1, double dt2) {
	double lse = 0;


	for(int i = 0; i<(size+1); i++) {
	lse += pow(((u[i]*dt1) - (un[i*2]* dt2 + un[(i*2)+1] * dt2)),2); 
	//printf("#%d ERROR: %.15f\n", (i+1), lse);
		
	}
	//lse = lse/size;
	printf("LSE dt: %f vs dt/2: %f: %g\n", dt1, dt2, (lse/size));
	return lse;

	
	
	
}
void step_size_estimation_test() {
	char * errorfile = "../res/step_size_estimation_beelerreuter.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");
	

	char * params = "../params.dat";
	initParam(params);
	createFile();
	int num_iteration_old = num_iteration;
	double * us_old = runBeelerReuter();
	int frac = 2;
	double dt_old = dt;
	dt = dt/frac;
	double err = 10;
	
	while(dt>=0.000781) {
		
		
		num_iteration = (int)floor(simTime/dt + 0.5);
		createFile();
		double * us;
		printf("RUNNING TEST.. DT (MS): %.20f \n", dt);
		us = runBeelerReuter();
		err = least_squares(us_old, us, num_iteration_old, dt_old, dt);
		printf("DT(MS): %g\n ERROR: %lf\n---------------------------------------\n", dt, (err/num_iteration_old));
		fprintf(fp,"%g\n", (err/num_iteration_old));
		err = (err/num_iteration_old);
		us_old = us;
		frac *=2;
		dt_old = dt;
		num_iteration_old = num_iteration;
		dt = dt/2;
		

		
	}
	fclose(fp);





}

int main()
{

	step_size_estimation_test();
	return 0;
}
