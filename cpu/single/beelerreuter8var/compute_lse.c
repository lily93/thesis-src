#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../header/model.h"


#define GS  0.09
#define	ENA 50
#define	GNAC 0.003
#define	GNA  4.

void init(double * u, double * x1, double * m, double * h, double * j, double * d, double *f, double * ca, double dt) {
      *u = -84.5737;
      *ca = 0.0000001;
      double ax1 = dt* ((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
     double bx1 = dt* (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));
      
      double am = dt *(-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
    double  bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));
      
    double  ah = dt*(0.126 * exp(-0.25 * ((*u) + 77.0)));
   double   bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));
      
     double aj = dt *(0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
     double bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));
      
    double  ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
    double  bd = dt *(0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));
      
    double  af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
    double  bf = dt * (0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));


   *x1 = (ax1 / (ax1 + bx1));
    *m = (am / (am + bm));
     *h = (ah / (ah + bh));
      *j = (aj / (aj + bj));
      *d = (ad / (ad + bd));
      *f = (af / (af + bf));
      
}

void beelerreuter_cstep(double * u, double * x1, double * m, double * h, double * j, double *d, double *f, double * ca, double dt) {
 

 double ax1 = dt *((5.0*pow(10,-4)) * exp(0.083 * ((*u) + 50.0)) / (exp(0.057 * ((*u) + 50.0)) + 1.0));
  double        bx1 =  dt * (0.0013 * exp(-0.06 * ((*u) + 20.0)) / (exp(-0.04 * ((*u) + 20.0)) + 1.0));
          
  double        am = dt* (-((*u) + 47.0) / (exp(-0.1 * ((*u) + 47.0)) - 1.0));
  double        bm = dt *(40.0 * exp(-0.056 * ((*u) + 72.0)));
          
  double        ah = dt * (0.126 * exp(-0.25 * ((*u) + 77.0)));
  double        bh = dt *(1.7 / (exp(-0.082 * ((*u) + 22.5)) + 1.0));
          
  double        aj = dt * (0.055 * exp(-0.25 * ((*u) + 78.0)) / (exp(-0.2 * ((*u) + 78.0)) + 1.0));
  double        bj = dt * (0.3 / (exp(-0.1 * ((*u) + 32.0)) + 1.0));
          
  double        ad = dt * (0.095 * exp(-0.01 * ((*u) - 5.0)) / (exp(-0.072 * ((*u) - 5.0)) + 1.0));
  double        bd =dt * (0.07 * exp(-0.017 * ((*u) + 44.0)) / (exp(0.05 * ((*u) + 44.0)) + 1.0));
          
  double        af = dt * (0.012 * exp(-0.008 * ((*u) + 28.0)) / (exp(0.15 * ((*u) + 28.0)) + 1.0));
  double        bf = dt *(0.0065 * exp(-0.02 * ((*u) + 30.0)) / (exp(-0.2 * ((*u) + 30.0)) + 1.0));
          
          *x1 = *(x1) + (ax1 * (1.0 - (*x1)) - bx1 * (*x1));
          *m = (*m) + (am * (1.0 - (*m)) - bm * (*m));
          *h =  (*h)+ (ah * (1.0 - (*h)) - bh * (*h));
          *j = (*j) + (aj * (1.0 - (*j)) - bj * (*j));
          *d = (*d) + (ad * (1.0 - (*d)) - bd * (*d));
          *f = (*f) + (af * (1.0 - (*f)) - bf * (*f));
          

         double es = (-82.3 - 13.0287 * log((*ca)));
         double is  =0.09*(*d)*(*f)*((*u)-es);
          *ca = *ca +dt*(-0.0000001*is+0.07*(0.0000001-(*ca)));

     double  xik1 = (exp(0.08 * ((*u) + 53.0)) + exp(0.04 * ((*u) + 53.0)));
   double       xik2 = (1.0 - exp(-0.04 * ((*u) + 23.0)));
          if (xik1 == 0.0) xik1 = 0.001;
          if (xik2 == 0.0) xik2 = 0.001;
   double       ik1 = (0.35 * (4.0 * (exp(0.04 * ((*u) + 85.0)) - 1.0) / xik1 + 0.2 * ((*u) + 23.0) / xik2));
  
        double  gix1 = (0.8 * (exp(0.04 * ((*u) + 77.0)) - 1.0) /exp(0.04 * ((*u) + 35.0)));
          double ix1 = (gix1 * (*x1));
          double ina = ((GNA * (*m) * (*m) * (*m) * (*h) * (*j) + GNAC) * ((*u) - ENA));
          (*u) = (*u) -( dt * (ik1 + ix1 + ina + is));
          
}

double compute_lse_cepi (double dt)
{

      double t_sim_time      = 1000.0; //in ms
      
      
      double t0_dt           = dt;
      int  t0_num_element  = (int)floor(t_sim_time/t0_dt + 0.5); 
      
      double * t0_result     = (double *) calloc (t0_num_element+1, sizeof(double));
      double t0_u = 0;
 double  x1 = 0;
 double m = 0;
 double  h = 0;
 double  j = 0;
 double  d = 0;
 double f = 0;
 double ca = 0;

init(&t0_u, &x1, &m, &h, &j, &d, &f, &ca, t0_dt);
t0_u= -50.0;
	
	  
	  t0_result[0]         = t0_u;
	  
	  //printf("%f\t%f\n", t0_t1, t0_u);
	  
	  for (int i=1; i <= t0_num_element; i++)
	  {
	     //  t0_t1 += t0_dt;
	      // t0_t2 += t0_dt;
	       
	       //t0_stim  = heaviside(t0_t1 - 0)*(1 - heaviside(t0_t2 - 1)) + heaviside(t0_t1 - 300)*(1 - heaviside(t0_t2 - 301)) + heaviside(t0_t1 - 700)*(1 - heaviside(t0_t2 - 701));
	    beelerreuter_cstep(&t0_u,&x1, &m, &h, &j, &d, &f, &ca, t0_dt) ;
	       t0_result[i]  = t0_u;
	       //printf("%f\t%f\n", t0_t1, t0_u);
	  }
	  
	  
	  double t1_dt           = dt/2;
      int  t1_num_element  = (int)floor(t_sim_time/t1_dt + 0.5); 
      
      double * t1_result     = (double *) calloc (t1_num_element+1, sizeof(double));
 double t1_u = 0;
 double  t1_x1 = 0;
 double t1_m = 0;
 double  t1_h = 0;
 double  t1_j = 0;
 double  t1_d = 0;
 double t1_f = 0;
 double t1_ca = 0;

init(&t1_u, &t1_x1, &t1_m, &t1_h, &t1_j, &t1_d, &t1_f, &t1_ca, t1_dt);
t1_u = -50;
	  
	  t1_result[0]         = t1_u;
	  
	  //printf("%f\t%f\n", t1_t1, t1_u);
	  
	   for (int i=1; i <= t1_num_element; i++)
	  {
	     //  t1_t1 += t1_dt;
	      // t1_t2 += t1_dt;
	       
	       //t1_stim  = heaviside(t1_t1 - 0)*(1 - heaviside(t1_t2 - 1)) + heaviside(t1_t1 - 300)*(1 - heaviside(t1_t2 - 301)) + heaviside(t1_t1 - 700)*(1 - heaviside(t1_t2 - 701));
	       beelerreuter_cstep(&t1_u,&t1_x1, &t1_m, &t1_h, &t1_j, &t1_d, &t1_f, &t1_ca, t1_dt);
	       t1_result[i]  = t1_u;
	       //printf("%f\t%f\n", t1_t1, t1_u);
	  }
	  
	  double lse = 0.0;
	  
	  for (int i=0; i < t0_num_element; i++)
	  {
	       lse += pow(((t0_result[i]*dt) - (t1_result[i*2]* dt/2 + t1_result[i*2+1] * dt/2)),2); 
	  }
	  
	  printf("dt=%f vs dt=%f lse = %g\n", dt, dt/2, lse);
	  
	  free(t0_result);
	  free(t1_result);
	  
	  return lse;

}

int main()
{

	  compute_lse_cepi (0.1);
      compute_lse_cepi (0.1/2.0);
	  compute_lse_cepi (0.1/4.0);
	  compute_lse_cepi (0.1/8.0);
	  compute_lse_cepi (0.1/16.0);
	  compute_lse_cepi (0.1/32.0);
	  compute_lse_cepi (0.1/64.0);
 compute_lse_cepi (0.1/128.0);
 compute_lse_cepi (0.1/256.0);




	return 0;
}
