/**
 * Document: MaxCompiler Tutorial (maxcompiler-tutorial)
 * Chapter: 3      Example: 1      Name: Moving Average Simple
 * MaxFile name: MovingAverageSimple
 * Summary:
 *   CPU code for the three point moving average design.
 */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../header/model.h"

typedef Real float;
#define bits 32
#define NEXT_LINE(fp) while(getc(fp)!='\n');

Real dt = 0;
int simTime = 0;
int num_iteration=0; 
int mode = 0;
char  filename[];


void initValues(Real * u, Real * v, Real * w, Real * s) {
	for(int i = 0; i<simTime; i++) {
		u[i] = 0.0;
		v[i] = 1.0;
		w[i] = 1.0;
		s[i] = 0.0;

	}
}

int random_index(int min, int max)
{
   srand ( time(NULL) );
   return min + rand() % (max+1 - min);
}


void initStim(Real * stim_in) {
	int simTimesControlled[3] = {0 , 300, 700};

	

		if(mode == 1) { 
			printf("Normal Mode.. Generating\n");
			take_step(stim_in,num_iteration,3,simTimesControlled);

		 }else if(mode == 2) {
			printf("Random Mode.. Generating\n");
			int num = 0;
			int * randm = malloc((sizeof(int)*num));
			
			for (int i = 0; i<simTime;) {
				randm[num] = i;
				num++;
				int min = 300;
				int max = simTime;
				srand ( time(NULL) );
				int random = rand() % (max+1);
				i += random;
				
				
			}
			take_step(stim_in, num_iteration, num, randm);
			
			
		} else if(mode == 3) { 
			printf("Spaced Mode.. Generating\n");
			int num = simTime/400;
			int * spaced = malloc((sizeof(int)*num));
			int count = 0;
			for(int i = 0; i<simTime; i+=400) {
				spaced[count] = i;
				count++;
				
			}
			take_step(stim_in, num_iteration, (num+1), spaced);
		} else {
			printf("FALLBACK\nNormal Mode.. Generating\n");
			take_step(stim_in,num_iteration,3,simTimesControlled);

		}
		
	

	

}
void  cstep_epi ( Real *ui, Real *vi, Real *wi, Real *si,  Real dt, Real lap, Real stim, int flag){

	  Real tvm, vinf, winf;
	  Real jfi, jso, jsi;
	  Real twm, tso, ts, to, ds, dw, dv;

	  tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
      twm =    TW1M + (  TW2M_TW1M_DIVBY2)*(1.+tanh(  KWM*(*ui-  UWM)));
      tso =    TSO1 + (  TSO2_TSO1_DIVBY2)*(1.+tanh(  KSO*(*ui-  USO)));
      ts  = (*ui >   THW)  ?   TS2  :   TS1;
      to  = (*ui >   THO)  ?   TO2  :   TO1;

      vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  winf = (*ui >    THWINF) ?   WINFSTAR: 1.0-*ui/  TWINF;
      if (winf > 1) winf = 1.0;

      dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
      dw = (*ui >   THW) ? -*wi/  TWP : (winf-*wi)/twm;
      ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;

      //winf = (*ui > 0.06) ? 0.94: 1.0-*ui/0.07;
      //twm  =  60 + (-22.5)*(1.+tanh(65*(*ui-0.03)));
      //dw   = (*ui > 0.13) ? -*wi/200 : (winf-*wi)/twm;




	  //tvm =  (*ui >   THVM) ?   TV2M :   TV1M;
	  //one_o_twm = segm_table[0][th] * (*ui) + segm_table[1][th];
	  //vinf = (*ui >    THVINF) ? 0.0: 1.0;
	  //winf = (*ui >    THWINF) ?   WINFSTAR * one_o_twm: (segm2_table[0][th2] * (*ui) + segm2_table[1][th2]);
	  //if (winf >one_o_twm) winf = one_o_twm;
	  //dv = (*ui >   THV) ? -*vi/  TVP : (vinf-*vi)/tvm;
	  //dw = (*ui >   THW) ? -*wi/  TWP : winf - *wi * one_o_twm;
	  //ds = (((1.+tanh(  KS*(*ui-  US)))/2.) - *si)/ts;





	  //Update gates

       *vi += dv*dt;
       *wi += dw*dt;
       *si += ds*dt;

	  //Compute currents
      jfi = (*ui >   THV)  ? -*vi * (*ui -   THV) * (  UU - *ui)/  TFI : 0.0;
      /*if (*ui >   THV){
         	  if (((*vi - dv*dt) > 0.0) && *vi < 0.0001){
         		  printf("vi=%4.20f, ui=%f, jfi=%f\n", *vi, *ui, jfi);
         	  }
           }*/
      jso = (*ui >   THSO) ? 1./tso : (*ui-  UO)/to;
      jsi = (*ui >   THSI) ? -*wi * *si/  TSI : 0.0;

      *ui = *ui  - (jfi+jso+jsi-stim)*dt + lap;
}
Real compute_lse_cepi (Real dt)
{

      Real t_sim_time      = 1000.0; //in ms
      
      
      Real t0_dt           = dt;
      int  t0_num_element  = (int)floor(t_sim_time/t0_dt + 0.5); 
      
      Real * t0_result     = (Real *) calloc (t0_num_element+1, sizeof(Real));
      
      Real   t0_u          = 1.0;
      Real   t0_v          = 1.0;
      Real   t0_w          = 1.0;
	  Real   t0_s          = 0.0;
	  
	  Real   t0_t1         = 0.0;
	  Real   t0_t2         = 0.0;
	  
	  Real   t0_stim       = 0.0;
	  
	  t0_result[0]         = t0_u;
	  
	  //printf("%f\t%f\n", t0_t1, t0_u);
	  
	  for (int i=1; i <= t0_num_element; i++)
	  {
	       t0_t1 += t0_dt;
	       t0_t2 += t0_dt;
	       
	       //t0_stim  = heaviside(t0_t1 - 0)*(1 - heaviside(t0_t2 - 1)) + heaviside(t0_t1 - 300)*(1 - heaviside(t0_t2 - 301)) + heaviside(t0_t1 - 700)*(1 - heaviside(t0_t2 - 701));
	       cstep_epi (&t0_u, &t0_v, &t0_w, &t0_s,  t0_dt, 0.0, t0_stim, 0);
	       t0_result[i]  = t0_u;
	       //printf("%f\t%f\n", t0_t1, t0_u);
	  }
	  
	  
	  Real t1_dt           = dt/2;
      int  t1_num_element  = (int)floor(t_sim_time/t1_dt + 0.5); 
      
      Real * t1_result     = (Real *) calloc (t1_num_element+1, sizeof(Real));
      
      Real   t1_u          = 1.0;
      Real   t1_v          = 1.0;
      Real   t1_w          = 1.0;
	  Real   t1_s          = 0.0;
	  
	  Real   t1_t1         = 0.0;
	  Real   t1_t2         = 0.0;
	  
	  Real   t1_stim       = 0.0;
	  
	  t1_result[0]         = t1_u;
	  
	  //printf("%f\t%f\n", t1_t1, t1_u);
	  
	   for (int i=1; i <= t1_num_element; i++)
	  {
	       t1_t1 += t1_dt;
	       t1_t2 += t1_dt;
	       
	       //t1_stim  = heaviside(t1_t1 - 0)*(1 - heaviside(t1_t2 - 1)) + heaviside(t1_t1 - 300)*(1 - heaviside(t1_t2 - 301)) + heaviside(t1_t1 - 700)*(1 - heaviside(t1_t2 - 701));
	       cstep_epi (&t1_u, &t1_v, &t1_w, &t1_s,  t1_dt, 0.0, t1_stim, 0);
	       t1_result[i]  = t1_u;
	       //printf("%f\t%f\n", t1_t1, t1_u);
	  }
	  
	  Real lse = 0.0;
	  
	  for (int i=0; i < t0_num_element; i++)
	  {
	       lse += pow(((t0_result[i]*dt) - (t1_result[i*2]* dt/2 + t1_result[i*2+1] * dt/2)),2); 
	  }
	  
	  printf("dt=%f vs dt=%f lse = %g\n", dt, dt/2, lse);
	  
	  free(t0_result);
	  free(t1_result);
	  
	  return lse;

}

void reactionTerm(Real * u, Real *v, Real * w, Real * s, Real stim) {
Real jfi, jso, jsi; 

 if ((*u) < 0.006) {
	              *w = (*w) + ((1.0 -((*u)/ TWINF) - (*w))/( TW1M + ( TW2M -  TW1M) * 0.5 * (1.+tanh( KWM*((*u)- UWM)))))*dt;
	              *v = (*v) + ((1.0-(*v))/ TV1M)*dt;
	              *s = (*s) + ((((1.+tanh( KS*((*u) -  US))) * 0.5) - (*s))/ TS1)*dt;
	              jfi = 0.0;
	              jso = (*u)/ TO1;
	              jsi = 0.0;
	    }

	         else if ((*u) < 0.13) {
	              *w = (*w) + ((0.94-(*w))/( TW1M + ( TW2M -  TW1M) * 0.5 * (1.+tanh( KWM*((*u)- UWM)))))*dt;
	              *v = (*v) + (-(*v)/ TV2M)*dt;
	              *s = (*s) +((((1.+tanh( KS*((*u)- US))) * 0.5) - (*s))/ TS1)*dt;
	              jfi = 0.0;
	              jso = (*u)/ TO2;
	              jsi = 0.0;
	         }

	         else if ((*u) < 0.3){
	              *w = (*w) + (-(*w)/ TWP)*dt;
	              *v = (*v) + (-(*v)/ TV2M)*dt;
	              *s = (*s) + ((((1.+tanh( KS*((*u)- US))) * 0.5) - (*s))/ TS2)*dt;
	              jfi = 0.0;
	              jso = 1./( TSO1+(( TSO2- TSO1)*(1.+tanh( KSO*((*u) -  USO)))) * 0.5);
	              jsi = -(*w) * (*s)/ TSI;
	         }  else {
	              *w  = (*w) + (-(*w)/ TWP)*dt;
	              *v  = (*v) + (-(*v)/ TVP)*dt;
	              *s  = (*s) +((((1.+tanh( KS*((*u) -  US))) * 0.5) - (*s))/ TS2)*dt;

	              jfi = -(*v) * ((*u) -  THV) * ( UU - (*u))/ TFI;
	              jso = 1./( TSO1+(( TSO2 -  TSO1)*(1.+tanh( KSO*((*u) -  USO)))) * 0.5);
	              jsi = -(*w) * (*s)/ TSI;
	}
	    *u = (*u)  - (jfi+jso+jsi-stim)*dt;
	
	
}




Real heavisidefun(Real x) {
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}

void writeToFile(Real u, Real v, Real s, Real  w, Real stim) {
FILE *fp = fopen(filename, "a");
fprintf(fp, "%g\t%f\t%f\t%f\t%f\n", u, v,  s, w,stim);
fclose(fp);
}


void printTimes(long times) {
FILE *fp = fopen("CPU_times.dat", "a");
fprintf(fp, "Bits: %d\nSimulation Time (MS): %d\nDT: %g\nElapsed Time: %ld\n-------------------------------\n",bits, simTime, dt, times);
fclose(fp);
}

Real * runCPU() {
	int  sizeBytes = (num_iteration) * sizeof(Real);
	Real * stim_in = malloc(sizeBytes);
	Real * us = malloc(sizeBytes);

	Real u  = 1.0;
	Real w = 1.0;
	Real v = 1.0;
	Real s = 0;
Real t0_t1 = 0;
Real t0_t2 = 0;
Real stim = 0.0;
	struct timeval start, end;
			long mtime, seconds, useconds;


	Real jfi;
	Real jso;
	Real jsi;

	
	
	//initStim(stim_in);
	
	printf("Running CPU\n");
	gettimeofday(&start, NULL);
	for(int i = 0; i<num_iteration; i++) {
       t0_t1 += dt;
	t0_t2 += dt;
	// stim = heavisidefun(t0_t1 - 0)*(1 - heavisidefun(t0_t2 - 1)) + heavisidefun(t0_t1 - 300)*(1 - heavisidefun(t0_t2 - 301)) + heavisidefun(t0_t1 - 700)*(1 - heavisidefun(t0_t2 - 701));	  
		reactionTerm(&u, &v, &w, &s, stim);
		//printf("u: %f\n v: %f\n, w: %f\n, s: %f\n, stim: %f\n", u, v, w, s, stim_in[i]);
		//writeToFile(u, v, w, s, stim);
		//us[i] = u;
		
		

	}

		gettimeofday(&end, NULL);
	  seconds = end.tv_sec - start.tv_sec;
	  		useconds = end.tv_usec - start.tv_usec;

	  		mtime = ((seconds) * 1000 + useconds / 1000.0);

	  		printf("CPU elapsed time: %ld milliseconds \n", mtime);
			printTimes(mtime);

	  
return us;

}


void take_step(Real *stim_in, const int time,int stimNum, int * stimTimes) {

	int sizeBytes = (stimNum) * sizeof(Real);
	Real t1 = 0.0;
	Real t2 = 0.0;



	for(int i = 0; i<time; i++){


			t1+=dt;
			t2+=dt;

			for(int j = 0; j<stimNum; j++){
			Real c1 = t1-stimTimes[j];
			Real c2 = t2-(stimTimes[j]+1);
			stim_in[i] += heavisidefun(c1) * (1 - heavisidefun(c2));
			}

	}









}


void initParam(char * params) {

	  FILE *fp;
	
	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.05;
		  simTime = 1000;
		  mode = 1;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%lf",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %.20f\n", dt);
		fscanf(fp,"%d",&mode);     
		printf("READ MODE: %i\n", mode); 

	   fclose(fp);

	  }
	  num_iteration = (int)floor(simTime/dt + 0.5);
	  preciscion = 1;


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %.20f\n", dt);
	  printf("mode     = %i\n", mode);
		


}

Real * getRefVals(int size, Real * y, int frac) {
	int sizeBytes = (size) * sizeof(Real);
	Real * cropped = malloc(sizeBytes) ;
	int index = 0;
	for(int i = (frac-1); i<size; i+=frac) {
		cropped[index] = y[i];
		index++;
	}	
	
	return cropped;

}
Real least_squares(Real * u, Real * un, int size, Real dt1, Real dt2) {
	Real lse = 0;
	char  errfi[1024];
	sprintf(errfi, "../res/error_%f_vs_%f.dat", dt1, dt2);
		if( access(errfi, F_OK) != -1) {
		remove(errfi);
		}
	FILE *fp = fopen(errfi, "w");

	for(int i = 0; i<(size+1); i++) {
	Real err = pow(((u[i]*dt1) - (un[i*2]* dt2 + un[(i*2)+1] * dt2)),2); 
	lse+=err;
	//printf("#%d ERROR: %.15f\n", (i+1), lse);
	fprintf(fp, "%g\n",err);
		
	}
	printf("LSE dt: %f vs dt/2: %f: %g\n", dt1, dt2, (lse));
	return lse;

	
	
	
}

void createFile() {
		sprintf(filename, "../res/cpu_single_cell_out_%f_%d_%d.csv", dt, mode, simTime);
		if( access(filename, F_OK) != -1) {
		remove(filename);
		}
}

void speedTest(double stepsize) {
	 dt                      =   stepsize;//Original is 0.1
		num_iteration = (int) simTime/dt;
		runCPU();
}

void step_size_estimation_test() {
	char * errorfile = "../res/step_size_estimation_minimal.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");
	

	char * params = "../../params/init.dat";
	initParam(params);
	createFile();
	int num_iteration_old = num_iteration;
	Real * us_old = runCPU();
	int frac = 2;
	Real dt_old = dt;
	dt = dt/frac;
	Real err = 10;
	
	while(dt>=0.000781) {
		
		
		num_iteration = (int)floor(simTime/dt + 0.5);
		createFile();
		Real * us;
		printf("RUNNING TEST.. DT (MS): %.20f \n", dt);
		us = runCPU();
		err = least_squares(us_old, us, num_iteration_old, dt_old, dt);
		printf("DT(MS): %g\n ERROR: %g\n---------------------------------------\n", dt, (err/num_iteration_old));
		fprintf(fp,"%g\n", err/num_iteration_old);
		us_old = us;
		frac *=2;
		dt_old = dt;
		num_iteration_old = num_iteration;
		dt = dt/2;
		

		
	}
	fclose(fp);



}

int main()
{
	test(1000, 0.0125);
	test(1000, (0.0125/2));
	test(1000, (0.0125/4));
	  
	  //compute_lse_cepi (0.1);
      //compute_lse_cepi (0.1/2.0);
	  //compute_lse_cepi (0.1/4.0);
	  //compute_lse_cepi (0.1/8.0);
	  //compute_lse_cepi (0.1/16.0);
	  //compute_lse_cepi (0.1/32.0);
	  //compute_lse_cepi (0.1/64.0);
	    
	return 0;
}


