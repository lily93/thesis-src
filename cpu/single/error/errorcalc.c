#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>


double * getRefVals(int size, double * y, int frac) {
	int sizeBytes = (size) * sizeof(double);
	double * cropped = malloc(sizeBytes) ;
	int index = 0;
	for(int i = (frac-1); i<size; i+=frac) {
		cropped[index] = y[i];
		index++;
	}	
	
	return cropped;

}

double least_squares(double * u, double * un, int size, double dt1, double dt2) {
	double lse = 0;

	for(int i = 0; i<size; i++) {
	lse += pow(((u[i]*dt1) - (un[i*2]* dt2 + un[i*2+1] * dt2)),2); 
		
	}
	return lse;

	
	
	
}
