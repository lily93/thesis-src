
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../header/model.h"

#define TAUE          2.5
#define TAUN        250
#define ONE_O_TAUE    0.4
#define ONE_O_TAUN    0.004
#define EH            3
#define EN            1
#define ESTAR         0.8 //1.5415
#define EPS           0.01               //TAUE/TAUN
#define RE            1.204              //VARIED BETWEEN 0.5 AND 1.4
#define EXPRE         0.299991841        //EXP(-RE)
#define M            10
#define GAMMA         0.0011
#define ONE_O_ONE_MINUS_EXPRE   1.428554778


#define NEXT_LINE(fp) while(getc(fp)!='\n');

double dt = 0;
int simTime = 0;
int num_iteration=0; 
int mode = 0;
int preciscion = 1;
char  filename[];
const double vstar = 0.64;
const double gam = 1.92;
const double xm = 10;
const double delta = 0.23;
const double alpha = 7.5;

double heavisidefun(double x) {
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}

void twoVarKarma(double * u, double * v) {
 	  
	      
	    double  dfunc = pow((*v), 10);
	    double  rfunc = 1.428554778 - (*v);
	    double  hfunc = (1-tanh((*u)-EH)) * (*u) * (*u) * 0.5;
	   double   ffunc = -(*u) + (ESTAR-dfunc) * hfunc;
	      //gfunc = rfunc * (e[i] - EN > 0) - (1 - (e[i] -EN>0)) * n[i];
	     double h = ((*u) > EN);
	      //(a -n)*h  - (1-h)*n
	      //a*h -n*h  - (n - n*h)
	    double  gfunc = 1.428554778*h - (*v); 
	    double  de    = ffunc * ONE_O_TAUE;
	     double dn    = gfunc * ONE_O_TAUN;
	      
          (*u)       = (*u) + dt*de;
         (*v)  = (*v) + dt*dn;

          
}

void cstep_naive ( double * u, double * v, double * w, double *s, double stim){


	  // double u, v, w, s;
	  double tvm, vinf, winf;
	  double jfi, jso, jsi;
	  double twm, tso, ts, to, ds, dw, dv;
	  
                   
           
         	      tvm =  ((*u) > THVM) ? TV2M : TV1M;
     	      ts  = ((*u) > THW)  ? TS2  : TS1;
     	      to  = ((*u) > THO)  ? TO2  : TO1;
               tso =  TSO1 + (TSO2_TSO1_DIVBY2)*(1.+tanh(KSO*((*u)-USO)));
               twm =  1./(TW1M + (TW2M_TW1M_DIVBY2)*(1.+tanh(KWM*((*u)-UWM))));   
               
              // ds = (((1.+tanh(KS*((*u)-US)))/2.) - (*s))/ts;

               vinf = ((*u) >  THVINF) ? 0.0: 1.0;
     	      winf = ((*u) >  THWINF) ? WINFSTAR: 1.0-(*u)/TWINF;

               dv = ((*u) > THV) ? -(*v)/TVP : (vinf-(*v))/tvm;
               dw = ((*u) > THW) ? -(*w)/TWP : (winf-(*w))*twm;
               

     	      //Update gates

              	(*v)    += dv*dt;
               (*w)    += dw*dt;
         

     	       //Compute currents
                jfi = ((*u) > THV)  ? -(*v) * ((*u) - THV) * (UU - (*u))/TFI : 0.0;
                jso = ((*u) > THSO) ? 1./tso : ((*u)-UO)/to;
               
                jsi = ((*u) > THSI) ? -(*w) * (1+tanh(KS*((*u)-US)))/(2*TSI) : 0.0;

               (*u)       = (*u) - (jfi+jso+jsi-stim)*dt;
                (*v)  = (*v);
                (*w) = (*w);
            
        }



void createFile() {
		sprintf(filename, "../res/cpu_karma_cell_out_%f_%d_%d.csv", dt, mode, simTime);
		if( access(filename, F_OK) != -1) {
		remove(filename);
		}
}

void initParam(char * params) {

	  FILE *fp;
	
	 fp=fopen(params,"r");
	  if(fp==NULL)
	  {	 printf("NO FILE FOUND: FALLBACK");
		  dt = 0.05;
		  simTime = 1000;
		  mode = 1;

	  }else {
		fscanf(fp,"%i",&simTime);   	NEXT_LINE(fp);
		printf("READ SIMTIME (MS): %d\n", simTime);
		fscanf(fp,"%lf",&dt);     NEXT_LINE(fp);
		printf("READ DT (MS): %.20f\n", dt);
		fscanf(fp,"%d",&mode);     
		printf("READ MODE: %i\n", mode); 

	   fclose(fp);

	  }
	  num_iteration = (int)(simTime/dt);
	  preciscion = 1;


	  printf("\n\n Parameters: \n");
	  printf("timesteps    = %d\n", num_iteration);
	  printf("dt     = %.20f\n", dt);
	  printf("mode     = %i\n", mode);
	  createFile();
		


}
void writeToFile(double u, double v) {
FILE *fp = fopen(filename, "a");
fprintf(fp, "%g\t%g\n", u, v);
fclose(fp);
}

double * runKarma() {
	int  sizeBytes = (num_iteration) * sizeof(double);
	double * stim_in = malloc(sizeBytes);
	double * us = malloc(sizeBytes);

	double u  = 1.5;
	double v = 0.0;
	struct timeval start, end;
			long mtime, seconds, useconds;


	double jfi;
	double jso;
	double jsi;
double t0_t1 = 0;
double t0_t2 = 0;
 double stim = 0.0;

	
printf("Running CPU\n");
	gettimeofday(&start, NULL);
	for(int i = 0; i<num_iteration; i++) {
       t0_t1 += dt;
	t0_t2 += dt;

 //stim = heavisidefun(t0_t1 - 0)*(1 - heavisidefun(t0_t2 - 1)) + heavisidefun(t0_t1 - 300)*(1 - heavisidefun(t0_t2 - 301)) + heavisidefun(t0_t1 - 700)*(1 - heavisidefun(t0_t2 - 701));	    
		twoVarKarma(&u, &v);//, &w, &s, stim);
		writeToFile(u, v);
		us[i] = u;
		
		

	}
gettimeofday(&end, NULL);
	  seconds = end.tv_sec - start.tv_sec;
	  		useconds = end.tv_usec - start.tv_usec;

	  		mtime = ((seconds) * 1000 + useconds / 1000.0);

	  		printf("CPU elapsed time: %ld milliseconds \n", mtime);
return us;

}

double least_squares(double * u, double * un, int size, double dt1, double dt2) {
	double lse = 0;


	for(int i = 0; i<(size+1); i++) {
	lse += pow(((u[i]*dt1) - (un[i*2]* dt2 + un[(i*2)+1] * dt2)),2); 
	//printf("#%d ERROR: %.15f\n", (i+1), lse);
		
	}
	//lse = lse/size;
	printf("LSE dt: %f vs dt/2: %f: %g\n", dt1, dt2, (lse/size));
	return lse;

	
	
	
}
void step_size_estimation_test() {
	char * errorfile = "../res/step_size_estimation_karma.dat";
if( access(errorfile, F_OK) !=-1) {
remove(errorfile);
}
	FILE *fp = fopen(errorfile, "a");
	

	char * params = "../../params/init.dat";
	initParam(params);
	createFile();
	int num_iteration_old = num_iteration;
	double * us_old = runKarma();
	int frac = 2;
	double dt_old = dt;
	dt = dt/frac;
	double err = 10;
	
	while(dt>=0.000781) {
		
		
		num_iteration = (int)floor(simTime/dt + 0.5);
		createFile();
		double * us;
		printf("RUNNING TEST.. DT (MS): %.20f \n", dt);
		us = runKarma();
		err = least_squares(us_old, us, num_iteration_old, dt_old, dt);
		printf("DT(MS): %g\n ERROR: %lf\n---------------------------------------\n", dt, (err/num_iteration_old));
		fprintf(fp,"%g\n", (err/num_iteration_old));
		us_old = us;
		frac *=2;
		dt_old = dt;
		num_iteration_old = num_iteration;
		dt = dt/2;
		

		
	}
	fclose(fp);





}

int main()
{

	step_size_estimation_test();
	return 0;
}
