
#define TVP      1.4506 //The same with Flavio's paper
#define TV1M     60.    //The same with Flavio's paper
#define TV2M     1150.  //The same with Flavio's paper

#define TWP      200.0   // We have TWP1 and TWP2 = TWP in Flavio's paper

//#define TW1P     200.0
//#define TW2P     200.0

#define TW1M     60.0    //The same with Flavio's paper
#define TW2M     15.     //The same with Flavio's paper
#define TS1      2.7342  //The same with Flavio's paper
#define TS2      16.     //The same with Flavio's paper
#define TFI      0.11    //The same with Flavio's paper
#define TO1      400.    //The same with Flavio's paper
#define TO2      6.      //The same with Flavio's paper
#define TSO1     30.0181 //The same with Flavio's paper
#define TSO2     0.9957  //The same with Flavio's paper

#define TSI      1.8875  // We have TSI1 and TSI2 = TSI in Flavio's paper
//#define TSI1     1.8875
//#define TSI2     1.8875


#define TWINF    0.07    //The same with Flavio's paper
#define THV      0.3     //EPUM // The same of Flavio's paper
#define THVM     0.006   //EPUQ // The same of Flavio's paper
#define THVINF   0.006   //EPUQ // The same of Flavio's paper
#define THW      0.13    //EPUP // The same of Flavio's paper
#define THWINF   0.006    //EPURR // In Flavio's paper 0.13
#define THSO     0.13    //EPUP // The same of Flavio's paper
#define THSI     0.13    //EPUP // The same of Flavio's paper
#define THO      0.006    //EPURR // The same of Flavio's paper
//#define KWP      5.7
#define KWM      65.     //The same of Flavio's paper
#define KS       2.0994  //The same of Flavio's paper
#define KSO      2.0458  //The same of Flavio's paper
//#define KSI      97.8
#define UWM      0.03    //The same of Flavio's paper
#define US       0.9087  //The same of Flavio's paper
#define UO       0.     // The same of Flavio's paper
#define UU       1.55   // The same of Flavio's paper
#define USO      0.65   // The same of Flavio's paper
#define SC       0.007
//#define WCP      0.15

#define WINFSTAR 0.94   // The same of Flavio's paper

#define TW2M_TW1M -45.0
#define TSO2_TSO1 -29.0224

#define TW2M_TW1M_DIVBY2 -22.5
#define TSO2_TSO1_DIVBY2 -14.5112


#define ATR_TVP        3.33 //The same with Flavio's paper
#define ATR_TV1M      19.2    //The same with Flavio's paper
#define ATR_TV2M      10.0  //The same with Flavio's paper

#define ATR_TWP      160.0   // We have TWP1 and TWP2 = TWP in Flavio's paper

#define ATR_TW1M      75.0    //The same with Flavio's paper
#define ATR_TW2M      75.0    //The same with Flavio's paper
#define ATR_TD         0.065
#define ATR_TSI       31.8364
#define ATR_TO        39.0
#define ATR_TA         0.009
#define ATR_UC         0.23
#define ATR_UV         0.055
#define ATR_UW         0.146
#define ATR_UO         0.0
#define ATR_UM         1.0
#define ATR_UCSI       0.8
#define ATR_USO        0.3

#define ATR_RSP        0.02
#define ATR_RSM        1.2
#define ATR_K          3.0
#define ATR_ASO        0.115
#define ATR_BSO        0.84
#define ATR_CSO        0.02
