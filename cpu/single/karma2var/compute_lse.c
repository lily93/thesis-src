#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../header/model.h"

#define TAUE          2.5
#define TAUN        250
#define ONE_O_TAUE    0.4
#define ONE_O_TAUN    0.004
#define EH            3
#define EN            1
#define ESTAR         0.8 //1.5415
#define EPS           0.01               //TAUE/TAUN
#define RE            1.204              //VARIED BETWEEN 0.5 AND 1.4
#define EXPRE         0.299991841        //EXP(-RE)
#define M            10
#define GAMMA         0.0011
#define ONE_O_ONE_MINUS_EXPRE   1.428554778


void cstep_karma(double * u, double * v, double dt) {
 	  
	      
	    double  dfunc = pow((*v), 10);
	    double  rfunc = 1.428554778 - (*v);
	    double  hfunc = (1-tanh((*u)-EH)) * (*u) * (*u) * 0.5;
	   double   ffunc = -(*u) + (ESTAR-dfunc) * hfunc;
	      //gfunc = rfunc * (e[i] - EN > 0) - (1 - (e[i] -EN>0)) * n[i];
	     double h = ((*u) > EN);
	      //(a -n)*h  - (1-h)*n
	      //a*h -n*h  - (n - n*h)
	    double  gfunc = 1.428554778*h - (*v); 
	    double  de    = ffunc * ONE_O_TAUE;
	     double dn    = gfunc * ONE_O_TAUN;
	      
          (*u)       = (*u) + dt*de;
         (*v)  = (*v) + dt*dn;

          
}

double compute_lse_cepi (double dt)
{

      double t_sim_time      = 1000.0; //in ms
      
      
      double t0_dt           = dt;
      int  t0_num_element  = (int)floor(t_sim_time/t0_dt + 0.5); 
      
      double * t0_result     = (double *) calloc (t0_num_element+1, sizeof(double));
      
      double   t0_u          = 1.5;
      double   t0_v          = 0.0;
	  
	  double   t0_t1         = 0.0;
	  double   t0_t2         = 0.0;
	  
	  double   t0_stim       = 0.0;
	  
	  t0_result[0]         = t0_u;
	  
	  //printf("%f\t%f\n", t0_t1, t0_u);
	  
	  for (int i=1; i <= t0_num_element; i++)
	  {
	      
	       cstep_karma (&t0_u, &t0_v, t0_dt);
	       t0_result[i]  = t0_u;
	       //printf("%f\t%f\n", t0_t1, t0_u);
	  }
	  
	  
	  double t1_dt           = dt/2;
      int  t1_num_element  = (int)floor(t_sim_time/t1_dt + 0.5); 
      
      double * t1_result     = (double *) calloc (t1_num_element+1, sizeof(double));
      
      double   t1_u          = 1.5;
      double   t1_v          = 0.0;
	  
	  double   t1_t1         = 0.0;
	  double   t1_t2         = 0.0;
	  
	  double   t1_stim       = 0.0;
	  
	  t1_result[0]         = t1_u;
	  
	  //printf("%f\t%f\n", t1_t1, t1_u);
	  
	   for (int i=1; i <= t1_num_element; i++)
	  {
	       cstep_karma (&t1_u, &t1_v,t1_dt);
	       t1_result[i]  = t1_u;
	       //printf("%f\t%f\n", t1_t1, t1_u);
	  }
	  
	  double lse = 0.0;
	  
	  for (int i=0; i < t0_num_element; i++)
	  {
	       lse += pow(((t0_result[i]*dt) - (t1_result[i*2]* dt/2 + t1_result[i*2+1] * dt/2)),2); 
	  }
	  
	  printf("dt=%f vs dt=%f lse = %g\n", dt, dt/2, lse);
	  
	  free(t0_result);
	  free(t1_result);
	  
	  return lse;

}

int main()
{

	  
	  compute_lse_cepi (0.1);
      compute_lse_cepi (0.1/2.0);
	  compute_lse_cepi (0.1/4.0);
	  compute_lse_cepi (0.1/8.0);
	  compute_lse_cepi (0.1/16.0);
	  compute_lse_cepi (0.1/32.0);
	  compute_lse_cepi (0.1/64.0);
	    
	return 0;
}

