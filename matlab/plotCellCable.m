res_path = './results/';
params = importdata('./params/params.dat');
time = params(1);
dt = 0.0125;% params(2);
dx = params(3);
nx = 256;
precision = 1;
spiral = params(6);
length = dx * nx;

maxeler_file = sprintf('%scable_32.csv', res_path);%, precision, nx, time, spiral);
cpu_file = sprintf('%scable_cpu.csv', res_path); %, precision, nx, time, spiral);
%matlab_file =sprintf('%sm_cellcable_out_%d_%d_%d.csv', res_path, nx, time,spiral);

simTime = round(time/dt);

 cpu = importdata(cpu_file);
 cpu_data = cpu; % cpu.data;
 
 
% matlab = importdata(matlab_file);
% matlab_data = matlab.data;

maxeler = importdata(maxeler_file);
maxeler_data = maxeler.data;

maxeler_u = reshape(maxeler_data(:,1), [nx,simTime]);
maxeler_v = reshape(maxeler_data(:,2), [nx, simTime]);
maxeler_w = reshape(maxeler_data(:,3), [nx, simTime]);
maxeler_s = reshape(maxeler_data(:,4), [nx, simTime]);
maxeler_stim = reshape(maxeler_data(:,5), [nx, simTime]);

% matlab_u =reshape(matlab_data(:,1), [nx,simTime]);
% matlab_v = reshape(matlab_data(:,2), [nx,simTime]);
% matlab_w = reshape(matlab_data(:,3), [nx,simTime]);
% matlab_s = reshape(matlab_data(:,4), [nx,simTime]);
% matlab_stim = reshape(matlab_data(:,5), [nx,simTime]);
% 
 cpu_u =reshape(cpu_data(:,1), [nx,simTime]);
% cpu_v = reshape(cpu_data(:,2), [nx,simTime]);
% cpu_w = reshape(cpu_data(:,3), [nx,simTime]);
% cpu_s = reshape(cpu_data(:,4), [nx,simTime]);
% cpu_stim = reshape(cpu_data(:,5), [nx,simTime]);

% maximum = 1.5;
% minimum = 0.0;
% t = matlab_u(:,1);

error = 0;
error_per_cell = zeros(nx,1);
iter = zeros(nx, 1);
A = (cpu_u(1,:) - maxeler_u(1, :)).^2;
sum(A);
for i=1:nx
error_per_cell(i) = sum((cpu_u(i,:) - maxeler_u(i, :)).^2);
iter(i) = i;
    for(j=1:simTime)
     error = error + (cpu_u(i,j) - maxeler_u(i,j))^2;
     
    end
end

for i=1:nx
     
figure
hold on 
plot(linspace(0,2,160), maxeler_u(i,:));
end
hold off
% 
% 
% for i=1:nx
% figure
% subplot(1,2,1)
%     ph_matlab = plot(linspace(0,1000, 20000), [matlab_u(i,:); matlab_v(i,:); matlab_w(i,:); matlab_s(i,:); matlab_stim(i,:)]);
%          legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
%          xlabel('Time in milliseconds');
%          title (['Matlab: Minimal Resistor Model Cell #' num2str(i)]);
%          
%          subplot(1,2,2)
%     ph_maxeler = plot(linspace(0,1000, 20000), [maxeler_u(i,:); maxeler_v(i,:); maxeler_w(i,:); maxeler_s(i,:); maxeler_stim(1,:)]);
%          legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
%          xlabel('Time in milliseconds');
%          title (['Maxeler: Minimal Resistor Model Cell #' num2str(i)]);
% 
% end
% 
% 
% figure
% for i=1:nx
%   
%    
%    axi = subplot(nx,1,i)
% hold on
%       ph = plot(axi,linspace(0, 1000, 20000), matlab_u(i,:),'Linewidth', 1.0,'DisplayName', sprintf('u in cell #%d', i));
%      ph2 =  plot(axi,linspace(0, 1000, 20000), matlab_stim(i,:),'Linewidth', 1.0,'DisplayName', sprintf('stimulus in cell #%d', i));
%        ph3 =  plot(axi,linspace(0, 1000, 20000), matlab_v(i,:),'Linewidth', 1.0,'DisplayName', sprintf('v in cell #%d', i));
%   ph4 =  plot(axi,linspace(0, 1000, 20000), matlab_w(i,:),'Linewidth', 1.0,'DisplayName', sprintf('w in cell #%d', i));
%     ph5=  plot(axi,linspace(0, 1000, 20000), matlab_s(i,:),'Linewidth', 1.0,'DisplayName', sprintf('s in cell #%d', i));   
%     legend('-DynamicLegend');
% hold off
% title('Matlab: Minimal Resister Model 5 cells');
%   xlabel('Time in milliseconds');
%      ylabel('current in mV');
% 
% end

% y=[];
% for(i=1:nx)
% if(i==1)
% y(i) = 0;
% else
%     y(i) = i*dx;
% end
% end

% x1 = cpu_u';
% length = nx*dx;
 x = linspace(0,time,simTime);
 y = linspace(0,length,nx);
% 
% pStim = reshape(cpu_stim, (simTime*nx), 1);


% %z = maxeler_u';
figure
subplot(3,1,1)
surf(x, y,cpu_u);
view([0 90]);
colormap(parula)
shading interp
colorbar
title('CPU: Minimal Resister Model cells');
ylabel('Length of the Cable (CM)');
xlabel('Time (MS)');
% 
% subplot(3,1,2)
% surf(x, y,matlab_u);
% view([0 90]);
% colormap(parula)
% shading interp
% colorbar
% title('Matlab: Minimal Resister Model cells');
% ylabel('Length of the Cable (CM)');
% xlabel('Time (MS)');

subplot(3,1,3)
surf(x, y,maxeler_u);
view([0 90]);
colormap(parula)
shading interp
colorbar
title('Maxeler: Minimal Resister Model cells');
ylabel('Length of the Cable (CM)');
xlabel('Time (MS)');








% subplot(1,3,2)
% mesh(x,y,z);
% view([0 90]);
% colorbar
% title('Maxeler: Minimal Resister Model 5 cells');
% xlabel('Length of the Cable');
% ylabel('Time in Miliseconds');

% subplot(1,3,3)
% mesh(x,y,matlab_u');
% view([0 90]);
% colorbar
% title('Matlab: Minimal Resister Model 5 cells');
% xlabel('Length of the Cable');
% ylabel('Time in Miliseconds');

% figure
%  
% for(i=1:nx)
%     p = 
%    axi2 = subplot(nx,1,i)
% hold on
%      ph = plot(axi2,linspace(0, 1000, 20000), maxeler_u(i,:),'Linewidth', 1.0,'DisplayName', sprintf('u in cell #%d', i));
%      ph2 =  plot(axi2,linspace(0, 1000, 20000), maxeler_stim(i,:),'Linewidth', 1.0,'DisplayName', sprintf('stimulus in cell #%d', i));
%        ph3 =  plot(axi2,linspace(0, 1000, 20000), maxeler_v(i,:),'Linewidth', 1.0,'DisplayName', sprintf('v in cell #%d', i));
%   ph4 =  plot(axi2,linspace(0, 1000, 20000), maxeler_w(i,:),'Linewidth', 1.0,'DisplayName', sprintf('w in cell #%d', i));
%     ph5=  plot(axi2,linspace(0, 1000, 20000), maxeler_s(i,:),'Linewidth', 1.0,'DisplayName', sprintf('s in cell #%d', i));   
%   legend('-DynamicLegend');
% hold off
% 
%  
% end 

  
        
      
 
 
 


