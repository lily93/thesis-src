#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double dt = 0.05;
const int simTime = 20000;
const int number_of_cells = 50;
const double ddt_o_dx2 = 0.02;
const int stimDur = 5;



const float EPI_TVP   =     1.4506;
  const float EPI_TV1M  =    60.;
 const float  EPI_TV2M  =  1150.;

 const float  EPI_TWP   =   200.0;

 const float  EPI_TW1M  =    60.0;
 const float  EPI_TW2M  =    15.;

 const float  EPI_TS1   =    2.7342;
  const float EPI_TS2   =   16.;
  const float EPI_TFI   =    0.11;
  const float EPI_TO1   =  400.;    //The same with Flavio's paper
 const float  EPI_TO2   =    6.  ;    //The same with Flavio's paper
 const float  EPI_TSO1  =   30.0181; //The same with Flavio's paper
 const float  EPI_TSO2  =    0.9957;  //The same with Flavio's paper

 const float  EPI_TSI   =    1.8875;  // We have TSI1 and TSI2 = TSI in Flavio's paper


  const float EPI_TWINF  =  0.07;
 const float  EPI_THV    =  0.3;     //EPUM % The same of Flavio's paper
  const float EPI_THVM   =  0.006;   //EPUQ % The same of Flavio's paper
 const float  EPI_THVINF =  0.006;  // %EPUQ % The same of Flavio's paper
 const float  EPI_THW    =  0.13;   // %EPUP % The same of Flavio's paper
 const float  EPI_THWINF =  0.006;  //  %EPURR % In Flavio's paper 0.13
 const float  EPI_THSO   =  0.13;   // %EPUP % The same of Flavio's paper
 const float  EPI_THSI   =  0.13;    //%EPUP % The same of Flavio's paper
 const float  EPI_THO    =  0.006;   // %EPURR % The same of Flavio's paper

 const float  EPI_KWM    =  65.;    // %The same of Flavio's paper
 const float  EPI_KS     =  2.0994; // %The same of Flavio's paper
 const float  EPI_KSO    =  2.0458;  //%The same of Flavio's paper

const float   EPI_UWM    =  0.03;   // %The same of Flavio's paper
const float   EPI_US     =  0.9087; // %The same of Flavio's paper
const float   EPI_UO     =  0.;     //% The same of Flavio's paper
 const float  EPI_UU     =  1.55;  // % The same of Flavio's paper
const float   EPI_USO    =  0.65;  // % The same of Flavio's paper

void printSingle(FILE * fp, double * u_out, double  * v_out, double * w_out, double * s_out , double * stim_out) {


		if(fp==NULL) {
			printf("no file found");
		}
		else {
			printf("u;v;w;s;stim\n");


			fprintf(fp,"u;v;w;s;stim\n");
			for (int i = 0; i<simTime; i++) {

	printf("%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
	fprintf(fp,"%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);

}
			fclose(fp);
			fp = NULL;
	}
}

double heavisidefun(double x) {
	if(x==0) {
		return 0.5;
	}
	if (x<0) {
		return 0.0;
	} if (x> 0){
		return 1.0;
	}
}

void take_step(double *stim_in, const int time,int stimNum, int * stimTimes) {

	int sizeBytes = (stimNum) * sizeof(double);
	double t1 = 0.0;
	double t2 = 0.0;



	for(int i = 0; i<time; i++){


			t1+=dt;
			t2+=dt;

			for(int j = 0; j<stimNum; j++){
			double c1 = t1-stimTimes[j];
			double c2 = t2-(stimTimes[j]+1);
			stim_in[i] += heavisidefun(c1) * (1 - heavisidefun(c2));
			}

	}



}

void runCPUSingle() {
	int  sizeBytes = (simTime) * sizeof(double);
	int simTimesControlled[3] = {0 , 300, 700};
	double * uRes = malloc(sizeBytes);
	double * vRes = malloc(sizeBytes);
	double * wRes = malloc(sizeBytes);
	double * sRes =  malloc(sizeBytes);
	double * stim_in = malloc(sizeBytes);


	float jfi;
	float jso;
	float jsi;
	float u = 0;
	float v = 1;
	float w = 1;
	float s = 0;
	float stim;


	take_step(stim_in,simTime,3,simTimesControlled);

	for(int i = 0;i < simTime; i ++) {

		stim = stim_in[i];


	    if (u < 0.006) {
	              w = w + ((1.0 -(u/EPI_TWINF) - w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*(u-EPI_UWM)))))*dt;

	              v = v + ((1.0-v)/EPI_TV1M)*dt;
	              s = s + ((((1.+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS1)*dt;
	              jfi = 0.0;
	              jso = u/EPI_TO1;
	              jsi = 0.0;
	    }

	         else if (u < 0.13) {
	              w = w + ((0.94-w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*(u-EPI_UWM)))))*dt;
	              v = v + (-v/EPI_TV2M)*dt;
	              s = s +((((1.+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS1)*dt;
	              jfi = 0.0;
	              jso = u/EPI_TO2;
	              jsi = 0.0;
	         }

	         else if (u < 0.3){
	              w = w + (-w/EPI_TWP)*dt;
	              v = v + (-v/EPI_TV2M)*dt;
	              s = s + ((((1.+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS2)*dt;

	              jfi = 0.0;
	              jso = 1./(EPI_TSO1+((EPI_TSO2-EPI_TSO1)*(1.+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5);
	              jsi = -w * s/EPI_TSI;
	         }  else {
	              w  = w + (-w/EPI_TWP)*dt;
	              v  = v + (-v/EPI_TVP)*dt;
	              s  = s +((((1.+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS2)*dt;

	              jfi = -v * (u - EPI_THV) * (EPI_UU - u)/EPI_TFI;
	              jso = 1./(EPI_TSO1+((EPI_TSO2 - EPI_TSO1)*(1.+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5);
	              jsi = -w * s/EPI_TSI;
	}
	    u = u  - (jfi+jso+jsi-stim)*dt;

	    uRes[i] = u;
	    vRes[i] = v;
	    sRes[i] = s;
	    wRes[i] = w;

	}

	FILE *fp = fopen("single_cell_out_cpu.csv", "w");
	printSingle(fp, uRes, vRes, wRes, sRes,stim_in);

	
}


void printCable(FILE * fp, float * u_out, float * v_out, float * s_out, float * w_out, float * stim_out) {
	int ticks = number_of_cells * simTime;

	if(fp==NULL) {
		printf("no file found");
	}
	else {
		printf("u;v;w;s;stim\n");

		fprintf(fp,"u;v;w;s;stim\n");

for (int i = 0;i<ticks;i++) {
if(i%number_of_cells==0) {
	printf("\n");


}
printf("%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);
fprintf(fp,"%f;%f;%f;%f;%f\n", u_out[i], v_out[i], w_out[i], s_out[i], stim_out[i]);



}
fclose(fp);
fp = NULL;
	}
}

void initValues(float * u, float * v, float * w, float * s) {
	for(int i = 0; i<number_of_cells; i++) {
		u[i] = 0.0;
		v[i] = 1.0;
		w[i] = 1.0;
		s[i] = 0.0;

	}
}

void runCPUCable() {

	int sizeBytesCells = number_of_cells * sizeof(float);
	int sizeBytes = (number_of_cells * simTime) * sizeof(float);
	float * uCable = malloc(sizeBytesCells);
	float * vCable = malloc(sizeBytesCells);
	float * wCable = malloc(sizeBytesCells);
	float * sCable = malloc(sizeBytesCells);
	
	float * resU = malloc(sizeBytes);
	float * resV = malloc(sizeBytes);
	float * resW = malloc(sizeBytes);
	float * resS = malloc(sizeBytes);
	float * resStim = malloc(sizeBytes);

	initValues(uCable, vCable, wCable, sCable);


	float uCurr , wCurr, vCurr, sCurr;

	float uNext, wNext, vNext, sNext;

	float jfi;
		float jso;
		float jsi;

	float lap;

	float stim = 0;
	float t = 0;
	int elementCounter = 0;
	float t1 = 0;
	float t2 = 0;
	


	  for (int i=0; i<simTime; i++) {
		  	t1 += dt;
			t2 += dt;
			int j = 0;


		 	stim= heavisidefun(t1 - 0)*(1 - heavisidefun(t2 - 1)) + heavisidefun(t1 - 300)*(1 - heavisidefun(t2 - 301)) + heavisidefun(t1 - 700)*(1 - heavisidefun(t2 - 701));


	            while(j<number_of_cells) {


	            	   uCurr = uCable[j];
	            	   vCurr = vCable[j];
	            	   sCurr = sCable[j];
	            	   wCurr = wCable[j];
	            	 float  prev = uCable[j-1];
	            	 float  next = uCable[j+1];

	                if(j==0) {
	                    lap = ddt_o_dx2 * (-2*uCurr + 2*next);
	                } else if(j==number_of_cells) {
	                    lap = ddt_o_dx2 * (-2*uCurr +2*prev);
	                }else {
	                  lap = ddt_o_dx2 * (next + prev - 2.0 *uCurr);
	                }

	                if(stim > 0) {
	                    if(j<stimDur) {
	                    t = 0.66;
	                    }       	 else {
	                        t= 0;
	              		  }
	                }

	                if (uCurr < 0.006) {

	                              wNext = wCurr + ((1.0 -(uCurr/EPI_TWINF) - wCurr)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1+tanh(EPI_KWM*(uCurr-EPI_UWM)))))*dt;
	                            vNext = vCurr +((1.0-vCurr)/EPI_TV1M)*dt;
	                              sNext = sCurr + ((((1+tanh(EPI_KS*(uCurr - EPI_US))) * 0.5) - sCurr)/EPI_TS1)*dt;
	                              jfi = 0.0;
	                              jso = uCurr/EPI_TO1;
	                              jsi = 0.0;

	                } else if (uCurr < 0.13) {
	                             wNext = wCurr+ ((0.94-wCurr)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1+tanh(EPI_KWM*(uCurr-EPI_UWM)))))*dt;
	                             vNext = vCurr+ (-vCurr/EPI_TV2M)*dt;
	                             sNext = sCurr +((((1+tanh(EPI_KS*(uCurr-EPI_US))) * 0.5) - sCurr)/EPI_TS1)*dt;
	                              jfi = 0.0;
	                              jso = uCurr/EPI_TO2;
	                              jsi = 0.0;

	                } else if (uCurr < 0.3) {
	                              wNext = wCurr + (-wCurr/EPI_TWP)*dt;
	                             vNext = vCurr + (-vCurr/EPI_TV2M)*dt;
	                              sNext = sCurr + ((((1+tanh(EPI_KS*(uCurr-EPI_US))) * 0.5) - sCurr)/EPI_TS2)*dt;
	                              jfi = 0.0;
	                              jso = 1/(EPI_TSO1+((EPI_TSO2-EPI_TSO1)*(1+tanh(EPI_KSO*(uCurr - EPI_USO)))) * 0.5);
	                              jsi = -wCurr * sCurr/EPI_TSI;
	                  } else {
	                              wNext  = wCurr + (-wCurr/EPI_TWP)*dt;
	                             vNext  = vCurr + (-vCurr/EPI_TVP)*dt;
	                              sNext  = sCurr + ((((1+tanh(EPI_KS*(uCurr - EPI_US))) * 0.5) - sCurr)/EPI_TS2)*dt;

	                              jfi = -vCurr * (uCurr - EPI_THV) * (EPI_UU - uCurr)/EPI_TFI;
	                              jso = 1/(EPI_TSO1+((EPI_TSO2 - EPI_TSO1)*(1+tanh(EPI_KSO*(uCurr - EPI_USO)))) * 0.5);
	                              jsi = -wCurr * sCurr/EPI_TSI;
	                  }

	                         uNext =  uCurr - (jfi+jso+jsi-t)*dt+lap;

	                         uCable[j] = uNext;
	                         wCable[j] = wNext;
	                         sCable[j] = sNext;
	                         vCable[j] = vNext;

	                    resU[elementCounter] = uNext;
	                    resV[elementCounter] = vNext;
	                    resW[elementCounter] = wNext;
	                    resS[elementCounter] = sNext;
	                    resStim[elementCounter] = t;
	                    elementCounter++;
			    j++;
   			 
	            }
	                


	            }
				
				FILE *fp = fopen("cell_cable_out_cpu.csv", "w");
				printCable(fp, resU, resV, resS, resW,resStim);

free(resU);
resU = NULL;

free(resV);
resV = NULL;

free(resW);
resW = NULL;

free(resS);
resS = NULL;

free(resStim);
resStim = NULL;




}


int main()
{

printf("start..\n");
	runCPUSingle();
	runCPUCable();

printf("done\n");
	
}
