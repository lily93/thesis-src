delimiterIn = ';';
headerlinesIn = 1;
res_path = ['.' filesep 'results' filesep];
par_path = sprintf('%sparams_single.dat', res_path);
params = importdata(par_path);
simTime = params(1);
dt = params(2);
mode = params(3);
precicion = 1;

maxeler_file = sprintf('%ssinglecell_out_maxeler_test.csv', res_path);%, precicion, mode, simTime);
matlab_file = sprintf('%sm_single_cell_out_%d_2_%d.csv', res_path, mode, simTime);
cpu_file = sprintf('%ssinglecell_out_cpu_%d_%d_%d.csv', res_path, precicion, mode, simTime);

maxeler = importdata(maxeler_file);%, delimiterIn, headerlinesIn);
matlab = importdata(matlab_file);%, delimiterIn, headerlinesIn);
cpu = importdata(cpu_file); %, delimiterIn, headerlinesIn);

maxeler_data = maxeler.data;
matlab_data = matlab.data;
cpu_data = cpu;% cpu.data;

maxeler_u = maxeler_data(:,1);
maxeler_v = maxeler_data(:,2);
maxeler_w = maxeler_data(:,3);
maxeler_s = maxeler_data(:,4);
maxeler_stim = maxeler_data(:,5);


matlab_u = matlab_data(:,1);
matlab_v = matlab_data(:,2);
matlab_w = matlab_data(:,3);
matlab_s = matlab_data(:,4);
matlab_stim = matlab_data(:,5);

cpu_u = cpu_data(:,1);
cpu_v = cpu_data(:,2);
cpu_w = cpu_data(:,3);
cpu_s = cpu_data(:,4);
cpu_stim = cpu_data(:,5);


subplot(1,3,1)
    ph_matlab = plot(linspace(0,1000, 20000), [matlab_u'; matlab_v'; matlab_w'; matlab_s'; matlab_stim']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('Matlab: Minimal Resistor Model');
         
         subplot(1,3,2)
    ph_maxeler = plot(linspace(0,1000, 20000), [maxeler_u'; maxeler_v'; maxeler_w'; maxeler_s'; maxeler_stim']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('Maxeler: Minimal Resistor Model');
         
            subplot(1,3,3)
    ph_cpu = plot(linspace(0,1000, 20000), [cpu_u'; cpu_v'; cpu_w'; cpu_s'; cpu_stim']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('CPU: Minimal Resistor Model');

