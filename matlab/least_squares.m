function [err] = least_squares(x,y) 
residuals = x-y;
squared = residuals.^2;

err= sum(squared);

end