% ===============================================================
% ==   Minimal Resistor Model (4 state variables)              ==
% ==                                                           ==
% ==   Supplement for the CMACS Workshop                       ==
% ==                                                           ==
% ==                                                           ==
% ==   Author:                                                 ==
% ==                                                           ==
% ==     E. Bartocci                                           ==
% ==                                                           == 
% ==   Date:  11/05/10                                         ==
% ==                                                           ==
% ==   Free distribution with authors permission               ==
% ==                                                           ==
% ==   SUNY Stony Brook, Stony Brook, NY                       ==
% ==                                                           == 
% ===============================================================  

% The following are the parameters that you can find in the paper 
% A. Bueno-Orovio, M. Cherry, and F. Fenton, ?Minimal model for 
% human ventricular action potentials in tissue,? Journal of 
% Theoretical Biology, no. 253, pp. 544?560, 2008. 


function [out] = single_cell_mrm4V ()
        format long g
         global EPI_TVP; 
         global EPI_TV1M;  
         global EPI_TV2M; 

         global EPI_TWP;   

         global EPI_TW1M; %190    
         global EPI_TW2M;
         
         global EPI_TS1;
         global EPI_TS2;
         global EPI_TFI;
         global EPI_TO1;
         global EPI_TO2;
         global EPI_TSO1;
         global EPI_TSO2;

         global EPI_TSI;


         global EPI_TWINF;
         global EPI_THV;

         global EPI_KWM;
         global EPI_KS;
         global EPI_KSO;  
         global EPI_UWM;    
         global EPI_US;    
         global EPI_UU;  
         global EPI_USO;    

         EPI_TVP   =     1.4506; 
         EPI_TV1M  =    60.;  
         EPI_TV2M  =  1150.; 

         EPI_TWP   =   200.0;   

         EPI_TW1M  =    60.0;     
         EPI_TW2M  =    15.;  
         
         EPI_TS1   =    2.7342;  
         EPI_TS2   =   16.;     %The same with Flavio's paper
         EPI_TFI   =    0.11;    %The same with Flavio's paper
         EPI_TO1   =  400.;    %The same with Flavio's paper
         EPI_TO2   =    6.  ;    %The same with Flavio's paper
         EPI_TSO1  =   30.0181; %The same with Flavio's paper
         EPI_TSO2  =    0.9957;  %The same with Flavio's paper

         EPI_TSI   =    1.8875;  % We have TSI1 and TSI2 = TSI in Flavio's paper


         EPI_TWINF  =  0.07;    %The same with Flavio's paper
         EPI_THV    =  0.3;     %EPUM % The same of Flavio's paper
         EPI_THVM   =  0.006;   %EPUQ % The same of Flavio's paper
         EPI_THVINF =  0.006;   %EPUQ % The same of Flavio's paper
         EPI_THW    =  0.13;    %EPUP % The same of Flavio's paper
         EPI_THWINF =  0.006;    %EPURR % In Flavio's paper 0.13
         EPI_THSO   =  0.13;    %EPUP % The same of Flavio's paper
         EPI_THSI   =  0.13;    %EPUP % The same of Flavio's paper
         EPI_THO    =  0.006;    %EPURR % The same of Flavio's paper

         EPI_KWM    =  65.;     %The same of Flavio's paper
         EPI_KS     =  2.0994;  %The same of Flavio's paper
         EPI_KSO    =  2.0458;  %The same of Flavio's paper
         
         EPI_UWM    =  0.03;    %The same of Flavio's paper
         EPI_US     =  0.9087;  %The same of Flavio's paper
         EPI_UO     =  0.;     % The same of Flavio's paper
         EPI_UU     =  1.55;   % The same of Flavio's paper
         EPI_USO    =  0.65;   % The same of Flavio's paper
         

         ut1     = [];
         vt1     = [];
         wt1     = [];
         st1     = [];
        stim1 = [];
        
 res_path = ['.' filesep 'results' filesep];
par_path = sprintf('%sparams.dat', res_path);
params = importdata(par_path);
time = params(1);
dt = params(2);
dx = params(3);
diff = params(4);
nx = params(5);
spiral = params(6);



outfile = sprintf('%sm_cellcable_out_%d_%d_%d.csv',res_path, nx, time, spiral);
         
   
         simTime = time/dt;
         vCable = ones(nx,1);
         uCable = zeros(nx,1);
         wCable = ones(nx,1);
         sCable = zeros(nx,1);
         lap = 0;
          ddt_o_dx2 = dt*diff/(dx * dx);
               
        stimTimesControl = [0, 300, 700];
       
        stimDur = 5;
         stim =  takeStep(dt,stimTimesControl, simTime);
        stimVal = zeros([nx, 1]);
        t = 0;
         for i=1:simTime  
        
        
            for j=1:nx
                if(j==1)
                    lap = ddt_o_dx2 * (-2.*uCable(j) + 2.*uCable(j+1));
                elseif(j==nx)
                    lap = ddt_o_dx2 * (-2.*uCable(j) +2.*uCable(j-1));
                else
                  lap = ddt_o_dx2 * (uCable(j+1) + uCable(j-1) - 2.0 *uCable(j));
                end
                if(stim(i) > 0)
                    if(j<=stimDur)
                    t = 0.66;
                    else
                        t= 0;
                    end
                end
                    
               [uCable(j), vCable(j), wCable(j), sCable(j), stimVal(j)] = nextStepNonlinear (uCable(j),vCable(j),wCable(j),sCable(j), t, stimVal(j), dt, lap, j,stimDur)
             
            end
             
                  
                  
             ut1(:,i) = uCable;
             vt1(:,i)= vCable;
             wt1(:,i) = wCable;
             st1(:,i) = sCable;
             stim1(:,i) = stimVal;
           %  fprintf('u: %f\n v: %f\n w: %f\n s: %f\n stim: %f\n-------------------------%d------------------\n', u, v,w,s,stim,i); 
            
         end
         
         fileID = fopen(outfile,'w');
         	fprintf(fileID,'U Values;V Values;W Values;S Values;Stimulus\n');
            elements = simTime*nx;
            s =  zeros([nx,simTime]);
            for(c=1:nx)
                s(c,:) = stim;
            end
           
            for ii = 1:elements
            if(mod(ii,nx)==0)
                fprintf(fileID,'\n');
            end
             	fprintf(fileID,'%f;%f;%f;%f;%f\n', ut1(ii), vt1(ii), wt1(ii), st1(ii), stim1(ii));
            end
    
 
         fclose(fileID);
   stop = 1;
       


     
end


function [stim] = takeStep(dt, stimTimes, simTime)
  stim = zeros([simTime,1]);
    
    t1 = 0;
    t2 = 0;
        for(i =1:simTime)
        t1=t1+dt;
        t2=t2+dt;
        for(tc = 1:size(stimTimes,2))
            c1 = t1 - stimTimes(tc);
            c2 = t2 - (stimTimes(tc)+1);
            stim(i) = stim(i) + heaviside(c1) * (1-heaviside(c2));
        end
    end
end

function [stim] = takeStep_generateStim (dt,stimTimes, simTime, nx, stimDur) 
    stim = zeros([nx,simTime]);
    s= zeros([simTime,1]);
    duration = stimDur+1;
    t1 = 0;
    t2 = 0;
    for(i =1:simTime)
        t1=t1+dt;
        t2=t2+dt;
        for(tc = 1:size(stimTimes,2))
            c1 = t1 - stimTimes(tc);
            c2 = t2 - (stimTimes(tc)+1);
            s(i) = s(i) + heaviside(c1) * (1-heaviside(c2));
        end
    end
 
    ci = 0;
for(i=1:size(s,1))
    for(c=1:nx)
        if(s(i)==1)
           if(c<duration)
               stim(ci) = 0.66;
           else
               stim(ci) = 0;
           end
           ci = ci+1;
        end
ci = ci+1;
    end
end

 end

            

            
                
   
        
    






function [u,v,w,s,stimVal] = nextStepNonlinear (u,v,w,s, stim,stimVal, dt, lap, cellNum, duration)
         global EPI_TVP; 
         global EPI_TV1M;  
         global EPI_TV2M; 

         global EPI_TWP;   

         global EPI_TW1M; %190    
         global EPI_TW2M;
         
         global EPI_TS1;
         global EPI_TS2;
         global EPI_TFI;
         global EPI_TO1;
         global EPI_TO2;
         global EPI_TSO1;
         global EPI_TSO2;

         global EPI_TSI;


         global EPI_TWINF;
         global EPI_THV;

         global EPI_KWM;
         global EPI_KS;
         global EPI_KSO;  
         global EPI_UWM;    
         global EPI_US;    
         global EPI_UU;  
         global EPI_USO;   


         %% Stimulating the cell at 0, 300, 700 milliseconds
        % t1 = t1 + dt;
       %  t2 = t2 + dt;
     
        stimVal = stim;
%             if(cellNum ==1)
%             stimVal = 1;
%             elseif(cellNum<duration)
%                 stimVal = stimVal*0.66;
%             else
%                 stimVal = 0;
%             end
      
        
      
         if u < 0.006
              w = w + ((1.0 -(u/EPI_TWINF) - w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*(u-EPI_UWM)))))*dt;     
              v = v + ((1.0-v)/EPI_TV1M)*dt;
              s = s + ((((1.+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS1)*dt;
              jfi = 0.0;
              jso = u/EPI_TO1;
              jsi = 0.0;  
              
         elseif u < 0.13
              w = w + ((0.94-w)/(EPI_TW1M + (EPI_TW2M - EPI_TW1M) * 0.5 * (1.+tanh(EPI_KWM*(u-EPI_UWM)))))*dt;
              v = v + (-v/EPI_TV2M)*dt;
              s = s +((((1.+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS1)*dt;
              jfi = 0.0;
              jso = u/EPI_TO2;
              jsi = 0.0;
         
         elseif u < 0.3
              w = w + (-w/EPI_TWP)*dt;
              v = v + (-v/EPI_TV2M)*dt;
              s = s + ((((1.+tanh(EPI_KS*(u-EPI_US))) * 0.5) - s)/EPI_TS2)*dt;
              jfi = 0.0;
              jso = 1./(EPI_TSO1+((EPI_TSO2-EPI_TSO1)*(1.+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5);
              jsi = -w * s/EPI_TSI;
         else
              w  = w + (-w/EPI_TWP)*dt;
              v  = v + (-v/EPI_TVP)*dt;
              s  = s +((((1.+tanh(EPI_KS*(u - EPI_US))) * 0.5) - s)/EPI_TS2)*dt;
                       
              jfi = -v * (u - EPI_THV) * (EPI_UU - u)/EPI_TFI;
              jso = 1./(EPI_TSO1+((EPI_TSO2 - EPI_TSO1)*(1.+tanh(EPI_KSO*(u - EPI_USO)))) * 0.5);
              jsi = -w * s/EPI_TSI;  
         end
        
         u = u  - (jfi+jso+jsi-stimVal)*dt+lap;
                 
end


