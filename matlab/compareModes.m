res_path = './results/';
paramfile = sprintf('%sparams_single.dat', res_path);
params = importdata(paramfile);

simTime = params(1);
dt = params(2);
mode = params(3);
num_iterations = simTime/dt;
precicion = 1;

cpuRandom = importdata('./results/singlecell_out_cpu_1_2_1000.csv');
cpuSpaced = importdata('./results/singlecell_out_cpu_1_3_1000.csv');
cpuControl = importdata('./results/singlecell_out_cpu_1_1_1000.csv');

maxelerRandom = importdata('./results/singlecell_out_maxeler_1_2_1000.csv');
maxelerSpaced = importdata('./results/singlecell_out_maxeler_1_3_1000.csv');
maxelerControl = importdata('./results/singlecell_out_maxeler_1_1_1000.csv');

cpu_u_n = cpuControl(:,1);
cpu_v_n = cpuControl(:,2);
cpu_w_n = cpuControl(:,3);
cpu_s_n = cpuControl(:,4);
cpu_stim_n = cpuControl(:,5);

cpu_u_r = cpuRandom(:,1);
cpu_v_r = cpuRandom(:,2);
cpu_w_r = cpuRandom(:,3);
cpu_s_r = cpuRandom(:,4);
cpu_stim_r = cpuRandom(:,5);

cpu_u_s = cpuSpaced(:,1);
cpu_v_s = cpuSpaced(:,2);
cpu_w_s = cpuSpaced(:,3);
cpu_s_s = cpuSpaced(:,4);
cpu_stim_s = cpuSpaced(:,5);


maxeler_u_n = maxelerControl.data(:,1);
maxeler_v_n = maxelerControl.data(:,2);
maxeler_w_n = maxelerControl.data(:,3);
maxeler_s_n = maxelerControl.data(:,4);
maxeler_stim_n = maxelerControl.data(:,5);

maxeler_u_r = maxelerRandom.data(:,1);
maxeler_v_r = maxelerRandom.data(:,2);
maxeler_w_r = maxelerRandom.data(:,3);
maxeler_s_r = maxelerRandom.data(:,4);
maxeler_stim_r = maxelerRandom.data(:,5);

maxeler_u_s = maxelerSpaced.data(:,1);
maxeler_v_s = maxelerSpaced.data(:,2);
maxeler_w_s = maxelerSpaced.data(:,3);
maxeler_s_s = maxelerSpaced.data(:,4);
maxeler_stim_s = maxelerSpaced.data(:,5);

figure
subplot(1,3,1)
    ph_natcpu = plot(linspace(0,simTime, num_iterations), [cpu_u_n'; cpu_v_n'; cpu_w_n'; cpu_s_n'; cpu_stim_n']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('CPU Orig');
         
         subplot(1,3,2)
    ph_rancpu = plot(linspace(0,simTime, num_iterations), [cpu_u_r'; cpu_v_r'; cpu_w_r'; cpu_s_r'; cpu_stim_r']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('CPU Random');
         
            subplot(1,3,3)
    ph_spacecpu = plot(linspace(0,simTime, num_iterations), [cpu_u_s'; cpu_v_s'; cpu_w_s'; cpu_s_s'; cpu_stim_s']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('CPU Evenly Spaced (400ms)');
         
         figure
subplot(1,3,1)
    ph_nat = plot(linspace(0,simTime, num_iterations), [maxeler_u_n'; maxeler_v_n'; maxeler_w_n'; maxeler_s_n'; maxeler_stim_n']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('Maxeler Orig');
         
         subplot(1,3,2)
    ph_ran = plot(linspace(0,simTime, num_iterations), [maxeler_u_r'; maxeler_v_r'; maxeler_w_r'; maxeler_s_r'; maxeler_stim_r']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('Maxeler Random');
         
            subplot(1,3,3)
    ph_space = plot(linspace(0,simTime, num_iterations), [maxeler_u_s'; maxeler_v_s'; maxeler_w_s'; maxeler_s_s'; maxeler_stim_s']);
         legend('u state variable', 'v state variable', 'w state variable','s state variable', 'stimulus');
         xlabel('Time in milliseconds');
         title ('Maxeler Evenly Spaced (400ms)');
