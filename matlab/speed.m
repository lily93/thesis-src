clear all
dt = 0.0125;
cells = [];

maxeler32time1000 = [26462, 52927, 105859, 211718, 423382, 846828];
maxeler64time1000 = [43028, 86047, 172121, 344253, 688513];

cpu32times1000 = [2571, 4826, 9666, 19331, 38660, 77303];
cpu64times1000 = [2021, 3636, 7272, 14547, 29151, 58299];

gpu64times1000 = [2168.96, 2968.86, 4680.35, 6439.36, 10046.6, 17624];
gpu32times1000 = [2224.2, 2937.91, 4417.29, 6047.39, 9602.48, 16179];

all32 = [26462 2571 2168.96; 52927 4826 2937.91;105859 9666 4680.35; 211718 19331 6439.36; 423382 38660 10046.6; 846828 77303 17624];

    

i = 256;
c = 1;
while(i<10000) 
   cells(c) = i;
   c = c+1;
   i = i*2;
end
mc = cells(1:(end-1));
figure
hold on
%bar(cells, all32);
bar(cells, maxeler32time1000, 'r');
bar(cells, cpu32times1000, 'b');
bar(cells,gpu32times1000, 'g');
legend('Maxeler', 'CPU', 'GPU');
ylabel('Elapsed Time (MS)');
xlabel('Number of Cells');
set(gca, 'YScale', 'log')
%title('Single Precision Performance');
hold off

figure
hold on
%bar(cells, all32);
bar(mc, maxeler64time1000, 'r');
bar(cells, cpu64times1000, 'b');
bar(cells,gpu64times1000, 'g');
legend('Maxeler', 'CPU', 'GPU');
ylabel('Elapsed Time (MS)');
xlabel('Number of Cells');
set(gca, 'YScale', 'log')
%title('Double Precision Performance');
hold off

figure
hold on
%bar(cells, all32);
bar(mc, maxeler64time1000, 'r');
bar(cells, maxeler32time1000, 'b');

legend('Maxeler 64bit', 'Maxeler 32bit');
ylabel('Elapsed Time (MS)');
xlabel('Number of Cells');
set(gca, 'YScale', 'log')
%title('Double vs. Single Precision Performance Maxeler');
hold off


