delimiterIn = ';';
headerlinesIn = 1;
res_path = ['.' filesep 'results' filesep];
par_path = sprintf('%sparams_single.dat', res_path);
%params = importdata(par_path);
simTime = 1000;
dt = 0.1/64;
dt1 = (0.1/(2*2*2*2));
mode = 1;
precicion = 1;
iterations = simTime/dt;

karma0 = importdata('./results/karma_out_maxeler_0.001563_1000_20.csv');
karma1 = importdata('./results/karma_out_maxeler_0.001563_1000_32.csv');
 karma2 = importdata('./results/cpu_karma_cell_out_0.001563_1_1000.csv');
% 
beelerreuter0 = importdata('./results/beelerreuter_out_maxeler_0.001563_1000_20.csv');
beelerreuter1 = importdata('./results/beelerreuter_out_maxeler_0.001563_1000_32.csv');
beelerreuter2 = importdata('./results/cpu_beelerreuter_cell_out_0.001563_2_1000.csv');
% 
minimal0 = importdata('./results/minimal_cell_out_maxeler_0.001563_1000_20.csv');
minimal1 = importdata('./results/minimal_cell_out_maxeler_0.001563_1000_32.csv');
minimal2 = importdata('./results/cpu_single_cell_out_0.001563_1_1000.csv');


 uk0 = karma0(:,1);
 uk1 = karma1(:,1);
 uk2 = karma2(:,1);

 ub0 = beelerreuter0(:,1);
 ub1 = beelerreuter1(:,1);
 ub2 = beelerreuter2(:,1);

 um0 = minimal0(:,1);
um1 = minimal1(:,1);
um2 = minimal2(:,1);

% err = 0;
% for i=1:size(uk1,1)
%     err = err + (uk2(i)-(uk1(i)*uk1(i)))^2;
% end
% err = err/size(uk1,1);
% 
figure
subplot(3,1,1)
 ph_cpu = plot(linspace(0,simTime, iterations), [uk2, uk1, uk0]);
         xlabel('Time in milliseconds');
         title ('Karma Model');
         legend('CPU', 'Maxeler 32bit', 'Maxeler 20bit');
         
   subplot(3,1,2)
 ph_cpu = plot(linspace(0,simTime, iterations), [um1, um2, um0]);
         xlabel('Time in milliseconds');
         title ('Minimal Model');
         legend('Maxeler 32bit', 'CPU', 'Maxeler 20bit');
         
        
      subplot(3,1,3)
 ph_cpu = plot(linspace(0,simTime, iterations), [ub2, ub1, ub0]);
         xlabel('Time in milliseconds');
         title ('Beeler-Reuter Model');
         legend( 'CPU', 'Maxeler 32bit', 'Maxeler 20bit');



