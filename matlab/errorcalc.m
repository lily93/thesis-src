clear all
simTime = 1000;
res_path = ['.' filesep 'results' filesep];
%par_path = sprintf('%sparams_single.dat', res_path);
error_path_minimal = sprintf('%serror_minimal.dat', res_path);
error_path_karma = sprintf('%serror_karma.dat', res_path);
error_path_beelerreuter = sprintf('%serror_beelerreuter.dat', res_path);
%params = importdata(par_path);
errors_minimal = importdata(error_path_minimal);
errors_beelerreuter = importdata(error_path_beelerreuter);
errors_karma = importdata(error_path_karma);

dts_minimal = [];
dts_minimal(1) = 0.1;
for i = 1:size(errors_minimal)-1
dts_minimal(i+1) = 0.1/(2^i);
end

dts_karma = [];
dts_karma(1) = 0.1;
for i = 1:size(errors_karma)-1
dts_karma(i+1) = 0.1/(2^i);
end

dts_br = [];
dts = [];
dts_br(1) = 0.025;
for i = 1:size(errors_beelerreuter)-1
dts_br(i+1) = 0.025/(2^i);
end
ending = find(dts_karma==min(dts_karma));
start = find(dts_karma==0.025);

dts = dts_karma(start:ending);
errors_karma_trim = errors_karma(start:ending);
errors_minimal_trim = errors_minimal(start:ending);

% first_dt_karma = importdata('./results/cpu_karma_cell_out_0.100000_1_1000.csv');
% first_dt_minimal = importdata('./results/cpu_single_cell_out_0.100000_1_1000.csv');
% first_dt_karma_m = importdata('./results/karma_cell_out_maxeler_0.100000_1_1000.csv');
% first_dt_beelerreuter_m = importdata('./results/beelerreuter_out_maxeler_0.025000_1000.csv');
% first_dt_beelerreuter = importdata('./results/cpu_beelerreuter_cell_out_0.025000_2_1000.csv');
% 
% figure
% plot(linspace(0, 1000, 10000), first_dt_karma_m(:,1));

figure
semilogx(dts_minimal, errors_minimal); 
title('LSE Minimal Model');
xlabel('dt (ms)');
ylabel('error (ms)');
mini = dts_minimal(end);
max = dts_minimal(1);
xlim([mini max]);

figure
semilogx(dts_karma, errors_karma); 
title('LSE Karma Model');
xlabel('dt (ms)');
ylabel('error (ms)');
mini = dts_karma(end);
max = dts_karma(1);
xlim([mini max]);


figure
semilogx(dts_br, errors_beelerreuter); 
title('LSE BEELER REUTER MODEL');
xlabel('dt (ms)');
ylabel('error (ms)');
mini = dts_br(end);
max = dts_br(1);
xlim([mini max]);


figure
hold on 
%scatter(dts, errors_beelerreuter);
plot(dts_karma, errors_karma);
plot(dts_minimal, errors_minimal);
set(gca, 'xscale', 'log');
title('LSE KARMA VS MINIMAL MODEL');
xlabel('dt (ms)');
ylabel('error (ms)');
legend('2 Variable Karma Model', '4 Variable Minimal Model');
hold off

figure
hold on 
plot(dts, errors_beelerreuter);
%plot(dts_karma, errors_karma);
plot(dts_minimal, errors_minimal);
set(gca, 'xscale', 'log');
title('LSE BEELER-REUTER VS MINIMAL MODEL');
xlabel('dt (ms)');
ylabel('error (ms)');
legend('8 Variable Beeler-Reuter Model', '4 Variable Minimal Model');
hold off




% 
% 
% files = {};
% maxelerfiles = {};
%  count = 1;
%  dts = [];
%  num = 1;
% % maxf1 = sprintf('%smaxeler.csv', res_path);
% % maxf2 = sprintf('%smaxeler2.csv', res_path);
% % maxeler{1} = importdata(maxf1);
% % maxeler{2} = importdata(maxf2);
% % 
% while dt>=0.003;
% dts(count) = dt;
% name = sprintf('%scpu_single_cell_out_%f_%d_%d.csv', res_path, dt, mode, simTime);
% files{count} = name;
% maxeler =sprintf('%ssinglecell_out_maxeler_%f_%d_%d.dat', res_path, dt, mode, simTime);
% maxelerfiles{count} = maxeler;
% dt= dt/2;
% count = count+1;
% end
% num = (count-1);
% 
% 
% 
% res = {};
% maxres = {};
% for i=1:num
%     res{i} = importdata(files{i});
%     maxres{i} = importdata(maxelerfiles{i});
% end
% 
% u = {1, num};
% stim = {1,num};
% 
% 
% 
% 
% 
% 
% 
% x = {1, num};
% u_trim = {};
% uMaxeler = {};
% uMax_trim = {};
% val = 2;
% figure
% hold on
% for i=1:num
%     u{i} = res{i}(:,1);
%     stim{i} = res{i}(:,5);
%     uMaxeler{i} = maxres{i}(:,1);
%     stimMaxeler{i} = maxres{i}(:,5);
%     if(i>1)
%     uMax_trim{i} = uMaxeler{:,i}(val:val:end);
%     u_trim{i} = u{:,i}(val:val:end);
%     val = val*2;
%     else
%         uMax_trim{i} = uMaxeler{:,1};
%         u_trim{i} = u{:,i};
% 
%     end
% 
%     
%     hold on 
%     label = sprintf('CPU u(dt): %f', dts(i));
%     plot(linspace(0, simTime, iter), u_trim{:,i}, 'DisplayName', label);
%     legend('-DynamicLegend');
%     hold off
% % 
% %     subplot(num, 2, i)
% %      label2 = sprintf('Maxeler u(dt): %f', dts(i));
% %     plot(linspace(0, simTime, iter), uMax_trim{:,i}, 'DisplayName', label2);
% %     legend('-DynamicLegend')
% %     hold off
% 
%     
% 
%     
% 
%    
% end
% 
% mean_error_overall = 0;
% mean_error = {};
% maxerr = 0;
% cpuerr = 0;
% for i=1:num
%      for(j=1:iter)
%     r1 = uMax_trim{i}(j,1);
%     r2 = u_trim{i}(j,1);
% 
%     residual = r2-r1;
%     maxerr = maxerr + (residual*residual);
%     end
%     mean_error_overall = mean_error_overall + immse(u_trim{i}, uMax_trim{i});
%     mean_error{i} = immse(u_trim{i}, uMax_trim{i});
%    
% 
% end
% mean_error_overall = mean_error_overall/num;
%  disp(mean_error);
% disp(mean_error_overall);
% % dt1Maxeler = uMaxeler{:,1};
% % dt2Maxeler = uMaxeler{:,2}(2:2:end);
% % dt3Maxeler = uMaxeler{:,3}(4:4:end);
% 
% 
% 
%     figure
% for i=1:num
%     hold on 
%     label3 = sprintf('Maxeler u(dt): %f', dts(i));
%     plot(linspace(0, simTime, iter), uMax_trim{:,i}, 'DisplayName', label3);
%     legend('-DynamicLegend')
%     hold off
% end
% 
% % figure
% % plot(linspace(0, simTime, 20000), [dt1, dt2, dt3, dt1Maxeler, dt2Maxeler, dt3Maxeler]);
% %    legend('cpu dt', 'cpu dt/2', 'cpu dt/4','maxeler dt', 'maxeler dt/2', 'maxeler dt/4');
% %    xlabel('Time in milliseconds');
% %    title ('Double Step Method');
% % 
% % figure
% % plot(linspace(0, simTime, 20000), [dt1Maxeler, dt2Maxeler, dt3Maxeler]);
% %    legend('maxeler dt', 'maxeler dt/2', 'maxeler dt/4');
% %    xlabel('Time in miliseconds');
% %    title('Double Step Method Maxeler');
% % 
% % figure
% %  plot(linspace(0, simTime, 20000), [dt1, dt2Maxeler]);
% %     legend('cpu dt', 'maxeler dt/2');
% %    xlabel('Time in miliseconds');
% %    title('Double Step Method Ref CPU vs Maxeler dt/2');
% %    
% %    figure
% %  plot(linspace(0, simTime, 20000), [dt1, dt3Maxeler]);
% %     legend('cpu dt', 'maxeler dt/4');
% %    xlabel('Time in miliseconds');
% %    title('Double Step Method Ref CPU vs Maxeler dt/4');
% 
% 
% 
% 
% 
% % cpuerr1 = sprintf('%f', immse(dt1, dt2));
% % cpuerr2 = sprintf('%f', immse(dt1, dt3));
% 
% % maxerr1 = immse(dt1, dt1Maxeler);
% % maxerr2 = immse(dt2, dt2Maxeler);
% % maxerr3 = immse(dt3, dt3Maxeler);
% % maxerr4 = immse(dt1, dt2Maxeler);
% % maxerr5 = immse(dt1, dt3Maxeler);
% 
