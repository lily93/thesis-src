
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include "cell_model.h"

double ddt_o_dx2 = 0;
int number_of_cells = 0;
int simTime = 0;
int num_iteration = 0;
const int stimDur = 5;
double  dt = 0;
double diff = 0;
double dx = 0;

#define N number_of_cells
#define nxb N

   int blockSize;      // The launch configurator returned block size
    int minGridSize;    // The minimum grid size needed to achieve the maximum occupancy for a full device launch
    int gridSize;       // The actual grid size needed, based on input size

void checkCUDAError(const char *message)
{
     cudaError_t error = cudaGetLastError();
     if(error!=cudaSuccess)
     {
       fprintf(stderr,"ERROR: %s: %s\n", message, cudaGetErrorString(error) );
       exit(-1);
     }
}

void printTimes(float times) {
FILE *fp = fopen("GPU_times.dat", "a");
fprintf(fp, "Bits: %d\nSimulation Time (MS): %d\nNumber of Cells:%d\nDT: %g\n Grids: %d\n Blocks: %d\nElapsed Time: %g\n-------------------------------\n",bits, simTime, number_of_cells, dt, gridSize, blockSize, times);
fclose(fp);
}

Real heavisidefun_cpu(Real x) {
        if(x==0) {
                return 0.5;
        }
        if (x<0) {
                return 0.0;
        } if (x> 0){
                return 1.0;
        }
}
__device__ Real heavisidefun(Real x) {
        if(x==0) {
                return 0.5;
        }
        if (x<0) {
                return 0.0;
        } if (x> 0){
                return 1.0;
        }
}



void writeToFile(Real u, Real v, Real s, Real  w, Real stim) {
FILE *fp = fopen("gpuOut.csv", "a");
fprintf(fp, "%g\t%g\t%g\t%g\t%g\n", u, v, w,s,stim);
fclose(fp);
}

__global__ void calcPDECable (Real *u, Real *lap, Real ddt_o_dx2, int nx){
           int i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i == 0) {
            lap[i] = (ddt_o_dx2 * (-2.*u[i] + 2.*u[i+1])) ;
        }else if (i == nx-1){
            lap[i] = (ddt_o_dx2 * (-2.*u[i] +2.*u[i-1]));
        }else{
            lap[i] = (ddt_o_dx2 * (u[i+1] + u[i-1] - 2.0 * u[i]));
        }

        //printf("cell :%d\t lap: %g\n", i, lap[i]);
}

__global__ void calcStim(Real * stim,  Real t1, Real t2) {
        int i = threadIdx.x;

if( i<5) {
                stim[i] = ((heavisidefun(t1 - 0)*(1 - heavisidefun(t2 - 1)) + heavisidefun(t1 - 300)*(1 - heavisidefun(t2 - 301)) + heavisidefun(t1 - 700)*(1 - heavisidefun(t2 - 701)))==1)? 0.66 : 0;
                } else {
                stim[i] = 0.0;
                }

}

void createStim(Real * stim, Real t1, Real t2) {
        for(int i =0; i< N; i++) {
                stim[i]= ((heavisidefun_cpu(t1 - 0)*(1 - heavisidefun_cpu(t2 - 1)) + heavisidefun_cpu(t1 - 300)*(1 - heavisidefun_cpu(t2 - 301)) + heavisidefun_cpu(t1 - 700)*(1 - heavisidefun_cpu(t2 - 701)))==1&&i<5)? 0.66 : 0;

        }
}

__global__ void  calcODECable ( Real *state_vars_gpu, int nxb, Real dt,Real *lap, Real  * stim){

          Real u, v, w, s;
          Real tvm, vinf, winf;
          Real jfi, jso, jsi;
          Real twm, tso, ts, to, ds, dw, dv;


           int i = threadIdx.x + blockIdx.x * blockDim.x;


     if(i< nxb) {


              u     = state_vars_gpu[i];
              v     = state_vars_gpu[i+nxb];
              w     = state_vars_gpu[i+2*nxb];
              s     = state_vars_gpu[i+3*nxb];


              tvm =  (u > THVM) ? TV2M : TV1M;
              ts  = (u > THW)  ? TS2  : TS1;
              to  = (u > THO)  ? TO2  : TO1;
          tso =  TSO1 + (TSO2_TSO1_DIVBY2)*(1.+tanh(KSO*(u-USO)));
          twm =  1./(TW1M + (TW2M_TW1M_DIVBY2)*(1.+tanh(KWM*(u-UWM))));

          ds = (((1.+tanh(KS*(u-US)))/2.) - s)/ts;

          vinf = (u >  THVINF) ? 0.0: 1.0;
              winf = (u >  THWINF) ? WINFSTAR: 1.0-u/TWINF;

          dv = (u > THV) ? -v/TVP : (vinf-v)/tvm;
          dw = (u > THW) ? -w/TWP : (winf-w)*twm;


              //Update gates

          v    += dv*dt;
          w    += dw*dt;
          s    += ds*dt;

               //Compute currents
           jfi = (u > THV)  ? -v * (u - THV) * (UU - u)/TFI : 0.0;
           jso = (u > THSO) ? 1./tso : (u-UO)/to;

           jsi = (u > THSI) ? -w * s/TSI : 0.0;
                //Real stim = (active==1 && i<5)? 0.66 : 0.0;
           u = u-(jfi+jso+jsi-stim[i]) *dt + lap[i];
           state_vars_gpu[i]       = u;
           state_vars_gpu[i+nxb]   = v;
           state_vars_gpu[i+2*nxb] = w;
           state_vars_gpu[i+3*nxb] = s;
          }
}

void runGPU() {
 Real * state_vars_cable, * uo, * vo, * wo, *so, *stim_cpu;
   Real * state_vars_gpu, * cable_lap_gpu, *stim_gpu;
   state_vars_cable = (Real *) calloc (4 * N, sizeof (Real));
   stim_cpu = (Real *) calloc (N, sizeof(Real));
    uo       = state_vars_cable;
    vo       = state_vars_cable +      N;
    wo       = state_vars_cable + 2 *  N;
    so       = state_vars_cable + 3 *  N;



 printf("\nInitialating cable \n\n");
         for (int i=0; i < N; i++){
                uo[i] =  1.0;
                vo[i]  = 1.0;
                wo[i]  = 1.0;
                so[i]  = 0.0;
         }

  /*Memory Allocation on the GPU side*/
         cudaMalloc((void**) &state_vars_gpu,         4* N * sizeof(Real));
         checkCUDAError("1\n");
         cudaMalloc((void**) &cable_lap_gpu,             N * sizeof(Real));
         checkCUDAError("2\n");
         cudaMemcpy(state_vars_gpu, state_vars_cable, 4* N * sizeof(Real), cudaMemcpyHostToDevice);
        checkCUDAError("3\n");
        cudaMalloc((void**) &stim_gpu, N* sizeof(Real));
        cudaMemcpy(stim_gpu, stim_cpu, N*sizeof(Real), cudaMemcpyHostToDevice);
         /*Definition of Block Grid and Block Size*/


double t1 = 0;
double t2= 0;

 cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize,calcPDECable, 0, N);

    // Round up according to array size
    gridSize = (N + blockSize - 1) / blockSize;

Real * res = (Real *) calloc ((N*num_iteration), sizeof(Real));
printf("Running GPU\n");
                        float time;
cudaEvent_t start, stop;

cudaEventCreate(&start);
checkCUDAError("create start");
 cudaEventCreate(&stop);
checkCUDAError("create stop");
cudaEventRecord(start, 0);
checkCUDAError("start record");

for(int ntime= 0; ntime<=num_iteration; ntime++) {
        t1 += dt;
        t2 += dt;

//Real  active = ((heavisidefun(t1 - 0)*(1 - heavisidefun(t2 - 1)) + heavisidefun(t1 - 300)*(1 - heavisidefun(t2 - 301)) + heavisidefun(t1 - 700)*(1 - heavisidefun(t2 - 701)))==1);
//calcStim <<<1, N>>> (stim_gpu,t1, t2);
createStim(stim_cpu, t1, t2);
cudaMemcpy(stim_gpu, stim_cpu, N * sizeof(Real), cudaMemcpyHostToDevice);
checkCUDAError("4\n");
calcPDECable    <<<gridSize, blockSize>>> (state_vars_gpu, cable_lap_gpu, ddt_o_dx2, N);
checkCUDAError("5\n");
calcODECable   <<<gridSize, blockSize>>> (state_vars_gpu, N, dt, cable_lap_gpu, stim_gpu);
checkCUDAError("6\n");
//cudaThreadSynchronize();
cudaMemcpy(state_vars_cable, state_vars_gpu, 4 * N * sizeof(Real), cudaMemcpyDeviceToHost);
checkCUDAError("7\n");
//for(int c = 0; c < N ; c++) {
        //writeToFile(uo[c], vo[c], so[c], wo[c], 0.0);
//      printf("u: %g\n stim: %g\nstep:%d\n", uo[c], stim, ntime);
//}
}
  cudaMemcpy(state_vars_cable, state_vars_gpu, 4 * N * sizeof(Real), cudaMemcpyDeviceToHost);
         cudaEventRecord(stop, 0);
        checkCUDAError("stopped");
cudaEventSynchronize(stop);
checkCUDAError("sync");
 cudaEventElapsedTime(&time, start, stop);
checkCUDAError("elapsed time");

printf("GPU elapsed time: %g milliseconds \n", time);
        printTimes(time);
     //printf("\nFinishing cable computation \n\n");



     free( state_vars_cable );
     cudaFree(state_vars_gpu);
     cudaFree(cable_lap_gpu);


}
void test(int time) {
 dt                      =   0.0125;//Original is 0.1
                  dx                      =   0.02;//Original is 0.25
                  diff                    =   0.00116;
                  number_of_cells = 256;
                simTime = time;
  ddt_o_dx2      = dt*diff/(dx * dx);
          num_iteration = (int)(simTime/dt);



  printf("\n\n Parameters: \n");
          printf("timesteps    = %d\n", num_iteration);
          printf("number of cells     = %d\n", number_of_cells);
          printf("dt     = %f\n", dt);
          printf("dx    = %f\n", dx);
          printf("diff   = %f\n", diff);
          printf("ddt_o_dx2   = %f\n", ddt_o_dx2);

while(number_of_cells<10000) {

printf("\n\n Parameters: \n");
          printf("timesteps    = %d\n", num_iteration);
          printf("number of cells     = %d\n", number_of_cells);
          printf("dt     = %f\n", dt);
          printf("dx    = %f\n", dx);
          printf("diff   = %f\n", diff);
          printf("ddt_o_dx2   = %f\n", ddt_o_dx2);

runGPU();
number_of_cells = number_of_cells *2;
}
}

int main(int argc, char** argv)
{


        printf("hello world");
     int driverVersion = 0, runtimeVersion = 0;
     cudaDriverGetVersion(&driverVersion);
     printf("CUDA Driver Version:                           %d.%d\n", driverVersion/1000, driverVersion%100);
     cudaRuntimeGetVersion(&runtimeVersion);
     printf("CUDA Runtime Version:                          %d.%d\n", runtimeVersion/1000, runtimeVersion%100);
        test(2);
        test(1000);
}
